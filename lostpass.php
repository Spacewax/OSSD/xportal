<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

$xoopsOption['pagetype'] = "user";
include("mainfile.php");
if ( !isset($email) || $email == "" ) {
	redirect_header("user.php",2,_US_SORRYNOTFOUND);
	exit();
}

include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
$myts = new MyTextSanitizer();
$result = $xoopsDB->query("SELECT uid, uname, pass FROM ".$xoopsDB->prefix("users")." WHERE (email='".$myts->oopsAddSlashes($email)."')");

if ( !$result || $xoopsDB->num_rows($result)==0 ) {
	redirect_header("user.php",2,_US_SORRYNOTFOUND);
	exit();
} else {
	$host_name = getenv("REMOTE_ADDR");
	list($uid, $uname, $pass) = $xoopsDB->fetch_row($result);
	$uname = $myts->makeTboxData4Show($uname);
	$areyou = substr($pass, 0, 5);
	if ( isset($code) && $areyou == $code ) {
		$newpass=makepass();
		$message = sprintf(_US_HELLO,$uname);
		$message .= "\n\n";
		$message .= sprintf(_US_AUSERREQUESTED,$host_name);
		$message .="\n";
		$message .= sprintf(_US_YOURDETAILS,$xoopsConfig['sitename']);
		$message .= "\n\n"._US_NICKNAMECOLON." ".$uname."";
		$message .= "\n"._US_NEWPASSWORD." ".$newpass."\n\n";
		$message .= sprintf(_US_YOUCANCHANGE,$xoopsConfig['xoops_url']."/user.php");
		$message .= "\n\n"._US_DONTWORRYNOTTHEM."";
		$message .= "\n\n";
		$message .= sprintf(_US_SITETEAM,$xoopsConfig['sitename']);
		$message .= "\n";
		$message .= $xoopsConfig['xoops_url']."/";
		$subject = sprintf(_US_NEWPWDREQ,$xoopsConfig['xoops_url']);
		mail($email, $subject, $message, "From: ".$xoopsConfig['adminmail']."\nX-Mailer: PHP/" . phpversion());

	// Next step: add the new password to the database
		$cryptpass = md5($newpass);
		$query="UPDATE ".$xoopsDB->prefix("users")." SET pass='$cryptpass' WHERE uid=$uid";
		if ( !$xoopsDB->query($query) ) {
			include("header.php");
			echo _US_MAILPWDNG;
			include("footer.php");
			exit();
		}
		$pwdmessage = sprintf(_US_PWDMAILED,$uname);
		redirect_header("user.php",3,$pwdmessage);
		exit();
	// If no Code, send it
	} else {
		$result = $xoopsDB->query("select uname, pass from ".$xoopsDB->prefix("users")." where (email='".$myts->oopsAddSlashes($email)."')");
		if ( !$result ) {
			echo "<div style='text-align: center;'>"._US_SORRYNOTFOUND."</div>";
		} else {
			$host_name = getenv("REMOTE_ADDR");
		    list($uname, $pass) = $xoopsDB->fetch_row($result);
		    $areyou = substr($pass, 0, 5);
			$uname = $myts->makeTboxData4Show($uname);
			$message = sprintf(_US_HELLO,$uname);
			$message .= "\n\n";
			$message .= sprintf(_US_AUSERREQUESTED,$host_name);
			$message .= "\n\n"._US_GETNEWPASS."\n\n".$xoopsConfig['xoops_url']."/lostpass.php?email=".$email."&code=".$areyou."\n\n"._US_DONTWORRY."";
			$message .= "\n\n";
			$message .= sprintf(_US_SITETEAM,$xoopsConfig['sitename']);
			$message .= "\n";
			$message .= $xoopsConfig['xoops_url']."/";
			$subject=sprintf(_US_NEWPWDREQ,$xoopsConfig['xoops_url']);
			mail($email, $subject, $message, "From: ".$xoopsConfig['adminmail']."\nX-Mailer: PHP/" . phpversion());
			include ("header.php");
			echo "<h4>";
			printf(_US_CONFMAIL,$uname);
			echo "</h4>";
			include ("footer.php");
		}
	}
}
?>