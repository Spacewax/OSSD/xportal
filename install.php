<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

function mainfile_settings(){
	@include_once("mainfile.php");
	install_header();
	echo "<div style='text-align:center'>";
	if ( XOOPS_ROOT_PATH == "/path/to/xoops/directory" ) {
		echo "<table width=\"70%\"><tr><td>";
		echo "Please Open mainfile.php and find the following codes on line 34:<br /><br />";
		echo "<b>define(\"XOOPS_ROOT_PATH\",\"/path/to/xoops/directory\");</b><br /><br />";
		echo "Now, change this line to:<br /><br />";
		echo "<b>define(\"XOOPS_ROOT_PATH\",\"".getcwd()."\");</b>";
		echo "</td></tr></table>";
		echo "<br /><br /><input type=\"button\" value=\"OK, I have set the above setings, let me try again!\" onclick=\"location='install.php'\" /></div>";
		install_footer();
		exit();
	} else {
		$detected = getcwd();
		if ( XOOPS_ROOT_PATH != $detected ) {
			echo "<p>\n";
			echo "<font color=\"#ff0000\">WARNING!</font><br />";
			echo "There is a mismatch between your XOOPS_ROOT_PATH configuration on line 34 of mainfile.php and the root path info we have detected.<br />\n";
			echo "Your setting: <b>".XOOPS_ROOT_PATH."</b><br />";
			echo "We detected: <b>".$detected."</b><br />";
			echo "<b>( On MS platforms, you may receive this error message even when your configuration is correct. If that is the case, please press the button below to continue)</b><br /><br />\n";
			echo "Plesae press the button below to continue if this is really ok.\n";
			echo "</p>\n";
		} else {
			echo "Your root path: <b>".XOOPS_ROOT_PATH."</b><br />";
			echo "If the above setting is correct, press the button below to continue.<br />(This will not start creating tables yet)<br /><br />\n";
		}
		echo "<form action='install.php' method='post'>";
		echo "<input type=\"hidden\" name=\"op\" value=\"initial\">\n";
		echo "<input type=\"submit\" name=\"submit\" value=\"Next\">\n";
		echo "</form>\n";
		install_footer();
		exit();
	}
}


$myTables = array('groups','groups_users_link','groups_modules_link','bad_words','bannerclient','banner','bannerfinish','bb_categories','bb_config','bb_forums','bb_forum_access','bb_forum_mods','bb_posts','bb_posts_text','bb_topics','click','comments','ephem','faqanswer','faqcategories','headlines','lastseen','metafooter','mydownloads_broken','mydownloads_cat','mydownloads_downloads','mydownloads_mod','mydownloads_text','mydownloads_votedata','mylinks_broken','mylinks_cat','mylinks_links','mylinks_mod','mylinks_text','mylinks_votedata','poll_data','poll_desc','pollcomments','priv_msgs','quotes','ranks','seccont','sections','session','smiles','stories','stories_cat','topics','user_banlist','users','users_status','v2_archives','v2_domains','v2_visitors','newblocks','modules');

function install_header(){
    ?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head>
	<title>XOOPS Custom Installation</title>
	<meta http-equiv="Content-Type" content="text/html; charset=">
	<meta name="AUTHOR" content="XOOPS">
	<meta name="GENERATOR" content="XOOPS-- http://xoops.sourceforge.net/">
	<style type='text/css' media='all'><!-- @import url(xoops.css); --></style>
	</head>
	<body>
	<div style='text-align:center'><img src='images/logo.gif' /><h3>XOOPS Installation</h3></div><br />
<?php
}

function install_footer(){
    ?>
	<br /><br /><div style='text-align:center'><small>Install.php v1.0 for XOOPS by <a href="http://www.mywebaddons.com" target="_blank">myWebAddons.com</a></small><br /><br /><a href='http://www.xoops.org/'><img src='images/s_poweredby.gif' /></a></div>
	</body>
	</html>
<?php
}



function install_init(){
	global $xoopsConfig;
	install_header();
	if($xoopsConfig[dbhost]=="" || $xoopsConfig[dbname] == "" || $xoopsConfig[xoops_url] == "" || $xoopsConfig[prefix] =="" || !isset($xoopsConfig[dbhost]) || !isset($xoopsConfig[dbuname]) || !isset($xoopsConfig[dbpass]) || !isset($xoopsConfig[dbname]) || !isset($xoopsConfig[xoops_url]) || !isset($xoopsConfig[prefix])){
		echo "<div style='text-align:center'>\n";
		echo "Please open include/config.php and enter required DB settings data<br /><br />\n";
		echo "<table width=\"70%\">\n";
		if($xoopsConfig[dbhost]=="" || !isset($xoopsConfig[dbhost])){
			echo "<tr><td><b>\$xoopsConfig['dbhost']</b> is the hostname of your database server.</td></tr>\n";
		}
		if(!isset($xoopsConfig[dbuname])){
			echo "<tr><td><b>\$xoopsConfig['dbuname']</b> is the username of your database account.</td></tr>\n";
		}
		if(!isset($xoopsConfig[dbpass])){
			echo "<tr><td><b>\$xoopsConfig['dbpass']</b> is the password required to access your database.</td></tr>\n";
		}
		if($xoopsConfig[dbname] == "" || !isset($xoopsConfig[dbname])){
			echo "<tr><td><b>\$xoopsConfig['dbname']</b> is the name of your database in which XOOPS tables will be created.</td></tr>\n";
		}
		if ( $xoopsConfig['prefix'] =="" || !isset($xoopsConfig['prefix']) ) {
			echo "<tr><td><b>\$xoopsConfig['prefix']</b> is the prefix for tables that will be made during the installation. If you are upgrading from the older versions of *Nuke, please set this value to a value different from the one you have been using for your old versions. </td></tr>\n";
		}
		if ( $xoopsConfig['xoops_url'] == "" || $xoopsConfig['xoops_url'] == "http://yourdomain" ) {
			echo "<tr><td><b>\$xoopsConfig['xoops_url']</b> is the virtual path (URL) to the main directory of XOOPS (Generally, it is the directory where this install script is uploaded.). Please DO NOT add \"/\" at the end.</td></tr>\n";
		}
		echo "</table>\n";
		echo "<br /><br /><form action='/' method='post'><input type=\"button\" value=\"OK, I have set the above setings, let me try again!\" onclick=\"location='install.php?op=initial'\" /></form></div>\n";
		install_footer();
		exit();
	}
	echo "<table width=\"70%\" align=\"center\">\n";
	echo "<tr><td>We have detected the following information from your config file. Please fix it now if this is not correct.<br /><br />\n";
	echo "<b>Database Configuration</b><br /><br />\n";
	echo "&nbsp;Database Hostname:&nbsp;<b>".$xoopsConfig[dbhost]."</b><br />\n";
	echo "&nbsp;Database Username:&nbsp;<b>".$xoopsConfig[dbuname]."</b><br />\n";
	echo "&nbsp;Database Name:&nbsp;<b>".$xoopsConfig[dbname]."</b><br />\n";
	echo "&nbsp;Prefix for new tables:&nbsp;<b>".$xoopsConfig[prefix]."</b><br /><br />\n";
	echo "<b>Site Configuration</b><br /><br />\n";
	echo "&nbsp;URL (Virtual Path):&nbsp;<b>".$xoopsConfig[xoops_url]."</b><br /><br />\n";
	echo "<tr><td align=\"center\"><form action=\"install.php\" method=\"post\">\n";
	echo "If the above data is correct, press the button below to continue.<br />(This will not start creating tables yet)<br /><br />\n";
	echo "<input type=\"hidden\" name=\"op\" value=\"siteInit\">\n";
	echo "<input type=\"submit\" name=\"submit\" value=\"Next\">\n";
	echo "</form>\n";
	echo "</td></tr></table>\n";
	install_footer();
}


function install_createDB($adminname, $adminpass){
	global $xoopsConfig, $xoopsDB;
	$myts = new MytextSanitizer;
	$madetables = array();


#
# Table structure for table 'admingroups'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(groups)."` (
  `groupid` int(10) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` text,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY  (`groupid`),
  KEY `type` (`type`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(groups)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(groups));
}
#
# Dumping data for table 'admingroups'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(groups)." VALUES (1,'webmaster','webmasters of this site','Admin')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(groups)." VALUES (2,'Registered Users','Registered Users Group','User')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(groups)." VALUES (3,'Anonymous Users','Anonymous Users Group','Anonymous')",1);
# --------------------------------------------------------

#
# Table structure for table 'groups_users_link'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(groups_users_link)."` (
  `groupid` int(10) NOT NULL default '0',
  `uid` int(10) NOT NULL default '0',
  KEY `groupid_uid` (`groupid`,`uid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(groups_users_link)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(groups_users_link));
}
#
# Dumping data for table '".$xoopsDB->prefix(groups_users_link'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(groups_users_link)." VALUES (1,1)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(groups_users_link)." VALUES (2,1)",1);
# --------------------------------------------------------

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix("groups_modules_link")."` (
  `groupid` int(10) NOT NULL default '0',
  `mid` int(10) NOT NULL default '0',
  `type` char(1) default NULL,
  KEY `groupid_type_mid` (`groupid`, `type`, `mid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix("groups_modules_link")."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix("groups_modules_link"));
}
#
# Dumping data for table '".$xoopsDB->prefix(groups_modules_link'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("groups_modules_link")." VALUES (1,1,'A')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("groups_modules_link")." VALUES (1,2,'A')",1);
# --------------------------------------------------------


#
# Table structure for table 'bad_words'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bad_words)."` (
  `word_id` int(10) NOT NULL auto_increment,
  `word` varchar(100) default NULL,
  `replacement` varchar(100) default NULL,
  PRIMARY KEY  (`word_id`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bad_words)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bad_words));
}
#
# Dumping data for table 'bad_words'
#

# --------------------------------------------------------

#
# Table structure for table 'banner'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(banner)."` (
  `bid` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `imptotal` int(11) NOT NULL default '0',
  `impmade` int(11) NOT NULL default '0',
  `clicks` int(11) NOT NULL default '0',
  `imageurl` varchar(255) NOT NULL default '',
  `clickurl` varchar(255) NOT NULL default '',
  `date` int(10) NOT NULL default '0',
  PRIMARY KEY  (`bid`),
  KEY `idxbannercid` (`cid`),
  KEY `idxbannerbidcid` (`bid`,`cid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(banner)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(banner));
}
#
# Dumping data for table 'banner'
#
# --------------------------------------------------------

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("banner")." (bid, cid, imptotal, impmade, clicks, imageurl, clickurl, date) VALUES (1, 1, 0, 1, 0, '".$xoopsConfig['xoops_url']."/images/banners/xoops_banner.gif', 'http://www.xoops.org/', 1008813250)");

#
# Table structure for table 'bannerclient'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bannerclient)."` (
  `cid` int(11) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL default '',
  `contact` varchar(60) NOT NULL default '',
  `email` varchar(60) NOT NULL default '',
  `login` varchar(10) NOT NULL default '',
  `passwd` varchar(10) NOT NULL default '',
  `extrainfo` text NOT NULL,
  PRIMARY KEY  (`cid`),
  KEY `idxbannerclientlogin` (`login`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bannerclient)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bannerclient));
}
#
# Dumping data for table 'bannerclient'
#
# --------------------------------------------------------

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("bannerclient")." (cid, name, contact, email, login, passwd, extrainfo) VALUES (1, 'XOOPS', 'XOOPS Dev Team', 'webmaster@xoops.org', '', '', '')");

#
# Table structure for table 'bannerfinish'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bannerfinish)."` (
  `bid` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `impressions` int(11) NOT NULL default '0',
  `clicks` int(11) NOT NULL default '0',
  `datestart` int(10) NOT NULL default '0',
  `dateend` int(10) NOT NULL default '0',
  PRIMARY KEY  (`bid`),
  KEY `idxbannerfinishcid` (`cid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bannerfinish)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bannerfinish));
}
#
# Dumping data for table 'bannerfinish'
#

# --------------------------------------------------------

#
# Table structure for table 'bb_categories'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_categories)."` (
  `cat_id` int(10) NOT NULL auto_increment,
  `cat_title` varchar(100) default NULL,
  `cat_order` varchar(10) default NULL,
  PRIMARY KEY  (`cat_id`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_categories)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_categories));
}
#
# Dumping data for table 'bb_categories'
#
# --------------------------------------------------------

#
# Table structure for table 'bb_config'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_config)."` (
  config_id int(10) NOT NULL auto_increment,
  allow_html int(2) NOT NULL default '1',
  allow_sig int(2) NOT NULL default '1',
  selected int(2) NOT NULL default '1',
  posts_per_page int(10) NOT NULL default '10',
  hot_threshold int(10) NOT NULL default '10',
  topics_per_page int(10) NOT NULL default '20',
  PRIMARY KEY  (config_id),
  UNIQUE KEY selected (selected)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_config)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_config));
}
#
# Dumping data for table 'bb_config)."'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(bb_config)." VALUES (0,1,1,1,10,10,20)",1);
# --------------------------------------------------------

#
# Table structure for table 'bb_forum_access'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_forum_access)."` (
  `forum_id` int(10) NOT NULL default '0',
  `user_id` int(10) NOT NULL default '0',
  `can_post` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`forum_id`,`user_id`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_forum_access)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_forum_access));
}
#
# Dumping data for table 'bb_forum_access'
#
# --------------------------------------------------------

#
# Table structure for table 'bb_forum_mods'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_forum_mods)."` (
  `forum_id` int(10) NOT NULL default '0',
  `user_id` int(10) NOT NULL default '0',
  KEY `forum_user_id` (`forum_id`, `user_id`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_forum_mods)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_forum_mods));
}
#
# Dumping data for table 'bb_forum_mods'
#
# --------------------------------------------------------

#
# Table structure for table 'bb_forums'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_forums)."` (
  `forum_id` int(10) NOT NULL auto_increment,
  `forum_name` varchar(150) default NULL,
  `forum_desc` text,
  `forum_access` int(10) default '1',
  `forum_moderator` int(10) default NULL,
  `forum_topics` int(10) NOT NULL default '0',
  `forum_posts` int(10) NOT NULL default '0',
  `forum_last_post_id` int(10) unsigned NOT NULL default '0',
  `cat_id` int(10) default NULL,
  `forum_type` int(10) default '0',
  PRIMARY KEY  (`forum_id`),
  KEY `forum_last_post_id` (`forum_last_post_id`),
  KEY `cat_id` (`cat_id`)

)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_forums)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_forums));

}
#
# Dumping data for table 'bb_forums'
#
# --------------------------------------------------------

#
# Table structure for table 'bb_posts'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_posts)."` (
  `post_id` int(10) unsigned NOT NULL auto_increment,
  `pid` int(10) NOT NULL default '0',
  `topic_id` int(10) NOT NULL default '0',
  `forum_id` int(10) NOT NULL default '0',
  `post_time` int(10) NOT NULL default '0',
  `uid` int(10) unsigned NOT NULL default '0',
  `poster_ip` varchar(16) default NULL,
  `subject` varchar(255) NOT NULL default '',
  `nohtml` tinyint(2) NOT NULL default '0',
  `nosmiley` tinyint(2) NOT NULL default '0',
  `icon` varchar(25) default NULL,
  `attachsig` tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (`post_id`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_id` (`topic_id`),
  KEY `uid` (`uid`),
  KEY `post_time` (`post_time`),
  KEY `subject` (subject(40)),
  KEY `pid` (`pid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_posts)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_posts));
}
#
# Dumping data for table 'bb_posts'
#
# --------------------------------------------------------

#
# Table structure for table 'bb_posts_text'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_posts_text)."` (
  `post_id` int(10) NOT NULL default '0',
  `post_text` text,
  PRIMARY KEY  (`post_id`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_posts_text)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_posts_text));
}
#
# Dumping data for table 'bb_posts_text'
#

# --------------------------------------------------------

#
# Table structure for table 'bb_topics'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(bb_topics)."` (
  `topic_id` int(10) NOT NULL auto_increment,
  `topic_title` varchar(255) default NULL,
  `topic_poster` int(10) NOT NULL default '0',
  `topic_time` int(10) NOT NULL default '0',
  `topic_views` int(10) NOT NULL default '0',
  `topic_replies` int(10) NOT NULL default '0',
  `topic_last_post_id` int(10) unsigned NOT NULL default '0',
  `forum_id` int(10) NOT NULL default '0',
  `topic_status` int(10) NOT NULL default '0',
  `topic_notify` int(2) default '0',
  PRIMARY KEY  (`topic_id`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_forum` (`topic_id`,`forum_id`),
  KEY `topic_last_post_id` (`topic_last_post_id`),
  KEY `topic_poster` (`topic_poster`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(bb_topics)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(bb_topics));
}
#
# Dumping data for table 'bb_topics'
#




#
# Table structure for table 'click'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(click)."` (
  `num` int(10) NOT NULL auto_increment,
  `hits` int(10) NOT NULL default '0',
  `url` varchar(100) NOT NULL default '',
  `image` varchar(100) NOT NULL default '',
  `text` text NOT NULL,
  PRIMARY KEY  (`num`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(click)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(click));
}
#
# Dumping data for table 'click'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("click")." VALUES (1, 0, 'http://www.xoops.org/', '".$xoopsConfig['xoops_url']."images/poweredby.gif', 'XOOPS Official Website')",1);


# --------------------------------------------------------

#
# Table structure for table 'comments'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(comments)."` (
  `tid` int(11) NOT NULL auto_increment,
  `pid` int(11) NOT NULL default '0',
  `storyid` int(11) NOT NULL default '0',
  `date` int(10) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `host_name` varchar(20) NOT NULL default '',
  `subject` varchar(255) NOT NULL default '',
  `comment` text NOT NULL,
  `nohtml` tinyint(2) NOT NULL default '0',
  `nosmiley` tinyint(2) NOT NULL default '0',
  `icon` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`tid`),
  KEY `pid` (`pid`),
  KEY `storyid` (`storyid`),
  KEY `uid` (`uid`),
  KEY `subject` (subject(40))
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(comments)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(comments));
}
#
# Dumping data for table 'comments'
#


# --------------------------------------------------------

#
# Table structure for table 'ephem'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(ephem)."` (
  `eid` int(11) NOT NULL auto_increment,
  `did` int(2) NOT NULL default '0',
  `mid` int(2) NOT NULL default '0',
  `yid` int(4) NOT NULL default '0',
  `content` text NOT NULL,
  PRIMARY KEY  (`eid`),
  KEY `idxephemdidmid` (`did`,`mid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(ephem)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(ephem));
}
#
# Dumping data for table 'ephem'
#

# --------------------------------------------------------

#
# Table structure for table 'faqanswer'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(faqanswer)."` (
  `id` tinyint(4) NOT NULL auto_increment,
  `id_cat` tinyint(4) default NULL,
  `question` varchar(255) default NULL,
  `answer` text,
  PRIMARY KEY  (`id`),
  KEY `idxfaqansweridcat` (`id_cat`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(faqanswer)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(faqanswer));
}
#
# Dumping data for table 'faqanswer'
#

# --------------------------------------------------------

#
# Table structure for table 'faqcategories'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(faqcategories)."` (
  `id_cat` tinyint(3) NOT NULL auto_increment,
  `categories` varchar(255) default NULL,
  PRIMARY KEY  (`id_cat`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(faqcategories)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(faqcategories));
}
#
# Dumping data for table 'faqcategories'
#


# --------------------------------------------------------

#
# Table structure for table 'headlines'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(headlines)."` (
  `hid` int(11) NOT NULL auto_increment,
  `sitename` varchar(30) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `headlinesurl` varchar(200) NOT NULL default '',
  `status` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`hid`),
  KEY `idxheadlinesstatus` (`status`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(headlines)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(headlines));
}
#
# Dumping data for table '".$xoopsDB->prefix(headlines'
#
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (1,'XOOPS','http://xoops.sourceforge.net/','http://xoops.sourceforge.net/backend.php',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (2,'PHP-Nuke','http://www.phpnuke.org','http://www.phpnuke.org/backend.php',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (3,'LinuxPreview','http://linuxpreview.org','http://linuxpreview.org/backend.php3',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (4,'Slashdot','http://slashdot.org','http://slashdot.org/slashdot.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (5,'NewsForge','http://www.newsforge.com','http://www.newsforge.com/newsforge.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (6,'PHPBuilder','http://phpbuilder.com','http://phpbuilder.com/rss_feed.php',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (7,'Linux.com','http://linux.com','http://linux.com/mrn/front_page.rss',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (8,'Freshmeat','http://freshmeat.net','http://freshmeat.net/backend/fm.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (9,'myWebAddons.com','http://www.mywebaddons.com','http://www.mywebaddons.com/backend.php',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (10,'LinuxWeelyNews','http://lwn.net','http://lwn.net/headlines/rss',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (11,'HappyPenguin','http://happypenguin.org','http://happypenguin.org/html/news.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (12,'Segfault','http://segfault.org','http://segfault.org/stories.xml',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (13,'KDE','http://www.kde.org','http://www.kde.org/news/kdenews.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (14,'Perl.com','http://www.perl.com','http://www.perl.com/pace/perlnews.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (15,'Themes.org','http://themes.org','http://www.themes.org/news.rdf.phtml',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (16,'BrunchingShuttlecocks','http://www.brunching.com','http://www.brunching.com/brunching.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (17,'MozillaNewsBot','http://www.mozilla.org/newsbot/','http://www.mozilla.org/newsbot/newsbot.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (18,'NewsTrolls','http://newstrolls.com','http://newstrolls.com/newstrolls.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (19,'FreakTech','http://sunsite.auc.dk/FreakTech/','http://sunsite.auc.dk/FreakTech/FreakTech.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (20,'AbsoluteGames','http://files.gameaholic.com','http://files.gameaholic.com/agfa.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (21,'SciFi-News','http://www.technopagan.org/sf-news/','http://www.technopagan.org/sf-news/rdf.php',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (22,'SisterMachineGun','http://www.smg.org','http://www.smg.org/index/mynetscape.html',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (23,'LinuxM68k','http://www.linux-m68k.org','http://www.linux-m68k.org/linux-m68k.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (24,'Protest.net','http://www.protest.net','http://www.protest.net/netcenter_rdf.cgi',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (25,'HollywoodBitchslap','http://hollywoodbitchslap.com','http://hollywoodbitchslap.com/hbs.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (26,'DrDobbsTechNetCast','http://www.technetcast.com','http://www.technetcast.com/tnc_headlines.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (27,'myPHPNuke.com','http://www.myphpnuke.com','http://www.myphpnuke.com/backend.php',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (28,'Linuxpower','http://linuxpower.org','http://linuxpower.org/linuxpower.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (29,'PBSOnline','http://www.pbs.org','http://cgi.pbs.org/cgi-registry/featuresrdf.pl',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (30,'Listology','http://www.listology.com','http://listology.com/recent.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (31,'WebDeveloper.com','http://www.webdeveloper.com/','http://www.webdeveloper.com/webdeveloper.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (32,'LinuxNewbie','http://www.linuxnewbie.org','http://www.linuxnewbie.org/news.cdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (33,'exoScience','http://www.exosci.com','http://www.exosci.com/exosci.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (34,'WebReference.com','http://www.webreference.com','http://www.webreference.com/webreference.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (35,'PDABuzz','http://www.pdabuzz.com','http://www.pdabuzz.com/netscape.txt',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (36,'MicroUnices','http://mu.current.nu','http://mu.current.nu/mu.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (37,'TheNextLevel','http://www.the-nextlevel.com','http://www.the-nextlevel.com/rdf/tnl.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (38,'Gnotices','http://news.gnome.org/gnome-news/','http://news.gnome.org/gnome-news/rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (39,'DailyDaemonNews','http://daily.daemonnews.org','http://daily.daemonnews.org/ddn.rdf.php3',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (40,'PerlMonks','http://www.perlmonks.org','http://www.perlmonks.org/headlines.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (41,'PerlNews','http://news.perl.org','http://news.perl.org/perl-news-short.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (42,'BSDToday','http://www.bsdtoday.com','http://www.bsdtoday.com/backend/bt.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (43,'DotKDE','http://dot.kde.org','http://dot.kde.org/rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (44,'GeekNik','http://www.geeknik.net','http://www.geeknik.net/backend/weblog.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (45,'HotWired','http://www.hotwired.com','http://www.hotwired.com/webmonkey/meta/headlines.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (46,'JustLinux','http://www.justlinux.com','http://www.justlinux.com/backend/features.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (47,'LAN-Systems','http://www.lansystems.com','http://www.lansystems.com/backend/gazette_news_backend.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (48,'LinuxCentral','http://linuxcentral.com','http://linuxcentral.com/backend/lcnew.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (49,'Linux.nu','http://www.linux.nu','http://www.linux.nu/backend/lnu.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (50,'Lin-x-pert','http://www.lin-x-pert.com','http://www.lin-x-pert.com/linxpert_apps.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (51,'MaximumBSD','http://www.maximumbsd.com','http://www.maximumbsd.com/backend/weblog.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (52,'SolarisCentral','http://www.SolarisCentral.org','http://www.SolarisCentral.org/news/SolarisCentral.rdf',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (53,'DigitalTheatre','http://www.dtheatre.com','http://www.dtheatre.com/backend.php3?xml=yes',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (54,'PlanetX','http://www.planetx.com','http://www.planetx.com/xml.php',0)",1);

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (55,'PostNuke','http://www.postnuke.com','http://www.postnuke.com/backend.php',0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(headlines)." VALUES (56,'xml.com','http://www.xml.com','http://meerkat.oreillynet.com/&p=6304&_fl=rss',0)",1);



# --------------------------------------------------------

#
# Table structure for table 'lastseen'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(lastseen)."` (
  `uid` int(11) unsigned NOT NULL default '0',
  `username` varchar(25) NOT NULL default '',
  `time` int(11) NOT NULL default '0',
  `ip` varchar(15) default NULL,
  `online` tinyint(2) unsigned NOT NULL default '0',
  KEY `uid` (`uid`),
  KEY `time` (`time`),
  KEY `ip` (`ip`),
  KEY `online` (`online`)

)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(lastseen)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(lastseen));
}
#
# Dumping data for table 'lastseen'
#


# --------------------------------------------------------

#
# Table structure for table 'metafooter'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(metafooter)."` (
  `meta` text NOT NULL,
  `footer` text NOT NULL
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(metafooter)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(metafooter));
}
#
# Dumping data for table 'metafooter'
#
$result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(metafooter)." VALUES ('News, news, New, New, Technology, technology, Headlines, headlines, XOOPS, Xoops, xoops, OOP, oop, Nuke, nuke, myPHPNuke, myphpnuke, myphp-nuke, PHPNuke, SE, Geek, geek, Geeks, geeks, Hacker, hacker, Hackers, hackers, Linux, linux, Windows, windows, Software, software, Download, download, Downloads, downloads, Free, FREE, free, Community, community, Forum, forum, Forums, forums, Bulletin, bulletin, Board, board, Boards, boards, PHP, php, Survey, survey, Kernel, kernel, Comment, comment, Comments, comments, Portal, portal, ODP, odp, Open, open, Open Source, OpenSource, Opensource, opensource, open source, Free Software, FreeSoftware, Freesoftware, free software, GNU, gnu, GPL, gpl, License, license, Unix, UNIX, *nix, unix, MySQL, mysql, SQL, sql, Database, DataBase, database, Web Site, web site, Weblog, WebLog, weblog, Guru, GURU, guru', '\nThis web site was made with <a href=\"http://www.xoops.org/\">XOOPS</a>, a web portal system written in PHP.\n XOOPS is a free software released under the <a href=\"http://www.gnu.org/\">GNU/GPL license</a>.<br /><br /><img src=\"".$xoopsConfig['xoops_url']."/images/s_poweredby.gif\" alt=\"/\" />\n\n')",1);


# --------------------------------------------------------
#
# Table structure for table 'xoops_modules'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(modules)."` (
  `mid` int(10) NOT NULL auto_increment,
  `name` varchar(150) NOT NULL default '',
  `version` int(5) NOT NULL default '100',
  `last_update` int(10) NOT NULL default '0',
  `dirname` varchar(25) NOT NULL default '',
  `hasmain` tinyint(1) NOT NULL default '0',
  `hasadmin` tinyint(1) NOT NULL default '0',
  `hassearch` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`mid`),
  KEY `hasmain` (`hasmain`),
  KEY `hasadmin` (`hasadmin`),
  KEY `hassearch` (`hassearch`),
  KEY `dirname` (`dirname`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(modules)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(modules));
}
#
# Dumping data for table 'xoops_modules'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(modules)." VALUES (1,'System Admin',100,1006722151,'system',0,1,0)",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(modules)." VALUES (2,'News',100,1006722023,'news',1,1,1)",1);


#
# Table structure for table 'xoops_newblocks'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(newblocks)."` (
  `bid` int(10) NOT NULL auto_increment,
  `mid` int(10) NOT NULL default '0',
  `func_num` int(5) NOT NULL default '0',
  `options` varchar(255) NOT NULL default '',
  `name` varchar(150) NOT NULL default '',
  `position` tinyint(2) NOT NULL default '0',
  `title` varchar(150) NOT NULL default '',
  `content` text NOT NULL,
  `side` tinyint(2) NOT NULL default '0',
  `weight` int(5) NOT NULL default '0',
  `visible` tinyint(2) NOT NULL default '0',
  `type` char(1) NOT NULL default '',
  `c_type` char(1) NOT NULL default '',
  PRIMARY KEY  (`bid`),
  KEY `mid` (`mid`),
  KEY `visible` (`visible`),
  KEY `side_visible_mid` (`side`, `visible`, `mid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(newblocks)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(newblocks));
}
#
# Dumping data for table 'xoops_newblocks'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (1,1,1,'','User Block',1,'','',0,0,1,'S','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (2,1,2,'','Login Block',1,'','',0,0,1,'S','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (3,1,3,'','Search Block',1,'','',1,0,1,'S','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (4,1,4,'','Waiting Contents Block',1,'','',0,0,1,'S','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (5,1,5,'','Main Menu Block',0,'','',0,0,1,'S','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (6, 1, 6, '320|250|poweredby.gif', 'Site Info Block', 0, '', '', 0, 1, 1, 'S', 'H')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (7,2,1,'','News Topics Block',0,'','',0,0,1,'M','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (8,2,2,'','News Categories Block',0,'','',0,0,1,'M','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (9,2,3,'','Big Story Block',0,'','',1,0,1,'M','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (10,2,4,'counter|10','Top News Block',0,'','',4,0,1,'M','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(newblocks)." VALUES (11,2,5,'published|10','Recent News Block',0,'','',3,0,1,'M','')",1);

#
# Table structure for table 'mydownloads_broken'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mydownloads_broken)."` (
  `reportid` int(5) NOT NULL auto_increment,
  `lid` int(11) NOT NULL default '0',
  `sender` int(11) NOT NULL default '0',
  `ip` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`reportid`),
  KEY `lid` (`lid`),
  KEY `sender` (`sender`),
  KEY `ip` (`ip`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mydownloads_broken)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mydownloads_broken));
}
#
# Dumping data for table 'mydownloads_broken'
#

# --------------------------------------------------------

#
# Table structure for table 'mydownloads_cat'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mydownloads_cat)."` (
  `cid` int(5) unsigned NOT NULL auto_increment,
  `pid` int(11) unsigned NOT NULL default '0',
  `title` varchar(50) NOT NULL default '',
  `imgurl` varchar(150) NOT NULL default '',
  PRIMARY KEY  (`cid`),
  KEY `pid` (`pid`)
) ",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mydownloads_cat)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mydownloads_cat));
}
#
# Dumping data for table 'mydownloads_cat'
#


# --------------------------------------------------------

#
# Table structure for table 'mydownloads_downloads'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mydownloads_downloads)."` (
  `lid` int(11) unsigned NOT NULL auto_increment,
  `cid` int(5) unsigned NOT NULL default '0',
  `title` varchar(100) NOT NULL default '',
  `url` varchar(250) NOT NULL default '',
  `homepage` varchar(100) NOT NULL default '',
  `version` varchar(10) NOT NULL default '',
  `size` int(8) NOT NULL default '0',
  `platform` varchar(50) NOT NULL default '',
  `logourl` varchar(60) NOT NULL default '',
  `submitter` int(11) NOT NULL default '0',
  `status` tinyint(2) NOT NULL default '0',
  `date` int(10) NOT NULL default '0',
  `hits` int(11) unsigned NOT NULL default '0',
  `rating` double(6,4) NOT NULL default '0.0000',
  `votes` int(11) unsigned NOT NULL default '0',
  `comments` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`lid`),
  KEY `title` (title(40)),
  KEY `cid` (`cid`),
  KEY `status` (`status`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mydownloads_downloads)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mydownloads_downloads));
}
#
# Dumping data for table 'mydownloads_downloads'
#


# --------------------------------------------------------

#
# Table structure for table 'mydownloads_mod'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mydownloads_mod)."` (
  `requestid` int(11) unsigned NOT NULL auto_increment,
  `lid` int(11) unsigned NOT NULL default '0',
  `cid` int(5) unsigned NOT NULL default '0',
  `title` varchar(100) NOT NULL default '',
  `url` varchar(250) NOT NULL default '',
  `homepage` varchar(100) NOT NULL default '',
  `version` varchar(10) NOT NULL default '',
  `size` int(8) NOT NULL default '0',
  `platform` varchar(50) NOT NULL default '',
  `logourl` varchar(60) NOT NULL default '',
  `description` text NOT NULL,
  `modifysubmitter` int(11) NOT NULL default '0',
  PRIMARY KEY  (`requestid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mydownloads_mod)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mydownloads_mod));
}
#
# Dumping data for table 'mydownloads_mod'
#

# --------------------------------------------------------

#
# Table structure for table 'mydownloads_text'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mydownloads_text)."` (
  `lid` int(11) unsigned NOT NULL default '0',
  `description` text NOT NULL,
  KEY `lid` (`lid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mydownloads_text)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mydownloads_text));
}
#
# Dumping data for table 'mydownloads_text'
#


# --------------------------------------------------------

#
# Table structure for table 'mydownloads_votedata'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mydownloads_votedata)."` (
  `ratingid` int(11) unsigned NOT NULL auto_increment,
  `lid` int(11) unsigned NOT NULL default '0',
  `ratinguser` int(11) NOT NULL default '0',
  `rating` tinyint(3) unsigned NOT NULL default '0',
  `ratinghostname` varchar(60) NOT NULL default '',
  `ratingtimestamp` int(10) NOT NULL default '0',
  PRIMARY KEY  (`ratingid`),
  KEY `ratinguser` (`ratinguser`),
  KEY `ratinghostname` (`ratinghostname`),
  KEY `ratingtimestamp` (`ratingtimestamp`),
  KEY `lid` (`lid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mydownloads_votedata)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mydownloads_votedata));
}
#
# Dumping data for table 'mylinks_votedata'
#


#
# Table structure for table 'mylinks_broken'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mylinks_broken)."` (
  `reportid` int(5) unsigned NOT NULL auto_increment,
  `lid` int(11) unsigned NOT NULL default '0',
  `sender` int(11) unsigned NOT NULL default '0',
  `ip` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`reportid`),
  KEY `lid` (`lid`),
  KEY `sender` (`sender`),
  KEY `ip` (`ip`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mylinks_broken)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mylinks_broken));
}
#
# Dumping data for table 'mylinks_broken'
#

# --------------------------------------------------------

#
# Table structure for table 'mylinks_cat'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mylinks_cat)."` (
  `cid` int(5) unsigned NOT NULL auto_increment,
  `pid` int(11) unsigned NOT NULL default '0',
  `title` varchar(50) NOT NULL default '',
  `imgurl` varchar(150) NOT NULL default '',
  PRIMARY KEY  (`cid`),
  KEY `pid` (`pid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mylinks_cat)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mylinks_cat));
}
#
# Dumping data for table 'mylinks_cat'
#


# --------------------------------------------------------

#
# Table structure for table 'mylinks_links'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mylinks_links)."` (
  `lid` int(11) unsigned NOT NULL auto_increment,
  `cid` int(5) unsigned NOT NULL default '0',
  `title` varchar(100) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `email` varchar(60) NOT NULL default '',
  `logourl` varchar(60) NOT NULL default '',
  `submitter` int(11) unsigned NOT NULL default '0',
  `status` tinyint(2) NOT NULL default '0',
  `date` int(10) NOT NULL default '0',
  `hits` int(11) unsigned NOT NULL default '0',
  `rating` double(6,4) NOT NULL default '0.0000',
  `votes` int(11) unsigned NOT NULL default '0',
  `comments` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`lid`),
  KEY `title` (title(40)),
  KEY `cid` (`cid`),
  KEY `status` (`status`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mylinks_links)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mylinks_links));
}
#
# Dumping data for table 'mylinks_links'
#


# --------------------------------------------------------

#
# Table structure for table 'mylinks_mod'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mylinks_mod)."` (
  `requestid` int(11) unsigned NOT NULL auto_increment,
  `lid` int(11) unsigned NOT NULL default '0',
  `cid` int(5) unsigned NOT NULL default '0',
  `title` varchar(100) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `email` varchar(60) NOT NULL default '',
  `logourl` varchar(150) NOT NULL default '',
  `description` text NOT NULL,
  `modifysubmitter` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`requestid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mylinks_mod)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mylinks_mod));
}
#
# Dumping data for table 'mylinks_mod'
#

# --------------------------------------------------------

#
# Table structure for table 'mylinks_text'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mylinks_text)."` (
  `lid` int(11) unsigned NOT NULL default '0',
  `description` text NOT NULL,
  KEY `lid` (`lid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mylinks_text)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mylinks_text));
}
#
# Dumping data for table 'mylinks_text'
#


# --------------------------------------------------------

#
# Table structure for table 'mylinks_votedata'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(mylinks_votedata)."` (
  `ratingid` int(11) unsigned NOT NULL auto_increment,
  `lid` int(11) unsigned NOT NULL default '0',
  `ratinguser` int(11) unsigned NOT NULL default '0',
  `rating` tinyint(3) unsigned NOT NULL default '0',
  `ratinghostname` varchar(60) NOT NULL default '',
  `ratingtimestamp` int(10) NOT NULL default '0',
  PRIMARY KEY  (`ratingid`),
  KEY `ratinguser` (`ratinguser`),
  KEY `ratinghostname` (`ratinghostname`),
  KEY `ratingtimestamp` (`ratingtimestamp`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(mylinks_votedata)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(mylinks_votedata));
}
#
# Dumping data for table 'mylinks_votedata'
#


# --------------------------------------------------------

#
# Table structure for table 'poll_data'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(poll_data)."` (
  `pollID` int(11) NOT NULL default '0',
  `optionText` char(50) NOT NULL default '',
  `optionCount` int(11) NOT NULL default '0',
  `voteID` int(11) NOT NULL default '0',
  KEY `idxpolldatapollidvoteid` (`pollID`,`voteID`),
  KEY `idxpolldatapolliddesc` (`pollID`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(poll_data)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(poll_data));
}
#
# Dumping data for table 'poll_data'
#
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', 'Ummmm, not bad', '0', '1')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', 'Cool', '0', '2')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', 'Terrific', '0', '3')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', 'The best one!', '0', '4')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', 'what the hell is this?', '0', '5')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', '', '0', '6')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', '', '0', '7')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', '', '0', '8')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', '', '0', '9')",1);
            $result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_data)." VALUES ( '1', '', '0', '10')",1);



# --------------------------------------------------------

#
# Table structure for table 'poll_desc'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(poll_desc)."` (
  `pollID` int(11) NOT NULL auto_increment,
  `pollTitle` char(100) NOT NULL default '',
  `timeStamp` int(11) NOT NULL default '0',
  `voters` mediumint(9) NOT NULL default '0',
  `display` tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (`pollID`),
  KEY `idxpolldesctimestamp` (`timeStamp`),
  KEY `display` (`display`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(poll_desc)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(poll_desc));
}
#
# Dumping data for table 'poll_desc'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(poll_desc)." VALUES (1,'What do you think about XOOPS?',999724846,0,1)",1);
# --------------------------------------------------------

#
# Table structure for table 'pollcomments'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(pollcomments)."` (
  `tid` int(11) NOT NULL auto_increment,
  `pid` int(11) NOT NULL default '0',
  `pollID` int(11) NOT NULL default '0',
  `date` int(10) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `host_name` varchar(20) NOT NULL default '',
  `subject` varchar(255) NOT NULL default '',
  `comment` text NOT NULL,
  `nohtml` tinyint(2) NOT NULL default '0',
  `nosmiley` tinyint(2) NOT NULL default '0',
  `icon` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`tid`),
  KEY `pid` (`pid`),
  KEY `pollID` (`pollID`),
  KEY `uid` (`uid`),
  KEY `subject` (subject(40))
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(pollcomments)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(pollcomments));
}
#
# Dumping data for table 'pollcomments'
#


# --------------------------------------------------------

#
# Table structure for table 'priv_msgs'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(priv_msgs)."` (
  `msg_id` int(10) NOT NULL auto_increment,
  `msg_image` varchar(100) default NULL,
  `subject` varchar(100) default NULL,
  `from_userid` int(10) unsigned NOT NULL default '0',
  `to_userid` int(10) unsigned NOT NULL default '0',
  `msg_time` int(10) NOT NULL default '0',
  `msg_text` text,
  `read_msg` tinyint(10) NOT NULL default '0',
  PRIMARY KEY  (`msg_id`),
  KEY `to_userid` (`to_userid`),
  KEY `idxprivmsgstouseridreadmsg` (`to_userid`,`read_msg`),
  KEY `idxprivmsgsmsgidfromuserid` (`msg_id`,`from_userid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(priv_msgs)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(priv_msgs));
}
#
# Dumping data for table 'priv_msgs'
#


# --------------------------------------------------------

#
# Table structure for table 'quotes'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(quotes)."` (
  `qid` int(10) unsigned NOT NULL auto_increment,
  `quote` text,
  `author` varchar(150) NOT NULL default '',
  PRIMARY KEY  (`qid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(quotes)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(quotes));
}
#
# Dumping data for table 'quotes'
#

# --------------------------------------------------------

#
# Table structure for table 'ranks'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(ranks)."` (
  `rank_id` int(5) NOT NULL auto_increment,
  `rank_title` varchar(50) NOT NULL default '',
  `rank_min` int(10) NOT NULL default '0',
  `rank_max` int(10) NOT NULL default '0',
  `rank_special` int(2) NOT NULL default '0',
  `rank_image` varchar(255) default NULL,
  PRIMARY KEY  (`rank_id`),
  KEY `rank_min` (`rank_min`),
  KEY `rank_max` (`rank_max`),
  KEY `idxranksrankminrankmaxranspecial` (`rank_min`,`rank_max`,`rank_special`),
  KEY `idxranksrankspecial` (`rank_special`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(ranks)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(ranks));
}
#
# Dumping data for table 'ranks'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(ranks)." VALUES (2,'Just popping in',0,20,0,'')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(ranks)." VALUES (3,'Not too shy to talk',21,40,0,'star.gif')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(ranks)." VALUES (4,'Quite a regular',41,70,0,'stars2.gif')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(ranks)." VALUES (5,'Just can\'t stay away',71,150,0,'stars3.gif')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(ranks)." VALUES (6,'Home away from home',151,10000,0,'stars4.gif')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(ranks)." VALUES (7,'Webmaster',-1,-1,1,'redstars5.gif')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(ranks)." VALUES (8,'Moderator',-1,-1,1,'stars5.gif')",1);
# --------------------------------------------------------



#
# Table structure for table 'seccont'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(seccont)."` (
  `artid` int(11) NOT NULL auto_increment,
  `secid` int(11) NOT NULL default '0',
  `title` text NOT NULL,
  `content` text NOT NULL,
  `counter` int(11) NOT NULL default '0',
  PRIMARY KEY  (`artid`),
  KEY `idxseccontsecid` (`secid`),
  KEY `idxseccontcounterdesc` (`counter`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(seccont)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(seccont));
}
#
# Dumping data for table 'seccont'
#


# --------------------------------------------------------

#
# Table structure for table 'sections'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(sections)."` (
  `secid` int(11) NOT NULL auto_increment,
  `secname` varchar(40) NOT NULL default '',
  `image` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`secid`),
  KEY `idxsectionssecname` (`secname`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(sections)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(sections));
}
#
# Dumping data for table '".$xoopsDB->prefix(sections'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(sections)." VALUES (1,'Section Test','doc.jpg')",1);
# --------------------------------------------------------

#
# Table structure for table 'session'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(session)."` (
  `uid` int(11) unsigned NOT NULL default '0',
  `time` int(11) NOT NULL default '',
  `ip` varchar(15) NOT NULL default '',
  `hash` varchar(32) NOT NULL default '',
  KEY `uid` (`uid`),
  KEY `time` (`time`),
  KEY `hash_ip` (`hash`,`ip`),
  KEY `uid_ip` (`uid`,`ip`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(session)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(session));
}
#
# Dumping data for table 'session'
#


# --------------------------------------------------------

#
# Table structure for table 'smiles'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(smiles)."` (
  `id` int(10) NOT NULL auto_increment,
  `code` varchar(50) default NULL,
  `smile_url` varchar(100) default NULL,
  `emotion` varchar(75) default NULL,
  PRIMARY KEY  (`id`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(smiles)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(smiles));
}
#
# Dumping data for table 'smiles'
#


$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (1,':-D','icon_biggrin.gif','Very Happy')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (2,':-)','icon_smile.gif','Smile')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (3,':-(','icon_frown.gif','Sad')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (4,':-o','icon_eek.gif','Surprised')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (5,':-?','icon_confused.gif','Confused')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (6,'8-)','icon_cool.gif','Cool')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (7,':lol:','icon_lol.gif','Laughing')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (8,':-x','icon_mad.gif','Mad')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (9,':-P','icon_razz.gif','Razz')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (10,':oops:','icon_redface.gif','Embaressed')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (11,':cry:','icon_cry.gif','Crying (very sad)')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (12,':evil:','icon_evil.gif','Evil or Very Mad')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (13,':roll:','icon_rolleyes.gif','Rolling Eyes')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (14,';-)','icon_wink.gif','Wink')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (15,':pint:','icon_drink.gif','Another pint of beer')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (16,':hammer:','icon_hammer.gif','ToolTimes at work')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(smiles)." VALUES (17,':idea:','icon_idea.gif','I have an idea')",1);
# --------------------------------------------------------

#
# Table structure for table 'stories'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(stories)."` (
  `storyid` int(11) NOT NULL auto_increment,
  `catid` int(11) NOT NULL default '0',
  `uid` int(11) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `created` int(10) NOT NULL default '0',
  `published` int(10) NOT NULL default '0',
  `hostname` varchar(20) NOT NULL default '',
  `nohtml` tinyint(2) NOT NULL default '0',
  `nosmiley` tinyint(2) NOT NULL default '0',
  `hometext` text NOT NULL,
  `bodytext` text NOT NULL,
  `counter` mediumint(8) unsigned default '0',
  `topicid` int(11) NOT NULL default '1',
  `notes` text NOT NULL,
  `ihome` int(1) NOT NULL default '0',
  `notifypub` tinyint(2) NOT NULL default '0',
  `type` varchar(10) NOT NULL default '0',
  PRIMARY KEY  (`storyid`),
  KEY `idxstoriestime` (`created`),
  KEY `idxstoriescatid` (`catid`),
  KEY `idxstoriestopic` (`topicid`),
  KEY `published` (`published`),
  KEY `ihome` (`ihome`),
  KEY `published_ihome` (`published`,`ihome`),
  KEY `uid` (`uid`),
  KEY `title` (title(40))
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(stories)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(stories));
}
#
# Dumping data for table 'stories'
#
$result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(stories)." VALUES ( '1', '0', '1', 'Welcome to XOOPS!!', '".time()."', '".time()."','150.0.0.0', '0', '0', 'This is the first article in XOOPS. You can delete it. Pelase read carefully the README file for some details, CREDITS files to see from where comes the things and remember that this is a free software under the GPL License (COPYING file for details). Hope you enjoy this software. Please report any bug you have found to the support team at xoops.sourceforge.net.', '', '1', '1', '', '0', '0', '0');",1);

# --------------------------------------------------------

#
# Table structure for table 'stories_cat'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(stories_cat)."` (
  `catid` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL default '',
  `counter` int(11) NOT NULL default '0',
  PRIMARY KEY  (`catid`),
  KEY `idxstoriescattitle` (`title`),
  KEY `idxstoriescatcounterdesc` (`counter`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(stories_cat)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(stories_cat));
}
#
# Dumping data for table 'stories_cat'
#

# --------------------------------------------------------

#
# Table structure for table 'topics'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(topics)."` (
  `topicid` int(11) unsigned NOT NULL auto_increment,
  `pid` int(11) unsigned NOT NULL default '0',
  `topicimage` varchar(20) default NULL,
  `topictext` varchar(50) default NULL,
  PRIMARY KEY  (`topicid`),
  KEY `pid` (`pid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(topics)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(topics));
}
#
# Dumping data for table 'topics'
#
$result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(topics)." VALUES (1,0,'xoops.gif','XOOPS')",1);

# --------------------------------------------------------

#
# Table structure for table 'user_banlist'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(user_banlist)."` (
  `ban_id` int(10) NOT NULL auto_increment,
  `ban_userid` int(10) default NULL,
  `ban_ip` varchar(16) default NULL,
  `ban_start` int(32) default NULL,
  `ban_end` int(50) default NULL,
  `ban_time_type` int(10) default NULL,
  PRIMARY KEY  (`ban_id`),
  KEY `ban_userid` (`ban_userid`),
  KEY `ban_end` (`ban_end`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(user_banlist)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(user_banlist));
}
#
# Dumping data for table 'user_banlist'
#

# --------------------------------------------------------

#
# Table structure for table 'users'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(users)."` (
  `uid` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(60) NOT NULL default '',
  `uname` varchar(25) NOT NULL default '',
  `email` varchar(60) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `user_avatar` varchar(30) default NULL,
  `user_regdate` int(10) NOT NULL default '',
  `user_icq` varchar(15) default NULL,
  `user_from` varchar(100) default NULL,
  `user_sig` text,
  `user_viewemail` tinyint(2) NOT NULL default '0',
  `actkey` varchar(8) default NULL,
  `user_aim` varchar(18) default NULL,
  `user_yim` varchar(25) default NULL,
  `user_msnm` varchar(25) default NULL,
  `pass` varchar(40) NOT NULL default '',
  `posts` int(10) default '0',
  `attachsig` int(2) default '0',
  `rank` int(5) NOT NULL default '0',
  `level` int(5) NOT NULL default '1',
  `theme` varchar(100) NOT NULL default '',
  `timezone_offset` float(3,1) NOT NULL default '0.0',
  PRIMARY KEY  (`uid`),
  KEY `idxusersuname` (`uname`),
  KEY `idxusersemail` (`email`),
  KEY `idxusersuiduname` (`uid`,`uname`),
  KEY `idxusersunamepass` (`uname`,`pass`)
)",1);

if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(users)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(users));
}
#
# Dumping data for table 'users'
#

$temp = md5($adminpass);
$regdate = time();
$dbadminname= $myts->makeTboxData4Save($adminname);
$result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(users)." VALUES ('0','','Anonymous','".$adminmail."','','blank.gif','".$regdate."','','','',0,NULL,'','','','".$temp."',0,0,0,0,'','0.0')",1);
$result = $xoopsDB->query("UPDATE ".$xoopsDB->prefix(users)." SET uid=0 WHERE uid=1",1);
$result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(users)." VALUES (1,'','".$dbadminname."','".$adminmail."','".$xoopsConfig['xoops_url']."/','001.gif','".$regdate."','','','',1,NULL,'','','','".$temp."',0,0,7,5,'','0.0')",1);


# --------------------------------------------------------

#
# Table structure for table 'users_status'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(users_status)."` (
  `uid` int(11) unsigned NOT NULL default '0',
  `umode` varchar(10) NOT NULL default '',
  `uorder` tinyint(1) NOT NULL default '0',
  `ublockon` tinyint(1) NOT NULL default '0',
  `ublock` tinytext NOT NULL,
  `user_occ` varchar(100) default NULL,
  `bio` tinytext NOT NULL,
  `user_intrest` varchar(150) default NULL,
  PRIMARY KEY  (`uid`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(users_status)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(users_status));
}
#
# Dumping data for table 'users_status'
#
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(users_status)." VALUES (0,'',0,0,'','','','')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(users_status)." VALUES (1,'',0,0,'','','','')",1);


# --------------------------------------------------------

#
# Table structure for table 'v2_archives'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(v2_archives)."` (
  `code` int(11) NOT NULL auto_increment,
  `annee` int(11) NOT NULL default '0',
  `mois` int(11) NOT NULL default '0',
  `vpm` int(11) NOT NULL default '0',
  `topVis` blob NOT NULL,
  `topNavOS` blob NOT NULL,
  `topOS` blob NOT NULL,
  `topNav` blob NOT NULL,
  `vph` blob NOT NULL,
  `topRef` blob NOT NULL,
  `topDom` blob NOT NULL,
  `vpj` blob NOT NULL,
  PRIMARY KEY  (`code`),
  KEY `annee` (`annee`),
  KEY `mois` (`mois`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(v2_archives)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(v2_archives));
}
#
# Dumping data for table 'v2_archives'
#

# --------------------------------------------------------

#
# Table structure for table 'v2_domains'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(v2_domains)."` (
  `code` int(11) NOT NULL auto_increment,
  `domaine` char(20) NOT NULL default '',
  `description` char(50) NOT NULL default '',
  PRIMARY KEY  (`code`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(v2_domains)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(v2_domains));
}
#
# Dumping data for table 'v2_domains'
#

$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (1,'ac','Ascension Island')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (2,'ad','Andorra')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (3,'ae','United Arab Emirates')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (4,'af','Afghanistan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (5,'ag','Antigua and Barbuda')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (6,'ai','Anguilla')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (7,'al','Albania')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (8,'am','Armenia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (9,'an','Netherlands Antilles')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (10,'ao','Angola')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (11,'aq','Antartica')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (12,'ar','Argentina')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (13,'as','American Samoa')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (14,'au','Australia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (15,'aw','Aruba')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (16,'az','Azerbaijan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (17,'ba','Bosnia and Herzegovina')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (18,'bb','Barbados')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (19,'bd','Bangladesh')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (20,'be','Belgium')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (21,'bf','Burkina Faso')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (22,'bg','Bulgaria')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (23,'bh','Bahrain')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (24,'bi','Burundi')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (25,'bj','Benin')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (26,'bm','Bermuda')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (27,'bn','Brunei Darussalam')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (28,'bo','Bolivia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (29,'br','Brazil')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (30,'bs','Bahamas')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (31,'bt','Bhutan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (32,'bv','Bouvet Island')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (33,'bw','Botswana')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (34,'by','Belarus')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (35,'bz','Belize')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (36,'ca','Canada')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (37,'cc','Cocos (Keeling) Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (38,'cd','Congo, Democratic People\'s Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (39,'cf','Central African Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (40,'cg','Congo, Republic of')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (41,'ch','Switzerland')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (42,'ci','Cote d\'Ivoire')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (43,'ck','Cook Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (44,'cl','Chile')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (45,'cm','Cameroon')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (46,'cn','China')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (47,'co','Colombia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (48,'cr','Costa Rica')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (49,'cu','Cuba')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (50,'cv','Cap Verde')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (51,'cx','Christmas Island')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (52,'cy','Cyprus')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (53,'cz','Czeck Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (54,'de','Germany')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (55,'dj','Djibouti')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (56,'dk','Denmark')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (57,'dm','Dominica')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (58,'do','Dominican Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (59,'dz','Algeria')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (60,'ec','Ecuador')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (61,'ee','Estonia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (62,'eg','Egypt')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (63,'eh','Western Sahara')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (64,'er','Eritrea')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (65,'es','Spain')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (66,'et','Ethiopia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (67,'fi','Finland')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (68,'fj','Fiji')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (69,'fk','Falkland Islands (Malvina)')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (70,'fm','Micronesia, Federal State of')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (71,'fo','Faroe Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (72,'fr','France')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (73,'ga','Gabon')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (74,'gd','Grenada')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (75,'ge','Georgia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (76,'gf','French Guiana')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (77,'gg','Guernsey')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (78,'gh','Ghana')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (79,'gi','Gibraltar')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (80,'gl','Greenland')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (81,'gm','Gambia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (82,'gn','Guinea')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (83,'gp','Guadeloupe')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (84,'gq','Equatorial Guinea')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (85,'gr','Greece')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (86,'gs','South Georgia and the South Sandwich Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (87,'gt','Guatemala')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (88,'gu','Guam')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (89,'gw','Guinea-Bissau')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (90,'gy','Guyana')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (91,'hk','Hong Kong')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (92,'hm','Heard and McDonald Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (93,'hn','Honduras')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (94,'hr','Croatia/Hrvatska')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (95,'ht','Haiti')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (96,'hu','Hungary')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (97,'id','Indonesia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (98,'ie','Ireland')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (99,'il','Israel')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (100,'im','Isle of Man')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (101,'in','India')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (102,'io','British Indian Ocean Territory')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (103,'iq','Iraq')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (104,'ir','Iran (Islamic Republic of)')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (105,'is','Iceland')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (106,'it','Italy')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (107,'je','Jersey')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (108,'jm','Jamaica')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (109,'jo','Jordan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (110,'jp','Japan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (111,'ke','Kenya')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (112,'kg','Kyrgyzstan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (113,'kh','Cambodia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (114,'ki','Kiribati')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (115,'km','Comoros')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (116,'kn','Saint Kitts and Nevis')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (117,'kp','Korea, Democratic People\'s Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (118,'kr','Korea, Republic of')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (119,'kw','Kuwait')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (120,'ky','Cayman Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (121,'kz','Kazakhstan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (122,'la','Lao, People\'s Democratic Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (123,'lb','Lebanon')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (124,'lc','Saint Lucia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (125,'li','Liechtenstein')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (126,'lk','Sri Lanka')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (127,'lr','Liberia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (128,'ls','Lesotho')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (129,'lt','Lithuania')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (130,'lu','Luxembourg')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (131,'lv','Latvia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (132,'ly','Libyan Arab Jamahiriya')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (133,'ma','Morocco')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (134,'mc','Monaco')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (135,'md','Moldova, Republic of')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (136,'mg','Madagascar')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (137,'mh','Marshall Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (138,'mk','Macedonia, Former Yugoslav Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (139,'ml','Mali')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (140,'mm','Myanmar')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (141,'mn','Mongolia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (142,'mo','Macau')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (143,'mp','Northern Mariana Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (144,'mq','Martinique')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (145,'mr','Mauritania')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (146,'ms','Montserrat')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (147,'mt','Malta')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (148,'mu','Mauritius')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (149,'mv','Maldives')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (150,'mw','Malawi')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (151,'mx','Mexico')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (152,'my','Malaysia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (153,'mz','Mozambique')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (154,'na','Namibia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (155,'nc','New Caledonia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (156,'ne','Niger')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (157,'nf','Norfolk Island')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (158,'ng','Nigeria')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (159,'ni','Nicaragua')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (160,'nl','Netherlands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (161,'no','Norway')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (162,'np','Nepal')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (163,'nr','Nauru')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (164,'nu','Niue')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (165,'nz','New Zealand')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (166,'om','Oman')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (167,'pa','Panama')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (168,'pe','Peru')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (169,'pf','French Polynesia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (170,'pg','Papua New Guinea')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (171,'ph','Philippines')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (172,'pk','Pakistan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (173,'pl','Poland')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (174,'pm','St. Pierre and Miquelon')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (175,'pn','Pitcairn Island')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (176,'pr','Puerto Rico')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (177,'pt','Portugal')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (178,'pw','Palau')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (179,'py','Paraguay')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (180,'qa','Qatar')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (181,'re','Reunion Island')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (182,'ro','Romania')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (183,'ru','Russian Federation')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (184,'rw','Rwanda')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (185,'sa','Saudi Arabia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (186,'sb','Solomon Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (187,'sc','Seychelles')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (188,'sd','Sudan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (189,'se','Sweden')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (190,'sg','Singapore')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (191,'sh','St. Helena')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (192,'si','Slovenia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (193,'sj','Svalbard and Jan Mayen Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (194,'sk','Slovak Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (195,'sl','Sierra Leone')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (196,'sm','San Marino')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (197,'sn','Senegal')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (198,'so','Somalia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (199,'sr','Suriname')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (200,'st','Sao Tome and Principe')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (201,'sv','El Salvador')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (202,'sy','Syrian Arab Republic')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (203,'sz','Swaziland')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (204,'tc','Turks and Ciacos Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (205,'td','Chad')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (206,'tf','French Southern Territories')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (207,'tg','Togo')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (208,'th','Thailand')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (209,'tj','Tajikistan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (210,'tk','Tokelau')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (211,'tm','Turkmenistan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (212,'tn','Tunisia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (213,'to','Tonga')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (214,'tp','East Timor')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (215,'tr','Turkey')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (216,'tt','Trinidad and Tobago')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (217,'tv','Tuvalu')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (218,'tw','Taiwan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (219,'tz','Tanzania')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (220,'ua','Ukraine')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (221,'ug','Uganda')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (222,'uk','United Kingdom')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (223,'gb','United Kingdom')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (224,'um','US Minor Outlying Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (225,'us','United States')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (226,'uy','Uruguay')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (227,'uz','Uzbekistan')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (228,'va','Holy See (City Vatican State)')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (229,'vc','Saint Vincent and the Grenadines')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (230,'ve','Venezuela')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (231,'vg','Virgin Islands (British)')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (232,'vi','Virgin Islands (USA)')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (233,'vn','Vietnam')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (234,'vu','Vanuatu')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (235,'wf','Wallis and Futuna Islands')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (236,'ws','Western Samoa')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (237,'ye','Yemen')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (238,'yt','Mayotte')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (239,'yu','Yugoslavia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (240,'za','South Africa')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (241,'zm','Zambia')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (242,'zr','Zaire')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (243,'zw','Zimbabwe')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (244,'com','-')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (245,'net','-')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (246,'org','-')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (247,'edu','-')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (248,'int','-')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (249,'arpa','-')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (250,'at','Austria')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (251,'gov','Governement')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (252,'mil','Miltary')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (253,'su','Ex U.S.R.R.')",1);
$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix(v2_domains)." VALUES (254,'reverse','-')",1);
# --------------------------------------------------------

#
# Table structure for table 'v2_visitors'
#

$result = $xoopsDB->query("CREATE TABLE `".$xoopsDB->prefix(v2_visitors)."` (
  `AGENT` char(100) default NULL,
  `REFERER` char(200) default NULL,
  `ADDR` char(50) NOT NULL default '',
  `DATE` char(20) default NULL,
  `HOST` char(100) default NULL,
  `CODE` int(11) NOT NULL auto_increment,
  `REF_HOST` char(100) default NULL,
  PRIMARY KEY  (`CODE`),
  KEY `ADDR` (`ADDR`)
)",1);
if(!$result){
	for($i=0;$i<sizeof($madetables);$i++){
		$xoopsDB->query("DROP table ".$madetables[$i]."");
	}
	die ("<b>Unable to make ".$xoopsDB->prefix(v2_visitors)."</b>");
}else{
	array_push($madetables, $xoopsDB->prefix(v2_visitors));
}
#
# Dumping data for table 'v2_visitors'
#

	$output = "<p>";
	foreach($madetables as $table){
		$output .= "Table $table created<br />";
	}
	$output .= "</p>";
	echo "<div style='text-align:center'>\n";
	echo $output;
	echo "<br /><br /><b>Installation Complete</b><br />\n";
	echo "Click <a href=\"index.php\">here</a> to see the home page of your site.<br />\n";
	echo "Or click <a href='user.php'>here</a> to login and edit your general site settings.<br />\n";
	echo "If you had any errors, please contact the dev team at <a href=\"http://xoops.sourceforge.net/\">XOOPS.org</a>\n";
	echo "<br /><br /></div>\n";

}

function dropAll($tables){
	if($ok){
		for($i=0;$i<sizeof($tables);$i++){
			$xoopsDB->query("DROP table ".$tables[$i]."");
		}
	} else {
		echo "Do you really want to drop all tables?";
		echo "<a href=\"install.php?op=dropAll&ok=1\">Yes</a>&nbsp;";
	}
}



switch($op){

	case "siteInit":
		install_header();
		echo "<form action=\"install.php\" method=\"post\">\n";
		echo "<table align=\"center\" width=\"70%\">\n";
		echo "<tr><td colspan=\"2\">Please choose your site admin's name and password and click on the button below to initiate the creation of database tables.</td></tr>\n";
		echo "<tr><td align=\"right\"><b>Admin Name:</b></td><td><input type=\"text\" name=\"adminname\" /></td></tr>\n";
		echo "<tr><td align=\"right\"><b>Admin Email:</b></td><td><input type=\"text\" name=\"adminmail\" /></td></tr>\n";
		echo "<tr><td align=\"right\"><b>Admin Password:</b></td><td><input type=\"text\" name=\"adminpass\" /></td></tr>\n";
		echo "<tr><td align=\"center\" colspan=\"2\"><input type=\"hidden\" name=\"op\" value=\"createDB\">\n";
		echo "<input type=\"submit\" name=\"submit\" value=\"Create Tables\"></td></tr>\n";
		echo "</table></form>\n";
		install_footer();
		break;

	case "createDB":
		if ( !isset($adminname) || !isset($adminpass) || !isset($adminmail) || $adminmail == "" || $adminname =="" || $adminpass =="" ){
			install_header();
			echo "<div style='text-align:center'><b>Please go back and type in both admin name and admin password values.</b>\n";
			echo "<br /><br /><input type=\"button\" value=\"Back\" onclick=\"javascript:history.go(-1);\"></div>\n";
			install_footer();
			exit();
		}
		include_once("mainfile.php");
		include_once("include/config.php");
		include_once("class/module.textsanitizer.php");
		install_header();
		install_createDB($adminname, $adminpass);
		install_footer();
		break;

	case "drop":
		include_once("mainfile.php");
		include_once("include/config.php");
		install_header();
		for($i=0;$i<sizeof($myTables);$i++){
			$xoopsDB->query("DROP table ".$xoopsDB->prefix($myTables[$i])."",1);
		}
		echo "<br /><br /><b>Tables Dropped</b>";
		install_footer();
		break;
	case "initial":
		//include_once("mainfile.php");
		include_once("include/config.php");
		//include_once("class/module.textsanitizer.php");
		//include_once("class/xoopsuser.php");
		install_init();
		break;
	default:
		mainfile_settings();
}
?>