<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

include("mainfile.php");
header("Content-Type: text/plain");
$sql = "SELECT storyid, title, published FROM ".$xoopsDB->prefix("stories")." WHERE published>0 AND published<".time()." ORDER BY published DESC";
$result = $xoopsDB->query($sql,0,10,0);
    if ( !$result ) {
	echo "An error occured";
    } else {
        echo "<?xml version=\"1.0\" encoding=\""._CHARSET."\"?>\n";
	//echo "<!DOCTYPE rss PUBLIC \"-//Netscape Communications//DTD RSS 0.91//EN\"\n";
	//echo "           \"http://my.netscape.com/publish/formats/rss-0.91.dtd\">\n\n";
        echo "<rss version=\"0.92\">\n\n";
	echo "  <channel>\n";
        echo "       <title>".$xoopsConfig['sitename']."</title>\n";
        echo "       <link>".$xoopsConfig['xoops_url']."/</link>\n";
        echo "       <description>".$xoopsConfig['slogan']."</description>\n";
        echo "    <image>\n";
        echo "       <title>".$xoopsConfig['sitename']."</title>\n";
	echo "       <url>".$xoopsConfig['xoops_url']."/images/logo.gif</url>\n";
        echo "       <link>".$xoopsConfig['xoops_url']."/</link>\n";		
	echo "       <description>".$xoopsConfig['slogan']."</description>\n";
	echo "       <width>88</width>\n";
	echo "       <height>31</height>\n";
        echo "    </image>\n\n";
        for ($m=0; $m < $xoopsDB->num_rows($result); $m++) {
            list($storyid, $title, $time) = $xoopsDB->fetch_row($result);
            echo "    <item>\n";
            echo "       <title>".$title."</title>\n";
            echo "       <link>".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$storyid."</link>\n";
            echo "    </item>\n\n";
        }
	echo "  </channel>\n\n";
        echo "</rss>";
    }

?>