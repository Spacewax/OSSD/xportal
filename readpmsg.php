<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
/* =========================                                            */
/* Part of phpBB integration                                            */
/* Copyright (c) 2001 by                                                */
/*    Richard Tirtadji AKA King Richard (rtirtadji@hotmail.com)         */
/*    Hutdik Hermawan AKA hotFix (hutdik76@hotmail.com)                 */
/* http://www.phpnuke.web.id                                            */
/************************************************************************/

$xoopsOption['pagetype'] = "pmsg";
include_once("mainfile.php");
include_once("class/module.textsanitizer.php");
$myts = new MyTextSanitizer;

if ( !$xoopsUser ) {
	redirect_header("user.php",0);
	exit();
} else {
	if ( $delete ) {
		$sql = "DELETE FROM ".$xoopsDB->prefix("priv_msgs")." WHERE msg_id=$msg_id AND to_userid=".$xoopsUser->uid()."";
        if ( !$xoopsDB->query($sql,1) ) {
			exit();
		} else { 
			redirect_header("viewpmsg.php",1,_PM_DELETED);
			exit();
		}
	}

	include("header.php");
    $sql = "SELECT * FROM ".$xoopsDB->prefix("priv_msgs")." WHERE to_userid = ".$xoopsUser->uid()." ORDER BY msg_time";
    $resultID = $xoopsDB->query($sql,1,1,$start);
    if (!$resultID) {
		include("footer.php");
		exit();
    } else {
		$myrow = $xoopsDB->fetch_array($resultID);
        $sql = "UPDATE ".$xoopsDB->prefix("priv_msgs")." SET read_msg=1 WHERE msg_id=".$myrow['msg_id']."";
        $result = $xoopsDB->query($sql,1);
    }
	$myrow['subject'] = $myts->makeTboxData4Show($myrow['subject']);
	$myrow['msg_text'] = $myts->makeTareaData4Show($myrow['msg_text'],1,1,1);
	OpenTable();

	echo "<div style='text-align: center;'><h4>". _PM_PRIVATEMESSAGE."</h4></div><br /><a href='userinfo.php?uid=". $xoopsUser->uid() ."'>". _PM_PROFILE ."</a>&nbsp;<span style='font-weight:bold;'>&raquo;&raquo;</span>&nbsp;<a href='viewpmsg.php'>". _PM_INBOX ."</a>&nbsp;<span style='font-weight:bold;'>&raquo;&raquo;</span>&nbsp;". $myrow['subject'] ."<br /><table border='0' cellpadding='1' cellpadding='0' valign='top' width='100%'><tr><td><table border='0' cellpadding='3' cellpadding='1' width='100%'><tr bgcolor='". $xoopsTheme['bgcolor2'] ."' align='left'><td width='20%' colspan='2' align='center'><span style='color: ". $xoopsTheme['textcolor2'] .";'><b>". _PM_FROM ."</b></span></td></tr>";

	if ( !$xoopsDB->num_rows($resultID) ) {
		echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."' colspan='2' align='center'>"._PM_YOUDONTHAVE."</td></tr>";
	} else {
		echo "<tr bgcolor='".$xoopsTheme['bgcolor3']."' align='left'>\n";
		$poster = new XoopsUser($myrow['from_userid']);
		if ( !$poster->is_active() ) {
			$poster = 0;
		}
		echo "<td valign='top'>";
		if ( $poster ) { // we need to do this for deleted users
		    echo "<b>".$poster->uname()."</b><br />\n";
			if ( $poster->user_avatar() != "" ) {
				echo "<img src='images/avatar/".$poster->user_avatar()."' alt='' /><br />\n";
			}
			if ( $poster->user_from() != "" ) {
				echo _PM_FROMC."".$poster->user_from()."<br /><br />\n";
			}
		} else {
			echo $xoopsConfig['anonymoud']; // we need to do this for deleted users
		}
		echo "</td><td><img src='images/subject/".$myrow['msg_image']."' alt='' />&nbsp;"._PM_SENTC."".formatTimestamp($myrow['msg_time'])."&nbsp;&nbsp;&nbsp";
		echo "<hr /><b>".$myrow['subject']."</b><br /><br />\n";
		echo $myrow['msg_text'] . "<br /><br />";
        echo "<hr />\n";
		if ( $poster ) {
			echo "&nbsp;&nbsp<a href='userinfo.php?uid=".$poster->uid()."'><img src='images/icons/profile.gif' alt='"._PM_PROFILE."' /></a>\n";
			if ( $poster->user_viewemail() ) {
				echo "&nbsp;<a href='mailto:".$poster->email()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/email.gif' alt='";
				printf(_PM_SENDEMAIL,$poster->uname());
				echo "' /></a>\n";
			}
			$posterurl = $poster->url();
            if ( $posterurl != "" ) {
				if ( strstr("http://", $posterurl) ) {
					$posterurl = "http://" . $posterurl;
				}
				echo "&nbsp;&nbsp;<a href='".$posterurl."'><img src='images/icons/www.gif' alt='www' /></a>\n";
			}
            if ( $poster->user_icq() != "" ) {
				//echo "&nbsp;<a href=\"http://wwp.icq.com/".$poster->user_icq()."#pager\" target=\"_blank\"><img src=\"http://online.mirabilis.com/scripts/online.dll?icq=".$poster->user_icq()."&img=5\" border=\"0\" alt=\"icq\" /></a>";
				echo "&nbsp;<a href='http://wwp.icq.com/scripts/search.dll?to=".$poster->user_icq()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/icq_add.gif' alt='"._PM_ADDTOLIST."' /></a>\n";
			}
			if ( $poster->user_aim() != "" ) {
				echo "&nbsp;<a href='aim:goim?screenname=".$poster->user_aim()."&amp;message=Hi+".$poster->user_aim()."+Are+you+there?'><img src='".$xoopsConfig['xoops_url']."/images/icons/aim.gif' alt='aim' /></a>\n";
			}
			if ( $poster->user_yim() != "" ) {
				echo "&nbsp;<a href='http://edit.yahoo.com/config/send_webmesg?.target=".$poster->user_yim()."&.src=pg'><img src='".$xoopsConfig['xoops_url']."/images/icons/yim.gif' alt='yim' /></a>\n";
			}
			if ( $poster->user_msnm() != "" ) {
				echo "&nbsp;<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$poster->uid()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/msnm.gif' alt='msnm' /></a>\n";
			}
		} else {
			echo "&nbsp;";
		}
        echo "</td></tr>";
		echo "<tr bgcolor='".$xoopsTheme['bgcolor1']."' align='right'><td width='20%' colspan='2' align='right' style='color: ".$xoopsTheme['textcolor2'].";'>";
		$previous = $start-1;
        $next = $start+1;

        if ( $previous >= 0 ) { 
			echo "<a href='readpmsg.php?start=".$previous."&amp;total_messages=".$total_messages."'>"._PM_PREVIOUS."</a> | ";
		} else {
			echo _PM_PREVIOUS." | ";
		}
		if ( $next < $total_messages ) {
			echo "<a href='readpmsg.php?start=".$next."&amp;total_messages=".$total_messages."'>"._PM_NEXT."</a>";
		} else {
			echo ""._PM_NEXT."";
		}

		echo "</td></tr>";
        echo "<tr bgcolor='".$xoopsTheme['bgcolor2']."' align='left'><td width='20%' colspan='2' align='left'>";
		// we dont want to reply to a deleted user!
		if ( $poster ) {
			echo "<input type='button' value='"._PM_REPLY."' onclick=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?reply=1&amp;msg_id=".$myrow['msg_id']."&amp;theme=".$xoopsTheme['thename']."','pmlite',360,290);\" />\n";
			echo "&nbsp;";
		}
		echo "<input type='button' value='"._PM_DELETE."' onclick=\"location='".$xoopsConfig['xoops_url']."/readpmsg.php?delete=1&amp;msg_id=".$myrow['msg_id']."'\" />\n";
		echo "</td></tr>";
	}

	echo "</table></td></tr></table>";
	CloseTable();
	include("footer.php");
}
?>