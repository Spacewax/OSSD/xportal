<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
/* =========================                                            */
/* Part of phpBB integration                                            */
/* Copyright (c) 2001 by                                                */
/*    Richard Tirtadji AKA King Richard (rtirtadji@hotmail.com)         */
/*    Hutdik Hermawan AKA hotFix (hutdik76@hotmail.com)                 */
/* http://www.phpnuke.web.id                                            */
/************************************************************************/

$xoopsOption['pagetype'] = "pmsg";
include_once("mainfile.php");
include_once("class/module.textsanitizer.php");
$eh = new MyTextSanitizer; //ErrorHandler object

if ( !$xoopsUser ) {
	$errormessage = _PM_SORRY."<br />"._PM_PLZREG."";
    	redirect_header("user.php",2,$errormessage);
} else {
	if ( isset($delete_messages) && $delete_messages.x && $delete_messages.y) {
		$status = 0;
		$size = sizeof($msg_id);
        	for ( $i=0;$i<$size;$i++ ) {
			$sql = "DELETE FROM ".$xoopsDB->prefix("priv_msgs")." WHERE msg_id=".$msg_id[$i]." AND to_userid=".$xoopsUser->uid()."";
        		if ( !$xoopsDB->query($sql,1) ) {
				exit();
        		} else {
				$status = 1;
			}
		}
		if ( $status ) {
			redirect_header("viewpmsg.php",1,_PM_DELETED);
			exit();
		}
	}
	include("header.php");
    	$sql = "SELECT * FROM ".$xoopsDB->prefix("priv_msgs")." WHERE (to_userid = ".$xoopsUser->uid().") ORDER BY msg_time";
    	$resultID = $xoopsDB->query($sql,1);
    	if ( !$resultID ) {
		include("footer.php");
		exit();
	}
	OpenTable(); 
	echo "<h4 style='text-align:center;'>". _PM_PRIVATEMESSAGE ."</h4><br /><a href='userinfo.php?uid=". $xoopsUser->uid()."'>". _PM_PROFILE ."</a>&nbsp;<span style='font-weight:bold;'>&raquo;&raquo;</span>&nbsp;". _PM_INBOX ."<br /><br /><table border='0' cellspacing='1' cellpadding='0' align='center' valign='top' width='100%'><tr><td><table border='0' cellspacing='1' cellpadding='1' width='100%'>";
	echo "<form name='prvmsg' method='get' action='viewpmsg.php'>"; 
	echo "<tr bgcolor='". $xoopsTheme['bgcolor2'] ."' align='left'><td bgcolor='". $xoopsTheme['bgcolor2'] ."' align='center' valign='middle'><input name='allbox' onclick='CheckAll();' type='checkbox' value='Check All' /></td><td bgcolor='". $xoopsTheme['bgcolor2'] ."' align='center' valign='middle'><img src='images/download.gif' alt='' border='0' /></td><td bgcolor='". $xoopsTheme['bgcolor2'] ."' align='center' valign='middle'>&nbsp;</td><td><b>". _PM_FROM ."</b></td><td align='center'><b>". _PM_SUBJECT ."</b></td><td align='center'><b>". _PM_DATE ."</b></td></tr>";
	if ( !$total_messages = $xoopsDB->num_rows($resultID) ) {
		echo "<td bgcolor='".$xoopsTheme['bgcolor3']."' colspan='6' align='center'>"._PM_YOUDONTHAVE."</td></tr> ";
	} else {
		$display=1;
	}

	$count=0;
    	while ( $myrow = $xoopsDB->fetch_array($resultID) ) {
		echo "<tr align='left'><td bgcolor='".$xoopsTheme['bgcolor1']."' valign='top' width='2%' align='center'><input type='checkbox' onclick='CheckCheckAll();' name='msg_id[]' value='".$myrow['msg_id']."' /></TD>\n";
		if ( $myrow['read_msg'] == "1" ) {
			echo "<td valign='top' width='5%' align='center' bgcolor='".$xoopsTheme['bgcolor1']."'>&nbsp;</td>\n";
		} else {
			echo "<td valign='top' width='5%' align='center' bgcolor='".$xoopsTheme['bgcolor1']."'><img src='images/read.gif' alt='"._PM_NOTREAD."' border='0' /></td>\n";
		}
		echo "<td bgcolor='".$xoopsTheme['bgcolor3']."' valign='top' width='5%' align='center'><img src='images/subject/".$myrow['msg_image']."' alt='' border='0' /></td>\n";
		$postername = XoopsUser::get_uname_from_id($myrow['from_userid']);
		echo "<td bgcolor='".$xoopsTheme['bgcolor1']."' valign='middle' width='10%'>";
		// no need to show deleted users
		if ( $postername ) {
			echo "<a href='userinfo.php?uid=".$myrow['from_userid']."'>".$postername."</a>";
		} else {
			echo "&nbsp;";
		}
		echo "</td>\n";
		echo "<td bgcolor='".$xoopsTheme['bgcolor3']."' valign='middle'><a href='readpmsg.php?start=$count&amp;total_messages=$total_messages'>".$myrow['subject']."</a></td>";
		echo "<td bgcolor='".$xoopsTheme['bgcolor1']."' valign='middle' align='center' width='20%'>".formatTimestamp($myrow['msg_time'])."</td></tr>";
		$count++;
	}

	if ( $display ) {
		echo "<tr bgcolor='".$xoopsTheme['bgcolor2']."' align='left'><td colspan='6' align='left'><input type='button' onclick=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?send=1&theme=".$xoopsTheme['thename']."','pmlite',350,280);\" value='"._PM_SEND."' />&nbsp;<input type='submit' name='delete_messages' value='"._PM_DELETE."' border='0' /></td></tr><input type='hidden' name='total_messages' value='$total_messages'></form>";
	} else {
		echo "<tr bgcolor='".$xoopsTheme['bgcolor2']."' align='left'><td colspan='6' align='left'><input type='button' onclick=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?send=1&theme=".$xoopsTheme['thename']."','pmlite',360,290);\" value='"._PM_SEND."' /></td></tr></form>";
	}

	echo "</table></td></tr></table>";
	CloseTable();
	?>
	<script language='javascript'>
		<!--
		function CheckAll() {
		    for (var i=0;i<document.prvmsg.elements.length;i++) {
				var e = document.prvmsg.elements[i];
				if ((e.name != 'allbox') && (e.type=='checkbox')) {
					e.checked = document.prvmsg.allbox.checked;
				}
			}
		}

		function CheckCheckAll() {
			var TotalBoxes = 0;
			var TotalOn = 0;
			for (var i=0;i<document.prvmsg.elements.length;i++) {
				var e = document.prvmsg.elements[i];
				if ((e.name != 'allbox') && (e.type=='checkbox')) {
					TotalBoxes++;
					if (e.checked) {
						TotalOn++;
					}
				}
			}
			if (TotalBoxes==TotalOn) {
				document.prvmsg.allbox.checked=true;
			} else {
				document.prvmsg.allbox.checked=false;
			}
		}
	//-->
	</script>

	<?php
	include("footer.php");
}
?>