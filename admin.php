<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

$xoopsOption['pagetype'] = "admin";
include("mainfile.php");
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");

/*********************************************************/
/* Admin Authentication                                  */
/*********************************************************/
$admintest = 0;

if ( $xoopsUser ) {
	if ( !$xoopsUser->is_admin() ) {
		redirect_header("index.php",2,_AD_NORIGHT);
		exit();
	}
	$admintest=1;
} else {
	redirect_header("index.php",2,_AD_NORIGHT);
	exit();
}

if ( $admintest ) {
	adminMain();
}

function admin_menu_item($module) {
	global $xoopsConfig;

	if ($xoopsConfig['admingraphic']) {
		echo "<a href='".$xoopsConfig['xoops_url']."/modules/".$module->dirname()."/".$module->adminpath()."'><img src='".$xoopsConfig['xoops_url']."/modules/".$module->dirname()."/".$module->image()."' alt='".$module->name()."' /><br /><b>" .trim($module->name())."</b></a>";
	} else {
		echo "<a href='".$xoopsConfig['xoops_url']."/modules/".$module->dirname()."/".$module->adminpath()."'><b>" .trim($module->name())."</b></a>";
	}
}

function adminMain() {
	global $xoopsDB;
    	global $xoopsConfig, $xoopsTheme, $xoopsUser;
    	include ("header.php");
	OpenTable();
	echo "<div style='text-align: center;'><h4>"._ADMINMENU."</h4></div><br /><table border='0' cellpadding='1' align='center'><tr>\n";
	$counter = 0;
	$sql = "SELECT * FROM ".$xoopsDB->prefix("modules")." WHERE hasadmin=1 ORDER BY mid ASC";
	$result = $xoopsDB->query($sql);
	while ( $myrow = $xoopsDB->fetch_array($result) ) {
		$module = new XoopsModule($myrow);
		if ( $module->hasAdmin() && $xoopsUser->is_admin($module->mid()) ) {
			echo "<td align='center' valign='bottom' width='24%'>\n";
			admin_menu_item($module);
			echo "</td>\n";
			$counter++;
			if ( $counter > 4 ) {
				$counter = 0;
				echo "</tr>";
				echo "<tr>";
			}
		}
		unset($module);
	}
	echo "</tr>";
	echo "</table>";
	CloseTable();
	echo "<br />";
	OpenTable();
	$sql = "SELECT * FROM ".$xoopsDB->prefix("users")." WHERE level>0 ORDER BY uid DESC";
	$result = $xoopsDB->query($sql,1,10,0);
	echo "<div style='text-align: center;'><h4>"._AD_LASTTENUSERS."</h4>";
	echo "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'><table width='100%' border='0' cellpadding='2' cellspacing='1'>
	<tr bgcolor='".$xoopsTheme['bgcolor3']."'><td align='center'><b>"._AD_NICKNAME."</b></td><td align='center'><b>"._AD_EMAIL."</b></td><td align='center'><b>"._AD_AVATAR."</b></td><td align='center'><b>"._AD_REGISTERED."</b></td><td align='center'><b>"._AD_ACTION."</b></td></tr>\n";
	while ( $myrow = $xoopsDB->fetch_array($result) ) {
		$myuser = new XoopsUser($myrow);
		echo "<tr bgcolor='".$xoopsTheme['bgcolor1']."'><td><a href='userinfo.php?uid=".$myuser->uid()."'>".$myuser->uname()."</a></td><td><a href='mailto:".$myuser->email()."'>".$myuser->email()."</a></td><td align='center'><img src='images/avatar/".$myuser->user_avatar()."' alt='' /></td><td align='center'>".formatTimestamp($myuser->user_regdate())."</td><td align='right'><a href='modules/system/admin.php?fct=users&amp;op=modifyUser&amp;chng_uid=".$myuser->uid()."'>"._AD_EDIT."</a>&nbsp;<a href='modules/system/admin.php?fct=users&amp;op=delUser&amp;chng_uid=".$myuser->uid()."'>"._AD_DELETE."</a></td></tr>\n";
	}
	echo "</table></td></tr></table></div>\n";
	CloseTable();
	echo "<br />";
	OpenTable();
	$sql = "SELECT uid FROM ".$xoopsDB->prefix("users")." WHERE user_regdate>0 AND level=0 ORDER BY uid DESC";
	$result = $xoopsDB->query($sql);
	echo "<div style='text-align: center;'><h4>"._AD_NOTACTVUSERS."</h4>";
	echo "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>";
	echo "<table width='100%' border='0' cellpadding='2' cellspacing='1'>\n";
	echo "<tr bgcolor='".$xoopsTheme['bgcolor3']."'><td align='center'><b>"._AD_NICKNAME."</b></td><td align='center'><b>"._AD_EMAIL."</b></td><td align='center'><b>"._AD_AVATAR."</b></td><td align='center'><b>"._AD_REGISTERED."</b></td><td align='center'><b>"._AD_ACTION."</b></td></tr>\n";
	while ( list($uid) = $xoopsDB->fetch_row($result) ) {
		if ( $uid != 0 ) {
			$myuser = new XoopsUser($uid);
			echo "<tr bgcolor='".$xoopsTheme['bgcolor1']."'><td><a href='userinfo.php?uid=".$myuser->uid()."'>".$myuser->uname()."</a></td><td><a href='mailto:".$myuser->email()."'>".$myuser->email()."</a></td><td align='center'><img src='images/avatar/".$myuser->user_avatar()."' alt='' /></td><td align='center'>".formatTimestamp($myuser->user_regdate())."</td><td align='right'><a href='modules/system/admin.php?fct=users&amp;op=delUser&amp;chng_uid=".$myuser->uid()."'>"._AD_DELETE."</a></td></tr>\n";
		}
	}
	echo "</table></td></tr></table></div>\n";
	CloseTable();
/*	echo "<br />";
	OpenTable();
	$lastvisit = time()- 8640000;
	$sql = "SELECT l.uid, u.uname, u.email, u.user_avatar, u.user_regdate FROM ".$xoopsDB->prefix("lastseen")." l LEFT JOIN ".$xoopsDB->prefix("users")." u ON u.uid=l.uid WHERE l.time < ".$lastvisit." AND u.level=1 ORDER BY uid DESC";
	$result = $xoopsDB->query($sql);
	echo "<div align='center'><h4>"._AD_NOTSEEN100D."</h4>";
	echo "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>";
	echo "<table width='100%' border='0' cellpadding='2' cellspacing='1'>\n";
	echo "<tr bgcolor='".$xoopsTheme['bgcolor3']."'><td align='center'><b>"._AD_NICKNAME."</b></td><td align='center'><b>"._AD_EMAIL."</b></td><td align='center'><b>"._AD_AVATAR."</b></td><td align='center'><b>"._AD_REGISTERED."</b></td><td align='center'><b>"._AD_ACTION."</b></td></tr>\n";
	while(list($uid, $uname, $email, $avatar, $regdate) = $xoopsDB->fetch_row($result)){
		if($uid != 0){
			echo "<tr bgcolor='".$xoopsTheme['bgcolor1']."'><td><a href='userinfo.php?uid=".$uid."'>".$uname."</a></td><td><a href='mailto:".$email."'>".$email."</a></td><td align='center'><img src='images/avatar/".$avatar."' /></td><td align='center'>".formatTimestamp($regdate)."</td><td align='right'><a href='admin.php?fct=users&op=delUser&chng_uid=".$uid."'>"._AD_DELETE."</a></td></tr>\n";
		}
	}
	echo "</table></td></tr></table></div>\n";
	CloseTable();*/
    	include ("footer.php");
}
?>