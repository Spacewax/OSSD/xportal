<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
$xoopsOption['pagetype'] = "user";
if(!isset($mainfile)) { include("mainfile.php"); }

function main() {
	global $xoopsUser, $HTTP_COOKIE_VARS, $xoopsConfig;
	if ( !$xoopsUser ) {
		include("header.php");
		OpenTable();
		echo "<form action='user.php' method='post'><b>"._US_USERLOGIN."</b><br /><br />". _US_NICKNAMECOLON."<input class='textbox' type='text' name='uname' size='26' maxlength='25'";
		if ( $HTTP_COOKIE_VARS[$xoopsConfig['usercookie']] ) {
			echo " value='".$HTTP_COOKIE_VARS[$xoopsConfig['usercookie']]."'";
		}
		echo " /><br />". _US_PASSWORDCOLON."<input class='textbox' type='password' name='pass' size='21' maxlength='20' /><br /><input type='hidden' name='op' value='login' /><input type='submit' value='"._US_LOGIN."' />";
		CloseTable();
		echo "</form><br /><a name='lost'></a><p><b>". _US_NOTREGISTERED ."</b></p>";
		OpenTable();
		echo "<b>"._US_LOSTPASSWORD."</b><br /><br />"._US_NOPROBLEM."<br /><form action='lostpass.php' method='post'>".  _US_YOUREMAIL ."<input class='textbox' type='text' name='email' size='26' maxlength='60' />&nbsp;&nbsp<input type='hidden' name='op' value='mailpasswd' /><input type='submit' value='"._US_SENDPASSWORD."' />";
		CloseTable();
		echo "</form>";
		include("footer.php");
	} elseif ( $xoopsUser ) {
		redirect_header("userinfo.php?uid=".$xoopsUser->uid()."",1,_US_TAKINGYOU);
		exit();
	}
}

function logout() {
	global $xoopsConfig, $xoopsUser;
	if ( $xoopsUser ) {
		$xoopsUser->logout();
	}
	$message = _US_LOGGEDOUT;
	$message .= "<br />"._US_THANKYOUFORVISIT."<br />";
    	redirect_header("index.php",1,$message);
	exit();
}

function login($uname, $pass) {
	global $xoopsDB;
	$user = new XoopsUser();
	$uname = trim($uname);
	$pass = trim($pass);
	if ( !isset($uname) || !isset($pass) || $uname == "" || $pass == "" ) {
		redirect_header("user.php",1,_US_INCORRECTLOGIN);
		exit();
	}
	$uid = $user->login($uname, $pass);
	if ( $uid ){
		redirect_header("userinfo.php?uid=".$uid."",1,sprintf(_US_HELLOTAKINGYOU,$uname));
		exit();
	} else {
		if ( !$user->is_active() ) {
			redirect_header("index.php",5,_US_NOACTTPADM);
			exit();
		}
		redirect_header("user.php",1,_US_INCORRECTLOGIN);
		exit();
	}
}

function activate($id,$key) {
	global $xoopsDB;
	$thisuser = new XoopsUser($id);
	if ( $thisuser->actkey() != $key ) {
		redirect_header("index.php",5,_US_ACTKEYNOT);
		exit();
	} else {
		if ( $thisuser->is_active() ) {
			redirect_header("user.php",5,_US_ACONTACT);
			exit();
		} else {
			if ($thisuser->activate()) {
				redirect_header("user.php",5,_US_ACTLOGIN);
				exit();
			} else {
				redirect_header("index.php",5,"Activation failed!");
				exit();
			}
		}
	}
}

function delete($ok=0) {
	global $xoopsUser, $xoopsConfig;
	if ( !$xoopsUser || !$xoopsConfig['self_delete'] ) {
		redirect_header("index.php",5,_US_NOPERMISS);
		exit();
	} else {
		if ( !$ok ) {
			include("header.php");
			echo "<div style='text-align: center;'><h4>"._US_SURETODEL."</h4>". _US_REMOVEINFO."<br /><br />[ <a href='user.php?op=delete&amp;ok=1'>"._US_YES."</a> | <a href='user.php'>"._US_NO."</a> ]</div>";
			include("footer.php");
			exit();
		} else {
			$xoopsUser->delete();
			redirect_header("index.php",5,_US_BEENDELED);
			exit();
		}
	}
}

switch($op) {
	case "logout":
		logout();
		break;
	case "login":
		login($uname, $pass);
		break;
	case "actv":
		activate($id, $key);
		break;
	case "delete":
		delete($ok);
		break;
	default:
		main();
		break;
}
?>