<?php
//%%%%%%		File Name user.php 		%%%%%
define("_US_USERLOGIN","User Login");
define("_US_NICKNAMECOLON","Nickname: ");
define("_US_PASSWORDCOLON","Password: ");
define("_US_LOGIN","Login");
define("_US_NOTREGISTERED","Not registered?  Click <a href=register.php>here</a>.");
define("_US_LOSTPASSWORD","Lost your Password?");
define("_US_NOPROBLEM","No problem. Simply enter the e-mail address we have on file for your account.");
define("_US_YOUREMAIL","Your Email: ");
define("_US_SENDPASSWORD","Send Password");
define("_US_TAKINGYOU","Taking you to your profile page...");
define("_US_LOGGEDOUT","You are now logged out");
define("_US_THANKYOUFORVISIT","Thank you for your visit to our site!");
define("_US_HELLOTAKINGYOU","Hello %s, taking you to your profile page...");
define("_US_PWDNG","Password Not Correct! Please try again!");
define("_US_INCORRECTLOGIN","Incorrect Login!");

// 2001-11-17 ADD
define("_US_NOACTTPADM","The selected user has been deactivated or has not been activated yet.<br>Please contact the administrator for details.");
define("_US_ACTKEYNOT","Activation key not correct!");
define("_US_ACONTACT","Selected account is already activated!");
define("_US_ACTLOGIN","Your account has been activated. Please login with the registered password.");
define("_US_NOPERMISS","Sorry, you dont have the permission to perform this action!");
define("_US_SURETODEL","Are you sure you want to delete your account?");
define("_US_REMOVEINFO","This will remove all your info from our database.");
define("_US_BEENDELED","Your account has been deleted.");
//

//%%%%%%		File Name register.php 		%%%%%
define("_US_USERREG","User Registration");
define("_US_NICKNAME","Nickname");
define("_US_EMAIL","Email");
define("_US_OPTION","Option");
define("_US_ALLOWVIEWEMAIL","Allow other users to view my email address");
define("_US_WEBSITE","Website");
define("_US_TIMEZONE","Time Zone");
define("_US_AVATAR","Avatar");
define("_US_LIST","List");
define("_US_VERIFYPASS","Verify Password");
define("_US_SUBMIT","Submit");
define("_US_COOKIEBASED","Notice: Account preferences are cookie based.");
define("_US_ASAREGUSER","As a registered user you can:");
define("_US_COMMENTSWITHNAME","Post comments with your name");
define("_US_NEWSWITHNAME","Send news with your name");
define("_US_CUSTOMIZECOMMENTS","Customize the comments");
define("_US_SELECTTHEMES","Select different themes");
define("_US_SOMEOTHER","some other cool stuff...");
define("_US_REGISTERNOW","Register Now! It's Free!");
define("_US_WEDONTSELL","We don't sell/give to others your personal info.");
define("_US_USERNAME","Username");
define("_US_FINISH","Finish");
define("_US_REGISTERNG","Could not register new user.");
define("_US_WELCOME","Welcome to %s!");
define("_US_UORSOMEONE","You or someone else has used your email account (%s) to register an account at the following site:");
define("_US_CLICKTOACTV","Click on the link below to activate your user account:");

// %s is username. This is a subject for email
define("_US_USERKEYFOR","User activateion key for %s");

define("_US_YOURREGISTERED","You are now registered. Email containing an user activation key has been sent to the email account you provided. Please follow the instruction on the mail to activate your acount. ");


// %s is your site name
define("_US_NEWUSERREGAT","New user registration at %s");
// %s is a username
define("_US_HASJUSTREG","%s has just registered!");

define("_US_INVALIDMAIL","ERROR: Invalid email");
define("_US_EMAILNOSPACES","ERROR: Email addresses do not contain spaces.");
define("_US_INVALIDNICKNAME","ERROR: Invalid Nickname");
define("_US_NICKNAMETOOLONG","Nickname is too long. It must be less than 25 characters.");
define("_US_NAMERESERVED","ERROR: Name is reserved.");
define("_US_NICKNAMENOSPACES","There cannot be any spaces in the Nickname.");
define("_US_NICKNAMETAKEN","ERROR: Nickname taken.");
define("_US_EMAILTAKEN","ERROR: Email address already registered.");
define("_US_ENTERPWD","ERROR: You must provide a password.");
define("_US_SORRYNOTFOUND","Sorry, no corresponding user info was found.");
define("_US_HELLO","Hello %s,");

// %s is a host address
define("_US_AUSERREQUESTED","A web user from %s has just requested that password be sent.");

// %s is your site name
define("_US_YOURDETAILS","Here are your login details at %s");
define("_US_YOUCANCHANGE","You can change it after you login at %s");
define("_US_SITETEAM","%s team");
define("_US_NEWPWDREQ","New Password Request at %s");

define("_US_NEWPASSWORD","New Password: ");
define("_US_DONTWORRYNOTTHEM","If you didn't ask for this, don't worry. You are seeing this message, not 'them'. If this was an error, we are really sorry but please login with your new password.");
define("_US_MAILPWDNG","mail_password: could not update user entry. Contact the Administrator");

// %s is a username
define("_US_PWDMAILED","Password for %s mailed.");
define("_US_CONFMAIL","Confirmation Mail for %s mailed.");

define("_US_GETNEWPASS","You can get your new password by clicking on the link below:");
define("_US_DONTWORRY","If you didn't ask for this, don't worry. Just delete this Email.");

//%%%%%%		File Name userinfo.php 		%%%%%
define("_US_SELECTNG","No User Selected! Please go back and try again.");
define("_US_THISISYOURPAGE","This is your personal page");
define("_US_PROFILEFOR","Profile for %s");
define("_US_PM","PM");
define("_US_ICQ","ICQ");
define("_US_AIM","AIM");
define("_US_YIM","YIM");
define("_US_MSNM","MSNM");
define("_US_LOCATION","Location");
define("_US_OCCUPATION","Occupation");
define("_US_INTEREST","Interest");
define("_US_SIGNATURE","Signature");
define("_US_EXTRAINFO","Extra Info");
define("_US_NOINFO","There is no available user info for user id %s");
define("_US_EDITPROFILE","Edit Profile");
define("_US_LOGOUT","Logout");
define("_US_INBOX","Inbox");
define("_US_MEMBERSINCE","Member Since");
define("_US_RANK","Rank");
define("_US_POSTS","Comments/Posts");
define("_US_NEWS","Submitted News");
define("_US_LASTVISIT","Last Visit");
define("_US_ALLABOUT","All about %s");
define("_US_STATISTICS","Statistics");
define("_US_NODATA","No Data");

//%%%%%%		File Name edituser.php 		%%%%%
define("_US_PROBLEMOCURRED","A Problem Ocurred. You don't have the right to edit this user or you're not logged in.");
define("_US_PROFILE","Profile");
define("_US_REALNAME","Real Name");
define("_US_THISWILLBEPUBLIC","(This Email will not be public but is required, will be used to send your password if you lost it.)");
define("_US_OPTIONAL","(optional)");
define("_US_REQUIRED","(required)");
define("_US_THEME","Theme");
define("_US_TYPEYOURSIG","(Type your signature with HTML coding)");
define("_US_SHOWSIG","Always attach my signature");
define("_US_CDISPLAYMODE","Comments Display Mode");
define("_US_NOCOMMENTS","No Comments");
define("_US_NESTED","Nested");
define("_US_FLAT","Flat");
define("_US_THREAD","Thread");
define("_US_CSORTORDER","Comments Sort Order");
define("_US_OLDESTFIRST","Oldest First");
define("_US_NEWESTFIRST","Newest First");
define("_US_255CHARMAX","(255 characters max. Type what others can know about yourself)");
define("_US_PASSWORD","Password");
define("_US_TYPEPASSTWICE","(type a new password twice to change it)");
define("_US_SAVECHANGES","Save Changes");
define("_US_NOEDITRIGHT","Sorry, you don't have the right to edit this user's info.");
define("_US_PASSNOTSAME","Both passwords are different. They must be identical.");
define("_US_PWDTOOSHORT","Sorry, your password must be at least <b>%s</b> characters long.");
define("_US_USERQUERYNG","A problem occurred while querying user tables.");
define("_US_PROFUPDATED","Your Profile Updated!");
define("_US_USECOOKIE","Store my user name in a cookie for 1 year");
define("_US_YES","Yes");
define("_US_NO","No");
define("_US_DELACCOUNT","Delete Account");
?>