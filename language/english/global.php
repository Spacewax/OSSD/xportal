<?php
//%%%%%%	File Name mainfile.php 	%%%%%
define("_PLEASEWAIT","Please Wait");
define("_FETCHING","Loading...");
define("_TAKINGBACK","Taking you back to where you were....");
define("_YOURNAME","Your Name");
define("_LOGOUT","Logout");
define("_SUBJECT","Subject");
define("_MESSAGEICON","Message Icon");
define("_COMMENT","Comment");
define("_CHKMSGLENGTH","[check message length]");
define("_ALLOWEDHTML","Allowed HTML:");
define("_POSTANON","Post Anonymously");
define("_DISABLESMILEY","Disable Smiley");
define("_DISABLEHTML","Disable html");
define("_PREVIEW","Preview");
define("_POSTCOMMENT","Post Comment");
define("_GO","Go!");
define("_NOCOMMENTS","No Comments");
define("_FLAT","Flat");
define("_THREAD","Thread");
define("_OLDESTFIRST","Oldest First");
define("_NEWESTFIRST","Newest First");
define("_REFRESH","Refresh");
define("_MORE","more...");
define("_IFNOTRELOAD","If the page does not automatically reload, please click <a href=%s>here</a>");

//%%%%%%	File Name themeuserpost.php 	%%%%%
define("_POSTER","Poster");
define("_JOINED","Joined: ");
define("_POSTS","Posts: ");
define("_FROM","From: ");
define("_POSTED","Posted: "); // Posted date
define("_PROFILE","Profile");
define("_VISITWEBSITE","Visit Website");
define("_SENDPMTO","Send Private Message to %s");
define("_SENDEMAILTO","Send Email to %s");
define("_ADDTOLIST","Add to Contact List");
define("_ADD","Add");
define("_REPLY","Reply");
define("_EDITTHISPOST","Edit this post");
define("_DELETETHISPOST","Delete this post");
define("_POSTEDBY","Posted by");

//%%%%%%	File Name header.php 	%%%%%
define("_PLZCOMPLETE","Please complete the subject and message fields.");
define("_MESSAGETOOLONG","Your message is too long.");
define("_REDUCEMESSAGE","Reduce your message to 10000 characters.");
define("_MAXIMUMLENGTH","The maximum permitted length is 10000 characters.");
define("_ITISCHARSLONG","It is currently %s characters long.");

//%%%%%%	File Name admin_functions.php 	%%%%%
define("_ADMINMENU","Administration Menu");
define("_MAIN","Main");
define("_MANUAL","Manual");
define("_INFO","Info");

//%%%%%%	File Name misc.php (who's-online popup)	%%%%%
define("_WHOSONLINE","Who's Online"); 
define("_NOUSRONLINE","No users are currently online"); 
define("_CLOSE","Close");  // Close window

//%%%%%%	File Name misc.php (smiles popup)	%%%%%
define("_SMILIES","Smilies");
define("_CLICKASMILIE","Click a smilie to insert it into your message.");
define("_CODE","Code");
define("_EMOTION","Emotion");
define("_IMAGE","Image");

//%%%%%%	File Name misc.php (send-to-friend popup)	%%%%%
define("_YOURNAMEC","Your Name: ");
define("_YOUREMAILC","Your Email: ");
define("_FRIENDNAMEC","Friend Name: ");
define("_FRIENDEMAILC","Friend Email: ");
define("_SEND","Send");

define("_RECOMMENDSITE","Recommend this Site to a Friend");

// %s is your site name
define("_INTSITE","Interesting Site: %s");

// %s is the name of your friend
define("_HELLO","Hello %s,");

define("_YOURFRIEND","Your friend %s considered our site interesting and wanted to send it to you.");

define("_SITENAME","Site Name:");
define("_SITEURL","Site URL:");
define("_REFERENCESENT","The reference to our site has been sent to your friend. Thanks!");

define("_ENTERYNAME","Please enter your name");
define("_ENTERYMAIL","Please enteer your email address");
define("_ENTERFNAME","Please enter your friend's name");
define("_ENTERFMAIL","Please enter your friend's email address");
define("_NEEDINFO","You need to enter required info!");
define("_INVALIDEMAIL1","Email address you provided is not a valid address.");
define("_INVALIDEMAIL2","Please check the address and try again.");

//%%%%%%	File Name module.textsanitizer.php 	%%%%%
define("_QUOTEC","Quote:");

//%%%%%%	File Name admin.php 	%%%%%
define("_NOPERM","Sorry, you don't have the permission to access this area.");

define("_NO","No");
define("_YES","Yes");
define("_EDIT","Edit");
define("_DELETE","Delete");
define("_MODULENOEXIST","Selected module does not exist!");

//%%%%%%	File Name commentform.php 	%%%%%
define("_REGISTER","Register");

//%%%%%		TIME FORMAT SETTINGS   %%%%%

define("_DATESTRING","Y/n/j G:i:s");
define("_MEDIUMDATESTRING","Y/n/j G:i");
define("_SHORTDATESTRING","Y/n/j");
/*
The following characters are recognized in the format string: 
a - "am" or "pm" 
A - "AM" or "PM" 
d - day of the month, 2 digits with leading zeros; i.e. "01" to "31" 
D - day of the week, textual, 3 letters; i.e. "Fri" 
F - month, textual, long; i.e. "January" 
h - hour, 12-hour format; i.e. "01" to "12" 
H - hour, 24-hour format; i.e. "00" to "23" 
g - hour, 12-hour format without leading zeros; i.e. "1" to "12" 
G - hour, 24-hour format without leading zeros; i.e. "0" to "23" 
i - minutes; i.e. "00" to "59" 
j - day of the month without leading zeros; i.e. "1" to "31" 
l (lowercase 'L') - day of the week, textual, long; i.e. "Friday" 
L - boolean for whether it is a leap year; i.e. "0" or "1" 
m - month; i.e. "01" to "12" 
n - month without leading zeros; i.e. "1" to "12" 
M - month, textual, 3 letters; i.e. "Jan" 
s - seconds; i.e. "00" to "59" 
S - English ordinal suffix, textual, 2 characters; i.e. "th", "nd" 
t - number of days in the given month; i.e. "28" to "31" 
T - Timezone setting of this machine; i.e. "MDT" 
U - seconds since the epoch 
w - day of the week, numeric, i.e. "0" (Sunday) to "6" (Saturday) 
Y - year, 4 digits; i.e. "1999" 
y - year, 2 digits; i.e. "99" 
z - day of the year; i.e. "0" to "365" 
Z - timezone offset in seconds (i.e. "-43200" to "43200") 
*/


//%%%%%		LANGUAGE SPECIFIC SETTINGS   %%%%%
define("_CHARSET", "ISO-8859-1");

// change 0 to 1 if this language is a multi-bytes language
define("XOOPS_USE_MULTIBYTES", "0");
?>