<?php
//%%%%%%	File Name search.php 	%%%%%
define("_SR_SEARCH","Search");
define("_SR_PLZENTER","Please enter all required data!");
define("_SR_SEARCHRESULTS","Search Results");
define("_SR_NOMATCH","No Match Found to your Query");
define("_SR_FOUND","Found <b>%s</b> matche(s)");
define("_SR_SHOWING","(Showing %d - %d)");
define("_SR_ANY","Any (OR)");
define("_SR_ALL","All (AND)");
define("_SR_EXACT","Exact Match");
define("_SR_SHOWALLR","Show all results");
define("_SR_NEXT","Next >>");
define("_SR_PREVIOUS","<< Previous");
?>