<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

if(!defined("XOOPS_HEADER_INCLUDED")){
	define("XOOPS_HEADER_INCLUDED",1);
	include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

	function head() {
		global $xoopsConfig, $xoopsTheme;
		global $xoopsOption;
		$myts = new MyTextSanitizer;
		$meta = getMeta();
		$meta = $myts->makeTareaData4InsideQuotes($meta);
		echo "<?xml version='1.0' encoding='utf-8'?>\n";
    		echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n";
    		echo "<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>\n";


    		echo "<head>\n";
		echo "<title>".$xoopsConfig['sitename']."</title>\n";
		echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset="._CHARSET."\"></meta>\n";
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  // Date in the past
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
                                                         // always modified
                header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");                // HTTP/1.0
		echo "<meta name=\"author\" content=\"".$xoopsConfig['sitename']."\"></meta>\n";
		echo "<meta name=\"copyright\" content=\"Copyright (c) 2001 by ".$xoopsConfig['sitename']."\"></meta>\n";
		echo "<meta name=\"keywords\" content=\"".$meta."\"></meta>\n";
		echo "<meta name=\"description\" content=\"".$xoopsConfig['slogan']."\"></meta>\n";
		echo "<meta name=\"generator\" content=\"".$xoopsConfig['version']."\"></meta>\n\n\n";
		echo "<script type=\"text/javascript\">\n";
		echo "<!--\n";
		echo "function showimage() {\n";
		echo "if (!document.images)\n";
		echo "return\n";
		echo "document.images.avatar.src=\n";
		echo "'".$xoopsConfig['xoops_url']."/images/avatar/' + document.userinfo.user_avatar.options[document.userinfo.user_avatar.selectedIndex].value\n";
		echo "}\n\n";

		if ($xoopsOption['forumpage']==1) {
			echo "function x () {\n";
			echo "return;\n";
			echo "}\n";
			echo "\n";
			echo "function DoSmilie(addSmilie) {\n";
			echo "\n";
			echo "var addSmilie;\n";
			echo "var revisedMessage;\n";
			echo "var currentMessage = document.coolsus.message.value;\n";
			echo "revisedMessage = currentMessage+addSmilie;\n";
			echo "document.coolsus.message.value=revisedMessage;\n";
			echo "document.coolsus.message.focus();\n";
			echo "return;\n";
			echo "}\n";
			echo "\n";

			$jsmesvalue = "\"+coolsus.message.value.length+\"";
			echo "var postmaxchars = 10000;\n";
			echo "function validate(coolsus) {\n";
			echo "if (coolsus.message.value==\"\" || coolsus.subject.value==\"\") {\n";
			echo "alert(\""._PLZCOMPLETE."\");\n";
			echo "return false; }\n";
			echo "if (postmaxchars != 0) {\n";
			echo "if (coolsus.message.value.length > 10000) {\n";
			echo "alert(\""._MESSAGETOOLONG."\\n\\n"._REDUCEMESSAGE."\\n";
			printf(_ITISCHARSLONG,$jsmesvalue);
			echo " \");\n";
			echo "return false; }\n";
			echo "else { return true; }\n";
			echo "} else { return true; }\n";
			echo "}\n";
			echo "function checklength(coolsus) {\n";
			echo "if (postmaxchars != 0) { message = \"\\n"._MAXIMUMLENGTH."\"; }\n";
			echo "else { message = \"\"; }\n";
			echo "alert(\"";
			printf(_ITISCHARSLONG,$jsmesvalue);
			echo "\"+message);\n";
			echo "}\n";
			echo "\n\n\n";
		}
		echo "function init() {\n\n";
		echo "}\n\n";
		echo "function openWithSelfMain(url,name,width,height){\n";
		echo "var options='width=' + width + ',height=' + height + 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no';\n";
		echo "new_window=window.open(url, name, options);\n";
		echo "self.name = 'main';\n";
		echo "new_window.focus();\n";
		echo "}\n\n";
		echo "//-->";
		echo "</script>\n\n";

		$currenttheme = getTheme();
		include($xoopsConfig['root_path']."themes/".$currenttheme."/theme.php");
		if(file_exists($xoopsConfig['root_path']."themes/".$currenttheme."/language/lang-".$xoopsConfig['language'].".php")){
			include($xoopsConfig['root_path']."themes/".$currenttheme."/language/lang-".$xoopsConfig['language'].".php");
		}
		$themecss = getcss($currenttheme);
		echo "<style type='text/css' media='all'><!-- @import url(".$xoopsConfig['xoops_url']."/xoops.css); --></style>\n";
		if($themecss){
			echo "<style type='text/css' media='all'><!-- @import url($themecss); --></style>\n\n";
		}
		echo "<script type='text/javascript'>\n\n";
		echo "window.onload=init;\n";
		echo "</script>\n";

		themeheader($xoopsOption['show_rblock']);
	}

	if ($xoopsConfig['gzip_compression']) {
		ob_start("ob_gzhandler");
	}
	updateLastseen();
	head();

}
?>