<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

if ( !defined("XOOPS_MAINFILE_INCLUDED") ) {
	define("XOOPS_MAINFILE_INCLUDED",1);

	//################## CHANGE THIS !! #######################

	// Change /path/to/xoops/directory to the physical path to
	// your main XOOPS directory WITHOUT trailing slash

	define("XOOPS_ROOT_PATH","/path/to/xoops/directory");

	//#########################################################
	
	define("XOOPS_SIDEBLOCK_LEFT",0);
	define("XOOPS_SIDEBLOCK_RIGHT",1);
	define("XOOPS_SIDEBLOCK_BOTH",2);
	define("XOOPS_CENTERBLOCK_LEFT",3);
	define("XOOPS_CENTERBLOCK_RIGHT",4);
	define("XOOPS_CENTERBLOCK_CENTER",5);
	define("XOOPS_BLOCK_INVISIBLE",0);
	define("XOOPS_BLOCK_ANON",1);
	define("XOOPS_BLOCK_USER",2);
	define("XOOPS_BLOCK_ADMIN",3);

	// #################### Error reporting settings ##################
	if ( ini_get("error_reporting") != "E_ERROR & E_PARSE & ~E_WARNING" ) {
		$old_inivalue = ini_set("error_reporting", "E_ERROR & E_PARSE & ~E_WARNING");
	}
	// uncomment the following line to show all warning messages (for debug)
	//if ( $debug==1 ) {
	//	error_reporting (E_ALL); 
	//}

	// #################### Include global config files ##################
	include(XOOPS_ROOT_PATH."/include/config.php");
	include($xoopsConfig['root_path']."modules/system/cache/config.php");

	// ############## Include blocks construction class file ##############
	include_once($xoopsConfig['root_path']."class/xoopsblock.php");

	// #################### Include site-wide lang file ##################
	if ( file_exists($xoopsConfig['root_path']."language/".$xoopsConfig['language']."/global.php") ) {
		include($xoopsConfig['root_path']."language/".$xoopsConfig['language']."/global.php");
	} else {
		include($xoopsConfig['root_path']."language/english/global.php");
	}

	// ################ Include page-specific lang file ################
	if ( isset($xoopsOption['pagetype']) ) {
		if ( file_exists($xoopsConfig['root_path']."/language/".$xoopsConfig['language']."/".$xoopsOption['pagetype'].".php") ) {
			include($xoopsConfig['root_path']."language/".$xoopsConfig['language']."/".$xoopsOption['pagetype'].".php");
		} else {
			include($xoopsConfig['root_path']."language/english/".$xoopsOption['pagetype'].".php");
		}
	}

	if ( !defined("XOOPS_USE_MULTIBYTES") ) {
		define("XOOPS_USE_MULTIBYTES",0);
	}

	// #################### Connect to DB ##################
	include($xoopsConfig['root_path']."class/".$xoopsConfig['database'].".php");

	if ( $xoopsConfig['prefix'] && $xoopsConfig['dbhost'] && isset($xoopsConfig['dbuname']) && isset($xoopsConfig['dbpass']) && $xoopsConfig['dbname'] ) {
		$xoopsDB = new Database;
		$xoopsDB->setPrefix($xoopsConfig['prefix']);
		$xoopsDB->connect($xoopsConfig['dbhost'], $xoopsConfig['dbuname'], $xoopsConfig['dbpass'], $xoopsConfig['dbname'], $xoopsConfig['db_pconnect']);
	}
	// ############## Login a user with a valid session ##############
	include_once($xoopsConfig['root_path']."class/xoopsuser.php");
	$xoopsUser = "";

	if ( isset($HTTP_COOKIE_VARS[$xoopsConfig['sessioncookie']]) ) {
		$session = new XoopsUserSession($HTTP_COOKIE_VARS[$xoopsConfig['sessioncookie']]);
		// if the session is valid, update it and create a user object
		if($session->isValid()){
			$session->update();
			$xoopsUser = new XoopsUser($session->uid());
		}
	}


	// ################## Various functions from here ################

	/*
	 * Function to display formatted times in user timezone
	 */
	function formatTimestamp($time, $format="l", $timeoffset="") {
		global $xoopsConfig, $xoopsUser;
		if($timeoffset == ""){
			if($xoopsUser){
				$timeoffset = $xoopsUser->timezone();
			}else{
				$timeoffset = $xoopsConfig['default_TZ'];
			}
		}
		$usertimestamp = $time + ($timeoffset - $xoopsConfig['server_TZ'])*3600;
		if($format=="s"){
			$datestring = _SHORTDATESTRING;
		}elseif($format=="m"){
			$datestring = _MEDIUMDATESTRING;
		}elseif($format=="mysql"){
			$datestring = "Y-m-d H:i:s";
		}else{
			$datestring = _DATESTRING;
		}
        	$datetime = date($datestring, $usertimestamp);
        	$datetime = ucfirst($datetime);
        	return $datetime;
	}


	/*
	 * Functions to display dhtml loading image box
	 */
	function OpenWaitBox() {
		global $xoopsTheme, $xoopsConfig;
		if ($xoopsConfig['display_loading_img']==1){
		echo "<div id=\"waitDiv\" style=\"position:absolute;left:40%;top:50%;visibility:hidden\" align=\"center\">
<table cellpadding=6 border=2 bgcolor=\"".$xoopsTheme['bgcolor2']."\" bordercolor=\"".$xoopsTheme['textcolor2']."\"><tr><td align=\"center\">
<font color=\"".$xoopsTheme['textcolor1']."\"><b><big>" ._FETCHING."</big></b></font><br /> <img src=\"".$xoopsConfig['xoops_url']."/images/await.gif\" border=\"0\" /><br />
	<font color=\"".$xoopsTheme['textcolor1']."\">" ._PLEASEWAIT."</font></td></tr></table></div> <script> <!-- 
	var DHTML = (document.getElementById || document.all || document.layers);
function ap_getObj(name) { if (document.getElementById) { return document.getElementById(name).style; } else if (document.all) { return document.all[name].style;
} else if (document.layers) { return document.layers[name]; } } function ap_showWaitMessage(div,flag)  { if (!DHTML) return; var x = ap_getObj(div); x.visibility = (flag) ? 'visible':'hidden'
if(! document.getElementById) if(document.layers) x.left=280/2; return true; } ap_showWaitMessage('waitDiv', 1); 
 //--> </script>";
		}
	}

	function CloseWaitBox()  {
		global $xoopsConfig;
		if ($xoopsConfig['display_loading_img']==1){
			echo "<script language=\"javascript\"> <!-- 
  			ap_showWaitMessage('waitDiv', 0);
 			//--></script><br />";
		}	
	}

	function makePass() {
		$makepass="";
		$syllables = array(er,in,tia,wol,fe,pre,vet,jo,nes,al,len,son,cha,ir,ler,bo,ok,tio,nar,sim,ple,bla,ten,toe,cho,co,lat,spe,ak,er,po,co,lor,pen,cil,li,ght,wh,at,the,he,ck,is,mam,bo,no,fi,ve,any,way,pol,iti,cs,ra,dio,sou,rce,sea,rch,pa,per,com,bo,sp,eak,st,fi,rst,gr,oup,boy,ea,gle,tr,ail,bi,ble,brb,pri,dee,kay,en,be,se);
		srand((double)microtime()*1000000);
		for ($count=1;$count<=4;$count++) {
			if (rand()%10 == 1) {
				$makepass .= sprintf("%0.0f",(rand()%50)+1);
			} else {
				$makepass .= sprintf("%s",$syllables[rand()%62]);
			}
		}
		return $makepass;
	}

	function checkEmail($email){
		if (!email || !eregi("^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3}$",$email)){
			return false;
		}
		return true;
	}

	function formatURL($url){
		if (($url != "") && (!(eregi('(^http[s]*:[/]+)(.*)', $url)))){
			$url = "http://" . $url;
		}
		return $url;
	}

	/*
	* Prints allowed html tags on this site
	*/
	function get_allowed_html(){
		global $xoopsConfig;
		$allowed = str_replace(">","> ",$xoopsConfig['allowed_html']);
		return htmlspecialchars($allowed);
	}

	/*
	 * Function to create formatted html image tag
	 */
	function GetImgTag($dir, $image, $alttext="", $border=0, $align="",$height=0,$width=0,$hspace=0,$vspace=0,$bordercolor="") { 
		global $xoopsConfig;
		if (strrpos($dir,"/") == strlen($dir) -1 ) {
			$dir = substr($dir,0, strlen($dir) -1 );
		}
		$hpath = $xoopsConfig['root_path'].$dir;
		$path = "".$hpath."/".$xoopsConfig['language']."/".$image.""; 
		if (!file_exists($path)) { 
			$path = $hpath."/".$image.""; 
		} 
		$size = @GetImageSize($path); 
		if(!stristr($dir, "http://")){
			$ImgTag = "<img src=\"".$xoopsConfig['xoops_url']."/$dir/$image\"";
		} else {
			$ImgTag = "<img src=\"$dir/$image\"";
		}
		if ($width != -1) {
			if (($size[0]) and ($width == 0)) { 
				$ImgTag .= " width=\"$size[0]\""; 
			} elseif ($width != 0) {
				$ImgTag .= " width=\"$width\"";
			}
		}
		if ($height != -1) {
			if (($size[1]) and ($height == 0)) { 
				$ImgTag .= " height=\"$size[1]\""; 
			} elseif ($height !=0) {
				$ImgTag .= " height=\"$height\"";
			} 
		}
		if ($alttext != "") {
			$ImgTag .= " alt=\"$alttext\""; 
		}
		$ImgTag .= " border=\"$border\""; 
		if ($align != "") {
			$ImgTag .= " align=\"$align\"";
		}
		if ($hspace != 0) {
			$ImgTag .= " hspace=\"$hspace\"";
		}
		if ($vspace != 0) {
			$ImgTag .= " vspace=\"$vspace\"";
		}
		if ($bordercolor != "") {
			$ImgTag .= " bordercolor=\"$bordercolor\"";
		}
		$ImgTag .= " />"; 
		return $ImgTag; 
	}

	/*
	 * Function to display banners in all pages
	 */
	function showbanner() {
		global $xoopsDB, $xoopsConfig;
    		$bresult = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("banner")."");
    		list($numrows) = $xoopsDB->fetch_row($bresult);
    		if ($numrows>1) {
			$numrows = $numrows-1;
			mt_srand((double)microtime()*1000000);
			$bannum = mt_rand(0, $numrows);
    		} else {
			$bannum = 0;
    		}
    		if($numrows>0) {
			$bresult = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("banner")."",0,1,$bannum);
    			list($bid, $cid, $imptotal, $impmade, $clicks, $imageurl, $clickurl, $date) = $xoopsDB->fetch_row($bresult);
    			$myhost = getenv("REMOTE_ADDR");
    			if($xoopsConfig['my_ip']==$myhost) {
    			} else {
				$xoopsDB->query("UPDATE ".$xoopsDB->prefix("banner")." SET impmade=impmade+1 WHERE bid=".$bid."");
    			}
	/* Check if this impression is the last one and print the banner */
			if($imptotal==$impmade) {
				$newid = $xoopsDB->GenID("bannerfinish_bid_seq");
	    			$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("bannerfinish")." (bid, cid, impressions, clicks, datestart, dateend) VALUES ($newid, $cid, $impmade, $clicks, $date, ".time().")");
	    			$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("banner")." WHERE bid=".$bid."");
			}
    			echo"<div align=\"center\"><a href=\"".$xoopsConfig['xoops_url']."/banners.php?op=click&amp;bid=".$bid."\" target=\"_blank\"><img src=\"".$imageurl."\" border=\"0\" /></a></div>";
    		}
	}


	/*
	 * Function to redirect a user to certain pages
	 */
	function redirect_header($url, $time=4, $message=""){
		global $xoopsConfig;
		echo "<html><head>\n";
		echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset="._CHARSET."\">\n";
		echo "<meta http-equiv=\"Refresh\" content=\"".$time."; URL=".$url."\">\n";
		$currenttheme = getTheme();
		include_once($xoopsConfig['root_path']."themes/".$currenttheme."/theme.php");
		if(file_exists($xoopsConfig['root_path']."themes/".$currenttheme."/language/lang-".$xoopsConfig['language'].".php")){
			include($xoopsConfig['root_path']."themes/".$currenttheme."/language/lang-".$xoopsConfig['language'].".php");
		}
		$themecss = getcss($currenttheme);
		if($themecss){
   			echo "<link rel=\"stylesheet\" href=\"".$themecss."\" type=\"text/css\">\n\n";
		}
		echo "</head><body><br /><br /><br /><br /><br /><br /><div align=\"center\">\n";
		if($message!=""){
			echo "<p><h4>".$message."</h4>\n";
		}else{
    			echo "<p><h4>"._TAKINGBACK."</h4>\n";
		}
		echo "<br /><b>\n";
		printf(_IFNOTRELOAD,$url);
		echo "</b></p>\n";
		echo "</div>\n";
		echo "</body></html>";
	}


	/*
	 * Function to get meta tag keywords
	 */
	function getMeta(){
		global $xoopsDB;
		$sql = "SELECT meta FROM ".$xoopsDB->prefix("metafooter")." WHERE (1=1)";
		$result = $xoopsDB->query($sql);
		list($meta) = $xoopsDB->fetch_row($result);
		return $meta;
	}

	/*
	 * Function to get footer
	 */
	function getFooter(){
		global $xoopsDB;
		$sql = "SELECT footer FROM ".$xoopsDB->prefix("metafooter")." WHERE (1=1)";
		$result = $xoopsDB->query($sql);
		list($footer) = $xoopsDB->fetch_row($result);
		return $footer;
	}

	/*
	 * Function to get a user selected theme file
	 */
	function getTheme($theme=""){
		global $xoopsConfig, $xoopsDB, $xoopsUser;
		$themedir = $xoopsConfig['root_path']."themes";
		if(!isset($theme) || trim($theme) == ""){
			if($xoopsUser) {
				$theme = $xoopsUser->theme();
				if(isset($theme) && $theme != ""){
					$theme_exists = @fopen($themedir."/".$theme."/theme.php", "r");
					if($theme_exists) {
						return $theme;
					}
				}else{
					return $xoopsConfig['default_theme'];
				}
			}else{
				return $xoopsConfig['default_theme'];
			}
		}else{
			$theme = trim($theme);
			$theme_exists = @fopen("$themedir/$theme/theme.php", "r");
			if($theme_exists) {
				return $theme;
			}
		}
		return $xoopsConfig['default_theme'];
	}

	/*
	 * Function to get css file for a certain theme
	 */
	function getcss($whatdir) {
		global $xoopsConfig;
		if(ereg('MSIE',getenv("HTTP_USER_AGENT")) && !ereg('Opera',getenv("HTTP_USER_AGENT"))){
			$str_css = "style.css";
		}else{
			$str_css = "styleNN.css";
		}
		$themedir = $xoopsConfig['root_path']."themes";
		$filepath = "$themedir/$whatdir/style/$str_css";
		$default = "$themedir/$whatdir/style/style.css";
		if (file_exists($filepath)) {
		//need to change to absolute path for inclusion from modules
			$whatcss = $xoopsConfig['xoops_url']."/themes/$whatdir/style/$str_css";
		} elseif(file_exists($default)) {
			$whatcss = $xoopsConfig['xoops_url']."/themes/$whatdir/style/style.css";
		}else{
			$whatcss = "";
		}
		return $whatcss;
	}

	/*
	 * Function to display a message encouraging users
	 * to use web standards browser
	 */
	function waspInfo() {
		return "<p class='ahem'><small>This site will look MUCH better in a browser that supports <a title='The Web Standards Project&apos;s BROWSER UPGRADE initiative.' href='http://www.webstandards.org/upgrade/'>web standards</a>, but its content is accessible to any browser or Internet device.</small></p>";
	}

	/*
	 * Function to find if a certain user/IP is banned
	 */
	function is_banned($ipuser, $type) {
   		global $xoopsDB;
   		// Remove old bans
   		$sql = "DELETE FROM ".$xoopsDB->prefix("user_banlist")." WHERE (ban_end < ". mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")).") AND (ban_end > 0)";
   		$xoopsDB->query($sql);
   		switch($type) {
    			case "ip":
      			$sql = "SELECT ban_ip FROM ".$xoopsDB->prefix("user_banlist")."";
      			if($r = $xoopsDB->query($sql)) {
	 			while($iprow = $xoopsDB->fetch_array($r)) {
	    				$ip = $iprow['ban_ip'];
	    				if($ip[strlen($ip) - 1] == ".") {
	       					$db_ip = explode(".", $ip);
	       					$this_ip = explode(".", $ipuser);
	       					$size = count($db_ip);
	       					for($x=0;$x<$size-1;$x++) {
		 					$my_ip .= $this_ip[$x] . ".";

	       						if($my_ip == $ip)
		 						return TRUE;
						}
	    				} else {
	       					if($ipuser == $ip)
		 				return TRUE;
	    				}
	 			}
      			} else {
				return FALSE;
			}
      			break;

    			case "username":
      			$sql = "SELECT ban_userid FROM ".$xoopsDB->prefix("user_banlist")." WHERE ban_userid = $ipuser";
      			if($r = $xoopsDB->query($sql)) {
	 			if( $xoopsDB->num_rows($r) > 0 ){
	   				return TRUE;
				}
      			}
      			break;
   		}
		return FALSE;
	}

	/*
	 * Function to update last seen table
	 */
	function updateLastseen(){
		global $xoopsUser, $xoopsDB, $xoopsConfig;
		$past = time() - 300;
		$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("lastseen")." WHERE uid=0 AND time < ".$past."",1);
		$xoopsDB->query("UPDATE ".$xoopsDB->prefix("lastseen")." SET online=0 WHERE time < ".$past."",1);
		$ip = getenv("REMOTE_ADDR");
		if($xoopsUser){
			$uid = $xoopsUser->uid();
			$uname = $xoopsUser->uname();
		} else {
			$uid = 0;
			$uname = "Anonymous";
		}
		$sql = "SELECT count(*) FROM ".$xoopsDB->prefix("lastseen")." WHERE uid=".$uid."";
		if($uid == 0){
			$sql .= " AND ip='".$ip."'";
		}
		//echo $sql;
		$result = $xoopsDB->query($sql);
		list($num_rows) = $xoopsDB->fetch_row($result);

		if ( $num_rows > 0) {
			$sql = "UPDATE ".$xoopsDB->prefix("lastseen")." SET time = " . time() . ", ip='".$ip."', online=1 WHERE uid=".$uid."";
			if($uid == 0){
				$sql .= " AND ip='".$ip."'";
			}
			$xoopsDB->query($sql,1);
		} else {
			$sql = "INSERT INTO ".$xoopsDB->prefix("lastseen")." (uid, username, time, ip, online) VALUES (".$uid.", '".$uname."', ".time().", '".$ip."', 1)";
			$xoopsDB->query($sql,1);
		}
	}

	// #################### Block functions from here ##################

	/*
	 * Purpose : Builds the blocks on both sides
	 * Input   : $side = On wich side should the block are displayed?
	 *             0, l, left : On the left side
	 *             1, r, right: On the right side
	 *             other:   Only on one side (
	 *                          Call from theme.php makes all blocks on the left side
	 *                          and from theme.php for the right site)
	 */
	function make_sidebar($side) {
		global $xoopsUser;
		$xoopsblock = new XoopsBlock();
		$arr = array();
		if (($side == "l") or ($side == 'left') or ($side == "0")) {
			$side = XOOPS_SIDEBLOCK_LEFT;
		} elseif (($side == "r") or ($side == 'right') or ($side == "1")) {
			$side = XOOPS_SIDEBLOCK_RIGHT;
		} else {
			$side = XOOPS_SIDEBLOCK_BOTH;
		}
		if ( $xoopsUser ) {
			if ( $xoopsUser->is_admin() ) {
				$visible = XOOPS_BLOCK_ADMIN;
			} else {
				$visible = XOOPS_BLOCK_USER;
			}
			$arr = $xoopsblock->getAllBlocks($side, $xoopsUser, $visible);
		} else {
			$arr = $xoopsblock->getAllBlocks($side, 0, XOOPS_BLOCK_ANON);
		}
		foreach($arr as $myblock){
			$block = array();
			$block = $myblock->buildBlock();
			if($block){
				themesidebox($block['title'],$block['content']);
			}
			unset($myblock);
			unset($block);
		}
	}

	/*
	 * Function to display center block
	 * side 3 : left half side of the center block
	 *      4 : right half side of the center block
	 *      5 : takes both the left and right sides of the center block
	 */
	function make_cblock() {
		global $xoopsUser;
		$xoopsblock = new XoopsBlock();
		$arr = array();
		$cblock = "<table width=\"100%\">";
		$hasContent = 0;
		if ( $xoopsUser ) {
			if ( $xoopsUser->is_admin() ) {
				$visible = XOOPS_BLOCK_ADMIN;
			} else {
				$visible = XOOPS_BLOCK_USER;
			}
		} else {
			$visible = XOOPS_BLOCK_ANON;
		}
		if ( $xoopsUser ) {
			$arr = $xoopsblock->getAllBlocks(XOOPS_CENTERBLOCK_CENTER, $xoopsUser, $visible);
		} else {
			$arr = $xoopsblock->getAllBlocks(XOOPS_CENTERBLOCK_CENTER, 0, $visible);
		}

		if ( count($arr) > 0 ){
			$hasContent = 1;
			foreach ( $arr as $myblock ) {
				$block = array();
				$block = $myblock->buildBlock();
				if ( $block ) {
					if ( $block['title'] != "" ) {
						$cblock .= "<tr><td colspan=\"2\"><b>".$block['title']."</b><hr />".$block['content']."<br /><br /></td></tr>\n";
					} else {
						$cblock .= "<tr><td colspan=\"2\">".$block['content']."<br /><br /></td></tr>\n";
					}
				}
				unset($myblock);
				unset($block);
			}
		}
		if ( $xoopsUser ) {
			$arr = $xoopsblock->getAllBlocks(XOOPS_CENTERBLOCK_LEFT, $xoopsUser, $visible);
		} else {
			$arr = $xoopsblock->getAllBlocks(XOOPS_CENTERBLOCK_LEFT, 0, $visible);
		}
		$cblock .= "<tr valign=\"top\"><td width=\"50%\">\n";
		if ( count($arr) > 0 ){
			$hasContent = 1;
			foreach ( $arr as $myblock ) {
				$block = array();
				$block = $myblock->buildBlock();
				if ( $block ) {
					if ( $block['title'] != "" ) {
						$cblock .= "<p><b>".$block['title']."</b><hr />".$block['content']."</p>\n";
					} else {
						$cblock .= "<p>".$block['content']."</p>\n";
					}
				}
				unset($myblock);
				unset($block);
			}
		}
		if ( $xoopsUser ) {
			$arr = $xoopsblock->getAllBlocks(XOOPS_CENTERBLOCK_RIGHT, $xoopsUser, $visible);
		} else {
			$arr = $xoopsblock->getAllBlocks(XOOPS_CENTERBLOCK_RIGHT, 0, $visible);
		}
		$cblock .= "</td><td width=\"50%\">\n";
		if ( count($arr > 0) ){
			$hasContent = 1;
			foreach ( $arr as $myblock ) {
				$block = array();
				$block = $myblock->buildBlock();
				if ( $block ) {
					if ( $block['title'] != "" ) {
						$cblock .= "<p><b>".$block['title']."</b><hr />".$block['content']."</p>\n";
					} else {
						$cblock .= "<p>".$block['content']."</p>\n";
					}
				}
				unset($myblock);
				unset($block);
			}
		}
		$cblock .= "</td></tr>\n";
		$cblock .= "</table>";
		if ( $hasContent ) {
			OpenTable();
			echo $cblock;
			CloseTable();
		}
	}
}
?>