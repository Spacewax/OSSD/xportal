<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
$xoopsOption['pagetype'] = "search";

include("mainfile.php");
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");
include_once($xoopsConfig['root_path']."class/xoopshtmlform.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
if ( isset($action) ) {
	$action = trim($action);
}
if ( isset($query) ) {
	$query = trim($query);
}
if ( !isset($action) or $action=="" ) {
	$action = "search";
}

if ( $action == "results" && (!isset($query) || $query == "") ) {
	redirect_header("search.php?query=$query",1,_SR_PLZENTER);
	exit();
}

if ( $action == "showall" && (!isset($query) || $query == "" || empty($mid)) ) {
	redirect_header("search.php?query=$query",1,_SR_PLZENTER);
	exit();
}

if ( !isset($andor) || $andor =="" ) {
	$andor = "AND";
}

if ( $action == "results" ) {
	$myts = new MyTextSanitizer();
	if ( !isset($mids) || count($mids)==0 ) {
		$mids = array();
		$mids = XoopsModule::getHasSearchModulesList(false);
	}
	if ( $andor != "exact" ) {
		$queries = split(" ",$myts->oopsAddSlashes(trim($query)));
	} else {
		$queries = array($query);
	}
	include($xoopsConfig['root_path']."header.php");
	OpenTable();
	echo "<h3>"._SR_SEARCHRESULTS."</h3>\n";
	foreach($mids as $mid){
		$module = new XoopsModule($mid);
		$results = array();
		$results = $module->Search($queries, $andor, 5, 0);
		echo "<p><h4>".$module->name()."</h4>";
		if ( !is_array($results) || !($count = count($results)) ) {
			echo _SR_NOMATCH."</p>";
		} else {
			for ( $i=0;$i<$count;$i++ ) {
				if ( isset($results[$i]['image']) || $results[$i]['image'] != "" ) {
					echo "<img src='modules/".$module->dirname()."/".$results[$i]['image']."' alt='".$module->name()."' />&nbsp;";
				} else {
					echo "<img src='images/icons/posticon.gif' alt='".$module->name()."' width='14' height='11' />&nbsp;";
				}
				echo "<b><a href='modules/".$module->dirname()."/".$results[$i]['link']."'>".$myts->makeTboxData4Show($results[$i]['title'])."</a></b><br />\n";
				echo "<small>";
				if ( isset($results[$i]['uid']) ) {
					$uname = XoopsUser::get_uname_from_id($results[$i]['uid']);
					echo "&nbsp;&nbsp;<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$results[$i]['uid']."'>".$uname."</a>\n";
				}
				echo $results[$i]['time'] ? " (". formatTimestamp($results[$i]['time']).")" : "";
				echo "</small><br />\n";
			}
			if ( $count == 5 ) {
				echo "<br /><form action='search.php' method='post' name='showall".$mid."'>
				<input type='hidden' value='".$query."' name='query' />
				<input type='hidden' name='mid' value='".$mid."' />
				<input type='hidden' name='action' value='showall' />
				<input type='hidden' name='andor' value='".$andor."' />
				<a href='#$mid' onclick=\"javascript:document.showall".$mid.".submit();\">"._SR_SHOWALLR."</a></form></p>";
			}
		}
	}
	CloseTable();
}

if ( $action =="search" || $action =="results" ) {
	$hf = new XoopsHtmlForm();
	if ( !isset($myts) ) {
		$myts = new MyTextSanitizer();
	}
	$modlist = array();
	$modlist = XoopsModule::getHasSearchModulesList();
	if ( $action == "search" ) {
		include($xoopsConfig['root_path']."header.php");
	}
	OpenTable();
	if ( $action == "search" ) {
		echo "<h3>"._SR_SEARCH."</h3>\n";
	}
	echo "<form action='search.php' method='post'>";
	$query = $myts->makeTboxData4PreviewInForm($query);
	echo $hf->input_text("query",$query);
	$andorlist = array("AND"=>_SR_ALL, "OR"=>_SR_ANY, "exact"=>_SR_EXACT);
	echo $hf->select("andor", $andorlist, $andor);
	echo $hf->input_submit(_SR_SEARCH)."<br />"; 
	echo $hf->input_check_radio("mids", $modlist, $mids, TRUE);
	echo $hf->input_hidden("action","results");
	
	echo "</form>";
	CloseTable();
}

if ( $action == "showall" ) {
	$myts = new MyTextSanitizer();
	if ( $andor != "exact" ){
		$queries = split(" ",$myts->oopsAddSlashes(trim($query)));
	} else {
		$queries = array($query);
	}
	include($xoopsConfig['root_path']."header.php");
	$module = new XoopsModule($mid);
	$results = array();
	$results = $module->Search($queries, $andor);
	$count = count($results);
	if ( !isset($start) ) {
		$start = 0;
	}
	if ( $count-$start > 20 ) {
		$limit = $start + 20;
	} else { 
		$limit = $count;
	}
	OpenTable();
	echo "<h3>"._SR_SEARCHRESULTS."</h3>\n";
	printf(_SR_FOUND,$count);
	echo "<br />";
	printf(_SR_SHOWING, $start+1, $limit);
	echo "<p><h4>".$module->name()."</h4>";
	for ( $i=$start;$i<$limit;$i++ ) {
		if ( isset($results[$i]['image']) || $results[$i]['image'] != "" ) {
			echo "<img src='modules/".$module->dirname()."/".$results[$i]['image']."' alt='".$module->name()."' />&nbsp;";
		} else {
			echo "<img src='images/icons/posticon.gif' alt='".$module->name()."' width='14' height='11' />&nbsp;";
		}
		echo "<b><a href='modules/".$module->dirname()."/".$results[$i]['link']."'>".$myts->makeTboxData4Show($results[$i]['title'])."</a></b><br />\n";
		echo "<small>";
		if ( isset($results[$i]['uid']) ) {
			$uname = XoopsUser::get_uname_from_id($results[$i]['uid']);
			echo "&nbsp;&nbsp;<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$results[$i]['uid']."'>".$uname."</a>\n";
		}
		echo $results[$i]['time'] ? " (". formatTimestamp($results[$i]['time']).")" : "";
		echo "</small><br />\n";
	}
	echo "</p><table><tr>";
	if ( $start>0 ) {
		$prev = $start-20;
		echo "<td align='left'><form action='search.php' method='post' name='showprev'>
		<input type='hidden' value='".$query."' name='query' />
		<input type='hidden' name='mid' value='".$mid."' />
		<input type='hidden' name='action' value='showall' />
		<input type='hidden' name='andor' value='".$andor."' />
		<input type='hidden' name='start' value='".$prev."' />
		<a href='#$prev' onclick=\"javascript:document.showprev.submit();\">"._SR_PREVIOUS."</a></form></td>\n";
	}
	echo "<td>&nbsp;&nbsp;</td>";
	if ( $count-$start > 20 ) {
		$next = $start + 20;
		echo "<td align='right'><form action='search.php' method='post' name='shownext'>
		<input type='hidden' value='".$query."' name='query' />
		<input type='hidden' name='mid' value='".$mid."' />
		<input type='hidden' name='action' value='showall' />
		<input type='hidden' name='andor' value='".$andor."' />
		<input type='hidden' name='start' value='".$next."' />
		<a href='#$next' onclick=\"javascript:document.shownext.submit();\">"._SR_NEXT."</a></form></td>\n";
	}
	echo "</tr></table><p>\n";

	$hf = new XoopsHtmlForm();
	$modlist = array();
	$modlist = XoopsModule::getHasSearchModulesList();
	echo "<form action='search.php' method='post'>";
	$query = $myts->makeTboxData4PreviewInForm($query);
	echo $hf->input_text("query",$query);
	$andorlist = array("AND"=>_SR_ALL, "OR"=>_SR_ANY, "exact"=>_SR_EXACT);
	echo $hf->select("andor", $andorlist, $andor);
	echo $hf->input_submit(_SR_SEARCH)."<br />"; 
	echo $hf->input_check_radio("mids", $modlist, $mid, TRUE);
	echo $hf->input_hidden("action","results");	
	echo "</form></p>\n";

	CloseTable();
}
	
include($xoopsConfig['root_path']."footer.php");

?>