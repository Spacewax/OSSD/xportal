xPortal
=======

A portal or Content management system (cms) based on XOOPS rc-1 and modded into a modern system.

Plans are to take the original system as a whole and

 1. make everything work on php7+
 2. rework into a *CORE* system - keeping only general purpose stuff.
 3. keep *ORIGINAL* modules updated to new *CORE* but seperated.
 4. online updates through the *ADMIN AREA*

## License(s)

The original work, ***XOOPS rc-1***, is [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) Licensed. As it has the clause 
``either version 2 of the License, or (at your option) any later version.``

**xPortal** will be released under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt).
