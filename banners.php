<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

if (eregi("banners.php",$PHP_SELF)) {
	include("mainfile.php");
}

/********************************************/
/* Function to let your client login to see */
/* the stats                                */
/********************************************/
function clientlogin() {
	global $xoopsDB;
	include("header.php");
	OpenTable();
	echo "<form action='banners.php' method='post'>
	<table width='100%'><tr><td colspan='2' align='center' bgcolor='#fffacd'>
	<b>Advertising Statistics</b></td></tr>
	<tr><td align='right'><b>Login: </b></td>
		<td><input class='textbox' type='text' name='login' size='12' maxlength='10' /></td></tr>
	<tr><td align='right'><b>Password: </b></td>
		<td><input class='textbox' type='password' name='pass' size='12' maxlength='10' /></td></tr>
	<tr><td align='center' colspan='2'><input class='textbox' type='hidden' name='op' value='Ok' />
		<input type='submit' value='Login'></td></tr>
	<tr><td colspan='2' align='center' bgcolor='#eecfa1'>Please type your client information</td></tr>
	</table></form>";
	CloseTable();
	include("footer.php");
}

/*********************************************/
/* Function to display the banners stats for */
/* each client                               */
/*********************************************/
function bannerstats($login, $pass) {
	global $xoopsDB, $xoopsConfig;
	$result = $xoopsDB->query("SELECT cid, name, passwd FROM ".$xoopsDB->prefix("bannerclient")." WHERE login='$login'");
    list($cid, $name, $passwd) = $xoopsDB->fetch_row($result);
	
    if ( $login=="" AND $pass=="" OR $pass=="" ) {
		redirect_header("banners.php",2,"Login Incorrect!!!");
		//include("footer.php");
		exit();
    } else {
		if ( $pass==$passwd ) {
			include("header.php");
			OpenTable();
    		echo "<h4 style='text-align:center;'><b>Current Active Banners for $name</b><br /></h4>
			<table width='100%' border='0'><tr><td bgcolor='#887765' align='center'><b>ID</b></td>
				<td bgcolor='#887765' align='center'><b>Imp. Made</b></td>
				<td bgcolor='#887765' align='center'><b>Imp. Total</b></td>
				<td bgcolor='#887765' align='center'><b>Imp. Left</b></td>
				<td bgcolor='#887765' align='center'><b>Clicks</b></td>
				<td bgcolor='#887765' align='center'><b>% Clicks</b></td>
				<td bgcolor='#887765' align='center'><b>Functions</b></td><tr>";
			$result = $xoopsDB->query("select bid, imptotal, impmade, clicks, date from ".$xoopsDB->prefix("banner")." where cid=$cid");
    		while ( list($bid, $imptotal, $impmade, $clicks, $date) = $xoopsDB->fetch_row($result) ) {
				if ( $impmade == 0 ) {
					$percent = 0;
        		} else {
					$percent = substr(100 * $clicks / $impmade, 0, 5);
				}
				if ( $imptotal == 0 ) {
					$left = "Unlimited";
				} else {
					$left = $imptotal-$impmade;
				}
				echo "<td bgcolor='#aa9985' align='center'>$bid</td><td bgcolor='#aa9985' align='center'>$impmade</td>
				<td bgcolor='#aa9985' align='center'>$imptotal</td>
				<td bgcolor='#aa9985' align='center'>$left</td>
				<td bgcolor='#AA9985' align='center'>$clicks</td>
				<td bgcolor='#AA9985' align='center'>$percent%</td>
        		<td bgcolor='#AA9985' align='center'><a href='banners.php?op=EmailStats&amp;login=$login&amp;pass=$pass&amp;cid=$cid&amp;bid=$bid'>E-mail Stats</a></td><tr>";
			}
			echo "</table><br /><br /><div style='text-align:center;'>Following are your running Banners in ".$xoopsConfig['sitename']." </div><br /><br />";
			$result = $xoopsDB->query("select bid, imageurl, clickurl from ".$xoopsDB->prefix("banner")." where cid=$cid");
    		while ( list($bid, $imageurl, $clickurl) = $xoopsDB->fetch_row($result) ) {
				$numrows = $xoopsDB->num_rows($result);
				if ($numrows>1) {
					echo "<hr /><br />";
				}

				echo "<img src='$imageurl' alt='' style='border:1px;' /><br />
				Banner ID: $bid<br />
				Send <a href='banners.php?op=EmailStats&amp;login=$login&amp;cid=$cid&amp;bid=$bid&amp;pass=$pass'>E-Mail Stats</a> for this Banner<br />
				This Banners points to <a href='$clickurl'>this URL</a><br />
				<form action='banners.php' method='submit'>
				Change URL: <input class='textbox' type='text' name='url' size='50' maxlength='200' value='$clickurl' />
				<input class='textbox' type='hidden' name='login' value='$login' />
				<input class='textbox' type='hidden' name='bid' value='$bid' />
				<input class='textbox' type='hidden' name='pass' value='$pass' />
				<input class='textbox' type='hidden' name='cid' value='$cid' />
				<input type='submit' name='op' value='Change' /></form>";
			}
    		CloseTable();

			/* Finnished Banners */
			echo "<br />";
			OpenTable();
			echo "<h4 style='text-align:center;'>Banners Finished for $name</h4><br />
			<table width='100%' border='0'><tr>
    		<td bgcolor='#887765' align='center'><b>ID</b></td>
    		<td bgcolor='#887765' align='center'><b>Impressions</b></td>
    		<td bgcolor='#887765' align='center'><b>Clicks</b></td>
    		<td bgcolor='#887765' align='center'><b>% Clicks</b></td>
    		<td bgcolor='#887765' align='center'><b>Start Date</b></td>
    		<td bgcolor='#887765' align='center'><b>End Date</b></td></tr>";
    		$result = $xoopsDB->query("select bid, impressions, clicks, datestart, dateend from ".$xoopsDB->prefix("bannerfinish")." where cid=$cid");
    		while ( list($bid, $impressions, $clicks, $datestart, $dateend) = $xoopsDB->fetch_row($result) ) {
				$percent = substr(100 * $clicks / $impressions, 0, 5);
				echo "<tr><td bgcolor='#AA9985' align='center'>$bid</td>
				<td bgcolor='#aa9985' align='center'>$impressions</td>
        		<td bgcolor='#aa9985' align='center'>$clicks</td>
				<td bgcolor='#aa9985' align='center'>$percent%</td>
        		<td bgcolor='#aa9985' align='center'>".formatTimestamp($datestart)."</td>
        		<td bgcolor='#aa9985' align='center'>".formatTimestamp($dateend)."</td></tr>";
			}
			CloseTable();
			include("footer.php");
		} else {
			redirect_header("banners.php",2,"Login Incorrect!!!");
			//include("footer.php");
			exit();
		}
	}
}

/*********************************************/
/* Function to let the client E-mail his     */
/* banner Stats                              */
/*********************************************/
function EmailStats($login, $cid, $bid, $pass) {
	global $xoopsDB, $xoopsConfig;
	$result2 = $xoopsDB->query("select name, email from ".$xoopsDB->prefix("bannerclient")." where cid=$cid");
    list($name, $email) = $xoopsDB->fetch_row($result2);
    if ( $email == "" ) {
		redirect_header("banners.php",3,"There isn't an email associated with client ".$name.".<br />Please contact the Administrator");
		//include("footer.php");
		exit();
	} else {
		$result = $xoopsDB->query("select bid, imptotal, impmade, clicks, imageurl, clickurl, date from ".$xoopsDB->prefix("banner")." where bid=$bid and cid=$cid");
		list($bid, $imptotal, $impmade, $clicks, $imageurl, $clickurl, $date) = $xoopsDB->fetch_row($result);
        if ( $impmade == 0 ) {
			$percent = 0;
        } else {
			$percent = substr(100 * $clicks / $impmade, 0, 5);
        }

        if ( $imptotal == 0 ) {
			$left = "Unlimited";
	    	$imptotal = "Unlimited";
        } else {
			$left = $imptotal-$impmade;
        }
		$fecha = date("F jS Y, h:iA.");
		$subject = "Your Banner Statistics at ".$xoopsConfig[sitename]."";
		$message = "Following are the complete stats for your advertising investment at ". $xoopsConfig['sitename']." :\n\n\nClient Name: $name\nBanner ID: $bid\nBanner Image: $imageurl\nBanner URL: $clickurl\n\nImpressions Purchased: $imptotal\nImpressions Made: $impmade\nImpressions Left: $left\nClicks Received: $clicks\nClicks Percent: $percent%\n\n\nReport Generated on: $fecha";
		$from = $xoopsConfig[sitename];
		mail($email, $subject, $message, "From: $from\nX-Mailer: PHP/" . phpversion());
		redirect_header("banners.php?op=Ok&amp;login=$login&amp;pass=$pass",3,"Statistics for your banner has been sent to your email address.");
		//include("footer.php");
		exit();
	}
}

/*********************************************/
/* Function to let the client to change the  */
/* url for his banner                        */
/*********************************************/
function change_banner_url_by_client($login, $pass, $cid, $bid, $url) {
	global $xoopsDB;
    $result = $xoopsDB->query("select passwd from ".$xoopsDB->prefix("bannerclient")." where cid=".$cid."");
    list($passwd) = $xoopsDB->fetch_row($result);
    if ( $pass == $passwd ) {
		$xoopsDB->query("update ".$xoopsDB->prefix("banner")." set clickurl='".$url."' where bid=".$bid."");
    }
	redirect_header("banners.php?op=Ok&amp;login=$login&amp;pass=$pass",3,"URL has been changed.");
	//include("footer.php");
	exit();
}

function clickbanner($bid){
	global $xoopsDB;
	$bresult = $xoopsDB->query("select clickurl from ".$xoopsDB->prefix("banner")." where bid=$bid");
	list($clickurl) = $xoopsDB->fetch_row($bresult);
    $xoopsDB->query("update ".$xoopsDB->prefix("banner")." set clicks=clicks+1 where bid=$bid");
	echo "<html><head><meta http-equiv='Refresh' content='0; URL=".$clickurl."'></head><body></body></html>";
	exit();
}

switch ( $op ) {
	case "click":
		clickbanner($bid);
		break;
   	case "login":
		clientlogin();
		break;
   	case "Ok":
		bannerstats($login, $pass);
		break;
   	case "Change":
		change_banner_url_by_client($login, $pass, $cid, $bid, $url);
		break;
   	case "EmailStats":
		EmailStats($login, $cid, $bid, $pass);
		break;
   	default:
		clientlogin();
		break;
}

?>