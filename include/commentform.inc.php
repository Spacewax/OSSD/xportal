<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

include_once($xoopsConfig['root_path']."class/xoopslists.php");
if ( !isset($submit_page) ) {
	$submit_page = $PHP_SELF;
}

echo "<table><tr><td><form action='$submit_page' method='post' name='coolsus' onsubmit='return validate(this)'><br /><br /><b>". _YOURNAME .":</b>";

if ( $xoopsUser ) {
	echo "<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$xoopsUser->uid()."'>".$xoopsUser->uname()."</a>&nbsp;[&nbsp;<a href='".$xoopsConfig['xoops_url']."/user.php?op=logout'>"._LOGOUT."</a>&nbsp;]";
} else {
	echo "<b>".$xoopsConfig['anonymous']."</b>";
	echo " [ <a href='".$xoopsConfig['xoops_url']."/register.php'>"._REGISTER."</a> ] ";
}

echo "<br /><br /><b>". _SUBJECT ."</b><br />";

if ( !eregi("^re:",$subject) ) {
	$subject = "Re: ".substr($subject,0,56);
}
echo "<input type='text' name='subject' size='50' maxlength='60' value='$subject' /><br /><br /><b>". _MESSAGEICON ."</b><table><tr align='left'><td>";
$lists = new XoopsLists;
$filelist = $lists->getSubjectsList();
$count = 1;
while ( list ($key, $file) = each ($filelist) ){
	$checked = "";
	if ( isset($icon) && $file==$icon ) {
		$checked = " checked='checked'";
	}
	echo "<input type='radio' value='$file' name='icon'$checked />&nbsp;";
	echo "<img src='".$xoopsConfig['xoops_url']."/images/subject/".$file."' border='0' alt='' />&nbsp;";
	if ( $count == 8 ){ 
		echo "<br />"; 
		$count = 0; 
	}
	$count++;
}
echo "</td></tr></table><br /><b>". _COMMENT ."</b><br /><textarea wrap='virtual' cols='50' rows='10' name='message'>$message</textarea><br />";
putitems();
echo "&nbsp;[<a href='javascript:openWithSelfMain(\"".$xoopsConfig['xoops_url']."/misc.php?action=showpopups&amp;type=smilies\",\"smilies\",300,400)'>"._MORE."</a>]";
echo "<br /><br /><a href='javascript:checklength(document.coolsus);'>". _CHKMSGLENGTH ."</a><br /><br />". _ALLOWEDHTML ."<br />";
echo get_allowed_html();
echo "<br />";
if ( $xoopsUser && $xoopsConfig['anonpost'] ) { 
	echo "<br /><input type='checkbox' name='noname'";
	if ( $noname ) {
		echo " checked='checked'";
	}
	echo " /> "._POSTANON.""; 
}

echo "<br /><input type='checkbox' name='nosmiley' value='1'";
if ( $nosmiley ) {
	echo " checked='checked'";
}
echo " />". _DISABLESMILEY ."<br /><input type='checkbox' name='nohtml' value='1'";
if ( $nohtml ) {
	echo " checked='checked'";
}
echo " />". _DISABLEHTML ."<br /><input type='hidden' name='pid' value='$pid' />
<input type='hidden' name='tid' value='$tid' />
<input type='hidden' name='itemid' value='$itemid' />
<input type='hidden' name='mode' value='$mode' />
<input type='hidden' name='order' value='$order' />
<select name='op'><option value='preview' selected='selected'>". _PREVIEW ."</option>
<option value='post'>". _POSTCOMMENT ."</option>
</select>
<input type='submit' value='". _GO ."' />
</form>
</td></tr></table>";
unset($submit_page);

function putitems() {
	global $xoopsConfig;

	$smileyPath = "images/smilies";
	
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_smile.gif' alt=':-)' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-( ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_frown.gif' alt=':-(' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-D ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_biggrin.gif' alt=':-D' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' ;-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_wink.gif' alt=';-)' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-o ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_eek.gif' alt=':-o' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' 8-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_cool.gif' alt='8-)' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-? ');\"><img width='15' height='22' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_confused.gif' alt=':-?' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-P ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_razz.gif' alt=':-P' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-x ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_mad.gif' alt=':-x' /></a>";
}
?>