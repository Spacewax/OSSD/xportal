<?php
######################################################################
# Database Config
#
# $xoopsConfig['database']: Database you are using (currently only mysql)
# $xoopsConfig['prefix']:      Prefix for database tables
# $xoopsConfig['dbhost']:   Database Hostname
# $xoopsConfig['dbuname']:  Database Username
# $xoopsConfig['dbpass']:   Database Password
# $xoopsConfig['dbname']:   Database Name
# $xoopsConfig['db_pconnect']:  Use persistent connection  Default = 1
#
# $xoopsConfig[nuke_url]:  Virtual path to your main directory (Do not put / at end) (ex. http://yourdomain.com)
#
#
# $xoopsConfig['root_path']: Physical path to your main directory set in mainfile.php
######################################################################

// Database Configuration
$xoopsConfig['database'] = "mysql";
$xoopsConfig['prefix'] = "";
$xoopsConfig['dbhost'] = "localhost";
$xoopsConfig['dbuname'] = "";
$xoopsConfig['dbpass'] = "";
$xoopsConfig['dbname'] = "";
$xoopsConfig['db_pconnect'] = 1;

// Site Configuration
$xoopsConfig['xoops_url'] = "http://yourdomain";

// DO NOT CHANGE THE FOLLOWINGS
$xoopsConfig['root_path'] = XOOPS_ROOT_PATH."/";
$xoopsConfig['version'] = "XOOPS 1.0 RC1";
?>