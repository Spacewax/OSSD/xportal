<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

function themeuserpost($tid,$uid,$subject,$comment,$date,$hostname="",$image="",$mode="",$order="") {
	global $xoopsConfig, $xoopsTheme, $xoopsUser, $PHP_SELF;
	
	$allowpost = 0;
	$allowedit = 0;
	$allowhost = 0;
	if ( $xoopsUser ) {
		$myuid = $xoopsUser->uid();
		$allowpost = 1;
		if ( $xoopsUser->is_admin() ) {
			$allowhost = 1;
		}
	}

	echo "<a name='$tid'></a><table border='0' cellpadding='1' cellpadding='0' align='center' valign='top' width='100%'><tr><td><table border='0' cellpadding='3' cellpadding='1' width='100%'><tr bgcolor='". $xoopsTheme['bgcolor2'] ."' align='left'><td width='20%'><b>". _POSTER ."</b></td><td><b>$subject</b></td></tr><tr bgcolor='". $xoopsTheme['bgcolor3'] ."' align='left'>";
	if ( $uid != 0 ) {
		$poster = new XoopsUser($uid);
		if ( $myuid ) {
			if ( $poster->uid() == $myuid ) {
				$allowedit = 1;
			}
		}
		if ( !$poster->is_active() ) {
			$poster = 0;
		}
	} else {
		$poster = 0;
	}
	if ( $poster ) {
		$rank = $poster->rank();
   		echo "<td width='18%' valign='top'><b>".$poster->uname()."</b><br /><small>". $rank['title']."";
		if ( $rank['image'] != "" ) {
			echo "<br /><img src='".$xoopsConfig['xoops_url']."/images/ranks/".$rank['image']."' alt='' />";
		}
		if ( $poster->user_avatar() != "" ) {
			echo "<br /><img src='".$xoopsConfig['xoops_url']."/images/avatar/".$poster->user_avatar()."' alt='' />";
		}
		echo "<br /><br />"._JOINED."";
		echo formatTimestamp($poster->user_regdate(),"s");
		echo "\n";
		echo "<br />"._POSTS."".$poster->posts()."<br />\n";
		echo ""._FROM."".$poster->user_from()."<br /></small>\n";
	} else {
		echo "<td width='18%' valign='top'><b>".$xoopsConfig['anonymous']."</b><br />\n";
	}
	echo "</td><td valign='top'><table width='100%'><tr><td align='left'>";
	if ( $image != "" ) {
		echo "<img src='".$xoopsConfig['xoops_url']."/images/subject/$image' alt='' />";
	} else {
		echo "<img src='".$xoopsConfig['xoops_url']."/images/icons/posticon.gif' alt='' />";
	}
	echo "<small>&nbsp;&nbsp;"._POSTED." $date</small></td><td align='right'>";
	if ($xoopsConfig['anonpost'] || $allowpost) {
		echo "&nbsp;<a href='".$PHP_SELF."?op=reply&amp;tid=$tid&amp;mode=$mode&amp;order=$order'><img src='".$xoopsConfig['xoops_url']."/images/icons/reply.gif' alt='"._REPLY."' /></a>\n";
	}
  	if ( $allowedit || $allowhost ) {
		echo "&nbsp;<a href='".$PHP_SELF."?op=edit&amp;tid=$tid&amp;mode=$mode&amp;order=$order'><img src='".$xoopsConfig['xoops_url']."/images/icons/edit.gif' alt='"._EDITTHISPOST."' /></a>\n";
	}
	if ( $allowpost && $allowhost ) {
		echo "&nbsp;<a href='".$PHP_SELF."?op=delete&amp;tid=$tid&amp;mode=$mode&amp;order=$order'><img src='".$xoopsConfig['xoops_url']."/images/icons/delete.gif' alt='"._DELETETHISPOST."' /></a>\n";
	}
	if ( $allowhost ) {
		echo "<img src='".$xoopsConfig['xoops_url']."/images/icons/ip.gif' alt='IP' />\n";
	}
	echo "</td></tr></table><hr /><p>$comment</p><hr />";
   	if ( $poster ) {
		echo "&nbsp;<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$poster->uid()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/profile.gif' alt='"._PROFILE."' /></a>\n";
		if ( $allowpost ) {
			echo "&nbsp;<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?send2=1&theme=".$xoopsTheme['thename']."&to_userid=".$poster->uid()."','pmlite',360,300);\">";
			echo "<img src='".$xoopsConfig['xoops_url']."/images/icons/pm.gif' alt='";
			printf(_SENDPMTO,$poster->uname());
			echo "' />";
			echo "</a>\n";
		}
   		if ( $poster->user_viewemail() ) {
			echo "&nbsp;<a href='mailto:".$poster->email()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/email.gif' alt='";
			printf(_SENDEMAILTO,$poster->uname());
			echo "' /></a>\n";
		}
		$posterurl = $poster->url();
   		if ( $posterurl != "" ) {
			if ( strstr("http://", $posterurl) ) {
				$posterurl = "http://" . $posterurl;
			}
			echo "&nbsp;<a href='$posterurl'><img src='".$xoopsConfig['xoops_url']."/images/icons/www.gif' alt='"._VISITWEBSITE."' /></a>\n";
		}
   		if ( $poster->user_icq() != "" ) {
			//echo "&nbsp;<a href=\"http://wwp.icq.com/".$poster->user_icq()."#pager\" target=\"_blank\"><img src=\"http://online.mirabilis.com/scripts/online.dll?icq=".$poster->user_icq()."&img=5\" border=\"0\" alt=\"icq\" /></a>";
			echo "&nbsp;<a href='http://wwp.icq.com/scripts/search.dll?to=".$poster->user_icq()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/icq_add.gif' alt='"._ADDTOLIST."' /></a>\n";
		}
		if ( $poster->user_aim() != "" ) {
			echo "&nbsp;<a href='aim:goim?screenname=".$poster->user_aim()."&message=Hi+".$poster->user_aim()."+Are+you+there?'><img src='".$xoopsConfig['xoops_url']."/images/icons/aim.gif' alt='aim' /></a>\n";
		}
   		if ( $poster->user_yim() != "" ) {
			echo "&nbsp;<a href='http://edit.yahoo.com/config/send_webmesg?.target=".$poster->user_yim()."&.src=pg'><img src='".$xoopsConfig['xoops_url']."/images/icons/yim.gif' alt='yim' /></a>\n";
		}
		if ( $poster->user_msnm() != '' ) {
			echo "&nbsp;<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$poster->uid()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/msnm.gif' alt='msnm' /></a>\n";
		}
	}
	
	echo "</td></tr></table></td></tr></table>";
}

?>