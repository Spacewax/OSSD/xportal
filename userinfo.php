<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
$xoopsOption['pagetype'] = "user";
include("mainfile.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");

if ( !$uid && !$uname ) {
	redirect_header("index.php",3,_US_SELECTNG);
	exit();
}

if ( $xoopsUser ) {
	if ( $uid == $xoopsUser->uid() ) {
		include("header.php");
		OpenTable();
		echo "<div style='text-align: center;'>";
		echo "<h3>";
		printf(_US_WELCOME,$xoopsConfig['sitename']);
		echo "</h3><br />";
		echo "<h4>"._US_THISISYOURPAGE."</h4><br />";
		echo "<form name='usernav' action='user.php' method='post'>
		<table align='center' border='0'><tr><td><input type='button' class='button' value='"._US_EDITPROFILE."' onclick=\"location='edituser.php'\" /></td>
		<td><input type='button' class='button' value='"._US_INBOX."' onclick=\"location='viewpmsg.php'\" /></td>";

		if ( $xoopsConfig['self_delete'] ) {
			echo "<td><input type='button' class='button' value='"._US_DELACCOUNT."' onclick=\"location='user.php?op=delete'\" /></td>";
		}
		echo "<td><input type='button' class='button' value='"._US_LOGOUT."' onclick=\"location='user.php?op=logout'\" /></td>
		<td></td></tr></table></form><br /><br />\n\n";
		$thisUser = $xoopsUser;
	} else {
		$thisUser= new XoopsUser($uid);
		if ( !$thisUser->is_active() ) {
			redirect_header("index.php",3,_US_SELECTNG);
			exit();
		}
		include("header.php");
		OpenTable();
		echo "<div style='text-align: center;'>";
		echo "<h4>";
		printf(_US_PROFILEFOR,$thisUser->uname());
		echo "</h4><br />";
	}
} else {
	$thisUser = new XoopsUser($uid);
	if ( !$thisUser->is_active() ) {
		redirect_header("index.php",3,_US_SELECTNG);
		exit();
	}
	include("header.php");
	OpenTable();
	echo "<div style='text-align: center;'>";
	echo "<h4>";
	printf(_US_PROFILEFOR,$thisUser->uname());
	echo "</h4><br />";
}
$myts = new MyTextSanitizer;
if ( $thisUser->level() > 0 ) {
	echo "<table width='100%' border='0'><tr valign='top'><td width='50%'><table border='0' cellpadding='1' cellspacing='0' align='center' valign='top' width='80%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'><table border='0' cellpadding='3' cellspacing='1' width='100%'><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td colspan='2' align='center'><b>".sprintf(_US_ALLABOUT,$thisUser->uname())."</b></font></td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_AVATAR.":</b></td><td align='center'><img src='images/avatar/".$thisUser->user_avatar()."'></td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_REALNAME.":</b></td><td align='center'>".$thisUser->name()."</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_WEBSITE.":</b></td><td>";

	if ( $thisUser->url() ) {
		echo "<a href='".$thisUser->url()."'>".$thisUser->url()."</a>\n";
	} else {
		echo "&nbsp;";
	}
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_EMAIL.":</b></td><td>"; 
	if ( $thisUser->user_viewemail() ) { 
		 echo "<a href='mailto:".$thisUser->email()."'>".$thisUser->email()."</a><br />\n"; 
	} else {
		if ( $xoopsUser ) {
			if ( $xoopsUser->is_admin() || ($xoopsUser->uid() == $thisUser->uid()) ) {
				echo "<a href='mailto:".$thisUser->email()."'>".$thisUser->email()."</a><br />\n"; 
			}
		}
		echo "&nbsp;";
	}
	echo "</td></tr>";
	echo "<tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_PM.":</b></td><td>";
	if ( $xoopsUser ) {
		echo "<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?send2=1&theme=".$xoopsTheme['thename']."&to_userid=".$thisUser->uid()."','pmlite',360,290);\"><img src='".$xoopsConfig['xoops_url']."/images/icons/pm.gif' alt='";
		printf(_SENDPMTO,$thisUser->uname());
		echo "' />";
		echo "</a></center>\n";
	}
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_ICQ.":</b></td><td>";
	echo $thisUser->user_icq();
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_AIM.":</b></td><td>";
	echo $thisUser->user_aim();
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_YIM.":</b></td><td>";
	echo $thisUser->user_yim();
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_MSNM.":</b></td><td>";
	echo $thisUser->user_msnm();
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_LOCATION.":</b></td><td>";
	echo $thisUser->user_from();
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_OCCUPATION.":</b></td><td>";
	echo $thisUser->user_occ();
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_INTEREST.":</b></td><td>";
	echo $thisUser->user_intrest();
	echo "</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_EXTRAINFO.":</b></td><td>";
	echo $thisUser->bio();
	echo "</td></tr></table></td></tr></table></td>";
	echo "<td width='50%'><table border='0' cellpadding='1' cellspacing='0' align='center' valign='top' width='80%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'><table border='0' cellpadding='3' cellspacing='1' width='100%'><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td colspan='2' align='center'><b>"._US_STATISTICS."</b></td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_MEMBERSINCE.":</b></td><td align='center'>".formatTimestamp($thisUser->user_regdate(),"s")."</td></tr>\n";
	$userrank = $thisUser->rank();
	echo "<tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_RANK.":</b></td><td align='center'>";
	if ( $userrank['image'] ) {
		echo "<img src='".$xoopsConfig['xoops_url']."/images/ranks/".$userrank['image']."' alt='' /><br />";
	}
	echo $userrank['title']."</td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td><b>"._US_POSTS.":</b></td><td align='center'>".$thisUser->posts()."</td></tr>\n";
	if ( $xoopsUser ) {
		if ( $xoopsUser->uid() != $thisUser->uid() ) {
			$date = $thisUser->lastVisit();
			if ( $date ) {
				echo "<tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_LASTVISIT.":</b></td><td align='center'>".formatTimestamp($date,"m")."</td></tr>\n";
			}
		}
	} else {
		$date = $thisUser->lastVisit();
		if ( $date ) {
			echo "<tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td><b>"._US_LASTVISIT.":</b></td><td align='center'>".formatTimestamp($date,"m")."</td></tr>\n";
		}
	}
	echo "</table></td></tr></table><br /><table border='0' cellpadding='1' cellspacing='0' align='center' valign='top' width='80%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'><table border='0' cellpadding='3' cellspacing='1' width='100%'><tr valign='top' bgcolor='".$xoopsTheme["bgcolor1"]."'><td colspan='2' align='center'><b>"._US_SIGNATURE."</b></td></tr><tr valign='top' bgcolor='".$xoopsTheme["bgcolor3"]."'><td>".$thisUser->user_sig()."</td></tr></table></td></tr></table></td></tr></table></div>";
} else {
	echo "<div style='text-align: center;'>";
	printf(_US_INACTIVE,$uid);
	echo "</div>";
}

CloseTable();
echo "<div style='text-align: center;'><br /><br />";
OpenTable();
$mids = XoopsModule::getHasSearchModulesList(false);
foreach($mids as $mid){
	$module = new XoopsModule($mid);
	$results = array();
	$results = $module->Search("", "", 5, 0, $thisUser->uid());
	echo "<p><h4>".$module->name()."</h4>";
	if ( !is_array($results) || !($count = count($results)) ) {
		echo _US_NODATA."</p>";
	} else {
		for ( $i=0;$i<$count;$i++ ) {
			if ( isset($results[$i]['image']) || $results[$i]['image'] != "" ) {
				echo "<img src='modules/".$module->dirname()."/".$results[$i]['image']."' alt='".$module->name()."' />&nbsp;";
			} else {
				echo "<img src='images/icons/posticon.gif' alt='".$module->name()."' width='14' height='11' />&nbsp;";
			}
			echo "<b><a href='modules/".$module->dirname()."/".$results[$i]['link']."'>".$myts->makeTboxData4Show($results[$i]['title'])."</a></b><br />\n";
			echo "<small>";
			echo $results[$i]['time'] ? " (". formatTimestamp($results[$i]['time']).")" : "";
			echo "</small><br />\n";
		}
/*		if ( $count == 5 ) {
			echo "<br /><form action='search.php' method='post' name='showall".$mid."'>
			<input type='hidden' value='".$query."' name='query' />
			<input type='hidden' name='mid' value='".$mid."' />
			<input type='hidden' name='action' value='showall' />
			<input type='hidden' name='andor' value='".$andor."' />
			<a href='#$mid' onclick=\"javascript:document.showall".$mid.".submit();\">"._SR_SHOWALLR."</a></form></p>";
		}*/
	}
}
CloseTable();
echo "</div>";
include("footer.php");
?>