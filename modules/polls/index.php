<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");

if(!isset($pollID)) {
 	if($xoopsConfig['startpage'] == "polls"){
		$xoopsOption['show_rblock'] =1;
		include($xoopsConfig['root_path']."header.php");
		make_cblock();
		echo "<br />";
	}else{
		$xoopsOption['show_rblock'] =0;
		include($xoopsConfig['root_path']."header.php");
	}
  	OpenTable();
  	pollList();
  	CloseTable();
  	include ($xoopsConfig['root_path']."footer.php");

} elseif(isset($op) && $op == "results" && $pollID > 0) {
	Header("Location: ".$xoopsConfig['xoops_url']."/modules/polls/pollresults.php?pollID=$pollID");
	$redirecturl = $xoopsConfig['xoops_url']."/modules/polls/pollresults.php?pollID=".$pollID."";
	echo "<html><head><meta http-equiv=\"Refresh\" content=\"0; URL=".$redirecturl."\"></meta></head><body></body></html>";
	exit();
} elseif(isset($voteID) && $voteID > 0) {
    	pollCollector($pollID, $voteID);
} elseif(isset($pollID) && $pollID) {
 	if($xoopsConfig['startpage'] == "polls"){
		$xoopsOption['show_rblock'] =1;
		include($xoopsConfig['root_path']."header.php");
		make_cblock();
		echo "<br />";
	}else{
		$xoopsOption['show_rblock'] =0;
		include($xoopsConfig['root_path']."header.php");
	}
  	OpenTable();
  	echo "<table align=\"center\" border=\"0\" width=\"200\"><tr><td>";
  	$content = "";
  	$content = pollShow($pollID);
  	echo $content;
  	echo "</td></tr></table>";
  	CloseTable();
  	include ($xoopsConfig['root_path']."footer.php");
} else {
 	if($xoopsConfig['startpage'] == "polls"){
		$xoopsOption['show_rblock'] =1;
		include($xoopsConfig['root_path']."header.php");
		make_cblock();
		echo "<br />";
	}else{
		$xoopsOption['show_rblock'] =0;
		include($xoopsConfig['root_path']."header.php");
	}
  	OpenTable();
  	pollList();
  	CloseTable();
  	include ($xoopsConfig['root_path']."footer.php");
}

function pollCollector($pollID, $voteID, $forwarder) {
        global $xoopsConfig, $HTTP_COOKIE_VARS, $xoopsDB;
        $voteValid = "1";
	// we have to check for cookies, so get timestamp of this poll
	$result = $xoopsDB->query("SELECT timeStamp FROM ".$xoopsDB->prefix("poll_desc")." WHERE pollID=$pollID");
	list($timeStamp) = $xoopsDB->fetch_row($result);
	$cookieName = "MyPoll".$timeStamp;
	// check if cookie exists
	if(isset($HTTP_COOKIE_VARS["$cookieName"]) && $HTTP_COOKIE_VARS["$cookieName"] == "1") {
		// cookie exists, invalidate this vote
		$warn = _PL_ALREADYVOTED;
		$voteValid = 0;
	} else {
		// cookie does not exist yet, set one now
		setcookie($cookieName, 1, time()+86400, "/",  "", 0);
	}
        // update database if the vote is valid
        if($voteValid>0) {
                $xoopsDB->query("UPDATE ".$xoopsDB->prefix("poll_data")." SET optionCount=optionCount+1 WHERE pollID=$pollID AND voteID=$voteID");
                $xoopsDB->query("UPDATE ".$xoopsDB->prefix("poll_desc")." SET voters=voters+1 WHERE pollID=$pollID");
		$url = $xoopsConfig['xoops_url']."/modules/polls/index.php?op=results&amp;pollID=".$pollID."";
		redirect_header($url,1,_PL_THANKSFORVOTE);
		exit();
        } else {
		$url = $xoopsConfig['xoops_url']."/modules/polls/index.php?op=results&amp;pollID=".$pollID."";
                redirect_header($url,1,$warn);
		exit();
        }
}

function pollList() {
        global $xoopsConfig, $xoopsDB;
        $result = $xoopsDB->query("SELECT pollID, pollTitle, timeStamp, voters FROM ".$xoopsDB->prefix("poll_desc")." ORDER BY timeStamp");
        $counter = 0;
	echo "<div align=\"center\"><h4>"._PL_PASTPOLLS."</h4></div>";
        echo "<table border=\"0\" cellpadding=\"8\"><tr><td>";
        while($object = $xoopsDB->fetch_array($result)) {
                $resultArray[$counter] = array($object['pollID'], $object['pollTitle'], $object['timeStamp'], $object['voters']);
                $counter++;
        }

        for ($count = 0; $count < count($resultArray); $count++) {
                $id = $resultArray[$count][0];
                $pollTitle = $resultArray[$count][1];
                $voters = $resultArray[$count][3];
                $result2 = $xoopsDB->query("SELECT SUM(optionCount) AS SUM FROM ".$xoopsDB->prefix("poll_data")." WHERE pollID=$id");
                list($sum) = $xoopsDB->fetch_row($result2);

                echo "&nbsp;&nbsp;<strong><big>&middot;</big></strong>&nbsp;<b><a href=\"".$xoopsConfig['xoops_url']."/modules/polls/index.php?pollID=$id\">$pollTitle</a></b>&nbsp;";
                echo "(<a href=\"".$xoopsConfig['xoops_url']."/modules/polls/index.php?op=results&amp;pollID=".$id."\">"._PL_RESULTS."</a> - ";
		if($sum==1){
			echo _PL_ONEVOTE;
		}else{
			printf(_PL_NUMVOTES,$sum);
		}
		echo ")<br /><br />\n";
        }
        echo "</td></tr></table>";
}



function pollShow($pollID) {
        global $xoopsConfig, $xoopsDB, $xoopsOption;
	$query = "SELECT pollTitle, voters FROM ".$xoopsDB->prefix("poll_desc")." WHERE pollID=".$pollID."";
	
        $result = $xoopsDB->query($query);
        list($pollTitle, $voters) = $xoopsDB->fetch_row($result);
	
        if(!isset($url))
	  
          $url = $xoopsConfig['xoops_url']."/modules/polls/index.php?op=results&amp;pollID=".$pollID."";
        $boxContent = "<form action=\"".$xoopsConfig['xoops_url']."/modules/polls/index.php\" method=\"post\">";
        $boxContent .= "<input type=\"hidden\" name=\"pollID\" value=\"".$pollID."\" />";
        $boxContent .= "<input type=\"hidden\" name=\"forwarder\" value=\"".$url."\" />";
        $boxTitle = _PL_POLL;
        $boxContent .= "<b>$pollTitle</b><br /><br />\n";
        for($i = 1; $i <= 10; $i++) {
                $result = $xoopsDB->query("SELECT pollID, optionText, optionCount, voteID FROM ".$xoopsDB->prefix("poll_data")." WHERE pollID=$pollID AND voteID=$i");
                $myrow = $xoopsDB->fetch_array($result);
                if($myrow['optionText'] != "") {
                	$boxContent .= "<input type=\"radio\" name=\"voteID\" value=\"".$i."\" />". $myrow['optionText']. "<br />\n";
                        
                }
        }
        $boxContent .= "<br /><table align=\"center\" cellspacing=\"0\" cellpadding=\"5\" border=\"0\" width=\"111\"><tr>";
        $boxContent .= "<td align=\"center\"> <input type=\"submit\" class=\"button\" value=\""._PL_VOTE."\" /></td>";
        $boxContent .= "</tr><tr>";
        $boxContent .= "<td align=\"center\">\n";

        $boxContent .= "<input type=\"button\" value=\""._PL_RESULTS."\" class=\"button\" onclick=\"location='".$xoopsConfig['xoops_url']."/modules/polls/index.php?op=results&pollID=".$pollID."'\" /></td></tr></table></form>";
	$boxContent .= "<div align=\"center\"><small><b><a href=\"".$xoopsConfig['xoops_url']."/modules/polls/index.php\">"._PL_PASTPOLLS."</a></b></small><br />\n";
	list($numcom) = $xoopsDB->fetch_row($xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("pollcomments")." WHERE pollID=$pollID"));
	$boxContent .= "<br />"._PL_VOTES." <b>$voters</b><br />"._PL_COMMENTS." <b>$numcom</b></div>\n";
        $boxContent .= "</div>\n";
        return $boxContent;
}
?>