<?php
include("admin_header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopscomments.php");
/*********************************************************/
/* Poll/Surveys Functions                                */
/*********************************************************/

function poll_listPoll(){
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
	$result = $xoopsDB->query("SELECT pollID, pollTitle, timeStamp, display from ".$xoopsDB->prefix("poll_desc")." ORDER BY timestamp DESC");
	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	echo "<h4>"._AM_POLLCONFIG."</h4>";
	echo "<a href=index.php?op=create>"._AM_CREATNEWPOLL."</a><br /><br />";
	echo "<table>";
	echo "<tr><td><b>"._AM_POLLTITLE."</b></td><td><b>"._AM_ACTION."</b></td><td align=\"right\"><b>"._AM_DISPLAY."</b></td></tr>";
	$flag = 0;
	while($myrow=$xoopsDB->fetch_array($result)){
		$title = $myts->makeTboxData4Show($myrow['pollTitle']);
		echo "<tr><td>$title</td>";
		echo "<td><a href=index.php?op=poll_edit&pollID=".$myrow['pollID'].">"._AM_EDIT."</a>";
		echo "&nbsp;";
		echo "<a href=index.php?op=remove&pollID=".$myrow['pollID'].">"._AM_DELETE."</a></td>";
		echo "<td align=\"right\">";
		if($myrow['display']==1){
			echo _AM_YES; 
		}else{
			echo _AM_NO;
		}
		"</td>";
		echo "</tr>";
	}
	echo "</table>";
	CloseTable();
}

function poll_createPoll() {
	global $xoopsConfig, $xoopsDB, $xoopsModule;
	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	?>
	<h4><?php echo _AM_CREATNEWPOLL; ?></h4>
<?php 
	echo "<form action=\"index.php\" method=\"post\">";
?>
	<input type="hidden" name="op" value="createPosted">
	<p><?php echo _AM_POLLTITLE; ?>:&nbsp;&nbsp;<input class=textbox type=text name="pollTitle" size=50 maxlength=100></p>
	<p><?php echo _AM_PEEAOIASF; ?></p>
	<table>
	<?PHP
	for($i = 1; $i <= 10; $i++)
	{
		echo "<tr>";
		echo "<td>"._AM_OPTION." $i:</td><td><input class=textbox type=text name=\"optionText[$i]\" size=50 maxlength=50></td>";
		echo "</tr>";
	}
	echo "</table><br />";
	echo "<input type=\"checkbox\" name=\"display\" class=\"textbox\" value=\"1\" checked=\"checked\" />&nbsp;"._AM_DISPLAY."&nbsp;";
	echo "<br /><br />";
	echo "<input type=\"submit\" value=\""._AM_CREATE."\">";
	echo "&nbsp;";
	echo "<input type=\"button\" value=\""._AM_CANCEL."\" onClick=\"history.go(-1)\" />";
	echo "</form>";
	CloseTable();
}

function poll_editPoll($pollID) {
	global $xoopsConfig, $xoopsDB, $xoopsModule;
	$myts = new MyTextSanitizer;
	$result = $xoopsDB->query("SELECT pollTitle, display FROM ".$xoopsDB->prefix("poll_desc")." where pollID=".$pollID."");
	list($title, $display) = $xoopsDB->fetch_row($result);
	$title = $myts->makeTboxData4Edit($title);
	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	echo "<h4>"._AM_EDITPOLL."</h4>";
	echo "<form action=\"index.php\" method=\"post\">";
	echo "<input type=\"hidden\" name=\"pollID\" value=\"".$pollID."\" />";
	echo "<input type=\"hidden\" name=\"op\" value=\"poll_editPosted\" />";
	echo "<p>"._AM_POLLTITLE.":&nbsp;&nbsp;<input class=textbox type=text name=\"pollTitle\" value=\"$title\" size=\"50\" maxlength=\"100\"></p>";
	echo "<p>"._AM_PEEAOIASF."</p>";
	echo "<table>";
	$result = $xoopsDB->query("SELECT optionText, voteID FROM ".$xoopsDB->prefix("poll_data")." WHERE pollID=".$pollID."");
	$i=1;
	while(list($option, $voteID)=$xoopsDB->fetch_row($result)){
		$option = $myts->makeTboxData4Edit($option);
		echo "<tr>";
		echo "<td>";
		printf(_AM_OPTION %s,$i);
		echo "</td>";
		echo "<td><input class=\"textbox\" type=\"text\" name=\"optionText[$i]\" value=\"$option\" size=\"50\" maxlength=\"50\" /></td>";
		echo "<input type=\"hidden\" name=\"voteID[$i]\" value=\"".$voteID."\" />";
	
		echo "</tr>";
		$i++;
	}
	echo "</table><br />";

	echo "<input type=\"checkbox\" name=\"display\" class=\"textbox\" value=\"1\"";
	if($display==1){
		echo " checked=\"checked\"";
	}
	echo " />&nbsp;"._AM_DISPLAY."<br /><br />";
	echo "<input type=\"submit\" value=\""._AM_EDIT."\" />";
	echo "&nbsp;";
	echo "<input type=\"button\" value=\""._AM_CANCEL."\" onClick=\"history.go(-1)\" />";
	echo "</form>";
	CloseTable();
}



function poll_createPosted() {
	global $xoopsConfig, $HTTP_POST_VARS, $xoopsDB;
	$myts = new MyTextSanitizer;
	$optionText = $HTTP_POST_VARS['optionText'];
	$display = $HTTP_POST_VARS['display'];
	if($display != 1){
		$display = 0;
	}
	$timeStamp = time();
	$pollTitle = $myts->makeTboxData4Save($HTTP_POST_VARS['pollTitle']);
	$newid = $xoopsDB->GenID("polls_desc_pollID_seq");
	if(!$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("poll_desc")." (pollID, pollTitle, timeStamp, voters, display) VALUES ($newid, '$pollTitle', $timeStamp, 0, $display)",1)) {
		return;
	}
	if($newid == 0){
		$newid = $xoopsDB->Insert_ID();
	}
	list($id) = $xoopsDB->fetch_row($xoopsDB->query("SELECT pollID FROM ".$xoopsDB->prefix("poll_desc")." WHERE pollTitle='$pollTitle'"));

	for($i = 1; $i <= sizeof($optionText); $i++) {
		if($optionText[$i] != "")
			$optionText[$i] = $myts->makeTboxData4Save($optionText[$i]);
			if(!$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("poll_data")." (pollID, optionText, optionCount, voteID) VALUES ($newid, '$optionText[$i]', 0, $i)",1)) {
				return;
			}
	}
	$content = polls_get();
	polls_write($content);
	redirect_header("index.php",1,_AM_DBUPDATED);
	exit();
}

function poll_editPosted() {
	global $xoopsConfig, $HTTP_POST_VARS, $xoopsDB, $xoopsTheme;
	$myts = new MyTextSanitizer;
	$pollID = $HTTP_POST_VARS['pollID'];
	$display = $HTTP_POST_VARS['display'];
	if($display != 1){
		$display = 0;
	}
	$voteID = $HTTP_POST_VARS['voteID'];
	$optionText = $HTTP_POST_VARS['optionText'];
	$timeStamp = time();
	$pollTitle = $myts->makeTboxData4Save($HTTP_POST_VARS['pollTitle']);
	$query = "UPDATE ".$xoopsDB->prefix("poll_desc")." SET pollTitle='".$pollTitle."', display=".$display." WHERE pollID=".$pollID."";
#	echo $query;
	if(!$xoopsDB->query($query,1)) {
		return;
	}
	
	for($i = 1; $i <= sizeof($optionText); $i++) {
		if($optionText[$i] != "")
			$optionText[$i] = $myts->makeTboxData4Save($optionText[$i]);
	$query = "update ".$xoopsDB->prefix("poll_data")." set optionText='$optionText[$i]' where pollID=".$pollID." and voteID=".$voteID[$i]."";
#	echo $query;
			if(!$xoopsDB->query($query,1)) {
				return;
			}
	}
	$content = polls_get();
	polls_write($content);
	redirect_header("index.php",1,_AM_DBUPDATED);
	exit();
}


function poll_removePoll($pollID) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	?>
	<h4><?php echo _AM_DELPOLL; ?></h4>
	<font color="#ff0000"><?php echo _AM_WTCPWBRIFD; ?></font>
	
<?php 
	echo "<form action=\"index.php\" method=\"post\">"; 
?>
	<input type="hidden" name="op" value="removePosted" />

	<?php
	$result = $xoopsDB->query("SELECT pollTitle FROM ".$xoopsDB->prefix("poll_desc")." WHERE pollID=".$pollID.""); 
	if(!$result) {
		return;
	}

	// cycle through the descriptions until everyone has been fetched
	list($title) = $xoopsDB->fetch_row($result);
	$title = $myts->makeTboxData4Show($title);
	echo "<b>".$title."</b><input type=\"hidden\" name=\"id\" value=\"".$pollID."\">";
	echo "<br /><br /><input type=\"submit\" value=\""._AM_REMOVE."\" />";
	echo "&nbsp;";
	echo "<input type=\"button\" value=\""._AM_CANCEL."\" onClick=\"history.go(-1)\" />";
	echo "</form></center>";
	CloseTable();
}

function poll_removePosted($id) {
	global $xoopsDB;
	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("poll_desc")." WHERE pollID=$id");
	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("poll_data")." WHERE pollID=$id");
	$commentsarray = array();
	$com = new XoopsComments($xoopsDB->prefix("pollcomments"),"pollID");
	$com->setItemId($id);
	$com->setOrder("tid DESC");
	$com->setParent(0);
	$commentsarray = $com->getTopComments();
	foreach($commentsarray as $comment){
		$comment->delete();
	}
	$content = polls_get();
	polls_write($content);
	redirect_header("index.php?op=adminMain",1,_AM_DBUPDATED);
	exit();
}

function polls_get() {
        global $xoopsConfig, $xoopsDB;

	$content = "<"."?php\n";
	$content .= "\$block['content']=\"";
	$myts = new MyTextSanitizer();
	$query = "SELECT pollId, pollTitle, voters FROM ".$xoopsDB->prefix("poll_desc")." WHERE display=1 ORDER BY timestamp DESC";
 	$result = $xoopsDB->query($query);
	$counter = 0;
        while($mypoll = $xoopsDB->fetch_array($result)){
		$content .= "<p><div align='center'><form action='".$xoopsConfig['xoops_url']."/modules/polls/index.php' method='post'>";
		$content .= "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='\".\$xoopsTheme[\"bgcolor2\"].\"'>\n";
		$content .= "<table width='100%' border='0' cellpadding='2' cellspacing='1'>\n";
		$content .= "<tr bgcolor='\".\$xoopsTheme[\"bgcolor3\"].\"'><td align='center' colspan='2'><input type='hidden' name='pollID' value='".$mypoll['pollId']."' />\n";
		$content .= "<b>".$mypoll['pollTitle']."</b></td></tr>\n";
		for($i = 1; $i <= 10; $i++) {
			$sql = "SELECT optionText, optionCount, voteID FROM ".$xoopsDB->prefix("poll_data")." WHERE (pollID=".$mypoll['pollId'].") AND (voteID=".$i.")";
                	$result2 = $xoopsDB->query($sql);
                	$myrow = $xoopsDB->fetch_array($result2);
                	if($myrow['optionText'] != "") {
                		$content .= "<tr bgcolor='\".\$xoopsTheme[\"bgcolor1\"].\"'><td align='center'><input type='radio' name='voteID' value='".$i."' /></td><td align='left'>". $myts->makeTboxData4Show($myrow['optionText']). "</td></tr>\n";
                        }
		}
        	$content .= "<tr bgcolor='\".\$xoopsTheme[\"bgcolor3\"].\"'><td align='center' colspan='2'><input type='submit' class='button' value='"._AM_VOTE."' />&nbsp;";
        	$content .= "<input type='button' value='"._AM_RESULTS."' class='button' onclick='location=\\\"".$xoopsConfig['xoops_url']."/modules/polls/index.php?op=results&pollID=".$mypoll['pollId']."\\\"' />";
        	$content .= "</td></tr></table></td></tr></table></form>\n";
        	$content .= "</div></p>\n";
	}
	$content .= "\";\n";
	$content .= "?".">";
        return $content;
}

function polls_write($content){
	global $xoopsConfig;
	$filename = $xoopsConfig['root_path']."modules/polls/cache/pollsblock.inc.php";
	$file = fopen($filename, "w");
	fwrite($file, $content);
	fclose($file);
}

switch($op) {

    	case "poll_listPoll":
		poll_listPoll();
		break;

	case "create":
		poll_createPoll();
		break;

	case "createPosted":
		poll_createPosted();
		break;

	case "poll_edit":
		poll_editPoll($pollID);
		break;

	case "poll_editPosted":
		poll_editPosted($pollID);
		break;

	case "ChangePoll":
		ChangePoll($pollID, $pollTitle, $optionText, $optionCount, $voteID);
		break;

	case "remove":
		poll_removePoll($pollID);
		break;

	case "removePosted":
		poll_removePosted($id);
		break;
	
	default:
		poll_listPoll();
		break;
}
include("admin_footer.php");
?>