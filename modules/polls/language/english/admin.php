<?php
//%%%%%%	Admin Module Name  Polls 	%%%%%
define("_AM_DBUPDATED","Database Updated Successfully!");

define("_AM_POLLCONFIG","Polls Configuration");
define("_AM_CREATNEWPOLL","Create new poll");
define("_AM_ACTION","Action");
define("_AM_DISPLAY","Display");
define("_AM_YES","Yes");
define("_AM_NO","No");
define("_AM_NONE","none");
define("_AM_EDIT","Edit");
define("_AM_DELETE","Delete");
define("_AM_POLLTITLE","Poll Title");
define("_AM_PEEAOIASF","Please enter each available option into a single field");
define("_AM_OPTION","Option");
define("_AM_CREATE","Create");
define("_AM_CANCEL","Cancel");
define("_AM_EDITPOLL","Edit Poll");
define("_AM_Option %s:","Option %s:");
define("_AM_DELPOLL","Delete Poll");
define("_AM_WTCPWBRIFD","WARNING: The chosen poll will be removed IMMEDIATELY from the database!");
define("_AM_REMOVE","Remove");

define("_AM_RESULTS","Results");
define("_AM_VOTE","Vote");
?>