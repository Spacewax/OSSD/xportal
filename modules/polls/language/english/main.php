<?php
//%%%%%%	File Name pollbooth.php 	%%%%%
define("_PL_CURRENTPOLL","Current Poll Voting Booth");
define("_PL_POSTCOMMENT","Post Comment");
define("_PL_RESULTS","Results");
define("_PL_ONEVOTE","1 vote");
define("_PL_NUMVOTES","%s votes");
define("_PL_TOTALVOTES","Total Votes:");
define("_PL_ALLOWONEVOTE","We allow just one vote per day");
define("_PL_VOTINGBOOTH","Voting Booth");
define("_PL_OTHERPOLLS","Other Polls");
define("_PL_POLL","Poll");
define("_PL_VOTE","Vote!");
define("_PL_PASTPOLLS","Past Polls");
define("_PL_VOTES","Votes:");
define("_PL_COMMENTS","Comments:");
define("_PL_THANKSFORVOTE","Thanks for your vote!");
define("_PL_ALREADYVOTED","You already voted today!");
//define("_PL_","");
//define("_PL_","");

//%%%%%%	File Name pollresults.php 	%%%%%
define("_PL_TOP","Top");
define("_PL_PARENT","Parent");
define("_PL_POSTERC","Poster: ");
define("_PL_DATEC","Date: ");
define("_PL_EDITNOTALLOWED","You're not allowed to edit this comment!");
define("_PL_ANONNOTALLOWED","Anonymous user not allowed to post.");
define("_PL_THANKSFORPOST","Thanks for your submission!");
define("_PL_DELNOTALLOWED","You're not allowed to delete this comment!");
define("_PL_GOBACK","Go Back");
define("_PL_AREYOUSURE","Are you sure you want to delete this comment and all its child comments?");
define("_PL_YES","Yes");
define("_PL_NO","No");
define("_PL_COMMENTSDEL","Comment(s) Deleted Successfully!");
//define("_PL_","");
?>