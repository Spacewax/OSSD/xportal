<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
include("header.php");
include_once($xoopsConfig['root_path']."class/xoopscomments.php");
include($xoopsConfig['root_path']."include/themeuserpost.php");

$enablepollcomm = 1; // Set this to 0 if you want to disable comments in polls

if ( !$enablepollcomm ) {
	include($xoopsConfig['root_path']."header.php");
	echo "<table align=\"center\" cellpadding=\"0\" cellspacing=\"2\" border=\"0\" bgcolor=\"".$xoopsTheme['bgcolor2']."\"><tr><td colspan=\"2\">";
    	echo "<table cellpadding=\"5\" cellspacing=\"2\" border=\"0\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><tr><td align=\"center\">";
    	pollResults($pollID);
    	echo "</td></tr></table></td></tr></table><br /><br />";
	include($xoopsConfig['root_path']."footer.php");
	exit();
}


$itemname = "pollID";

switch($op) {
	default:
		include($xoopsConfig['root_path']."header.php");
		if ( isset($tid) && $tid != "" ){
			$pollcomment = new XoopsComments($xoopsDB->prefix("pollcomments"),$itemname,$tid);
		} else {
			$pollcomment = new XoopsComments($xoopsDB->prefix("pollcomments"),$itemname);
			// must do these because $itemid is not set if no $tid
			$pollcomment->setItemId($pollID);
		}
		if ( !$pollcomment->parent() ){
			OpenTable();
			echo "<table align=\"center\" cellpadding=\"0\" cellspacing=\"2\" border=\"0\" bgcolor=\"".$xoopsTheme['bgcolor2']."\"><tr><td colspan=\"2\">";
    echo "<table cellpadding=\"5\" cellspacing=\"2\" border=\"0\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><tr><td align=\"center\">";
    			pollResults($pollID);
    			echo "</td></tr></table></td></tr></table><br /><br />";
    			CloseTable();
			echo "<br />";
		}
		if ( $xoopsUser ) {
			if ( !isset($order) ) {
				$order = $xoopsUser->uorder();
			}
			if ( !isset($mode) || $mode=="" ) {
				$mode = $xoopsUser->umode();
			}
		}
		$commentsArray = $pollcomment->getTopPageComments($order, $mode);
		OpenTable();

		// print navbar if the page is on top of comments tree
		if ( !$pollcomment->parent() ) {
			$pollcomment->printNavBar($order, $mode);
		}

		if ( is_array($commentsArray) && count($commentsArray) ) {
		foreach($commentsArray as $ele){
				$subject = $ele->subject();
				$comment = $ele->comment();
			
				$postdate = formatTimestamp($ele->postdate());
				themeuserpost($ele->tid(),$ele->uid(),$subject,$comment,$postdate,$ele->hostname(),$ele->icon(),$mode,$order);
				//if the mode is thread, show thread tree
				if ( $mode=="thread" ) {
					echo "<table><tr><td width=\"30%\" valign=\"top\">";
					//if not in the top page, show links
					if ( $pollcomment->parent() ) {
						echo "[&nbsp;<a href=\"".$PHP_SELF."?pollID=".$pollID."&amp;mode=".$mode."&amp;order=".$order."\">"._PL_TOP."</a>&nbsp;|&nbsp;";
						echo "<a href=\"".$PHP_SELF."?pollID=".$pollID."&amp;tid=".$pollcomment->parent()."&amp;mode=".$mode."&amp;order=".$order."\">"._PL_PARENT."</a>&nbsp;]";
					}
					echo "</td><td valign=\"top\">";
					$treeArray = $pollcomment->getCommentTree($ele->tid());
					foreach($treeArray as $treeItem){
						$prefix = str_replace(".", "&nbsp;&nbsp;&nbsp;&nbsp;", $treeItem->prefix());
						$csubject = $treeItem->subject();
						$date = formatTimestamp($treeItem->postdate());
						$name = XoopsUser::get_uname_from_id($treeItem->uid());
						echo "".$prefix."<img src=\"".$xoopsConfig['xoops_url']."/images/icons/posticon.gif\" />&nbsp;<a href=\"".$PHP_SELF."?pollID=".$pollID."&amp;tid=".$treeItem->tid()."&amp;mode=".$mode."&amp;order=".$order."\">".$csubject."</a>&nbsp;"._POSTEDBY."&nbsp;".$name."&nbsp;(".$date.")<br />";
					}
					echo "</td></tr></table>";
				}
			}
		}
		CloseTable();
		break;
		
	case "new":
		$xoopsOption['forumpage'] = 1;
		include($xoopsConfig['root_path']."header.php");
		include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
		$myts = new MyTextsanitizer;
		$result = $xoopsDB->query("select pollID, pollTitle FROM ".$xoopsDB->prefix("poll_desc")." where pollID=".$pollID."");
		list($pollID, $pollTitle) = $xoopsDB->fetch_row($result);
		$subject=$myts->makeTboxData4Edit($pollTitle);
		$itemid = $pollID;
		$pid = 0;
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;
	case "reply":
		$xoopsOption['forumpage'] = 1;
		include($xoopsConfig['root_path']."header.php");
		$pollcomment = new XoopsComments($xoopsDB->prefix("pollcomments"),$itemname,$tid);
		$r_comment = $pollcomment->comment();
		$r_date = formatTimestamp($pollcomment->postdate());
		$r_name = XoopsUser::get_uname_from_id($pollcomment->uid());
		$r_content = _PL_POSTERC."".$r_name."&nbsp;"._PL_DATEC."".$r_date."<br /><br />";
		$r_content .= $r_comment;
		$r_subject=$pollcomment->subject();
		$subject=$pollcomment->subject("Edit");
		themecenterposts($r_subject,$r_content);
		$pid=$tid;
		unset($tid);
		$itemid=$pollcomment->itemid();
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;

	case "edit":
		$xoopsOption['forumpage'] = 1;
		include($xoopsConfig['root_path']."header.php");
		$pollcomment = new XoopsComments($xoopsDB->prefix("pollcomments"),$itemname,$tid);
		$nohtml = $pollcomment->nohtml();
		$nosmiley = $pollcomment->nosmiley();
		$icon = $pollcomment->icon();
		$itemid=$pollcomment->itemid();
		$subject=$pollcomment->subject("Edit");
		$message=$pollcomment->comment("Edit");
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;

	case "preview":
		$xoopsOption['forumpage'] = 1;
		include($xoopsConfig['root_path']."header.php");
		include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
		$myts = new MyTextsanitizer;
		$p_subject = $myts->makeTboxData4Preview($subject);
		if ( $nosmiley && $nohtml ) {
			$p_comment = $myts->makeTareaData4Preview($message,0);
		} elseif ( $nohtml ) {
			$p_comment = $myts->makeTareaData4Preview($message,0,1);
		} elseif ( $nosmiley ) {
			$p_comment = $myts->makeTareaData4Preview($message);
		} else {
			$p_comment = $myts->makeTareaData4Preview($message,1,1);
		}
		themecenterposts($p_subject,$p_comment);
		$subject = $myts->makeTboxData4PreviewInForm($subject);
		$message = $myts->makeTareaData4PreviewInForm($message);
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;
	case "post":
		if ( !empty($tid) ) {
			$pollcomment = new XoopsComments($xoopsDB->prefix(pollcomments),$itemname,$tid);
			$accesserror = 0;
			if ( $xoopsUser ) {
				if ( $xoopsuser->is_admin($xoopsModule->mid()) ) {
					if($pollcomment->uid() != $xoopsUser->uid()){
						$accesserror = 1;
					}
				}
			} else {
				$accesserror = 1;
			}
			if($accesserror == 1){
				redirect_header("pollresults.php?pollID=".$itemid."&amp;tid=".$tid."&amp;order=".$order."&amp;mode=".$mode."",1,_PL_EDITNOTALLOWED);
				exit();
			}
		} else {
			$pollcomment = new XoopsComments($xoopsDB->prefix(pollcomments),$itemname);
			$pollcomment->setParent($pid);
			$pollcomment->setItemId($itemid);
			$pollcomment->setHostname(getenv("REMOTE_ADDR"));
			if ( $xoopsUser ) {
				$uid = $xoopsUser->uid();
			} else {
				if ( $xoopsConfig['anonpost'] ) {
					$uid = 0;
				} else {
					redirect_header("pollresults.php?pollID=".$itemid."&amp;tid=".$tid."&amp;order=".$order."&amp;mode=".$mode."&amp;pid=".$pid."",1,_PL_ANONNOTALLOWED);
					exit();
				}
			}
			$pollcomment->setUid($uid);
		}
		$pollcomment->setSubject($subject);
		$pollcomment->setComment($message);
		$pollcomment->setNohtml($nohtml);
		$pollcomment->setNosmiley($nosmiley);
		$pollcomment->setIcon($icon);
		$pollcomment->store();
		redirect_header("pollresults.php?pollID=".$itemid."&amp;tid=".$tid."&amp;order=".$order."&amp;mode=".$mode."",2,_PL_THANKSFORPOST);
		exit();
		break;
	case "delete":
		if ( !$xoopsUser ) {
			include($xoopsConfig['root_path']."header.php");
			echo "<h4>"._PL_DELNOTALLOWED."</h4>";
			echo "<br />";
			echo "<a href=\"javascript:history.go(-1)\">"._PL_GOBACK."</a>";
			include($xoopsConfig['root_path']."footer.php");
			exit();
		} else {
			if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
				include($xoopsConfig['root_path']."header.php");
				echo "<h4>"._PL_DELNOTALLOWED."</h4>";
				echo "<br />";
				echo "<a href=\"javascript:history.go(-1)\">"._PL_GOBACK."</a>";
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
		}
		if ( !empty($ok) ) {
			if ( !empty($tid) ) {
				$pollcomment = new XoopsComments($xoopsDB->prefix(pollcomments),$itemname,$tid);
				$deleted = $pollcomment->delete();
				$itemid = $pollcomment->itemid();
			}
			redirect_header("pollresults.php?pollID=".$itemid."&amp;tid=".$tid."&amp;order=".$order."&amp;mode=".$mode."",2,_PL_COMMENTSDEL);
			exit();
		} else {
			include($xoopsConfig['root_path']."header.php");
			OpenTable();
			echo "<div align=\"center\">";
			echo "<h4><font color=\"#ff0000\">Are you sure you want to delete this comment and all its child comments?</font></h4>";
			echo "[&nbsp;<a href=\"pollresults.php?op=delete&amp;tid=".$tid."&amp;mode=".$mode."&amp;order=".$order."&amp;ok=1\">"._PL_YES."</a>&nbsp;|&nbsp;<a href=\"javascript:history.go(-1)\">"._PL_NO."</a>&nbsp;]";
			echo "</div>";
			CloseTable();
		}
		break;
		

}

include($xoopsConfig['root_path']."footer.php");


function pollResults($pollID) {
        global $xoopsConfig, $xoopsDB;
        if ( !isset($pollID) ) $pollID = 1;
        $result = $xoopsDB->query("SELECT pollTitle FROM ".$xoopsDB->prefix("poll_desc")." WHERE pollID=$pollID");
        list($holdtitle) = $xoopsDB->fetch_row($result);
        echo "<br /><b>".$holdtitle."</b><br /><br />";
        //$xoopsDB->free_result($result);
        $result = $xoopsDB->query("SELECT SUM(optionCount) AS SUM FROM ".$xoopsDB->prefix("poll_data")." WHERE pollID=$pollID");
        list($sum) = $xoopsDB->fetch_row($result);
        //$xoopsDB->free_result($result);
        echo "<table>";
        // cycle through all options
        for($i = 1; $i <= 10; $i++) {
                // select next vote option
                $result = $xoopsDB->query("SELECT pollID, optionText, optionCount, voteID FROM ".$xoopsDB->prefix("poll_data")." WHERE (pollID=$pollID) AND (voteID=$i)");
                $object = $xoopsDB->fetch_array($result);
                if ( is_array($object) ) {
                        $optionText = $object['optionText'];
                        $optionCount = $object['optionCount'];
                        if ( $optionText != "" ) {
                                echo "<td>";
                                echo "$optionText";
                                echo "</td>";
                                if($sum) {
                                        $percent = 100 * $optionCount / $sum;
                                } else {
                                        $percent = 0;
                                }
                                echo "<td>";
                                $percentInt = (int)$percent * 4 * 1 ;
                                $percent2 = (int)$percent;
                                if ( $percent > 0 ) {
                                        echo "<img src=\"images/leftbar.gif\" height=\"14\" width=\"7\" alt=\"\" />";
                                        echo "<img src=\"images/mainbar.gif\" height=\"14\" width=\"".$percentInt."\" alt=\"$percent2 %\" />";
                                        echo "<img src=\"images/rightbar.gif\" height=\"14\" width=\"7\" alt=\"\" />";
                                } else {
                                        echo "<img src=\"images/leftbar.gif\" height=\"14\" width=\"7\" alt=\"$percent2 %\" />";
                                        echo "<img src=\"images/mainbar.gif\" height=\"14\" width=\"3\" alt=\"$percent2 %\" />";
                                        echo "<img src=\"images/rightbar.gif\" height=\"14\" width=\"7\" alt=\"$percent2 %\" />";
                                }
                                printf(" %.2f %% (%d)", $percent, $optionCount);
                                echo "</td>";
                        }
                }
                echo "</tr>";
        }
        echo "</td></tr></table><br />";
        echo "<center><b>"._PL_TOTALVOTES." $sum</b><br />";
        if ( isset($setCookies) && $setCookies>0 ) {
            echo "<small>"._PL_ALLOWONEVOTE."</small><br /><br />";
        } else {
            echo "<br /><br />";
        }
        $booth = $pollID;
        echo("[ <a href=\"".$xoopsConfig['xoops_url']."/modules/polls/index.php?pollID=$booth\">"._PL_VOTINGBOOTH."</a> | ");
        echo("<a href=\"".$xoopsConfig['xoops_url']."/modules/polls/index.php\">"._PL_OTHERPOLLS."</a> ]");
        return(1);
}

?>