<?php
$modversion['name'] = _MI_POLLS_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_POLLS_DESC;
$modversion['author'] = "Francisco Burzi<br />( http://phpnuke.org/ )";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "polls.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "polls_slogo.jpg";
$modversion['dirname'] = "polls";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

//Blocks
$modversion['blocks'][1]['file'] = "polls.php";
$modversion['blocks'][1]['name'] = _MI_POLLS_BNAME1;
$modversion['blocks'][1]['description'] = "Shows unlimited number of polls/surveys";
$modversion['blocks'][1]['show_func'] = "b_polls_show";

// Menu
$modversion['hasMain'] = 1;
?>