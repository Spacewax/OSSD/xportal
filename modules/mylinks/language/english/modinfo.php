<?php
// Module Info

// The name of this module
define("_MI_MYLINKS_NAME","Web Links");

// A brief description of this module
define("_MI_MYLINKS_DESC","Creates a web links section where users can search/submit/rate various web sites.");

// Names of blocks for this module (Not all module has blocks)
define("_MI_MYLINKS_BNAME1","Recent Links");
define("_MI_MYLINKS_BNAME2","Top Links");

// Sub menu titles
define("_MI_MYLINKS_SMNAME1","Submit");
define("_MI_MYLINKS_SMNAME2","Popular");
define("_MI_MYLINKS_SMNAME3","Top Rated");
?>