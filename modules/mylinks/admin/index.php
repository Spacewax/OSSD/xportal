<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("admin_header.php");

include("../include/config.php");
include("../include/functions.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
$myts = new MyTextSanitizer;
$eh = new ErrorHandler;
$mytree = new XoopsTree($xoopsDB->prefix("mylinks_cat"),"cid","pid");

function mylinks() {
    	global $xoopsDB;

	OpenTable();
	// Temporarily 'homeless' links (to be revised in admin.php breakup)
    	$result = $xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_broken")."");
    	list($totalbrokenlinks) = $xoopsDB->fetch_row($result);
	if($totalbrokenlinks>0){
		$totalbrokenlinks = "<font color=\"#ff0000\"><b>$totalbrokenlinks</b></font>";
	}
    	$result2 = $xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_mod")."");
    	list($totalmodrequests) = $xoopsDB->fetch_row($result2);
	if($totalmodrequests>0){
		$totalmodrequests = "<font color=\"#ff0000\"><b>$totalmodrequests</b></font>";
	}
	$result3 = $xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_links")." where status=0");
    	list($totalnewlinks) = $xoopsDB->fetch_row($result3);
	if($totalnewlinks>0){
		$totalnewlinks = "<font color=\"#ff0000\"><b>$totalnewlinks</b></font>";
	}
	echo " - <a href=index.php?op=myLinksConfigAdmin>"._MD_GENERALSET."</a>";
	echo "<br><br>";
	echo " - <a href=index.php?op=linksConfigMenu>"._MD_ADDMODDELETE."</a>";
	echo "<br><br>";
	echo " - <a href=index.php?op=listNewLinks>"._MD_LINKSWAITING." ($totalnewlinks)</a>";
	echo "<br><br>";
	echo " - <a href=index.php?op=listBrokenLinks>"._MD_BROKENREPORTS." ($totalbrokenlinks)</a>";
	echo "<br><br>";
	echo " - <a href=index.php?op=listModReq>"._MD_MODREQUESTS." ($totalmodrequests)</a>";
	$result=$xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_links")." where status>0");
        list($numrows) = $xoopsDB->fetch_row($result);
	echo "<br><br><div align=\"center\">";
	printf(_MD_THEREARE,$numrows);	echo "</div>";
    	CloseTable();
}

function listNewLinks(){
	global $xoopsDB, $xoopsConfig, $myts, $eh, $mytree;
// List links waiting for validation
    	$result = $xoopsDB->query("select lid, cid, title, url, email, logourl, submitter from ".$xoopsDB->prefix("mylinks_links")." where status=0 order by date DESC");
    	$numrows = $xoopsDB->num_rows($result);
	OpenTable();
    	echo "<h4>"._MD_LINKSWAITING."&nbsp;($numrows)</h4><br>";
    	if ($numrows>0) {
    		while(list($lid, $cid, $title, $url, $email, $logourl, $submitterid) = $xoopsDB->fetch_row($result)) {
		$result2 = $xoopsDB->query("select description from ".$xoopsDB->prefix("mylinks_text")." where lid=$lid");
		list($description) = $xoopsDB->fetch_row($result2);
		$title = $myts->makeTboxData4Edit($title);
		$url = $myts->makeTboxData4Edit($url);
//		$url = urldecode($url);
		$email = $myts->makeTboxData4Edit($email);
//		$logourl = $myts->makeTboxData4Edit($logourl);
//		$logourl = urldecode($logourl);
		$description = $myts->makeTareaData4Edit($description);
		$submitter = XoopsUser::get_uname_from_id($submitterid);
        	echo "<form action=\"index.php\" method=post>\n";
        	echo "<table width=\"80%\">";
		echo "<tr><td align=\"right\" nowrap>"._MD_SUBMITTER."</td><td>\n";
		echo "<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$submitterid."\">$submitter</a>";
        	echo "</td></tr>\n";
        	echo "<tr><td align=\"right\" nowrap>"._MD_SITETITLE."</td><td>";
    		echo "<input type=\"text\" name=\"title\" size=\"50\" maxlength=\"100\" value=\"$title\">";
    		echo "</td></tr><tr><td align=\"right\" nowrap>"._MD_SITEURL."</td><td>";
        	echo "<input type=\"text\" name=\"url\" size=\"50\" maxlength=\"100\" value=\"$url\">";
		echo "&nbsp;[&nbsp;<a href=\"$url\" target=\"_blank\">"._MD_VISIT."</a>&nbsp;]";
        	echo "</td></tr>";
        	echo "<tr><td align=\"right\" nowrap>"._MD_CATEGORYC."</td><td>";
        	$mytree->makeMySelBox("title", "title", $cid);
        	echo "</td></tr>\n";
		echo "<tr><td align=\"right\" nowrap>"._MD_CONTACTEMAIL."</td><td>\n";
		echo "<input type=\"text\" name=\"email\" size=\"50\" maxlength=\"60\" value=\"$email\"></td></tr>\n";
	
		
        	echo "<tr><td align=\"right\" valign=\"top\" nowrap>"._MD_DESCRIPTIONC."</td><td>\n";
        	echo "<textarea name=description cols=\"60\" rows=\"5\">$description</textarea>\n";
        	echo "</td></tr>\n";
		echo "<tr><td align=\"right\" nowrap>"._MD_SHOTIMAGE."</td><td>\n";
		echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\"></td></tr>\n";
		$shotdir = "<b>".$xoopsConfig['xoops_url']."/modules/mylinks/images/shots/</b>";
		echo "<tr><td></td><td>";
		printf(_MD_SHOTMUST,$shotdir);
		echo "</td></tr>\n";
		echo "</table>\n";
		echo "<br><input type=\"hidden\" name=\"op\" value=\"approve\"></input>";
		echo "<input type=\"hidden\" name=\"lid\" value=\"$lid\"></input>";
    		echo "<input type=\"submit\" value=\""._MD_APPROVE."\">&nbsp;<input type=\"button\" value=\""._MD_DELETE."\" onclick=\"location='index.php?op=delNewLink&lid=$lid'\"></form><br><br>";
		}
    	}else{
		echo ""._MD_NOSUBMITTED."";
	}
	CloseTable();
}


function linksConfigMenu(){
	global $xoopsDB,$xoopsConfig, $myts, $eh, $mytree;
// Add a New Main Category
    	OpenTable();
   	echo "<form method=post action=index.php>\n";
    	echo "<h4>"._MD_ADDMAIN."</h4><br>"._MD_TITLEC."<input type=text name=title size=30 maxlength=50><br>";
	echo ""._MD_IMGURL."<br><input type=\"text\" name=\"imgurl\" size=\"100\" maxlength=\"150\" value=\"http://\"><br><br>";
	echo "<input type=hidden name=cid value=0>\n";
	echo "<input type=hidden name=op value=addCat>";
	echo "<input type=submit value="._MD_ADD."><br></form>";
    	CloseTable();
    	echo "<br>";
// Add a New Sub-Category
    	$result=$xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_cat")."");
	list($numrows)=$xoopsDB->fetch_row($result);
    	if ($numrows>0) {
		OpenTable();
    		echo "<form method=post action=index.php>";
    		echo "<h4>"._MD_ADDSUB."</h4><br />"._MD_TITLEC."<input type=text name=title size=30 maxlength=50>&nbsp;"._MD_IN."&nbsp;";
		$mytree->makeMySelBox("title", "title");
#		echo "<br>"._MD_IMGURL."<br><input type=\"text\" name=\"imgurl\" size=\"100\" maxlength=\"150\">\n";
    		echo "<input type=hidden name=op value=addCat><br><br>";
		echo "<input type=submit value="._MD_ADD."><br></form>";
		CloseTable();
		echo "<br>";
// If there is a category, add a New Link   
	
    		OpenTable();
    		echo "<form method=post action=index.php>\n";
    		echo "<h4>"._MD_ADDNEWLINK."</h4><br>\n";
    		echo "<table width=\"80%\"><tr>\n";
		echo "<td align=\"right\">"._MD_SITETITLE."</td><td>";
    		echo "<input type=text name=title size=50 maxlength=100>";
    		echo "</td></tr><tr><td align=\"right\" nowrap>"._MD_SITEURL."</td><td>";
        	echo "<input type=text name=url size=50 maxlength=100 value=\"http://\">";
        	echo "</td></tr>";
        	echo "<tr><td align=\"right\" nowrap>"._MD_CATEGORYC."</td><td>";
        	$mytree->makeMySelBox("title", "title");
        	echo "</td></tr><tr><td></td><td></td></tr>\n";
		echo "<tr><td align=\"right\" nowrap>"._MD_EMAILC."</td><td>\n";
		echo "<input type=text name=email size=50 maxlength=60></td></tr>\n";
        	echo "<tr><td align=\"right\" valign=\"top\" nowrap>"._MD_DESCRIPTIONC."</td><td>\n";
        	echo "<textarea name=description cols=60 rows=5></textarea>\n";
        	echo "</td></tr>\n";
		echo "<tr><td align=\"right\"nowrap>"._MD_SHOTIMAGE."</td><td>\n";
		echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\"></td></tr>\n";
		$shotdir = "<b>".$xoopsConfig['xoops_url']."/modules/mylinks/images/shots/</b>";
		echo "<tr><td></td><td>";
		printf(_MD_SHOTMUST,$shotdir);
		echo "</td></tr>\n";
        	echo "</table>\n<br>";
		echo  "<input type=\"hidden\" name=\"op\" value=\"addLink\"></input>";
		echo "<input type=\"submit\" class=\"button\" value=\""._MD_ADD."\"></input>\n";
    		echo "</form>";
		CloseTable();
		echo "<br>";
   	
// Modify Category
    		OpenTable();
    		echo "
    		</center><form method=post action=index.php>
    		<h4>"._MD_MODCAT."</h4><br>";
    		echo _MD_CATEGORYC;
    		$mytree->makeMySelBox("title", "title");
    		echo "<br><br>\n";
    		echo "<input type=hidden name=op value=modCat>\n";
    		echo "<input type=submit value="._MD_MODIFY.">\n";
		echo "</form>";
		CloseTable();
		echo "<br>";
    	}
// Modify Link
    	$result2 = $xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_links")."");
    	list($numrows2) = $xoopsDB->fetch_row($result2);
    	if ($numrows2>0) {
    		OpenTable();
    		echo "<form method=get action=\"index.php\">\n";
    		echo "<h4>"._MD_MODLINK."</h4><br>\n";
    		echo _MD_LINKID."<input type=text name=lid size=12 maxlength=11>\n";
		echo "<input type=hidden name=fct value=mylinks>\n";
    		echo "<input type=hidden name=op value=modLink><br><br>\n";
		echo "<input type=submit value="._MD_MODIFY."></form>\n";
		echo "</td></tr></table></td></tr></table>";
    	}
}

function modLink() {
   	global $xoopsDB, $HTTP_GET_VARS, $myts, $eh, $mytree, $xoopsConfig;
    	$lid = $HTTP_GET_VARS['lid'];
    	OpenTable();
    	$result = $xoopsDB->query("select cid, title, url, email, logourl from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid") or $eh->show("0013");
    	echo "<h4>"._MD_MODLINK."</h4><br>";
    	list($cid, $title, $url, $email, $logourl) = $xoopsDB->fetch_row($result); 
    	$title = $myts->makeTboxData4Edit($title);
    	$url = $myts->makeTboxData4Edit($url);
//   	$url = urldecode($url);
    	$email = $myts->makeTboxData4Edit($email);
    	$logourl = $myts->makeTboxData4Edit($logourl);
//  	$logourl = urldecode($logourl);
    	$result2 = $xoopsDB->query("select description from ".$xoopsDB->prefix("mylinks_text")." where lid=$lid"); 
    	list($description)=$xoopsDB->fetch_row($result2);
    	$description = $myts->makeTareaData4Edit($description);
	echo "<table>";
    	echo "<form method=post action=index.php>";
    	echo "<tr><td>"._MD_LINKID."</td><td><b>$lid</b></td></tr>";
    	echo "<tr><td>"._MD_SITETITLE."</td><td><input type=text name=title value=\"$title\" size=50 maxlength=100></input></td></tr>\n";
    	echo "<tr><td>"._MD_SITEURL."</td><td><input type=text name=url value=\"$url\" size=50 maxlength=100></input></td></tr>\n";
    	echo "<tr><td>"._MD_EMAILC."</td><td><input type=text name=email value=\"$email\" size=50 maxlength=60></input></td></tr>\n";
    	echo "<tr><td valign=\"top\">"._MD_DESCRIPTIONC."</td><td><textarea name=description cols=60 rows=5>$description</textarea></td></tr>";
    	echo "<tr><td>"._MD_CATEGORYC."</td><td>";
    	$mytree->makeMySelBox("title", "title", $cid);
	echo "</td></tr>\n";
	echo "<tr><td>"._MD_SHOTIMAGE."</td><td><input type=text name=logourl value=\"$logourl\" size=\"50\" maxlength=\"60\"></input></td></tr>\n";
	$shotdir = "<b>".$xoopsConfig['xoops_url']."/modules/mylinks/images/shots/</b>";
	echo "<tr><td></td><td>";
	printf(_MD_SHOTMUST,$shotdir);
	echo "</td></tr>\n";
	echo "</table>";
    	echo "<br><BR><input type=hidden name=lid value=$lid></input>\n";
    	echo "<input type=hidden name=op value=modLinkS><input type=submit value="._MD_MODIFY.">";
	echo "&nbsp;<input type=button value="._MD_DELETE." onclick=\"javascript:location='index.php?op=delLink&lid=".$lid."'\">";
	echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\">";
	echo "</form>\n";
    	echo "<hr>";

    	$result5=$xoopsDB->query("SELECT count(*) FROM ".$xoopsDB->prefix("mylinks_votedata")." WHERE lid = $lid");
    	list($totalvotes) = $xoopsDB->fetch_row($result5);
    	echo "<table valign=top width=100%>\n";
    	echo "<tr><td colspan=7><b>";
	printf(_MD_TOTALVOTES,$totalvotes);
	echo "</b><br><br></td></tr>\n";
        // Show Registered Users Votes
    	$result5=$xoopsDB->query("SELECT ratingid, ratinguser, rating, ratinghostname, ratingtimestamp FROM ".$xoopsDB->prefix("mylinks_votedata")." WHERE lid = $lid AND ratinguser >0 ORDER BY ratingtimestamp DESC");
    	$votes = $xoopsDB->num_rows($result5);
    	echo "<tr><td colspan=7><br><br><b>";
	printf(_MD_USERTOTALVOTES,$votes);
	echo "</b><br><br></td></tr>\n";
    	echo "<tr><td><b>" ._MD_USER."  </b></td><td><b>" ._MD_IP."  </b></td><td><b>" ._MD_RATING."  </b></td><td><b>" ._MD_USERAVG."  </b></td><td><b>" ._MD_TOTALRATE."  </b></td><td><b>" ._MD_DATE."  </b></td><td align=\"center\"><b>" ._MD_DELETE."</b></td></tr>\n";
    	if ($votes == 0){
	 	echo "<tr><td align=\"center\" colspan=\"7\">" ._MD_NOREGVOTES."<br></td></tr>\n";
    	}
    	$x=0;
    	$colorswitch="dddddd";
    	while(list($ratingid, $ratinguser, $rating, $ratinghostname, $ratingtimestamp)=$xoopsDB->fetch_row($result5)) {
    	//	$ratingtimestamp = formatTimestamp($ratingtimestamp);
    	//Individual user information
    		$result2=$xoopsDB->query("SELECT rating FROM ".$xoopsDB->prefix("mylinks_votedata")." WHERE ratinguser = '$ratinguser'");
        	$uservotes = $xoopsDB->num_rows($result2);
        	$useravgrating = 0;
        	while(list($rating2) = $xoopsDB->fetch_row($result2)){
			$useravgrating = $useravgrating + $rating2;
		}
        	$useravgrating = $useravgrating / $uservotes;
        	$useravgrating = number_format($useravgrating, 1);
		$ratingusername = XoopsUser::get_uname_from_id($ratinguser);
        	echo "<tr><td bgcolor=\"".$colorswitch."\">".$ratingusername."</td><td bgcolor=\"$colorswitch\">".$ratinghostname."</td><td bgcolor=\"$colorswitch\">$rating</td><td bgcolor=\"$colorswitch\">".$useravgrating."</td><td bgcolor=\"$colorswitch\">".$uservotes."</td><td bgcolor=\"$colorswitch\">".$ratingtimestamp."</td><td bgcolor=\"$colorswitch\" align=\"center\"><b><a href=index.php?op=delVote&lid=$lid&rid=$ratingid>X</a></b></td></tr>\n";
    		$x++;
    		if ($colorswitch=="dddddd"){ 
			$colorswitch="ffffff";
    		} else {
			$colorswitch="dddddd";
		}
    	}
	// Show Unregistered Users Votes
    	$result5=$xoopsDB->query("SELECT ratingid, rating, ratinghostname, ratingtimestamp FROM ".$xoopsDB->prefix("mylinks_votedata")." WHERE lid = $lid AND ratinguser = 0 ORDER BY ratingtimestamp DESC");
    	$votes = $xoopsDB->num_rows($result5);
    	echo "<tr><td colspan=7><b><br><br>";
	printf(_MD_ANONTOTALVOTES,$votes);
	echo "</b><br><br></td></tr>\n";
    	echo "<tr><td colspan=2><b>" ._MD_IP."  </b></td><td colspan=3><b>" ._MD_RATING."  </b></td><td><b>" ._MD_DATE."  </b></b></td><td align=\"center\"><b>" ._MD_DELETE."</b></td><br></tr>";
    	if ($votes == 0) {
		echo "<tr><td colspan=\"7\" align=\"center\">" ._MD_NOUNREGVOTES."<br></td></tr>";
    	}
    	$x=0;
    	$colorswitch="dddddd";
    	while(list($ratingid, $rating, $ratinghostname, $ratingtimestamp)=$xoopsDB->fetch_row($result5)) {
    		$formatted_date = formatTimestamp($ratingtimestamp);
        	echo "<td colspan=\"2\" bgcolor=\"$colorswitch\">$ratinghostname</td><td colspan=\"3\" bgcolor=\"$colorswitch\">$rating</td><td bgcolor=\"$colorswitch\">$formatted_date</td><td bgcolor=\"$colorswitch\" aling=\"center\"><b><a href=index.php?op=delVote&lid=$lid&rid=$ratingid>X</a></b></td></tr>";
    		$x++;
    		if ($colorswitch=="dddddd") {
			$colorswitch="ffffff";
    		} else {
			$colorswitch="dddddd";
		}
    	}
    	echo "<tr><td colspan=\"6\">&nbsp;<br></td></tr>\n";
    	echo "</table>\n";
    	echo "</td></tr></table></td></tr></table><br>";
	
    	include ("footer.php");
}

function delVote() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh;
    	$rid = $HTTP_GET_VARS['rid'];
    	$lid = $HTTP_GET_VARS['lid'];
    	$query = "delete from ".$xoopsDB->prefix("mylinks_votedata")." where ratingid=$rid";
    	$xoopsDB->query($query) or $eh->show("0013");
    	updaterating($lid);
	OpenTable();
    	echo _MD_VOTEDELETED."";
    	CloseTable();
}
function listBrokenLinks() {
    	global $xoopsDB, $eh;
    	$result = $xoopsDB->query("select * from ".$xoopsDB->prefix("mylinks_broken")." group by lid order by reportid DESC");
    	$totalbrokenlinks = $xoopsDB->num_rows($result);
	OpenTable();
	echo "<h4>"._MD_BROKENREPORTS." ($totalbrokenlinks)</h4><br>";
    	
    	if ($totalbrokenlinks==0) {
    		echo _MD_NOBROKEN;
    	} else {
		echo "<center>
    "._MD_IGNOREDESC."<br>
    "._MD_DELETEDESC."</center><br><br><br>";
        	$colorswitch="dddddd";
		echo "<table align=\"center\" width=\"90%\">";
        	echo "
        	<tr>
          	<td><b>Link Name</b></td>
          	<td><b>" ._MD_REPORTER."</b></td>
          	<td><b>" ._MD_LINKSUBMITTER."</b></td>
          	<td><b>" ._MD_IGNORE."</b></td>
          	<td><b>" ._MD_DELETE."</b></td>
        	</tr>";
        	while(list($reportid, $lid, $sender, $ip)=$xoopsDB->fetch_row($result)){
    			$result2 = $xoopsDB->query("select title, url, submitter from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid");
			if ($sender != 0) {
				$result3 = $xoopsDB->query("select uname, email from ".$xoopsDB->prefix("users")." where uid=$sender");
				list($uname, $email)=$xoopsDB->fetch_row($result3);
			}
    			list($title, $url, $ownerid)=$xoopsDB->fetch_row($result2);
//			$url=urldecode($url);
    			$result4 = $xoopsDB->query("select uname, email from ".$xoopsDB->prefix("users")." where uid='$ownerid'");
    			list($owner, $owneremail)=$xoopsDB->fetch_row($result4);
    			echo "<tr><td bgcolor=$colorswitch><a href=$url>$title</a></td>";
    			if ($email=='') { echo "<td bgcolor=\"".$colorswitch."\">".$sender." (".$ip.")"; 
			} else { 
				echo "<td bgcolor=\"".$colorswitch."\"><a href=\"mailto:".$email."\">".$uname."</a> (".$ip.")"; 
			}
    			echo "</td>";
    			if ($owneremail=='') { 
				echo "<td bgcolor=\"".$colorswitch."\">".$owner.""; 
			} else { echo "<td bgcolor=\"".$colorswitch."\"><a href=\"mailto:".$owneremail."\">".$owner."</a>"; 
			}
    			echo "</td><td bgcolor=\"".$colorswitch."\"><center><a href=\"index.php?op=ignoreBrokenLinks&lid=".$lid."\">X</a></center></td>
    			   	<td bgcolor=\"".$colorswitch."\"><center><a href=\"index.php?op=delBrokenLinks&lid=".$lid."\">X</a></center>
    			   	</td>
    			  	</tr>";
    			if ($colorswitch=="dddddd") {
				$colorswitch="ffffff";
			} else {
				$colorswitch="dddddd";
			}
    		}
		echo "</table>";
    	}

	CloseTable();
}
function delBrokenLinks() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh;
    	$lid = $HTTP_GET_VARS['lid'];
    	$query = "delete from ".$xoopsDB->prefix("mylinks_broken")." where lid=$lid";
    	$xoopsDB->query($query) or $eh->show("0013");
    	$query = "delete from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid";
    	$xoopsDB->query($query) or $eh->show("0013");
	OpenTable();
    	echo _MD_LINKDELETED;
    	CloseTable();
}
function ignoreBrokenLinks() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh;
    	$query = "delete from ".$xoopsDB->prefix("mylinks_broken")." where lid=".$HTTP_GET_VARS['lid']."";
    	$xoopsDB->query($query) or $eh->show("0013");
	OpenTable();
    	echo _MD_BROKENDELETED;
    	CloseTable();
}
function listModReq() {
    	global $xoopsDB, $myts, $eh, $mytree, $mylinks_shotwidth;
    	$result = $xoopsDB->query("select * from ".$xoopsDB->prefix("mylinks_mod")." order by requestid");
    	$totalmodrequests = $xoopsDB->num_rows($result);
	OpenTable();
    	echo "<h4>"._MD_USERMODREQ." ($totalmodrequests)</h4><br>";
	if($totalmodrequests>0){
    		echo "<table width=95%><tr><td>";
    		while(list($requestid, $lid, $cid, $title, $url, $email, $logourl, $description, $submitterid)=$xoopsDB->fetch_row($result)) {
			$result2 = $xoopsDB->query("select cid, title, url, email, logourl, submitter from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid");
			list($origcid, $origtitle, $origurl, $origemail, $origlogourl, $ownerid)=$xoopsDB->fetch_row($result2);
			$result2 = $xoopsDB->query("select description from ".$xoopsDB->prefix("mylinks_text")." where lid=$lid");
			list($origdescription) = $xoopsDB->fetch_row($result2);
			$result7 = $xoopsDB->query("select uname, email from ".$xoopsDB->prefix("users")." where uid='$submitterid'");
			$result8 = $xoopsDB->query("select uname, email from ".$xoopsDB->prefix("users")." where uid='$ownerid'");
			$cidtitle=$mytree->getPathFromId($cid, "title");
			$origcidtitle=$mytree->getPathFromId($origcid, "title");
			list($submitter, $submitteremail)=$xoopsDB->fetch_row($result7);
			list($owner, $owneremail)=$xoopsDB->fetch_row($result8);
			$title = $myts->makeTboxData4Show($title);
    			$url = $myts->makeTboxData4Show($url);
//			$url = urldecode($url);
    			$email = $myts->makeTboxData4Show($email);

// use original image file to prevent users from changing screen shots file
			$origlogourl = $myts->makeTboxData4Show($origlogourl);
    			$logourl = $origlogourl;

//			$logourl = urldecode($logourl);
    			$description = $myts->makeTareaData4Show($description);
    			$origurl = $myts->makeTboxData4Show($origurl);
//			$origurl = urldecode($origurl);
    			$origemail = $myts->makeTboxData4Show($origemail);
//			$origlogourl = urldecode($origlogourl);
    			$origdescription = $myts->makeTareaData4Show($origdescription);
    			if ($owner=="") { 
				$owner="administration"; 
			}
    			echo "<table border=1 bordercolor=black cellpadding=5 cellspacing=0 align=center width=450><tr><td>
    	   		<table width=100% bgcolor=dddddd>
    	     		<tr>
    	       		<td valign=top width=45%><b>"._MD_ORIGINAL."</b></td>
	       		<td rowspan=14 valign=top align=left><small><br>"._MD_DESCRIPTIONC."<br>$origdescription</small></td>
    	     		</tr>
    	     		<tr><td valign=top width=45%><small>"._MD_SITETITLE."$origtitle</small></td></tr>
    	     		<tr><td valign=top width=45%><small>"._MD_SITEURL."".$origurl."</small></td></tr>
	     		<tr><td valign=top width=45%><small>"._MD_CATEGORYC."$origcidtitle</small></td></tr>
	     		<tr><td valign=top width=45%><small>"._MD_EMAILC."$origemail</small></td></tr>
	     		<tr><td valign=top width=45%><small>"._MD_SHOTIMAGE."<img src=\"".$xoopsConfig['xoops_url']."/modules/mylinks/images/shots/".$origlogourl."\" width=\"".$mylinks_shotwidth."\"></small></td></tr>
    	   		</table></td></tr><tr><td>
    	   		<table width=100%>
    	     		<tr>
    	       		<td valign=top width=45%><b>"._MD_PROPOSED."</b></td>
    	       		<td rowspan=14 valign=top align=left><small><br>"._MD_DESCRIPTIONC."<br>$description</small></td>
    	     		</tr>
    	     		<tr><td valign=top width=45%><small>"._MD_SITETITLE."$title</small></td></tr>
    	     		<tr><td valign=top width=45%><small>"._MD_SITEURL."".$url."</small></td></tr>
	     		<tr><td valign=top width=45%><small>"._MD_CATEGORYC."$cidtitle</small></td></tr>
			<tr><td valign=top width=45%><small>"._MD_EMAILC."$email</small></td></tr>
	     		<tr><td valign=top width=45%><small>"._MD_SHOTIMAGE."<img src=\"".$xoopsConfig['xoops_url']."/modules/mylinks/images/shots/".$logourl."\" width=\"".$mylinks_shotwidth."\"></small></td></tr>
    	   		</table></td></tr></table>
    			<table align=center width=450>
    	  		<tr>";
    			if ($submitteremail=="") { 
				echo "<td align=left><small>"._MD_SUBMITTER."$submitter</small></td>"; 
			} else { 
				echo "<td align=left><small>"._MD_SUBMITTER."<a href=mailto:".$submitteremail.">".$submitter."</a></small></td>"; 
			}
    			if ($owneremail=="") { 
				echo "<td align=center><small>"._MD_OWNER."".$owner."</small></td>"; 
			} else { 
				echo "<td align=center><small>"._MD_OWNER."<a href=mailto:".$owneremail.">".$owner."</a></small></td>"; 
			}
    			echo "
    	    		<td align=right><small>( <a href=index.php?op=changeModReq&requestid=$requestid>"._MD_APPROVE."</a> / <a href=index.php?op=ignoreModReq&requestid=$requestid>"._MD_IGNORE."</a> )</small></td>
    	  		</tr>
    			</table><br><br>";
    		}
    		echo "</td></tr></table>";
	}else {
		echo _MD_NOMODREQ;
	}
	CloseTable();
}
function changeModReq() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh, $myts;
    	$requestid = $HTTP_GET_VARS['requestid'];
    	$query = "select lid, cid, title, url, email, logourl, description from ".$xoopsDB->prefix("mylinks_mod")." where requestid=".$requestid."";
    	$result = $xoopsDB->query($query);
    	while(list($lid, $cid, $title, $url, $email, $logourl, $description)=$xoopsDB->fetch_row($result)) {
	if (get_magic_quotes_runtime()) {
		$title = stripslashes($title);
    		$url = stripslashes($url);
    		$email = stripslashes($email);
    		$logourl = stripslashes($logourl);
    		$description = stripslashes($description);
	}
	$title = addslashes($title);
    	$url = addslashes($url);
    	$email = addslashes($email);
    	$logourl = addslashes($logourl);
    	$description = addslashes($description);
    	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("mylinks_links")." SET cid='$cid',title='$title',url='$url',email='$email',logourl='$logourl', status=2, date=".time()." WHERE lid='$lid'")
      or $eh->show("0013");
	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("mylinks_text")." SET description='$description' WHERE lid=".$lid."")
      or $eh->show("0013");
    	$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_mod")." where requestid='$requestid'") or $eh->show("0013");
    	}
	OpenTable();
    	echo _MD_DBUPDATED;
    	CloseTable();
}
function ignoreModReq() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh;
    	$query= "delete from ".$xoopsDB->prefix("mylinks_mod")." where requestid=".$HTTP_GET_VARS['requestid']."";
    	$xoopsDB->query($query) or $eh->show("0013");
	OpenTable();
    	echo _MD_MODREQDELETED;
    	CloseTable();
}

function modLinkS() {
    	global $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
    	$cid = $HTTP_POST_VARS["cid"];
    	if (($HTTP_POST_VARS["url"]) || ($HTTP_POST_VARS["url"]!="")) {
//		$url = $myts->formatURL($HTTP_POST_VARS["url"]);
//		$url = urlencode($url);
		$url = $myts->makeTboxData4Save($HTTP_POST_VARS["url"]);
	}
	$logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
    	$title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
    	$email = $myts->makeTboxData4Save($HTTP_POST_VARS["email"]);
    	
    	$description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
    	$xoopsDB->query("update ".$xoopsDB->prefix("mylinks_links")." set cid='$cid', title='$title', url='$url', email='$email', logourl='$logourl', status=2, date=".time()." where lid=".$HTTP_POST_VARS['lid']."")  or $eh->show("0013");
    	$xoopsDB->query("update ".$xoopsDB->prefix("mylinks_text")." set description='$description' where lid=".$HTTP_POST_VARS['lid']."")  or $eh->show("0013");
	OpenTable();
    	echo _MD_DBUPDATED;
    	CloseTable();
}
function delLink() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh;
    	$query = "delete from ".$xoopsDB->prefix("mylinks_links")." where lid=".$HTTP_GET_VARS['lid']."";
    	$xoopsDB->query($query) or $eh->show("0013");
	$query = "delete from ".$xoopsDB->prefix("mylinks_text")." where lid=".$HTTP_GET_VARS['lid']."";
	$xoopsDB->query($query) or $eh->show("0013");
	$query = "delete from ".$xoopsDB->prefix("mylinks_votedata")." where lid=".$HTTP_GET_VARS['lid']."";
	$xoopsDB->query($query) or $eh->show("0013");
	OpenTable();
    	echo _MD_LINKDELETED;
    	CloseTable();
}
function modCat() {
    	global $xoopsDB, $HTTP_POST_VARS, $myts, $eh, $mytree;
    	$cid = $HTTP_POST_VARS["cid"];
    	OpenTable();
    	echo "<h4>"._MD_MODCAT."</h4><br>";
	$result=$xoopsDB->query("select pid, title, imgurl from ".$xoopsDB->prefix("mylinks_cat")." where cid=$cid");
	list($pid,$title,$imgurl) = $xoopsDB->fetch_row($result);
	$title = $myts->makeTboxData4Edit($title);
	$imgurl = $myts->makeTboxData4Edit($imgurl);
//	$imgurl = urldecode($imgurl);
	echo "<form action=index.php method=post>"._MD_TITLEC."<input type=text name=title value=\"$title\" size=51 maxlength=50><br><br>"._MD_IMGURLMAIN."<br><input type=text name=imgurl value=\"$imgurl\" size=100 maxlength=150><br><br>";
	echo _MD_PARENT."&nbsp;";
	$mytree->makeMySelBox("title", "title", $pid, 1, pid);
//	<input type=hidden name=pid value=\"$pid\">
	echo "<br><input type=\"hidden\" name=\"cid\" value=\"".$cid."\">
	<input type=\"hidden\" name=\"op\" value=\"modCatS\"><br>
	<input type=\"submit\" value=\""._MD_SAVE."\">
	<input type=\"button\" value=\""._MD_DELETE."\" onClick=\"location='index.php?pid=$pid&cid=$cid&op=delCat'\">";
	echo "&nbsp;<input type=\"button\" value=\""._MD_CANCEL."\" onclick=\"javascript:history.go(-1)\">";
	echo "</form>";
    	echo "</td></tr></table></td></tr></table><br>";
}
function modCatS() {
    	global $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
    	$cid =  $HTTP_POST_VARS['cid'];
    	$pid =  $HTTP_POST_VARS['pid'];
    	$title =  $myts->makeTboxData4Save($HTTP_POST_VARS['title']);
	if (($HTTP_POST_VARS["imgurl"]) || ($HTTP_POST_VARS["imgurl"]!="")) {
//		$imgurl = $myts->formatURL($HTTP_POST_VARS["imgurl"]);
//		$imgurl = urlencode($imgurl);
		$imgurl = $myts->makeTboxData4Save($HTTP_POST_VARS["imgurl"]);
	}
	$xoopsDB->query("update ".$xoopsDB->prefix("mylinks_cat")." set pid=$pid, title='$title', imgurl='$imgurl' where cid=$cid") or $eh->show("0013");
	OpenTable();
    	echo _MD_DBUPDATED;
    	CloseTable();;
}
function delCat() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh, $mytree;
    	$cid =  $HTTP_GET_VARS['cid'];
    	if($HTTP_GET_VARS['ok']){
    		$ok =  $HTTP_GET_VARS['ok'];
    	}
    	if($ok==1) {
		//get all subcategories under the specified category
		$arr=$mytree->getAllChildId($cid);
		for($i=0;$i<sizeof($arr);$i++){
			//get all links in each subcategory
			$result=$xoopsDB->query("select lid from ".$xoopsDB->prefix("mylinks_links")." where cid=".$arr[$i]."") or $eh->show("0013");
			//now for each link, delete the text data and vote ata associated with the link
			while(list($lid)=$xoopsDB->fetch_row($result)){
				$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_text")." where lid=".$lid."") or $eh->show("0013");
				$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_votedata")." where lid=".$lid."") or $eh->show("0013");
				$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_links")." where lid=".$lid."") or $eh->show("0013");
			}
			
			//all links for each subcategory is deleted, now delete the subcategory data
    	    		$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_cat")." where cid=".$arr[$i]."") or $eh->show("0013");
		}
		//all subcategory and associated data are deleted, now delete category data and its associated data
		$result=$xoopsDB->query("select lid from ".$xoopsDB->prefix("mylinks_links")." where cid=".$cid."") or $eh->show("0013");
		while(list($lid)=$xoopsDB->fetch_row($result)){
			$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid") or $eh->show("0013");
			$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_text")." where lid=$lid") or $eh->show("0013");
			$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_votedata")." where lid=".$lid."") or $eh->show("0013");
		}
	    	$xoopsDB->query("delete from ".$xoopsDB->prefix("mylinks_cat")." where cid=$cid") or $eh->show("0013");
		OpenTable();
		echo _MD_CATDELETED;
        	CloseTable();
    	} else {
		OpenTable();
		echo "<center>";
		echo "<h4><font color=\"#ff0000\">";
		echo _MD_WARNING."</font></h4><br>";
    	echo "[ <a href=index.php?op=delCat&cid=$cid&ok=1>"._MD_YES."</a> | <a href=index.php>"._MD_NO."</a> ]<br><br>";
    	echo "</TD></TR></TABLE></TD></TR></TABLE>";
    	}
}
function delNewLink() {
    	global $xoopsDB, $HTTP_GET_VARS, $eh;
    	$query = "delete from ".$xoopsDB->prefix("mylinks_links")." where lid=".$HTTP_GET_VARS['lid']."";
    	$xoopsDB->query($query) or $eh->show("0013");
    	$query = "delete from ".$xoopsDB->prefix("mylinks_text")." where lid=".$HTTP_GET_VARS['lid']."";
    	$xoopsDB->query($query) or $eh->show("0013");
	OpenTable();
    	echo _MD_LINKDELETED;
    	CloseTable();
}
function addCat() {
    	global $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
    	$pid = $HTTP_POST_VARS["cid"];
    	$title = $HTTP_POST_VARS["title"];
    	if (($HTTP_POST_VARS["imgurl"]) || ($HTTP_POST_VARS["imgurl"]!="")) {
//		$imgurl = $myts->formatURL($HTTP_POST_VARS["imgurl"]);
//		$imgurl = urlencode($imgurl);
		$imgurl = $myts->makeTboxData4Save($HTTP_POST_VARS["imgurl"]);
	}
    	$title = $myts->makeTboxData4Save($title);
	$newid = $xoopsDB->GenID("mylinks_cat_cid_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mylinks_cat")." (cid, pid, title, imgurl) VALUES ($newid, $pid, '$title', '$imgurl')") or $eh->show("0013");
	OpenTable();
	echo _MD_NEWCATADDED;
	CloseTable();
    	
}

function addLink() {
    	global $xoopsConfig, $xoopsDB, $myts, $xoopsUser, $eh, $HTTP_POST_VARS;
	if (($HTTP_POST_VARS["url"]) || ($HTTP_POST_VARS["url"]!="")) {
	//	$url=$myts->formatURL($HTTP_POST_VARS["url"]);
//		$url = urlencode($url);
		$url = $myts->makeTboxData4Save($HTTP_POST_VARS["url"]);
	}
	$logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
    	$title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
    	$email = $myts->makeTboxData4Save($HTTP_POST_VARS["email"]);
    	$description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
    	$submitter = $xoopsUser->uid();
    	$result = $xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_links")." where url='$url'");
    	list($numrows) = $xoopsDB->fetch_row($result);
    	if ($numrows>0) {
		echo "<h4><font color=\"#ff0000\">";
		echo _MD_ERROREXIST."</font></h4><br>";
		$error = 1;
    	}
// Check if Title exist
    	if ($title=="") {
		echo "<h4><font color=\"#ff0000\">";
		echo _MD_ERRORTITLE."</font></h4><br>";
    		$error =1;
    	}

// Check if Description exist
    	if ($description=="") {
		echo "<h4><font color=\"#ff0000\">";
		echo _MD_ERRORDESC."</font></h4><br>";
    		$error =1;
    	}
    	if($error == 1) {
		include("footer.php");
		exit();
    	}
    	$cid = $HTTP_POST_VARS['cid'];
	$newid = $xoopsDB->GenID("mylinks_links_lid_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mylinks_links")." (lid, cid, title, url, email, logourl, submitter, status, date, hits, rating, votes, comments) VALUES ($newid, $cid, '$title', '$url', '$email', '$logourl', $submitter, 1, ".time().", 0, 0, 0, 0)",1) or $eh->show("0013");
	if($newid == 0){
		$newid = $xoopsDB->Insert_ID();
	}
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mylinks_text")." (lid, description) VALUES ($newid, '$description')") or $eh->show("0013");
	OpenTable();
	echo _MD_NEWLINKADDED."<br>";
    	CloseTable();
}
function approve(){
	global $xoopsConfig, $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
	$lid = $HTTP_POST_VARS['lid'];
	$title = $HTTP_POST_VARS['title'];
	$cid = $HTTP_POST_VARS['cid'];
	$email = $HTTP_POST_VARS['email'];
	$description = $HTTP_POST_VARS['description'];
	if (($HTTP_POST_VARS["url"]) || ($HTTP_POST_VARS["url"]!="")) {
//		$url=$myts->formatURL($HTTP_POST_VARS["url"]);
//		$url = urlencode($url);
		$url = $myts->makeTboxData4Save($HTTP_POST_VARS["url"]);
	}
	$logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
	$title = $myts->makeTboxData4Save($title);
	$email = $myts->makeTboxData4Save($email);
	$description = $myts->makeTareaData4Save($description);
	$query = "update ".$xoopsDB->prefix("mylinks_links")." set cid='$cid', title='$title', url='$url', email='$email', logourl='$logourl', status=1, date=".time()." where lid=".$lid."";
	$xoopsDB->query($query) or $eh->show("0013");
	$query = "update ".$xoopsDB->prefix("mylinks_text")." set description='$description' where lid=".$lid."";
	$xoopsDB->query($query) or $eh->show("0013");
	$result = $xoopsDB->query("select submitter from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid");
	list($submitterid)=$xoopsDB->fetch_row($result);
	$submitter = XoopsUser::get_uname_from_id($submitterid);
	$subject = sprintf(_MD_YOURLINK,$xoopsConfig['sitename']);
	$message = sprintf(_MD_HELLO,$submitter);
	$message .= "\n\n"._MD_WEAPPROVED."\n\n";
	$yourlinkurl = $xoopsConfig['xoops_url']."/modules/mylinks/";
	$message .= sprintf(_MD_YOUCANBROWSE,$yourlinkurl);
	$message .= "\n\n"._MD_THANKSSUBMIT."\n\n".$xoopsConfig['sitename']."\n".$xoopsConfig['xoops_url']."\n".$xoopsConfig['adminmail']."";
	$from = $xoopsConfig['adminmail'];
	mail($email, $subject, $message, "From: \"".$xoopsConfig['adminmail']."\" <$from>\nX-Mailer: PHP/" . phpversion());
	OpenTable();
    	echo _MD_NEWLINKADDED."<br><br>";
    	CloseTable();
}


function myLinksConfigAdmin() {

	global $xoopsConfig;
	global $mylinks_perpage, $mylinks_popular, $mylinks_newlinks, $mylinks_sresults, $mylinks_useshots, $mylinks_shotwidth;

	OpenTable();

	echo "<h4>" . _MD_GENERALSET . "</h4><br>";
	echo "<form action=\"index.php\" method=\"post\">";
    echo "
    <table width=100% border=0><tr><td nowrap>
    "._MD_LINKSPERPAGE."</td><td width=100%>
        <select name=xmylinks_perpage>
        <option value=$mylinks_perpage selected>$mylinks_perpage</option>
        <option value=10>10</option>
        <option value=15>15</option>
        <option value=20>20</option>
        <option value=25>25</option>
        <option value=30>30</option>
        <option value=50>50</option>
    </select>
    </td></tr><tr><td nowrap>
    "._MD_HITSPOP."</td><td>
        <select name=xmylinks_popular>
        <option value=$mylinks_popular selected>$mylinks_popular</option>
        <option value=10>10</option>
        <option value=20>20</option>
        <option value=50>50</option>
        <option value=100>100</option>
        <option value=500>500</option>
        <option value=1000>1000</option>
    </select>
    </td></tr><tr><td nowrap>
    "._MD_LINKSNEW."</td><td>
        <select name=xmylinks_newlinks>
        <option value=$mylinks_newlinks selected>$mylinks_newlinks</option>
        <option value=10>10</option>
        <option value=15>15</option>
        <option value=20>20</option>
        <option value=25>25</option>
        <option value=30>30</option>
        <option value=50>50</option>
    </select>";
    echo "</td></tr><tr><td nowrap>
    "._MD_LINKSSEARCH."</td><td>
        <select name=xmylinks_sresults>
        <option value=$mylinks_sresults selected>$mylinks_sresults</option>
        <option value=10>10</option>
        <option value=15>15</option>
        <option value=20>20</option>
        <option value=25>25</option>
        <option value=30>30</option>
        <option value=50>50</option>
    </select>";

	echo "</td></tr>";
	echo "<tr><td nowrap>" . _MD_USESHOTS . "</td><td>";
	if ($mylinks_useshots==1) {
		echo "<INPUT TYPE=\"RADIO\" NAME=\"xmylinks_useshots\" VALUE=\"1\" CHECKED>&nbsp;" ._MD_YES."&nbsp;</INPUT>";
		echo "<INPUT TYPE=\"RADIO\" NAME=\"xmylinks_useshots\" VALUE=\"0\" >&nbsp;" ._MD_NO."&nbsp;</INPUT>";
	} else {
		echo "<INPUT TYPE=\"RADIO\" NAME=\"xmylinks_useshots\" VALUE=\"1\">&nbsp;" ._MD_YES."&nbsp;</INPUT>";
		echo "<INPUT TYPE=\"RADIO\" NAME=\"xmylinks_useshots\" VALUE=\"0\" CHECKED>&nbsp;" ._MD_NO."&nbsp;</INPUT>";
	}
	
	echo "</td></tr>";
	echo "<tr><td nowrap>" . _MD_IMGWIDTH . "</td><td>"; 
	if($mylinks_shotwidth!=""){
		echo "<INPUT TYPE=\"text\" size=\"10\" NAME=\"xmylinks_shotwidth\" VALUE=\"$mylinks_shotwidth\"></INPUT>";
	}else{
		echo "<INPUT TYPE=\"text\" size=\"10\" NAME=\"xmylinks_shotwidth\" VALUE=\"140\"></INPUT>";
	}
	echo "</td></tr>";

	echo "<tr><td>&nbsp;</td></tr>";
    	echo "</table>";
    	echo "<input type=\"hidden\" name=\"op\" value=\"myLinksConfigChange\">";
    	echo "<input type=\"submit\" value=\""._MD_SAVE."\">";
	echo "&nbsp;<input type=\"button\" value=\""._MD_CANCEL."\" onclick=\"javascript:history.go(-1)\">";
    	echo "</form>";
    	echo "</td></tr></table></td></tr></table>";

}

function myLinksConfigChange() {
	global $xoopsConfig, $HTTP_POST_VARS;

	$xmylinks_popular = $HTTP_POST_VARS['xmylinks_popular'];
	$xmylinks_newlinks = $HTTP_POST_VARS['xmylinks_newlinks'];
	$xmylinks_sresults = $HTTP_POST_VARS['xmylinks_sresults'];
	$xmylinks_perpage = $HTTP_POST_VARS['xmylinks_perpage'];
	$xmylinks_useshots = $HTTP_POST_VARS['xmylinks_useshots'];
	$xmylinks_shotwidth = $HTTP_POST_VARS['xmylinks_shotwidth'];
	$filename = "../config.php";

	$file = fopen($filename, "w");
	$content = "";
	$content .= "<?PHP\n";
	$content .= "\n";
	$content .= "###############################################################################\n";
	$content .= "# my Links v1.0                                                                #\n";
	$content .= "#                                                                              #\n";
	$content .= "# \$mylinks_popular:	The number of hits required for a link to be a popular site. Default = 20      #\n";
	$content .= "# \$mylinks_newlinks:	The number of links that appear on the front page as latest listings. Default = 10  #\n";
	$content .= "# \$mylinks_sresults:	The number of links that appear in one page when performing search  Default = 10  #\n";
	$content .= "# \$mylinks_perpage:    	The number of links that appear for each page. Default = 10 #\n";
	$content .= "# \$mylinks_useshots:    	Use screenshots? Default = 1 (Yes) #\n";
	$content .= "# \$mylinks_shotwidth:    	Screenshot Image Width (Default = 140) #\n";
	$content .= "###############################################################################\n";
	$content .= "\n";
	$content .= "\$mylinks_popular = $xmylinks_popular;\n";
	$content .= "\$mylinks_newlinks = $xmylinks_newlinks;\n";
	$content .= "\$mylinks_sresults = $xmylinks_sresults;\n";
	$content .= "\$mylinks_perpage = $xmylinks_perpage;\n";
	$content .= "\$mylinks_useshots = $xmylinks_useshots;\n";
	$content .= "\$mylinks_shotwidth = $xmylinks_shotwidth;\n";
	$content .= "\n";
	$content .= "?>\n";

	fwrite($file, $content);
    	fclose($file);

	redirect_header("index.php",1,_MD_CONFUPDATED);
}




switch ($op) {
		default:
			mylinks();
			break;
		case "delNewLink":
			delNewLink();
			break;
		case "approve":
			approve();
			break;
		case "addCat":
			addCat();
			break;
		case "addLink":
			addLink();
			break;
		case "listBrokenLinks":
			listBrokenLinks();
			break;
		case "delBrokenLinks":
			delBrokenLinks();
			break;
		case "ignoreBrokenLinks":
			ignoreBrokenLinks();
			break;
		case "listModReq":
			listModReq();
			break;
		case "changeModReq":
			changeModReq();
			break;
	        case "ignoreModReq":
			ignoreModReq();
			break;
		case "delCat":
			delCat();
			break;
		case "modCat":
			modCat();
			break;
		case "modCatS":
			modCatS();
			break;
		case "modLink":
			modLink();
			break;
		case "modLinkS":
			modLinkS();
			break;
		case "delLink":
			delLink();
			break;
		case "delVote":
			delVote();
			break;
		case "delComment":
			delComment($bid, $rid);
			break;
		case "myLinksConfigAdmin":
			myLinksConfigAdmin();
			break;
		case "myLinksConfigChange":
			myLinksConfigChange();
			break;
		case "linksConfigMenu":
			linksConfigMenu();
			break;
		case "listNewLinks":
			listNewLinks();
			break;
}

include("admin_footer.php");
?>