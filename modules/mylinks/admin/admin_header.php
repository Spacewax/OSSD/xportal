<?php
include("../../../mainfile.php");
include($xoopsConfig['root_path']."header.php");
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");
if ( $xoopsUser ) {
	$xoopsModule = XoopsModule::getByDirname("mylinks");
	if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) { 
		redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
		exit();
	}
} else {
	redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
	exit();
}
if ( file_exists("../language/".$xoopsConfig['language']."/main.php") ) {
	include("../language/".$xoopsConfig['language']."/main.php");
} else {
	include("../language/english/main.php");
}
$xoopsModule->printAdminMenu();
echo "<br />";
?>