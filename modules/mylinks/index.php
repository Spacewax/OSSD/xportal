<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
$myts = new MyTextSanitizer; // MyTextSanitizer object
include_once($xoopsConfig['root_path']."class/xoopstree.php");
$mytree = new XoopsTree($xoopsDB->prefix("mylinks_cat"),"cid","pid");

if($xoopsConfig['startpage'] == "mylinks"){
	$xoopsOption['show_rblock'] =1;
	include($xoopsConfig['root_path']."header.php");
	make_cblock();
	echo "<br />";
}else{
	$xoopsOption['show_rblock'] =0;
	include($xoopsConfig['root_path']."header.php");
}
$result=$xoopsDB->query("SELECT cid, title, imgurl FROM ".$xoopsDB->prefix("mylinks_cat")." WHERE pid = 0 ORDER BY title") or die("Error");

OpenTable();
mainheader();
echo "<center>\n";
echo "<table border=0 cellspacing=5 cellpadding=0 width=\"90%\"><tr>\n";
$count = 0;
while($myrow = $xoopsDB->fetch_array($result)) {
	$title = $myts->makeTboxData4Show($myrow['title']);
    	echo "<td valign=\"top\" align=\"right\">";
    	if ($myrow['imgurl'] && $myrow['imgurl'] != "http://"){
		$imgurl = $myts->makeTboxData4Edit($myrow['imgurl']);
		echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/mylinks/viewcat.php?cid=".$myrow['cid']."\"><img src=\"".$imgurl."\" height=\"50\" border=\"0\"></a>";
    	} else {
		echo "";
    	}
	$totallink = getTotalItems($myrow['cid'], 1);
    	echo "</td><td valign=\"top\" width=\"40%\"><a href=\"".$xoopsConfig['xoops_url']."/modules/mylinks/viewcat.php?cid=".$myrow['cid']."\"><b>$title</b></a>&nbsp;($totallink)<br>";
	// get child category objects
	$arr=array();
	$arr=$mytree->getFirstChild($myrow['cid'], "title");
	$space = 0;
	$chcount = 0;
	foreach($arr as $ele){
		$chtitle=$myts->makeTboxData4Show($ele['title']);
		if ($chcount>5){
			echo "...";
			break;
		}
		if ($space>0) {
        		echo ", ";
        	}
        	echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/mylinks/viewcat.php?cid=".$ele['cid']."\">".$chtitle."</a>";
        	$space++;
		$chcount++;
	}
    	if ($count<1) {
        	echo "</td>";
    	}
    	$count++;
    	if ($count==2) {
        	echo "</td></tr><tr>";
        	$count = 0;
    	}
}
echo "</td></tr></table>";
list($numrows)=$xoopsDB->fetch_row($xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_links")." where status>0"));
echo "<br><br>";
printf(_MD_THEREARE,$numrows);
echo "</center>";
CloseTable();

echo "<br>";

OpenTable();
echo "<div align=\"left\"><h4>"._MD_LATESTLIST."</h4><br><br>";
showNew();
echo "</div>";
CloseTable();	

include($xoopsConfig['root_path']."modules/mylinks/footer.php");

// Shows the Latest Listings on the front page
function showNew(){
	global $myts, $xoopsDB, $xoopsConfig, $xoopsModule;
	global $mylinks_shotwidth, $mylinks_newlinks, $mylinks_useshots;
	$result = $xoopsDB->query("SELECT l.lid, l.cid, l.title, l.url, l.email, l.logourl, l.status, l.date, l.hits, l.rating, l.votes, l.comments, t.description FROM ".$xoopsDB->prefix("mylinks_links")." l, ".$xoopsDB->prefix("mylinks_text")." t where l.status>0 and l.lid=t.lid ORDER BY date DESC",0,$mylinks_newlinks,0);
	echo "<table width=\"100%\" cellspacing=0 cellpadding=10 border=0><tr><td width=\"110\" align=\"center\">";
    	while(list($lid, $cid, $ltitle, $url, $email, $logourl, $status, $time, $hits, $rating, $votes, $comments, $description)=$xoopsDB->fetch_row($result)) {
		$rating = number_format($rating, 2);
        	$ltitle = $myts->makeTboxData4Show($ltitle);
		$url = $myts->makeTboxData4Show($url);
		$email = $myts->makeTboxData4Show($email);
		$logourl = $myts->makeTboxData4Show($logourl);
#		$logourl = urldecode($logourl);
		$datetime = formatTimestamp($time,"m");
		$description = $myts->makeTareaData4Show($description);
		include($xoopsConfig['root_path']."modules/mylinks/include/linkformat.php");
    	}
	echo "</table>";
}
?>