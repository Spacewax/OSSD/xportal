<?PHP
###############################################################################
# my Links v1.0                                                                #
#                                                                              #
# $mylinks_popular:	The number of hits required for a link to be a popular site. Default = 20      #
# $mylinks_newlinks:	The number of links that appear on the front page as latest listings. Default = 10  #
# $mylinks_sresults:	The number of links that appear in one page when performing search  Default = 10  #
# $mylinks_perpage:    	The number of links that appear for each page. Default = 10 #
# $mylinks_useshots:    	Use screenshots? Default = 1 (Yes) #
# $mylinks_shotwidth:    	Screenshot Image Width (Default = 140) #
###############################################################################

$mylinks_popular = 20;
$mylinks_newlinks = 10;
$mylinks_sresults = 10;
$mylinks_perpage = 10;
$mylinks_useshots = 1;
$mylinks_shotwidth = 140;

?>