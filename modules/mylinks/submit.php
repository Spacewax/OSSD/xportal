<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
$myts = new MyTextSanitizer; // MyTextSanitizer object
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");

$eh = new ErrorHandler; //ErrorHandler object
$mytree = new XoopsTree($xoopsDB->prefix("mylinks_cat"),"cid","pid");

if(isset($HTTP_POST_VARS['submit']) && $HTTP_POST_VARS['submit'] != ""){
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	} 

    	if(!isset($HTTP_POST_VARS['submitter']) || $HTTP_POST_VARS['submitter']=="") {
                $submitter = $xoopsUser->uid();
    	}else{
		$submitter = $HTTP_POST_VARS['submitter'];
	}
// Check if Title exist
    	if ($HTTP_POST_VARS["title"]=="") {
        	$eh->show("1001");
    	}
// Check if URL exist
	$url = $HTTP_POST_VARS["url"];
    	if ($url=="" || !isset($url)) {
        	$eh->show("1016");
    	}	
// Check if Description exist
    	if ($HTTP_POST_VARS['description']=="") {
        	$eh->show("1008");
    	}
	if ( !empty($HTTP_POST_VARS['cid']) ) {
    		$cid = $HTTP_POST_VARS['cid'];
	} else {
		$cid = 0;
	}
//	$url = urlencode($url);
	$url = $myts->makeTboxData4Save($url);
	$title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
    	$email = $myts->makeTboxData4Save($HTTP_POST_VARS["email"]);
    	$description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
	$date = time();
	$newid = $xoopsDB->GenID("mylinks_links_lid_seq");
	$q = "INSERT INTO ".$xoopsDB->prefix("mylinks_links")." (lid, cid, title, url, email, logourl, submitter, status, date, hits, rating, votes, comments) VALUES ($newid, $cid, '$title', '$url', '$email', '', $submitter, 0, $date, 0, 0, 0, 0)";
    	$xoopsDB->query($q,1) or $eh->show("0013");
	if($newid == 0){
		$newid = $xoopsDB->Insert_ID();
	}
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mylinks_text")." (lid, description) VALUES ($newid, '$description')") or $eh->show("0013");
	redirect_header("index.php",2,_MD_RECEIVED."<br>"._MD_WHENAPPROVED."");
	exit();
	
}else{
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	}
	include($xoopsConfig['root_path']."header.php");
    	OpenTable();
    	mainheader();
    	echo "<table width=\"100%\" cellspacing=0 cellpadding=1 border=0><tr><td colspan=2>\n";
    	echo "<table width=\"100%\" cellspacing=0 cellpadding=8 border=0><tr><td>\n";
    	echo "<br><br>\n";
    	echo "&nbsp;&nbsp;<strong>&middot;</strong>&nbsp;"._MD_SUBMITONCE."<br>\n";
        echo "&nbsp;&nbsp;<strong>&middot;</strong>&nbsp;"._MD_ALLPENDING."<br>\n";
        echo "&nbsp;&nbsp;<strong>&middot;</strong>&nbsp;"._MD_DONTABUSE."<br>\n";
	echo "&nbsp;&nbsp;<strong>&middot;</strong>&nbsp;"._MD_TAKESHOT."<br>\n";

        echo "<form action=\"submit.php\" method=\"post\">\n";
        echo "<table width=\"80%\"><tr>";
        echo "<td align=\"right\" nowrap><b>"._MD_SITETITLE."</b></td><td>";
    	echo "<input type=text name=title size=50 maxlength=100>";
    	echo "</td></tr><tr><td align=\"right\" nowrap><b>"._MD_SITEURL."</b></td><td>";
        echo "<input type=text name=url size=50 maxlength=100 value=\"http://\">";
        echo "</td></tr>";
        echo "<tr><td align=\"right\" nowrap><b>"._MD_CATEGORYC."</b></td><td>";
        $mytree->makeMySelBox("title", "title");
        echo "</td></tr>\n";
	echo "<tr><td align=\"right\" nowrap><b>"._MD_CONTACTEMAIL."</b></td><td>\n";
	echo "<input type=text name=email size=50 maxlength=60></td></tr>\n";
	
/*	echo "<tr><td align=\"right\"><b>"._MD_SHOTIMAGE."</b></td><td>\n";
	echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\"></td></tr>\n";*/
        echo "<tr><td align=\"right\" valign=\"top\" nowrap><b>"._MD_DESCRIPTIONC."</b></td><td>\n";
        echo "<textarea name=description cols=50 rows=6></textarea>\n";
        echo "</td></tr>\n";
        echo "</table>\n";
	echo "<input type=\"hidden\" name=\"submitter\" value=\"".$xoopsUser->uid()."\"></input>";
	echo "<br><center><input type=\"submit\" name=\"submit\" class=\"button\" value=\""._MD_SUBMIT."\"></input>";
	echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\"></input></center>\n";
        echo "</form>\n";
    	echo "</td></tr></table></td></tr></table>";
    	CloseTable();

}

include("footer.php");
?>