<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
$myts = new MyTextSanitizer; // MyTextSanitizer object
if($HTTP_POST_VARS['submit']) {
	$eh = new ErrorHandler; //ErrorHandler object
	if(!$xoopsUser){
		$ratinguser = 0;
	}else{
		$ratinguser = $xoopsUser->uid();
	}
    	//Make sure only 1 anonymous from an IP in a single day.
    	$anonwaitdays = 1;
    	$ip = getenv("REMOTE_ADDR");
	$lid = $HTTP_POST_VARS['lid'];
	$rating = $HTTP_POST_VARS['rating'];
    	// Check if Rating is Null
    	if ($rating=="--") {
		redirect_header("ratelink.php?lid=".$lid."",4,_MD_NORATING);
		exit();
    	}
	$lid = $HTTP_POST_VARS['lid'];
    	// Check if Link POSTER is voting (UNLESS Anonymous users allowed to post)
    	if ($ratinguser != 0) {
        	$result=$xoopsDB->query("select submitter from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid");
        	while(list($ratinguserDB) = $xoopsDB->fetch_row($result)) {
        		if ($ratinguserDB == $ratinguser) {
				redirect_header("index.php",4,_MD_CANTVOTEOWN);
				exit();
                	}
        	}

    	// Check if REG user is trying to vote twice.
    		$result=$xoopsDB->query("select ratinguser from ".$xoopsDB->prefix("mylinks_votedata")." where lid=$lid");
        	while(list($ratinguserDB) = $xoopsDB->fetch_row($result)) {
        		if ($ratinguserDB == $ratinguser) {
				redirect_header("index.php",4,_MD_VOTEONCE2);
				exit();
                	}
        	}
    	}
     	// Check if ANONYMOUS user is trying to vote more than once per day.
    	if ($ratinguser == 0){
    		$yesterday = (time()-(86400 * $anonwaitdays));
        	$result=$xoopsDB->query("select count(*) FROM ".$xoopsDB->prefix("mylinks_votedata")." WHERE lid=$lid AND ratinguser=0 AND ratinghostname = '$ip' AND ratingtimestamp > $yesterday");
    		list($anonvotecount) = $xoopsDB->fetch_row($result);
    		if ($anonvotecount > 0) {
			redirect_header("index.php",4,_MD_VOTEONCE2);
			exit();
        	}
    	}
	if($rating > 10){
		$rating = 10;
	}
        //All is well.  Add to Line Item Rate to DB.
	$newid = $xoopsDB->GenID("mylinks_votedata_ratingid_seq");
	$datetime = time();
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mylinks_votedata")." (ratingid, lid, ratinguser, rating, ratinghostname, ratingtimestamp) VALUES ($newid, $lid, $ratinguser, $rating, '$ip', $datetime)") or $eh->show("0013");
        //All is well.  Calculate Score & Add to Summary (for quick retrieval & sorting) to DB.
        updaterating($lid);
	$ratemessage = _MD_VOTEAPPRE."<br>".sprintf(_MD_THANKURATE,$xoopsConfig[sitename]);
	redirect_header("index.php",2,$ratemessage);
	exit();
} else {
    	include($xoopsConfig['root_path']."header.php");
	
    	OpenTable();
    	mainheader();
    	$result=$xoopsDB->query("select title from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid");
	list($title) = $xoopsDB->fetch_row($result);
	
	$title = $myts->makeTboxData4Show($title);
    	echo "
    	<hr size=1 noshade>
	<table border=0 cellpadding=1 cellspacing=0 width=\"80%\"><tr><td>
    	<h4><center>$title</center></h4>
    	<UL>
     	<LI>"._MD_VOTEONCE."
     	<LI>"._MD_RATINGSCALE."
     	<LI>"._MD_BEOBJECTIVE."
     	<LI>"._MD_DONOTVOTE."";
    	echo "
     	</UL>
     	</td></tr>
     	<tr><td align=\"center\">
     	<form method=\"POST\" action=\"ratelink.php\">
     	<input type=\"hidden\" name=\"lid\" value=\"".$lid."\">
     	<select name=\"rating\">
     	<option>--</option>";
     	for($i=10;$i>0;$i--){
		echo "<option value=\"".$i."\">".$i."</option>\n";
	}
     	echo "</select>&nbsp;&nbsp;<input type=\"submit\" name=\"submit\" value=\""._MD_RATEIT."\">\n";
	echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\">\n";
    	echo "</form></td></tr></table>\n";
    	CloseTable();    	
	
}

include("footer.php");

?>