<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig[root_path]."class/module.textsanitizer.php");
$myts = new MyTextSanitizer; // MyTextSanitizer object

if($HTTP_POST_VARS[submit]) {
	if(!$xoopsUser){
		$sender = 0;
	}else{
		$sender = $xoopsUser->uid();
	}
	$lid = $HTTP_POST_VARS[lid];
	$ip = getenv("REMOTE_ADDR");
	if ($sender != 0) {
	// Check if REG user is trying to report twice.
    		$result=$xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mylinks_broken")." WHERE lid=".$lid." AND sender=".$sender."");
        	list($count)=$xoopsDB->fetch_row($result);
        	if ($count>0) {
			redirect_header("index.php",2,_MD_ALREADYREPORTED);
			exit();
                }
    	}
	
     	// Check if the sender is trying to vote more than once.
        $result=$xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mylinks_broken")." WHERE lid=$lid AND ip = '$ip'");
    	list($count)=$xoopsDB->fetch_row($result);
        if ($count>0) {
		redirect_header("index.php",2,_MD_ALREADYREPORTED);
		exit();
        }
	$newid = $xoopsDB->GenID("mylinks_broken_reportid_seq");
	$query = "INSERT INTO ".$xoopsDB->prefix("mylinks_broken")." (reportid, lid, sender, ip) VALUES ($newid, $lid, $sender, '$ip')";
	$xoopsDB->query($query) or die("");
	redirect_header("index.php",2,_MD_THANKSFORINFO);
	exit();

}else{
	include($xoopsConfig['root_path']."header.php");
    	$lid = $HTTP_GET_VARS['lid'];
    	OpenTable();
    	mainheader();
    	echo "<center><h4>"._MD_REPORTBROKEN."</h4>";
    	echo "<form action=\"brokenlink.php\" method=\"POST\">";
    	echo "<input type=hidden name=lid value=$lid>";
    	echo _MD_THANKSFORHELP;
        echo "<br>"._MD_FORSECURITY."<br><br>";
	echo "<input type=\"submit\" name=\"submit\" value=\""._MD_REPORTBROKEN."\">";
	echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\">";
	echo "</form></center><br>";
    	CloseTable();

}
include("footer.php");
?>