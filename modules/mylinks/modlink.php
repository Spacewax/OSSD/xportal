<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
$myts = new MyTextSanitizer; // MyTextSanitizer object
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
$eh = new ErrorHandler; //ErrorHandler object
$mytree = new XoopsTree($xoopsDB->prefix("mylinks_cat"),"cid","pid");

if($HTTP_POST_VARS['submit']) {
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	} else {
		$ratinguser = $xoopsUser->uid();
	}
    	$lid = $HTTP_POST_VARS["lid"];
	

// Check if Title exist
    	if ($HTTP_POST_VARS["title"]=="") {
        	$eh->show("1001");
    	}
// Check if URL exist
    	if ($HTTP_POST_VARS["url"]=="") {
         	$eh->show("1016");
    	}
// Check if Description exist
    	if ($HTTP_POST_VARS['description']=="") {
        	$eh->show("1008");
    	}
//	$url = $myts->formatURL($HTTP_POST_VARS["url"]);
//	$url = urlencode($url);
	$url = $myts->makeTboxData4Save($HTTP_POST_VARS["url"]);
//	$logourl = $myts->formatURL($HTTP_POST_VARS["logourl"]);
//	$logourl = urlencode($logourl);
	$logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
    	$cid = $HTTP_POST_VARS["cid"];
    	$title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
    	$email = $myts->makeTboxData4Save($HTTP_POST_VARS["email"]);
    	$description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
	$newid = $xoopsDB->GenID("mylinks_mod_requestid_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mylinks_mod")." (requestid, lid, cid, title, url, email, logourl, description, modifysubmitter) VALUES ($newid, $lid, $cid, '$title', '$url', '$email', '$logourl', '$description', $ratinguser)") or $eh->show("0013");
	redirect_header("index.php",2,_MD_THANKSFORINFO);
	exit();

} else {
    	$lid = $HTTP_GET_VARS['lid'];
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	}
	include($xoopsConfig['root_path']."header.php");
    	OpenTable();
	mainheader();
	echo "<table width=450 align=center>";
    	$result = $xoopsDB->query("select cid, title, url, email, logourl from ".$xoopsDB->prefix("mylinks_links")." where lid=$lid and status>0");
        echo "<h4>"._MD_REQUESTMOD."</h4>";
        list($cid, $title, $url, $email, $logourl) = $xoopsDB->fetch_row($result);
	$title = $myts->makeTboxData4Edit($title);
	$url = $myts->makeTboxData4Edit($url);
//	$url = urldecode($url);
	$email = $myts->makeTboxData4Edit($email);
	$logourl = $myts->makeTboxData4Edit($logourl);
//	$logourl = urldecode($logourl);
	$result2 = $xoopsDB->query("select description from ".$xoopsDB->prefix("mylinks_text")." where lid=$lid"); 
	list($description)=$xoopsDB->fetch_row($result2);
	$description = $myts->makeTareaData4Edit($description);
        echo "<form action=\"modlink.php\" method=\"post\">";
        echo "<table width=\"80%\"><tr><td align=\"right\">";
	echo ""._MD_LINKID."</td><td>";
    	echo "<b>$lid</b>";
    	echo "</td></tr><tr><td align=\"right\">"._MD_SITETITLE."</td><td>";
    	echo "<input type=\"text\" name=\"title\" size=\"50\" maxlength=\"100\" value=\"$title\">";
    	echo "</td></tr><tr><td align=\"right\">"._MD_SITEURL."</td><td>";
        echo "<input type=\"text\" name=\"url\" size=\"50\" maxlength=\"100\" value=\"$url\">";
        echo "</td></tr>";
        echo "<tr><td align=\"right\">"._MD_CATEGORYC."</td><td>";
        $mytree->makeMySelBox("title", "title", $cid);
        echo "</td></tr><tr><td></td><td></td></tr>\n";
	echo "<tr><td align=\"right\">"._MD_CONTACTEMAIL."</td><td>\n";
	echo "<input type=\"text\" name=\"email\" size=\"50\" maxlength=\"60\" value=\"$email\"></td></tr>\n";
#	echo "<tr><td align=\"right\">"._MD_SHOTIMAGE."</td><td>\n";
#	echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\" value=\"$logourl\"></td></tr>\n";
        echo "<tr><td align=\"right\" valign=\"top\">"._MD_DESCRIPTIONC."</td><td>\n";
        echo "<textarea name=description cols=60 rows=5>$description</textarea>\n";
        echo "</td></tr>\n";
        echo "<tr><td colspan=\"2\" align=\"center\"><br>\n";
	echo "<input type=\"hidden\" name=\"logourl\" value=\"$logourl\"></input>";
	echo "<input type=\"hidden\" name=\"lid\" value=\"$lid\"></input>";
        echo "<input type=\"hidden\" name=\"modifysubmitter\" value=\"".$xoopsUser->uid()."\"></input>";
	echo "<input type=\"submit\" name=\"submit\" value=\""._MD_SENDREQUEST."\"></input>";
	echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\"></input>";
	echo "</form>";
        echo "</td></tr></table>";
    
    	CloseTable();

}
include("footer.php");

?>