<?php
/******************************************************************************
 * Function: b_mylinks_top_show
 * Input   : $options[0] = date for the most recent links
 *                    hits for the most popular links
 *           $block['content'] = The optional above content
 *           $options[1]   = How many reviews are displayes
 * Output  : Returns the desired most recent or most popular links
 ******************************************************************************/
function b_mylinks_top_show($options) {
	global $xoopsDB, $xoopsConfig;
	$block = array();
	$block['content'] = "<small>";
	$myts = new MyTextSanitizer();
	$result = $xoopsDB->query("SELECT lid, cid, title, date, hits FROM ".$xoopsDB->prefix("mylinks_links")." WHERE status>0 ORDER BY ".$options[0]." DESC",0,$options[1],0);
	while($myrow = $xoopsDB->fetch_array($result)){
		$link = $myts->makeTboxData4Show($myrow["title"]);
		if ( !XOOPS_USE_MULTIBYTES ){
			if (strlen($link) >= 19) {
				$link = substr($link,0,18)."...";
			}
		}
		$block['content'] .= "&nbsp;&nbsp;<strong><big>&middot;</big></strong>&nbsp;<a href=\"".$xoopsConfig['xoops_url']."/modules/mylinks/singlelink.php?lid=".$myrow['lid']."\">".$link."</a> ";
		if($options[0] == "date"){
			$block['content'] .= "(".formatTimestamp($myrow['date'],"s").")<br />";
			$block['title'] = _MB_MYLINKS_TITLE1;
		}elseif($options[0] == "hits"){
			$block['content'] .= "(".$myrow['hits'].")<br />";
			$block['title'] = _MB_MYLINKS_TITLE2;
		}
	}
    	$block['content'] .= "</small>";
	return $block;
}

function b_mylinks_top_edit($options) {
	global $xoopsTheme;
	$form = ""._MB_MYLINKS_DISP."&nbsp;";
	$form .= "<input type=\"hidden\" name=\"options[]\" value=\"";
	if($options[0] == "date"){
		$form .= "date\"";
	}else {
		$form .= "hits\"";
	}
	$form .= " />";
	$form .= "<input type=\"text\" name=\"options[]\" value=\"".$options[1]."\" />&nbsp;"._MB_MYLINKS_LINKS."";
	return $form;
}
?>