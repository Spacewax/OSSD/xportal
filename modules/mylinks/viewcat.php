<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
$myts = new MyTextSanitizer; // MyTextSanitizer object
include_once($xoopsConfig['root_path']."class/xoopstree.php");

$mytree = new XoopsTree($xoopsDB->prefix("mylinks_cat"),"cid","pid");

$cid = $HTTP_GET_VARS['cid'];
include($xoopsConfig['root_path']."header.php");
OpenTable();
mainheader();
if ($HTTP_GET_VARS['show']!="") {
	$show = $HTTP_GET_VARS['show'];
} else {
	$show = $mylinks_perpage;
}
if (!isset($HTTP_GET_VARS['min'])) {
	$min = 0;
} else {
	$min = $HTTP_GET_VARS['min'];
}
if (!isset($max)) {
	$max = $min + $show;
}
if(isset($HTTP_GET_VARS['orderby'])) {
	$orderby = convertorderbyin($HTTP_GET_VARS['orderby']);
} else {
	$orderby = "title ASC";
}

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\"><tr><td align=\"center\">\n";
echo "<table width=\"100%\" cellspacing=\"2\" cellpadding=\"2\" border=\"0\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><tr><td>\n";
$pathstring = "<a href=index.php>"._MD_MAIN."</a>&nbsp;:&nbsp;";
$nicepath = $mytree->getNicePathFromId($cid, "title", "viewcat.php?op=");
$pathstring .= $nicepath;
echo "<b>".$pathstring."</b>";
echo "</td></tr></table>";

// get child category objects
$arr=array();
$arr=$mytree->getFirstChild($cid, "title");
if($arr!=0){
	echo "</td></tr>";
	echo "<tr><td align=\"left\"><h4>"._MD_CATEGORIES."</h4></td></tr>\n";
	echo "<tr><td align=\"center\">";
	$scount = 0;
        echo "<table width=\"90%\"><tr>";
		
        foreach($arr as $ele){
		$title = $myts->makeTboxData4Show($ele['title']);
		$totallink = getTotalItems($ele['cid'], 1);
               	echo "<td align=\"left\"><b><a href=viewcat.php?cid=".$ele['cid'].">".$title."</a></b>&nbsp;(".$totallink.")&nbsp;&nbsp;</td>";
               	$scount++;
               	if ($scount==4) {
               		echo "</tr><tr>";
                       	$scount = 0;
               	}
        }
        echo "</tr></table><br />\n";
	echo "<hr>";
}
$fullcountresult=$xoopsDB->query("select count(*) from ".$xoopsDB->prefix("mylinks_links")." where cid=$cid and status>0");
list($numrows) = $xoopsDB->fetch_row($fullcountresult);
	
if($numrows>0){
	$q = "select l.lid, l.title, l.url, l.email, l.logourl, l.status, l.date, l.hits, l.rating, l.votes, l.comments, t.description from ".$xoopsDB->prefix("mylinks_links")." l, ".$xoopsDB->prefix("mylinks_text")." t where cid=$cid and l.lid=t.lid and status>0 order by $orderby";
	$result=$xoopsDB->query($q,0,$show,$min);
    
	//if 2 or more items in result, show the sort menu
	if($numrows>1){
    		$orderbyTrans = convertorderbytrans($orderby);
    		echo "<br /><small><center>"._MD_SORTBY."&nbsp;&nbsp;
              	"._MD_TITLE." (<a href=viewcat.php?cid=$cid&orderby=titleA><img src=\"images/up.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a><a href=viewcat.php?cid=$cid&orderby=titleD><img src=\"images/down.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a>)
              	"._MD_DATE." (<a href=viewcat.php?cid=$cid&orderby=dateA><img src=\"images/up.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a><a href=viewcat.php?cid=$cid&orderby=dateD><img src=\"images/down.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a>)
              "._MD_RATING." (<a href=viewcat.php?cid=$cid&orderby=ratingA><img src=\"images/up.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a><a href=viewcat.php?cid=$cid&orderby=ratingD><img src=\"images/down.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a>)
              "._MD_POPULARITY." (<a href=viewcat.php?cid=$cid&orderby=hitsA><img src=\"images/up.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a><a href=viewcat.php?cid=$cid&orderby=hitsD><img src=\"images/down.gif\" border=\"0\" align=\"middle\" alt=\"\" /></a>)
              	";
    		echo "<b><br />";
		printf(_MD_CURSORTEDBY,$orderbyTrans);
		echo "</b></center><br /><br />";
	}
    	echo "<table width=\"100%\" cellspacing=0 cellpadding=10 border=0>";
    	while(list($lid, $ltitle, $url, $email, $logourl, $status, $time, $hits, $rating, $votes, $comments, $description) = $xoopsDB->fetch_row($result)) {
		$rating = number_format($rating, 2);
        	$ltitle = $myts->makeTboxData4Show($ltitle);
		$url = $myts->makeTboxData4Show($url);
		$url = urldecode($url);
		$email = $myts->makeTboxData4Show($email);
		$logourl = $myts->makeTboxData4Show($logourl);
#		$logourl = urldecode($logourl);
		$datetime = formatTimestamp($time);
		$description = $myts->makeTareaData4Show($description);
		include("include/linkformat.php");
   	}
	echo "</table>";
   	$orderby = convertorderbyout($orderby);
    	//Calculates how many pages exist.  Which page one should be on, etc...
    	$linkpages = ceil($numrows / $show);
    	if ($numrows % $show == 0) {
    		$linkpages = $linkpages-1;
        }
        //Page Numbering
    	if ($linkpages!=1 && $linkpages!=0) {
       		echo "<br /><br />";
        	$prev = $min - $show;
        	if ($prev>=0) {
        		echo "&nbsp;<a href=viewcat.php?cid=$cid&min=$prev&orderby=$orderby&show=$show>";
               		echo "<b>&lt; "._MD_PREVIOUS." ]</b></a>&nbsp;";
        	}
        	$counter = 1;
        	$currentpage = ($max / $show);
        	while ( $counter<=$linkpages ) {
               		$mintemp = ($show * $counter) - $show;
               		if ($counter == $currentpage) {
				echo "<b>$counter</b>&nbsp;";
			} else {
				echo "<a href=viewcat.php?cid=$cid&min=$mintemp&orderby=$orderby&show=$show>$counter</a>&nbsp;";
			}
               		$counter++;
        	}
        	if ( $numrows>$max ) {
        		echo "&nbsp;<a href=viewcat.php?cid=$cid&min=$max&orderby=$orderby&show=$show>";
               		echo "<b>[ "._MD_NEXT." &gt;</b></a>";
        	}
    	}
		
}	
echo "</td></tr></table>\n";
CloseTable();

include("footer.php");

?>