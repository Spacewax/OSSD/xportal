<?php
$modversion['name'] = _MI_MYLINKS_NAME;
$modversion['version'] = 1.10;
$modversion['description'] = _MI_MYLINKS_DESC;
$modversion['credits'] = "Kazumi Ono<br>( http://www.mywebaddons.com/ )<br>The MPN SE Project";
$modversion['help'] = "mylinks.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "images/mylinks_slogo.gif";
$modversion['dirname'] = "mylinks";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Blocks
$modversion['blocks'][1]['file'] = "mylinks_top.php";
$modversion['blocks'][1]['name'] = _MI_MYLINKS_BNAME1;
$modvertion['blocks'][1]['description'] = "Shows recently added web links";
$modversion['blocks'][1]['show_func'] = "b_mylinks_top_show";
$modversion['blocks'][1]['edit_func'] = "b_mylinks_top_edit";
$modversion['blocks'][1]['options'] = "date|10";
$modversion['blocks'][2]['file'] = "mylinks_top.php";
$modversion['blocks'][2]['name'] = _MI_MYLINKS_BNAME2;
$modvertion['blocks'][2]['description'] = "Shows most visited web links";
$modversion['blocks'][2]['show_func'] = "b_mylinks_top_show";
$modversion['blocks'][2]['edit_func'] = "b_mylinks_top_edit";
$modversion['blocks'][2]['options'] = "hits|10";

// Menu
$modversion['hasMain'] = 1;
$modversion['sub'][1]['name'] = _MI_MYLINKS_SMNAME1;
$modversion['sub'][1]['url'] = "submit.php";
$modversion['sub'][2]['name'] = _MI_MYLINKS_SMNAME2;
$modversion['sub'][2]['url'] = "topten.php?hit=1";
$modversion['sub'][3]['name'] = _MI_MYLINKS_SMNAME3;
$modversion['sub'][3]['url'] = "topten.php?rate=1";

// Search
$modversion['hasSearch'] = 1;
$modversion['search']['file'] = "include/search.inc.php";
$modversion['search']['func'] = "mylinks_search";
?>