<?php
/***************************************************************************
                          page_header.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com
 
    $Id: page_header.php,v 1.74 2001/06/27 19:09:13 bartvb Exp $ 

 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

//$mtime = microtime();
//$mtime = explode(" ",$mtime);
//$mtime = $mtime[1] + $mtime[0];
//$starttime = $mtime;
$separator = "<span style='font-weight:bold;color:".$xoopsTheme['bgcolor2'].";'>&raquo;&raquo;</span>";

if ( isset($pagetype) && $pagetype == "admin" ) {
	$header_image = "../$header_image";
}

//  Table layout (col and rowspans are marked with '*' and '-')
//  *one*   | two
//  *three* | four
//  -five-  | -six-

// cell one and three in the first TD with rowspan (logo)

echo "<table border='0' width='$TableWidth' cellpadding='5' align='center'>";

//Third row with cell five and six (misc. information)
if ( isset($pagetype) ) {
	switch ( $pagetype ) {
		case 'search':
			echo "<tr><td colspan='2' align='left'><a href='".$url_phpbb."index.php'>";
		    echo printf(_MD_FORUMINDEX,$xoopsConfig['sitename']);
			echo "</a>";
			echo $separator;
			echo _MD_SEARCH;
			echo "</td></tr>";
			break;
		case 'searchresults':
			echo "<tr><td colspan='2' align='left'><a href='".$url_phpbb."index.php'>";
			echo printf(_MD_FORUMINDEX,$xoopsConfig['sitename']);
			echo "</a><b>$separator</b><a href='search.php'>". _MD_SEARCH ."</a>";
			echo $separator;
			echo _MD_SEARCHRESULTS;
			echo "</td></tr>";
			break;
		case 'newforum':
			// No third row
			break;
		case 'viewforum':
			echo "<tr><td align='left'><a href='".$url_phpbb."index.php'>";
		    echo printf(_MD_FORUMINDEX,$xoopsConfig['sitename']);
			echo "</a>$separator<b>$forum_name</b><br />(". _MD_MODERATEDBY .":";
			
			$count = 0;
			$forum_moderators = get_moderators($forum);
			while ( list($null, $mods) = each($forum_moderators) ) {
				while ( list($mod_id, $mod_name) = each($mods) ) {
					if ( $count > 0 ) {
						echo ", ";
					}
					echo "<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".trim($mod_id)."'>".trim($mod_name)."</a>";
					$count++;
				}
			}
			echo ")</td><td align='right'><a href='newtopic.php?forum=$forum'><img src='$newtopic_image' alt='' /></a>&nbsp;&nbsp;</td></tr>";
			break;
		case 'viewtopic':
			$total_forum = get_total_posts($forum, 'forum');
		    echo "<tr><td colspan='2' align='left'><a href='".$url_phpbb."index.php'>";
			echo printf(_MD_FORUMINDEX,$xoopsConfig['sitename']);
			echo "</a>$separator<a href='". $url_phpbb."viewforum.php?forum=$forum&amp;$total_forum'>$forum_name</a>";
			if ( $pagetype != "viewforum" ) {
				echo $separator;
			}
			echo $forumdata['topic_title'];
			echo "</td></tr>";
			break;
	}
}

echo "</table>";

?>