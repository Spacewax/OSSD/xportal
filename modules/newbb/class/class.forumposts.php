<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");

class ForumPosts {
	var $post_id;
	var $topic_id;
	var $forum_id;
	var $post_time;
	var $poster_ip;
	var $order;
	var $subject;
	var $post_text;
	var $pid;
	var $nohtml=0;
	var $nosmiley=0;
	var $uid;
	var $icon;
	var $notify;
	var $attachsig;
	var $prefix;
	var $db;

	function ForumPosts($id=-1) {
		global $xoopsDB;
		$this->db = $xoopsDB;
		if ( is_array($id) ) {
			$this->makePost($id);
		} elseif ( $id != -1 ) {
			$this->getPost($id);
		}
	}

	function setTopicId($value){
		$this->topic_id = $value;
	}

	function setOrder($value){
		$this->order = $value;
	}

	function getTopPosts(){
		$sql = "SELECT p.*, t.post_text FROM ".$this->db->prefix("bb_posts")." p, ".$this->db->prefix("bb_posts_text")." t WHERE p.topic_id=".$this->topic_id." AND p.pid=".$this->pid." AND p.post_id = t.post_id ORDER BY p.".$this->order."";
		//echo $sql;
		$arr=array();
		$result=$this->db->query($sql);
		$numrows = $this->db->num_rows($result);
		if ( $numrows == 0 ) {
			return 0;
		}
		$ret = array();
		while ( $myrow = $this->db->fetch_array($result) ) {
			$ret[] = new ForumPosts($myrow);
		}
		return $ret;
	}

	function getPostTree($pid){
		$mytree = new XoopsTree($this->db->prefix("bb_posts"), "post_id", "pid");
		$parray = array();
		$parray = $mytree->getChildTreeArray($pid, "post_id");
		$ret = array();
		foreach ( $parray as $post ) {
			$ret[] = new ForumPosts($post);
		}
		return $ret;
	}

	function getAllPosts($perpage, $start=0){
		$sql = "SELECT p.*, t.post_text FROM ".$this->db->prefix("bb_posts")." p, ".$this->db->prefix("bb_posts_text")." t WHERE p.topic_id=".$this->topic_id." AND p.post_id = t.post_id ORDER BY p.".$this->order."";
		//echo $sql;
		$arr=array();
		$result=$this->db->query($sql,0,$perpage,$start);
		$numrows = $this->db->num_rows($result);
		if ( $numrows == 0 ) {
			return 0;
		}
		$ret = array();
		while ( $myrow = $this->db->fetch_array($result) ) {
			$ret[] = new ForumPosts($myrow);
		}
		return $ret;
	}

	function setParent($value){
		$this->pid=$value;
	}

	function setSubject($value){
		$this->subject=$value;
	}

	function setText($value){
		$this->post_text=$value;
	}

	function setUid($value){
		$this->uid=$value;
	}

	function setForum($value){
		$this->forum_id=$value;
	}

	function setIp($value){
		$this->poster_ip=$value;
	}

	function setNohtml($value=0){
		$this->nohtml=$value;
	}

	function setNosmiley($value=0){
		$this->nosmiley=$value;
	}

	function setIcon($value){
		$this->icon=$value;
	}

	function setNotify($value){
		$this->notify=$value;
	}

	function setAttachsig($value){
		$this->attachsig=$value;
	}

	function store() {
		$myts = new MyTextSanitizer;
		$subject =$myts->censorString($this->subject);
		$post_text =$myts->censorString($this->post_text);
		$subject = $myts->makeTboxData4Save($subject);
		$post_text = $myts->makeTareaData4Save($post_text);
		if ( !isset($this->post_id) ) {
			if ( !isset($this->topic_id) ) {
				$this->topic_id = $this->db->GenID($this->db->prefix("bb_topics")."_topic_id_seq");
				$datetime = time();
				$sql = "INSERT INTO ".$this->db->prefix("bb_topics")." (topic_id, topic_title, topic_poster, forum_id, topic_time, topic_notify) VALUES (".$this->topic_id.",'$subject', ".$this->uid.", ".$this->forum_id.", $datetime";
				if ( isset($this->notify) && $this->uid != 0 ) {
     					$sql .= ", '1'";
   				} else {
     					$sql .= ", '0'";
				}
   				$sql .= ")";
				//echo $sql;
   				if ( !$result = $this->db->query($sql,1) ) {
					ErrorHandler::show('0022');
   				}
				if ( $this->topic_id == 0 ) {
					$this->topic_id = $this->db->Insert_ID();
				}
			}
			if ( !isset($this->nohtml) || $this->nohtml != 1 ) {
				$this->nohtml = 0;
			}
			if ( !isset($this->nosmiley) || $this->nosmiley != 1 ) {
				$this->nosmiley = 0;
			}
			if ( !isset($this->attachsig) || $this->attachsig != 1 ) {
				$this->attachsig = 0;
			}
			$this->post_id = $this->db->GenID($this->db->prefix("bb_posts")."_post_id_seq");
			$datetime = time(); 
			$sql = "INSERT INTO ".$this->db->prefix("bb_posts")." (post_id, pid, topic_id, forum_id, post_time, uid, poster_ip, subject, nohtml, nosmiley, icon, attachsig) VALUES (".$this->post_id.",".$this->pid.",".$this->topic_id.",".$this->forum_id.",$datetime,".$this->uid.",'".$this->poster_ip."','".$subject."',".$this->nohtml.",".$this->nosmiley.",'".$this->icon."',".$this->attachsig.")";
			if ( !$result = $this->db->query($sql,1) ) {
				ErrorHandler::show('0022');
   			} else {
				if ( $this->post_id == 0 ) {
					$this->post_id = $this->db->Insert_ID();
				}
   				$sql = "INSERT INTO ".$this->db->prefix("bb_posts_text")." (post_id, post_text) VALUES (".$this->post_id.", '".$post_text."')";
   				if ( !$result = $this->db->query($sql) ) {
					$this->db->query("DELETE FROM ".$this->db->prefix("bb_posts")." WHERE post_id=".$this->post_id."");
   					ErrorHandler::show('0022');
   				}
				//echo $sql;
   			}

   			if ( $this->uid != 0 ) {
      				XoopsUser::incrementPost($this->uid);
   			}
			if ( $this->pid == 0 ) {
				$sql = "UPDATE ".$this->db->prefix("bb_topics")." SET topic_last_post_id = ".$this->post_id.", topic_time = $datetime WHERE topic_id =".$this->topic_id."";
   				if ( !$result = $this->db->query($sql) ) {
   					//ErrorHandler::show('0022');
   				}
   				$sql = "UPDATE ".$this->db->prefix("bb_forums")." SET forum_posts = forum_posts+1, forum_topics = forum_topics+1, forum_last_post_id = ".$this->post_id." WHERE forum_id = ".$this->forum_id."";
				$result = $this->db->query($sql);
   				if ( !$result ) {
      					//ErrorHandler::show('0022');
   				}
			} else {
				$sql = "UPDATE ".$this->db->prefix("bb_topics")." SET topic_replies=topic_replies+1, topic_last_post_id = ".$this->post_id.", topic_time = $datetime WHERE topic_id =".$this->topic_id."";
   				if ( !$result = $this->db->query($sql) ) {
   					//ErrorHandler::show('0022');
   				}
				$sql = "UPDATE ".$this->db->prefix("bb_forums")." SET forum_posts = forum_posts+1, forum_last_post_id = ".$this->post_id." WHERE forum_id = ".$this->forum_id."";
				$result = $this->db->query($sql);
   				if ( !$result ) {
      					//ErrorHandler::show('0022');
   				}
			}
		}else{
			if ( $this->is_topic($this->post_id) && isset($this->notify) && $this->notify != "" ) {
			 	$sql = "UPDATE ".$this->db->prefix("bb_topics")." SET topic_title = '$subject', topic_notify = ".$this->notify." WHERE topic_id = ".$this->topic_id."";
			 	if ( !$result = $this->db->query($sql) ) {
			 		ErrorHandler::show('0022');
			 	}
			}
			if ( !isset($this->nohtml) || $this->nohtml != 1 ) {
				$this->nohtml = 0;
			}
			if ( !isset($this->nosmiley) || $this->nosmiley != 1 ) {
				$this->nosmiley = 0;
			}
			if ( !isset($this->attachsig) || $this->attachsig != 1 ) {
				$this->attachsig = 0;
			}
			$sql = "UPDATE ".$this->db->prefix("bb_posts")." SET subject='".$subject."', nohtml=".$this->nohtml.", nosmiley=".$this->nosmiley.", icon='".$this->icon."', attachsig=".$this->attachsig." WHERE post_id=".$this->post_id."";
			$result = $this->db->query($sql);
			if ( !$result ) {
				//echo $sql;
				ErrorHandler::show('0022');
			} else {
				$sql = "UPDATE ".$this->db->prefix("bb_posts_text")." SET post_text = '".$post_text."' WHERE post_id =".$this->post_id."";
				$result = $this->db->query($sql);
				if ( !$result ) {
					//echo $sql;
					ErrorHandler::show('0022');
				}
			}
		}
	}

	function getPost($id) {
		$sql = "SELECT p.*, t.post_text FROM ".$this->db->prefix("bb_posts")." p, ".$this->db->prefix("bb_posts_text")." t WHERE p.post_id=$id and p.post_id=t.post_id";
		$array = $this->db->fetch_array($this->db->query($sql));
		$this->post_id = $array['post_id'];
		$this->pid = $array['pid'];
		$this->topic_id = $array['topic_id'];
		$this->forum_id = $array['forum_id'];
		$this->post_time = $array['post_time'];
		$this->uid = $array['uid'];
		$this->poster_ip = $array['poster_ip'];
		$this->subject = $array['subject'];
		$this->nohtml = $array['nohtml'];
		$this->nosmiley = $array['nosmiley'];
		$this->icon = $array['icon'];
		$this->attachsig = $array['attachsig'];
		$this->post_text = $array['post_text'];
		if ( $this->is_topic($this->post_id) ) {
			$sql = "SELECT * FROM ".$this->db->prefix("bb_topics")." where topic_id=".$this->topic_id."";
			$array = $this->db->fetch_array($this->db->query($sql));					
			$this->notify = $array['topic_notify'];
		}
	}

	function makePost($array){
		foreach($array as $key=>$value){
			$this->$key = $value;
		}
	}

	function delete() {
		$sql = "DELETE FROM ".$this->db->prefix("bb_posts")." WHERE post_id=".$this->post_id."";
		$mytree = new XoopsTree($this->db->prefix("bb_posts"), "post_id", "pid");
		$arr = $mytree->getAllChild($this->post_id);
		if ( sizeof($arr)>0 ) {
			for ( $i = 0; $i < sizeof($arr); $i++ ) {
				$sql = "DELETE FROM ".$this->db->prefix("bb_posts")." WHERE post_id=".$arr[$i]['post_id']."";
			}
		}
	}

	function subject($format="Show") {
		$myts = new MyTextSanitizer;
		$smiley = 1;
		if ( $this->nosmiley() ) {
			$smiley = 0;
		}
		switch ( $format ) {
			case "Show":
				$subject= $myts->makeTboxData4Show($this->subject,$smiley);
				break;
			case "Edit":
				$subject = $myts->makeTboxData4Edit($this->subject);
				break;
			case "Preview":
				$subject = $myts->makeTboxData4Preview($this->subject,$smiley);
				break;
			case "InForm":
				$subject = $myts->makeTboxData4PreviewInForm($this->subject);
				break;
		}
		return $subject;
	}

	function text($format="Show"){
		$myts = new MyTextSanitizer;
		$smiley = 1;
		$html = 1;
		if ( $this->nohtml() ) {
			$html = 0;
		}
		if ( $this->nosmiley() ) {
			$smiley = 0;
		}
		switch ( $format ) {
			case "Show":
				$text = $myts->makeTareaData4Show($this->post_text,$html,$smiley,1);
				break;
			case "Edit":
				$text = $myts->makeTareaData4Edit($this->post_text);
				break;
			case "Preview":
				$text = $myts->makeTareaData4Preview($this->post_text,$html,$smiley,1);
				break;
			case "InForm":
				$text = $myts->makeTareaData4PreviewInForm($this->post_text);
				break;
			case "Quotes":
				$text = $myts->makeTareaData4InsideQuotes($this->post_text);
				break;
		}
		return $text;
	}

	function postid() {
		return $this->post_id;
	}

	function posttime(){
		return $this->post_time;
	}

	function uid(){
		return $this->uid;
	}

	function posterip(){
		return $this->poster_ip;
	}

	function parent(){
		return $this->pid;
	}

	function topic(){
		return $this->topic_id;
	}

	function nohtml(){
		return $this->nohtml;
	}

	function nosmiley(){
		return $this->nosmiley;
	}

	function icon(){
		return $this->icon;
	}

	function forum(){
		return $this->forum_id;
	}

	function notify(){
		return $this->notify;
	}

	function attachsig(){
		return $this->attachsig;
	}

	function prefix(){
		return $this->prefix;
	}

	function is_topic($post_id) {
		$sql = "SELECT pid FROM ".$this->db->prefix("bb_posts")." WHERE post_id = ".$post_id."";
		if ( !$r = $this->db->query($sql,1) ) {
			ErrorHandler::show('0013');
		}
		list($pid)=$this->db->fetch_row($r);
		if ( $pid == 0 ) {
			return true;
		} else {
			return false;
		}
	}
}

?>