<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
/***************************************************************************
Original-     auth.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

***************************************************************************/

if(is_banned($REMOTE_ADDR, "ip"))
  die(_MD_BANNED);

// Setup forum Options.
$sql = "SELECT * FROM ".$xoopsDB->prefix("bb_config")." WHERE selected = 1";
if($result = $xoopsDB->query($sql)) {
   $newbb_config = $xoopsDB->fetch_array($result);
}

// We MUST do this up here, so it's set even if the cookie's not present.
$userdata = Array();

$header_image = "images/header-dark.jpg";
$newtopic_image = 'images/new_topic-dark.jpg';
$reply_image = 'images/reply-dark.jpg';
$tablewidth = "95%";
$TableWidth = $tablewidth;
$reply_locked_image = 'images/reply_locked-dark.jpg';

// set expire dates: one for a year, one for 10 minutes
$expiredate1 = time() + 3600 * 24 * 365;
$expiredate2 = time() + 600;

// update LastVisit cookie. This cookie is updated each time auth.php runs
setcookie("NewBBLastVisit", time(), $expiredate1,  $cookiepath, $cookiedomain, $cookiesecure);

// set LastVisitTemp cookie, which only gets the time from the LastVisit
// cookie if it does not exist yet
// otherwise, it gets the time from the LastVisitTemp cookie
if (!isset($HTTP_COOKIE_VARS["NewBBLastVisitTemp"])) {
	if(isset($HTTP_COOKIE_VARS["NewBBLastVisit"])){
		$temptime = $HTTP_COOKIE_VARS["NewBBLastVisit"];
	}else{
		$temptime = time();
	}
}
else {
	$temptime = $HTTP_COOKIE_VARS["NewBBLastVisitTemp"];
}

// set cookie.
setcookie("NewBBLastVisitTemp", $temptime ,$expiredate2, $cookiepath, $cookiedomain, $cookiesecure);

// set vars for all scripts
$last_visit = $temptime;


// Include the appropriate language file.
if(file_exists($xoopsConfig['root_path'].'modules/newbb/language/'.$xoopsConfig['language'].'/main.php')){
	include($xoopsConfig['root_path'].'modules/newbb/language/'.$xoopsConfig['language'].'/main.php');
}else{
	include($xoopsConfig['root_path'].'modules/newbb/language/english/main.php');
}
?>