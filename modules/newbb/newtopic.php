<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
$pagetype = "newtopic";
$xoopsOption['forumpage'] = 1;
include("header.php");
if(!isset($forum)){
	redirect_header("index.php", 2, _MD_ERRORFORUM);
	exit();
}else{	
	$sql = "SELECT forum_name, forum_access, forum_type FROM ".$xoopsDB->prefix(bb_forums)." WHERE forum_id = $forum";
	if(!$result = $xoopsDB->query($sql)){
		error_die(_MD_CANTGETFORUM);
	}
	$myrow = $xoopsDB->fetch_array($result);
	$forum_name = $myrow['forum_name'];
	$forum_access = $myrow['forum_access'];
	if ($myrow['forum_type'] == 1){
		// To get here, we have a logged-in user. So, check whether that user is allowed to post in
		// this private forum.
		$accesserror = 0; //initialize
		if ( $xoopsUser ) {
			//check if the user has forum admin right
			if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
				if ( !check_priv_forum_auth($xoopsUser->uid(), $forum, TRUE) ) {
					$accesserror = 1;
				}
			}
		} else {
			$accesserror = 1;
		}
		if ( $accesserror ) {
			redirect_header("viewforum.php?order=$order&viewmode=$viewmode&forum=$forum",2,_MD_NORIGHTTOPOST);
			exit;
		}
		// Ok, looks like we're good.
	}
	include($xoopsConfig['root_path']."header.php");
	OpenTable();
	include("page_header.php");
	$istopic = 1;
	$pid=0;
	unset($post_id);
	unset($topic_id);
	include("include/forumform.inc.php");
	include("page_tail.php");
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}
?>