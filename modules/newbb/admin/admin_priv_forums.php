<?php
/***************************************************************************
                          admin_priv_forums.php  -  description
                             -------------------
    begin                : Thu 12 Jan 2001
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

    $Id: admin_priv_forums.php,v 1.6 2001/04/13 05:24:05 thefinn Exp $
 ***************************************************************************/

/***************************************************************************
 *                                         				                                
 *   This program is free software; you can redistribute it and/or modify  	
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or	    	
 *   (at your option) any later version.
 *
 ***************************************************************************/
include("admin_header.php");
include('../functions.php');
include('../config.php');
require('../auth.php');
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
$myts = new MyTextSanitizer;
OpenTable();
	$pagetype = "admin";
	include('../page_header.php');
	
	if (!$op)
	{
		// No opcode passed. Show list of private forums.
?>
		
	<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
		<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%">
			<TR>
				<TD BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
					<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
						<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3']?>" ALIGN="LEFT">
							<TD ALIGN="CENTER" COLSPAN="2">
								<FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>">
									<B><?php _MD_A_SAFTE;?></B>
								</FONT>
							</TD>
						</TR>
						<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
							<TD ALIGN="CENTER" COLSPAN="2">
								<SELECT NAME="forum" SIZE="0">
	<?php

		$sql = "SELECT forum_name, forum_id FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_type = 1 ORDER BY forum_id";
		if(!$result = $xoopsDB->query($sql,1))
		{
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
		
		if($myrow = $xoopsDB->fetch_array($result)) 
		{
			do 
			{
				$name = $myts->makeTboxData4Show($myrow['forum_name']);
				echo "<OPTION VALUE=\"".$myrow['forum_id']."\">$name</OPTION>\n";
			} 
			while($myrow = $xoopsDB->fetch_array($result));
		}
		else 
		{
			echo "<OPTION VALUE=\"-1\">"._MD_A_NFID."</OPTION>\n";
		}
	
?>
								</SELECT>
							</TD>
						</TR>
						<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3']?>" ALIGN="LEFT">
							<TD ALIGN="CENTER" COLSPAN="2">
								<INPUT TYPE="HIDDEN" NAME="op" VALUE="showform">
								<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_A_EDIT;?>">&nbsp;&nbsp;
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</FORM>

<?php	
	
	}
	else
	{
		// Opcode exists. See what it is, do stuff.
		
		
		if ($op == "adduser")
		{
			// Add user(s) to the list for this forum.
			if ($userids)
			{
				while(list($null, $curr_userid) = each($HTTP_POST_VARS["userids"]))
				{
					$sql = "INSERT INTO ".$xoopsDB->prefix("bb_forum_access")." (forum_id, user_id, can_post) VALUES ($forum, $curr_userid, 0)";
					if (!$result = $xoopsDB->query($sql,1))
					{
						CloseTable();
						include($xoopsConfig['root_path']."footer.php");
						exit();
					}
				}
			}	
			$op = "showform";
		
		}
		else if ($op == "deluser")
		{
			// Remove a user from the list for this forum.
			$sql = "DELETE FROM ".$xoopsDB->prefix("bb_forum_access")." WHERE forum_id = $forum AND user_id = $op_userid";
			if (!$result = $xoopsDB->query($sql,1))
			{
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
			
			$op = "showform";
			
		}
		else if ($op == "clearusers")
		{
			// Remove all users from the list for this forum.
			$sql = "DELETE FROM ".$xoopsDB->prefix("bb_forum_access")." WHERE forum_id = $forum";
			if (!$result = $xoopsDB->query($sql,1))
			{
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
			
			$op = "showform";
		}
		else if ($op == "grantuserpost")
		{
			// Add posting rights for this user in this forum.
			$sql = "UPDATE ".$xoopsDB->prefix("bb_forum_access")." SET can_post=1 WHERE forum_id = $forum AND user_id = $op_userid";
			if (!$result = $xoopsDB->query($sql,1))
			{
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}

			$op = "showform";
		
		}
		else if ($op == "revokeuserpost")
		{
			// Revoke posting rights for this user in this forum.
			$sql = "UPDATE ".$xoopsDB->prefix("bb_forum_access")." SET can_post=0 WHERE forum_id = $forum AND user_id = $op_userid";
			if (!$result = $xoopsDB->query($sql,1))
			{
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
			
			$op = "showform";
		
		}
		
		// We want this one to be available even after one of the above blocks has executed.
		// The above blocks will set $op to "showform" on success, so it goes right back to the form.
		// Neato. This is really slick.
		if ($op == "showform")
		{
			// Show the form for the given forum.

			$sql = "SELECT forum_name FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = $forum";
			if ((!$result = $xoopsDB->query($sql,1)) || ($forum == -1))
			{
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
			$forum_name = "";
			if ($row = $xoopsDB->fetch_array($result))
			{
				$forum_name = $myts->makeTboxData4Show($row['forum_name']);
			}
?>			
	 <TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="<?php echo $TableWidth?>">
		<TR>
			<TD BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
				<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
					<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3']?>" ALIGN="LEFT">
		     <td colspan="3" align="center"><?php printf(_MD_A_EFPF,$forum_name);?></td>
		     </tr>
		     <tr>
		     <td bgcolor="<?php echo $xoopsTheme['bgcolor3']?>" align="center" width="40%">
			<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
		         <b><?php echo _MD_A_UWA;?></b>
		     </TD>
		     <TD bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="center" width="20%">
		        &nbsp;
		     </TD>
		     <TD bgcolor="<?php echo $xoopsTheme['bgcolor3']?>" align="center">
		        <b><?php echo _MD_A_UWA;?></b>
		     </TD>
		     </TR>
		     
		     <TR>
		      <TD VALIGN="TOP" bgcolor="<?php echo $xoopsTheme['bgcolor3']?>" align="center" width="40%">
		     <SELECT NAME="userids[]" SIZE="10" MULTIPLE>
<?php
			$sql = "SELECT u.uid FROM ".$xoopsDB->prefix("users")." u, ".$xoopsDB->prefix("bb_forum_access")." f WHERE u.uid = f.user_id AND f.forum_id = $forum";
			if (!$result = $xoopsDB->query($sql,1))
			{
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
			
			$current_users = Array();
			
			while ($row = $xoopsDB->fetch_array($result))
			{
				$current_users[] = $row[uid];
			}
			
			$sql = "SELECT uid, uname FROM ".$xoopsDB->prefix("users")." WHERE uid <> 0 AND level <> -1 ";
			while(list($null, $curr_userid) = each($current_users))
			{
	 			$sql .= "AND (uid != $curr_userid) ";
      	}
      	$sql .= "ORDER BY uname ASC";
 
      	if (!$result = $xoopsDB->query($sql,1))
      	{
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
      	}
      	while ($row = $xoopsDB->fetch_array($result))
      	{
?>      	
	     <OPTION VALUE="<?php echo $row['uid'] ?>"> <?php echo $row['uname'] ?> </OPTION>
<?php      	
      	}
?>	
							</SELECT>
						</TD>
						<TD bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="center">

							<INPUT TYPE="HIDDEN" NAME="op" VALUE="adduser">
							<INPUT TYPE="HIDDEN" NAME="forum" VALUE="<?php echo $forum ?>">
							<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_A_ADDUSERS;?>">
							<br><br>
							<b><A HREF="<?php echo $PHP_SELF ?>?forum=<?php echo $forum ?>&op=clearusers"><?php echo _MD_A_CLEARALLUSERS;?></A></b>
						</TD>
						<TD VALIGN="TOP" bgcolor="<?php echo $xoopsTheme['bgcolor3']?>" align="center">
<?php
			$sql = "SELECT u.uname, u.uid, f.can_post FROM ".$xoopsDB->prefix("users")." u, ".$xoopsDB->prefix("bb_forum_access")." f WHERE u.uid = f.user_id AND f.forum_id = $forum ORDER BY u.uid ASC";
			if (!$result = $xoopsDB->query($sql,1))
			{
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
?>			
							<TABLE BORDER="0" CELLPADDING="10" CELLSPACING="0">
								
<?php									
			while ($row = $xoopsDB->fetch_array($result))
			{
				$post_text = ($row['can_post']) ? "can" : "can't";
				$post_text .= " post";
				
				$post_toggle_link = "<A HREF=\"$PHP_SELF?forum=$forum&op_userid=".$row['uid']."&op=";
				if ($row['can_post'])
				{
					$post_toggle_link .= "revokeuserpost\">"._MD_A_REVOKEPOSTING."</A>";
				}
				else
				{
					$post_toggle_link .= "grantuserpost\">"._MD_A_GRANTPOSTING."</A>";
				}
				
				$remove_link = "<A HREF=\"$PHP_SELF?forum=$forum&op=deluser&op_userid=".$row['uid']."\">"._MD_A_REMOVE."</A>";
?>
								<TR>
									<TD>
			                                              
										<b><?php echo $row['uname']?></b>
							
			                                                </TD>
			                                                
									<TD>
			                                               
										<?php echo $post_text ?>
			                                         
									</TD>
									<TD>
			                                               
										<?php echo $post_toggle_link ?>
			                                          
									</TD>
									<TD>
			                                              
										<?php echo $remove_link ?>
			                                            
									</TD>
								<TR>
<?php			
			}
?>							
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</FORM>
			</table></table>
<?php			
		} // end of big opcode if/else block.
		
	
	}
	
//}

include('../page_tail.php');
CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>