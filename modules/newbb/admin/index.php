<?php
/***************************************************************************
                          admin.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

    $Id: index.php,v 1.28 2001/04/13 06:35:18 thefinn Exp $
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
include('admin_header.php');
include('../functions.php');
include('../config.php');
require('../auth.php');
OpenTable();

$pagetype = "admin";
include('../page_header.php');

if(isset($mode)) {

}
else {
	
?>
<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="95%"><tr><td  bgcolor="<?php echo $xoopsTheme['bgcolor2']?>">
<table border="0" cellpadding="1" cellspacing="1" width="100%">

<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_forums.php?mode=addforum"><?php echo _MD_A_ADDAFORUM;?></a></font></td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2ADDFORUM;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_forums.php?mode=editforum"><?php echo _MD_A_EDITAFORUM;?></a></font></td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2EDITFORUM;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_priv_forums.php?mode=editforum"><?php echo _MD_A_SETPRIVFORUM;?></a></font></td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2SETPRIV;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_board.php?mode=sync"><?php echo _MD_A_SYNCFORUM;?></a></font></td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2SYNC;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_forums.php?mode=addcat"><?php echo _MD_A_ADDACAT;?></a></font></td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2ADDCAT;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_forums.php?mode=editcat"><?php echo _MD_A_EDITCATTTL;?></a></font></td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2EDITCAT;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
     <td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_forums.php?mode=remcat"><?php echo _MD_A_RMVACAT;?></a></font></td>
     <td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2RMVCAT;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
     <td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_forums.php?mode=catorder"><?php echo _MD_A_REORDERCAT;?></a></font></td>
     <td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2ORDERCAT;?></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="left">
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><a href="<?php echo $url_admin?>/admin_board.php?mode=setoptions"><?php echo _MD_A_SETFOPTIONS;?></a></font></td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_LINK2FOPTIONS;?></font></td>
</tr>

</table></td></tr></table>
<?php
}

include('../page_tail.php');
CloseTable();
include($xoopsConfig['root_path'].'footer.php');
?>