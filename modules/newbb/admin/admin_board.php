<?php
/***************************************************************************
                          admin_board.php  -  description
                             -------------------
    begin                : Wed July 19 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

    $Id: admin_board.php,v 1.35 2001/06/16 06:05:50 thefinn Exp $

 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
include("admin_header.php");
include('../functions.php');
include('../config.php');
require('../auth.php');
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
OpenTable();

$pagetype = "admin";
include('../page_header.php');

switch($mode) {
	case 'setoptions':
		$myts = new MyTextSanitizer;
		if(isset($HTTP_POST_VARS['submit']) && $HTTP_POST_VARS['submit'] != "") {
		   //$name = addslashes($name);
		   $esig = $myts->makeTareaData4Save($esig);
		   $sql = "SELECT count(*) AS total FROM ".$xoopsDB->prefix("bb_config")." WHERE (selected = 1)";
		   $result = $xoopsDB->query($sql,1);
		   if (!$result) {
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		   }
		   $row = $xoopsDB->fetch_array($result);
		   if ($row['total'] != 0) {
		      // settings exist, so we can just update.
		      $sql = "UPDATE ".$xoopsDB->prefix("bb_config")." SET allow_html = '$html', allow_sig = '$sig', posts_per_page = '$ppp', hot_threshold = '$hot', topics_per_page = '$tpp' WHERE selected = 1";
			//echo $sql;
		      $result = $xoopsDB->query($sql,1);
		   } else {
		      // have to do an insert..
		      $sql = "INSERT INTO ".$xoopsDB->prefix("bb_config")." (allow_html, allow_sig, posts_per_page, hot_threshold, topics_per_page, selected) ";
		      $sql .= "VALUES ($html, $sig, $ppp, $hot, $tpp, 1)";
		      $result = $xoopsDB->query($sql,1);
		   }
		   if (!$result) {
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		   }
		   echo "<TABLE width=\"95%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bordercolor=\"".$xoopsTheme['bgcolor2']."\">";
		   echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><B>"._MD_A_FORUMUPDATE."</B></font></td>";
		   echo "</tr><TR><TD><TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><TR>";
		   echo "<td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><p><a href=\"$url_admin_index\">"._MD_A_RETURNTOADMINPANEL."</p><p><a href=\"$url_phpbb_index\">"._MD_A_RETURNTOFORUMINDEX."</a></a></font></td>";
		   echo "</tr></table></td></tr></table>";

		}
		else {
		$html_yes = $html_no = $sig_yes = $sig_no = "";
		if($newbb_config['allow_html'] == 1)
		     $html_yes = "CHECKED";
		   else
		     $html_no = "CHECKED";

		   if($newbb_config['allow_sig'] == 1)
		     $sig_yes = "CHECKED";
		   else
		     $sig_no = "CHECKED";

?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><B><?php echo _MD_A_SETFORUMWIDEOPTION;?></B></FONT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><I><?php echo _MD_A_NTSWBSITDAWOASIC;?></I></FONT></TD>
</TR>

<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_ALLOWHTML;?></FONT></TD>
	<TD><INPUT TYPE="RADIO" NAME="html" VALUE="1" <?php echo $html_yes?>> <?php echo _MD_A_YES;?> <INPUT TYPE="RADIO" NAME="html" VALUE="0" <?php echo $html_no?>> <?php echo _MD_A_NO;?></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_ALLOWSIGNATURES;?></FONT></TD>
	<TD><INPUT TYPE="RADIO" NAME="sig" VALUE="1" <?php echo $sig_yes?>> <?php echo _MD_A_YES;?> <INPUT TYPE="RADIO" NAME="sig" VALUE="0" <?php echo $sig_no?>> <?php echo _MD_A_NO;?></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_HOTTOPICTHRESHOLD;?></FONT></TD>
	<TD><INPUT TYPE="TEXT" NAME="hot" SIZE="3" MAXLENGTH="3" VALUE="<?php echo $newbb_config['hot_threshold'];?>"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_POSTPERPAGE;?></FONT><br><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><I><?php echo _MD_A_TITNOPPTTWBDPPOT;?></I></FONT></TD>
	<TD><INPUT TYPE="TEXT" NAME="ppp" SIZE="3" MAXLENGTH="3" VALUE="<?php echo $newbb_config['posts_per_page']?>"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
        <TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><?php echo _MD_A_TOPICPERFORUM;?></FONT><br><FONT COLOR="<?php echo $xoopsTheme["textcolor2"];?>"><I><?php echo _MD_A_TITNOTPFTWBDPPOAF;?></I></FONT></TD>
        <TD><INPUT TYPE="TEXT" NAME="tpp" SIZE="3" MAXLENGTH="3" VALUE="<?php echo $newbb_config['topics_per_page']?>"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2">
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="setoptions">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_A_SAVECHANGES;?>">&nbsp;&nbsp;
		<INPUT TYPE="RESET" VALUE="<?php echo _MD_A_CLEAR;?>">
	</TD>
</TR>
</TABLE></TD></TR></TABLE>
<?php
		}
	break;

	case 'sync':
		if($submit)
		{
			echo "<div align=\"center\">"._MD_A_SYNCHING."<br>";
			flush();
			sync(NULL, "all forums");
			flush();
			sync(NULL, "all topics");
			echo "<br>";
			echo _MD_A_DONESYNC."</div>";
		}
		else
		{
?>
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
  <td><?php echo _MD_A_CLICKBELOWSYNC;?></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" align="center">
	<td><form action="<?php echo $PHP_SELF?>" method="POST">
	    <input type="hidden" name="mode" value="<?php echo $mode?>"><input type="submit" name="submit" value="<?php echo _MD_A_SYNCFORUM;?>"></form></td>
	</td>
</tr>
</table>
</td></tr></table>
<?php
		}
	break;
}
include('../page_tail.php');
CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>