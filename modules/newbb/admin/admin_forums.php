<?php
/***************************************************************************
                          admin_forums.php  -  description
                             -------------------
    begin                : Wed July 19 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

    $Id: admin_forums.php,v 1.41 2001/06/16 06:05:50 thefinn Exp $
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

/**
* Thursday, July 20, 2000 - Yokhannan - I added [$url_admin] to most of the links.
* I fixed a few typo errors
*
* 09/13/2000 - John B. Abela (abela@4cm.com)
* 	Added Some Cosmetic HTML Code, fixed a Hyperlink typo.
*/
include("admin_header.php");
include('../functions.php');
include('../config.php');
require('../auth.php');
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

switch($mode) {
 case 'editforum':
    $myts = new MyTextSanitizer;
   if(isset($HTTP_POST_VARS['save']) && $HTTP_POST_VARS['save'] != "") {
      if(!$HTTP_POST_VARS['delete']) {
	 		$name = $myts->makeTboxData4Save($HTTP_POST_VARS['name']);
	 		$desc = $myts->makeTareaData4Save($HTTP_POST_VARS['desc']);

	 $sql = "UPDATE ".$xoopsDB->prefix("bb_forums")." SET forum_name = '$name', forum_desc = '$desc', forum_type = '$type', cat_id = $cat, forum_access = $forum_access WHERE forum_id = $forum";

	 if(!$r = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
	 $count = 0;
	 if(isset($mods)) {
	    while(list($null, $mod) = each($HTTP_POST_VARS["mods"])) {
	       $mod_data = new XoopsUser($mod);
	       if(($mod_data->is_active()) && ($mod_data->level() < 2)) {
		  if(!isset($user_query))
		    $user_query = "UPDATE ".$xoopsDB->prefix("users")." SET level = 2 WHERE ";
		  if($count > 0)
		    $user_query .= "OR ";
		  $user_query .= "uid = '$mod' ";
		  $count++;
	       }
	       $mod_query = "INSERT INTO ".$xoopsDB->prefix("bb_forum_mods")." (forum_id, user_id) VALUES ('$forum', '$mod')";
	       if(!$xoopsDB->query($mod_query,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
	    }
	 }

	 if(!isset($mods)) {
	    $current_mods = "SELECT count(*) AS total FROM ".$xoopsDB->prefix("bb_forum_mods")." WHERE forum_id = $forum";
	    $r = $xoopsDB->query($current_mods);
	    list($total) = $xoopsDB->fetch_row($r);
	 }
	 else
	   $total = count($mods) + 1;

	 if(isset($rem_mods) && $total > 1) {
	    while(list($null, $mod) = each($HTTP_POST_VARS["rem_mods"])) {
	       $rem_query = "DELETE FROM ".$xoopsDB->prefix("bb_forum_mods")." WHERE forum_id = $forum AND user_id = $mod";
	       if(!$xoopsDB->query($rem_query,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
	    }
	 }
	 else {
	    if(isset($rem_mods))
	      $mod_not_removed = 1;
	 }
	 if(isset($user_query)) {
	    if(!$xoopsDB->query($user_query,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
	 }

	 echo "<table width=\"95%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bordercolor=\"".$xoopsTheme['bgcolor2']."\">";
	 echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><b>"._MD_A_FORUMUPDATED."</b></font></td>";
	 if($mod_not_removed)
	   echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><b>"._MD_A_HTSMHNBRBITHBTWNLBAMOTF."</b></font></td>";

	 echo "</tr><TR><TD><TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><TR>";
	 echo "<td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><P><BR>&nbsp;&nbsp;<a href=\"$url_admin_index\">"._MD_A_RETURNTOADMINPANEL."</a><P><a href=\"$url_phpbb_index\">"._MD_A_RETURNTOFORUMINDEX."</a></font><P><BR><P></TD>";
	 echo "</TR></table></TD></TR></TABLE>";
      }
      else {
      	$sql = "SELECT post_id FROM ".$xoopsDB->prefix("bb_posts")." WHERE forum_id = $forum";
    		if(!$r = $xoopsDB->query($sql,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
		if($xoopsDB->num_rows($r) > 0){
	 		$sql = "DELETE FROM ".$xoopsDB->prefix("bb_posts_text")." WHERE ";
	 		$looped = FALSE;
	 		while($ids = $xoopsDB->fetch_array($r))
	 		{
	 			if($looped == TRUE)
	 			{
	 				$sql .= " OR ";
	 			}
	 			$sql .= "post_id = ".$ids["post_id"]." ";
	 			$looped = TRUE;
	 		}
			if(!$r = $xoopsDB->query($sql,1)){
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}

	 		$sql = "DELETE FROM ".$xoopsDB->prefix("bb_posts")." WHERE forum_id = '$forum'";
	 		if(!$r = $xoopsDB->query($sql,1)){
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
		}
	 		$sql = "DELETE FROM ".$xoopsDB->prefix("bb_topics")." WHERE forum_id = '$forum'";
	 		if(!$r = $xoopsDB->query($sql,1)){
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
			 $sql = "DELETE FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = '$forum'";
	 		if(!$r = $xoopsDB->query($sql,1)){
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}

	 		$sql = "DELETE FROM ".$xoopsDB->prefix("bb_forum_mods")." WHERE forum_id = '$forum'";
	 		if(!$r = $xoopsDB->query($sql,1)){
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
	 		echo "<table width=\"95%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bordercolor=\"".$xoopsTheme['bgcolor2']."\">";
	 		echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><B>"._MD_A_FORUMREMOVED."</B></font></td>";
	 		echo "</tr><tr><td><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><TR>";
	 		echo "<td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><P><BR>&nbsp;&nbsp;"._MD_A_FRFDAWAIP."<P><a href=\"$url_admin_index\">"._MD_A_RETURNTOADMINPANEL."</a></p><p><a href=\"$url_phpbb_index\">"._MD_A_RETURNTOFORUMINDEX."</a></font></p></td>";
	 		echo "</tr></table></td></tr></table>";
      }
   }
   if(isset($HTTP_POST_VARS['submit']) && !isset($HTTP_POST_VARS['save'])) {
      $sql = "SELECT * FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = $forum";
      if(!$result = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      if(!$myrow = $xoopsDB->fetch_array($result)) {
	 echo _MD_A_NOSUCHFORUM;
	 include('page_tail.php');
      }
      $name = $myts->makeTboxData4Edit($myrow['forum_name']);
      $desc = $myts->makeTareaData4Edit($myrow['forum_desc']);
      ?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
        <TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_EDITTHISFORUM;?></B></FONT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
        <TD COLSPAN=2><INPUT TYPE="CHECKBOX" NAME="delete" VALUE="1"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"> <?php echo _MD_A_DTFTWARAPITF;?></FONT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
        <TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_FORUMNAME;?></FONT></TD>
        <TD><INPUT TYPE="TEXT" NAME="name" SIZE="40" MAXLENGTH="150" VALUE="<?php echo $name?>"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
        <TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_FORUMDESCRIPTION;?></FONT></TD>
        <TD><TEXTAREA NAME="desc" ROWS="15" COLS="45" WRAP="VIRTUAL"><?php echo $desc?></TEXTAREA></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
        <TD valign="top"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_MODERATOR;?></FONT></TD>
        <TD><b>Current:</b><BR>
<?php
	$sql = "SELECT u.uname, u.uid FROM ".$xoopsDB->prefix("users")." u, ".$xoopsDB->prefix("bb_forum_mods")." f WHERE f.forum_id = $forum AND u.uid = f.user_id";
      if(!$r = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      if($row = $xoopsDB->fetch_array($r)) {
	 do {
	    echo $row['uname']." (<input type=\"checkbox\" name=\"rem_mods[]\" value=\"".$row['uid']."\"> "._MD_A_REMOVE.")<BR>";
	    $current_mods[] = $row['uid'];
	 } while($row = $xoopsDB->fetch_array($r));
	 echo "<BR>";
      }
      else {
	 echo _MD_A_NOMODERATORASSIGNED."<BR><BR>\n";
      }
?>
	<b>Add:</b><BR>
	<SELECT NAME="mods[]" size="5" multiple>
<?php
	$sql = "SELECT uid, uname FROM ".$xoopsDB->prefix("users")." WHERE uid != 0 AND level > 0 ";
      while(list($null, $currMod) = each($current_mods)) {
	 $sql .= "AND uid != $currMod ";
      }
      $sql .= "ORDER BY uname";
      if(!$r = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      if($row = $xoopsDB->fetch_array($r)) {
	 do {
	    $s = "";
	    if($row['uid'] == $myrow['forum_moderator'])
	      $s = "SELECTED";
	    echo "<OPTION VALUE=\"".$row['uid']."\" $s>".$row['uname']."</OPTION>\n";
	 } while($row = $xoopsDB->fetch_array($r));
      }
      else {
	 echo "<OPTION VALUE=\"0\">"._MD_A_NONE."</OPTION>\n";
      }
?>
        </SELECT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
        <TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_CATEGORY;?></FONT></TD>
        <TD><SELECT NAME="cat">
<?php
	$sql = "SELECT * FROM ".$xoopsDB->prefix("bb_categories")."";
      if(!$r = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      if($row = $xoopsDB->fetch_array($r)) {
	 do {
	    $s = "";
	    if($row['cat_id'] == $myrow['cat_id'])
						$s = "SELECTED";
	    echo "<OPTION VALUE=\"".$row['cat_id']."\" $s>".$row['cat_title']."</OPTION>\n";
	 } while($row = $xoopsDB->fetch_array($r));
      }
      else {
	 echo "<OPTION VALUE=\"0\">"._MD_A_NONE."</OPTION>\n";
      }
?>
        </SELECT></TD>
<?php
if($myrow['forum_access'] == 1)
    $access1 = "SELECTED";
if($myrow['forum_access'] == 2)
    $access2 = "SELECTED";
if($myrow['forum_access'] == 3)
    $access3 = "SELECTED";
?>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
         <TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>">Access Level:</font></TD>
	 <TD><SELECT NAME="forum_access">
	     <OPTION VALUE="2" <?php echo $access2?>><?php echo _MD_A_ANONYMOUSPOST;?></OPTION>
	     <OPTION VALUE="1" <?php echo $access1?>><?php echo _MD_A_REGISTERUSERONLY;?></OPTION>
	     <OPTION VALUE="3" <?php echo $access3?>><?php echo _MD_A_MODERATORANDADMINONLY;?></OPTION>
	     </SELECT>
        </TD>
</TR>


<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_TYPE;?></FONT></TD>
	<TD><SELECT NAME="type">
<?php
	if($myrow['forum_type'] == 1)
		$priv = "SELECTED";
	else
		$pub = "SELECTED";
?>
	<OPTION VALUE="0" <?php echo $pub?>><?php echo _MD_A_PUBLIC;?></OPTION>
	<OPTION VALUE="1" <?php echo $priv?>><?php echo _MD_A_PRIVATE;?></OPTION>
	</SELECT>
	</TD>
</TR>
<?php

?>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
        <TD ALIGN="CENTER" COLSPAN="2">
                <INPUT TYPE="HIDDEN" NAME="mode" VALUE="editforum">
                <INPUT TYPE="HIDDEN" NAME="forum" VALUE="<?php echo $forum?>">
                <INPUT TYPE="SUBMIT" NAME="save" VALUE="<?php echo _MD_A_SAVECHANGES;?>">&nbsp;&nbsp;
                <INPUT TYPE="RESET" VALUE="<?php echo _MD_A_CLEAR;?>">
        </TD>
</TR>
</TR>
</TABLE></TD></TR></TABLE>

<?php
		}
		if(!isset($HTTP_POST_VARS['submit']) && !isset($HTTP_POST_VARS['save'])){
?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_SELECTFORUMEDIT;?></B><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><SELECT NAME="forum" SIZE="0">
	<?php

	$sql = "SELECT forum_name, forum_id FROM ".$xoopsDB->prefix("bb_forums")." ORDER BY forum_id";
	if($result = $xoopsDB->query($sql)) {
		if($myrow = $xoopsDB->fetch_array($result)) {
			do {
				$name = $myts->makeTboxData4Show($myrow['forum_name']);
				echo "<OPTION VALUE=\"".$myrow['forum_id']."\">$name</OPTION>\n";
			} while($myrow = $xoopsDB->fetch_array($result));
		}
		else {
			echo "<OPTION VALUE=\"-1\">"._MD_A_NOFORUMINDATABASE."</OPTION>\n";
		}
	}
	else {
		echo "<OPTION VALUE=\"-1\">"._MD_A_DATABASEERROR."</OPTION>\n";
	}

	?>
	</SELECT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2">
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="editforum">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_A_EDIT;?>">&nbsp;&nbsp;
	</TD>
</TR>
</TR>
</TABLE></TD></TR></TABLE>
<?php
		}
   break;
   case 'editcat':
    $myts = new MyTextSanitizer;
   	if(isset($HTTP_POST_VARS['submit']) && isset($HTTP_POST_VARS['save']))
   	{
			$new_title = $myts->makeTboxData4Save($HTTP_POST_VARS['new_title']);
			$sql = "UPDATE ".$xoopsDB->prefix("bb_categories")." SET cat_title = '$new_title' WHERE cat_id = $cat_id";
			if(!$result = $xoopsDB->query($sql,1))
   		{
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
   		}
   		else
   		{
   			echo "<TABLE width=\"95%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bordercolor=\"".$xoopsTheme['bgcolor2']."\">";
	 			echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><B>"._MD_A_CATEGORYUPDATED."</B></font></td>";
	 			echo "</tr><TR><TD><TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><TR>";
	 			echo "<td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><P><a href=\"$url_admin_index\">"._MD_A_RETURNTOADMINPANEL."</a></p><p><a href=\"$url_phpbb_index\">"._MD_A_RETURNTOFORUMINDEX."</font></p></TD>";
	 			echo "</tr></table></td></tr></table>";
	 		}

   	}
   	else if(isset($HTTP_POST_VARS['submit']) && $HTTP_POST_VARS['submit'] != "")
   	{
   		$sql = "SELECT cat_title FROM ".$xoopsDB->prefix("bb_categories")." WHERE cat_id = '$cat'";
   		if(!$result = $xoopsDB->query($sql,1))
   		{
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
   		}
   		$cat_data = $xoopsDB->fetch_array($result);
   		$cat_title = $myts->makeTboxData4Edit($cat_data["cat_title"]);
?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_EDITCATEGORY;?> <?php echo $cat_title ?></B><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<td><?php echo _MD_A_CATEGORYTITLE;?></td>
	<td><input type="text" name="new_title" value="<?php echo $cat_title ?>" size="45" maxlength="100"></td>
</tr>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2">
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="editcat">
		<input type="hidden" name="save" value="TRUE">
		<input type="hidden" name="cat_id" value="<?php echo $cat?>">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_A_SAVECHANGES;?>">
	</td>
</tr>
</tr>
</table></td></tr></table>
<?php
   	}
   	else {
   		$sql = "SELECT cat_id, cat_title FROM ".$xoopsDB->prefix("bb_categories")." ORDER BY cat_order";
   		if(!$result = $xoopsDB->query($sql,1))
   		{
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
   		}
?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT color="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_SELECTACATEGORYEDIT;?></B><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><SELECT NAME="cat" SIZE="0">
<?php
			while($cat_data = $xoopsDB->fetch_array($result))
			{
				echo "<option value=\"".$cat_data["cat_id"]."\">".$myts->makeTboxData4Show($cat_data["cat_title"])."</option>\n";
			}
?>
</select></td>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2">
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="editcat">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_A_EDIT;?>">&nbsp;&nbsp;
	</TD>
</TR>
</TR>
</TABLE></TD></TR></TABLE>
<?php
   	}
   break;
 case 'remcat':
    $myts = new MyTextSanitizer;
   if(isset($HTTP_POST_VARS['submit']) && $HTTP_POST_VARS['submit'] != "") {
      $sql = "DELETE FROM ".$xoopsDB->prefix("bb_categories")." WHERE cat_id = $cat";
      if(!$r = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      echo "<TABLE width=\"95%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bordercolor=\"".$xoopsTheme['bgcolor2']."\">";
      echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><B>"._MD_A_CATEGORYCREATED."</B></font></td>";
      echo "</tr><tr><td><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><TR>";
      echo "<td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><p><a href=\"$url_admin_index\">"._MD_A_RETURNTOADMINPANEL."</a></p><p><a href=\"$url_phpbb_index\">"._MD_A_RETURNTOFORUMINDEX."</a></p></font></td>";
      echo "</tr></table></td></tr></table>";

   }
   else {
?>
	<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
	<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
	<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
	<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_RMVACAT;?></B></FONT></TD>
	</TR>
	<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><i><?php echo _MD_A_NTWNRTFUTCYMDTVTEFS;?></i></FONT></TD>
	</TR>
	<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>">
	<SELECT NAME="cat">

<?php
      $sql = "SELECT * FROM ".$xoopsDB->prefix("bb_categories")." ORDER BY cat_title";
      if(!$r = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      while($m = $xoopsDB->fetch_array($r)) {
	 echo "<OPTION VALUE=\"".$m['cat_id']."\">".$myts->makeTboxData4Show($m['cat_title'])."</OPTION>\n";
      }
?>
	</SELECT>
	<INPUT TYPE="HIDDEN" NAME="mode" VALUE="<?php echo $mode ?>"></TD>
	</TR>
	<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>">
	<td align="center" colspan="2"><font color="<?php echo $xoopsTheme["textcolor2"]?>">
	<input type="submit" name="submit" value="<?php echo _MD_A_REMOVECATEGORY;?>"></td></tr>
	</table></table></form>
<?php
   }
   break;
 case 'addcat':
    $myts = new MyTextSanitizer;
   if(isset($HTTP_POST_VARS['submit']) && $HTTP_POST_VARS['submit'] != "") {
	$nextid = $xoopsDB->GenID("bb_categories_cat_id_seq");
      $sql = "SELECT max(cat_order) AS highest FROM ".$xoopsDB->prefix("bb_categories")."";
      if(!$r = $xoopsDB->query($sql,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
	}
      list($highest) = $xoopsDB->fetch_row($r);
      $highest++;
      $title = $myts->makeTboxData4Save($title);
      $sql = "INSERT INTO ".$xoopsDB->prefix("bb_categories")." (cat_id, cat_title, cat_order) VALUES ($nextid, '$title', '$highest')";
      if(!$result = $xoopsDB->query($sql,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
	}
      echo "<TABLE width=\"95%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bordercolor=\"".$xoopsTheme['bgcolor2']."\">";
      echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><b>"._MD_A_CATEGORYCREATED."</B></font></td>";
      echo "</tr><tr><td><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
      echo "<td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><p><a href=\"$url_admin_index\">"._MD_A_RETURNTOADMINPANEL."</a></p><p><a href=\"$url_phpbb_index\">"._MD_A_RETURNTOFORUMINDEX."</a></p></font></td>";
      echo "</tr></table></td></tr></table>";
   }
   else {
?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_CREATENEWCATEGORY;?></b></font></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_CATEGORYTITLE;?></FONT></TD>
	<td><input type="text" name="title" size="40" maxlength="100"></td>
</tr>
<tr bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>" align="left">
	<td align="center" colspan="2">
		<input type="hidden" name="mode" value="addcat">
		<input type="submit" name="submit" value="<?php echo _MD_A_CREATENEWCATEGORY;?>">&nbsp;&nbsp;
		<input type="reset" value="<?php echo _MD_A_CLEAR;?>">
	</td>
</tr>
</tr>
</table></td></tr></table>
<?
		}
   break;
 case 'addforum':
    $myts = new MyTextSanitizer;
   if(isset($HTTP_POST_VARS['submit']) && $HTTP_POST_VARS['submit'] != "") {
      if($name == '' || $desc == '' || !is_array($mods)){
			echo _MD_A_YDNFOATPOTFDYAA;
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
	}
      	$desc = $myts->makeTareaData4Save($HTTP_POST_VARS['desc']);
      	$name = $myts->makeTboxData4Save($HTTP_POST_VARS['name']);
	$nextid = $xoopsDB->GenID("bb_forums_forum_id_seq");
	$sql = "INSERT INTO ".$xoopsDB->prefix("bb_forums")." (forum_id, forum_name, forum_desc, forum_access, cat_id, forum_type) VALUES ($nextid, '$name', '$desc', $forum_access, $cat, $type)";

      if(!$result = $xoopsDB->query($sql,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
	}
	if($nextid == 0){
		$nextid = $xoopsDB->Insert_ID();
	}
      $count = 0;

      while(list($mod_number, $mod) = each($HTTP_POST_VARS["mods"])) {
	 		$mod_data = new XoopsUser($mod);

	 		if($mod_data->is_active() && $mod_data->level() < 2) {
	    		if(!isset($user_query))
	      		$user_query = "UPDATE ".$xoopsDB->prefix("users")." SET level = 2 WHERE ";
	    	if($count > 0)
	      	$user_query .= "OR ";
	    	$user_query .= "uid = $mod ";
	    	$count++;
	 		}
	 		$mod_query = "INSERT INTO ".$xoopsDB->prefix("bb_forum_mods")." (forum_id, user_id) VALUES ($nextid, $mod)";
	 		if(!$xoopsDB->query($mod_query,1)){
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
    	}

    if(isset($user_query)) {
	 	if(!$xoopsDB->query($user_query,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
    }
      echo "<table width=\"95%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bordercolor=\"".$xoopsTheme['bgcolor2']."\">";
      echo "<tr><td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><b>"._MD_A_FORUMCREATED."</b></font></td>";
      echo "</tr><tr><td><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><TR>";
      echo "<td align=\"center\" width=\"100%\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><p><a href=\"$url_admin_index\">"._MD_A_RETURNTOADMINPANEL."</a></p><p><a href=\"".$url_phpbb."viewforum.php?forum=$nextid\">"._MD_A_VTFYJC."</a></p></font></TD>";
      echo "</tr></table></td></tr></table>";
   }
   else {
      $sql = "SELECT count(*) AS total FROM ".$xoopsDB->prefix("bb_categories")."";
      if(!$r = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      list($total) = $xoopsDB->fetch_row($r);
      if($total < 1 || !isset($total)){
		echo _MD_A_EYMAACBYAF;
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      ?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_CREATENEWFORUM;?></B></FONT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_FORUMNAME;?></FONT></TD>
	<TD><INPUT TYPE="TEXT" NAME="name" SIZE="40" MAXLENGTH="150"></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_FORUMDESCRIPTION;?></FONT></TD>
	<TD><TEXTAREA NAME="desc" ROWS="15" COLS="45" WRAP="VIRTUAL"></TEXTAREA></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_MODERATOR;?></FONT></TD>
	<TD><SELECT NAME="mods[]" size="5" multiple>
<?php
	$sql = "SELECT uid, uname FROM ".$xoopsDB->prefix("users")." WHERE uid != 0 AND level > 0 ORDER BY uname";
      if(!$result = $xoopsDB->query($sql,1)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
      if($myrow = $xoopsDB->fetch_array($result)) {
	 do {
	    echo "<OPTION VALUE=\"".$myrow['uid']."\">".$myrow['uname']."</OPTION>\n";
	 } while($myrow = $xoopsDB->fetch_array($result));
      }
      else {
	 echo "<OPTION VALUE=\"0\">"._MD_A_NONE."</OPTION>\n";
      }
?>
	</SELECT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	<TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_CATEGORY;?></FONT></TD>
	<TD><SELECT NAME="cat">
<?php
			$sql = "SELECT * FROM ".$xoopsDB->prefix("bb_categories")."";
			if(!$result = $xoopsDB->query($sql,1)){
				CloseTable();
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
			if($myrow = $xoopsDB->fetch_array($result)) {
				do {
					echo "<OPTION VALUE=\"".$myrow['cat_id']."\">".$myrow['cat_title']."</OPTION>\n";
				} while($myrow = $xoopsDB->fetch_array($result));
			}
			else {
				echo "<OPTION VALUE=\"0\">"._MD_A_NONE."</OPTION>\n";
			}
?>
	</SELECT></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
	 <TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_ACCESSLEVEL;?></font></TD>
	 <TD><SELECT NAME="forum_access">
	     <OPTION VALUE="2"><?php echo _MD_A_ANONYMOUSPOST;?></OPTION>
	     <OPTION VALUE="1"><?php echo _MD_A_REGISTERUSERONLY;?></OPTION>
	     <OPTION VALUE="3"><?php echo _MD_A_MODERATORANDADMINONLY;?></OPTION>
	     </SELECT>
	 </TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor1'];?>" ALIGN="LEFT">
        <TD><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><?php echo _MD_A_TYPE;?></FONT></TD>
        <TD><SELECT NAME="type">
        <OPTION VALUE="0"><?php echo _MD_A_PUBLIC;?></OPTION>
        <OPTION VALUE="1"><?php echo _MD_A_PRIVATE;?></OPTION>
        </SELECT>
        </TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
	<TD ALIGN="CENTER" COLSPAN="2">
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="addforum">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_A_CREATENEWFORUM;?>">&nbsp;&nbsp;
		<INPUT TYPE="RESET" VALUE="<?php echo _MD_A_CLEAR;?>">
	</td>
</tr>
</tr>

</table></td></tr></table>
<?php
		}
   break;
 case 'catorder':
    $myts = new MyTextSanitizer;
//    update catagories set cat_order = cat_order + 1 WHERE cat_order >= 2; update catagories set cat_order = cat_order - 2 where cat_id = 3;

      if(isset($up) && $up != "") {
	 if($current_order != "1") {
	    $order = $current_order - 1;
	    $sql1 = "UPDATE ".$xoopsDB->prefix("bb_categories")." SET cat_order = $order WHERE cat_id = $cat_id";
	    if(!$r = $xoopsDB->query($sql1,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
	    $sql2 = "UPDATE ".$xoopsDB->prefix("bb_categories")." SET cat_order = $current_order WHERE cat_id = $last_id";
	    if(!$r = $xoopsDB->query($sql2,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
	    echo "<div align=\"center\"><font color=\"".$xoopsTheme["textcolor2"]."\">"._MD_A_CATEGORYMOVEUP."</font></div><BR>";
	 }
	 else
	   echo "<div align=\"center\"><font color=\"".$xoopsTheme["textcolor2"]."\">"._MD_A_TCIATHU."</font></div><br>";

      }
      else if(isset($down) && $down != "") {
	 $sql = "SELECT cat_order FROM ".$xoopsDB->prefix("bb_categories")." ORDER BY cat_order DESC";
	 if(!$r  = $xoopsDB->query($sql,1,1,0)){
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
	 list($last_number) = $xoopsDB->fetch_row($r);
	 if($last_number != $current_order) {
	    $order = $current_order + 1;
	    $sql = "UPDATE ".$xoopsDB->prefix("bb_categories")." SET cat_order = $current_order WHERE cat_order = $order";
	    if(!$r  = $xoopsDB->query($sql,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
	    $sql = "UPDATE ".$xoopsDB->prefix("bb_categories")." SET cat_order = $order where cat_id = $cat_id";
	    if(!$r  = $xoopsDB->query($sql,1)){
			CloseTable();
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
	    echo "<div align=\"center\"><font color=\"".$xoopsTheme["textcolor2"]."\">"._MD_A_CATEGORYMOVEDOWN."</font></div><br>";

	 }
	 else
	   echo "<div align=\"center\"><font color=\"".$xoopsTheme["textcolor2"]."\">"._MD_A_TCIATLD."</font></div><br>";
      }

?>
     <FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
     <TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme['bgcolor2']?>">
     <TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
     <TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="LEFT">
     <TD ALIGN="CENTER" COLSPAN="3"><FONT COLOR="<?php echo $xoopsTheme["textcolor2"]?>"><B><?php echo _MD_A_SETCATEGORYORDER;?></B></FONT><BR>
     <?php echo _MD_A_TODHITOTCWDOTIP;?><BR>
     <?php echo _MD_A_ECWMTCPUODITO;?></TD>
     </TR>
     <TR BGCOLOR="<?php echo $xoopsTheme['bgcolor3'];?>" ALIGN="CENTER">
     <TD><?php echo _MD_A_CATEGORY1;?></TD><TD><?php echo _MD_A_MOVEUP;?></TD><TD><?php echo _MD_A_MOVEDOWN;?></TD>
     </TR>
<?php
     $sql = "SELECT * FROM ".$xoopsDB->prefix("bb_categories")." ORDER BY cat_order";
   if(!$r = $xoopsDB->query($sql,1)) {
      exit();
   }
   while($m = $xoopsDB->fetch_array($r)) {
      echo "<!-- New Row -->\n";
      echo "<FORM ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
      echo "<tr bgcolor=\"".$xoopsTheme['bgcolor1']."\" align=\"center\">\n";
      echo "<td>".$myts->makeTboxData4Show($m['cat_title'])."</TD>\n";
      echo "<td><input type=\"hidden\" name=\"mode\" value=\"$mode\">\n";
      echo "<input type=\"hidden\" name=\"cat_id\" value=\"".$m['cat_id']."\">\n";
      echo "<input type=\"hidden\" name=\"last_id\" value=\"";
      if(isset($last_id)){
	echo $last_id;
      }
      echo "\">\n";
      echo "<input type=\"hidden\" name=\"current_order\" value=\"".$m['cat_order']."\"><input type=\"submit\" name=\"up\" value=\""._MD_A_MOVEUP."\"></td>\n";
      echo "<td><input type=\"submit\" name=\"down\" value=\""._MD_A_MOVEDOWN."\"></td></tr></form>\n<!-- End of Row -->\n";
      $last_id = $m['cat_id'];
   }
?>
     </TABLE></TABLE>
<?php
   break;

}

include('../page_tail.php');
CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>