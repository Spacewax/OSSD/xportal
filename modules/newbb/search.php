<?php
/***************************************************************************
                          search.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

    $Id: search.php,v 1.17 2001/04/04 14:01:11 bartvb Exp $
	
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
$pagetype = "search";
include("header.php");
include($xoopsConfig['root_path']."header.php");
if($submit){
	$pagetype = "searchresults";
}else{
	$pagetype = "search";
}
include('page_header.php');

if(!$submit)
{
?>
<form name="Search" action="<?php echo $PHP_SELF; ?>" method="post">
<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="<?php echo $tablewidth?>">
<tr>
	<td  bgcolor="<?php echo $xoopsTheme["bgcolor2"];?>">
	<table border="0" cellpadding="1" cellspacing="1" width="100%" bgcolor="<?php echo $xoopsTheme["bgcolor3"]; ?>">
	<tr>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"]?>" width="50%" align="RIGHT">
		<b><?php echo _MD_KEYWORDS;?></b>&nbsp;
	</td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>" width="50%">
		<input type="text" name="term"></input>
	</td>
	</tr>
	<tr>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" width="50%">&nbsp;</td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>" width="50%">
		<input type="radio" name="addterms" value="any" checked>
		<?php echo _MD_SEARCHANY;?>
	</td>
	</tr>
	<tr>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" width="50%">&nbsp;</td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>" width="50%">
		<input type="radio" name="addterms" value="all">
		<?php echo _MD_SEARCHALL;?>
	</td>
	</tr>
	<tr>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" width="50%" align="right">
		<b><?php echo _MD_FORUMC;?></b>&nbsp;
	</td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>" width="50%">
		<select name="forum">
		<option value="all"><?php echo _MD_SEARCHALLFORUMS?></option>
		<?php
			$query = "SELECT forum_name,forum_id FROM ".$xoopsDB->prefix(bb_forums)." WHERE forum_type != 1";
			if(!$result = $xoopsDB->query($query))
			{
				die("<big>"._MD_ERROROCCURED."</big><hr>"._MD_COULDNOTQUERY."");
			}
			while($row = $xoopsDB->fetch_array($result))
			{
				echo "<option value=".$row['forum_id'].">".$row['forum_name']."</option>";
			}
		?>
		</select>
	</td>
	</tr>
	<tr>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" width="50%" align="right">
		<?php echo "<b>"._MD_AUTHORC."</b>&nbsp;";
                ?>
	</td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>" width="50%">
		<input type="text" name="search_username">
	</td>
	</tr>
	<tr>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" width="50%" align="RIGHT">
		<b><?php echo _MD_SORTBY?></b>&nbsp;
	</td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>" width="50%">
	<?php //All values are the fields used to search the database - a table must be specified for each field ?>
		<input type="radio" name="sortby" value="p.post_time desc" CHECKED><?php echo _MD_DATE;?></input>
		&nbsp;&nbsp;
		<input type="radio" name="sortby" value="t.topic_title"><?php echo _MD_TOPIC?></input>
		&nbsp;&nbsp;
		<input type="radio" name="sortby" value="f.forum_name"><?php echo _MD_FORUM?></input>
		&nbsp;&nbsp;
		<input type="radio" name="sortby" value="u.uname"><?php echo _MD_USERNAME;?></input>
		&nbsp;&nbsp;
	</td>
	</tr> 

<?php 
// 25oct00 dsig -add radio to determine what to search title or text or both..default both 
?> 
   <tr> 
   	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" width="50%" align="right"> 
        <b><?php echo _MD_SEARCHIN;?></b>&nbsp;
       	</td> 
        <td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>" width="50%"> 
<?php 
/* 
26oct00 dsig added note
//           on default to change default 'checked' item simply move the 'CHECKED' keyword 
//           from one 'radio' to another. 
*/ 
?> 
      <input type="radio" name="searchboth" value="both" checked></input><?php echo _MD_SUBJECT." & "._MD_BODY."";?></input>
      <input type="radio" name="searchboth" value="title"></input><?php echo _MD_SUBJECT;?>
      <input type="radio" name="searchboth" value="text"></input><?php echo _MD_BODY;?>
      </td> 
  </tr>      
</table>

	</td>
</tr>
</table>
<br />
	<div align="center">
	<input type="submit" name="submit" value="<?php echo _MD_SEARCH;?>">
	</form>
	</div>

<?php
}
else  // Submitting query
{
	include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
	$myts = new MyTextSanitizer();
/**********
 Sept 6.
 $query is the basis of the query
 $addquery is all the additional search fields - necessary because of the WHERE clause in SQL
**********/

$query = "SELECT u.uid,f.forum_id, p.topic_id, u.uname, p.post_time,t.topic_title,f.forum_name FROM ".$xoopsDB->prefix("bb_posts")." p, ".$xoopsDB->prefix("bb_posts_text")." pt, ".$xoopsDB->prefix("users")." u, ".$xoopsDB->prefix("bb_forums")." f,".$xoopsDB->prefix("bb_topics")." t";
if(isset($term) && $term != "")
{
	$terms = split(" ",addslashes($term));				// Get all the words into an array
	$addquery .= "(pt.post_text LIKE '%$terms[0]%'";		
	$subquery .= "(t.topic_title LIKE '%$terms[0]%'"; 
	
	if($addterms=="any")					// AND/OR relates to the ANY or ALL on Search Page
		$andor = "OR";
	else
		$andor = "AND";
	$size = sizeof($terms);
	for($i=1;$i<$size;$i++) {
		$addquery.=" $andor pt.post_text LIKE '%$terms[$i]%'";
		$subquery.=" $andor t.topic_title LIKE '%$terms[$i]%'"; 
	}	     
	$addquery.=")";
	$subquery.=")";
}
if(isset($forum) && $forum!="all")
{
	if(isset($addquery)) {
	   $addquery .= " AND ";
	   $subquery .= " AND ";
	}
	
	$addquery .=" p.forum_id=$forum";
	$subquery .=" p.forum_id=$forum";
}
if(isset($search_username)&&$search_username!="")
{
	$search_username = addslashes($search_username);
   if(!$result = $xoopsDB->query("SELECT uid FROM ".$xoopsDB->prefix("users")." WHERE uname='$search_username'"))
	{
		error_die("<big>"._MD_ERROROCCURED."</big><hr>"._MD_COULDNOTQUERY."");
	}
   $row = $xoopsDB->fetch_array($result);
   if(!$row)
	{
		error_die(_MD_USERNOEXIST);
	}
   $userid = $row['uid'];
   if(isset($addquery)) {
      $addquery.=" AND p.uid=$userid AND u.uname='$search_username'";
      $subquery.=" AND p.uid=$userid AND u.uname='$search_username'";
   }
   else {
      $addquery.=" p.uid=$userid AND u.uname='$search_username'";
      $subquery.=" p.uid=$userid AND u.uname='$search_username'";
   }
}	
if(isset($addquery)) {
   switch ($searchboth) { 
    case "both" : 
      $query .= " WHERE ( $subquery OR $addquery ) AND "; 
      break; 
    case "title" : 
      $query .= " WHERE ( $subquery ) AND "; 
      break; 
    case "text" : 
      $query .= " WHERE ( $addquery ) AND "; 
      break; 
   }
}
else
{
     $query.=" WHERE ";
}

   $query .= " p.post_id = pt.post_id 
						AND p.topic_id = t.topic_id 
						AND p.forum_id = f.forum_id 
						AND p.uid = u.uid 
						AND f.forum_type != 1";
//  100100 bartvb  Uncomment the following GROUP BY line to show matching topics instead of all matching posts.
//   $query .= " GROUP BY t.topic_id";
   $query .= " ORDER BY $sortby";

	if(!$result = $xoopsDB->query($query,1,200,0))
	{
		die("<big>"._MD_ERROROCCURED."</big><hr>"._MD_COULDNOTQUERY);
	}

	if(!$row = $xoopsDB->fetch_array($result))
	{
		die(_MD_NOMATCH);
	}

?>
<table border="0" cellpadding="1" cellspacing="0" align="center" valign="TOP" width="95%"><tr><td  bgcolor="<?php echo $xoopsTheme["bgcolor2"]; ?>">
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<tr bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" align="left">
        <td align="center" width="30%"><b><?php echo _MD_FORUM;?></b></td>
        <td align="center" width="30%"><b><?php echo _MD_TOPIC;?></b></td>
        <td align="center" width="25%"><b><?php echo _MD_AUTHOR;?></b></td>
        <td align="center" width="15%"><b><?php echo _MD_POSTTIME?></b></td>
</tr>
<?php
	do {
		echo "<tr bgcolor=\"".$xoopsTheme['bgcolor1']."\">";
		echo "<td align=\"center\" width=\"30%\"><a href=\"viewforum.php?forum=".$row['forum_id']."\">". $myts->makeTboxData4Show($row['forum_name']) . "</a></td>";
		echo "<td align=\"center\" width=\"30%\"><a href=\"viewtopic.php?topic_id=".$row['topic_id']."&amp;forum=".$row['forum_id']."\">". $myts->makeTboxData4Show($row['topic_title']) . "</a></td>";
		echo "<td align=\"center\" width=\"25%\"><a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$row['uid']."\">".$myts->makeTboxData4Show($row['uname'])."</a></td>";
		echo "<td align=\"center\" width=\"15%\">".formatTimestamp($row['post_time'])."</td>";
		echo "</tr>";
	}while($row=$xoopsDB->fetch_array($result));
?>	

</table>
</tr>
</tr>
</table>
<?php
}
	include('page_tail.php');
	include($xoopsConfig['root_path']."footer.php");
?>