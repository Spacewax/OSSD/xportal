<?php
// ------------------------------------------------------------------------- //
//            myPHPNuke SE 1.9  - PHP Content Management System              //
//                     <http://www.myphpnuke-se.com/>                        //
// ------------------------------------------------------------------------- //
// Based on:								     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
/***************************************************************************
Original-                        config.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
 	 email                : support@phpbb.com

 ***************************************************************************/

// You shouldn't have to change any of these
$url_phpbb = $xoopsConfig['xoops_url']."/modules/newbb/";
$url_admin = $url_phpbb."admin";
$url_images = $url_phpbb."images";
$url_smiles = $url_images."/smiles";
$url_phpbb_index = $url_phpbb."index.php";
$url_admin_index = $url_admin."/index.php";

/* -- Cookie settings (lastvisit) -- */
// Most likely you can leave this be, however if you have problems
// logging into the forum set this to your domain name, without
// the http://
// For example, if your forum is at http://www.mysite.com/phpBB then
// set this value to
// $cookiedomain = "www.mysite.com";
$cookiedomain = "";

// It should be safe to leave these alone as well.
$cookiepath = $url_phpbb;
$cookiesecure = false;


/* -- You shouldn't have to change anything after this point */
/* -- Images -- */

$folder_image = $url_images."/folder.gif";
$hot_folder_image = $url_images."/hot_folder.gif";
$newposts_image = $url_images."/red_folder.gif";
$hot_newposts_image = $url_images."/hot_red_folder.gif";


$locked_image = $url_images."/lock.gif";
$locktopic_image = $url_images."/lock_topic.gif";
$deltopic_image = $url_images."/del_topic.gif";
$movetopic_image = $url_images."/move_topic.gif";
$unlocktopic_image = $url_images."/unlock_topic.gif";

/* -- Other Settings -- */
$phpbbversion = "1.4.4";
?>