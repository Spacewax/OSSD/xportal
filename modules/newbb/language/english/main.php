<?php

//%%%%%%		Module Name phpBB  		%%%%%
//functions.php
define("_MD_ERROR","Error");
define("_MD_NOPOSTS","No Posts");
define("_MD_SELFORUM","Select a Forum");
define("_MD_GO","Go");

//auth.php
define("_MD_BANNED","You have been banned from this forum. Contact the system administrator if you have any questions.");

//index.php
define("_MD_FORUM","Forum");
define("_MD_WELCOME","Welcome to %s Forum.");
define("_MD_TOPICS","Topics");
define("_MD_POSTS","Posts");
define("_MD_LASTPOST","Last Post");
define("_MD_MODERATOR","Moderator");
define("_MD_NEWPOSTS","New posts since your last visit");
define("_MD_NONEWPOSTS","No new posts since your last visit");
define("_MD_BY","by"); // Posted by
define("_MD_TOSTART","To start viewing messages, select the forum that you want to visit from the selection below.");
define("_MD_TOTALTOPICSC","Total Topics: ");
define("_MD_TOTALPOSTSC","Total Posts: ");
define("_MD_TIMENOW","The time now is %s");
define("_MD_LASTVISIT","You last visited: %s");
define("_MD_ADVSEARCH","Advanced Search");

//page_header.php
define("_MD_MODERATEDBY","Moderated by");
define("_MD_SEARCH","Search");
define("_MD_SEARCHRESULTS","Search Results");
define("_MD_FORUMINDEX","%s Forum Index");

//page_tail.php
define("_MD_ADMINPANEL","Administration Panel");
define("_MD_POWEREDBY","Powered by");
define("_MD_VERSION","Version");
define("_MD_ENHANCEDBY","Enhanced by");

//search.php
define("_MD_KEYWORDS","Keywords:");
define("_MD_SEARCHANY","Search for ANY of the terms (Default)");
define("_MD_SEARCHALL","Search for ALL of the terms");
define("_MD_SEARCHALLFORUMS","Search All Forums");
define("_MD_FORUMC","Forum:");
define("_MD_AUTHORC","Author:");
define("_MD_SORTBY","Sort by:");
define("_MD_DATE","Date");
define("_MD_TOPIC","Topic");
define("_MD_USERNAME","Username");
define("_MD_SEARCHIN","Search in:");
define("_MD_SUBJECT","Subject");
define("_MD_BODY","Body");
define("_MD_NOMATCH","No records match that query. Please broaden your search.");
define("_MD_POSTTIME","Post Time");

//viewforum.php
define("_MD_REPLIES","Replies");
define("_MD_POSTER","Poster");
define("_MD_VIEWS","Views");
define("_MD_GOTOPAGE","Goto page"); // Goto page 1,2,3...
define("_MD_MORETHAN","More than %s posts");
define("_MD_NOTOPICS","There are no topics for this forum. You can post one.");
define("_MD_TOPICLOCKED","Topic is Locked (No new posts may be made in it)");
define("_MD_NEXTPAGE","Next Page");
define("_MD_SORTEDBY","Sorted by");
define("_MD_TOPICTITLE","topic title");
define("_MD_NUMBERREPLIES","number of replies");
define("_MD_NUMBERVIEWS","number of views");
define("_MD_TOPICPOSTER","topic poster");
define("_MD_LASTPOSTTIME","last post time");
define("_MD_ASCENDING","Ascending order");
define("_MD_DESCENDING","Descending order");
define("_MD_FROMLASTDAYS","From last %s days");
define("_MD_LASTDAY","From the last day");  
define("_MD_THELASTYEAR","From the last year");
define("_MD_BEGINNING","From the beginning");

//viewtopic.php
define("_MD_AUTHOR","Author");
define("_MD_THREAD","Thread");
define("_MD_LOCKTOPIC","Lock this topic");
define("_MD_UNLOCKTOPIC","Unlock this topic");
define("_MD_MOVETOPIC","Move this topic");
define("_MD_DELETETOPIC","Delete this topic");
define("_MD_TOP","Top");
define("_MD_PARENT","Parent");

//forumform.inc
define("_MD_ABOUTPOST","About Posting");
define("_MD_ANONCANPOST","<b>Anonymous</b> users can post new topics and replies to this forum");
define("_MD_REGCANPOST","All <b>Registered</b> users can post new topics and replies to this forum");
define("_MD_MODSCANPOST","Only <B>Moderators and Administrators</b> can post new topics and replies to this forum");
define("_MD_PREVPAGE","Previous Page");
define("_MD_QUOTE","Quote");

// ERROR messages
define("_MD_ERRORFORUM","ERROR: Forum not selected!");
define("_MD_ERRORPOST","ERROR: Post not selected!");
define("_MD_NORIGHTTOPOST","You don't have the right to post in this forum.");
define("_MD_NORIGHTTOACCESS","You don't have the right to access this forum.");
define("_MD_ERRORTOPIC","ERROR: Topic not selected!");
define("_MD_ERRORCONNECT","ERROR: Could not connect to the forums database.");
define("_MD_ERROREXIST","ERROR: The forum you selected does not exist. Please go back and try again.");
define("_MD_CANTGETFORUM","Can't get forum data.");
define("_MD_ERROROCCURED","An Error Occured");
define("_MD_COULDNOTQUERY","Could not query the forums database.");
define("_MD_FORUMNOEXIST","Error - The forum/topic you selected does not exist. Please go back and try again.");
define("_MD_USERNOEXIST","That user does not exist.  Please go back and search again.");
define("_MD_COULDNOTREMOVE","Error - Could not remove posts from the database!");
define("_MD_COULDNOTREMOVETXT","Error - Could not remove post texts!");

//reply.php
define("_MD_ON","on"); //Posted on
define("_MD_USERWROTE","%s wrote:"); // %s is username

//post.php
define("_MD_EDITNOTALLOWED","You're not allowed to edit this post!");
define("_MD_EDITEDBY","Edited by");
define("_MD_ANONNOTALLOWED","Anonymous user not allowed to post.<br>Please register.");
define("_MD_THANKSSUBMIT","Thanks for your submission!");
define("_MD_REPLYPOSTED","A reply to your topic has been posted.");
define("_MD_HELLO","Hello %s,");
define("_MD_URRECEIVING","You are receiving this email because a message you posted on %s forums has been replied to."); // %s is your site name
define("_MD_CLICKBELOW","Click on the link below to view the thread:");

//navbar.php
define("_MD_FLAT","Flat");
define("_MD_THREADED","Threaded");
define("_MD_OLDFIRST","Oldest First");
define("_MD_NEWFIRST","Newest First");

//forumform.inc
define("_MD_YOURNAME","Your Name:");
define("_MD_LOGOUT","Logout");
define("_MD_REGISTER","Register");
define("_MD_SUBJECTC","Subject:");
define("_MD_MESSAGEICON","Message Icon:");
define("_MD_MESSAGEC","Message:");
define("_MD_CHECKMESSAGE","check message length");
define("_MD_ALLOWEDHTML","Allowed HTML:");
define("_MD_OPTIONS","Options:");
define("_MD_POSTANONLY","Post Anonymously");
define("_MD_DISABLESMILEY","Disable Smiley");
define("_MD_DISABLEHTML","Disable html");
define("_MD_EMAILNOTIFY","Email Notification");
define("_MD_ATTACHSIG","Attach Signature");
define("_MD_PREVIEW","Preview");
define("_MD_POST","Post");
define("_MD_SUBMIT","Submit");
define("_MD_CANCELPOST","Cancel Post");

// forumuserpost.php
define("_MD_JOINEDC","Joined:");
define("_MD_POSTSC","Posts:");
define("_MD_FROMC","From:"); //user location
define("_MD_LOGGED","Logged");
define("_MD_PROFILE","Profile");
define("_MD_SENDEMAIL","Send Email To %s");
define("_MD_SENDPM","Send Private Message To %s");
define("_MD_VISITSITE","Visit Website");
define("_MD_ADDTOLIST","Add to Contact List");
define("_MD_ADD","Add");
define("_MD_EDITTHISPOST","Edit this post");
define("_MD_REPLY","Reply");

// topicmanager.php
define("_MD_YANTMOTFTYCPTF","You are not the moderator of this forum therefore you cannot perform this function.");
define("_MD_TTHBRFTD","The topic has been removed from the database.");
define("_MD_RETURNTOTHEFORUM","Return to the forum");
define("_MD_RTTFI","Return to the forum index");
define("_MD_EPGBATA","Error - Please go back and try again.");
define("_MD_TTHBM","The topic has been moved.");
define("_MD_VTUT","View the updated topic");
define("_MD_TTHBL","The topic has been locked.");
define("_MD_VIEWTHETOPIC","View the topic");
define("_MD_TTHBU","The topic has been unlocked.");
define("_MD_ENSUOPITD","Error - No such user or post in the database.");
define("_MD_UIAAI","Users IP and Account information");
define("_MD_USERIP","User IP:");
define("_MD_UOUTPFTIAPC","Usernames of users that posted from this IP + post counts");
define("_MD_OYPTDBATBOTFTTY","Once you press the delete button at the bottom of this form the topic you have selected, and all its related posts, will be <b>permanently</b> removed.");
define("_MD_OYPTMBATBOTFTTY","Once you press the move button at the bottom of this form the topic you have selected, and its related posts, will be moved to the forum you have selected.");
define("_MD_OYPTLBATBOTFTTY","Once you press the lock button at the bottom of this form the topic you have selected will be locked. You may unlock it at a later time if you like.");
define("_MD_OYPTUBATBOTFTTY","Once you press the unlock button at the bottom of this form the topic you have selected will be unlocked. You may lock it again at a later time if you like.");
define("_MD_VTUIA","View this users IP address.");
define("_MD_MOVETOPICTO","Move Topic To:");
define("_MD_NOFORUMINDB","No Forums in DB");
define("_MD_DATABASEERROR","Database Error");
define("_MD_DELTOPIC","Delete Topic");
define("_MD_VIEWIP","View IP");
?>