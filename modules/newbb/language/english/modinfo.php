<?php
// Module Info

// The name of this module
define("_MI_NEWBB_NAME","Forum");

// A brief description of this module
define("_MI_NEWBB_DESC","An enhanced version of phpBB 1.4.4");

// Names of blocks for this module (Not all module has blocks)
define("_MI_NEWBB_BNAME1","Recent Discussions in the Forums");
?>