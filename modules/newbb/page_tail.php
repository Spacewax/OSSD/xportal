<?php
/***************************************************************************
  	                        page_tail.php  -  description
 	                        -------------------    
	begin                : Sat June 17 2000    
	copyright            : (C) 2001 The phpBB Group
	email                : support@phpbb.com
 
    $Id: page_tail.php,v 1.29 2001/03/28 08:02:20 thefinn Exp $
 
***************************************************************************/

/*************************************************************************** *                                         				                                 
 *   This program is free software; you can redistribute it and/or modify  	 
 *   it under the terms of the GNU General Public License as published by   
 *   the Free Software Foundation; either version 2 of the License, or	    	 
 *   (at your option) any later version. * 
 ***************************************************************************/
echo "<div style='text-align: center;'>". _MD_POWEREDBY ."&nbsp;XoopsBB "._MD_VERSION." 1.0<br /><a href='http://www.xoops.org/'>The XOOPS Project</a></div><br />";

?>