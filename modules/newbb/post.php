<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
$pagetype = "post";
$xoopsOption['forumpage'] = 1;
include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
if(!isset($forum)){
	redirect_header("index.php", 2, _MD_ERRORFORUM);
	exit();
}else{	
	$sql = "SELECT forum_name, forum_access, forum_type FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = $forum";
	if(!$result = $xoopsDB->query($sql)){
		error_die(_MD_CANTGETFORUM);
	}
	$myrow = $xoopsDB->fetch_array($result);
	$forum_name = $myrow['forum_name'];
	$forum_access = $myrow['forum_access'];
	if ($myrow['forum_type'] == 1 || $myrow['forum_access'] == 3){
	// To get here, we have a logged-in user. So, check whether that user is allowed to view
	// this private forum.
		$accesserror = 0;
		if ( $xoopsUser ) {
			if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
				if ( !check_priv_forum_auth($xoopsUser->uid(), $forum, TRUE) ) {
					$accesserror = 1;
				}
			}
		} else {
			$accesserror = 1;
		}
		if ( $accesserror == 1 ) {
			redirect_header("viewforum.php?order=$order&viewmode=$viewmode&forum=$forum",2,_MD_NORIGHTTOPOST);
			exit();
		}
	// Ok, looks like we're good.
	}
	if ( $op == "preview" ) {
		include($xoopsConfig['root_path']."header.php");
		OpenTable();
		include("page_header.php");
		$myts = new MyTextSanitizer;
		$p_subject = $myts->makeTboxData4Preview($subject);
		if($nosmiley && $nohtml){
			$p_message = $myts->makeTareaData4Preview($message,0,0,1);
		}elseif($nohtml){
			$p_message = $myts->makeTareaData4Preview($message,0,1,1);
		}elseif($nosmiley){
			$p_message = $myts->makeTareaData4Preview($message,1,0,1);
		}else{
			$p_message = $myts->makeTareaData4Preview($message,1,1,1);
		}
		themecenterposts($p_subject,$p_message);
		echo "<br />";
		$subject = $myts->makeTboxData4PreviewInForm($subject);
		$message = $myts->makeTareaData4PreviewInForm($message);

		include("include/forumform.inc.php");
		include("page_tail.php");
		CloseTable();
	} else {
		include_once("class/class.forumposts.php");
		if ( $post_id ) {
			$editerror = 0;
			$forumpost = new ForumPosts($post_id);
			if ( $xoopsUser ) {
				if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
					if ( $forumpost->uid() != $xoopsUser->uid() && !is_moderator($forum, $xoopsUser->uid()) ) {
						$editerror = 1;
					}
				}	
			} else {
				$editerror = 1;
			}
			if ( $editerror == 1 ) {
				redirect_header("viewtopic.php?topic_id=$topic_id&post_id=$post_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",2,_MD_EDITNOTALLOWED);
				exit();
			}
			$editor = $xoopsUser->uname();
   			$on_date .= _MD_ON." ".formatTimestamp(time());
   			
			$message .= "\n\n<small>[ "._MD_EDITEDBY." ".$editor." ".$on_date." ]</small>";
		}else{
			if($xoopsUser && !$noname){
				$uid = $xoopsUser->uid();
			}else{
				if($forum_access == 2){
					$uid = 0;
				}else{
					if(isset($topic_id) && $topic_id !=""){
						redirect_header("viewtopic.php?topic_id=$topic_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",2,_MD_ANONNOTALLOWED);
					}else{
						redirect_header("viewforum.php?forum=$forum",2,_MD_ANONNOTALLOWED);
					}
					exit();
				}
			}
			$forumpost = new ForumPosts();
			$forumpost->setForum($forum);
			if(isset($pid) && $pid != ""){
				$forumpost->setParent($pid);
			}
			if(isset($topic_id) && $topic_id != ""){
				$forumpost->setTopicId($topic_id);
				$isreply = 1;
			}
			$forumpost->setIp(getenv("REMOTE_ADDR"));
			$forumpost->setUid($uid);
		}
		$forumpost->setSubject($subject);
		$forumpost->setText($message);
		$forumpost->setNohtml($nohtml);
		$forumpost->setNosmiley($nosmiley);
		$forumpost->setIcon($icon);
		$forumpost->setAttachsig($attachsig);
		if(isset($notify) && $notify != ""){
			$forumpost->setNotify($notify);
		}
		$forumpost->store();
		if($isreply){
			$sql = "SELECT t.topic_notify, u.email, u.uname, u.uid FROM ".$xoopsDB->prefix("bb_topics")." t, ".$xoopsDB->prefix("users")." u WHERE t.topic_id = $topic_id AND t.topic_poster = u.uid";
   			if(!$result = $xoopsDB->query($sql,1)) {
				echo _MD_COULDNOTQUERY;
   			}
   			$m = $xoopsDB->fetch_array($result);
			$myuid = 0;
			if($xoopsUser){
				$myuid = $xoopsUser->uid();
			}
   			if($m['topic_notify'] == 1 && $m['uid'] != $myuid && $m['uid'] != 0) {
      				// We have to get the mail body and subject line in the board default language!
      				$subject = _MD_REPLYPOSTED;
				
      				$message = sprintf(_MD_HELLO,$m['uname']);
				$message .= "\n\n";
				$message .= sprintf(_MD_URRECEIVING,$xoopsConfig['sitename']);
				$message .= "\n\n"._MD_CLICKBELOW;
				$message .= "\n\n";
				$message .= $xoopsConfig['xoops_url']."/modules/newbb/viewtopic.php?forum=".$forum."&topic_id=".$topic_id."\n\n------------\n".$xoopsConfig['sitename']."\n".$xoopsConfig['xoops_url']."";
      				mail($m['email'], $subject, $message, "From: ".$xoopsConfig['adminmail']."\r\nX-Mailer: ".$xoopsConfig['sitename']."");
   			}
		}

		if($viewmode == "flat"){
			redirect_header("viewtopic.php?topic_id=".$forumpost->topic()."&post_id=&order=$order&viewmode=flat&pid=$pid&forum=$forum",2,_MD_THANKSSUBMIT);
			exit();
		}else{
			$post_id = $forumpost->postid();
			redirect_header("viewtopic.php?topic_id=".$forumpost->topic()."&post_id=".$post_id."&order=$order&viewmode=thread&pid=$pid&forum=$forum#".$post_id."",2,_MD_THANKSSUBMIT);
			exit();
		}
	}
	include($xoopsConfig['root_path']."footer.php");
}
?>