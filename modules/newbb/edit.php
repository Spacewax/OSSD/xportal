<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
$pagetype = "edit";
$xoopsOption['forumpage'] = 1;
include("header.php");
if(!isset($forum)){
	redirect_header("index.php", 2, _MD_ERRORFORUM);
	exit();
}elseif(!isset($post_id)){
	redirect_header("viewforum.php?forum=$forum", 2, _MD_ERRORPOST);
	exit();
}else{	
	include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

	$sql = "SELECT forum_name, forum_access, forum_type FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = $forum";
	if(!$result = $xoopsDB->query($sql)){
		error_die(_MD_CANTGETFORUM);
	}
	$myrow = $xoopsDB->fetch_array($result);
	$myts=new MyTextsanitizer;
	$forum_name = $myts->makeTboxData4Show($myrow['forum_name']);
	$forum_access = $myrow['forum_access'];
	if ($myrow['forum_type'] == 1){
		// To get here, we have a logged-in user. So, check whether that user is allowed to post in
		// this private forum.
		$accesserror = 0; //initialize
		if ( $xoopsUser ) {
			if ( !$xoopsUser->is_admin($xoopsModule->mid()) ){
				if ( !check_priv_forum_auth($xoopsUser->uid(), $forum, TRUE) ) {
					$accesserror = 1;
				}
			}
		} else {
			$accesserror = 1;
		}
		if ( $accesserror == 1 ) {
			redirect_header("viewtopic.php?topic_id=$topic_id&post_id=$post_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",2,_MD_NORIGHTTOPOST);
			exit();
		}
		// Ok, looks like we're good.
	}
	include($xoopsConfig['root_path']."header.php");
	include_once("class/class.forumposts.php");
	OpenTable();
	include("page_header.php");
	$forumpost = new ForumPosts($post_id);
	$nohtml = $forumpost->nohtml();
	$nosmiley = $forumpost->nosmiley();
	$icon = $forumpost->icon();
	$attachsig = $forumpost->attachsig();
	$topic_id=$forumpost->topic();
	if($forumpost->is_topic($post_id)){
		$istopic = 1;
		$notify = $forumpost->notify();
	}else{
		$istopic = 0;
	}
	$subject=$forumpost->subject("Edit");
	$message=$forumpost->text("Edit");
	include("include/forumform.inc.php");
	include("page_tail.php");
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}
?>