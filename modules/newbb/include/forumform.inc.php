<?php

include_once($xoopsConfig['root_path']."class/xoopslists.php");

echo "<table border='0' cellpadding='1' cellspacing='1' width='100%' bgcolor='".$xoopsTheme["bgcolor2"]."'><tr bgcolor='".$xoopsTheme["bgcolor3"]."' align='left'><td width='25%' valign='top' nowrap><b>". _MD_ABOUTPOST .":</b></td>";

if ( $forum_access == 1 ) {
	echo "<td>". _MD_REGCANPOST ."</td>";
} elseif ( $forum_access == 2 ) {
	echo "<td>". _MD_ANONCANPOST ."</td>";
} elseif ( $forum_access == 3 ) {
	echo "<td>". _MD_MODSCANPOST ."</td>";
}

echo "</tr><tr bgcolor='".$xoopsTheme["bgcolor1"]."' align='left'><form action='post.php' method='post' name='coolsus' onsubmit='return validate(this)'><td width='25%' valign='top' style='white-space: nowrap;'><b>". _MD_YOURNAME ."</b></td><td>";

if ( $xoopsUser ) {
	echo "<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$xoopsUser->uid()."'>".$xoopsUser->uname()."</a>&nbsp;[&nbsp;<a href='".$xoopsConfig['xoops_url']."/user.php?op=logout'>"._MD_LOGOUT."</a>&nbsp;]";
} else {
	echo "<span style='color: #ff0000;'>".$xoopsConfig['anonymous']."</span>";
	echo " [ <a href='".$xoopsConfig['xoops_url']."/register.php'>"._MD_REGISTER."</a> ] ";	
}

echo "</td></tr>
<tr bgcolor='".$xoopsTheme["bgcolor3"]."' align='left'>
<td valign='top' style='white-space: nowrap;'><b>". _MD_SUBJECTC ."</b></td>
<td>";

if ( !$istopic ) {
	if ( !eregi("^re:",$subject) ) {
		$subject = "Re: ".substr($subject,0,81);
	}
}

echo "<input type='text' name='subject' size='60' maxlength='100' value='$subject' /></td></tr>
<tr bgcolor='". $xoopsTheme["bgcolor1"] ."' align='left'>
<td valign='top' style='white-space: nowrap;'><b>". _MD_MESSAGEICON ."</b></td>
<td>
<table>
<tr align='left'>
<td>";

$lists = new XoopsLists;
$filelist = $lists->getSubjectsList();
$count = 1;
while ( list($key, $file) = each($filelist) ) {
	$checked = "";
	if ( isset($icon) && $file==$icon ) {
		$checked = " checked='checked'";
	}
	echo "<input type='radio' value='$file' name='icon'$checked />&nbsp;";
	echo "<img src='".$xoopsConfig['xoops_url']."/images/subject/$file' alt='' />&nbsp;";
	if ( $count == 8 ) { 
		echo "<br />"; 
		$count = 0; 
	}
	$count++;
}

echo "</td></tr></table></td></tr>
<tr bgcolor='". $xoopsTheme["bgcolor3"] ."' align='left'>
<td valign='top' style='white-space: nowrap;'><b>". _MD_MESSAGEC ."</b><br />
</td>
<td>
<textarea wrap='virtual' cols='50' rows='10' name='message'>$message</textarea><br />";

if ( $isreply ) {
	echo "<input type='hidden' name='hide' value='$hidden' />
	<input type='button' name='quote' value='"._MD_QUOTE."' onclick='this.form.message.value=this.form.message.value + this.form.hide.value; this.form.hide.value=\"\";' /><br />";
} 
putitems();
echo "&nbsp;[<a href='javascript:openWithSelfMain(\"".$xoopsConfig['xoops_url']."/misc.php?action=showpopups&amp;type=smilies\",\"smilies\",300,400)'>"._MORE."</a>]";

echo "<br /><br /><a href='javascript:checklength(document.coolsus);'>[". _MD_CHECKMESSAGE ."]</a><br />";

if ( $newbb_config['allow_html'] ) {
	echo "<br />"._MD_ALLOWEDHTML."<br />";
	echo get_allowed_html();
}

echo "</td></tr>
<tr bgcolor='".$xoopsTheme["bgcolor1"]."' align='left'>";
echo "<td valign='top' style='white-space: nowrap;'><b>"._MD_OPTIONS."</b></td>\n";
echo "<td>";

if ( $xoopsUser && $forum_access == 2 && !$post_id ) {
	echo "<input type='checkbox' name='noname' value='1'";
	if ( $noname ) {
		echo " checked='checked'";
	}
	echo " />&nbsp;"._MD_POSTANONLY."<br />\n"; 
}

echo "<input type='checkbox' name='nosmiley' value='1'";
if ( $nosmiley ) {
	echo " checked='checked'";
}
echo " />&nbsp;"._MD_DISABLESMILEY."<br />\n";

if ( $newbb_config['allow_html'] ) {
	echo "<input type='checkbox' name='nohtml' value='1'";
	if ( $nohtml ) {
		echo " checked='checked'";
	}
	echo " />&nbsp;"._MD_DISABLEHTML."<br />\n";
} else {
	echo "<input type='hidden' name='nohtml' value='1' />";
}

if ( $istopic ) {
	echo "<input type='hidden' name='istopic' value='1' />";
	echo "<input type='checkbox' name='notify' value='1'";
	if($notify){
		echo " checked='checked'";
	}
	echo " />&nbsp;"._MD_EMAILNOTIFY."<br />\n";
}

if ( $newbb_config['allow_sig'] && $xoopsUser ) {
	echo "<input type='checkbox' name='attachsig' value='1'";
	if ( $op == "preview" ) {
		if ( $attachsig ) {
			echo " checked='checked' />&nbsp;";
		} else {
			echo " />&nbsp;";
		}
	} else {
		if ( $xoopsUser->attachsig() || $attachsig ) {
			echo " checked='checked' />&nbsp;";
		} else {
			echo "/>&nbsp;";
		}
	}
			
	echo _MD_ATTACHSIG."<br />\n";
}

echo "</td></tr>
<tr bgcolor='". $xoopsTheme["bgcolor3"] ."' align='left'><td colspan='2' align='center'><br />
<input type='hidden' name='pid' value='$pid' />
<input type='hidden' name='post_id' value='$post_id' />
<input type='hidden' name='topic_id' value='$topic_id' />
<input type='hidden' name='forum' value='$forum' />
<input type='hidden' name='viewmode' value='$viewmode' />
<input type='hidden' name='order' value='$order' />
<select name='op'>
<option value='preview' selected='selected'>". _MD_PREVIEW ."</option>
<option value='post'>". _MD_POST ."</option>
</select>
<input type='submit' value='". _MD_SUBMIT ."' />&nbsp;";
echo "<input type='button' onclick='location=\"";
if ( isset($topic_id) && $topic_id != "" ) {
	echo "viewtopic.php?topic_id=".$topic_id."&amp;forum=".$forum."\"'";
} else {
	echo "viewforum.php?forum=".$forum."\"'";
}
echo " value='"._MD_CANCELPOST."' />";
echo "</form>\n";
echo "</td></tr></table>\n";

function putitems() {
	global $xoopsConfig;

	$smileyPath = "images/smilies";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_smile.gif' border='0' alt=':-)' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-( ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_frown.gif' border='0' alt=':-(' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-D ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_biggrin.gif' border='0' alt=':-D' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' ;-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_wink.gif' border='0' alt=';-)' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-o ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_eek.gif' border='0' alt=':-o' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' 8-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_cool.gif' border='0' alt='8-)' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-? ');\"><img width='15' height='22' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_confused.gif' border='0' alt=':-?' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-P ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_razz.gif' border='0' alt=':-P' /></a>";
	echo "<a href='javascript: x()' onclick=\"DoSmilie(' :-x ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_mad.gif' border='0' alt=':-x' /></a>";
		
}
?>