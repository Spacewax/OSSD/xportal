<?php

function forumuserpost($forum_access,$color,$postid,$topicid,$forum,$uid,$subject,$text,$attachsig,$time,$hostname,$iconimage="",$viewmode="",$order="", $lock_state="") { 
	global $xoopsConfig, $xoopsTheme, $xoopsUser, $xoopsModule, $myts, $newbb_config;
	$allowpost = 0;
	$allowhost = 0;
	$allowedit = 0;
	$myuid = 0;
	if ( $xoopsUser ) {
		$allowpost = 1;
		$myuid = $xoopsUser->uid();
		if ( $xoopsUser->is_admin($xoopsModule->mid()) || is_moderator($forum, $xoopsUser->uid()) ) {
			$allowhost = 1;
		}
	}
	
	echo "<a name='$postid' id='$postid'></a><tr bgcolor='$color' align='left'>\n";
   	if ( $uid != 0 ) {
		$poster = new XoopsUser($uid);
		if ( $myuid ) {
			if ( $poster->uid() == $myuid ) {
				$allowedit = 1;
			}
		}
		if ( !$poster->is_active() ) {
			$poster = 0;
		}
	} else {
		$poster = 0;
	}
	$text = "<p>".$text."</p>";
	if ( $poster ) {
		echo "<td valign='top' style='word-space: nowrap;'><b>".$poster->uname()."</b>";
		$rank = $poster->rank();
   		if ( $newbb_config['allow_sig'] && $attachsig && $poster->attachsig() ) {
			$text .= "<p><br />_________________<br />". $poster->user_sig()."</p>";
		}
		echo "<br /><small>" . $rank['title'] . "</small>";
      	if($rank['image'] != ''){
			echo "<br /><img src='".$xoopsConfig['xoops_url']."/images/ranks/".$rank['image']."' alt='' />";
		}
		if ( $poster->user_avatar() != '' ) {
			echo "<br /><img src='".$xoopsConfig['xoops_url']."/images/avatar/".$poster->user_avatar()."' alt='' />";
		}

		echo "<br /><br /><small>"._MD_JOINEDC." ".formatTimestamp($poster->user_regdate(),"s")."</small>";
		echo "<br /><small>"._MD_POSTSC." ".$poster->posts()."</small>";
		$user_from = $poster->user_from();
		if ( $user_from != '' ) {
			echo "<br /><small>"._MD_FROMC." ".$user_from."</small><br />";
      	}
      	echo "</td>";
   	} else {
		echo "<td valign='top'><small>".$xoopsConfig['anonymous']."</small></td>";
   	}
	echo "<td valign='top'>\n";
	echo "<table width='100%' border='0'><tr><td valign='top'>\n";
	if ( $iconimage != "" ) {
		echo "<img src='".$xoopsConfig['xoops_url']."/images/subject/$iconimage' alt='' />&nbsp;<small><b>$subject</b></small>\n";
	} else {
		echo "<small><b>$subject</b></small>\n";
	}
   	echo "</td><td align=\"right\">";
	if ( $allowhost ) {
		echo "<img src='".$xoopsConfig['xoops_url']."/images/icons/ip.gif' alt='IP' />";
   	}
	if ( $lock_state != 1 ) {
		if ( $allowedit || $allowhost ) {
			echo "&nbsp;<a href='edit.php?forum=$forum&amp;post_id=$postid&amp;topic_id=$topicid&amp;viewmode=$viewmode&amp;order=$order'><img src='".$xoopsConfig['xoops_url']."/images/icons/edit.gif' alt='"._MD_EDITTHISPOST."' /></a>";
		}
		if ( $forum_access==2 || $allowpost ) {
			echo "&nbsp;<a href='reply.php?forum=$forum&amp;post_id=$postid&amp;topic_id=$topicid&amp;viewmode=$viewmode&amp;order=$order'><img src='".$xoopsConfig['xoops_url']."/images/icons/reply.gif' alt='"._MD_REPLY."' /></a>\n";
		}
	}
	echo "</td></tr>\n";
	echo "<tr><td colspan='2'>$text</td></tr></table></td></tr>
	<tr bgcolor='$color' align='left'>\n";

	$time = formatTimestamp($time);
	echo "<td valign='middle' style='word-space: nowrap;'><img src='".$xoopsConfig['xoops_url']."/images/icons/posticon.gif' alt='' />&nbsp;<small>$time</small></td><td valign='middle'>";
   	if ( $poster ) {
		echo "&nbsp;<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$poster->uid()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/profile.gif' alt='"._MD_PROFILE."' /></a>\n";
		if ( $allowpost ) {
			echo "&nbsp;<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?send2=1&theme=".$xoopsTheme['thename']."&to_userid=".$poster->uid()."','pmlite',360,290);\">";
			echo "<img src='".$xoopsConfig['xoops_url']."/images/icons/pm.gif' alt='";
			printf(_MD_SENDPM,$poster->uname());
			echo "' />";
			echo "</a>\n";
		}
	   	if ( $poster->user_viewemail() ) {
			echo "&nbsp;<a href='mailto:".$poster->email()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/email.gif' alt='";
			printf(_MD_SENDEMAIL,$poster->uname());
			echo "' /></a>\n";
		}
	   	if ( $poster->url() != "" ) {
			if ( strstr("http://", $poster->url()) ) {
				$poster_url = "http://" . $poster->url();
			} else {
				$poster_url = $poster->url();
			}
			echo "&nbsp;<a href='$poster_url' target='_blank'><img src='".$xoopsConfig['xoops_url']."/images/icons/www.gif' alt='"._MD_VISITSITE."' /></a>\n";
	   	}
	   	if ( $poster->user_icq() != "" ) {
			//echo "&nbsp;<a href=\"http://wwp.icq.com/".$poster->user_icq()."#pager\" target=\"_blank\"><img src=\"http://online.mirabilis.com/scripts/online.dll?icq=".$poster->user_icq()."&img=5\" border=\"0\" alt=\"icq\" /></a>";
			echo "&nbsp;<a href='http://wwp.icq.com/scripts/search.dll?to=".$poster->user_icq()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/icq_add.gif' alt='"._MD_ADDTOLIST."' /></a>";
		}
		if ( $poster->user_aim() != "" ) {
			echo "&nbsp;<a href='aim:goim?screenname=".$poster->user_aim()."&message=Hi+".$poster->user_aim()."+Are+you+there?'><img src='".$xoopsConfig['xoops_url']."/images/icons/aim.gif' alt='aim' /></a>";
		}

   		if ( $poster->user_yim() != "" ) {
			echo "&nbsp;<a href='http://edit.yahoo.com/config/send_webmesg?.target=".$poster->user_yim()."&.src=pg'><img src='".$xoopsConfig['xoops_url']."/images/icons/yim.gif' alt='yim' /></a>";
		}
		if ( $poster->user_msnm() != "" ) {
			echo "&nbsp;<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$poster->uid()."'><img src='".$xoopsConfig['xoops_url']."/images/icons/msnm.gif' alt='msnm' /></a>";
		}
   	} else {
		echo "&nbsp;\n";
   	}
   	echo "</td></tr>";
}


?>