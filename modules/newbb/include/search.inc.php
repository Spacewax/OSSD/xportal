<?php

function newbb_search($queryarray, $andor, $limit, $offset, $userid){
	global $xoopsDB;
	$sql = "SELECT p.post_id,p.topic_id,p.forum_id,p.post_time,p.uid,p.subject FROM ".$xoopsDB->prefix("bb_posts")." p LEFT JOIN ".$xoopsDB->prefix("bb_posts_text")." t ON t.post_id=p.post_id LEFT JOIN ".$xoopsDB->prefix("bb_forums")." f ON f.forum_id=p.forum_id WHERE f.forum_type=0";
	if ( $userid != 0 ) {
		$sql .= " AND p.uid=".$userid." ";
	}
	// because count() returns 1 even if a supplied variable
	// is not an array, we must check if $querryarray is really an array
	if ( is_array($queryarray) && $count = count($queryarray) ) {
		$sql .= " AND ((p.subject LIKE '%$queryarray[0]%' OR t.post_text LIKE '%$queryarray[0]%')";
		for($i=1;$i<$count;$i++){
			$sql .= " $andor ";
			$sql .= "(p.subject LIKE '%$queryarray[$i]%' OR t.post_text LIKE '%$queryarray[$i]%')";
		}
		$sql .= ") ";
	}
	$sql .= "ORDER BY p.post_time DESC";
	$result = $xoopsDB->query($sql,0,$limit,$offset);
	$ret = array();
	$i = 0;
 	while($myrow = $xoopsDB->fetch_array($result)){
		$ret[$i]['link'] = "viewtopic.php?topic_id=".$myrow['topic_id']."&amp;forum=".$myrow['forum_id']."#".$myrow['post_id']."";
		$ret[$i]['title'] = $myrow['subject'];
		$ret[$i]['time'] = $myrow['post_time'];
		$ret[$i]['uid'] = $myrow['uid'];
		$i++;
	}
	return $ret;
}
?>