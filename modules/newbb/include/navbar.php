<?php

echo "<form method='get' action='$PHP_SELF'>";

echo "<table width='$TableWidth' border='0'  align='center' cellspacing='1' cellpadding='2'><tr valign='bottom'>";

if ( $viewmode== "flat" ) {
	if ( $total > $newbb_config['posts_per_page'] ) {
		$times = 1;
   		echo "<td valign='bottom' style='color: ".$xoopsTheme["textcolor2"].";'>"._MD_GOTOPAGE." ( ";
   		$last_page = $start - $newbb_config['posts_per_page'];
   		if ( $start > 0 ) {
			echo "<a href='$PHP_SELF?topic_id=$topic_id&amp;forum=$forum&amp;start=$last_page&amp;viewmode=$viewmode&amp;order=$order'>"._MD_PREVPAGE."</a> ";
		}
   		for ( $x = 0; $x < $total; $x += $newbb_config['posts_per_page'] ) {
			if ( $times != 1 ) {
				echo " | ";
			}
			if ( $start && ($start == $x) ) {
				echo $times;
			} else if ( $start == 0 && $x == 0 ) {
				echo "1";
			} else {
				echo "<a href='$PHP_SELF?mode=viewtopic&amp;topic_id=$topic_id&amp;forum=$forum&amp;start=$x&amp;viewmode=$viewmode&amp;order=$order'>$times</a>";
			}
			$times++;
   		}

   		if ( ($start + $newbb_config['posts_per_page']) < $total ) {
			$next_page = $start + $newbb_config['posts_per_page'];
      		echo " <a href='$PHP_SELF?topic_id=$topic_id&amp;forum=$forum&amp;start=$next_page&amp;viewmode=$viewmode&amp;order=$order'>"._MD_NEXTPAGE."</a>";
		}
   		echo " ) </td>\n";
	}
}

echo "<td align='right' valign='bottom'>
<select name='viewmode'>
<option value='flat' ";
if ($viewmode == 'flat') {
	echo "selected='selected'"; 
}
echo ">". _MD_FLAT ."<option value='thread' ";
if ( !isset($viewmode) || $viewmode=='thread' || $viewmode=="" ) {
	echo "selected='selected'";
}
echo ">". _MD_THREADED ."</select>
<select name='order'>
<option value='0' ";
if ( !$order ) { 
	echo "selected='selected'";
}
echo ">". _MD_OLDFIRST ."
<option value='1' ";
if ($order==1) { 
	echo "selected='selected'";
}
echo ">". _MD_NEWFIRST ."</select>
<input type='hidden' name='topic_id' value='$topic_id' />
<input type='hidden' name='forum' value='$forum' />
<input type='hidden' name='post_id' value='$post_id' />
<input type='submit' name='refresh' value='". _MD_GO ."' />
</td></tr></table></form>";

?>