<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_newbb_new_show($options) {
	global $xoopsDB, $xoopsConfig, $xoopsTheme;
	$myts = new MyTextSanitizer;
    	$block = array();
    	$block['title'] = _MB_NEWBB_RECENT;

    	$query="SELECT t.topic_id, t.topic_title, t.topic_time, t.topic_views, t.topic_replies, t.forum_id, f.forum_name FROM ".$xoopsDB->prefix("bb_topics")." t, ".$xoopsDB->prefix("bb_forums")." f WHERE (f.forum_id=t.forum_id) AND (f.forum_type <> '1') ORDER BY t.topic_time DESC";
//echo $query;
    	if(!$result=$xoopsDB->query($query,0,$options[0],0)){
		echo "ERROR";
	}

    	$block['content'] = "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>\n";
	$block['content'] .= "<table width='100%' border='0' cellpadding='2' cellspacing='1'>\n";
	$block['content'] .= "<tr bgcolor='".$xoopsTheme["bgcolor3"]."'>";
    	$block['content'] .="<td><b>" ._MB_NEWBB_FORUM."</b></td><td><b>" ._MB_NEWBB_TOPIC."</b></td><td align=\"center\"><b>" ._MB_NEWBB_RPLS."</b></td><td align=\"center\"><b>" ._MB_NEWBB_VIEWS."</b></td><td align=\"right\"><b>" ._MB_NEWBB_LPOST."</b></td></tr>";
    	while ($array=$xoopsDB->fetch_array($result)){
		$block['content'] .= "<tr bgcolor='".$xoopsTheme["bgcolor1"]."'><td><a href='".$xoopsConfig['xoops_url']."/modules/newbb/viewforum.php?forum=" . $array["forum_id"] . "'>";
		$block['content'] .= $myts->makeTboxData4Show($array["forum_name"]);

		$block['content'] .= "</a></td><td><a href=\"".$xoopsConfig['xoops_url']."/modules/newbb/viewtopic.php?topic_id=" . $array["topic_id"] . "&forum=" . $array["forum_id"] . "\">";
		$block['content'] .= $myts->makeTboxData4Show($array["topic_title"]);
		$block['content'] .= "</a></td>";
		$block['content'] .= "<td align=\"center\"><span style=\"color:".$xoopsTheme['textcolor2'].";\">".$array["topic_replies"]."</span></td>";
		$block['content'] .= "<td align=\"center\">".$array["topic_views"]."</td><td align=\"right\">";
		$block['content'] .= formatTimestamp($array["topic_time"],"m"). "</td></tr>";
	}
	$block['content'] .= "</td></tr></table><tr><td colspan=\"5\" align=\"right\"><span style=\"font-weight:bold;\">&raquo;&raquo;</span>&nbsp;<b><a href=\"".$xoopsConfig['xoops_url']."/modules/newbb/\">"._MB_NEWBB_VSTFRMS."</a></b>";
	$block['content'] .= "</td></tr></table>";
    	return $block;
}

function b_newbb_new_edit($options) {
	global $xoopsTheme;
	$inputtag = "<input type=\"text\" name=\"options[]\" value=\"".$options[0]."\">";
	$form = sprintf(_MB_NEWBB_DISPLAY,$inputtag);
	return $form;
}

?>