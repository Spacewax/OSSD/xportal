<?php
/***************************************************************************
                            topicmanager.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

    $Id: topicmanager.php,v 1.21 2001/06/16 06:05:50 thefinn Exp $

 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
include("header.php");
$accesserror = 0;
if ( $xoopsUser ) {
	if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
		if ( !is_moderator($forum, $xoopsUser->uid()) ) {
			$accesserror = 1;
		}
	}
} else {
	$accesserror = 1;
}
if ( $accesserror == 1 ) {
	redirect_header("viewtopic.php?topic_id=$topic_id&post_id=$post_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",3,_MD_YANTMOTFTYCPTF);
	exit();
}


include($xoopsConfig['root_path']."header.php");
OpenTable();
$pagetype = "bbcode_ref";
include('page_header.php');
if($HTTP_POST_VARS['submit'] || ($mode=='viewip')) {

   switch($mode) {
    case 'del':
      // Update the users's post count, this might be slow on big topics but it makes other parts of the
      // forum faster so we win out in the long run.
      $sql = "SELECT uid, post_id FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $topic_id";
      if(!$r = $xoopsDB->query($sql))
			die(_MD_COULDNOTQUERY);
      while($row = $xoopsDB->fetch_array($r)) {
	 		if($row[uid] != 0) {
	    		$sql = "UPDATE ".$xoopsDB->prefix("users")." SET posts = posts - 1 WHERE uid = ".$row['uid']."";
	    		$xoopsDB->query($sql);
	 		}
      }

		// Get the post ID's we have to remove.
		$sql = "SELECT post_id FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $topic_id";
		if(!$r = $xoopsDB->query($sql))
			die(_MD_COULDNOTQUERY);
      while($row = $xoopsDB->fetch_array($r))
      {
			$posts_to_remove[] = $row["post_id"];
		}

      $sql = "DELETE FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $topic_id";
      if(!$result = $xoopsDB->query($sql))
			die(_MD_COULDNOTREMOVE);
      $sql = "DELETE FROM ".$xoopsDB->prefix("bb_topics")." WHERE topic_id = $topic_id";
      if(!$result = $xoopsDB->query($sql))
			die(_MD_COULDNOTQUERY);
		$sql = "DELETE FROM ".$xoopsDB->prefix("bb_posts_text")." WHERE ";
		for($x = 0; $x < count($posts_to_remove); $x++)
		{
			if($set)
			{
				$sql .= " OR ";
			}
			$sql .= "post_id = ".$posts_to_remove[$x];
			$set = TRUE;
		}

		if(!$xoopsDB->query($sql))
		{
			die(_MD_COULDNOTREMOVETXT);
		}
		sync($forum, 'forum');

      echo _MD_TTHBRFTD."<p><a href=\"viewforum.php?forum=$forum\">"._MD_RETURNTOTHEFORUM."</a></p><p><a href=\"index.php\">"._MD_RTTFI."</a></p>";
      break;
    case 'move':
      $sql = "UPDATE ".$xoopsDB->prefix("bb_topics")." SET forum_id = $newforum WHERE topic_id = $topic_id";
      if(!$r = $xoopsDB->query($sql,1))
		die(_MD_EPGBATA);
      $sql = "UPDATE ".$xoopsDB->prefix("bb_posts")." SET forum_id = $newforum WHERE topic_id = $topic_id";
      if(!$r = $xoopsDB->query($sql,1))
		die(_MD_EPGBATA);
		sync($newforum, 'forum');
		sync($forum, 'forum');

      echo _MD_TTHBM."<p><a href=\"viewtopic.php?topic_id=$topic_id&forum=$newforum\">"._MD_VTUT."</a></p><p><a href=\"index.php\">"._MD_RTTFI."</a></p>";
      break;
    case 'lock':
      $sql = "UPDATE ".$xoopsDB->prefix("bb_topics")." SET topic_status = 1 WHERE topic_id = $topic_id";
      if(!$r = $xoopsDB->query($sql))
		die(_MD_EPGBATA);
      echo _MD_TTHBL."<p><a href=\"viewtopic.php?topic_id=$topic_id&forum=$forum\">"._MD_VIEWTHETOPIC."</a></p><p><a href=\"index.php\">"._MD_RTTFI."</a></p>";
      break;
    case 'unlock':
      $sql = "UPDATE ".$xoopsDB->prefix("bb_topics")." SET topic_status = 0 WHERE topic_id = $topic_id";
      if(!$r = $xoopsDB->query($sql))
	die("Error - Could not unlock the selected topic. Please go back and try again.");
      echo _MD_TTHBU."<p><a href=\"viewtopic.php?topic_id=$topic_id&forum=$forum\">"._MD_VIEWTHETOPIC."</a></p><p><a href=\"index.php\">"._MD_RTTFI."</a></p>";
      break;
    case 'viewip':
      $sql = "SELECT u.uname, p.poster_ip FROM ".$xoopsDB->prefix("users")." u, ".$xoopsDB->prefix("bb_posts")." p WHERE p.post_id = $post_id AND u.uid = p.uid";
      if(!$r = $xoopsDB->query($sql,1))
	die(_MD_COULDNOTQUERY);
      if(!$m = $xoopsDB->fetch_array($r))
	die(_MD_ENSUOPITD);
      $poster_host = gethostbyaddr($m['poster_ip']);
?>
<TABLE BORDER="0" CELLPADDING="1" CELLSPACEING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD BGCOLOR="<?php echo $xoopsTheme["bgcolor2"];?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACEING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme["bgcolor3"];?>" ALIGN="LEFT">
	<TD COLSPAN="2" ALIGN="CENTER"><?php echo _MD_UIAAI;?></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme["bgcolor1"];?>" ALIGN="LEFT">
	<TD><?php echo _MD_USERIP;?></TD>
	<TD><?php echo $m['poster_ip'] . " ( $poster_host )"?></TD>
</TR>
<TR BGCOLOR="<?php echo $xoopsTheme["bgcolor3"];?>" ALIGN="LEFT">
	<TD COLSPAN="2" ALIGN="CENTER"><?php echo _MD_UOUTPFTIAPC;?></TD>
</TR>
<?php
	$sql = "SELECT u.uid, u.uname, COUNT(*) AS postcount FROM ".$xoopsDB->prefix("bb_posts")." p, ".$xoopsDB->prefix("users")." u WHERE p.poster_ip='".$m['poster_ip']."' && p.uid = u.uid GROUP BY u.uid";
	if(!$r = $xoopsDB->query($sql,1))
	{
		echo "<TR><TD COLSPAN=\"2\">"._MD_COULDNOTQUERY."</TD></TR></TABLE>";
		exit();
	}

	while ($row = $xoopsDB->fetch_array($r)){
		print "<TR BGCOLOR=\"".$xoopsTheme["bgcolor1"]."\" ALIGN=\"LEFT\">\n";
		print "	<TD><A HREF=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$row['uid']."\">".$row['uname']."</A></TD>\n";
		print "	<TD>".$row['postcount']." posts</TD>\n";
		print "</TR>\n";
	}
?>

</TABLE></TD></TR></TABLE>
<?php
		break;

	}
}
else {  // No submit
?>
<FORM ACTION="<?php echo $PHP_SELF?>" METHOD="POST">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" ALIGN="CENTER" VALIGN="TOP" WIDTH="95%"><TR><TD  BGCOLOR="<?php echo $xoopsTheme["bgcolor2"];?>">
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="1" WIDTH="100%">
<TR BGCOLOR="<?php echo $xoopsTheme["bgcolor3"];?>" ALIGN="LEFT">
<?php
	switch($mode) {
		case 'del':
?>
	<TD COLSPAN=2><?php echo _MD_OYPTDBATBOTFTTY;?></TD>
<?php
		break;
		case 'move':
?>
	<TD COLSPAN=2><?php echo _MD_OYPTMBATBOTFTTY;?></TD>
<?php
		break;
		case 'lock':
?>
	<TD COLSPAN=2><?php echo _MD_OYPTLBATBOTFTTY;?></TD>
<?php
		break;
		case 'unlock':
?>
	<TD COLSPAN=2><?php echo _MD_OYPTUBATBOTFTTY;?></TD>
<?php
		break;
		case 'viewip':
?>
	<TD COLSPAN=2><?php echo _MD_VTUIA;?></TD>
<?php
		break;
	}
?>
</TR>
<?php

	if($mode == 'move') {
?>
<TR>
	<TD BGCOLOR="<?php echo $xoopsTheme["bgcolor3"];?>"><?php echo _MD_MOVETOPICTO;?></TD>
	<TD BGCOLOR="<?php echo $xoopsTheme["bgcolor1"];?>"><SELECT NAME="newforum" SIZE="0">
<?php
	$sql = "SELECT forum_id, forum_name FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id != $forum ORDER BY forum_id";
	if($result = $xoopsDB->query($sql)) {
		if($myrow = $xoopsDB->fetch_array($result)) {
			do {
				echo "<OPTION VALUE=\"".$myrow['forum_id']."\">".$myrow['forum_name']."</OPTION>\n";
			} while($myrow = $xoopsDB->fetch_array($result));
		}
		else {
			echo "<OPTION VALUE=\"-1\">"._MD_NOFORUMINDB."</OPTION>\n";
		}
	}
	else {
		echo "<OPTION VALUE=\"-1\">"._MD_DATABASEERROR."</OPTION>\n";
	}
?>
	</SELECT></TD>
</TR>
<?php
	}
?>
<TR BGCOLOR="<?php echo $xoopsTheme["bgcolor3"];?>">
	<TD COLSPAN="2" ALIGN="CENTER">
<?php
	switch($mode) {
		case 'del':
?>
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="del">
		<INPUT TYPE="HIDDEN" NAME="topic_id" VALUE="<?php echo $topic_id?>">
		<INPUT TYPE="HIDDEN" NAME="forum" VALUE="<?php echo $forum?>">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_DELTOPIC;?>">
<?php
		break;
		case 'move':
?>
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="move">
		<INPUT TYPE="HIDDEN" NAME="topic_id" VALUE="<?php echo $topic_id?>">
		<INPUT TYPE="HIDDEN" NAME="forum" VALUE="<?php echo $forum?>">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_MOVETOPIC;?>">
<?php
		break;
		case 'lock':
?>
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="lock">
		<INPUT TYPE="HIDDEN" NAME="topic_id" VALUE="<?php echo $topic_id?>">
		<INPUT TYPE="HIDDEN" NAME="forum" VALUE="<?php echo $forum?>">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_LOCKTOPIC;?>">
<?php
		break;
		case 'unlock':
?>
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="unlock">
		<INPUT TYPE="HIDDEN" NAME="topic_id" VALUE="<?php echo $topic_id?>">
		<INPUT TYPE="HIDDEN" NAME="forum" VALUE="<?php echo $forum?>">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_UNLOCKTOPIC;?>">
<?php
		break;
		case 'viewip':
?>
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="viewip">
		<INPUT TYPE="HIDDEN" NAME="post_id" VALUE="<?php echo $post_id?>">
		<INPUT TYPE="HIDDEN" NAME="forum" VALUE="<?php echo $forum?>">
		<INPUT TYPE="SUBMIT" NAME="submit" VALUE="<?php echo _MD_VIEWIP;?>">
<?php
		break;
	}
?>
</TD></TR>
</FORM>
</TABLE></TD></TR></TABLE>
<?php
}
include('page_tail.php');
CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>