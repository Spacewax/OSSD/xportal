<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// ORIGINAL FILE INFO
/***************************************************************************
                            veiwforum.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com
 
 ***************************************************************************/

include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

$pagetype = "viewforum";

if($forum == -1 || !isset($forum)){
	redirect_header("index.php", 2, _MD_ERRORFORUM);
	exit();
}
$sql = "SELECT f.forum_type, f.forum_name FROM ".$xoopsDB->prefix("bb_forums")." f WHERE forum_id = $forum";
if(!$result = $xoopsDB->query($sql)){
	redirect_header("index.php", 2, _MD_ERRORCONNECT);
	exit();
}
if(!$myrow = $xoopsDB->fetch_array($result)){
	redirect_header("index.php", 2, _MD_ERROREXIST);
	exit();
}
$myts = new MyTextSanitizer;
$forum_name = $myts->makeTboxData4Show($myrow['forum_name']);

if ($myrow['forum_type'] == 1){
	// To get here, we have a logged-in user. So, check whether that user is allowed to view
	// this private forum.
	$accesserror = 0;
	if ( $xoopsUser ) {
		if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
			if ( !check_priv_forum_auth($xoopsUser->uid(), $forum, FALSE) ) {
				$accesserror = 1;
			}
		}
	} else {
		$accesserror = 1;
	}
	if ( $accesserror == 1 ) {
		redirect_header("index.php",2,_MD_NORIGHTTOACCESS);
		exit();
	}
		// Ok, looks like we're good.
}

include($xoopsConfig['root_path']."header.php");
OpenTable();
require('page_header.php');

if(!isset($sortname) || $sortname == "") $sortname = "p.post_time";
if(!isset($sortorder) || $sortorder == "") $sortorder = "DESC";
if(!isset($sortdays) || $sortdays == "") $sortdays = "100";
?>
<form action="viewforum.php" method="get">
<div>
<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="<?php echo $tablewidth?>">
<tr><td align="center">

<b><?php echo _MD_SORTEDBY;?></b>
<select name="sortname">
	<option value="t.topic_title"<?php if($sortname=="t.topic_title") echo " selected";?>><?php echo _MD_TOPICTITLE;?></option>
	<option value="p.post_time"<?php if($sortname=="p.post_time") echo " selected";?>><?php echo _MD_LASTPOSTTIME;?></option>
	<option value="t.topic_replies"<?php if($sortname=="t.topic_replies") echo " selected";?>><?php echo _MD_NUMBERREPLIES;?></option>
	<option value="t.topic_views"<?php if($sortname=="t.topic_views") echo " selected";?>><?php echo _MD_NUMBERVIEWS;?></option>
	<option value="u.uname"<?php if($sortname=="u.uname") echo " selected";?>><?php echo _MD_TOPICPOSTER;?></option>
</select>
</nobr><nobr>
<select name="sortorder">
	<option value="ASC"<?php if($sortorder=="ASC") echo " selected";?>><?php echo _MD_ASCENDING;?></option>
	<option value="DESC"<?php if($sortorder=="DESC") echo " selected";?>><?php echo _MD_DESCENDING;?></option>
</select>
<select name="sortdays">
	<option value="1"<?php if($sortdays=="1") echo " selected";?>><?php echo _MD_LASTDAY;?></option>
	<option value="2"<?php if($sortdays=="2") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,2);?></option>
	<option value="5"<?php if($sortdays=="5") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,5);?></option>
	<option value="10"<?php if($sortdays=="10") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,10);?></option>
	<option value="20"<?php if($sortdays=="20") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,20);?></option>
	<option value="30"<?php if($sortdays=="30") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,30);?></option>
	<option value="45"<?php if($sortdays=="45") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,45);?></option>
	<option value="60"<?php if($sortdays=="60") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,60);?></option>
	<option value="75"<?php if($sortdays=="75") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,75);?></option>
	<option value="100"<?php if($sortdays=="100") echo " selected";?>><?php printf(_MD_FROMLASTDAYS,100);?></option>
	<option value="365"<?php if($sortdays=="365") echo " selected";?>><?php echo _MD_THELASTYEAR;?></option>
	<option value="1000"<?php if($sortdays=="1000") echo " selected";?>><?php echo _MD_BEGINNING;?></option>
</select>
<input type="hidden" name="forum" value="<?php echo $forum;?>">
<input type="submit" name="refresh" value="<?php echo _MD_GO;?>">
</nobr>
</td></tr>
</table>
</div>
</form>
<?php
$startdate = time() - (86400* $sortdays);
?>
<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="<?php echo $tablewidth?>"><tr><td bgcolor="<?php echo $xoopsTheme["bgcolor2"];?>">
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<tr bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" align="left">
	<td width="2%">&nbsp;</td>
	<td width="2%">&nbsp;</td>
	<td>&nbsp;<b><a href="viewforum.php?forum=<?php echo $forum;?>&sortname=t.topic_title&sortdays=<?php echo $sortdays;?>&sortorder=<?php if($sortname=="t.topic_title" && $sortorder=="DESC"){echo "ASC";}else{echo "DESC";}?>"><?php echo _MD_TOPIC;?></a></B></TD>
	<td width="9%" align="center"><b><a href="viewforum.php?forum=<?php echo $forum;?>&sortname=t.topic_replies&sortdays=<?php echo $sortdays;?>&sortorder=<?php if($sortname=="t.topic_replies" && $sortorder=="DESC"){echo "ASC";}else{echo "DESC";}?>"><?php echo _MD_REPLIES?></a></b></td>
	<td width="20%" align="center"><b><a href="viewforum.php?forum=<?php echo $forum;?>&sortname=u.uname&sortdays=<?php echo $sortdays;?>&sortorder=<?php if($sortname=="u.uname" && $sortorder=="DESC"){echo "ASC";}else{echo "DESC";}?>"><?php echo _MD_POSTER;?></a></b></td>
	<td width="8%" align="center"><b><a href="viewforum.php?forum=<?php echo $forum;?>&sortname=t.topic_views&sortdays=<?php echo $sortdays;?>&sortorder=<?php if($sortname=="t.topic_views" && $sortorder=="DESC"){echo "ASC";}else{echo "DESC";}?>"><?php echo _MD_VIEWS;?></a></b></td>
	<td width="15%" align="center"><b><a href="viewforum.php?forum=<?php echo $forum;?>&sortname=p.post_time&sortdays=<?php echo $sortdays;?>&sortorder=<?php if($sortname=="p.post_time" && $sortorder=="DESC"){echo "ASC";}else{echo "DESC";}?>"><?php echo _MD_DATE;?></a></b></td>	
</tr>
<?php
if(!isset($start) || $start=="") $start = 0;
$sql = "SELECT t.*, u.uname, u2.uname as last_poster, p.post_time, p.icon FROM ".$xoopsDB->prefix("bb_topics")." t
        LEFT JOIN ".$xoopsDB->prefix("users")." u ON u.uid = t.topic_poster 
        LEFT JOIN ".$xoopsDB->prefix("bb_posts")." p ON p.post_id = t.topic_last_post_id 
        LEFT JOIN ".$xoopsDB->prefix("users")." u2 ON  u2.uid = p.uid
        WHERE t.forum_id = $forum and p.post_time > '$startdate'
        ORDER BY ".$sortname." ".$sortorder."";
        
if(!$result = $xoopsDB->query($sql,0,$newbb_config['topics_per_page'],$start))
	error_die("</table></td></tr></table><h4>"._MD_ERROROCCURED."</h4><hr>"._MD_COULDNOTQUERY."");
$topics_start = $start;
if($myrow = $xoopsDB->fetch_array($result)) {
   do {
      	echo"<tr>\n";
      	if(!$myrow["last_poster"]){
		$myrow["last_poster"] = $xoopsConfig['anonymous'];
	}
      	$replys = $myrow["topic_replies"];
      	$last_post = formatTimestamp($myrow["post_time"]) . "<br>"._MD_BY." ".$myts->makeTboxData4Show($myrow["last_poster"]);
      	$last_post_time = $myrow["post_time"];
		 if($replys >= $newbb_config['hot_threshold']) {
			 
			 if($last_post_time < $last_visit) 
				 $image = $hot_folder_image;
			 else 
				 $image = $hot_newposts_image;
		 }
		 else {
			 if($last_post_time < $last_visit) 
				 $image = $folder_image;
			 else
				 $image = $newposts_image;
		 }
		 if($myrow['topic_status'] == 1)
			 $image = $locked_image;
      
      echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" align=\"center\"><img src=\"$image\" /></td>\n";
      
      $topic_title = $myts->makeTboxData4Show($myrow['topic_title']);
		$pagination = '';
		$start = '';
		$topiclink = "viewtopic.php?topic_id=".$myrow['topic_id']."&amp;forum=".$forum."";
		if($replys+1 > $newbb_config['posts_per_page']) 
		{
			$pagination .= "&nbsp;&nbsp;&nbsp;<font color=\"".$xoopsTheme["textcolor2"]."\">(<img src=\"".$xoopsConfig['xoops_url']."/images/icons/posticon.gif\" /> ";
			$pagenr = 1;
			$skippages = 0;
			for($x = 0; $x < $replys + 1; $x += $newbb_config['posts_per_page']) 
			{
				$lastpage = (($x + $newbb_config['posts_per_page']) >= $replys + 1);
				
				if($lastpage)
				{
					$start = "&start=$x&$replys";
				} 
				else 
				{
					if ($x != 0)
					{
						$start = "&start=$x";
					}
					$start .= "&" . ($x + $newbb_config['posts_per_page'] - 1);
				}
				
				if($pagenr > 3 && $skippages != 1 && !$lastpage) 
				{
					$pagination .= ", ... ";
					$skippages = 1;
				} 

				if ($skippages != 1 || $lastpage) 
				{
					if ($x!=0) $pagination .= ", ";
					$pagination .= "<a href=\"$topiclink$start\">$pagenr</a>";
				}
				
				$pagenr++;
			}
			$pagination .= ")</font>";
		} 

		$topiclink .= "&$replys";
	if($myrow['icon']){
		echo "<td bgcolor=\"".$xoopsTheme["bgcolor1"]."\" align=\"center\"><img src=\"".$xoopsConfig['xoops_url']."/images/subject/".$myrow['icon']."\" /></td>\n";
	}else{
		echo "<td bgcolor=\"".$xoopsTheme["bgcolor1"]."\" align=\"center\">&nbsp;</td>\n";
	}
      echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\"";
//      if($newbb_config['mouse_rollover']){
      	echo " onMouseOver=\"this.style.backgroundColor='". $xoopsTheme["bgcolor1"] . "'; this.style.cursor='hand';\" onMouseOut=\"this.style.backgroundColor='" . $xoopsTheme["bgcolor3"] . "';\" onclick=\"window.location.href='" . $topiclink . "'\"";
//      }
	echo ">&nbsp;<a href=\"$topiclink\">$topic_title</a>$pagination";
	      
      echo "</td>\n";
      echo "<td bgcolor=\"".$xoopsTheme["bgcolor1"]."\" align=\"center\" valign=\"middle\">$replys</td>\n";
      echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" align=\"center\" valign=\"middle\">";
	if($myrow['topic_poster'] != 0 && $myrow['uname']){
		echo "<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$myrow['topic_poster']."\">".$myrow['uname']."</a></TD>\n";
	}else{
		echo $xoopsConfig['anonymous'];
	}
      echo "<td bgcolor=\"".$xoopsTheme["bgcolor1"]."\" align=\"center\" valign=\"middle\">".$myrow['topic_views']."</td>\n";
      echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" align=\"right\" valign=\"middle\">$last_post</td></tr>\n";
      
   } while($myrow = $xoopsDB->fetch_array($result));
}
else {
	echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" colspan=\"7\" align=\"center\">"._MD_NOTOPICS."</td></tr>\n";
}

?>
</table></td></tr></table>

<table align="center" border="0" width="<?php echo $tablewidth?>"><tr><td valign="top">
<img src="<?php echo $newposts_image?>"> = <?php echo _MD_NEWPOSTS;?> (<img src="<?php echo $hot_newposts_image?>" /> = <?php printf(_MD_MORETHAN,$newbb_config['hot_threshold']);?>)
<br><img src="<?php echo $folder_image?>"> = <?php echo _MD_NONEWPOSTS?> (<img src="<?php echo $hot_folder_image?>" /> = <?php printf(_MD_MORETHAN,$newbb_config['hot_threshold']);?>)
<br><img src="<?php echo $locked_image?>"> = <?php echo _MD_TOPICLOCKED;?>
</td>
<td align="right">
<?php

$sql = "SELECT COUNT(*) FROM ".$xoopsDB->prefix("bb_topics")." t LEFT JOIN ".$xoopsDB->prefix("bb_posts")." p ON t.topic_last_post_id = p.post_id WHERE t.forum_id = $forum AND p.post_time > $startdate";
if(!$r = $xoopsDB->query($sql))
    error_die(_MD_COULDNOTQUERY."</table></td></tr></table>");
list($all_topics) = $xoopsDB->fetch_row($r);
//   echo $all_topics;
$count = 1;
$next = $topics_start + $newbb_config['topics_per_page'];
if($all_topics > $newbb_config['topics_per_page']) {
   if($next < $all_topics) 
     echo "\n<a href=\"viewforum.php?forum=$forum&start=$next&sortname=$sortname&sortorder=$sortorder&sortdays=$sortdays\">"._MD_NEXTPAGE."</a> | ";
   for($x = 0; $x < $all_topics; $x++) {
      if(!($x % $newbb_config['topics_per_page'])) {
	 if($x == $topics_start)
	   echo "$count\n";
	 else
	   echo "<a href=\"viewforum.php?forum=$forum&start=$x&sortname=$sortname&sortorder=$sortorder&sortdays=$sortdays\">$count</a>\n";
	 $count++;
	 if(!($count % 10)) echo "<br>";
      }
   }
}
echo "<br>\n";
make_jumpbox();
?>
</td>
</tr></table>

<?php
include('page_tail.php');
CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>