<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// ORIGINAL FILE INFO
/***************************************************************************
                          index.php  -  description
                             -------------------
    begin                : Sat June 17 2000
    copyright            : (C) 2001 The phpBB Group
    email                : support@phpbb.com

 ***************************************************************************/


include("header.php");

if($xoopsConfig['startpage'] == "newbb"){
	$xoopsOption['show_rblock'] =1;
	include($xoopsConfig['root_path']."header.php");
	make_cblock();
	echo "<br />";
}else{
	$xoopsOption['show_rblock'] =0;
	include($xoopsConfig['root_path']."header.php");
}
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
$myts = new MyTextSanitizer;

OpenTable();
include($xoopsConfig['root_path']."modules/newbb/page_header.php");

$sql = "SELECT c.* FROM ".$xoopsDB->prefix("bb_categories")." c, ".$xoopsDB->prefix("bb_forums")." f
	 WHERE f.cat_id=c.cat_id
	 GROUP BY c.cat_id, c.cat_title, c.cat_order
	 ORDER BY c.cat_order";
if(!$result = $xoopsDB->query($sql,1))
	error_die(_MD_COULDNOTQUERY);
$total_categories = $xoopsDB->num_rows($result);

?>

<!-- text -->
<table cellpadding="2" cellspacing="0" border="0" width="<?php echo $TableWidth?>"  align="center">
<tr valign="bottom">
	<td><b><?php echo $xoopsConfig['sitename']." "._MD_FORUM."";?></b></td>
</tr>
</table>

<br />

<table cellpadding="2" cellspacing="0" border="0" width="<?php echo $TableWidth?>"  align="center">
<tr>
	<td colspan="2"><b><?php printf(_MD_WELCOME,$xoopsConfig['sitename']);?></b><br />
	<small><?php echo _MD_TOSTART;?></small><hr></td>
</tr>
<tr valign="bottom">
	<td><small>
	<?php echo _MD_TOTALTOPICSC;?><b><?php echo get_total_topics();?></b> | <?php echo _MD_TOTALPOSTSC;?><b><?php echo get_total_posts("0", "all");?></b><br />
	</small></td>
	<td align="right"><small>
<?php
	$currenttime = formatTimestamp(time(),"m");
	printf(_MD_TIMENOW,$currenttime);
	echo "<br />";
	$yourlastvisit = formatTimestamp($last_visit);
	printf(_MD_LASTVISIT,$yourlastvisit);?>
	</small></td>
</tr>
</table>
<!-- /text -->

<br />

<table border="0" width="<?php echo $TableWidth?>" cellpadding="1" cellspacing="0" align="center" valign="top"><tr><td bgcolor="<?php echo $xoopsTheme["bgcolor2"];?>">
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<tr bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" align="left">
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" align="center" valign="middle">&nbsp;</td>
	<td><font color="<?php echo $xoopsTheme["textcolor2"]?>"><b><?php echo _MD_FORUM;?></b></font></td>
	<td align="center"><font color="<?php echo $xoopsTheme["textcolor2"]?>"><b><?php echo _MD_TOPICS;?></b></font></td>
	<td align="center"><font color="<?php echo $xoopsTheme["textcolor2"]?>"><b><?php echo _MD_POSTS;?></b></font></td>
	<td align="center"><font color="<?php echo $xoopsTheme["textcolor2"]?>"><b><?php echo _MD_LASTPOST;?></b></font></td>
	<td align="center"><font color="<?php echo $xoopsTheme["textcolor2"]?>"><b><?php echo _MD_MODERATOR;?></b></font></td>
</tr>

<?php
if($total_categories)
{
   if(!isset($viewcat) || $viewcat == "")
     {
	$viewcat = -1;
     }
   while($cat_row = $xoopsDB->fetch_array($result))
     {
	$categories[] = $cat_row;
     }

   $limit_forums = "";
   if($viewcat != -1)
     {
	$limit_forums = "WHERE f.cat_id = $viewcat";
     }
   $sql = "SELECT f.*, u.uname, u.uid, p.topic_id, p.post_time, p.subject, p.icon
	    FROM ".$xoopsDB->prefix("bb_forums")." f
	    LEFT JOIN ".$xoopsDB->prefix("bb_posts")." p ON p.post_id = f.forum_last_post_id
	    LEFT JOIN ".$xoopsDB->prefix("users")." u ON u.uid = p.uid
	    $limit_forums
	    ORDER BY f.cat_id, f.forum_id";
   if(!$f_res = $xoopsDB->query($sql))
     {
	die("Error <br />$sql");
     }

   while($forum_data = $xoopsDB->fetch_array($f_res))
     {
	$forum_row[] = $forum_data;
     }
for($i = 0; $i < $total_categories; $i++) {
   if($viewcat != -1) {
      if($categories[$i]['cat_id'] != $viewcat) {
	$title = $myts->makeTboxData4Show($categories[$i]['cat_title']);
	echo "<tr align=\"left\" valign=\"top\"><td colspan=\"6\" bgcolor=\"".$xoopsTheme["bgcolor1"]."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><b><a href=\"$PHP_SELF?viewcat=".$categories[$i]['cat_id']."\">$title</a></b></font></td></tr>";
	continue;
     }
   }
   $title = $myts->makeTboxData4Show($categories[$i]['cat_title']);
   echo "<tr align=\"left\" valign=\"top\"><td colspan=\"6\" bgcolor=\"".$xoopsTheme["bgcolor3"]."\"><font color=\"".$xoopsTheme["textcolor2"]."\"><a href=\"$PHP_SELF?viewcat=".$categories[$i]['cat_id']."\"><b>$title</b></a></font></td></tr>";
   @reset($forum_row);
   for($x = 0; $x < count($forum_row); $x++)
     {
      unset($last_post);
      if($forum_row[$x]['cat_id'] == $categories[$i]['cat_id']) {
	 //$last_post = $last_posts[$forum_row[$x]["forum_id"]];
	 if($forum_row[$x]['post_time'])
	 {
		$forum_row[$x]['subject'] = $myts->makeTboxData4Show($forum_row[$x]['subject']);
	 	$last_post = formatTimestamp($forum_row[$x]['post_time']) . "<br />";
		$last_post .= "<a href=\"".$xoopsConfig['xoops_url']."/modules/newbb/viewtopic.php?topic_id=".$forum_row[$x]['topic_id']."&forum=".$forum_row[$x]['forum_id']."\">";
		if($forum_row[$x]['icon']){
			$last_post .= "<img src=\"".$xoopsConfig['xoops_url']."/images/subject/".$forum_row[$x]['icon']."\" border=\"0\" alt=\"\" />";
		}else{
			$last_post .= "<img src=\"".$xoopsConfig['xoops_url']."/images/subject/icon1.gif\" width=\"15\" height=\"15\" border=\"0\" alt=\"\" />";
		}
		$last_post .= "</a><br />"._MD_BY."&nbsp;";
		if($forum_row[$x]['uid'] != 0 && $forum_row[$x]['uname']){
			$last_post .= "<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$forum_row[$x]['uid']."\">" . $myts->makeTboxData4Show($forum_row[$x]['uname'])."</a>";
		}else{
			$last_post .= $xoopsConfig['anonymous'];
		}
	 }
	 $last_post_datetime = $forum_row[$x]['post_time'];
	 if(empty($last_post))
	 {
	 	$last_post = _MD_NOPOSTS;
	 }
	 echo "<tr align=\"left\" valign=\"top\">";
 
	if($last_post_datetime > $last_visit && $last_post != _MD_NOPOSTS) {
	    echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" align=\"center\" valign=\"middle\" width=\"5%\"><img src=\"".$newposts_image."\" alt=\"\" /></td>";
	 }
	 else {
	    echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" align=\"center\" valign=\"middle\" width=\"5%\"><img src=\"$folder_image\" alt=\"\" /></td>";
	 }
	 	$name = $myts->makeTboxData4Show($forum_row[$x]['forum_name']);
		$total_posts = $forum_row[$x]["forum_posts"];
		$total_topics = $forum_row[$x]["forum_topics"];
		$desc = $myts->makeTareaData4Show($forum_row[$x]['forum_desc']);
		$forum_link = $xoopsConfig['xoops_url']."/modules/newbb/viewforum.php?forum=".$forum_row[$x]["forum_id"]."&$total_posts";
	 	echo "<td bgcolor=\"".$xoopsTheme["bgcolor1"]."\"";
	//      if($newbb_config['mouse_rollover']){
      			echo " onMouseOver=\"this.style.backgroundColor='". $xoopsTheme["bgcolor3"] . "'; this.style.cursor='hand';\" onMouseOut=\"this.style.backgroundColor='" . $xoopsTheme["bgcolor1"] . "';\" onclick=\"window.location.href='" . $forum_link . "'\"";
//      	}
		echo "><font color=\"".$xoopsTheme["textcolor2"]."\"><a href=\"$forum_link\"><b>$name</b></a></font>\n";
	 	echo "<br /><font color=\"".$xoopsTheme["textcolor2"]."\">$desc</font></td>\n";
	 	echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" width=\"5%\" align=\"center\" valign=\"middle\"><font color=\"".$xoopsTheme["textcolor2"]."\">$total_topics</font></td>\n";
	 	echo "<td bgcolor=\"".$xoopsTheme["bgcolor1"]."\" width=\"5%\" align=\"center\" valign=\"middle\"><font color=\"".$xoopsTheme["textcolor2"]."\">$total_posts</font></td>\n";
	 	echo "<td bgcolor=\"".$xoopsTheme["bgcolor3"]."\" width=\"15%\" align=\"center\" valign=\"middle\"><font color=\"".$xoopsTheme["textcolor2"]."\">$last_post</font></td>\n";
	 	$forum_moderators = get_moderators($forum_row[$x]['forum_id']);
	 	echo "<TD BGCOLOR=\"".$xoopsTheme["bgcolor1"]."\" width=\"5%\" align=\"center\" valign=\"middle\" nowrap><font color=\"".$xoopsTheme["textcolor2"]."\">";
	 	$count = 0;

	 while(list($null, $mods) = each($forum_moderators)) {
	    while(list($mod_id, $mod_name) = each($mods)) {
	       if($count > 0)
		 echo ", ";
	       if(!($count % 2) && $count != 0)
		 echo "<br />";
	       echo "<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$mod_id."\">".$myts->makeTboxData4Show($mod_name)."</a>";
	       $count++;
	    }
	 }
	 echo "</font></td></tr>\n";
      }
    }
  }
}

?>
     </table></td></tr></table>
<form name="search" action="search.php" method="post">
<table align="center" border="0" width="<?php echo $TableWidth?>"><tr><td valign="middle">
<font color="<?php echo $xoopsTheme["textcolor2"]?>">
<img src="<?php echo $newposts_image?>" alt="" /> = <?php echo _MD_NEWPOSTS;?>.
<br /><img src="<?php echo $folder_image?>" alt="" /> = <?php echo _MD_NONEWPOSTS;?>.
</font></td>
<td align="right" valign="bottom">

<b><?php echo _MD_SEARCH;?></b>&nbsp;<input name="term" type="text" size="20"></input>
<input type="hidden" name="forum" value="all"></input>
<input type="hidden" name="sortby" value="p.post_time desc"></input>
<input type="hidden" name="searchboth" value="both"></input>
<input type="hidden" name="submit" value="<?php echo _MD_SEARCH;?>"></input>
<br />[ <a href="<?php echo $xoopsConfig['xoops_url'];?>/modules/newbb/search.php"><?php echo _MD_ADVSEARCH;?></a> ]


</td>
</tr></table>
</form>
<?php
include($xoopsConfig['root_path']."modules/newbb/page_tail.php");
CloseTable();
include_once($xoopsConfig['root_path']."footer.php");
?>