<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
$pagetype = "reply";
$xoopsOption['forumpage'] = 1;
include("header.php");

if(!isset($forum)){
	redirect_header("index.php", 2, _MD_ERRORFORUM);
	exit();
}elseif(!isset($topic_id)){
	redirect_header("viewforum.php?forum=$forum", 2, _MD_ERRORTOPIC);
	exit();
}elseif(!isset($post_id)){
	redirect_header("viewtopic.php?topic_id=$topic_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum", 2, _MD_ERRORPOST);
	exit();
}else{
	if(is_locked($topic_id)) {
	redirect_header("viewtopic.php?topic_id=$topic_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum", 2, _MD_TOPICLOCKED);
	exit();
	}	
	include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
	$sql = "SELECT forum_name, forum_access, forum_type FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = $forum";
	if(!$result = $xoopsDB->query($sql)){
		error_die(_MD_CANTGETFORUM);
	}
	$myrow = $xoopsDB->fetch_array($result);
	$myts=new MyTextsanitizer;
	$forum_name = $myts->makeTboxData4Show($myrow['forum_name']);
	$forum_access = $myrow['forum_access'];
	if ($myrow['forum_type'] == 1){
		// To get here, we have a logged-in user. So, check whether that user is allowed to post in
		// this private forum.
		$accesserror = 0; //initialize
		if ( $xoopsUser ) {
			if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
				if ( !check_priv_forum_auth($xoopsUser->uid(), $forum, TRUE) ) {
					$accesserror = 1;
				}
			}
		} else {
			$accesserror = 1;
		}
		if ( $accesserror == 1 ) {
			redirect_header("viewtopic.php?topic_id=$topic_id&post_id=$post_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",2,_MD_NORIGHTTOPOST);
			exit();
		}
		// Ok, looks like we're good.
	}
	include($xoopsConfig['root_path']."header.php");
	OpenTable();
	include_once("class/class.forumposts.php");
	include("page_header.php");
	$forumpost = new ForumPosts($post_id);
	$r_message = $forumpost->text();
	$r_date = formatTimestamp($forumpost->posttime());
	$r_name = XoopsUser::get_uname_from_id($forumpost->uid());
	$r_content = _MD_BY." ".$r_name." "._MD_ON." ".$r_date."<br><br>";
	$r_content .= $r_message;
	$r_subject=$forumpost->subject();
	$subject=$forumpost->subject("Edit");
	$q_message = $forumpost->text("Quotes");
	$hidden = "[quote]\n";
	$hidden .= sprintf(_MD_USERWROTE,$r_name);
	$hidden .= "\n".$q_message."\n[/quote]";
	themecenterposts($r_subject,$r_content);
	echo "<br>";
	$pid=$post_id;
	unset($post_id);
	$topic_id=$forumpost->topic();
	$forum=$forumpost->forum();
	$isreply =1;
	include("include/forumform.inc.php");
	include("page_tail.php");
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}
?>