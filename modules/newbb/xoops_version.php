<?php
$modversion['name'] = _MI_NEWBB_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_NEWBB_DESC;
$modversion['credits'] = "Enhanced and Modularized into XOOPS by<br>Kazumi Ono<br>( http://www.mywebaddons.com/ )";
$modversion['author'] = "Original phpBB 1.4.4 by<br>The phpBB Group<br>( http://www.phpbb.com/ )<br>";
$modversion['help'] = "newbb.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "images/phpbb_slogo.gif";
$modversion['dirname'] = "newbb";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Menu
$modversion['hasMain'] = 1;

// Blocks
$modversion['blocks'][1]['file'] = "newbb_new.php";
$modversion['blocks'][1]['name'] = _MI_NEWBB_BNAME1;
$modversion['blocks'][1]['description'] = "Shows recent topics in the forums";
$modversion['blocks'][1]['show_func'] = "b_newbb_new_show";
$modversion['blocks'][1]['options'] = "10";
$modversion['blocks'][1]['edit_func'] = "b_newbb_new_edit";

// Search
$modversion['hasSearch'] = 1;
$modversion['search']['file'] = "include/search.inc.php";
$modversion['search']['func'] = "newbb_search";
?>