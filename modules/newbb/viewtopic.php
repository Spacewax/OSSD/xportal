<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
include("header.php");

if ( !isset($forum) ) {
	redirect_header("index.php",2,_MD_ERRORFORUM);
	exit();
} elseif ( !isset($topic_id) ) {
	redirect_header("viewforum.php?forum=$forum",2,_MD_ERRORTOPIC);
	exit();
}

include($xoopsConfig['root_path']."header.php");
include_once("class/class.forumposts.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

$pagetype = "viewtopic";
$myts = new MyTextsanitizer;
$sql = "SELECT t.topic_title, t.topic_status, f.forum_name, f.forum_access, f.forum_type FROM ".$xoopsDB->prefix("bb_topics")." t LEFT JOIN ".$xoopsDB->prefix("bb_forums")." f ON f.forum_id = t.forum_id WHERE t.topic_id = $topic_id AND t.forum_id = $forum";

if ( !$result = $xoopsDB->query($sql) ) {
	error_die("<h4>"._MD_ERROROCCURED."</h4><hr>"._MD_COULDNOTQUERY."<br />$sql");
}

if ( !$forumdata = $xoopsDB->fetch_array($result) ) {
	error_die(_MD_FORUMNOEXIST);
}
	
$forum_name = $myts->makeTboxData4Show($forumdata['forum_name']);
if ( $forumdata['forum_type'] == 1 ) {
	// To get here, we have a logged-in user. So, check whether that user is allowed to view
	// this private forum.
	$accesserror = 0;
	if ( $xoopsUser ) {
		if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
			if ( !check_priv_forum_auth($xoopsUser->uid(), $forum, FALSE) ) {
				$accesserror = 1;
			}
		}
	} else {
		$accesserror = 1;
	}
	if ( $accesserror == 1 ) {
		redirect_header("index.php",2,_MD_NORIGHTTOACCESS);
		exit();
	}
		// Ok, looks like we're good.
}

$total = get_total_posts($topic_id, "topic");
if ( $total > $newbb_config['posts_per_page'] ) {
	$times = 0;
	for ($x = 0; $x < $total; $x += $newbb_config['posts_per_page'] ) {
		$times++;
	}
   	$pages = $times;
}

$forumdata['topic_title'] = $myts->makeTboxData4Show($forumdata['topic_title']);
OpenTable();

include("page_header.php");

include_once("include/forumuserpost.php");

if ( isset($post_id) && $post_id != "" ) {
	$forumpost = new ForumPosts($post_id);
} else {
	$forumpost = new ForumPosts();
}

if ( $xoopsUser ) {
	if ( !isset($order) ) {
		$order = $xoopsUser->uorder();
	}
	if ( !isset($viewmode) || $viewmode=="" ) {
		if ($xoopsUser->umode() != "nocomments") {
			$viewmode = $xoopsUser->umode();
		}
	}
}
if ( isset($order) && $order == 1 ) {
	$qorder = "post_time DESC";
} else {
	$order = "";
	$qorder = "post_time ASC";
}
$forumpost->setOrder($qorder);

if ( !isset($post_id) || $post_id == "" ) {
	$pid=0;
	$forumpost->setTopicId($topic_id);
	$forumpost->setParent($pid);
}

if ( isset($viewmode) && $viewmode=="thread" ) {
	$postsArray = $forumpost->getTopPosts();
} else {
	$viewmode = "flat";
	if ( isset($start) ) {
		$postsArray = $forumpost->getAllPosts($newbb_config['posts_per_page'], $start);
	} else {
		$postsArray = $forumpost->getAllPosts($newbb_config['posts_per_page']);
	}
}

if ( !$forumpost->parent() ) {
	include("include/navbar.php");
}

echo "<table border='0' cellpadding='1' cellspacing='0' align='center' valign='top' width='$TableWidth'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'><table border='0' cellpadding='3' cellspacing='1' width='100%'><tr bgcolor='".$xoopsTheme["bgcolor3"]."' align='left'>
<td width='20%' style='color: ".$xoopsTheme['textcolor2'].";'>". _MD_AUTHOR ."</td>
<td style='color: ". $xoopsTheme["textcolor2"].";'>". _MD_THREAD ."</td></tr>";

$row_color = $xoopsTheme["bgcolor1"];
$count = 0;

foreach ( $postsArray as $obj ) {
	$subject = $obj->subject();
	$post_text = $obj->text();
	// If it's been edited more than once, there might be old "edited by" strings with
   	// escaped HTML code in them. We want to fix this up right here:
   	$post_text = preg_replace("#&lt;font\ size\=-1&gt;\[\ "._MD_EDITEDBY."(.*?)\ \]&lt;/font&gt;#si", '<small>[ ' . _MD_EDITEDBY . '\1 ]</small>', $post_text);
	$post_text = preg_replace("#&lt;small&gt;\[\ "._MD_EDITEDBY."(.*?)\ \]&lt;/small&gt;#si", '<small>[ ' . _MD_EDITEDBY . '\1 ]</small>', $post_text);
	if ( !($count % 2) ) {
		$row_color = $xoopsTheme['bgcolor1'];
	} else {
		$row_color = $xoopsTheme['bgcolor3'];
	}
	
	forumuserpost($forumdata['forum_access'],$row_color,$obj->postid(),$obj->topic(),$obj->forum(),$obj->uid(),$subject,$post_text,$obj->attachsig(),$obj->posttime(),$obj->posterip(),$obj->icon(),$viewmode,$order,$forumdata['topic_status']);
	//if the mode is thread, show thread
	if ( $viewmode=="thread" ) {
		$treeArray = $forumpost->getPostTree($obj->postid());
					
		//if not in the top page, show links
		if ( $forumpost->parent() ) {
			echo "<tr bgcolor='$row_color'><td valign='top'>[&nbsp;<a href='".$PHP_SELF."?forum=$forum&amp;topic_id=$topic_id&amp;viewmode=$viewmode&amp;order=$order'>"._MD_TOP."</a>&nbsp;|&nbsp;<a href='$PHP_SELF?forum=$forum&amp;topic_id=$topic_id&amp;post_id=".$forumpost->parent()."&amp;viewmode=$viewmode&amp;order=$order'>"._MD_PARENT."</a>&nbsp;]</td><td valign='top'>";
		} elseif ( $treeArray ) {
			echo "<tr bgcolor='$row_color'><td valign='top'>&nbsp;</td><td valign='top'>";
		}
		if ( $treeArray ) {
			foreach ( $treeArray as $ele ) {
				$prefix = str_replace(".", "&nbsp;&nbsp;&nbsp;&nbsp;", $ele->prefix());
				$psubject = $ele->subject();
				$date = formatTimestamp($ele->posttime(),"s");
				$name = XoopsUser::get_uname_from_id($ele->uid());
				echo "".$prefix."<img src='".$xoopsConfig['xoops_url']."/images/icons/posticon.gif' alt='' />&nbsp;<a href='$PHP_SELF?forum=$forum&amp;topic_id=$topic_id&amp;post_id=".$ele->postid()."&amp;viewmode=$viewmode&amp;order=$order#".$ele->postid()."'>$psubject</a>&nbsp;"._MD_BY."&nbsp;$name&nbsp;"._MD_ON."&nbsp;$date<br />";
			}
			echo "</td></tr>";
		} else {
			// EMPTY
		}
	}
	
	$count++;			
}

if ( !isset($refresh) && $viewmode != "thread" ) {
	$sql = "UPDATE ".$xoopsDB->prefix("bb_topics")." SET topic_views = topic_views + 1 WHERE topic_id = $topic_id";
	$xoopsDB->query($sql);
}

echo "</table></td></tr></table>";

if ( !$forumpost->parent() ) {
	include("include/navbar.php");
}

echo "<br /><table width='$TableWidth'><tr><td>&nbsp;&nbsp;</td><td align='right'>";

make_jumpbox();
echo "</td></tr></table>";

if ( $xoopsUser ) {
	if ( $xoopsUser->is_admin($xoopsModule->mid()) || is_moderator($forum,$xoopsUser->uid()) ) {
		echo "<div style='text-align: center;'>";
		if ( $forumdata['topic_status'] != 1 ) {
			echo "<a href='".$url_phpbb."topicmanager.php?mode=lock&amp;topic_id=$topic_id&amp;forum=$forum'><img src='$locktopic_image' alt='"._MD_LOCKTOPIC."' /></a> ";
		} else {
			echo "<a href='".$url_phpbb."topicmanager.php?mode=unlock&amp;topic_id=$topic_id&amp;forum=$forum'><img src='$unlocktopic_image' alt='"._MD_UNLOCKTOPIC."' /></a> ";
		}

		echo "<a href='".$url_phpbb."topicmanager.php?mode=move&amp;topic_id=$topic_id&amp;forum=$forum'><img src='$movetopic_image' alt='"._MD_MOVETOPIC."' /></a> ";
		echo "<a href='".$url_phpbb."topicmanager.php?mode=del&amp;topic_id=$topic_id&amp;forum=$forum'><img src='$deltopic_image' alt='"._MD_DELETETOPIC."' /></a></div>\n";
	}
}

include("page_tail.php");
CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>