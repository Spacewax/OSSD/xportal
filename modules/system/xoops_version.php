<?php
$modversion['name'] = _MI_SYSTEM_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_SYSTEM_DESC;
$modversion['author'] = "";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "system.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "system_slogo.jpg";
$modversion['dirname'] = "system";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin.php";

// Blocks
$modversion['blocks'][1]['file'] = "system_blocks.php";
$modversion['blocks'][1]['name'] = _MI_SYSTEM_BNAME2;
$modversion['blocks'][1]['description'] = "Shows user block";
$modversion['blocks'][1]['show_func'] = "b_system_user_show";
$modversion['blocks'][2]['file'] = "system_blocks.php";
$modversion['blocks'][2]['name'] = _MI_SYSTEM_BNAME3;
$modversion['blocks'][2]['description'] = "Shows login form";
$modversion['blocks'][2]['show_func'] = "b_system_login_show";
$modversion['blocks'][3]['file'] = "system_blocks.php";
$modversion['blocks'][3]['name'] = _MI_SYSTEM_BNAME4;
$modversion['blocks'][3]['description'] = "Shows search form block";
$modversion['blocks'][3]['show_func'] = "b_system_search_show";
$modversion['blocks'][4]['file'] = "system_blocks.php";
$modversion['blocks'][4]['name'] = _MI_SYSTEM_BNAME5;
$modversion['blocks'][4]['description'] = "Shows contents waiting for approval";
$modversion['blocks'][4]['show_func'] = "b_system_waiting_show";
$modversion['blocks'][5]['file'] = "system_blocks.php";
$modversion['blocks'][5]['name'] = _MI_SYSTEM_BNAME6;
$modversion['blocks'][5]['description'] = "Shows contents waiting for approval";
$modversion['blocks'][5]['show_func'] = "b_system_main_show";
$modversion['blocks'][6]['file'] = "system_blocks.php";
$modversion['blocks'][6]['name'] = _MI_SYSTEM_BNAME7;
$modversion['blocks'][6]['description'] = "Shows basic info about the site and a link to Recommend Us pop up window";
$modversion['blocks'][6]['show_func'] = "b_system_info_show";
$modversion['blocks'][6]['edit_func'] = "b_system_info_edit";
$modversion['blocks'][6]['options'] = "320|250";

// Menu
$modversion['hasMain'] = 0;
?>