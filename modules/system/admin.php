<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

include("../../mainfile.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");
if ( file_exists("language/".$xoopsConfig['language']."/admin.php") ) {
	include("language/".$xoopsConfig['language']."/admin.php");
} else {
	include("language/english/admin.php");
}

/*********************************************************/
/* Admin Authentication                                  */
/*********************************************************/
$admintest = 0;

if ( $xoopsUser ) {
	$xoopsModule = XoopsModule::getByDirname("system");
	if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
		redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
		exit();
	}
	$admintest=1;
} else {
	redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
	exit();
}

if ( $admintest ) {
	if ( isset($fct) && $fct != "" ) { 
		if ( file_exists("admin/".$fct."/main.php") ) {
			if ( file_exists("language/".$xoopsConfig['language']."/admin/".$fct.".php") ) {
				include("language/".$xoopsConfig['language']."/admin/".$fct.".php");
			}
			include("admin/".$fct."/main.php");
		} else {
			adminMain();
		}
	} else {
		adminMain();
	}
} 

function adminMain() {
    	global $xoopsDB;
    	global $xoopsConfig, $xoopsTheme;
	$myts = new MyTextSanitizer();
    	include ($xoopsConfig['root_path']."header.php");
	system_menu();
    	include ($xoopsConfig['root_path']."footer.php");
}

/*********************************************************/
/* Core Menu Functions                                   */
/*********************************************************/

function system_menu_item($folder, $modversion) {
	global $xoopsConfig;

	if ( $xoopsConfig['admingraphic'] ) {
		echo "<a href='".$xoopsConfig['xoops_url']."/modules/system/admin.php?fct=".$folder."'><img src='".$xoopsConfig['xoops_url']."/modules/system/admin/".$folder."/".$modversion['image']."' alt='".$modversion['name']."' border='0' /><br /><b>" .trim($modversion['name'])."</b></a>\n";
	} else {
		echo "<a href='".$xoopsConfig['xoops_url']."/modules/system/admin.php?fct=".$folder."'><b>" .trim($modversion['name'])."</b></a>\n";
	}
}

function system_menu() {
	global $xoopsConfig, $xoopsUser, $xoopsModule;
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	echo "<table border='0' cellpadding='1' align='center'>";
	echo "<tr>";
	$i = 0;
	$admin_dir = $xoopsConfig['root_path']."modules/system/admin";
	$handle=opendir($admin_dir);
	$counter = 0;
	while ($file = readdir($handle)) {
		if ( !ereg('[.]',$file) ) {
			include($admin_dir."/".$file."/xoops_version.php");
			if ( $modversion['hasAdmin'] ) {
				echo "<td align='center' valign='bottom' width='19%'>";
				system_menu_item($file, $modversion);
				echo "</td>";
				$counter++;
			}
			if ( $counter > 4 ) {
				$counter = 0;
				echo "</tr>";
				echo "<tr>";
			}
		}
		unset($modversion);
	}
		
	echo "</tr>";
	echo "</table>";
	CloseTable();
}
?>