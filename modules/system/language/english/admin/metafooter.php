<?php
//%%%%%%	Admin Module Name  MetaFooter 	%%%%%
define("_AM_DBUPDATED",_MD_AM_DBUPDATED);

define("_AM_ERRDELOLD","Error doing deletion of old metafooter");
define("_AM_ERRINSERT","Error doing insertion of metafooter");
define("_AM_EDITMKF","Edit Meta Keywords & Footer");
define("_AM_METAKEY","Meta Keywords");
define("_AM_TYPEBYCOM","Type meta keywords separated by a comma. (Ex. myPHPNuke, PHP, mySQL, portal system)");
define("_AM_FOOTER","Footer");
define("_AM_BESURELINK","Be sure to type links in full path starting from http://, otherwise the links will not work correctly in modules pages.");
define("_AM_ADDCODE","Add Code");
define("_AM_CLEAR","Clear");
?>