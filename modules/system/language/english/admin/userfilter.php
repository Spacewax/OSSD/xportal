<?php
//%%%%%%	Admin Module Name  UserFilter 	%%%%%
define("_AM_DBUPDATED",_MD_AM_DBUPDATED);

define("_AM_MODIFY","Modify");
define("_AM_BWORDSSET","Bad Words Filter Settings");
define("_AM_IPFILTERSET","User/IP Filter Settings");
define("_AM_ERRORINSERT","Error. Could not insert into the DB");
define("_AM_UNEED2ENTER","You need to enter required info!");
define("_AM_ERRORDELETE","Error. Could not delete from the DB");
define("_AM_ERRORUPDATE","Error. Could not update the DB");
define("_AM_CURRENTWCENSOR","Current Word Censors");
define("_AM_TOMODIFYWORD","To modify a word and/or its replacement text simply change the values in the text boxes and click the Edit button.");
define("_AM_TOREMOVEWORD","To remove a censored word simply click on the 'Delete' button next to the word.");
define("_AM_WORD","Word");
define("_AM_REPLACEMENT","Replacement");
define("_AM_EDIT","Edit");
define("_AM_DELETE","Delete");
define("_AM_ERRORQUERY","Error. Could not query the DB");
define("_AM_NOWORDSINDB","No censored words in the database. You can enter one using the form below");
define("_AM_ADDAWORD","Add a Word");
define("_AM_FORM2ADDWORD","Use this form to add a word censor to the database.");
define("_AM_ACTION","Action");
define("_AM_ERRORNOUSER","Error. No such user!");
define("_AM_DURATION","Duration");
define("_AM_IPADDRESS","IP Address");
define("_AM_SECONDS","Seconds");
define("_AM_MINUTES","Minutes");
define("_AM_HOURS","Hours");
define("_AM_DAYS","Days");
define("_AM_YEARS","Years");
define("_AM_CURBANIP","Current Banned IPs");
define("_AM_CURBANUSER","Current Banned Usernames");
define("_AM_USERNAME","Username");
define("_AM_ADDABAN","Add a ban");
define("_AM_FORM2ADDBAN","Use the following form to add IPs or Usernames to the banlist.");
define("_AM_TOBANIPS","To ban a range of IPs simply do not enter the final IP number ie: 192.168.1. Will ban 192.168.1.0-255<br>
     Bans will be automaticly removed from the database when they expire, to create a permanent ban simply enter nothing in the duration field.");
define("_AM_IPUNAME","IP/Username");
?>