<?php
//%%%%%%	File Name  modulesadmin.php 	%%%%%
define("_MD_AM_MODADMIN","Modules Administration");
define("_MD_AM_MODULE","Module");
define("_MD_AM_VERSION","Version");
define("_MD_AM_LASTUP","Last Update");
define("_MD_AM_DEACTIVATED","Deactivated");
define("_MD_AM_ACTION","Action");
define("_MD_AM_DEACTIVATE","Deactivate");
define("_MD_AM_ACTIVATE","Activate");
define("_MD_AM_UPDATE","Update");
define("_MD_AM_DUPEN","Duplicate entry in modules table!");
define("_MD_AM_DEACTED","The selected module has been deactivated. You can now safely uninstall the module.");
define("_MD_AM_ACTED","The selected module has been activated!");
define("_MD_AM_UPDTED","The selected module has been updated!");
define("_MD_AM_SYSNO","Sysetm module cannot be deactivated.");
define("_MD_AM_STRTNO","This module is set as your default start page. Please first change the start page module of your site preferences.");
?>