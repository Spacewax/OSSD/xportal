<?php
//%%%%%%	Admin Module Name  Smilies 	%%%%%
define("_AM_DBUPDATED",_MD_AM_DBUPDATED);

define("_AM_CURRENTSMILE","Current Smilies");
define("_AM_CODE","Code");
define("_AM_SMILIE","Smilie");
define("_AM_ACTION","Action");
define("_AM_EDIT","Edit");
define("_AM_DEL","Delete");
define("_AM_CNRFTSD","Could not retrieve from the smilies database.");
define("_AM_ADDSMILE","Add a Smilie");
define("_AM_SMILECODE","Smilie Code:");
define("_AM_SMILEURL","Smilie URL:");
define("_AM_SMILEEMOTION","Smilie Emotion:");
define("_AM_ADD","Add");
define("_AM_SAVE","Save");
define("_AM_WAYSYWTDTS","WARNING: Are you sure you want to delete this Smile?");
define("_AM_YES","Yes");
define("_AM_NO","No");
?>