<?php
//%%%%%%	Admin Module Name  Blocks 	%%%%%
define("_AM_DBUPDATED",_MD_AM_DBUPDATED);

//%%%%%%	blocks.php 	%%%%%
define("_AM_BADMIN","Blocks Administration");
define("_AM_ADDBLOCK","Add a new block");
define("_AM_LISTBLOCK","List all blocks");
define("_AM_SIDEBADMIN","Side Blocks Administration");
define("_AM_SIDE","Side");
define("_AM_BLKDESC","Block Description");
define("_AM_TITLE","Title");
define("_AM_WEIGHT","Weight");
define("_AM_ACTION","Action");
define("_AM_CENTERBADMIN","Center Block Administration");
define("_AM_NO","No");
define("_AM_YES","Yes");
define("_AM_BLKTYPE","Block Type");
define("_AM_LEFT","Left");
define("_AM_RIGHT","Right");
define("_AM_CENTER","Center");
define("_AM_VISIBLE4","Visible for");
define("_AM_NOTVIS","Not Visible");
define("_AM_ALL","All");
define("_AM_REGUSERS","Registered users");
define("_AM_ADMINS","Admins");
define("_AM_POSCONTT","Position of the additional content");
define("_AM_ABOVEORG","Above the original content");
define("_AM_AFTERORG","After the original content");
define("_AM_EDIT","Edit");
define("_AM_DELETE","Delete");
define("_AM_SBLEFT","Side Block - Left");
define("_AM_SBRIGHT","Side Block - Right");
define("_AM_CBLEFT","Center Block - Left");
define("_AM_CBRIGHT","Center Block - Right");
define("_AM_CBCENTER","Center Block - Center");
define("_AM_TITLE","Title");
define("_AM_CONTENT","Content");
define("_AM_OPTIONS","Options");
define("_AM_CTYPE","Content Type");
define("_AM_HTML","HTML");
define("_AM_PHP","PHP Script");
define("_AM_AFWSMILE","Auto Format (smilies enabled)");
define("_AM_AFNOSMILE","Auto Format (smilies disabled)");
define("_AM_SUBMIT","Submit");
define("_AM_CUSTOMHTML","Custom Block (HTML)");
define("_AM_CUSTOMPHP","Custom Block (HTML)");
define("_AM_CUSTOMSMILE","Custom Block (Auto Format - Smilies Enabled)");
define("_AM_CUSTOMNOSMILE","Custom Block (Auto Format - Smilies Disabled)");
define("_AM_DISPRIGHT","Display only rightblocks");
define("_AM_SAVECHANGES","Save Changes");
define("_AM_EDITBLOCK","Edit a block");
define("_AM_SYSTEMCANT","System blocks cannot be deleted!");
define("_AM_MODULECANT","This block cannot be deleted direclty! If you wish to disable this block, deactivate the module.");
define("_AM_RUSUREDEL","Are you sure you want to delete block <b>%s</b>?");
define("_AM_NAME","Name");

?>