<?php
// Blocks
define("_MB_SYSTEM_TITLE1","Administration");
define("_MB_SYSTEM_ADMENU","Administration Menu");
define("_MB_SYSTEM_WHOS","Who's Online");
define("_MB_SYSTEM_LOGIN","Login");
define("_MB_SYSTEM_NICK","Nickname");
define("_MB_SYSTEM_PASS","Password");
define("_MB_SYSTEM_DHAAY","Don't have an account yet?");
define("_MB_SYSTEM_RNOW","Register now!");
define("_MB_SYSTEM_LPASS","Lost Password?");
define("_MB_SYSTEM_SEARCH","Search");
define("_MB_SYSTEM_ADVS","Advanced Search");
define("_MB_SYSTEM_MENU4","Menu for %s");  // %s represents username
define("_MB_SYSTEM_VACNT","View Account");
define("_MB_SYSTEM_LOUT","Logout");
define("_MB_SYSTEM_NNMSG","No New Messages");
define("_MB_SYSTEM_YDMSG","You don't have any Messages.");
// %s represents a number
define("_MB_SYSTEM_NMSGS","%s New Messages");
// %s represents a number
define("_MB_SYSTEM_TMSGS","%s Total Messages.");
define("_MB_SYSTEM_WCNT","Waiting Contents");
define("_MB_SYSTEM_SUBMS","Submitted News");
define("_MB_SYSTEM_WLNKS","Waiting Links");
define("_MB_SYSTEM_BLNK","Broken Links");
define("_MB_SYSTEM_MLNKS","Modified Links");
define("_MB_SYSTEM_WDLS","Waiting Downloads");
define("_MB_SYSTEM_BFLS","Broken Files");
define("_MB_SYSTEM_MFLS","Modified Downloads");
define("_MB_SYSTEM_MMENU","Main Menu");
define("_MB_SYSTEM_HOME","Home"); // link to home page in main menu block
define("_MB_SYSTEM_INFO","Information");
define("_MB_SYSTEM_RECO","Recommend Us");
define("_MB_SYSTEM_PWWIDTH","Pop-Up Window Width");
define("_MB_SYSTEM_PWHEIGHT","Pop-Up Window Height");
define("_MB_SYSTEM_LOGO","Logo image file under %s directory");  // %s is your root image directory name
?>