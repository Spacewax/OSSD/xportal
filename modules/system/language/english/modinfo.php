<?php
// Module Info

// The name of this module
define("_MI_SYSTEM_NAME","System Admin");

// A brief description of this module
define("_MI_SYSTEM_DESC","For administration of core settings of the site.");

// Names of blocks for this module (Not all module has blocks)
define("_MI_SYSTEM_BNAME2","User Block");
define("_MI_SYSTEM_BNAME3","Login Block");
define("_MI_SYSTEM_BNAME4","Search Block");
define("_MI_SYSTEM_BNAME5","Waiting Contents Block");
define("_MI_SYSTEM_BNAME6","Main Menu Block");
define("_MI_SYSTEM_BNAME7","Site Info Block");
?>