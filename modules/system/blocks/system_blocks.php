<?php
function b_system_login_show(){
	global $xoopsUser, $xoopsConfig, $HTTP_COOKIE_VARS;
	if (!$xoopsUser) {
		$block = array();
		$block['title'] = _MB_SYSTEM_LOGIN;
    		$block['content'] = "<div align='center'><p><form action='".$xoopsConfig['xoops_url']."/user.php' method='post'>";
    		$block['content'] .= _MB_SYSTEM_NICK."<br />";
    		$block['content'] .= "<input type='text' name='uname' size='12' maxlength='25'";
		if(isset($HTTP_COOKIE_VARS[$xoopsConfig['usercookie']])){
			$block['content'] .= " value='".$HTTP_COOKIE_VARS[$xoopsConfig['usercookie']]."'";
		}
		$block['content'] .= " /><br />";
    		$block['content'] .= _MB_SYSTEM_PASS."<br />";
    		$block['content'] .= "<input type='password' name='pass' size='12' maxlength='20' /><br />";
    		$block['content'] .= "<input type='hidden' name='op' value='login' />";
    		$block['content'] .= "<input type='submit' value='"._MB_SYSTEM_LOGIN."' /></form></p>";
		$block['content'] .= "<a href='".$xoopsConfig['xoops_url']."/user.php#lost'>"._MB_SYSTEM_LPASS."</a><br /><br />";
    		$block['content'] .= _MB_SYSTEM_DHAAY."<br />";
    		$block['content'] .= " <a href='".$xoopsConfig['xoops_url']."/register.php'>"._MB_SYSTEM_RNOW."</a>";
    		$block['content'] .= "</div>";
    		return $block;
    	}
	return FALSE;
}

function b_system_main_show(){
	global $xoopsDB, $xoopsConfig;
	$block = array();
	$block['title'] = _MB_SYSTEM_MMENU;
	include($xoopsConfig['root_path']."modules/system/cache/mainmenu.txt");
	$block['content'] = $mainmenu;
	return $block;
}

function b_system_search_show(){
	global $xoopsConfig;
	$block = array();
	$block['title'] = _MB_SYSTEM_SEARCH;
	$block['content'] = "<div align='center'><br /><form action='".$xoopsConfig['xoops_url']."/search.php' method='post'>\n";
    	$block['content'] .= "<input type='text' name='query' size='14' />\n";
	$block['content'] .= "<input type='hidden' name='action' value='results' />\n";
	$block['content'] .= "<br /><input type='submit' value='"._MB_SYSTEM_SEARCH."' />\n";
    	$block['content'] .= "</form>\n";
	$block['content'] .= "<a href='".$xoopsConfig['xoops_url']."/search.php'>"._MB_SYSTEM_ADVS."</a></div>";
	return $block;
}

function b_system_user_show(){
	global $xoopsDB, $xoopsUser, $xoopsConfig;
	if($xoopsUser) {
		$block = array();
		$block['title'] = sprintf(_MB_SYSTEM_MENU4,$xoopsUser->uname());
		$block['content'] = "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/user.php'>"._MB_SYSTEM_VACNT."</a><br />";
		$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/user.php?op=logout'>"._MB_SYSTEM_LOUT."</a><br /><br />";
		// Code for Messages
        	list($total_messages) = $xoopsDB->fetch_row($xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("priv_msgs")." WHERE to_userid = ".$xoopsUser->uid().""));
        	list($new_messages) = $xoopsDB->fetch_row($xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("priv_msgs")." WHERE to_userid = ".$xoopsUser->uid()." AND read_msg=0"));
        	if ($total_messages > 0) {
			if ($new_messages > 0) {
				$block['content'] .= "<a href=".$xoopsConfig['xoops_url']."/viewpmsg.php>";
				$block['content'] .= sprintf(_MB_SYSTEM_NMSGS,$new_messages );
				$block['content'] .= "</a><br />";
			}else{
				$block['content'] .= _MB_SYSTEM_NNMSG." <br /> ";
			}
			$block['content'] .= "<a href=".$xoopsConfig['xoops_url']."/viewpmsg.php>";
			$block['content'] .= sprintf(_MB_SYSTEM_TMSGS,$total_messages);
			$block['content'] .= "</a>";
		} else {
			$block['content'] .= "<a href=".$xoopsConfig['xoops_url']."/viewpmsg.php>"._MB_SYSTEM_YDMSG."</a>";
		}
		if ( $xoopsUser->is_admin() ) {
			$block['content'] .= "<br /><br /><strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/admin.php'>"._MB_SYSTEM_ADMENU."</a><br /><strong><big>&middot;</big></strong>&nbsp;<a href='javascript:openWithSelfMain(\"".$xoopsConfig['xoops_url']."/misc.php?action=showpopups&amp;type=whosonline&amp;t=".time()."\",\"Online\",300,350);'>"._MB_SYSTEM_WHOS."</a>";
		}
		// Code for Messages
		return $block;
	}
	return FALSE;
}

function b_system_waiting_show(){
	global $xoopsDB, $xoopsConfig, $xoopsUser;
	if ( $xoopsUser ) {
		$block = array();
		$block['title'] = _MB_SYSTEM_WCNT;
    		$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("stories")." WHERE published=0");
    		list($num) = $xoopsDB->fetch_row($result);
    		$block['content'] = "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/news/admin/index.php?op=newarticle'>"._MB_SYSTEM_SUBMS."</a>: $num<br />\n";

		$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mylinks_links")." WHERE status=0");
		list($num) = $xoopsDB->fetch_row($result);
    		$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/mylinks/admin/index.php?op=listNewLinks'>"._MB_SYSTEM_WLNKS."</a>: $num<br />\n";
    		$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mylinks_broken")."");
    		list($totalbrokenlinks) = $xoopsDB->fetch_row($result);
    		$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/mylinks/admin/index.php?op=listBrokenLinks'>"._MB_SYSTEM_BLNK."</a>: $totalbrokenlinks<br />\n";
    		$result2 = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mylinks_mod")."");
    		list($totalmodrequests) = $xoopsDB->fetch_row($result2);
		$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/mylinks/admin/index.php?op=listModReq'>"._MB_SYSTEM_MLNKS."</a>: $totalmodrequests<br />\n";
		$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE status=0");
		list($num) = $xoopsDB->fetch_row($result);
    		$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/mydownloads/admin/index.php?op=listNewDownloads'>"._MB_SYSTEM_WDLS."</a>: $num<br />\n";
    		$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_broken")."");
    		list($totalbrokenfiles) = $xoopsDB->fetch_row($result);
    		$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/mydownloads/admin/index.php?op=listBrokenDownloads'>"._MB_SYSTEM_BFLS."</a>: $totalbrokenfiles<br />\n";
    		$result2 = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_mod")."");
    		list($totalmodrequests) = $xoopsDB->fetch_row($result2);
		$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/mydownloads/admin/index.php?op=listModReq'>"._MB_SYSTEM_MFLS."</a>: $totalmodrequests<br />\n";

		return $block;
	}
	return FALSE;
}

function b_system_info_show($options){
	global $xoopsConfig, $xoopsUser;
	$block = array();
	$block['title'] = _MB_SYSTEM_INFO;
	$block['content'] = "<div style='text-align: center;'><p><img src='".$xoopsConfig['xoops_url']."/images/".$options[2]."' alt='".$xoopsConfig['sitename']."' border='0' />";
	if ( $xoopsUser ) {
		$block['content'] .= "<br /><a href='javascript:openWithSelfMain(\"".$xoopsConfig['xoops_url']."/misc.php?action=showpopups&type=friend&amp;op=sendform&amp;t=".time()."\",\"friend\",".$options[0].",".$options[1].")'>"._MB_SYSTEM_RECO."</a>";
	}
	$block['content'] .= "</p>\n";
    	$block['content'] .= waspInfo()."</div>";
	return $block;
}

function b_system_info_edit($options){
	global $xoopsConfig;
	$form = ""._MB_SYSTEM_PWWIDTH."&nbsp;";
	$form .= "<input type='text' name='options[]' value='".$options[0]."' />";
	$form .= "<br />"._MB_SYSTEM_PWHEIGHT."&nbsp;";
	$form .= "<input type='text' name='options[]' value='".$options[1]."' />";
	$form .= "<br />".sprintf(_MB_SYSTEM_LOGO,$xoopsConfig['xoops_url']."/images/")."&nbsp;";
	$form .= "<input type='text' name='options[]' value='".$options[2]."' />";
	return $form;
}
?>