<?php
$modversion['name'] = _MD_AM_FLTR;
$modversion['version'] = "";
$modversion['description'] = "Filter bad words & filter users by IP or username";
$modversion['author'] = "phpBB Group<br>( http://www.phpbb.com/ )";
$modversion['credits'] = "Modularized into MPN-SE by<br>Kazumi Ono<br>( http://www.mywebaddons.com/ )";
$modversion['help'] = "userfilter.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "userfilter.gif";

$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin.php?fct=userfilter";

?>