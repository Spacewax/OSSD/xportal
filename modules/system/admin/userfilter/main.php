<?php
if ( !eregi("admin.php", $PHP_SELF) ) {
	die("Access Denied");
} 
include ("admin/userfilter/userfilter.php");
switch ($op){
	default:
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		userfilter();
		include($xoopsConfig['root_path']."footer.php");
		break;
	case "badwordsAdd":
		badwordsAdd($bad_word, $replacement);
		break;
	case "badwordsEdit":
		badwordsEdit($bad_word, $replacement, $word_id);
		break;
	case "badwordsDelete":
		badwordsDelete($word_id);
		break;
	case "badwords":
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		userfilter();
		echo "<br />";
		badwords();
		include($xoopsConfig['root_path']."footer.php");
		break;
	case "banuserAdd":
		banuserAdd($durtype, $ipuser, $banby, $duration);
		break;
	case "banuserEdit":
		banuserEdit($ban_id, $unit, $dur, $ipaddy, $user_id);
		break;
	case "banuserDelete":
		banuserDelete($ban_id);
		break;
	case "banuser":
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		userfilter();
		echo "<br />";
		banuser();
		include($xoopsConfig['root_path']."footer.php");
		break;
}
?>