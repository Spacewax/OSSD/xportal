<?php
if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

function userfilter(){
	OpenTable();
	echo " - <b><a href='admin.php?fct=userfilter&op=badwords'>"._AM_BWORDSSET."</a></b>";
	echo "<br /><br />";
	echo " - <b><a href='admin.php?fct=userfilter&op=banuser'>"._AM_IPFILTERSET."</a></b> Not working yet";
	
    	CloseTable();
}

function badwordsAdd($bad_word, $replacement){
	global $xoopsDB;
	if($bad_word != "" && $replacement != "") {
	    	$myts = new MyTextSanitizer;
	    	$bad_word = $myts->makeTboxData4Save($bad_word);
	    	$replacement = $myts->makeTboxData4Save($replacement);
		$wordstable = $xoopsDB->prefix("bad_words");
		$newid = $xoopsDB->GenID("badwords_word_id_seq");
	    	$sql = "INSERT INTO ".$wordstable." (word_id, word, replacement) VALUES ($newid, '$bad_word', '$replacement')";
	    	if(!$r = $xoopsDB->query($sql)) {
	       		$message = _AM_ERRORINSERT;
	      	 	break;
	    	} else {
	       		$message = _AM_DBUPDATED;
	    	}
	} else {
	    	$message = _AM_UNEED2ENTER;
	}
	redirect_header("admin.php?fct=userfilter&op=badwords",3,$message);
	exit();
}
function badwordsDelete($word_id){
	global $xoopsDB;
	$sql = "DELETE FROM ".$xoopsDB->prefix("bad_words")." WHERE word_id = $word_id";
	if(!$r = $xoopsDB->query($sql)) {
	    	$message = _AM_ERRORDELETE;
	    	break;
	} else {
	    	$message = _AM_DBUPDATED;
	}
	redirect_header("admin.php?fct=userfilter&op=badwords",3,$message);
	exit();
}

function badwordsEdit($bad_word, $replacement, $word_id){
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$bad_word = $myts->makeTboxData4Save($bad_word);
	$replacement = $myts->makeTboxData4Save($replacement);
	$sql = "UPDATE ".$xoopsDB->prefix("bad_words")." SET word = '$bad_word', replacement = '$replacement' WHERE word_id = $word_id";
	if(!$r = $xoopsDB->query($sql)) {
	    	$message =  _AM_ERRORUPDATE;
	    	break;
	} else {
	    	$message =  _AM_DBUPDATED;
	}
	redirect_header("admin.php?fct=userfilter&op=badwords",3,$message);
	exit();
}

function badwords(){
	global $xoopsDB;
	OpenTable();
?>
     	<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="95%"><tr><td>
     	<table border="0" cellpadding="1" cellspacing="1" width="100%">
     	<tr align="left">
             <td align="center" colspan="4"><h4><?php echo _AM_CURRENTWCENSOR;?></h4><?php echo _AM_TOMODIFYWORD;?><br />
             <?php echo _AM_TOREMOVEWORD;?></td>
     	</tr>
     	<tr align="left">
             <td align="center"><?php echo _AM_WORD;?></td>
             <td align="center"><?php echo _AM_REPLACEMENT;?></td>
             <td align="center"><?php echo _AM_EDIT;?></td>
             <td align="center"><?php echo _AM_DELETE;?></td>
     	</tr>
<?php
     	$sql = "SELECT * FROM ".$xoopsDB->prefix("bad_words")."";
   	if(!$r = $xoopsDB->query($sql)) {
      		echo "<td align=\"center\" colspan=\"6\">"._AM_ERRORQUERY."</td></tr></table></table>";
      		include($xoopsConfig['root_path']."footer.php");
      		exit();
   	}
   	if($m = $xoopsDB->fetch_array($r)) {
		$myts = new MyTextSanitizer;
      		do {
			$m['word']=$myts->makeTboxData4Edit($m['word']);
			$m['replacement']=$myts->makeTboxData4Edit($m['replacement']);
	 		echo "<form action=\"admin.php\" method=\"post\">\n";
	 		echo "<tr align=\"center\">\n";
	 		echo "<td><input type=\"text\" name=\"bad_word\" value=\"" . $m['word'] . "\" maxlength=\"50\" size=\"25\"></td>\n";
	 		echo "<td><input type=\"text\" name=\"replacement\" value=\"" . $m['replacement'] . "\" maxlength=\"50\" size=\"25\"></td>\n";
	 		echo "<td><input type=\"hidden\" name=\"word_id\" value=\"".$m['word_id']."\">\n";
?>
		<input type="hidden" name="fct" value="userfilter"><input type="hidden" name="op" value="badwordsEdit">
<?php
	 		echo "<input type=\"submit\" name=\"action\" value=\""._AM_EDIT."\"></td>\n";
	 		echo "<td><br /><input type=\"button\" name=\"action\" value=\""._AM_DELETE."\" onclick=\"location='admin.php?fct=userfilter&op=badwordsDelete&word_id=".$m['word_id']."'\"></form></td>\n";
	 		echo "</tr>";
      		} while($m = $xoopsDB->fetch_array($r));
   	} else {
      		echo "<tr align=\"center\"><td colspan=\"4\">"._AM_NOWORDSINDB."</td></tr>";
   	}
?>
     	<tr bgcolor="<?php echo $color1?>" align="left">
             <td align="center" colspan="4"><h4><?php echo _AM_ADDAWORD;?></h4><?php echo _AM_FORM2ADDWORD;?>
             </td><form action="admin.php" method="post">
     	</tr>
     	<tr align="center">
             <td colspan="2"><?php echo _AM_WORD;?></td>
             <td><?php echo _AM_REPLACEMENT;?></td>
             <td><?php echo _AM_ACTION;?></td>
     	</tr>
     	<tr align="center">
             <td colspan="2"><input type="text" name="bad_word" maxlength="50" size="25"></td>
             <td><input type="text" name="replacement" maxlength="50" size="25"></td>
             <td><input type="hidden" name="fct" value="userfilter"><input type="hidden" name="op" value="badwordsAdd">
             <input type="submit" name="action" value="<?php echo _AM_ADDAWORD;?>"></td>
             </form>
     	</tr>
<?php
     	echo "</table></td></tr></table>\n";
	CloseTable();
}

function banuserAdd($durtype, $ipuser, $banby, $duration){
	global $xoopsDB;
      	$starttime = mktime (date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
//	echo $durtype;
//	echo $duration;
      	switch($durtype) {
       		case 1:
			$type = 1;
	 		break;
       		case 2:
	 		$type = 60;
	 		break;
       		case 3:
	 		$type = 3600;
	 		break;
       		case 4:
	 		$type = 86400;
	 		break;
       		case 5:
		 	$type = 31536000;
	 		break;
      	}
      	if(!isset($duration))
		$duration = 0;

      	if($duration != 0)
		$endtime = $starttime + ($duration * $type);
      	else
		$endtime = 0;
	$newid = $xoopsDB->GenID("user_banlist_ban_id_seq");
      	if($banby == 1) {
		
		$sql = "INSERT INTO ".$xoopsDB->prefix("user_banlist")." (ban_id, ban_ip, ban_start, ban_end, ban_time_type) VALUES ($newid, '$ipuser', $starttime, $endtime, $durtype)";
		if(!$r = $xoopsDB->query($sql)){
	 		$message = _AM_ERRORINSERT;
		}else{
	 		$message = _AM_DBUPDATED;
		}
      	} else {
	    	$sql = "INSERT INTO ".$xoopsDB->prefix("user_banlist")." (ban_id, ban_userid, ban_start, ban_end, ban_time_type) VALUES ($newid, $ipuser, $starttime, $endtime, $durtype)";

	    	if(!$r = $xoopsDB->query($sql)){
	    		$message =  _AM_ERRORINSERT;
		} else {
	    		$message =  _AM_DBUPDATED;
		}
      	}
	redirect_header("admin.php?fct=userfilter&op=banuser",3,$message);
	exit();
}

function banuserDelete($ban_id) {
	global $xoopsDB;
      	$sql = "DELETE FROM ".$xoopsDB->prefix("user_banlist")." WHERE ban_id = $ban_id";
      	if(!$r = $xoopsDB->query($sql)){
		$message = _AM_ERRORDELETE;
	}else{
      		$message = _AM_DBUPDATED;
	}
	redirect_header("admin.php?fct=userfilter&op=banuser",3,$message);
	exit();
}

function banuserEdit($ban_id, $unit, $dur, $ipaddy, $user_id) {
	global $xoopsDB;
      	$starttime = mktime (date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
      	switch($unit) {
       		case 1:
	 		$type = 1;
	 		break;
       		case 2:
	 		$type = 60;
	 		break;
       		case 3:
	 		$type = 3600;
		 	break;
       		case 4:
	 		$type = 86400;
	 		break;
       		case 5:
	 		$type = 31536000;
	 		break;
      	}
      	if(!isset($dur) || $dur == "")
		$dur = 0;

      	if($dur != 0)
		$endtime = $starttime + ($dur * $type);
      	else
		$endtime = 0;
      	if(isset($ipaddy) && $ipaddy != ""){
		$sql = "UPDATE ".$xoopsDB->prefix("user_banlist")." SET ban_ip = '$ipaddy', ban_start = $starttime, ban_end = $endtime, ban_time_type = $unit WHERE ban_id = $ban_id";
      	} else {
		$sql = "UPDATE ".$xoopsDB->prefix("user_banlist")." SET ban_userid = $user_id, ban_start = $starttime, ban_end = $endtime, ban_time_type = $unit WHERE ban_id = $ban_id";
      	}

      	if(!$r = $xoopsDB->query($sql)){
		$message = _AM_ERRORUPDATE;
	} else {
      		$message = _AM_DBUPDATED;
	}
	redirect_header("admin.php?fct=userfilter&op=banuser",3,$message);
	exit();
}

function banuser(){
	global $xoopsDB;
	OpenTable();
?>
<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="95%"><tr><td>
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<tr align="left">
	<td align="center" colspan="4"><h4><?php echo _AM_CURBANIP;?></h4></td>
</tr>
<tr align="center">
	<td><?php echo _AM_IPADDRESS;?></td>
	<td><?php echo _AM_DURATION;?></td>
	<td><?php echo _AM_EDIT;?></td>
     	<td><?php echo _AM_DELETE;?></td>
</tr>
<?php
     	$sql = "SELECT * FROM ".$xoopsDB->prefix("user_banlist")." WHERE ban_ip";
   	if(!$r = $xoopsDB->query($sql))
     		echo "<tr align=\"center\"><td colspan=\"4\"><b>"._AM_ERRORQUERY."</b></td></tr>";
	//echo $xoopsDB->num_rows($r);
   	while($banlist = $xoopsDB->fetch_array($r)) {
      		unset($dur);
      		unset($unit);
      		echo "<tr align=\"center\"><td><form action=\"admin.php\" method=\"POST\"><input type=\"text\" name=\"ipaddy\" value=\"".$banlist['ban_ip']."\" size=\"32\"></td>\n";
      		$type = $banlist['ban_time_type'];
      		if($banlist['ban_end'] == 0) {
	 		$dur = "Parmanent";
			$unit = "Ban";
      		} else {
	 		switch($type) {
	  			case 1:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']);
	    				$unit = "Seconds";
	    				$s = "selected";
	    				break;
	  			case 2:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 60;
	 	   			$unit = "Minutes";
	    				$m = "selected";
	    				break;
	  			case 3:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 3600;
	    				$unit = "Hours";
	    				$h = "selected";
	    				break;
	  			case 4:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 86400;
	    				$unit = "Days";
	    				$d = "selected";
	    				break;
	  			case 5:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 31536000;
	    				$unit = "Years";
	    				$y = "selected";
	    				break;
	 		}
      		}

      		if($unit != "Ban") {
	 		echo "<td align=\"center\"><input type=\"text\" name=\"dur\" size=\"".strlen($dur)."\" maxlengh=\"25\" value=\"$dur\">\n";
	 		echo "<select name=\"unit\"><option value=\"1\" $s>"._AM_SECONDS."</option>
			<option value=\"2\" $m>"._AM_MINUTES."</option>
			<option value=\"3\" $h>"._AM_HOURS."</option>
			<option value=\"4\" $d>"._AM_DAYS."</option>
			<option value=\"5\" $y>"._AM_YEARS."</option></select></td>";
      		} else {
	 		echo "<td align=\"center\">$dur $unit</td>";
      		}
      		echo "<td><input type=\"hidden\" name=\"ban_id\" value=\"".$banlist['ban_id']."\">";
?>
<input type="hidden" name="fct" value="userfilter"><input type="hidden" name="op" value="banuserEdit">
<?php
      		echo "<input type=\"submit\" name=\"edit\" value=\""._AM_EDIT."\"></td>";
      		echo "<td><br /><input type=\"button\" name=\"del\" value=\""._AM_DELETE."\" onclick=\"location='admin.php?fct=userfilter&op=banuserDelete&ban_id=".$banlist['ban_id']."'\"></form></td>";
      		echo "</tr>";
   	}
?>
<tr align="left">
	<td align="center" colspan="4"><h4><?php echo _AM_CURBANUSER;?></h4></td>
</tr>
<tr align="center">
	<td><?php echo _AM_USERNAME;?></td>
	<td><?php echo _AM_DURATION;?></td>
	<td><?php echo _AM_EDIT;?></td>
     	<td><?php echo _AM_DELETE;?></td>
</tr>
<?php
     	unset($banlist);
   	unset($dur);
   	unset($unit);
	
     	$sql = "SELECT * FROM ".$xoopsDB->prefix("user_banlist")." WHERE ban_userid";
   	if(!$r = $xoopsDB->query($sql))
     		echo "<tr><td colspan=\"4\"><b>"._AM_ERRORQUERY."</b></td></tr>";
	
   	while($banlist = $xoopsDB->fetch_array($r)) {
      		$banuserdata = new XoopsUser($banlist['ban_userid']);
      		echo "<tr align=\"center\"><td align=\"center\"><form action=\"admin.php\" method=\"post\">\n";
		echo "<select name=\"user_id\">\n";
		$result = $xoopsDB->query("SELECT uid, uname from ".$xoopsDB->prefix("users")." WHERE level>0 ORDER BY uname");
		while($myrow = $xoopsDB->fetch_array($result)){
			echo "<option value=\"".$myrow['uid']."\"";
			if($myrow['uid'] == $banlist['ban_userid']){
				echo " selected=\"selected\"";
			}
			echo ">".$myrow['uname']."</option>\n";
		}
		echo "</select>\n";
		echo "</td>\n";
      		$type = $banlist['ban_time_type'];
      		if($banlist['ban_end'] == 0) {
	 		$dur = "Permanent";
	 		$unit = "Ban";
      		} else {
	 		switch($type) {
	  			case 1:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']);
	    				$unit = "Seconds";
	    				$s = "selected";
	    				break;
	  			case 2:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 60;
	    				$unit = "Minutes";
	    				$m = "selected";
	    				break;
	  			case 3:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 3600;
	    				$unit = "Hours";
	    				$h = "selected";
	    				break;
	  			case 4:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 86400;
	    				$unit = "Days";
	   	 			$d = "selected";
	    				break;
	  			case 5:
	    				$dur = ($banlist['ban_end'] - $banlist['ban_start']) / 31536000;
	    				$unit = "Years";
	    				$y = "selected";
	    				break;
	 		}
      		}
     		if($unit != "Ban") {
	 		echo "<td align=\"center\"><input type=\"text\" name=\"dur\" size=\"".strlen($dur)."\" maxlengh=\"25\" value=\"$dur\">\n";
	 		echo "<select name=\"unit\"><option value=\"1\" $s>Seconds</option>
			<option value=\"2\" $m>"._AM_MINUTES."</option>
			<option value=\"3\" $h>"._AM_HOURS."</option>
			<option value=\"4\" $d>"._AM_DAYS."</option>
			<option value=\"5\" $y>"._AM_YEARS."</option></select></td>";
      		} else {
	 		echo "<td align=\"center\">$dur $unit</td>";
      		}
      		echo "<td align=\"center\"><input type=\"hidden\" name=\"ban_id\" value=\"".$banlist['ban_id']."\">";
      		echo "<input type=\"submit\" name=\"edit\" value=\""._AM_EDIT."\"></td>";
      ?>
<input type="hidden" name="fct" value="userfilter"><input type="hidden" name="op" value="banuserEdit">
<?php
      		echo "<td align=\"center\"><br /><input type=\"button\" name=\"del\" value=\""._AM_DELETE."\" onclick=\"location='admin.php?fct=userfilter&op=banuserDelete&ban_id=".$banlist['ban_id']."'\"></form></td>";
      		echo "</tr>";
   	}
?>


<tr align="center">
	<td colspan="4"><h4><?php echo _AM_ADDABAN;?></h4><?php echo _AM_FORM2ADDBAN;?><br /><?php echo _AM_TOBANIPS;?></td>
</tr>
<tr align="center">
     <td><?php echo _AM_IPUNAME;?></td>
     <td><?php echo _AM_DURATION;?></td>
     <td colspan="2"><?php echo _AM_ADDABAN;?></td>
</tr>
<tr align="center">
	<td nowrap="nowrap"><form action="admin.php" method="post">
	    <?php echo _AM_IPADDRESS;?>&nbsp;<input type="text" name="ipuser" maxlength="50" size="25">
	</td>
        <td nowrap="nowrap"><input type="text" name="duration" maxlength="32" size="10">&nbsp;
            <select name="durtype"><option value="1"><?php echo _AM_SECONDS;?></option>
                                   <option value="2"><?php echo _AM_MINUTES;?></option>
                                   <option value="3"><?php echo _AM_HOURS;?></option>
                                   <option value="4"><?php echo _AM_DAYS;?></option>
                                   <option value="5"><?php echo _AM_YEARS;?></option></select>
        </td>
	<td colspan="2"><input type="hidden" name="banby" value="1"><input type="hidden" name="fct" value="userfilter"><input type="hidden" name="op" value="banuserAdd">
	    <br /><input type="submit" name="add" value="<?php echo _AM_ADDABAN;?>"></form>
	</td>
</tr>
<tr align="center">
	<td nowrap="nowrap"><form action="admin.php" method="post">
	    <?php echo _AM_USERNAME;?>&nbsp;
            <select name="ipuser">
<?php 
	$result = $xoopsDB->query("SELECT uid, uname from ".$xoopsDB->prefix("users")." WHERE level>0 ORDER BY uname");
	while($myrow = $xoopsDB->fetch_array($result)){
		echo "<option value=\"".$myrow['uid']."\">".$myrow['uname']."</option>\n";
	}
?>
	    </select>
	</td>
        <td nowrap="nowrap"><input type="text" name="duration" maxlength="32" size="10">&nbsp;
            <select name="durtype"><option value="1"><?php echo _AM_SECONDS;?></option>
                                   <option value="2"><?php echo _AM_MINUTES;?></option>
                                   <option value="3"><?php echo _AM_HOURS;?></option>
                                   <option value="4"><?php echo _AM_DAYS;?></option>
                                   <option value="5"><?php echo _AM_YEARS;?></option></select>
        </td>
	<td colspan="2"><input type="hidden" name="banby" value="2"><input type="hidden" name="fct" value="userfilter"><input type="hidden" name="op" value="banuserAdd">
	    <br /><input type="submit" name="add" value="<?php echo _AM_ADDABAN;?>"></form>
	</td>
</tr>
</table>
</table>

<?php
	CloseTable();
}


} else {
    echo "Access Denied";
}
?>