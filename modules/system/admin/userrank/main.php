<?php
if ( !eregi("admin.php", $PHP_SELF) ) {
	die("Access Denied");
}
include_once("admin/userrank/userrank.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
switch($op) {
	case "RankForumAdmin":
        	RankForumAdmin();
                break;
	case "RankForumEdit":
		RankForumEdit($rank_id);
                break;
        case "RankForumDel":
                RankForumDel($rank_id, $ok);
                break;
        case "RankForumAdd":
                RankForumAdd($rank_title,$rank_min,$rank_max,$rank_image,$rank_special);
                break;
	case "RankForumSave":
                RankForumSave($rank_id, $rank_title, $rank_min, $rank_max, $rank_image, $rank_special);
                break;
	default:
        	RankForumAdmin();
                break;
}
?>