<?php
if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
function RankForumAdmin() {
    	global $xoopsTheme, $xoopsDB, $xoopsConfig, $xoopsModule;
    	include ($xoopsConfig['root_path']."header.php");
    	$xoopsModule->printAdminMenu();
	echo "<br>";
    	OpenTable();
   	echo "<div align=\"center\"><h4>"._AM_RANKSSETTINGS."</h4>
	<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
	<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\">
	<tr bgcolor=\"".$xoopsTheme['bgcolor3']."\">
	<td align=\"center\"><b>"._AM_TITLE."</b></td>
	<td align=\"center\"><b>"._AM_MINPOST."</b></td>
	<td align=\"center\"><b>"._AM_MAXPOST."</b></td>
	<td align=\"center\"><b>"._AM_IMAGE."</b></td>
	<td align=\"center\"><b>"._AM_SPERANK."</b></td>
	<td align=\"center\"><b>"._AM_ACTION."</b></td></tr>";

    	$result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("ranks")." ORDER BY rank_id");
    	while($rank = $xoopsDB->fetch_array($result)) {
    		echo "<tr bgcolor=\"".$xoopsTheme['bgcolor1']."\">
		<td align=\"center\">".$rank['rank_title']."</td>
		<td align=\"center\">".$rank['rank_min']."</td>
		<td align=\"center\">".$rank['rank_max']."</td>
		<td align=\"center\">";
		if($rank['rank_image']){
			echo "<img src=\"".$xoopsConfig['xoops_url']."/images/ranks/".$rank['rank_image']."\"></td>";
		}else{
			echo "&nbsp;";
		}
		if ($rank['rank_special'] ==1) {
			echo"<td align=\"center\">"._AM_ON."</td>";
		} else {
			echo"<td align=\"center\">"._AM_OFF."</td>";
		}
		echo"<td align=\"center\"><a href=\"admin.php?fct=userrank&op=RankForumEdit&rank_id=".$rank['rank_id']."\">"._AM_EDIT."</a> | <a href=\"admin.php?fct=userrank&op=RankForumDel&rank_id=".$rank['rank_id']."&ok=0\">"._AM_DEL."</a></td></tr>";
    	}
    	echo "</table></td></tr></table>
    	<br /><br />
    	<h4>"._AM_ADDNEWRANK."</h4>
    	<form action=admin.php method=post>
	<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
    	<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\"><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>
    	"._AM_RANKTITLE."&nbsp;</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=text name=rank_title size=31 maxlength=50></td></tr><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>
    	"._AM_MINPOST."&nbsp;</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=text name=rank_min size=4 maxsize=5></td></tr><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>
    	"._AM_MAXPOST."&nbsp;</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=text name=rank_max size=4 maxsize=5></td></tr><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>
    	"._AM_IMAGE."&nbsp;</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=text name=rank_image size=50 maxlength=255><br />";
	$rankimgdir = $xoopsConfig['xoops_url']."/images/ranks/";
	printf(_AM_VALIDUNDER,$rankimgdir);
	echo "</td></tr>";
    	echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>"._AM_SPECIAL."&nbsp;</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=checkbox name=rank_special value=1><br />";
	echo _AM_SPECIALCAN."</td></tr>";
	echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>&nbsp;</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=\"submit\" value=\""._AM_ADD."\"></td></tr>";
    	echo "</table></td></tr></table>
    	<input type=\"hidden\" name=\"op\" value=\"RankForumAdd\">
    	<input type=\"hidden\" name=\"fct\" value=\"userrank\">
    	</form>
	</div>\n";
    	CloseTable();
    	include($xoopsConfig['root_path']."footer.php");
}

function RankForumAdd($rank_title,$rank_min,$rank_max,$rank_image,$rank_special) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$rank_title = $myts->makeTboxData4Save($rank_title);
	$rank_image = $myts->makeTboxData4Save($rank_image);
	$newid = $xoopsDB->GenID("ranks_rank_id_seq");
    	if ($rank_special == 1) {
    		$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("ranks")." (rank_id, rank_title, rank_min, rank_max, rank_special, rank_image) VALUES ($newid, '$rank_title', -1 ,-1 ,1,'$rank_image')");
    	} else {
    		$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("ranks")." (rank_id, rank_title, rank_min, rank_max, rank_special, rank_image) VALUES ($newid, '$rank_title', '$rank_min' ,'$rank_max' , 0, '$rank_image')");
    	}
    	redirect_header("admin.php?fct=userrank&op=RankForumAdmin",1,_AM_DBUPDATED);
}

function RankForumEdit($rank_id) {
    	global $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsModule;
	$myts = new MyTextSanitizer;
    	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br>";
   	$result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("ranks")." WHERE rank_id=".$rank_id."");
    	$rank = $xoopsDB->fetch_array($result);
	$rank['rank_title'] = $myts->makeTboxData4Edit($rank['rank_title']);
	$rank['rank_image'] = $myts->makeTboxData4Edit($rank['rank_image']);
    	OpenTable();
    	echo "<div align=\"center\">
    	<h4>"._AM_EDITRANK."</h4>
    	<form action=admin.php method=post>
	<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" align=\"center\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
    	<input type=\"hidden\" name=\"rank_id\" value=\"".$rank['rank_id']."\">
    	<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\"><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
    	<b>"._AM_RANKTITLE."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=text name=\"rank_title\" size=\"31\" value=\"".$rank['rank_title']."\" maxlength=\"50\"></td></tr>";
	echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>"._AM_IMAGE."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=\"text\" name=\"rank_image\" size=\"50\" maxlength=\"255\" value=\"".$rank['rank_image']."\"><br />";
	$rankimgdir = $xoopsConfig['xoops_url']."/images/ranks/";
	printf(_AM_VALIDUNDER,$rankimgdir);
	echo "</td></tr>";
    	echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>"._AM_MINPOST."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=text name=rank_min size=4 value=\"".$rank['rank_min']."\"></td></tr>
    	<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>"._AM_MAXPOST."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=text name=rank_max size=4 value=\"".$rank['rank_max']."\"></td></tr>
    	<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>"._AM_SPECIAL."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=\"checkbox\" name=\"rank_special\" value=\"1\"";
	if ($rank['rank_special'] == 1){
		echo " checked=\"checked\"";
	}
	echo "></td></tr>";
    	echo"
    	<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">&nbsp;</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\"><input type=submit value=\""._AM_SAVECHANGE."\"></td></tr></table></td></tr></table>
    	<input type=hidden name=op value=RankForumSave>
    	<input type=\"hidden\" name=\"fct\" value=\"userrank\">
    	</form></div>";
	CloseTable();
    	include($xoopsConfig['root_path']."footer.php");
}

function RankForumSave($rank_id, $rank_title, $rank_min, $rank_max, $rank_image, $rank_special) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$rank_title = $myts->makeTboxData4Save($rank_title);
	$rank_image = $myts->makeTboxData4Save($rank_image);
	if($rank_special != 1){
		$sql = "UPDATE ".$xoopsDB->prefix("ranks")." SET rank_title='$rank_title',rank_min='$rank_min',rank_max='$rank_max',rank_special=0,rank_image='$rank_image' WHERE rank_id=".$rank_id."";
	}else{
		$sql = "UPDATE ".$xoopsDB->prefix("ranks")." SET rank_title='$rank_title',rank_min=-1,rank_max=-1,rank_special=1,rank_image='$rank_image' WHERE rank_id=".$rank_id."";
	}
    	$xoopsDB->query($sql);
    	redirect_header("admin.php?fct=userrank&op=RankForumAdmin",1,_AM_DBUPDATED);
}

function RankForumDel($rank_id, $ok=0) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
    	if($ok==1) {
		$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("ranks")." WHERE rank_id=".$rank_id."");
		redirect_header("admin.php?fct=userrank&op=ForumAdmin",1,_AM_DBUPDATED);
		exit();
    	} else {
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br>";
		OpenTable();
		echo "<center><br>";
		echo "<font color=\"#ff0000\">";
		echo "<h4>"._AM_WAYSYWTDTR."</h4></font><br>";
		echo "[ <a href=admin.php?fct=userrank&op=RankForumDel&rank_id=".$rank_id."&ok=1>"._AM_YES."</a> | <a href=admin.php?fct=userrank&op=RankForumAdmin>"._AM_NO."</a> ]<br><br>";
		CloseTable();
    	}
	include($xoopsConfig['root_path']."footer.php");	
}

} else {
    	echo "Access Denied";
}

?>