<?php
if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
// check if the user is authorised
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {
	include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
	include($xoopsConfig['root_path']."class/xoopshtmlform.php");
	include($xoopsConfig['root_path']."class/xoopslists.php");
	function show_pref(){
		global $xoopsTheme, $xoopsConfig;
		$hf = new XoopsHtmlForm();
		$lists = new XoopsLists();
    		OpenTable();
   		echo "<div align=\"center\">\n";
		echo "<h4>"._MD_AM_SITEPREF."</h4>";
		echo "<form action=\"admin.php?fct=preferences\" method=\"post\">\n";
		echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
		<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\">
		<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_SITENAME."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">";
		echo $hf->input_text("sitename",$xoopsConfig['sitename'],50);
		echo "</td></tr>
		<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_SLOGAN."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("slogan",$xoopsConfig['slogan'],50);
		echo "</td></tr>
		<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_ADMINML."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("adminmail",$xoopsConfig['adminmail'],50);
		echo "</td></tr>
		<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_LANGUAGE."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		$langlist = $lists->getLangList();
		echo $hf->select("language",$langlist,$xoopsConfig['language']);
		echo "</td></tr>
		<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_STARTPAGE."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		$moduleslist = XoopsModule::getHasMainModulesList();
		$moduleslist["--"] = _MD_AM_NONE;
		echo $hf->select("startpage",$moduleslist,$xoopsConfig['startpage']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_SERVERTZ."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		$tzlist = $lists->getTimeZoneList();
		echo $hf->select("server_TZ",$tzlist,$xoopsConfig['server_TZ']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_DEFAULTTZ."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		$tzlist = $lists->getTimeZoneList();
		echo $hf->select("default_TZ",$tzlist,$xoopsConfig['default_TZ']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_DTHEME."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		$themeslist = $lists->getThemesList();
		echo $hf->select("default_theme",$themeslist,$xoopsConfig['default_theme']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_ANONNAME."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("anonymous",$xoopsConfig['anonymous']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_MINPASS."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("minpass",$xoopsConfig['minpass'],10);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_ANONPOST."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_radio_YN("anonpost",_MD_AM_YES,_MD_AM_NO,$xoopsConfig['anonpost']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_NEWUNOTIFY."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_radio_YN("new_user_notify",_MD_AM_YES,_MD_AM_NO,$xoopsConfig['new_user_notify']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_SELFDELETE."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_radio_YN("self_delete",_MD_AM_YES,_MD_AM_NO,$xoopsConfig['self_delete']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_LOADINGIMG."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_radio_YN("display_loading_img",_MD_AM_YES,_MD_AM_NO,$xoopsConfig['display_loading_img']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">".sprintf(_MD_AM_USEGZIP,phpversion())."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_radio_YN("gzip_compression",_MD_AM_YES,_MD_AM_NO,$xoopsConfig['gzip_compression']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_UNAMELVL."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		$array = array(0=>_MD_AM_STRICT,1=>_MD_AM_MEDIUM,2=>_MD_AM_LIGHT);
		echo $hf->select("uname_test_level",$array,$xoopsConfig['uname_test_level']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_USERCOOKIE."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("usercookie",$xoopsConfig['usercookie']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_SESSCOOKIE."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("sessioncookie",$xoopsConfig['sessioncookie']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_SESSEXPIRE."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("sessionexpire",$xoopsConfig['sessionexpire']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_BANNERS."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_radio_YN("banners",_MD_AM_YES,_MD_AM_NO,$xoopsConfig['banners']);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_ADMINGRAPHIC."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_radio_YN("admingraphic",_MD_AM_YES,_MD_AM_NO,$xoopsConfig['admingraphic']);
		echo "</td></tr>\n";
		if(!isset($xoopsConfig['my_ip']) || $xoopsConfig['my_ip'] == ""){
			$my_ip = getenv("REMOTE_ADDR");
		} else {
			$my_ip = $xoopsConfig['my_ip'];
		}
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_MYIP."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_text("my_ip",$my_ip);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">"._MD_AM_ALWDHTML."</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		$htmllist = $lists->getHtmlList();
		$tags = array();
		$tags = explode(">",str_replace("<","",$xoopsConfig['allowed_html']));
		echo $hf->select("tags",$htmllist,$tags,10,TRUE);
		echo "</td></tr>\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">\n";
		echo $hf->input_submit(_GO);
		echo $hf->input_hidden("op","save");
		echo "</td></tr></table></td></tr></table>\n";
		echo "</form></div>";
		CloseTable();
	}

	function save_pref($sitename, $slogan, $adminmail, $language, $startpage, $server_TZ, $default_TZ, $default_theme, $anonymous, $minpass, $anonpost, $new_user_notify, $self_delete, $display_loading_img, $gzip_compression, $uname_test_level, $usercookie, $sessioncookie, $sessionexpire, $banners, $tags, $my_ip, $admingraphic){
		global $xoopsConfig;
		$myts = new MyTextSanitizer();
		$error = "";
		if(!is_numeric($minpass)){
			$error .= "<h4>"._MD_AM_INVLDMINPASS."</h4>";
		}
		if(!is_numeric($sessionexpire)){
			$error .= "<h4>"._MD_AM_INVLDSEXP."</h4>";
		}
		if ((!$usercookie) || ($usercookie=="") || (ereg("[^a-zA-Z0-9_-]",$usercookie))){
		 	$error .= "<h4>"._MD_AM_INVLDUCOOK."</h4>";
		}
		if ((!$sessioncookie) || ($sessioncookie=="") || (ereg("[^a-zA-Z0-9_-]",$sessioncookie))){
		 	$error .= "<h4>"._MD_AM_INVLDSCOOK."</h4>";
		}
		if($error != ""){
			include($xoopsConfig['root_path']."header.php");
			echo $error;
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
		$allowed_html = "";
		if(count($tags>0)){
			foreach($tags as $tag){
				$allowed_html .= "<".$tag.">";
			}
		}
		$config = "<"."?php
/*********************************************************************
"._MD_AM_REMEMBER."
"._MD_AM_IFUCANT."
*********************************************************************/

// "._MD_AM_SITENAME."
\$xoopsConfig['sitename'] = \"".$myts->makeTboxData4PreviewInForm($sitename)."\";

// "._MD_AM_SLOGAN."
\$xoopsConfig['slogan'] = \"".$myts->makeTboxData4PreviewInForm($slogan)."\";

// "._MD_AM_ADMINML."
\$xoopsConfig['adminmail'] = \"".$myts->makeTboxData4PreviewInForm($adminmail)."\";

// "._MD_AM_LANGUAGE."
\$xoopsConfig['language'] = \"".$myts->makeTboxData4PreviewInForm($language)."\";

// "._MD_AM_STARTPAGE."
\$xoopsConfig['startpage'] = \"".$myts->makeTboxData4PreviewInForm($startpage)."\";

// "._MD_AM_DTHEME."
\$xoopsConfig['default_theme'] = \"".$myts->makeTboxData4PreviewInForm($default_theme)."\";

// "._MD_AM_ANONNAME."
\$xoopsConfig['anonymous'] = \"".$myts->makeTboxData4PreviewInForm($anonymous)."\";

// "._MD_AM_MINPASS."
\$xoopsConfig['minpass'] = ".$minpass.";

// "._MD_AM_ANONPOST." (1="._MD_AM_YES." 0="._MD_AM_NO.")
\$xoopsConfig['anonpost'] = ".$anonpost.";

// "._MD_AM_NEWUNOTIFY." (1="._MD_AM_YES." 0="._MD_AM_NO.")
\$xoopsConfig['new_user_notify'] = ".$new_user_notify.";

// "._MD_AM_SELFDELETE." (1="._MD_AM_YES." 0="._MD_AM_NO.")
\$xoopsConfig['self_delete'] = ".$self_delete.";

// "._MD_AM_LOADINGIMG." (1="._MD_AM_YES." 0="._MD_AM_NO.") 
\$xoopsConfig['display_loading_img'] = ".$display_loading_img.";

// ".sprintf(_MD_AM_USEGZIP,phpversion())." (1="._MD_AM_YES." 0="._MD_AM_NO.") 
\$xoopsConfig['gzip_compression'] = ".$gzip_compression.";

// "._MD_AM_UNAMELVL."  ("._MD_AM_STRICT."=0 "._MD_AM_MEDIUM."=1 "._MD_AM_LIGHT."=2)
\$xoopsConfig['uname_test_level'] = ".$uname_test_level.";

// "._MD_AM_USERCOOKIE."
\$xoopsConfig['usercookie'] = \"".$myts->makeTboxData4PreviewInForm($usercookie)."\";

// "._MD_AM_SESSCOOKIE."
\$xoopsConfig['sessioncookie'] = \"".$myts->makeTboxData4PreviewInForm($sessioncookie)."\";

// "._MD_AM_SESSEXPIRE."
\$xoopsConfig['sessionexpire'] = ".$sessionexpire.";

// "._MD_AM_SERVERTZ."
\$xoopsConfig['server_TZ'] = ".$server_TZ.";

// "._MD_AM_DEFAULTTZ."
\$xoopsConfig['default_TZ'] = ".$default_TZ.";

// "._MD_AM_BANNERS." (1="._MD_AM_YES." 0="._MD_AM_NO.")
\$xoopsConfig['banners'] = ".$banners.";

// "._MD_AM_ADMINGRAPHIC." (1="._MD_AM_YES." 0="._MD_AM_NO.")
\$xoopsConfig['admingraphic'] = ".$admingraphic.";

// "._MD_AM_MYIP." 
\$xoopsConfig['my_ip'] = \"".$my_ip."\";

// "._MD_AM_ALWDHTML."
\$xoopsConfig['allowed_html'] = \"".$allowed_html."\";

?".">";

		$file=fopen($xoopsConfig['root_path']."modules/system/cache/config.php","w");
		fwrite($file,$config);
		fclose($file);
		redirect_header("admin.php",2,_MD_AM_DBUPDATED);
		exit();
	}

} else {

}

?>