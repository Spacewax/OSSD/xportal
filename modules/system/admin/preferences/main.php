<?php
if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
include ("admin/preferences/preferences.php");
if( $xoopsUser->is_admin($xoopsModule->mid()) ) {
	if ( isset($op) ) {
		switch($op) {
		case "save":
			save_pref($sitename, $slogan, $adminmail, $language, $startpage, $server_TZ, $default_TZ, $default_theme, $anonymous, $minpass, $anonpost, $new_user_notify, $self_delete, $display_loading_img, $gzip_compression, $uname_test_level, $usercookie, $sessioncookie, $sessionexpire, $banners, $tags, $my_ip, $admingraphic);
			break;
		default:
			include($xoopsConfig['root_path']."header.php");
			$xoopsModule->printAdminMenu();
			echo "<br />";
			show_pref();
			echo "<br />";
			include($xoopsConfig['root_path']."footer.php");
			break;
		}
	} else {
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		show_pref();
		echo "<br />";
		include($xoopsConfig['root_path']."footer.php");
	}
}
?>