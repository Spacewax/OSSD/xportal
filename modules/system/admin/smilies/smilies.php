<?php
if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {

include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
function SmilesAdmin() {
	global $xoopsDB, $xoopsModule;
	global $xoopsTheme, $xoopsConfig;
	$url_smiles = $xoopsConfig['xoops_url']."/images/smilies";
	$myts = new MyTextSanitizer;
	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	echo "<div align='center'><h4>"._AM_CURRENTSMILE."</h4>";

	if ($getsmiles = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("smiles"))) {
		if (($numsmiles = $xoopsDB->num_rows($getsmiles)) == "0") {
			
		} else {
			echo "<table border='0' cellpadding='1' cellspacing='0' align='center' valign='top' width='70%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>\n";
    			echo "<table width='100%' border='0' cellpadding='2' cellspacing='1'>\n";
			echo "<tr bgcolor='".$xoopsTheme['bgcolor3']."'><td><b>" ._AM_CODE."</b></td>";
			echo "<td align='center'><b>" ._AM_SMILIE."</b></td>";
			echo "<td align='right'><b>"._AM_ACTION."</b></td>";
			echo "</tr>\n";
			while ($smiles = $xoopsDB->fetch_array($getsmiles)) {
				$smiles['code'] = $myts->makeTboxData4Show($smiles['code']);
				$smiles['smile_url'] = $myts->makeTboxData4Show($smiles['smile_url']);
				echo "<tr bgcolor='".$xoopsTheme["bgcolor1"]."'>";
				echo "<td>".$smiles['code']."</td>";
				echo "<td align='center'><img src='".$url_smiles."/".$smiles['smile_url']."'></td>";
				echo "<td align='right'><a href='admin.php?fct=smilies&op=SmilesEdit&id=".$smiles['id']."'>" ._AM_EDIT."</a>&nbsp;|&nbsp;";
				echo "<a href='admin.php?fct=smilies&op=SmilesDel&id=".$smiles['id']."'>" ._AM_DEL."</a></td>";
				echo "</tr>\n";

			}
			echo "</table></td></tr></table>\n";
		}
	} else {
		echo _AM_CNRFTSD;
	}
	echo "<br /><br /><h4>"._AM_ADDSMILE."</h4>\n";
	echo "<form action='admin.php' method='post'>\n";
	echo "<table border='0' cellpadding='1' cellspacing='0' align='center' valign='top' width='70%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>\n";
    	echo "<table width='100%' border='0' cellpadding='2' cellspacing='1'>\n";
	echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'><b>" ._AM_SMILECODE."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='code'></td></tr>\n";
	echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'><b>" ._AM_SMILEURL."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='smile_url'></td></tr>\n";
	echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'><b>" ._AM_SMILEEMOTION."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='emotion'></td></tr>\n";
	echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>&nbsp;</td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='hidden' name='fct' value='smilies'>";
	echo "<input type='hidden' name='op' value='SmilesAdd'>";
	echo "<input type='submit' value=" ._AM_ADD. "></td></tr>\n";
	echo "</table></td></tr></table></form>\n";
	echo "</div>";
	CloseTable();

	include($xoopsConfig['root_path']."footer.php");
}

function SmilesEdit($id) {
	global $xoopsDB, $xoopsModule;
	global $xoopsTheme, $xoopsConfig;
	$myts = new MyTextSanitizer;
	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	echo "<div align='center'>\n";
	if ($getsmiles = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("smiles")." WHERE id = $id")) {
		if (($numsmiles = $xoopsDB->num_rows($getsmiles)) == "0") {
		
		} else {

			if ($smiles = $xoopsDB->fetch_array($getsmiles)) {
				$smiles['code'] = $myts->makeTboxData4Edit($smiles['code']);
				$smiles['smile_url'] = $myts->makeTboxData4Edit($smiles['smile_url']);
				$smiles['emotion'] = $myts->makeTboxData4Edit($smiles['emotion']);
				echo "<form action=admin.php method=post>";
				echo "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='70%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>\n";
    				echo "<table width='100%' border='0' cellpadding='2' cellspacing='1'>\n";
				echo "<tr><td bgcolor='".$xoopsTheme["bgcolor3"]."'><b>" ._AM_SMILECODE."</b></td><td bgcolor='".$xoopsTheme["bgcolor1"]."'><input type='text' name='code' value='".$smiles['code']."'></td></tr>\n";
				echo "<tr><td bgcolor='".$xoopsTheme["bgcolor3"]."'><b>" ._AM_SMILEURL."</b></td><td bgcolor='".$xoopsTheme["bgcolor1"]."'><input type='text' name='smile_url' value='".$smiles['smile_url']."'></td></tr>\n";
				echo "<tr><td bgcolor='".$xoopsTheme["bgcolor3"]."'><b>" ._AM_SMILEEMOTION."</b></td><td bgcolor='".$xoopsTheme["bgcolor1"]."'><input type='text' name='emotion' value='".$smiles['emotion']."'></td></tr>";

				echo "<tr><td bgcolor='".$xoopsTheme["bgcolor3"]."'><input type='hidden' name='id' value='".$smiles['id']."'>";
				echo "<input type='hidden' name='op' value='SmilesSave'>";
				echo "<input type='hidden' name='fct' value='smilies'>";
				echo "</td><td bgcolor='".$xoopsTheme["bgcolor1"]."'><input type='submit' value='" ._AM_SAVE. "'></td></tr>";
				echo "</table></td></tr></table></form>";
			}
		}
	} else {
		echo _AM_CNRFTSD;
	}
	echo "</div>";
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}

function SmilesAdd($code,$smile_url,$emotion) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$code = $myts->makeTboxData4Save($code);
	$smile_url = $myts->makeTboxData4Save($smile_url);
	$emotion = $myts->makeTboxData4Save($emotion);
	$newid = $xoopsDB->GenID("smilies_id_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("smiles")." (id, code, smile_url, emotion) VALUES ($newid, '$code','$smile_url','$emotion')",1);

    	redirect_header("admin.php?fct=smilies&op=SmilesAdmin",2,_AM_DBUPDATED);
	exit();
}

function SmilesSave($id,$code,$smile_url,$emotion) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$code = $myts->makeTboxData4Save($code);
	$smile_url = $myts->makeTboxData4Save($smile_url);
	$emotion = $myts->makeTboxData4Save($emotion);

    	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("smiles")." SET code = '$code', smile_url = '$smile_url', emotion = '$emotion' WHERE id = $id",1);
	redirect_header("admin.php?fct=smilies&op=SmilesAdmin",2,_AM_DBUPDATED);
	exit();
}

function SmilesDel($id, $ok=0) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
    	if($ok==1) {
		$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("smiles")." where id=$id");
		redirect_header("admin.php?fct=smilies&op=SmilesAdmin",2,_AM_DBUPDATED);
		exit();
    	} else {
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		OpenTable();
		echo "<div align='center'><br />";
		echo "<font color='#ff0000'>";
		echo "<b>"._AM_WAYSYWTDTS."</b><br /><br /></font>";
		echo "[ <a href='admin.php?fct=smilies&op=SmilesDel&id=$id&ok=1'>"._AM_YES."</a> | <a href='admin.php?fct=smilies&op=SmileAdmin'>"._AM_NO."</a> ]<br /><br /></div>";
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
	}
}

} else {
    	echo "Access Denied";
}
?>