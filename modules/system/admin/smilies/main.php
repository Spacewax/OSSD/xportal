<?php
if (!eregi("admin.php", $PHP_SELF)) die("Access Denied"); 
include_once("admin/smilies/smilies.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

switch($op) {

	case "SmilesAdmin":
		SmilesAdmin();
		break;

	case "SmilesAdd":
		SmilesAdd($code,$smile_url,$emotion);
		break;

	case "SmilesEdit":
		SmilesEdit($id);
		break;
	
	case "SmilesSave":
		SmilesSave($id,$code,$smile_url,$emotion);
		break;

	case "SmilesDel":
		SmilesDel($id, $ok);
		break;

	default:
		SmilesAdmin();
		break;			
}

?>