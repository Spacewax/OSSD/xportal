<?php
if ( !eregi("admin.php", $PHP_SELF) ) {
	 redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
}
if( $xoopsUser->is_admin($xoopsModule->mid()) ) {

function list_modules(){
	global $xoopsConfig, $xoopsUser, $xoopsDB, $xoopsTheme, $xoopsModule;
        include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br>";
	OpenTable();
	echo "<div align=\"center\"><h4>"._MD_AM_MODADMIN."</h4>";
	echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">";
	echo "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\">\n";
	echo "<tr valign=\"center\" bgcolor=\"".$xoopsTheme['bgcolor3']."\" align=\"center\"><td><b>"._MD_AM_MODULE."</b></td><td><b>"._MD_AM_VERSION."</b></td><td><b>"._MD_AM_LASTUP."</b></td><td><b>"._MD_AM_ACTION."</b></td></tr>\n";
	$modules_dir = $xoopsConfig['root_path']."modules";
	$handle=opendir($modules_dir);
	while ($file = readdir($handle)) {
		if (!ereg('[.]',$file)) {
			$sql = "SELECT * FROM ".$xoopsDB->prefix("modules")." WHERE dirname='".$file."'";
			if ( $result = $xoopsDB->query($sql) ) {
				$num_rows = $xoopsDB->num_rows($result);
				echo "<tr valign=\"middle\" bgcolor=\"".$xoopsTheme['bgcolor1']."\">";
				if ( $num_rows>0 ) {
					$myrow = $xoopsDB->fetch_array($result);
					$module = new XoopsModule($myrow);
					echo "<td align=\"center\" valign=\"bottom\">";
					if ( $module->hasAdmin() ) {
						echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/".$module->dirname()."/".$module->adminpath()."\"><img src=\"".$xoopsConfig['xoops_url']."/modules/".$module->dirname()."/".$module->image()."\" alt=\"".$module->name()."\" border=\"0\"><br><b>" .trim($module->name())."</b></a>";
					} else {
						echo "<img src=\"".$xoopsConfig['xoops_url']."/modules/".$module->dirname()."/".$module->image()."\" alt=\"".$module->name()."\" border=\"0\"><br><b>" .trim($module->name())."</b>";
					}
					echo "</td><td align=\"center\">".$module->current_version()."</td><td align=\"center\">".formatTimestamp($module->last_update())."</td><td>";
					if($module->dirname() != "system"){
						echo "<a href=\"admin.php?fct=modulesadmin&amp;op=deactivate&amp;mid=".$module->mid()."\">"._MD_AM_DEACTIVATE."</a>&nbsp;<a href=\"admin.php?fct=modulesadmin&amp;op=update&amp;mid=".$module->mid()."\">"._MD_AM_UPDATE."</a>\n";
					} else {
						echo "<a href=\"admin.php?fct=modulesadmin&amp;op=update&amp;mid=".$module->mid()."\">"._MD_AM_UPDATE."</a>\n";
					}
					echo "&nbsp;<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/modules/system/admin.php?fct=version&amp;mid=".$module->mid()."','Info',300,230);\">"._INFO."</a></td>";
				}elseif($num_rows==0){
					$module = new XoopsModule();
					$module->loadModInfo($file);
					echo "<td align=\"center\" valign=\"bottom\">";
					echo "<img src=\"".$xoopsConfig['xoops_url']."/modules/".$module->dirname()."/".$module->image()."\" alt=\"".$module->name()."\" border=\"0\"><br><b>" .trim($module->name())."</b>";
					echo "</td><td align=\"center\">".$module->current_version()."</td><td align=\"center\">"._MD_AM_DEACTIVATED."</td>";
					echo "<td><a href=\"admin.php?fct=modulesadmin&amp;op=activate&amp;dirname=".$module->dirname()."\">"._MD_AM_ACTIVATE."</a>&nbsp;<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/modules/system/admin.php?fct=version&amp;mid=".$module->dirname()."','Info',300,230);\">"._INFO."</a></td>\n";
				}else{
					echo "<td>".$file."</td><td colspan=\"3\">"._MD_AM_DUPEN."</td>";
				}
				echo "</tr>\n";
			}
		}
		unset($module);
	}
	echo "</table></td></tr></table>\n";
	echo "</div>\n";
	CloseTable();
	echo "</p>";
	include($xoopsConfig['root_path']."footer.php");
}

function activate_module($dirname){
	global $xoopsConfig;
	$mymodule = new XoopsModule();
	$mymodule->loadModInfo($dirname);
	$mymodule->activate();
	//update the main menu cache
	$content = get_module_main_menu();
	write_module_main_menu($content);
	redirect_header($xoopsConfig['xoops_url']."/modules/system/admin.php?fct=modulesadmin",1,_MD_AM_ACTED);
	exit();
}

function deactivate_module($mid){
	global $xoopsConfig;
	$xoopsModule = new XoopsModule($mid);
	if ($xoopsModule->dirname()=="system") {
		redirect_header($xoopsConfig['xoops_url']."/modules/system/admin.php?fct=modulesadmin",3,_MD_AM_SYSNO);
		exit();
	} elseif ($xoopsModule->dirname() == $xoopsConfig['startpage']) {
		redirect_header($xoopsConfig['xoops_url']."/modules/system/admin.php?fct=modulesadmin",3,_MD_AM_STRTNO);
		exit();
	}
	$xoopsModule->deactivate();
	//update the main menu cache
	$content = get_module_main_menu();
	write_module_main_menu($content);
	redirect_header($xoopsConfig['xoops_url']."/modules/system/admin.php?fct=modulesadmin",1,_MD_AM_DEACTED);
	exit();
}

function update_module($mid){
	global $xoopsConfig;
	$xoopsModule = new XoopsModule($mid);
	$xoopsModule->update();
	//update the main menu cache
	$content = get_module_main_menu();
	write_module_main_menu($content);
	redirect_header($xoopsConfig['xoops_url']."/modules/system/admin.php?fct=modulesadmin",2,_MD_AM_UPDTED);
	exit();
}

function get_module_main_menu(){
	global $xoopsDB, $xoopsConfig;
	$sublinks = array();
	$content = "<?php\n\$mainmenu = \"";
	$content .= "&nbsp;<a href=\\\"".$xoopsConfig['xoops_url']."/\\\">\"._MB_SYSTEM_HOME.\"</a>";
	$sql = "SELECT * FROM ".$xoopsDB->prefix("modules")." WHERE hasmain=1 ORDER BY mid ASC";
	$result = $xoopsDB->query($sql);
	while($myrow = $xoopsDB->fetch_array($result)){
		if ( file_exists($xoopsConfig['root_path']."modules/".$myrow['dirname']."/xoops_version.php") ) {
			$module = new XoopsModule($myrow);
			$content .= "<br />&nbsp;".$module->mainLink()."\n";
			if($sublinks = $module->subLink()){
				foreach($sublinks as $sublink){
					$content .= "<br />&nbsp;&nbsp;&nbsp;".$sublink;
				}
			}
		} else {
			$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("modules")." WHERE dirname='".$myrow['dirname']."'");
		}
	}
	$content .= "\";\n?>";
	return $content;
}

function write_module_main_menu($content){
	global $xoopsConfig;
	$filename = $xoopsConfig['root_path']."modules/system/cache/mainmenu.txt";
	$file = fopen($filename, "w");
	fwrite($file, $content);
	fclose($file);
}

} else {
    redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
}

?>