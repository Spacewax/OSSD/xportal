<?php
if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {

/*********************************************************/
/* Users Functions                                       */
/*********************************************************/
include_once($xoopsConfig['root_path']."class/xoopslists.php");
include_once($xoopsConfig['root_path']."class/xoopshtmlform.php");

function displayUsers() {
	global $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsModule;
        include($xoopsConfig['root_path']."header.php");
	$hf = new XoopsHtmlForm();
	$lists = new XoopsLists();
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	echo "<h4>"._AM_EDEUSER."</h4>";
	echo "<form method=\"post\" action=\"admin.php\">";
	echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
    	<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\"><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">\n";
	echo "<b>"._AM_NICKNAME."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><select name=\"chng_uid\">";
	$result = $xoopsDB->query("SELECT uid, uname FROM ".$xoopsDB->prefix("users")." WHERE user_regdate>0 ORDER BY uname ASC");
	while($myrow = $xoopsDB->fetch_array($result)){
		if($myrow['uid'] !=0){
			echo "<option value=\"".$myrow['uid']."\">".$myrow['uname']."</option>\n";
		}
	}
	echo "</select>&nbsp;\n";
        echo "<select name=\"op\">\n";
        echo "<option value=\"modifyUser\">"._AM_MODIFYUSER."</option>\n";
        echo "<option value=\"delUser\">"._AM_DELUSER."</option></select>\n";
	echo "<input type=\"hidden\" name=\"fct\" value=\"users\">";
        echo "<input type=\"submit\" value=\""._AM_GO."\">";
	echo "</td></tr></table></td></tr></table></form><br />\n";
	echo "<h4>"._AM_ADDUSER."</h4>";
	echo _AM_INDICATECOF."<br />"; 
	echo "<form name=\"userinfo\" action=\"admin.php\" method=\"post\">";
	echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
    	<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\"><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">\n";
	echo "<b>"._AM_NICKNAME."</b>*</td>";?>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_uname" size="30" maxlength="25"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_NAME;?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_name" size="30" maxlength="50"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_EMAIL;?></b>*</td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_email" size="30" maxlength="60"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_OPTION; ?></b></td><td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="checkbox" name="add_user_viewemail" value="1"> <?php echo _AM_AOUTVTEAD; ?></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_AVATAR;?></b>*</td>
	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>">
	<?php
	$avatarlist = $lists->getAvatarsList();
	echo $hf->select("user_avatar",$avatarlist,"blank.gif",1,FALSE,"onChange=\"showimage()\"");?>
	&nbsp;&nbsp;<img src="<?php echo $xoopsConfig['xoops_url'];?>/images/avatar/blank.gif" name="avatar"> 
	[&nbsp;<a href="javascript:openWithSelfMain('<?php echo $xoopsConfig['xoops_url'];?>/misc.php?action=showpopups&amp;type=avatars','avatars',600,400);"><?php echo _AM_LIST;?></a>&nbsp;]
	</td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_URL;?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_url" size="30" maxlength="60"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_ICQ; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_user_icq" size="20" maxlength="20"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_AIM; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_user_aim" size="20" maxlength="20"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_YIM; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_user_yim" size="20" maxlength="20"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_MSNM; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_user_msnm" size="20" maxlength="20"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_LOCATION; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_user_from" size="25" maxlength="60"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_OCCUPATION; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_user_occ" size="25" maxlength="60"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_INTEREST; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_user_intrest" size="25" maxlength="255"></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><a href="admin.php?fct=userrank&op=RankForumAdmin"><?php echo _AM_RANK; ?></a></b></td>
	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><select name="add_rank">
<?php
	$sql = "SELECT rank_id, rank_title FROM ".$xoopsDB->prefix("ranks")." WHERE rank_special = 1";
	$r = $xoopsDB->query($sql);
	if($m = $xoopsDB->fetch_array($r)) {
		echo "<option value=\"0\">"._AM_NSRA."</option>";
		echo "<option value=\"0\">------------------------</option>";
		do {
			unset($selected);
			echo "<option value=\"".$m['rank_id']."\">".$m['rank_title']."</option>\n";
		} while($m = $xoopsDB->fetch_array($r));
		echo "</select>\n";
	} else {
		echo "<option value=\"0\">"._AM_NSRID."</option></select>\n";
	}
?>
	</td>
	</tr>

	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_SIGNATURE; ?></b></td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><textarea name="add_user_sig" rows="6" cols="45"></textarea></td></tr>
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_PASSWORD;?></b>*</td>
    	<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="add_pass" size="12" maxlength="12"></td></tr>
	<input type="hidden" name="op" value="addUser">
	<input type="hidden" name="fct" value="users">
	<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>">&nbsp;</td><td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="submit" value="<?php echo _AM_ADDUSER;?>"></td></tr></table>
	</td></tr></table>
	</form>
<?php 
	
	CloseTable();
        include($xoopsConfig['root_path']."footer.php");
}

function modifyUser($chng_user) {
	global $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsModule;
        include($xoopsConfig['root_path']."header.php");
	$hf = new XoopsHtmlForm();
	$lists = new XoopsLists();
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	$user = new XoopsUser($chng_user);
	if(!$user->is_active()){
		echo "<div align=\"center\">";
		echo "<h4>"._AM_NOTACTIVE."</h4>";
		echo "[ <a href=\"admin.php?fct=users&amp;op=reactivate&amp;uid=".$user->uid()."\">"._AM_YES."</a> | <a href=\"admin.php?fct=users\">"._AM_NO."</a> ]</div>";
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
        if($user) {
                echo "<h4>"._AM_UPDATEUSER.": ".$user->uname()."</h4>";
		echo "<b>"._AM_USERINFO."</b><br />\n";
		echo _AM_INDICATECOF."<br />";
		echo "<form name=\"userinfo\" action=\"admin.php\" method=\"get\">";
		echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
    		<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\"><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">\n";?>
		<b><?php echo _AM_USERID;?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><?php echo $user->uid(); ?></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_NICKNAME;?></b>*</td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_uname" value="<?php echo $user->uname("E"); ?>"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_NAME;?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_name" value="<?php echo $user->name("E"); ?>"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_URL;?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_url" value="<?php echo $user->url(); ?>" size="30" maxlength="60"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_EMAIL;?></b>*</td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_email" value="<?php echo $user->email(); ?>" size="30" maxlength="60"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_OPTION; ?></b></td><td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="checkbox" name="chng_user_viewemail" value="1" <?php if($user->user_viewemail()) echo "checked=\"checked\"";?>> <?php echo _AM_AOUTVTEAD; ?></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_AVATAR;?></b>*</td>
		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>">
		<?php
		$avatarlist = $lists->getAvatarsList();
		echo $hf->select("user_avatar",$avatarlist,$user->user_avatar(),1,FALSE,"onChange=\"showimage()\"");
		?>
		&nbsp;&nbsp;<img src="<?php $xoopsConfig['xoops_url'];?>/images/avatar/blank.gif" name="avatar" width="32" height="32">
		[&nbsp;<a href="javascript:openWithSelfMain('<?php echo $xoopsConfig['xoops_url'];?>/misc.php?action=showpopups&amp;type=avatars','avatars',600,400);"><?php echo _AM_LIST;?></a>&nbsp;]
		</td></tr>
		
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_ICQ; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_user_icq" value="<?php echo $user->user_icq(); ?>" size=20 maxlength=20></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_AIM; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_user_aim" value="<?php echo $user->user_aim(); ?>" size=20 maxlength=20></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_YIM; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_user_yim" value="<?php echo $user->user_yim(); ?>" size="20" maxlength="20"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_MSNM; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_user_msnm" value="<?php echo $user->user_msnm(); ?>" size="20" maxlength="20"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_LOCATION; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_user_from" value="<?php echo $user->user_from("E"); ?>" size="25" maxlength="60"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_OCCUPATION; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_user_occ" value="<?php echo $user->user_occ("E"); ?>" size="25" maxlength="60"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_INTEREST; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="text" name="chng_user_intrest" value="<?php echo $user->user_intrest("E"); ?>" size="25" maxlength="255"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><a href="admin.php?fct=userrank&op=RankForumAdmin"><?php echo _AM_RANK; ?></a></b></td>
		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><select name="rank">
<?php
		$sql = "SELECT rank_id, rank_title FROM ".$xoopsDB->prefix("ranks")." WHERE rank_special = 1";
		$r = $xoopsDB->query($sql);
		if($m = $xoopsDB->fetch_array($r)) {
			echo "<option value=\"0\">"._AM_NSRA."</option>";
			echo "<option value=\"0\">------------------------</option>";
			do {
				unset($selected);
				if($user->rank(false) == $m['rank_id'])
				$selected = "selected=\"selected\"";
				echo "<option value=\"".$m['rank_id']."\" $selected>".$m['rank_title']."</option>\n";
			} while($m = $xoopsDB->fetch_array($r));
			echo "</select>\n";
		} else {
			echo "<option value=\"0\">"._AM_NSRID."</option></select>\n";
		}
?>
		</td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_SIGNATURE; ?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><textarea name="chng_user_sig" rows=6 cols=45><?php echo $user->user_sig("E"); ?></textarea></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_PASSWORD;?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="password" name="chng_pass" size="12" maxlength="12"></td></tr>
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>"><b><?php echo _AM_RETYPEPD;?></b></td>
    		<td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="password" name="chng_pass2" size="12" maxlength="12"> <?php echo _AM_CHANGEONLY;?></td></tr>
		<input type="hidden" name="chng_uid" value="<?php echo $user->uid(); ?>">
		<input type="hidden" name="op" value="updateUser">
		<input type="hidden" name="fct" value="users">
		<tr><td bgcolor="<?php echo $xoopsTheme['bgcolor3'];?>">&nbsp;</td><td bgcolor="<?php echo $xoopsTheme['bgcolor1'];?>"><input type="submit" value="<?php echo _AM_UPDATEUSER;?>"></td></tr></table>
		</td></tr></table></form></div>
<?php
		echo "<br /><b>"._AM_USERPOST."</b><br /><br />\n";
		echo "<table border=\"0\">\n";
		//echo "<tr><td>"._AM_STORIES."</td><td>".$user->counter()."</td></tr>\n";
		echo "<tr><td>"._AM_COMMENTS."</td><td>".$user->posts()."</td></tr>\n";
		echo "</table>\n";
		echo "<br />"._AM_PTBBTSDIYT."<br />\n";
		echo "<form action=\"admin.php\" method=\"post\">\n";
		echo "<input type=\"hidden\" name=\"id\" value=\"".$user->uid()."\">";
		echo "<input type=\"hidden\" name=\"type\" value=\"user\">\n";
		echo "<input type=\"hidden\" name=\"fct\" value=\"users\">\n";
		echo "<input type=\"hidden\" name=\"op\" value=\"synchronize\">\n";
		echo "<input type=\"submit\" value=\""._AM_SYNCHRONIZE."\">\n";
		echo "</form>\n";
		CloseTable();
        } else {
		echo "<div align=\"center\">";
		echo _AM_USERDONEXIT;
		echo "</div>";
		CloseTable();
        }
        include($xoopsConfig['root_path']."footer.php");
}

function updateUser($chng_uid, $chng_uname, $chng_name, $chng_url, $chng_email, $chng_user_icq, $chng_user_aim, $chng_user_yim, $chng_user_msnm, $chng_user_from, $chng_user_occ, $chng_user_intrest, $chng_user_viewemail, $user_avatar, $chng_user_sig, $chng_pass, $chng_pass2, $rank) {

	global $xoopsConfig, $xoopsDB, $xoopsModule;
	$myts = new MyTextSanitizer;
        $tmp = 0;
	if ($chng_pass2 != "") {
                if($chng_pass != $chng_pass2) {
                        include($xoopsConfig['root_path']."header.php");
			$xoopsModule->printAdminMenu();
			echo "<br />";
                        echo "
			<center>"._AM_STNPDNM."</center>";
                        include($xoopsConfig['root_path']."footer.php");
                        exit;
                }
            	$tmp = 1;
        }
	if($chng_user_from != ""){
		$chng_user_from = $myts->makeTboxData4Save($chng_user_from);
	}
	if($chng_user_sig != ""){
		$chng_user_sig = $myts->makeTareaData4Save($chng_user_sig);
	}
	if($chng_user_occ != ""){
		$chng_user_occ = $myts->makeTboxData4Save($chng_user_occ);
	}
	if($chng_user_intrest != ""){
		$chng_user_intrest = $myts->makeTboxData4Save($chng_user_intrest);
	}
	if(!isset($chng_user_viewemail) || $chng_user_viewemail==""){
		$chng_user_viewemail = 0;
	}
	if ($tmp == 0) {
	    	$sql = "UPDATE ".$xoopsDB->prefix("users")." SET uname='$chng_uname', name='$chng_name', email='$chng_email', url='$chng_url', user_icq='$chng_user_icq', user_aim='$chng_user_aim', user_yim='$chng_user_yim', user_msnm='$chng_user_msnm', user_from='$chng_user_from', user_viewemail=$chng_user_viewemail, user_avatar='$user_avatar', user_sig='$chng_user_sig', rank=$rank WHERE uid=".$chng_uid."";
	    	$xoopsDB->query($sql,1);
	    	$sql = "UPDATE ".$xoopsDB->prefix("users_status")." SET user_occ='$chng_user_occ', user_intrest='$chng_user_intrest' where uid=".$chng_uid."";
	    	$xoopsDB->query($sql,1);
	}
	if ($tmp == 1) {

	    	$cpass = md5($chng_pass);
	    	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("users")." SET uname='$chng_uname', name='$chng_name', email='$chng_email', url='$chng_url', user_icq='$chng_user_icq', user_aim='$chng_user_aim', user_yim='$chng_user_yim', user_msnm='$chng_user_msnm', user_from='$chng_user_from', user_viewemail=$chng_user_viewemail, user_avatar='$user_avatar', user_sig='$chng_user_sig', pass='$cpass', rank=$rank WHERE uid=$chng_uid",1);
	    	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("users_status")." SET user_occ='$chng_user_occ', user_intrest='$chng_user_intrest' where uid=$chng_uid",1);
	}
	redirect_header("admin.php?fct=users",1,_AM_DBUPDATED);
	exit();
}


function synchronize($id, $type) {
	global $xoopsDB;
   	switch($type) {
   		case 'user':
			$tables = array("comments","pollcomments","bb_posts");
			$total_posts = 0;
			foreach($tables as $table){
   				$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix($table)." WHERE uid = $id";
   				if(!$result = $xoopsDB->query($sql,1)){
   					die(_AM_CNGTCOM);
   				}
   				if($row = $xoopsDB->fetch_array($result)){
					
   					$total_posts = $total_posts + $row['total'];
   				}
   			}
   		/*	$sql = "SELECT COUNT(sid) AS total FROM ".$xoopsDB->prefix("stories")." WHERE uid = $id and published>0";
   			if(!$result = $xoopsDB->query($sql,1)){
   				die(_AM_CNGTST);
   			}
   			if($row = $xoopsDB->fetch_array($result)){
   				$counter = $row['total'];
   			}
   			$sql = "UPDATE ".$xoopsDB->prefix("users")." SET posts = $total_posts, counter = $counter WHERE uid = $id";*/
			$sql = "UPDATE ".$xoopsDB->prefix("users")." SET posts = $total_posts WHERE uid = $id";
   			if(!$result = $xoopsDB->query($sql,1)){
   				die(sprintf(_AM_CNUUSER %s ,$id));
   			}
   			break;

   		case 'all users':
   			$sql = "SELECT uid FROM ".$xoopsDB->prefix("users")."";
   			if(!$result = $xoopsDB->query($sql)){
   				die(_AM_CNGUSERID);
   			}
   			while($row = $xoopsDB->fetch_array($result)){
   				$id = $row['uid'];
   				sync($id, "user");
   			}
   			break;
   	
   	}
   	redirect_header("admin.php?fct=users&op=modifyUser&chng_uid=".$id."",1,_AM_DBUPDATED);
	exit();
}

} else {
    	echo "Access Denied";
}

?>