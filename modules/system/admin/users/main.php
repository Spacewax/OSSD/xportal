<?php
if ( !eregi("admin.php", $PHP_SELF) ) {
	die("Access Denied");
}
include_once("admin/users/users.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {
switch($op) {
               	case "mod_users":
                        displayUsers();
                        break;
                case "modifyUser":
                        modifyUser($chng_uid);
                        break;
                case "updateUser":
                        updateUser($chng_uid, $chng_uname, $chng_name, $chng_url, $chng_email, $chng_user_icq, $chng_user_aim, $chng_user_yim, $chng_user_msnm, $chng_user_from, $chng_user_occ, $chng_user_intrest, $chng_user_viewemail, $user_avatar, $chng_user_sig, $chng_pass, $chng_pass2, $rank);
                        break;
                case "delUser":
                        include($xoopsConfig['root_path']."header.php");
			$userdata = new XoopsUser($chng_uid);
                        echo "<div align=\"center\"><h4>".sprintf(_AM_AYSYWTDU,$userdata->uname())."</h4>";
			echo _AM_BYTHIS."<br><br>";
                        echo "[ <a href=\"admin.php?fct=users&op=delUserConf&del_uid=".$userdata->uid()."\">"._AM_YES."</a> | <a href=\"admin.php?op=adminMain\">"._AM_NO."</a> ]</div>";
                        include($xoopsConfig['root_path']."footer.php");
                        break;
                case "delUserConf":
			$user = new XoopsUser($del_uid);
                        $user->delete();
                        redirect_header("admin.php?fct=users",1,_AM_DBUPDATED);
                        break;
                case "addUser":
			$add_pass = md5($add_pass);
                        if ( !($add_uname && $add_email && $add_pass) ) {
                        	echo _AM_YMCACF;
                            	return;
                        }
			$myts = new MyTextSanitizer;
			if ( $add_user_from != "" ) {
				$add_user_from = $myts->makeTboxData4Save($add_user_from);
			}
			if ( $add_user_sig != "" ) {
				$add_user_sig = $myts->makeTareaData4Save($add_user_sig);
			}
			if ( $add_user_occ != "" ) {
				$add_user_occ = $myts->makeTboxData4Save($add_user_occ);
			}
			if ( $add_user_intrest != "" ) {
				$add_user_intrest = $myts->makeTboxData4Save($add_user_intrest);
			}
			if ( !isset($add_user_viewemail) || $add_user_viewemail == "" ) {
				$add_user_viewemail = 0;
			}
			$user_table = $xoopsDB->prefix("users");
			$newid = $xoopsDB->GenID("users_uid_seq");
			$sql    = "INSERT INTO ".$user_table." ";
			$sql    .= "(uid,name,uname,email,url,user_avatar,user_regdate,user_icq,user_from,user_sig,user_viewemail,user_aim,user_yim,user_msnm,pass, rank) ";
			$sql    .= "VALUES ($newid,'$add_name','$add_uname','$add_email','$add_url','$user_avatar',".time().",'$add_user_icq','$add_user_from','$add_user_sig',$add_user_viewemail,'$add_user_aim','$add_user_yim','$add_user_msnm','$add_pass','$add_rank')";
			if ( !$result = $xoopsDB->query($sql,1) ) {
				echo _AM_CNRNU;
				exit();
			}
			if ( $newid == 0 ) {
				$newid = $xoopsDB->Insert_ID();
			}
			$result = $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("users_status")." (uid, umode, uorder, ublockon, ublock, user_occ, bio, user_intrest) VALUES ($newid, '',0,0,'','$add_user_occ','','$add_user_intrest')",1);
			if ( !$result ) {
				$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("users")." WHERE uid=".$new_uid."");
				echo _AM_CNRNU;
				exit();
			} else {
            			redirect_header("admin.php?fct=users",1,_AM_DBUPDATED);
			}
            		break;
		case "synchronize":
			synchronize($id, $type);
			break;
		case "reactivate":
			$result=$xoopsDB->query("UPDATE ".$xoopsDB->prefix("users")." SET level=1 WHERE uid=".$uid."",1);
			if(!$result){
				exit();
			}
                        redirect_header("admin.php?fct=users&op=modifyUser&chng_uid=".$uid."",1,_AM_DBUPDATED);
                        break;
    		default:
			displayUsers();
			break;
}
} else {
    	echo "Access Denied";
}
?>