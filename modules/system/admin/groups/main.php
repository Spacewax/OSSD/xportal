<?php
if ( !eregi("admin.php", $PHP_SELF) ) {
	 die ("Access Denied"); 
}
include_once("admin/groups/groups.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopshtmlform.php");
include_once($xoopsConfig['root_path']."class/xoopsgroup.php");
if( $xoopsUser->is_admin($xoopsModule->mid()) ) {
switch($op) {

        case "modifyAdmin":
                modifyAdmin($g_id);
                break;

        case "updateAdmin":
                updateAdmin($g_id, $g_name, $g_desc, $g_type, $admin_mids, $restricted_mids);
                break;

        case "addAdmin":
                if (!$add_name) {
			include($xoopsConfig['root_path']."header.php");
                        echo _AM_UNEED2ENTER;
			include($xoopsConfig['root_path']."footer.php");
                        exit();
                }
		$xoopsgroup = new XoopsGroup();
		$xoopsgroup->setName($add_name);
		$xoopsgroup->setDescription($add_desc);
		$xoopsgroup->setAdminRightModules($admin_mids);
		array_push($restricted_mids, 1);
		$modlist = XoopsModule::getAllModulesList($restricted_mids, FALSE);
		$xoopsgroup->setRestrictedModules($modlist);
		$xoopsgroup->store();
                redirect_header("admin.php?fct=groups&op=adminMain",1,_AM_DBUPDATED);
                break;

        case "delAdmin":
                include($xoopsConfig['root_path']."header.php");
                echo "<b> "._AM_DELETEADG."</b><br><br>";
                printf(_AM_AREUSUREDEL,$name);
                echo "[ <a href='admin.php?fct=groups&op=delAdminConf&g_id=$g_id'>"._AM_YES."</a>&nbsp;|&nbsp;<a href='admin.php?fct=groups&op=adminMain'>"._AM_NO."</a> ]";
                include($xoopsConfig['root_path']."footer.php");
                break;

        case "delAdminConf":
		$xoopsgroup = new XoopsGroup($g_id);
		$xoopsgroup->delete();
                redirect_header("admin.php?fct=groups&op=adminMain",1,_AM_DBUPDATED);
                break;


	case "addUser":
		$size = sizeof($uids);
		for ( $i = 0; $i < $size; $i++ ) {
			$sql = "INSERT INTO ".$xoopsDB->prefix("groups_users_link")." (groupid, uid) VALUES (".$groupid.",".$uids[$i].")";
			$xoopsDB->query($sql,1);	
		}
		echo $sql;
		redirect_header("admin.php?fct=groups&op=modifyAdmin&g_id=".$groupid."",0,_AM_DBUPDATED);
		break;

	case "delUser":
		$size = sizeof($uids);
		for ( $i = 0; $i < $size; $i++ ) {
			$sql = "DELETE FROM ".$xoopsDB->prefix("groups_users_link")." WHERE uid=".$uids[$i]." AND groupid=".$groupid."";
			$xoopsDB->query($sql);	
		}
		redirect_header("admin.php?fct=groups&op=modifyAdmin&g_id=".$groupid."",0,_AM_DBUPDATED);
		break;
    	default:
		displayAdmins();
		break;
}

} else {
    	echo "Access Denied";
}
?>