<?php
if (!eregi("admin.php", $PHP_SELF)) { 
	die ("Access Denied"); 
}
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {

/*********************************************************/
/* Admin/Authors Functions                               */
/*********************************************************/

function displayAdmins() {
        global $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsModule;
        include($xoopsConfig['root_path']."header.php");
	$hf = new XoopsHtmlForm();
	$xoopsModule->printAdminMenu();
	echo "<br />";
        OpenTable();
        echo "<div align='center'>
        <h4>"._AM_EDITADG."</h4>
        ";
        $result = $xoopsDB->query("SELECT groupid, name, type FROM ".$xoopsDB->prefix("groups")."");
        echo "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='40%'>
<tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>
<table width='100%' border='0' cellpadding='2' cellspacing='1'>
";
	$myts = new MyTextSanitizer;

        while(list($a_groupid, $name, $type) = $xoopsDB->fetch_row($result)) {
		$name = $myts->makeTboxData4Save($name);
                echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>$name</td>";
                echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><a href='admin.php?fct=groups&op=modifyAdmin&g_id=$a_groupid'>"._AM_MODIFY."</a>";
                if ( $a_groupid == 1 || $type == "Anonymous" || $type == "User" ) {
			echo "</td></tr>";
                } else {
                    	echo "&nbsp;<a href='admin.php?fct=groups&amp;op=delAdmin&amp;g_id=$a_groupid&amp;name=$name'>"._AM_DELETE."</a></td></tr>";
                }
        }
	echo "</table></td></tr></table>";
	echo "<br /><br /><h4>"._AM_CREATENEWADG."</h4>";
	echo _AM_INDICATES."<br />";
	echo "<form action='admin.php' method='post'>"; ?>
	<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'>
	<tr><td bgcolor='<?php echo $xoopsTheme["bgcolor2"];?>'>
	<table width='100%' border='0' cellpadding='2' cellspacing='1'>
	<tr><td bgcolor='<?php echo $xoopsTheme['bgcolor3'];?>'><?php echo _AM_NAME;?>*</td>
    	<td bgcolor='<?php echo $xoopsTheme['bgcolor1'];?>'><input type='text' name='add_name' size='30' maxlength='50' /></td></tr>
<tr><td bgcolor='<?php echo $xoopsTheme['bgcolor3'];?>'><?php echo _AM_DESCRIPTION;?></td>
    	<td bgcolor='<?php echo $xoopsTheme['bgcolor1'];?>'><textarea name='add_desc' cols='50' rows='3'></textarea></td></tr>
	<tr><td bgcolor='<?php echo $xoopsTheme['bgcolor3'];?>'><?php echo _AM_ACTIVERIGHTS;?></td><td bgcolor='<?php echo $xoopsTheme['bgcolor1'];?>'>
	<?php $modlist = XoopsModule::getHasAdminModulesList();
		echo $hf->input_check_radio("admin_mids",$modlist,"",TRUE);
	?>
	<br /><b><?php echo _AM_IFADMIN;?></b></td></tr>
	<tr><td bgcolor='<?php echo $xoopsTheme['bgcolor3'];?>'><?php echo _AM_ACCESSRIGHTS;?></td><td bgcolor='<?php echo $xoopsTheme['bgcolor1'];?>'>
	<?php 
		$exclude = array(1);
		$modlist = XoopsModule::getAllModulesList($exclude);
		echo $hf->input_check_radio("restricted_mids",$modlist,"",TRUE);
	?>
	</td></tr>
	<tr><td bgcolor='<?php echo $xoopsTheme['bgcolor3'];?>'>&nbsp;</td><td bgcolor='<?php echo $xoopsTheme['bgcolor1'];?>'><input type='hidden' name='op' value='addAdmin' />
	<input type='hidden' name='fct' value='groups' />
	<input type='submit' value='<?echo _AM_CREATENEWADG;?>' />
	</td></tr></table>
	</td></tr></table>
	</form>
	</div>
<?php 
	CloseTable();
        include($xoopsConfig['root_path']."footer.php");
}


function modifyAdmin($g_id) {
        global $xoopsUser, $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsModule;
        include($xoopsConfig['root_path']."header.php");
	$hf = new XoopsHtmlForm();
	$xoopsModule->printAdminMenu();
	echo "<br />";
        OpenTable();
	echo "<div align='center'>";
	$thisgroup = new XoopsGroup($g_id);
	echo "<h4>"._AM_MODIFYADG."</h4>";
	echo _AM_INDICATES."<br />";
	echo "<form action='admin.php' method='post'>";
        echo "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'>
	<tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>
	<table width='100%' border='0' cellpadding='2' cellspacing='1'>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>" . _AM_NAME . "*</td>";
        echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='g_name' value='".$thisgroup->name("E")."' /></td></tr>";
        echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>" . _AM_DESCRIPTION . "</td>";
        echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><textarea name='g_desc' cols='50' rows='3'>".$thisgroup->description("E")."</textarea></td></tr>";
	$admin_mids = XoopsGroup::getAdminRightModules($thisgroup->groupid());
	$modlist = XoopsModule::getHasAdminModulesList();
	echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>" . _AM_ACTIVERIGHTS . "</td><td bgcolor='".$xoopsTheme['bgcolor1']."'>";
	echo $hf->input_check_radio("admin_mids",$modlist,$admin_mids,TRUE);
	echo "<br /><b>"._AM_IFADMIN."</b></td></tr>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>"._AM_ACCESSRIGHTS."</td><td bgcolor='".$xoopsTheme['bgcolor1']."'>";
	$restricted_mids = XoopsGroup::getRestrictedModules($thisgroup->groupid());
	$exclude = array(1);
	$modlist = XoopsModule::getAllModulesList($exclude);
	echo $hf->input_check_radio("restricted_mids",$modlist,$restricted_mids,TRUE,5,"",FALSE,TRUE);

	echo "</td></tr>
        <input type='hidden' name='op' value='updateAdmin' />
	<input type='hidden' name='fct' value='groups' />
	<input type='hidden' name='g_type' value='".$thisgroup->type("E")."' />
        <input type='hidden' name='g_id' value='".$thisgroup->groupid()."' />
        <tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>&nbsp;</td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='submit' value='" . _AM_UPDATEADG . "' /></form></td></tr>
        </table></td></tr></table>";
	if ( $thisgroup->type() != "Anonymous" ) {
		echo "<br /><h4>"._AM_EDITMEMBER."</h4>";
		$sql = "SELECT l.uid, u.uname FROM ".$xoopsDB->prefix("groups_users_link")." l, ".$xoopsDB->prefix("users")." u WHERE l.groupid = ".$g_id." AND l.uid=u.uid ORDER BY uname";
		$result = $xoopsDB->query($sql);
		$admins= array();
		while($myrow = $xoopsDB->fetch_array($result)){
			array_push($admins, $myrow);
		}
		$sql = "SELECT uid, uname FROM ".$xoopsDB->prefix("users")." WHERE level>0 ORDER BY uname";
		$result = $xoopsDB->query($sql);
		$users = array();
		while($myrow = $xoopsDB->fetch_array($result)){
			array_push($users, $myrow);
		}
		echo "<table border='0'>\n";
		echo "<tr><td align='center'>"._AM_NONMEMBERS."</td><td></td><td align='center'>"._AM_MEMBERS."</td></tr>";
		echo "<tr><td>\n";
		echo "<form action='admin.php' method='post'>";
		echo "<select name='uids[]' size='10' multiple='multiple'>\n";
		$usersize = sizeof($users);
		$adminsize = sizeof($admins);
		for ( $i = 0; $i < $usersize; $i++ ) {
			$isadmin = 0;
			for ( $j = 0; $j < $adminsize; $j++ ) {
				if ( $users[$i]['uid'] == $admins[$j]['uid'] ) {
					$isadmin = 1;
					continue;
				}
			}
			if ( !$isadmin ) {
				echo "<option value='".$users[$i]['uid']."'>".$users[$i]['uname']."</option>\n";
			}
		}
		echo "</select>\n";
		echo "</td>";
		echo "<td align='center'>";
		echo "<input type='hidden' name='op' value='addUser' />";
		echo "<input type='hidden' name='fct' value='groups' />";
		echo "<input type='hidden' name='groupid' value='".$thisgroup->groupid()."' />";
		echo "<input type='submit' name='submit' value='"._AM_ADDBUTTON."' />";
		echo "</form><br />";
		echo "<form action='admin.php' method='post' />";
		echo "<input type='hidden' name='op' value='delUser' />";
		echo "<input type='hidden' name='fct' value='groups' />";
		echo "<input type='hidden' name='groupid' value='".$thisgroup->groupid()."' />";
		echo "<input type='submit' name='submit' value='"._AM_DELBUTTON."' />";
		echo "</td>";
		echo "<td>";
		echo "<select name='uids[]' size='10' multiple='multiple'>";
		for ( $i = 0; $i < $adminsize; $i++ ) {
			echo "<option value='".$admins[$i]['uid']."'>".$admins[$i]['uname']."</option>";
		}
		echo "</select>";
		echo "</td></tr>";
		echo "</form>";
		echo "</table>";
	}
	echo "</div>";	
        CloseTable();
        include($xoopsConfig['root_path']."footer.php");
}

function updateAdmin($g_id, $g_name, $g_desc, $g_type, $admin_mids, $restricted_mids) {
	global $xoopsDB;
	if (!$g_id || !$g_name) { 
		redirect_header("admin.php?fct=groups&op=adminMain",2,_AM_UNEED2ENTER); 
		exit();
	}
	$xoopsgroup = new XoopsGroup($g_id);
	$xoopsgroup->setName($g_name);
	$xoopsgroup->setDescription($g_desc);
	$xoopsgroup->setAdminRightModules($admin_mids);
	array_push($restricted_mids, 1);
	$modlist = XoopsModule::getAllModulesList($restricted_mids, FALSE);
	$xoopsgroup->setRestrictedModules($modlist);
	$xoopsgroup->setType($g_type);
	$xoopsgroup->store();
        redirect_header("admin.php?fct=groups&op=adminMain",1,_AM_DBUPDATED);
	exit;
}

} else {
    echo "Access Denied";
}

?>