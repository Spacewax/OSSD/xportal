<?php
if ( !eregi("admin.php", $PHP_SELF) ) {
	die ("Access Denied"); 
}
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {

function saveMetaFooter($metacode,$footer){
	global $xoopsDB, $xoopsConfig;
	$myts = new MyTextSanitizer; // MyTextSanitizer object
	$metacode = $myts->makeTareaData4Save($metacode);
	$footer = $myts->makeTareaData4Save($footer);
	$sql = "DELETE FROM ".$xoopsDB->prefix("metafooter")." WHERE (1=1)";
	$result = $xoopsDB->query($sql,1);
	if (!$result) {
		include($xoopsConfig['root_path']."header.php");
		echo _AM_ERRDELOLD;
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
	$sql = "INSERT INTO ".$xoopsDB->prefix("metafooter")." (meta, footer) VALUES ('$metacode', '$footer')";
	$result = $xoopsDB->query($sql,1);
	if(!$result) {
		include($xoopsConfig['root_path']."header.php");
		echo _AM_ERRINSERT;
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
	redirect_header("admin.php",1,_AM_DBUPDATED);
}

function editMetaFooter(){
	global $xoopsDB, $xoopsTheme, $xoopsConfig;
	$sql = "SELECT * FROM ".$xoopsDB->prefix("metafooter")." WHERE (1=1)";
	$result = $xoopsDB->query($sql,1);
	if (!$result) {
		echo "Error doing DB query";
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		exit();
	}
	$row = $xoopsDB->fetch_array($result);
	$myts = new MyTextSanitizer; // MyTextSanitizer object
	$currMeta = $myts->makeTareaData4Edit($row[meta]);
	$currFooter = $myts->makeTareaData4Edit($row[footer]);
	echo "<div align=\"center\">";
	echo "<h4>"._AM_EDITMKF."</h4>";
?>
<form action="admin.php" method="post">
<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="95%"><tr><td bgcolor="<?php echo $xoopsTheme["bgcolor2"];?>">
<table border="0" cellpadding="1" cellspacing="1" width="100%">

<tr align="left">
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" nowrap><b><?php echo _AM_METAKEY;?></b></td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>"><textarea name="metacode" rows="15" cols="45" wrap="virtual"><?php echo $currMeta?></textarea><br><?php echo _AM_TYPEBYCOM;?><br><br></td>
</tr>
<tr align="left">
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>" nowrap><b><?php echo _AM_FOOTER;?></b></td>
	<td bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>"><textarea name="footer" rows="15" cols="45" wrap="virtual"><?php echo $currFooter?></textarea>
	<br><br><font color="#ff0000"><?php echo _AM_BESURELINK;?></font><br><br></td>
</tr>
<tr Align="left">
	<td bgcolor="<?php echo $xoopsTheme["bgcolor3"];?>">&nbsp</td>
	<td align="center" bgcolor="<?php echo $xoopsTheme["bgcolor1"];?>">
		<input type="hidden" name="fct" value="metafooter">
		<input type="hidden" name="op" value="save">
		<input type="submit" name="submit" value="<?php echo _AM_ADDCODE;?>">
	</td>
</tr>
</table></td></tr></table>
</form></div>
<?php
}

} else {
    echo "Access Denied";
}

?>