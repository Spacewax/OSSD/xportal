<?php
if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
include_once("admin/metafooter/metafooter.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

switch($op) {
	case "save":
		saveMetaFooter($metacode,$footer);
		break;
	default:
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		OpenTable();
		editMetaFooter();
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
		break;
}
?>