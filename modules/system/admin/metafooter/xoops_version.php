<?php
$modversion['name'] = _MD_AM_MTFT;
$modversion['version'] = "";
$modversion['description'] = "Meta Tags and Footer Configuration";
$modversion['author'] = "";
$modversion['credits'] = "The MPN SE Project";
$modversion['help'] = "metafooter.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "foot.gif";

$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin.php?fct=metafooter";

?>