<?php

if ( !eregi("admin.php", $PHP_SELF) ) { die ("Access Denied"); }
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {

/*********************************************************/
/* Banners Administration Functions                      */
/*********************************************************/

function BannersAdmin() {
    	global $xoopsDB, $xoopsConfig, $xoopsModule;
	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
// Banners List
	echo "<a name='top'>";
	OpenTable();
	echo "<div style='text-align:center'><b>"._AM_CURACTBNR."</b></div><br />
	<table width='100%' border='0'><tr>
	<td>"._AM_BANNERID."</td>
	<td>"._AM_IMPRESION."</td>
	<td>"._AM_IMPLEFT."</td>
	<td>"._AM_CLICKS."</td>
	<td>"._AM_NCLICKS."</td>
	<td>"._AM_CLINAME."</td>
	<td>"._AM_FUNCTION."</td></tr><tr align='center'>";
	$result = $xoopsDB->query("SELECT bid, cid, imptotal, impmade, clicks, date FROM ".$xoopsDB->prefix("banner")." ORDER BY bid");
	$myts = new MyTextSanitizer;
	while(list($bid, $cid, $imptotal, $impmade, $clicks, $date) = $xoopsDB->fetch_row($result)) {
		$result2 = $xoopsDB->query("SELECT cid, name FROM ".$xoopsDB->prefix("bannerclient")." WHERE cid=$cid");
	    	list($cid, $name) = $xoopsDB->fetch_row($result2);
		
		$name = $myts->makeTboxData4Show($name);
	    	if ( $impmade == 0 ) {
			$percent = 0;
	    	} else {
			$percent = substr(100 * $clicks / $impmade, 0, 5);
	    	}
	    
	    	if ( $imptotal == 0 ) {
			$left = ""._AM_UNLIMIT."";
	    	} else {
			$left = $imptotal-$impmade;
	    	}
	    	echo "
	    	<td>$bid</td>
	    	<td>$impmade</td>
	    	<td>$left</td>
	    	<td>$clicks</td>
	    	<td>$percent%</td>
	    	<td>$name</td>
	    	<td><a href='admin.php?fct=banners&amp;op=BannerEdit&amp;bid=$bid'>"._AM_EDIT."</a> | <a href='admin.php?fct=banners&amp;op=BannerDelete&amp;bid=$bid&amp;ok=0'>"._AM_DELETE."</a></td><tr>
	    	";
	}
	echo "</td></tr></table>";
	CloseTable();
	echo "<br />";
// Finished Banners List
	echo "<a name='top'>";
	OpenTable();
	echo "<div style='text-align:center'><b>"._AM_FINISHBNR."</b></div><br />
	<table width='100%' border='0'><tr>
	<td>"._AM_BANNERID."</td>
	<td>"._AM_IMPD."</td>
	<td>"._AM_CLICKS."</td>
	<td>"._AM_NCLICKS."</td>
	<td>"._AM_STARTDATE."</td>
	<td>"._AM_ENDDATE."</td>
	<td>"._AM_CLINAME."</td>
	<td>"._AM_FUNCTION."</td></tr><tr align='center'>";
	$result = $xoopsDB->query("SELECT bid, cid, impressions, clicks, datestart, dateend FROM ".$xoopsDB->prefix("bannerfinish")." ORDER BY bid");
	
	while(list($bid, $cid, $impressions, $clicks, $datestart, $dateend) = $xoopsDB->fetch_row($result)) {
		$result2 = $xoopsDB->query("SELECT cid, name FROM ".$xoopsDB->prefix("bannerclient")." WHERE cid=$cid");
	    	list($cid, $name) = $xoopsDB->fetch_row($result2);
		$name = $myts->makeTboxData4Show($name);
	    	$percent = substr(100 * $clicks / $impressions, 0, 5);
	    	echo "
	    	<td>$bid</td>
	    	<td>$impressions</td>
	    	<td>$clicks</td>
	    	<td>$percent%</td>
	    	<td>".formatTimestamp($datestart,"m")."</td>
	    	<td>".formatTimestamp($dateend,"m")."</td>
	    	<td>$name</td>
	    	<td><a href='admin.php?fct=banners&op=BannerFinishDelete&bid=$bid'>"._AM_DELETE."</a></td><tr>
	    	";
	}
	echo "</td></tr></table>";
	CloseTable();
	echo "<br />";

// Clients List
	OpenTable();
	echo "
	<div style='text-align:center'><b>"._AM_ADVCLI."</b></div><br />
	<table width='100%' border='0'><tr align='center'>
	<td>"._AM_BANNERID."</td>
	<td>"._AM_CLINAME."</td>
	<td>"._AM_ACTIVEBNR."</td>
	<td>"._AM_CONTNAME."</td>
	<td>"._AM_CONTMAIL."</td>
	<td>"._AM_FUNCTION."</td></tr><tr align='center'>";
	$result = $xoopsDB->query("SELECT cid, name, contact, email FROM ".$xoopsDB->prefix("bannerclient")." ORDER BY cid");
	
	while(list($cid, $name, $contact, $email) = $xoopsDB->fetch_row($result)) {
		$name = $myts->makeTboxData4Show($name);
		$contact = $myts->makeTboxData4Show($contact);
	    	$result2 = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("banner")." WHERE cid=$cid");
	    	list($numrows) = $xoopsDB->fetch_row($result2);
	    	echo "
	    	<td>$cid</td>
	    	<td>$name</td>
	    	<td>$numrows</td>
	    	<td>$contact</td>
	    	<td>$email</td>
	    	<td><a href='admin.php?fct=banners&op=BannerClientEdit&cid=$cid'>"._AM_EDIT."</a> | <a href='admin.php?fct=banners&op=BannerClientDelete&cid=$cid'>"._AM_DELETE."</a></td><tr>
	    	";
		}
	echo "</td></tr></table>";
	CloseTable();
	echo "<br />";
// Add Banner
    	$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("bannerclient"));
    	list($numrows) = $xoopsDB->fetch_row($result);
    	if ( $numrows > 0 ) {
		OpenTable();
		echo"
		<h4>"._AM_ADDNWBNR."</h4>
		<form action='admin.php' method='post'>
		"._AM_CLINAMET."
		<select name='cid'>";
		$result = $xoopsDB->query("SELECT cid, name FROM ".$xoopsDB->prefix("bannerclient"));
		while(list($cid, $name) = $xoopsDB->fetch_row($result)) {
			$name = $myts->makeTboxData4Show($name);
	    		echo "<option value='$cid'>$name</option>";
			
		}
		echo "
		</select><br />
		"._AM_IMPPURCHT."<input type='text' name='imptotal' size='12' maxlength='11' /> 0 = "._AM_UNLIMIT."<br />
		"._AM_IMGURLT."<input type='text' name='imageurl' size='50' maxlength='255' /><br />
		"._AM_CLICKURLT."<input type='text' name='clickurl' size='50' maxlength='255' /><br />
		<input type='hidden' name='fct' value='banners' />
		<input type='hidden' name='op' value='BannersAdd' />
		<input type='submit' value='"._AM_ADDBNR."' />
		</form>";
		CloseTable();
    	}
// Add Client
	echo "<br />";
	OpenTable();
	echo"
	<h4>"._AM_ADDNWCLI."</h4>
	<form action='admin.php' method='post'>
	"._AM_CLINAMET."<input type='text' name='name' size='30' maxlength='60' /><br />
	"._AM_CONTNAMET."<input type='text' name='contact' size='30' maxlength='60' /><br />
	"._AM_CONTMAILT."<input type='text' name='email' size='30' maxlength='60' /><br />
	"._AM_CLILOGINT."<input type='text' name='login' size='12' maxlength='10' /><br />
	"._AM_CLIPASST."<input type='text' name='passwd' size='12' maxlength='10' /><br />
	"._AM_EXTINFO."<br /><textarea name='extrainfo' cols='60' rows='10' /></textarea><br />
	<input type='hidden' name='op' value='BannerAddClient' />
	<input type='hidden' name='fct' value='banners' />
	<input type='submit' value='"._AM_ADDCLI."' />
	</form>";
	CloseTable();
	include ($xoopsConfig['root_path']."footer.php");
}

function BannersAdd($name, $cid, $imageurl, $clickurl, $imptotal=0) {
	global $xoopsDB;
	$nowtime = time();
	$newid = $xoopsDB->GenID("banner_bid_seq");
	if ( !isset($imptotal) || $imptotal == "" ) {
		$imptotal = 0;
	}
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("banner")." (bid, cid, imptotal, impmade, clicks, imageurl, clickurl, date) VALUES ($newid, $cid, $imptotal, 1, 0, '$imageurl', '$clickurl', $nowtime)",1);
    	redirect_header("admin.php?fct=banners&op=BannersAdmin#top",1,_AM_DBUPDATED);
	exit();
}

function BannerAddClient($name, $contact, $email, $login, $passwd, $extrainfo) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$name = $myts->makeTboxData4Save($name);
	$contact = $myts->makeTboxData4Save($contact);
	$extrainfo = $myts->makeTareaData4Save($extrainfo);
	$newid = $xoopsDB->GenID("bannerclient_cid_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("bannerclient")." (cid, name, contact, email, login, passwd, extrainfo) VALUES ($newid, '$name', '$contact', '$email', '$login', '$passwd', '$extrainfo')");
    	redirect_header("admin.php?fct=banners&op=BannersAdmin#top",1,_AM_DBUPDATED);
	exit();
}

function BannerFinishDelete($bid) {
	global $xoopsDB;
    	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("bannerfinish")." WHERE bid=$bid");
    	redirect_header("admin.php?fct=banners&op=BannersAdmin#top",1,_AM_DBUPDATED);
	exit();
}

function BannerDelete($bid, $ok=0) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	if ( $ok == 1 ) {
	    	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("banner")." WHERE bid=$bid");
	    	redirect_header("admin.php?fct=banners&op=BannersAdmin#top",1,_AM_DBUPDATED);
		exit();
	} else {
		$myts = new MyTextSanitizer;
	    	include($xoopsConfig['root_path']."header.php");
	 	$xoopsModule->printAdminMenu();
		echo "<br />";
	    	$result=$xoopsDB->query("SELECT cid, imptotal, impmade, clicks, imageurl, clickurl FROM ".$xoopsDB->prefix("banner")." where bid=$bid");
	    	list($cid, $imptotal, $impmade, $clicks, $imageurl, $clickurl) = $xoopsDB->fetch_row($result);
	    	OpenTable();
	    	echo "
	    	<h4>"._AM_DELEBNR."</h4>
	    	<a href='$clickurl'><img src='$imageurl' border='1' /></a><br />
	    	<a href='$clickurl'>$clickurl</a><br /><br />
	    	<table width='100%' border='0'><tr align='center'>
	    	<td>"._AM_BANNERID."</td>
	    	<td>"._AM_IMPRESION."</td>
	    	<td>"._AM_IMPLEFT."</td>
	    	<td>"._AM_CLICKS."</td>
	    	<td>"._AM_NCLICKS."</td>
	    	<td>"._AM_CLINAME."</td></tr><tr align='center'>";
	    	$result2 = $xoopsDB->query("SELECT cid, name FROM ".$xoopsDB->prefix("bannerclient")." WHERE cid=$cid");
	    	list($cid, $name) = $xoopsDB->fetch_row($result2);
		$name = $myts->makeTboxData4Show($name);
	    	$percent = substr(100 * $clicks / $impmade, 0, 5);
	    	if ( $imptotal == 0 ) {
			$left = unlimited;
	    	} else {
			$left = $imptotal-$impmade;
	    	}
	    	echo "
	    	<td>$bid</td>
	    	<td>$impmade</td>
	    	<td>$left</td>
	    	<td>$clicks</td>
	    	<td>$percent%</td>
	    	<td>$name</td>
	    	</tr></table><br />
		"._AM_SUREDELE."<br /><br />
		[ <a href='admin.php?fct=banners&op=BannersAdmin#top'>"._AM_NO."</a> | <a href='admin.php?fct=banners&op=BannerDelete&bid=$bid&ok=1'>"._AM_YES."</a> ]<br /><br />";
		CloseTable();
		include($xoopsConfig['root_path']."footer.php");
	}
}

function BannerEdit($bid) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	$myts = new MyTextSanitizer;
	$result=$xoopsDB->query("SELECT cid, imptotal, impmade, clicks, imageurl, clickurl FROM ".$xoopsDB->prefix("banner")." where bid=$bid");
	list($cid, $imptotal, $impmade, $clicks, $imageurl, $clickurl) = $xoopsDB->fetch_row($result);
	OpenTable();
	echo"<h4>"._AM_EDITBNR."</h4>
	<img src='$imageurl' border='1' /><br /><br />
	<form action='admin.php' method='post'>
	"._AM_CLINAMET."
	<select name='cid'>\n";
	$result = $xoopsDB->query("SELECT cid, name FROM ".$xoopsDB->prefix("bannerclient")." where cid=$cid");
	list($cid, $name) = $xoopsDB->fetch_row($result);
	$name = $myts->makeTboxData4Show($name);
	echo "<option value='$cid' selected='selected'>$name</option>";
	$result = $xoopsDB->query("SELECT cid, name FROM ".$xoopsDB->prefix("bannerclient"));
	while(list($ccid, $name) = $xoopsDB->fetch_row($result)) {
		$name = $myts->makeTboxData4Show($name);
	    	if ( $cid != $ccid ) {
			echo "<option value='$ccid'>$name</option>";
	    	}
	}
	echo "</select><br />";
	if ( $imptotal == 0 ) {
	    	$impressions = ""._AM_UNLIMIT."";
	} else {
	    	$impressions = $imptotal;
	}
	echo"
	"._AM_ADDIMPT."<input type='text' name='impadded' size='12' maxlength='11' /> "._AM_PURCHT."<b>$impressions</b> "._AM_MADET."<b>$impmade</b><br />
	"._AM_IMGURLT."<input type='text' name='imageurl' size='50' maxlength='200' value=\"$imageurl\"><br />
	"._AM_CLICKURLT."<input type='text' name='clickurl' size='50' maxlength='200' value='$clickurl' /><br />
	<input type='hidden' name='bid' value='$bid' />
	<input type='hidden' name='imptotal' value='$imptotal' />
	<input type='hidden' name='fct' value='banners' />
	<input type='hidden' name='op' value='BannerChange' />
	<input type='submit' value='"._AM_CHGBNR."' />
	</form>";
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}

function BannerChange($bid, $cid, $imptotal, $impadded, $imageurl, $clickurl) {
	global $xoopsDB;
	$imp = $imptotal+$impadded;
	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("banner")." SET cid=$cid, imptotal=$imp, imageurl='$imageurl', clickurl='$clickurl' WHERE bid=$bid");
	redirect_header("admin.php?fct=banners&op=BannersAdmin#top",1,_AM_DBUPDATED);
	exit();
}

function BannerClientDelete($cid, $ok=0) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	if ( $ok == 1 ) {
	    	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("banner")." WHERE cid=$cid");
	    	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("bannerclient")." where cid=$cid");
	    	redirect_header("admin.php?fct=banners&op=BannersAdmin#top",1,_AM_DBUPDATED);
		exit();
	} else {
		$myts = new MyTextSanitizer;
	    	include($xoopsConfig['root_path']."header.php");
	    	$xoopsModule->printAdminMenu();
		echo "<br />";
	    	$result = $xoopsDB->query("SELECT cid, name FROM ".$xoopsDB->prefix("bannerclient")." WHERE cid=$cid");
	    	list($cid, $name) = $xoopsDB->fetch_row($result);
		$name = $myts->makeTboxData4Show($name);
	    	OpenTable();
	    	echo "
	    	<h4>"._AM_DELEADC."</h4>
	    	".sprintf(_AM_SUREDELCLI,$name)."<br /><br />";
	    	$result2 = $xoopsDB->query("SELECT imageurl, clickurl FROM ".$xoopsDB->prefix("banner")." WHERE cid=$cid");
	    	$numrows = $xoopsDB->num_rows($result2);
	    	if ( $numrows == 0 ) {
			echo ""._AM_NOBNRRUN."<br /><br />";
	    	} else {
			echo "<font color='#ff0000'><b>"._AM_WARNING."</b></font><br />
			"._AM_ACTBNRRUN."<br /><br />";
	    	}
	    	while(list($imageurl, $clickurl) = $xoopsDB->fetch_row($result2)) {
			echo"
			<a href='$clickurl'><img src='$imageurl' border='1' /></a><br />
			<a href='$clickurl'>$clickurl</a><br /><br />
			";
	    	}
	}
	echo _AM_SUREDELBNR."<br /><br />
	[ <a href='admin.php?fct=banners&op=BannersAdmin#top'>"._AM_NO."</a> | <a href='admin.php?fct=banners&op=BannerClientDelete&cid=$cid&ok=1'>"._AM_YES."</a> ]<br /><br />";
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}

function BannerClientEdit($cid) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	$result = $xoopsDB->query("SELECT name, contact, email, login, passwd, extrainfo FROM ".$xoopsDB->prefix("bannerclient")." WHERE cid=$cid");
	list($name, $contact, $email, $login, $passwd, $extrainfo) = $xoopsDB->fetch_row($result);
	$name = $myts->makeTboxData4Edit($name);
	$contact = $myts->makeTboxData4Show($contact);
	$extrainfo = $myts->makeTareaData4Show($extrainfo);
	OpenTable();
	echo"
	<h4>"._AM_EDITADVCLI."</h4>
	<form action='admin.php' method='post'>
	"._AM_CLINAMET."<input type='text' name='name' value='$name' size='30' maxlength='60' /><br />
	"._AM_CONTNAMET."<input type='text' name='contact' value='$contact' size='30' maxlength='60' /><br />
	"._AM_CONTMAILT ."<input type='text' name='email' size='30' maxlength='60' value='$email' /><br />
	"._AM_CLILOGINT."<input type='text' name='login' size='12' maxlength='10' value='$login' /><br />
	"._AM_CLIPASST."<input type='text' name='passwd' size='12' maxlength='10' value='$passwd' /><br />
	"._AM_EXTINFO."<br /><textarea name='extrainfo' cols='60' rows='10' />$extrainfo</textarea><br />
	<input type='hidden' name='cid' value='$cid' />
	<input type='hidden' name='op' value='BannerClientChange' />
	<input type='hidden' name='fct' value='banners' />
	<input type='submit' value='"._AM_CHGCLI."' />";
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}

function BannerClientChange($cid, $name, $contact, $email, $extrainfo, $login, $passwd) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$name = $myts->makeTboxData4Save($name);
	$contact = $myts->makeTboxData4Save($contact);
	$extrainfo = $myts->makeTareaData4Save($extrainfo);
	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("bannerclient")." SET name='$name', contact='$contact', email='$email', login='$login', passwd='$passwd', extrainfo='$extrainfo' where cid=$cid");
	redirect_header("admin.php?fct=banners&op=BannersAdmin#top",1,_AM_DBUPDATED);
}

} else {
    	echo "Access Denied";
}

?>