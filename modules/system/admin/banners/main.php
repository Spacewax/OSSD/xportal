<?php
if (!eregi("admin.php", $PHP_SELF)) { die ("Access Denied"); }
include_once("admin/banners/banners.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

switch($op) {
		case "BannersAdmin":
			BannersAdmin();
			break;

		case "BannersAdd":
			BannersAdd($name, $cid, $imageurl, $clickurl, $imptotal);
			break;

		case "BannerAddClient":
			BannerAddClient($name, $contact, $email, $login, $passwd, $extrainfo);
			break;

		case "BannerFinishDelete":
			BannerFinishDelete($bid);
			break;

		case "BannerDelete":
			BannerDelete($bid, $ok);
			break;

		case "BannerEdit":
			BannerEdit($bid);
			break;
		
		case "BannerChange":
			BannerChange($bid, $cid, $imptotal, $impadded, $imageurl, $clickurl);
			break;

		case "BannerClientDelete":
			BannerClientDelete($cid, $ok);
			break;

		case "BannerClientEdit":
			BannerClientEdit($cid);
			break;

		case "BannerClientChange":
			BannerClientChange($cid, $name, $contact, $email, $extrainfo, $login, $passwd);
			break;

    		default:
			BannersAdmin();
			break;
}
?>