<?php

if (!eregi("admin.php", $PHP_SELF)) die("Access Denied"); 

include ("admin/blocksadmin/blocksadmin.php");

if(isset($op)){
	switch($op){
		case "add":
      		include($xoopsConfig['root_path']."header.php");
	  	$xoopsModule->printAdminMenu();
	  	echo "<br />";
	  	add_block();
	  	echo "<br />";
      		include($xoopsConfig['root_path']."footer.php");
      		break;

   		case "save":
      		save_block($bside, $bweight, $bposition, $bvisible, $btitle, $bcontent, $bctype);
      		break;

   		case "update":
      		update_block($bid, $bside, $bweight, $bposition, $bvisible, $btitle, $bcontent, $bctype, $options);
      		break;

   		case "delete_ok":
      		delete_block_ok($bid);
      		break;

   		case "delete":
      		include($xoopsConfig['root_path']."header.php");
	  	$xoopsModule->printAdminMenu();
	  	echo "<br />";
	  	delete_block($bid);
	  	echo "<br />";
      		include($xoopsConfig['root_path']."footer.php");
      		break;

   		case "edit":
      		include($xoopsConfig['root_path']."header.php");
	  	$xoopsModule->printAdminMenu();
	  	echo "<br />";
	  	edit_block($bid);
	  	echo "<br />";
      		include($xoopsConfig['root_path']."footer.php");
      		break;
   
   		default:
      		include($xoopsConfig['root_path']."header.php");
	 	$xoopsModule->printAdminMenu();
	  	echo "<br />";
      		list_blocks();
	  	echo "<br />";
      		include($xoopsConfig['root_path']."footer.php");
      		break;
	}
}else{
	      	include($xoopsConfig['root_path']."header.php");
	 	$xoopsModule->printAdminMenu();
	  	echo "<br />";
      		list_blocks();
	  	echo "<br />";
      		include($xoopsConfig['root_path']."footer.php");
      		break;
}
?>