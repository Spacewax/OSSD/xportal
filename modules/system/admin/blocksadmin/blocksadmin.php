<?php

if ( !eregi("admin.php", $PHP_SELF) ) { 
	die ("Access Denied"); 
}
// check if the user is authorised
if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {
	include_once($xoopsConfig['root_path']."class/xoopsblock.php");
	function blocks_admin(){
		OpenTable();
		echo "<div align=\"center\"><h4>"._AM_BADMIN."</h4></div>\n";
		echo " - <b><a href=admin.php?fct=blocksadmin&amp;op=list>"._AM_LISTBLOCK."</a></b>";
		echo "<br /><br />";
		echo " - <b><a href=admin.php?fct=blocksadmin&amp;op=add>"._AM_ADDBLOCK."</a></b>";
 	   	CloseTable();
		echo "<br />";
	}

	function list_blocks(){
		global $xoopsTheme, $xoopsUser;
		$arr = array();
		$block = new XoopsBlock();
		blocks_admin();
		OpenTable();
		echo "<div align=\"center\"><h4>"._AM_SIDEBADMIN."</h4>\n";
		echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">";
		echo "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\">\n";
		echo "<tr valign=\"middle\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><td><b>"._AM_SIDE."</b></td><td><b>"._AM_BLKDESC."</b></td><td><b>"._AM_TITLE."</b></td><td align=\"center\"><b>"._AM_WEIGHT."</b></td><td align=\"center\"><b>"._AM_VISIBLE4."</b></td><td align=\"right\"><b>"._AM_ACTION."</b></td></tr>\n";
		$arr = $block->getAllBlocks(XOOPS_SIDEBLOCK_LEFT, $xoopsUser);
		foreach($arr as $ele){
			show_block($ele);
		}
		$arr = $block->getAllBlocks(XOOPS_SIDEBLOCK_RIGHT, $xoopsUser);
		foreach($arr as $ele){
			show_block($ele);
		}
		echo "</table></td></tr></table>\n";
		echo "<br /><br /><br />";

		echo "<h4>"._AM_CENTERBADMIN."</h4>\n";
		echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">";
		echo "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\">\n";
		echo "<tr valign=\"middle\" bgcolor=\"".$xoopsTheme['bgcolor3']."\"><td><b>"._AM_SIDE."</b></td><td><b>"._AM_BLKDESC."</b></td><td><b>"._AM_TITLE."</b></td><td align=\"center\"><b>"._AM_WEIGHT."</b></td><td align=\"center\"><b>"._AM_VISIBLE4."</b></td><td align=\"right\"><b>"._AM_ACTION."</b></td></tr>\n";
		$arr = $block->getAllBlocks(XOOPS_CENTERBLOCK_CENTER, $xoopsUser);
		foreach($arr as $ele){
			show_block($ele);
		}
		$arr = $block->getAllBlocks(XOOPS_CENTERBLOCK_LEFT, $xoopsUser);
		foreach($arr as $ele){
			show_block($ele);
		}
		$arr = $block->getAllBlocks(XOOPS_CENTERBLOCK_RIGHT, $xoopsUser);
		foreach($arr as $ele){
			show_block($ele);
		}
		echo "</table></td></tr></table></div>\n";
		CloseTable();
	}

	function show_block($ele){
		global $xoopsTheme;
		if ( $ele->visible() == 3 ) {
			$visible = _AM_ADMINS;
		} elseif ( $ele->visible() == 2 ) {
			$visible = _AM_REGUSERS;
		} elseif ( $ele->visible() == 1 ) {
			$visible = _AM_ALL;
		} else {
			$visible = _AM_NOTVIS;
		}
		if ( $ele->side() == XOOPS_SIDEBLOCK_LEFT){
			$side = _AM_LEFT;
		} elseif ( $ele->side() == XOOPS_SIDEBLOCK_RIGHT ){
			$side = _AM_RIGHT;
		} elseif ( $ele->side() == XOOPS_CENTERBLOCK_LEFT ){
			$side = _AM_LEFT;
		} elseif ( $ele->side() == XOOPS_CENTERBLOCK_RIGHT ){
			$side = _AM_RIGHT;
		} elseif ( $ele->side() == XOOPS_CENTERBLOCK_CENTER ){
			$side = _AM_CENTER;
		}
		if($ele->title() == ""){
			$title = "&nbsp;";
		} else {
			$title = $ele->title();
		}
		echo "<tr valign=\"top\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><td>".$side."</td><td>".$ele->name()."</td><td>".$title."</td><td align=\"center\">".$ele->weight()."</td><td align=\"center\">".$visible."</td><td align=\"right\"><a href=\"admin.php?fct=blocksadmin&amp;op=edit&amp;bid=".$ele->bid()."\">"._AM_EDIT."</a>&nbsp;<a href=\"admin.php?fct=blocksadmin&amp;op=delete&amp;bid=".$ele->bid()."\">"._AM_DELETE."</a></tr>\n";
	}

	function add_block(){
		global $xoopsTheme;
		blocks_admin();
		OpenTable();
		echo "<div align=\"center\">";
                echo "<h4>" ._AM_ADDBLOCK."</h4>
                <form action=\"admin.php?fct=blocksadmin\" method=\"post\">";
		echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">";
                echo "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\">\n";
		echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\" width=\"30%\">
		<b>"._AM_BLKTYPE."</b>
		</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <select name=\"bside\">
                <option value=\"0\">" ._AM_SBLEFT."</option>
                <option value=\"1\">" ._AM_SBRIGHT."</option>
		<option value=\"3\">" ._AM_CBLEFT."</option>
                <option value=\"4\">" ._AM_CBRIGHT."</option>
                <option value=\"5\">" ._AM_CBCENTER."</option>
                </select>
                </td></tr><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
		<b>"._AM_WEIGHT."</b>
                </td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <input type=\"text\" name=\"bweight\" size=\"2\" maxlength=\"2\">
                </td></tr><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>" ._AM_VISIBLE4."</b>
		</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <select name=\"bvisible\">
                <option value=\"0\">" ._AM_NOTVIS."</option>
                <option value=\"1\" selected=\"selected\">" ._AM_ALL."</option>
                <option value=\"2\">" ._AM_REGUSERS."</option>
                <option value=\"3\">" ._AM_ADMINS."</option>
                </select>
                </td></tr><tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>" ._AM_TITLE."</b>
		</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <input class=\"textbox\" type=\"text\" name=\"btitle\" size=\"60\" maxlength=\"60\">
                </td></tr><tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>" ._AM_CONTENT."</b>
		</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <textarea class=\"textbox\" name=\"bcontent\" cols=\"60\" rows=\"10\"></textarea>
                </td></tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>" ._AM_CTYPE."</b>
		</td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <select name=\"bctype\">
                <option value=\"H\">" ._AM_HTML."</option>
                <option value=\"P\">" ._AM_PHP."</option>
		<option value=\"S\">" ._AM_AFWSMILE."</option>
		<option value=\"T\">" ._AM_AFNOSMILE."</option>
                </select>
                </td></tr>";
                echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"></td>
		<td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <input type=\"hidden\" name=\"op\" value=\"save\">
                <input type=\"submit\" value=\"" ._AM_SUBMIT."\">
		</td></tr></table></td></tr></table>
                </form></div>";
                CloseTable();
	}

	function save_block($bside, $bweight, $bposition, $bvisible, $btitle, $bcontent, $bctype){
		$myblock = new XoopsBlock();
		$myblock->setSide($bside);
		$myblock->setWeight($bweight);
		$myblock->setPosition($bposition);
		$myblock->setVisible($bvisible);
		$myblock->setWeight($bweight);
		$myblock->setTitle($btitle);
		$myblock->setContent($bcontent);
		$myblock->setCType($bctype);
		switch($bctype){
			case "H":
			$name = _AM_CUSTOMHTML;
			break;
			case "P":
			$name = _AM_CUSTOMPHP;
			break;
			case "S":
			$name = _AM_CUSTOMSMILE;
			break;
			default:
			$name = _AM_CUSTOMNOSMILE;
			break;
		}
		$myblock->setName($name);
		$myblock->store();
		redirect_header("admin.php?fct=blocksadmin",2,_AM_DBUPDATED);
		exit();
	}

	function edit_block($bid){
		global $xoopsTheme;
		$myblock = new XoopsBlock($bid);
		blocks_admin();
		OpenTable();
		echo "<div align=\"center\">";
                echo "<h4>" ._AM_EDITBLOCK."</h4>
                <form action=\"admin.php?fct=blocksadmin\" method=\"post\">
		<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">
                <table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\">
		<tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\" width=\"30%\">
		<b>"._AM_NAME."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">".$myblock->name()."</td></tr>
		<tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>"._AM_BLKTYPE."</b></td>
		<td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <select name=\"bside\">
                <option value=\"0\"";
		if($myblock->side() == 0) echo " selected=\"selected\"";
		echo ">" ._AM_SBLEFT."</option>
                <option value=\"1\"";
		if($myblock->side() == 1) echo " selected=\"selected\"";
		echo ">" ._AM_SBRIGHT."</option>
		<option value=\"3\"";
		if($myblock->side() == 3) echo " selected=\"selected\"";
		echo ">" ._AM_CBLEFT."</option>
                <option value=\"4\"";
		if($myblock->side() == 4) echo " selected=\"selected\"";
		echo ">" ._AM_CBRIGHT."</option>
                <option value=\"5\"";
		if($myblock->side() == 5) echo " selected=\"selected\"";
		echo ">" ._AM_CBCENTER."</option>
                </select>
                </td></tr><tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>"._AM_WEIGHT."</b>
                </td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <input class=\"textbox\" type=\"text\" name=\"bweight\" size=\"2\" maxlength=\"2\" value=\"";
		echo $myblock->weight();
		echo "\" />
                </td></tr><tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>"._AM_VISIBLE4."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <select name=\"bvisible\">
                <option value=\"0\"";
		if($myblock->visible() == 0) echo " selected=\"selected\"";
		echo ">" ._AM_NOTVIS."</option>
                <option value=\"1\"";
		if($myblock->visible() == 1) echo " selected=\"selected\"";
		echo ">" ._AM_ALL."</option>
		<option value=\"2\"";
		if($myblock->visible() == 2) echo " selected=\"selected\"";
		echo ">" ._AM_REGUSERS."</option>
		<option value=\"3\"";
		if($myblock->visible() == 3) echo " selected=\"selected\"";
		echo ">" ._AM_ADMINS."</option>
                </select>
                </td></tr><tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>" ._AM_TITLE."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <input class=\"textbox\" type=\"text\" name=\"btitle\" size=\"60\" maxlength=\"60\" value=\"";
		echo $myblock->title("E");
		echo "\" />
                </td></tr><tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>"._AM_CONTENT."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <textarea class=\"textbox\" name=\"bcontent\" cols=\"60\" rows=\"10\">";
		echo $myblock->content("E");
		echo "</textarea>
                </td></tr>";
		if ( !$myblock->isCustom() ) {
			echo "<tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                	<b>" ._AM_POSCONTT."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                	<select name=\"bposition\">
                	<option value=\"0\"";
			if($myblock->position() == 0) echo " selected=\"selected\"";
			echo ">" ._AM_ABOVEORG."</option>
                	<option value=\"1\"";
			if($myblock->position() == 1) echo " selected=\"selected\"";
			echo ">" ._AM_AFTERORG."</option>
                	</select>
                	</td></tr>";
		}
		echo "<tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\">
                <b>" ._AM_CTYPE."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <select name=\"bctype\">
                <option value=\"H\"";
		if($myblock->c_type() == "H") echo " selected=\"selected\"";
		echo ">" ._AM_HTML."</option>
                <option value=\"P\"";
		if($myblock->c_type() == "P") echo " selected=\"selected\"";
		echo ">" ._AM_PHP."</option>
		<option value=\"S\"";
		if($myblock->c_type() == "S") echo " selected=\"selected\"";
		echo ">" ._AM_AFWSMILE."</option>
		<option value=\"T\"";
		if($myblock->c_type() == "T") echo " selected=\"selected\"";
		echo ">" ._AM_AFNOSMILE."</option>
                </select>
                </td></tr>";

		$edit_form = $myblock->getOptions();
		if ($edit_form) {
			echo "<tr valign=\"top\"><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"><b>"._AM_OPTIONS."</b></td><td bgcolor=\"".$xoopsTheme['bgcolor1']."\">";
			echo $edit_form;
			echo "</td></tr>";
		}
                echo "<tr><td bgcolor=\"".$xoopsTheme['bgcolor3']."\"></td>
		<td bgcolor=\"".$xoopsTheme['bgcolor1']."\">
                <input type=\"hidden\" name=\"op\" value=\"update\">
                <input type=\"hidden\" name=\"bid\" value=\"".$myblock->bid()."\">
                <input type=\"submit\" value=\"" ._AM_SAVECHANGES."\">
		</td></tr></table></td></tr></table>
                </form></div>";
                CloseTable();
	}

	function update_block($bid, $bside, $bweight, $bposition, $bvisible, $btitle, $bcontent, $bctype, $options=""){
		$myblock = new XoopsBlock($bid);
		$myblock->setSide($bside);
		$myblock->setWeight($bweight);
		$myblock->setPosition($bposition);
		$myblock->setVisible($bvisible);
		$myblock->setWeight($bweight);
		$myblock->settitle($btitle);
		$myblock->setContent($bcontent);
		$myblock->setOptions($options);
		$myblock->setCType($bctype);
		if($myblock->type == "C"){
			switch($bctype){
				case "H":
				$name = _AM_CUSTOMHTML;
				break;
				case "P":
				$name = _AM_CUSTOMPHP;
				break;
				case "S":
				$name = _AM_CUSTOMSMILE;
				break;
				default:
				$name = _AM_CUSTOMNOSMILE;
				break;
			}
			$myblock->setName($name);
		}
		$myblock->store();
		redirect_header("admin.php?fct=blocksadmin",2,_AM_DBUPDATED);
		exit();
	}

	function delete_block($bid){
		$myblock = new XoopsBlock($bid);
		if($myblock->type() == "S"){
			$message = _AM_SYSTEMCANT;
			redirect_header("admin.php?fct=blocksadmin",5,$message);
			exit();
		} elseif ($myblock->type() == "M") {
			$message = _AM_MODULECANT;
			redirect_header("admin.php?fct=blocksadmin",5,$message);
			exit();
		} else {
			echo "<div align=\"center\"><big>".sprintf(_AM_RUSUREDEL,$myblock->title())."</big>";
			echo "<br>";
                        echo "[ <a href=\"admin.php?fct=blocksadmin&op=delete_ok&bid=".$myblock->bid()."\">"._AM_YES."</a> | <a href=\"admin.php?fct=blocksadmin\">"._AM_NO."</a> ]</div>";
		}
	}

	function delete_block_ok($bid){
		$myblock = new XoopsBlock($bid);
		$myblock->delete();
		redirect_header("admin.php?fct=blocksadmin",2,_AM_DBUPDATED);
		exit();
	}

} else {
	echo "Access Denied";
}
?>