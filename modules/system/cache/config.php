<?php
/*********************************************************************
Remember to chmod 666 this file in order to let the system write to it properly.
If you can't change the permissions you can edit the rest of this file by hand.
*********************************************************************/

// Site name
$xoopsConfig['sitename'] = "your site name";

// Slogan for your site
$xoopsConfig['slogan'] = "XOOPS Site";

// Admin mail address
$xoopsConfig['adminmail'] = "";

// Default language
$xoopsConfig['language'] = "english";

// Module for your start page
$xoopsConfig['startpage'] = "news";

// Default theme
$xoopsConfig['default_theme'] = "phpkaox";

// Username for anonymous users
$xoopsConfig['anonymous'] = "Anonymous";

// Minimum length of password required
$xoopsConfig['minpass'] = 5;

// Allow anonymous users to post? (1=Yes 0=No)
$xoopsConfig['anonpost'] = 1;

// Notify by mail when a new user is registered? (1=Yes 0=No)
$xoopsConfig['new_user_notify'] = 1;

// Allow users to delete own account? (1=Yes 0=No)
$xoopsConfig['self_delete'] = 1;

// Display loading.. image? (1=Yes 0=No) 
$xoopsConfig['display_loading_img'] = 1;

// Use gzip compression? PHP version 4.0.5 or higher recommended. Your server version is  (1=Yes 0=No) 
$xoopsConfig['gzip_compression'] = 0;

// How strict should the allowed chars for username be?   (Strict (only alphabets and numbers)=0 Medium=1 Light (recommended for multi-byte chars)=2)
$xoopsConfig['uname_test_level'] = 0;

// Name for user cookies. This cookie contains only a user name and is saved in a user pc for a year (if the user wishes). If a user have this cookie, username will be automatically inserted in the login box.
$xoopsConfig['usercookie'] = "xoops_user";

// Name for session cookies. This cookie enables a user to automatically login until session expires or the user logs out.
$xoopsConfig['sessioncookie'] = "xoops_session";

// Maximum duration of session idle time in seconds
$xoopsConfig['sessionexpire'] = 3600;

// Server timezone
$xoopsConfig['server_TZ'] = 0;

// Default timezone
$xoopsConfig['default_TZ'] = 0;

// Activate banner ads? (1=Yes 0=No)
$xoopsConfig['banners'] = 1;

// Activate graphic menu for Administration Menu? (1=Yes 0=No)
$xoopsConfig['admingraphic'] = 1;

// Your IP. This ip will not counted as impression for banners 
$xoopsConfig['my_ip'] = "";

// HTML tags allowed in all posts.
$xoopsConfig['allowed_html'] = "<a><b><big><blockquote><font><hr><i><img><li><small><strong><table><tbody><td><tfoot><th><thead><tr><tt><ul>";

?>