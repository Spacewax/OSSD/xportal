<?php
// Module Info

// The name of this module
define("_MI_HEADLINES_NAME","Headlines");

// A brief description of this module
define("_MI_HEADLINES_DESC","Displayes RSS/XML Newsfeed from other sites in a block");

// Names of blocks for this module (Not all module has blocks)
define("_MI_HEADLINES_BNAME","Headlines block");
?>