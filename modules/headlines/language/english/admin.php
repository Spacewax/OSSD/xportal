<?php
//%%%%%%        Admin Module Name  Headlines         %%%%%
define("_AM_DBUPDATED","Database Updated Successfully!");

define("_AM_ID","ID");
define("_AM_SITENAME","Site Name");
define("_AM_URL","URL");
define("_AM_STATUS","Status");
define("_AM_FUNCTION","Functions");
define("_AM_ACTIVE","Active");
define("_AM_INACTIVE","Inactive");
define("_AM_EDIT","Edit");
define("_AM_DELETE","Delete");
define("_AM_ADDHEADL","Add Headline");
define("_AM_SITENAMET","Site Name:");
define("_AM_URLT","URL:");
define("_AM_URLEDFXMLT","URL for the RDF/XML file:");
define("_AM_STATUST","Status:");
define("_AM_ADD","Add");
define("_AM_EDITHEADL","Edit Headline");
define("_AM_SAVECHANG","Save Changes");
define("_AM_WANTDEL","WARNING: Are you sure you want to delete this Headline");
define("_AM_YES","Yes");
define("_AM_NO","No");
?>