<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

include_once("admin_header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

/*********************************************************/
/* Headlines Grabber to put other sites news in our site */
/*********************************************************/

function HeadlinesAdmin() {
	global $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsModule;
        include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
        echo "<br />";
        OpenTable();
        echo "<form action='index.php' method='post'>
	<div align='center'>
	<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>
	<table width='100%' border='0' cellpadding='2' cellspacing='1'>
	<tr bgcolor='".$xoopsTheme['bgcolor3']."'>
        <td align='center'><b>"._AM_ID."</b></td>
        <td align='center'><b>"._AM_SITENAME."</b></td>
        <td align='center'><b>"._AM_URL."</b></td>
        <td align='center'><b>"._AM_STATUS."</b></td>
        <td align='center'><b>"._AM_FUNCTION."</b></td>";
        $result = $xoopsDB->query("SELECT hid, sitename, url, status FROM ".$xoopsDB->prefix("headlines")." ORDER BY hid");
        $myts = new MyTextSanitizer;
        while ( list($hid, $sitename, $url, $status) = $xoopsDB->fetch_row($result) ) {
                $sitename = $myts->makeTboxData4Show($sitename);
                echo "<tr bgcolor='".$xoopsTheme["bgcolor1"]."'>
                <td align='center'>$hid</td>
                <td align='center'><a href=$url target=new>$sitename</a></td>
                <td align='center'>$url</td>";
                if ( $status == 1 ) {
                            $status = "<font color=Yellow>"._AM_ACTIVE."</font>";
                } else {
                            $status = ""._AM_INACTIVE."";
                }
                echo "
                <td align='center'>$status</td>
                <td align='center'><a href='index.php?op=HeadlinesEdit&hid=$hid'>"._AM_EDIT."</a> | <a href='index.php?op=HeadlinesDel&hid=$hid&ok=0'>"._AM_DELETE."</a></td></tr>";
            }
        echo "</table></td></tr></table></form>
        <br /><br />
        <h4>"._AM_ADDHEADL."</h4>
        <form action='index.php' method='post'>
	<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>
	<table width='100%' border='0' cellpadding='2' cellspacing='1'>
	<tr>
        <td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_SITENAMET."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='xsitename' size='31' maxlength='30' /></td></tr><tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_URLT."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='url' size='50' maxlength='100' /></td></tr><tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_URLEDFXMLT."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='headlinesurl' size='50' maxlength='200' /></td></tr><tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_STATUST."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><select name='status'>
        <option name='status' value='1'>"._AM_ACTIVE."</option>
        <option name='status' value='0' selected='selected'>"._AM_INACTIVE."</option>
        </select></td></tr>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>&nbsp;</td>
	<td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='hidden' name='fct' value='headlines' />
        <input type='hidden' name='op' value='HeadlinesAdd' /><input type='submit' value='"._AM_ADD."' /></td>
	</tr></table></td></tr></table>
        </form></div>";
        CloseTable();
}

function HeadlinesEdit($hid) {
        global $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsModule;
        include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
        echo "<br />";
        $result = $xoopsDB->query("SELECT sitename, url, headlinesurl, status FROM ".$xoopsDB->prefix("headlines")." WHERE hid=$hid");
        list($xsitename, $url, $headlinesurl, $status) = $xoopsDB->fetch_row($result);
        $myts = new MyTextSanitizer;
        $xsitename = $myts->makeTboxData4Edit($xsitename);
        OpenTable();
        echo "
        <h4>"._AM_EDITHEADL."</h4>
        <form action='index.php' method='post'>
        <input type='hidden' name='hid' value='$hid' />
	<div align='center'>
	<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>
	<table width='100%' border='0' cellpadding='2' cellspacing='1'>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_SITENAMET."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='xsitename' size='31' maxlength='30' value='$xsitename' /></td></tr>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_URLT."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type=text name=url size=50 maxlength=100 value=$url></td></tr>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_URLEDFXMLT."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'><input type='text' name='headlinesurl' size='50' maxlength='200' value='".$headlinesurl."' /></td></tr>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>
        <b>"._AM_STATUST."</b></td><td bgcolor='".$xoopsTheme['bgcolor1']."'>
	<select name='status'>";
        if( $status == 1 ) {
        	$sel_a = "selected='selected'";
        } else {
                $sel_i = "selected='selected'";
        }
        echo "
        <option name='status' value='1' $sel_a>"._AM_ACTIVE."</option>
        <option name='status' value='0' $sel_i>"._AM_INACTIVE."</option>
        </select></td></tr>
	<tr><td bgcolor='".$xoopsTheme['bgcolor3']."'>&nbsp;</td>
	<td bgcolor='".$xoopsTheme['bgcolor1']."'>
	<input type='hidden' name='fct' value='headlines' />
        <input type='hidden' name='op' value='HeadlinesSave' />
        <input type='submit' value='"._AM_SAVECHANG."' />
	</td>
	</tr></table></td></tr></table>
        </form></div>";
        CloseTable();
}

function HeadlinesSave($hid, $xsitename, $url, $headlinesurl, $status) {
        global $xoopsDB;
        $myts = new MyTextSanitizer;
        $xsitename = ereg_replace(" ", "", $xsitename);
        $xsitename = $myts->makeTboxData4Save($xsitename);
        $xoopsDB->query("UPDATE ".$xoopsDB->prefix("headlines")." SET sitename='$xsitename', url='$url', headlinesurl='$headlinesurl', status=$status WHERE hid=$hid");
        redirect_header("index.php?op=HeadlinesAdmin",1,_AM_DBUPDATED);
        exit();
}

function HeadlinesAdd($xsitename, $url, $headlinesurl, $status) {
        global $xoopsDB;
        $xsitename = ereg_replace(" ", "", $xsitename);
	$newid = $xoopsDB->GenID("headlines_hid_seq");
        $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("headlines")." (hid, sitename, url, headlinesurl, status) VALUES ($newid, '$xsitename', '$url', '$headlinesurl', $status)");
        redirect_header("index.php?op=HeadlinesAdmin",1,_AM_DBUPDATED);
        exit();
}

function HeadlinesDel($hid, $ok=0) {
        global $xoopsDB, $xoopsConfig, $xoopsModule;
        if ( $ok == 1 ) {
                $xoopsDB->query("delete from ".$xoopsDB->prefix(headlines)." where hid=$hid");
                redirect_header("index.php?op=HeadlinesAdmin",1,_AM_DBUPDATED);
                exit();
        } else {
                include($xoopsConfig['root_path']."header.php");
                $xoopsModule->printAdminMenu();
                echo "<br />";
                OpenTable();
                echo "<center><br />";
                echo "<font color='#ff0000'>";
                echo "<b>"._AM_WANTDEL."</b><br /><br /></font>";
        	echo "[ <a href='index.php?op=HeadlinesDel&hid=$hid&ok=1'>"._AM_YES."</a> | <a href='index.php?op=HeadlinesAdmin'>"._AM_NO."</a> ]<br /><br />";
        	CloseTable();
	}

}

switch($op) {
	case "HeadlinesDel":
		HeadlinesDel($hid, $ok);
		break;
	case "HeadlinesAdd":
		HeadlinesAdd($xsitename, $url, $headlinesurl, $status);
		break;
	case "HeadlinesSave":
		HeadlinesSave($hid, $xsitename, $url, $headlinesurl, $status);
		break;
	case "HeadlinesAdmin":
		HeadlinesAdmin();
		break;
	case "HeadlinesEdit":
		HeadlinesEdit($hid);
		break;
    	default:
		HeadlinesAdmin();
		break;
}
include("admin_footer.php");
?>