<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_headlines_show() {
	global $xoopsDB, $xoopsConfig;
	$block = array();
	$block['title'] = _MB_HEADLINES_TITLE;
	$block['content'] = "";
	include($xoopsConfig['root_path']."class/phpsyndication.lib.php");
/*    	$separ        =        "<li>";
    	$cache_time        =        3600;
    	$max_items        =        15;
    	$max_length = 100;
    	$target        =        "new";
    	$items        =        0;*/
    	$result = $xoopsDB->query("SELECT hid, sitename, url, headlinesurl FROM ".$xoopsDB->prefix("headlines")." WHERE status=1");
    	while (list($hid, $sitename, $url, $headlinesurl) = $xoopsDB->fetch_row($result)) {
		//$cache_dir = $xoopsConfig['root_path']."cache";
    		//$cache_file = "$cache_dir/newsheadline-$hid.cache";  //formod
		$synd = new RSStoHTML($headlinesurl);
    		$block['content'] .= "<p><b>".$synd->getTitle('_blank')."</b><br />";
        	$block['content'] .= $synd->getHtml('_blank');
		$block['content'] .= "</p>";
    	}
	return $block;
}
?>