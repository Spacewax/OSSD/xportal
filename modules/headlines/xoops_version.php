<?php
$modversion['name'] = _MI_HEADLINES_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_HEADLINES_DESC;
$modversion['author'] = "Francisco Burzi<br>( http://phpnuke.org/ )";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "headlines.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "headlines_slogo.jpg";
$modversion['dirname'] = "headlines";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Blocks
$modversion['blocks'][1]['file'] = "headlines.php";
$modversion['blocks'][1]['name'] = _MI_HEADLINES_BNAME;
$modversion['blocks'][1]['description'] = "Shows headline news via XML/RSS news feed";
$modversion['blocks'][1]['show_func'] = "b_headlines_show";

// Menu
$modversion['hasMain'] = 0;
?>