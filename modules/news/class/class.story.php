<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
################################################################################
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
include_once($xoopsConfig['root_path']."class/xoopscomments.php");

class Story {
	var $table;
	var $storyid;
	var $catid=0;
	var $topicid;
	var $uid;
	var $title;
	var $hometext;
	var $bodytext="";
	var $notes="";
	var $counter;
	var $created;
	var $published;
	var $hostname;
	var $nohtml=0;
	var $nosmiley=0;
	var $ihome=0;
	var $notifypub=0;
	var $type;
	var $approved;
	var $db;

	function Story($storyid=-1){
		global $xoopsDB;
		$this->db = $xoopsDB;
		$this->table = $this->db->prefix("stories");
		if(is_array($storyid)){
			$this->makeStory($storyid);
		}elseif($storyid != -1){
			$this->getStory($storyid);
		}
	}

	function setStoryId($value){
		$this->storyid = $value;
	}

	function setCatId($value){
		$this->catid = $value;
	}

	function setTopicId($value){
		$this->topicid = $value;
	}

	function setUid($value){
		$this->uid=$value;
	}

	function setTitle($value){
		$this->title=$value;
	}

	function setHometext($value){
		$this->hometext=$value;
	}

	function setBodytext($value){
		$this->bodytext=$value;
	}

	function setNotes($value){
		$this->notes=$value;
	}

	function setPublished($value){
		$this->published=$value;
	}

	function setHostname($value){
		$this->hostname=$value;
	}

	function setNohtml($value=0){
		$this->nohtml=$value;
	}

	function setNosmiley($value=0){
		$this->nosmiley=$value;
	}

	function setIhome($value){
		$this->ihome=$value;
	}

	function setNotifyPub($value){
		$this->notifypub=$value;
	}

	function setType($value){
		$this->type = $value;
	}

	function setApproved($value){
		$this->approved = $value;
	}

	function store($approved=false){
		$newpost = 0;
		$myts = new MyTextSanitizer;
		$title =$myts->censorString($this->title);
		$hometext =$myts->censorString($this->hometext);
		$bodytext =$myts->censorString($this->bodytext);
		$notes =$myts->censorString($this->notes);
		$title = $myts->makeTboxData4Save($title);
		$hometext = $myts->makeTareaData4Save($hometext);
		$bodytext = $myts->makeTareaData4Save($bodytext);
		$notes = $myts->makeTareaData4Save($notes);
		if(!isset($this->nohtml) || $this->nohtml != 1){
			$this->nohtml = 0;
		}
		if(!isset($this->nosmiley) || $this->nosmiley != 1){
			$this->nosmiley = 0;
		}
		if(!isset($this->notifypub) || $this->notifypub != 1){
			$this->notifypub = 0;
		}
		if(!isset($this->storyid)){
			$newpost = 1;
			$newstoryid = $this->db->GenID($this->table."_storyid_seq");
			$created = time();
			if($this->approved){
				$sql = "INSERT INTO ".$this->table." (storyid, catid, uid, title, created, published, hostname, nohtml, nosmiley, hometext, bodytext, counter, topicid, notes, ihome, notifypub, type) VALUES ($newstoryid,".$this->catid.",".$this->uid.",'".$title."',".$created.",".$this->published.",'".$this->hostname."',".$this->nohtml.",".$this->nosmiley.",'".$hometext."','".$bodytext."',0,".$this->topicid.",'".$notes."',".$this->ihome.",".$this->notifypub.",'".$this->type."')";
			} else {
				$sql = "INSERT INTO ".$this->table." (storyid, catid, uid, title, created, published, hostname, nohtml, nosmiley, hometext, bodytext, counter, topicid, notes, ihome, notifypub, type) VALUES ($newstoryid,".$this->catid.",".$this->uid.",'".$title."',".$created.",0,'".$this->hostname."',".$this->nohtml.",".$this->nosmiley.",'".$hometext."','".$bodytext."',0,".$this->topicid.",'".$notes."',".$this->ihome.",".$this->notifypub.",'".$this->type."')";
			}
//			echo $sql;
		}else{
			if($this->approved){
				$sql = "UPDATE ".$this->table." SET catid=".$this->catid.",title='".$title."',published=".$this->published.",nohtml=".$this->nohtml.",nosmiley=".$this->nosmiley.",hometext='".$hometext."',bodytext='".$bodytext."',topicid=".$this->topicid.",notes='".$notes."',ihome=".$this->ihome." WHERE storyid=".$this->storyid."";
			} else {
				$sql = "UPDATE ".$this->table." SET catid=".$this->catid.",title='".$title."',nohtml=".$this->nohtml.",nosmiley=".$this->nosmiley.",hometext='".$hometext."',bodytext='".$bodytext."',topicid=".$this->topicid.",notes='".$notes."',ihome=".$this->ihome." WHERE storyid=".$this->storyid."";
			}
//			echo $sql;
		}
		if(!$result = $this->db->query($sql,1)){
//			echo $sql;
			ErrorHandler::show('0022');
		}
		//if($newpost && $this->uid != 0){
		//	XoopsUser::incrementPost($this->uid);
		//}
	}


	function getStory($storyid){
		$sql = "SELECT * FROM ".$this->table." WHERE storyid=".$storyid."";
		//echo $sql;
		$array = $this->db->fetch_array($this->db->query($sql));
		$this->makeStory($array);
	}

	function makeStory($array){
		foreach($array as $key=>$value){
			$this->$key = $value;
		}
	}

	function delete(){
		$sql = "DELETE FROM ".$this->table." WHERE storyid=".$this->storyid."";
		if(!$result = $this->db->query($sql,1)){
			ErrorHandler::show('0022');
		}
		$sql = "UPDATE ".$this->db->prefix("users")." SET counter=counter-1 WHERE uid=".$this->uid."";
		if(!$result = $this->db->query($sql)){
			echo "Could not update user posts.";
			//echo "(".$sql.")";
		}
		//echo $sql;
		$commentsarray = array();
		$com = new XoopsComments($this->db->prefix("comments"),"storyid");
		$com->setItemId($this->storyid);
		$com->setOrder("tid DESC");
		$com->setParent(0);
		$commentsarray = $com->getTopComments();
		foreach($commentsarray as $comment){
			$comment->delete();
		}
		return true;
	}

	function updateCounter(){
		$sql = "UPDATE ".$this->table." SET counter=counter+1 WHERE storyid=".$this->storyid."";
		if(!$result = $this->db->query($sql,1)){
			//echo $sql;
			//ErrorHandler::show('0022');
		}
		return true;
	}

	function catTitle(){
		$myts = new MyTextSanitizer();
		$sql = "SELECT title FROM ".$this->db->prefix("stories_cat")." WHERE catid=".$this->catid."";
		list($title) = $this->db->fetch_row($this->db->query($sql));
		$title = $myts->makeTboxData4Show($title);
		return $title;
	}

	function search($term, $mode, $searchin, $orderby, $topicid="all", $userid=0, $start=0){
		global $xoopsConfig;
		$query = "SELECT s.storyid, s.catid, s.uid, s.title, s.published, s.topicid, u.uname, c.title as cattitle, t.topictext FROM ".$this->table." s LEFT JOIN ".$this->db->prefix("users")." u ON s.uid=u.uid LEFT JOIN ".$this->db->prefix("stories_cat")." c ON c.catid=s.catid LEFT JOIN ".$this->db->prefix("topics")." t ON t.topicid=s.topicid";

		$count_query = "SELECT COUNT(*) FROM ".$this->table." s LEFT JOIN ".$this->db->prefix("users")." u ON s.uid=u.uid LEFT JOIN ".$this->db->prefix("stories_cat")." c ON c.catid=s.catid LEFT JOIN ".$this->db->prefix("topics")." t ON t.topicid=s.topicid";

		if(isset($term) && $term != ""){
			$terms = split(" ",addslashes($term));						// Get all the words into an array
			$addquery .= "((s.hometext LIKE '%$terms[0]%' OR s.bodytext LIKE '%$terms[0]%')";		
			$subquery .= "(s.title LIKE '%$terms[0]%'"; 
	
			if($mode=="any"){
			// AND/OR relates to the ANY or ALL on Search Page
				$andor = "OR";
			} else {
				$andor = "AND";
			}
			for($i=1;$i<sizeof($terms);$i++) {
				$addquery.=" $andor (s.hometext LIKE '%$terms[$i]%' OR s.bodytext LIKE '%$terms[$i]%')";
				$subquery.=" $andor s.title LIKE '%$terms[$i]%'"; 
			}	     
			$addquery.=")";
			$subquery.=")";
		}
		if(isset($topicid) && $topicid!="all"){
			if(isset($addquery)) {
	   			$addquery .= " AND ";
	   			$subquery .= " AND ";
			}
			$addquery .=" s.topicid=$topicid";
			$subquery .=" s.topicid=$topicid";
		}
		if($userid){
   			if(isset($addquery)) {
      				$addquery.=" AND s.uid=$userid";
      				$subquery.=" AND s.uid=$userid";
   			} else {
      				$addquery.=" s.uid=$userid";
      				$subquery.=" s.uid=$userid";
   			}
		}	
		if(isset($addquery)) {
   			switch ($searchin) { 
    				case "both" : 
      					$query .= " WHERE ( $subquery OR $addquery ) AND "; 
					$count_query .= " WHERE ( $subquery OR $addquery ) AND "; 
      					break; 
    				case "title" : 
      					$query .= " WHERE ( $subquery ) AND "; 
					$count_query .= " WHERE ( $subquery ) AND "; 
      					break; 
    				default: 
      					$query .= " WHERE ( $addquery ) AND "; 
					$count_query .= " WHERE ( $addquery ) AND ";
      					break; 
   			}
		} else {
     			$query.=" WHERE ";
			$count_query.=" WHERE ";
		}

   		$query .= " s.published > 0";
		$count_query .= " s.published > 0";

   		$query .= " ORDER BY $orderby";
		if(!$result = $this->db->query($query,0,20,$start)){
			die();
		}
		$rows = array();
		if($this->db->num_rows($result)>0){
			while($row = $this->db->fetch_array($result)){
				if(!$row['uname']){
					$row['uname'] = $xoopsConfig['anonymous'];
				}
				array_push($rows, $row);
			}
		}else{
			return 0;
		}
		list($total_rows) = $this->db->fetch_row($this->db->query($count_query));
		$searchresult=array();
		$searchresult['rows'] = $rows;
		$searchresult['numrows'] = $total_rows;
		return $searchresult;
		
	}

	function topicid(){
		return $this->topicid;
	}

	function topic(){
		$myts = new MyTextSanitizer();
		$ta = $this->db->query("SELECT topictext FROM ".$this->db->prefix("topics")." WHERE topicid=".$this->topicid."");
        	list($topicname) = $this->db->fetch_row($ta);
		return $myts->makeTboxData4Show($topicname);
	}

	function catid(){
		return $this->catid;
	}

	function uid(){
		return $this->uid;
	}

	function title($format="Show"){
		$myts = new MyTextSanitizer;
		$smiley = 1;
		if($this->nosmiley()){
			$smiley = 0;
		}
		switch($format){
			case "Show":
				$title = $myts->makeTboxData4Show($this->title,$smiley);
				break;
			case "Edit":
				$title = $myts->makeTboxData4Edit($this->title);
				break;
			case "Preview":
				$title = $myts->makeTboxData4Preview($this->title,$smiley);
				break;
			case "InForm":
				$title = $myts->makeTboxData4PreviewInForm($this->title);
				break;
		}
		return $title;
	}

	function hometext($format="Show"){
		$myts = new MyTextSanitizer;
		$html = 1;
		$smiley = 1;
		if($this->nohtml()){
			$html = 0;
		}
		if($this->nosmiley()){
			$smiley = 0;
		}
		switch($format){
			case "Show":
				$hometext = $myts->makeTareaData4Show($this->hometext,$html,$smiley);
				break;
			case "Edit":
				$hometext = $myts->makeTareaData4Edit($this->hometext);
				break;
			case "Preview":
				$hometext = $myts->makeTareaData4Preview($this->hometext,$html,$smiley);
				break;
			case "InForm":
				$hometext = $myts->makeTareaData4PreviewInForm($this->hometext);
				break;
		}
		return $hometext;
	}

	function bodytext($format="Show"){
		$myts = new MyTextSanitizer;
		$html = 1;
		$smiley = 1;
		if($this->nohtml()){
			$html = 0;
		}
		if($this->nosmiley()){
			$smiley = 0;
		}
		switch($format){
			case "Show":
				$bodytext = $myts->makeTareaData4Show($this->bodytext,$html,$smiley);
				break;
			case "Edit":
				$bodytext = $myts->makeTareaData4Edit($this->bodytext);
				break;
			case "Preview":
				$bodytext = $myts->makeTareaData4Preview($this->bodytext,$html,$smiley);
				break;
			case "InForm":
				$bodytext = $myts->makeTareaData4PreviewInForm($this->bodytext);
				break;
		}
		return $bodytext;
	}

	function notes($format="Show"){
		$myts = new MyTextSanitizer;
		$html = 1;
		$smiley = 1;
		if($this->nohtml()){
			$html = 0;
		}
		if($this->nosmiley()){
			$smiley = 0;
		}
		switch($format){
			case "Show":
				$notes = $myts->makeTareaData4Show($this->notes,$html,$smiley);
				break;
			case "Edit":
				$notes = $myts->makeTareaData4Edit($this->notes);
				break;
			case "Preview":
				$notes = $myts->makeTareaData4Preview($this->notes,$html,$smiley);
				break;
			case "InForm":
				$notes = $myts->makeTareaData4PreviewInForm($this->notes);
				break;
		}
		return $notes;
	}

	function counter(){
		return $this->counter;
	}

	function created(){
		return $this->created;
	}

	function published(){
		return $this->published;
	}

	function hostname(){
		return $this->hostname;
	}

	function storyid(){
		return $this->storyid;
	}

	function nohtml(){
		return $this->nohtml;
	}

	function nosmiley(){
		return $this->nosmiley;
	}

	function notifypub(){
		return $this->notifypub;
	}

	function type(){
		return $this->type;
	}

	function ihome(){
		return $this->ihome;
	}

	function getAllPublished($limit=0, $asobject=true){
		global $xoopsDB;
		$db =& $xoopsDB;
		$myts= new MyTextSanitizer();
		$ret = array();
		$sql = "SELECT * FROM ".$db->prefix("stories")." WHERE published > 0 AND published <= ".time()." ORDER BY published DESC";
		$result = $xoopsDB->query($sql,1,$limit,0);
		while ( $myrow = $db->fetch_array($result) ) {
			if ( $asobject ) {
				$ret[] = new Story($myrow);
			} else {
				$ret[$myrow['storyid']] = $myts->makeTboxData4Show($myrow['title']);
			}
		}
		return $ret;
	}

	function getAllAutoStory($limit=0, $asobject=true){
		global $xoopsDB;
		$db =& $xoopsDB;
		$myts= new MyTextSanitizer();
		$ret = array();
		$sql = "SELECT * FROM ".$db->prefix("stories")." WHERE published > ".time()." ORDER BY published ASC";
		$result = $xoopsDB->query($sql,1,$limit,0);
		while ( $myrow = $db->fetch_array($result) ) {
			if ( $asobject ) {
				$ret[] = new Story($myrow);
			} else {
				$ret[$myrow['storyid']] = $myts->makeTboxData4Show($myrow['title']);
			}
		}
		return $ret;
	}

	function getAllSubmitted($limit=0, $asobject=true){
		global $xoopsDB;
		$db =& $xoopsDB;
		$myts= new MyTextSanitizer();
		$ret = array();
		$sql = "SELECT * FROM ".$db->prefix("stories")." WHERE published=0 ORDER BY created DESC";
		$result = $xoopsDB->query($sql,1,$limit,0);
		while ( $myrow = $db->fetch_array($result) ) {
			if ( $asobject ) {
				$ret[] = new Story($myrow);
			} else {
				$ret[$myrow['storyid']] = $myts->makeTboxData4Show($myrow['title']);
			}
		}
		return $ret;
	}
}
?>