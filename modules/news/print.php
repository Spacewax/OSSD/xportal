<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
if ( !isset($storyid) ) {
	exit(); 
}
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

function PrintPage($storyid) {
    	global $xoopsConfig, $xoopsDB;
    	$myts = new MyTextSanitizer; // MyTextSanitizer object
    	$result=$xoopsDB->query("SELECT s.title, s.published, s.hometext, s.bodytext, s.topicid, s.notes, t.topictext FROM ".$xoopsDB->prefix("stories")." s LEFT JOIN ".$xoopsDB->prefix("topics")." t ON s.topicid=t.topicid WHERE storyid=$storyid");
    	list($title, $time, $hometext, $bodytext, $topic, $notes, $topictext) = $xoopsDB->fetch_row($result);
    	$datetime = formatTimestamp($time);
    	$title = $myts->makeTboxData4Show($title);
    	$hometext = $myts->makeTareaData4Show($hometext);
   	$bodytext = $myts->makeTareaData4Show($bodytext);
    	$notes = $myts->makeTareaData4Show($notes);
    	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
	echo "<html>\n<head>\n";
	echo "<title>".$xoopsConfig['sitename']."</title>\n";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset="._CHARSET."\"></meta>\n";
	echo "<meta name=\"AUTHOR\" content=\"".$xoopsConfig['sitename']."\"></meta>\n";
	echo "<meta name=\"COPYRIGHT\" content=\"Copyright (c) 2001 by ".$xoopsConfig['sitename']."\"></meta>\n";
	echo "<meta name=\"DESCRIPTION\" content=\"".$xoopsConfig['slogan']."\"></meta>\n";
	echo "<meta name=\"GENERATOR\" content=\"".$xoopsConfig['version']."\"></meta>\n\n\n";
    	echo "<body bgcolor=\"#ffffff\" text=\"#000000\">
    	<table border=\"0\"><tr><td align=\"center\">
    
    	<table border=\"0\" width=\"640\" cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#000000\"><tr><td>
    	<table border=\"0\" width=\"640\" cellpadding=\"20\" cellspacing=\"1\" bgcolor=\"#ffffff\"><tr><td align=\"center\">
    	<img src=\"".$xoopsConfig['xoops_url']."/images/logo.gif\" border=\"0\" alt=\"\" /><br /><br />
    	<h3>$title</h3>
    	<small><b>"._NW_DATE."</b> $datetime<br /><b>"._NW_TOPICC."</b> $topictext</small><br /><br /></td></tr>";
	echo "<tr><td>$hometext<br /><br />";
	if($bodytext){
    		echo $bodytext."<br /><br />";
	}
	echo "<i>".$notes."</i><br /><br />
    	</td></tr></table></td></tr></table>\n";
    	echo "<br /><br />\n";
    	printf(_NW_THISCOMESFROM,$xoopsConfig['sitename']);
	echo "<br /><a href=\"".$xoopsConfig['xoops_url']."/\">".$xoopsConfig['xoops_url']."</a><br /><br />
    	"._NW_URLFORSTORY."<br />
    	<a href=\"".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=$storyid\">".$xoopsConfig['xoops_url']."/article.php?storyid=$storyid</a>
    	</td></tr></table>
    	</body>
    	</html>
    	";
}

PrintPage($storyid);

?>