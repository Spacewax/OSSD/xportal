<?php
$modversion['name'] = _MI_NEWS_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_NEWS_DESC;
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "news.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "images/news_slogo.jpg";
$modversion['dirname'] = "news";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Blocks
$modversion['blocks'][1]['file'] = "news_topics.php";
$modversion['blocks'][1]['name'] = _MI_NEWS_BNAME1;
$modversion['blocks'][1]['description'] = "Shows news topics";
$modversion['blocks'][1]['show_func'] = "b_news_topics_show";
$modversion['blocks'][2]['file'] = "news_categories.php";
$modversion['blocks'][2]['name'] = _MI_NEWS_BNAME2;
$modversion['blocks'][2]['description'] = "Shows news categories";
$modversion['blocks'][2]['show_func'] = "b_news_categories_show";
$modversion['blocks'][3]['file'] = "news_bigstory.php";
$modversion['blocks'][3]['name'] = _MI_NEWS_BNAME3;
$modversion['blocks'][3]['description'] = "Shows most read story of the day";
$modversion['blocks'][3]['show_func'] = "b_news_bigstory_show";
$modversion['blocks'][4]['file'] = "news_top.php";
$modversion['blocks'][4]['name'] = _MI_NEWS_BNAME4;
$modversion['blocks'][4]['description'] = "Shows top read news articles";
$modversion['blocks'][4]['show_func'] = "b_news_top_show";
$modversion['blocks'][4]['edit_func'] = "b_news_top_edit";
$modversion['blocks'][4]['options'] = "counter|10";
$modversion['blocks'][5]['file'] = "news_top.php";
$modversion['blocks'][5]['name'] = _MI_NEWS_BNAME5;
$modversion['blocks'][5]['description'] = "Shows recent articles";
$modversion['blocks'][5]['show_func'] = "b_news_top_show";
$modversion['blocks'][5]['edit_func'] = "b_news_top_edit";
$modversion['blocks'][5]['options'] = "published|10";

// Menu
$modversion['hasMain'] = 1;
$modversion['sub'][1]['name'] = _MI_NEWS_SMNAME1;
$modversion['sub'][1]['url'] = "submit.php";
$modversion['sub'][2]['name'] = _MI_NEWS_SMNAME2;
$modversion['sub'][2]['url'] = "archive.php";

// Search
$modversion['hasSearch'] = 1;
$modversion['search']['file'] = "include/search.inc.php";
$modversion['search']['func'] = "news_search";
?>