<?php  
  /*********************************************/
  /*  PHP TreeMenu 1.1                         */
  /*                                           */
  /*  Author: Bjorge Dijkstra                  */
  /*  email : bjorge@gmx.net                   */
  /*                                           */  
  /*  Placed in Public Domain                  */
  /*                                           */  
  /*********************************************/

  /*********************************************/
  /*  Settings                                 */
  /*********************************************/
  /*                                           */      
  /*  $treefile variable needs to be set in    */
  /*  main file                                */
  /*                                           */ 
  /*********************************************/
  
  /*********************************************/
  /*                                           */
  /* - Multiple root node fix by Dan Howard    */
  /*                                           */
  /*********************************************/

  if(isset($PATH_INFO)) {
	  $script       =  $PATH_INFO; 
  } elseif(isset($SCRIPT_NAME)) {
	  $script	=  $SCRIPT_NAME;
  }else{
	  $script       =  $PHP_SELF;
  }

  $img_expand   = $xoopsConfig['xoops_url']."/images/tree/tree_expand.gif";
  $img_collapse = $xoopsConfig['xoops_url']."/images/tree/tree_collapse.gif";
  $img_line     = $xoopsConfig['xoops_url']."/images/tree/tree_vertline.gif";  
  $img_split	= $xoopsConfig['xoops_url']."/images/tree/tree_split.gif";
  $img_end      = $xoopsConfig['xoops_url']."/images/tree/tree_end.gif";
  $img_leaf     = $xoopsConfig['xoops_url']."/images/tree/tree_leaf.gif";
  $img_spc      = $xoopsConfig['xoops_url']."/images/tree/tree_space.gif";

 
  /*********************************************/
  /*  Read text file with tree structure       */
  /*********************************************/
  
  /*********************************************/
  /* read file to $tree array                  */
  /* tree[x][0] -> tree level                  */
  /* tree[x][1] -> item text                   */
  /* tree[x][2] -> item link                   */
  /* tree[x][3] -> link target                 */
  /* tree[x][4] -> last item in subtree        */
  /*********************************************/

  $maxlevel=0;
  $cnt=0;
  
  $fd = fopen($treefile, "r");
  if ($fd==0) die("treemenu.inc : Unable to open file ".$treefile);
  while ($buffer = fgets($fd, 4096)) 
  {
    $tree[$cnt][0]=strspn($buffer,".");
    $tmp=rtrim(substr($buffer,$tree[$cnt][0]));
    $node=explode("|",$tmp); 
    $tree[$cnt][1]=$node[0];
    $tree[$cnt][2]=$node[1];
    if(isset($node[2])){
    	$tree[$cnt][3]=$node[2];
    }
    $tree[$cnt][4]=0;
    if ($tree[$cnt][0] > $maxlevel) $maxlevel=$tree[$cnt][0];    
    $cnt++;
  }
  fclose($fd);

  for ($i=0; $i<count($tree); $i++) {
     $expand[$i]=0;
     $visible[$i]=0;
     $levels[$i]=0;
  }

  /*********************************************/
  /*  Get Node numbers to expand               */
  /*********************************************/
 $explevels = array();
  if (isset($topicpath) && $topicpath !="") $explevels = explode("|",$topicpath);
  
  $i=0;
  while($i<count($explevels))
  {
    $expand[$explevels[$i]]=1;
    $i++;
  }
  
  /*********************************************/
  /*  Find last nodes of subtrees              */
  /*********************************************/
  
  $lastlevel=$maxlevel;
  for ($i=count($tree)-1; $i>=0; $i--)
  {
     if ( $tree[$i][0] < $lastlevel )
     {
       for ($j=$tree[$i][0]+1; $j <= $maxlevel; $j++)
       {
          $levels[$j]=0;
       }
     }
     if ( $levels[$tree[$i][0]]==0 )
     {
       $levels[$tree[$i][0]]=1;
       $tree[$i][4]=1;
     }
     else
       $tree[$i][4]=0;
     $lastlevel=$tree[$i][0];  
  }
  
  
  /*********************************************/
  /*  Determine visible nodes                  */
  /*********************************************/
  
// all root nodes are always visible
  for ($i=0; $i < count($tree); $i++) if ($tree[$i][0]==1) $visible[$i]=1;


  for ($i=0; $i < count($explevels); $i++)
  {
    $n=$explevels[$i];
    if ( isset($visible[$n]) && isset($expand[$n]) && ($visible[$n]==1) && ($expand[$n]==1) )
    {
       $j=$n+1;
       while ( $tree[$j][0] > $tree[$n][0] )
       {
         if ($tree[$j][0]==$tree[$n][0]+1) $visible[$j]=1;     
         $j++;
       }
    }
  }
  
  
  /*********************************************/
  /*  Output nicely formatted tree             */
  /*********************************************/
  
  for ($i=0; $i<$maxlevel; $i++) $levels[$i]=1;

  $maxlevel++;
  $content = "<table cellspacing=0 cellpadding=0 border=0 cols=".($maxlevel+3)." width=92%>\n";
  $content .= "<tr>";
  for ($i=0; $i<$maxlevel; $i++) $content .= "<td width=16></td>";
  $content .= "<td width=100%>&nbsp;</td></tr>\n";
  $cnt=0;
  while ($cnt<count($tree))
  {
    if ($visible[$cnt])
    {
      /****************************************/
      /* start new row                        */
      /****************************************/      
      $content .= "<tr>";
      
      /****************************************/
      /* vertical lines from higher levels    */
      /****************************************/
      $i=0;
      while ($i<$tree[$cnt][0]-1) 
      {
        if ($levels[$i]==1){
            $content .= "<td><a name='$cnt'></a>";
#	    $content .= "<img src=\"".$img_line."\">";
	    $content .= "</td>";
        }else{
            $content .= "<td>";
	    $content .= "<a name='$cnt'></a>";
#	   $content .= "<img src=\"".$img_spc."\">";
	   $content .= "</td>";
	}
        $i++;
      }
      
      /****************************************/
      /* corner at end of subtree or t-split  */
      /****************************************/         
      if ($tree[$cnt][4]==1) 
      {
#        $content .= "<td>";
#	$content .= "<img src=\"".$img_end."\">";
#	$content .= "</td>";
        $levels[$tree[$cnt][0]-1]=0;
      }
      else
      {
#        $content .= "<td>";
#	$content .= "<img src=\"".$img_split."\">";
#	$content .= "</td>";                  
        $levels[$tree[$cnt][0]-1]=1;    
      } 
      
      /********************************************/
      /* Node (with subtree) or Leaf (no subtree) */
      /********************************************/
      if (isset($tree[$cnt+1][0]) && $tree[$cnt+1][0]>$tree[$cnt][0])
      {
        
        /****************************************/
        /* Create expand/collapse parameters    */
        /****************************************/
        $i=0; $params="?topicpath=";
        while($i<count($expand))
        {
          if ( isset($expand[$i]) && (($expand[$i]==1 && $cnt!=$i) || ($expand[$i]==0 && $cnt==$i)))
          {
            $params=$params.$i;
            $params=$params."|";
          }
          $i++;
        }
               
        if ($expand[$cnt]==0){
            $content .= "<td><a href=\"".$script.$params."\">";
	    $content .= "<img src=\"".$img_expand."\" border=no>";
	    $content .= "</a></td>";
        } else {
            $content .= "<td><a href=\"".$script.$params."\">";
	    $content .= "<img src=\"".$img_collapse."\" border=no>";
	    $content .= "</a></td>";
	}         
      }
      else
      {
        /*************************/
        /* Tree Leaf             */
        /*************************/

         $content .= "<td>";
#	 $content .= "<img src=\"".$img_leaf."\">";
	 $content .= "</td>";         
      }
      
      /****************************************/
      /* output item text                     */
      /****************************************/
      if ($tree[$cnt][2]==""){
          $content .= "<td nowrap colspan=".($maxlevel-$tree[$cnt][0]).">".$tree[$cnt][1]."</td>";
      } else {
	  if (!isset($topicpath)){
		 $topicpath ="";
	  }
          $content .= "<td nowrap colspan=".($maxlevel-$tree[$cnt][0])."><a href=\"".$tree[$cnt][2]."&amp;topicpath=".$topicpath."\"";
	  if(isset($tree[$cnt][3])){
		$content .= "target=\"".$tree[$cnt][3]."";
	  }
	  $content .= "\">".$tree[$cnt][1]."</a></td>";
      } 
      /****************************************/
      /* end row                              */
      /****************************************/
              
      $content .= "</tr>\n";      
    }
    $cnt++;    
  }
  $content .= "</table>\n";
?>