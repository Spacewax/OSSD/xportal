<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //


function HTMLbuttons(){
echo "<script type=\"text/javascript\"><!--\n\n";
echo "function x () {\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "function DoPrompt1(action) {\n";
echo "var revisedMessage;\n";
echo "var currentMessage = document.coolsus.story.value;\n";
echo "\n";
echo "if (action == \"url\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the link you want to add.\", \"http://\");\n";
echo "var thisTitle = prompt(\"Enter the web site title\", \"Page Title\");\n";
echo "var urlBBCode = \"<a href=\"+thisURL+\" target=blank>\"+thisTitle+\"</a>\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";
echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"image\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the image you want to add.\", \"http://\");\n";
echo "var urlBBCode = \"<img src=\"+thisURL+\" />\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";




echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"email\") {\n";
echo "var thisEmail = prompt(\"Enter the email address you want to add.\", \"\");\n";
echo "var emailBBCode = \"<a href=mailto:\"+thisEmail+\">\"+thisEmail+\"</a>\";\n";
echo "revisedMessage = currentMessage+emailBBCode;\n";




echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"bold\") {\n";
echo "var thisBold = prompt(\"Enter the text that you want to make bold.\", \"\");\n";
echo "var boldBBCode = \"<b>\"+thisBold+\"</b>\";\n";
echo "revisedMessage = currentMessage+boldBBCode;\n";




echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"italic\") {\n";
echo "var thisItal = prompt(\"Enter the text that you want to make italic.\", \"\");\n";
echo "var italBBCode = \"<i>\"+thisItal+\"</i>\";\n";
echo "revisedMessage = currentMessage+italBBCode;\n";



echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"quote\") {\n";
echo "var quoteBBCode = \"<blockquote><i> </i></blockquote>\";\n";
echo "revisedMessage = currentMessage+quoteBBCode;\n";



echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listopen\") {\n";
echo "var liststartBBCode = \"<ul>\";\n";
echo "revisedMessage = currentMessage+liststartBBCode;\n";



echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listclose\") {\n";
echo "var listendBBCode = \"</ul>\";\n";
echo "revisedMessage = currentMessage+listendBBCode;\n";




echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listitem\") {\n";
echo "var thisItem = prompt(\"Enter the new list item. Note that each list group must be preceeded by a List Close and must be ended with List Close.\", \"\");\n";
echo "var itemBBCode = \"<li>\"+thisItem+\"</li>\";\n";
echo "revisedMessage = currentMessage+itemBBCode;\n";
echo "document.coolsus.story.value=revisedMessage;\n";
echo "document.coolsus.story.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "}\n";
echo "//--></script>\n";
echo "\n\n\n";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('url');\">".GetImgTag("images/forum","b_url.gif","Web Address",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('image');\">".GetImgTag("images/forum","b_image.gif","Image",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('email');\">".GetImgTag("images/forum","b_email.gif","Email Address",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('bold');\">".GetImgTag("images/forum","b_bold.gif","Bold Text",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('italic');\">".GetImgTag("images/forum","b_italic.gif","Italic Text",0,83,21)."</a><br />";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('quote');\">".GetImgTag("images/forum","b_quote.gif","Quote",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('listopen');\">".GetImgTag("images/forum","b_listopen.gif","Open List",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('listitem');\">".GetImgTag("images/forum","b_listitem.gif","List Item",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('listclose');\">".GetImgTag("images/forum","b_listclose.gif","Close List",0,83,21)."</a>";
}

function HTMLbuttons_hometext(){
echo "<script type=\"text/javascript\"><!--\n\n";
echo "function x () {\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "function DoPrompt1(action) {\n";
echo "var revisedMessage;\n";
echo "var currentMessage = document.coolsus.hometext.value;\n";
echo "\n";
echo "if (action == \"url\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the link you want to add.\", \"http://\");\n";
echo "var thisTitle = prompt(\"Enter the web site title\", \"Page Title\");\n";
echo "var urlBBCode = \"<a href=\"+thisURL+\" target=blank>\"+thisTitle+\"</a>\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";
echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"image\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the image you want to add.\", \"http://\");\n";
echo "var urlBBCode = \"<img src=\"+thisURL+\" />\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";




echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
      echo "if (action == \"email\") {\n";
echo "var thisEmail = prompt(\"Enter the email address you want to add.\", \"\");\n";
echo "var emailBBCode = \"<a href=mailto:\"+thisEmail+\">\"+thisEmail+\"</a>\";\n";
echo "revisedMessage = currentMessage+emailBBCode;\n";




echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"bold\") {\n";
echo "var thisBold = prompt(\"Enter the text that you want to make bold.\", \"\");\n";
echo "var boldBBCode = \"<b>\"+thisBold+\"</b>\";\n";
echo "revisedMessage = currentMessage+boldBBCode;\n";




echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"italic\") {\n";
echo "var thisItal = prompt(\"Enter the text that you want to make italic.\", \"\");\n";
echo "var italBBCode = \"<i>\"+thisItal+\"</i>\";\n";
echo "revisedMessage = currentMessage+italBBCode;\n";



echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"quote\") {\n";
echo "var quoteBBCode = \"<blockquote><i> </i></blockquote>\";\n";
echo "revisedMessage = currentMessage+quoteBBCode;\n";



echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listopen\") {\n";
echo "var liststartBBCode = \"<ul>\";\n";
echo "revisedMessage = currentMessage+liststartBBCode;\n";



echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listclose\") {\n";
echo "var listendBBCode = \"</ul>\";\n";
echo "revisedMessage = currentMessage+listendBBCode;\n";




echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listitem\") {\n";
echo "var thisItem = prompt(\"Enter the new list item. Note that each list group must be preceeded by a List Close and must be ended with List Close.\", \"\");\n";
echo "var itemBBCode = \"<li>\"+thisItem+\"</li>\";\n";
echo "revisedMessage = currentMessage+itemBBCode;\n";
echo "document.coolsus.hometext.value=revisedMessage;\n";
echo "document.coolsus.hometext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "}\n";
echo "//--></script>\n";
echo "\n\n\n";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('url');\">".GetImgTag("images/forum","b_url.gif","Web Address",0,83,21)."</a>";
      echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('image');\">".GetImgTag("images/forum","b_image.gif","Image",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('email');\">".GetImgTag("images/forum","b_email.gif","Email Address",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('bold');\">".GetImgTag("images/forum","b_bold.gif","Bold Text",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('italic');\">".GetImgTag("images/forum","b_italic.gif","Italic Text",0,83,21)."</a><br />";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('quote');\">".GetImgTag("images/forum","b_quote.gif","Quote",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('listopen');\">".GetImgTag("images/forum","b_listopen.gif","Open List",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('listitem');\">".GetImgTag("images/forum","b_listitem.gif","List Item",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt1('listclose');\">".GetImgTag("images/forum","b_listclose.gif","Close List",0,83,21)."</a>";
}
function HTMLbuttons_bodytext() {
echo "<script type=\"text/javascript\"><!--\n\n";
echo "function x () {\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "function DoPrompt2(action) {\n";
echo "var revisedMessage;\n";
echo "var currentMessage = document.coolsus.bodytext.value;\n";
echo "\n";
echo "if (action == \"url\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the link you want to add.\", \"http://\");\n";
echo "var thisTitle = prompt(\"Enter the web site title\", \"Page Title\");\n";
echo "var urlBBCode = \"<a href=\"+thisURL+\" target=blank>\"+thisTitle+\"</a>\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";
echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"image\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the image you want to add.\", \"http://\");\n";
echo "var urlBBCode = \"<img src=\"+thisURL+\" />\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";




echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
      echo "if (action == \"email\") {\n";
echo "var thisEmail = prompt(\"Enter the email address you want to add.\", \"\");\n";
echo "var emailBBCode = \"<a href=mailto:\"+thisEmail+\">\"+thisEmail+\"</a>\";\n";
echo "revisedMessage = currentMessage+emailBBCode;\n";




echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"bold\") {\n";
echo "var thisBold = prompt(\"Enter the text that you want to make bold.\", \"\");\n";
echo "var boldBBCode = \"<b>\"+thisBold+\"</b>\";\n";
echo "revisedMessage = currentMessage+boldBBCode;\n";




echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"italic\") {\n";
echo "var thisItal = prompt(\"Enter the text that you want to make italic.\", \"\");\n";
echo "var italBBCode = \"<i>\"+thisItal+\"</i>\";\n";
echo "revisedMessage = currentMessage+italBBCode;\n";



echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"quote\") {\n";
echo "var quoteBBCode = \"<blockquote><i> </i></blockquote>\";\n";
echo "revisedMessage = currentMessage+quoteBBCode;\n";



echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listopen\") {\n";
echo "var liststartBBCode = \"<ul>\";\n";
echo "revisedMessage = currentMessage+liststartBBCode;\n";



echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listclose\") {\n";
echo "var listendBBCode = \"</ul>\";\n";
echo "revisedMessage = currentMessage+listendBBCode;\n";




echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listitem\") {\n";
echo "var thisItem = prompt(\"Enter the new list item. Note that each list group must be preceeded by a List Close and must be ended with List Close.\", \"\");\n";
echo "var itemBBCode = \"<li>\"+thisItem+\"</li>\";\n";
echo "revisedMessage = currentMessage+itemBBCode;\n";
echo "document.coolsus.bodytext.value=revisedMessage;\n";
echo "document.coolsus.bodytext.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "}\n";
echo "//--></script>\n";
echo "\n\n\n";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('url');\">".GetImgTag("images/forum","b_url.gif","Web Address",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('email');\">".GetImgTag("images/forum","b_email.gif","Email Address",0,83,21)."</a>";
      echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('image');\">".GetImgTag("images/forum","b_image.gif","Image",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('bold');\">".GetImgTag("images/forum","b_bold.gif","Bold Text",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('italic');\">".GetImgTag("images/forum","b_italic.gif","Italic Text",0,83,21)."</a><br />";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('quote');\">".GetImgTag("images/forum","b_quote.gif","Quote",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('listopen');\">".GetImgTag("images/forum","b_listopen.gif","Open List",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('listitem');\">".GetImgTag("images/forum","b_listitem.gif","List Item",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt2('listclose');\">".GetImgTag("images/forum","b_listclose.gif","Close List",0,83,21)."</a>";
}
function HTMLbuttons_notes() {
echo "<script type=\"text/javascript\"><!--\n\n";
echo "function x () {\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "function DoPrompt3(action) {\n";
echo "var revisedMessage;\n";
echo "var currentMessage = document.coolsus.notes.value;\n";
echo "\n";
echo "if (action == \"url\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the link you want to add.\", \"http://\");\n";
echo "var thisTitle = prompt(\"Enter the web site title\", \"Page Title\");\n";
echo "var urlBBCode = \"<a href=\"+thisURL+\" target=blank>\"+thisTitle+\"</a>\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";
echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"image\") {\n";
echo "var thisURL = prompt(\"Enter the URL for the image you want to add.\", \"http://\");\n";
echo "var urlBBCode = \"<img src=\"+thisURL+\" />\";\n";
echo "revisedMessage = currentMessage+urlBBCode;\n";




echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
      echo "if (action == \"email\") {\n";
echo "var thisEmail = prompt(\"Enter the email address you want to add.\", \"\");\n";
echo "var emailBBCode = \"<a href=mailto:\"+thisEmail+\">\"+thisEmail+\"</a>\";\n";
echo "revisedMessage = currentMessage+emailBBCode;\n";




echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"bold\") {\n";
echo "var thisBold = prompt(\"Enter the text that you want to make bold.\", \"\");\n";
echo "var boldBBCode = \"<b>\"+thisBold+\"</b>\";\n";
echo "revisedMessage = currentMessage+boldBBCode;\n";




echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"italic\") {\n";
echo "var thisItal = prompt(\"Enter the text that you want to make italic.\", \"\");\n";
echo "var italBBCode = \"<i>\"+thisItal+\"</i>\";\n";
echo "revisedMessage = currentMessage+italBBCode;\n";



echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"quote\") {\n";
echo "var quoteBBCode = \"<blockquote><i> </i></blockquote>\";\n";
echo "revisedMessage = currentMessage+quoteBBCode;\n";



echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listopen\") {\n";
echo "var liststartBBCode = \"<ul>\";\n";
echo "revisedMessage = currentMessage+liststartBBCode;\n";



echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listclose\") {\n";
echo "var listendBBCode = \"</ul>\";\n";
echo "revisedMessage = currentMessage+listendBBCode;\n";




echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "if (action == \"listitem\") {\n";
echo "var thisItem = prompt(\"Enter the new list item. Note that each list group must be preceeded by a List Close and must be ended with List Close.\", \"\");\n";
echo "var itemBBCode = \"<li>\"+thisItem+\"</li>\";\n";
echo "revisedMessage = currentMessage+itemBBCode;\n";
echo "document.coolsus.notes.value=revisedMessage;\n";
echo "document.coolsus.notes.focus();\n";
echo "return;\n";
echo "}\n";
echo "\n";
echo "}\n";
echo "//--></script>\n";
echo "\n\n\n";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('url');\">".GetImgTag("images/forum","b_url.gif","Web Address",0,83,21)."</a>";
      echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('image');\">".GetImgTag("images/forum","b_image.gif","Image",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('email');\">".GetImgTag("images/forum","b_email.gif","Email Address",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('bold');\">".GetImgTag("images/forum","b_bold.gif","Bold Text",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('italic');\">".GetImgTag("images/forum","b_italic.gif","Italic Text",0,83,21)."</a><br />";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('quote');\">".GetImgTag("images/forum","b_quote.gif","Quote",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('listopen');\">".GetImgTag("images/forum","b_listopen.gif","Open List",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('listitem');\">".GetImgTag("images/forum","b_listitem.gif","List Item",0,83,21)."</a>";
echo "<a href=\"javascript: x()\" onClick=\"DoPrompt3('listclose');\">".GetImgTag("images/forum","b_listclose.gif","Close List",0,83,21)."</a>";
}

?>