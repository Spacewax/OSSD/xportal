<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
if ( !isset($submit_page) ) {
	$submit_page = $PHP_SELF;
}
?>
<table><tr><td>
<form action="<?php echo $submit_page;?>" method="post" name="coolsus" onSubmit="return validate(this)">
<br /><br /><B><?php echo _NW_YOURNAME;?>:</B>
<?php
if ( $xoopsUser ) {
	echo "<a href=\"".$xoopsConfig['xoops_url']."userinfo.php?uid=".$xoopsUser->uid()."\">".$xoopsUser->uname()."</a>&nbsp;[&nbsp;<a href=\"".$xoopsConfig['xoops_url']."user.php?op=logout\">"._NW_LOGOUT."</a>&nbsp;]";
}else{
	echo "<b>".$xoopsConfig['anonymous']."</b>";
	echo " [ <a href=\"".$xoopsConfig['xoops_url']."register.php\">"._NW_REGISTER."</a> ] ";	
}
echo "<br /><br />";
echo "<b>"._NW_TITLE."</b>&nbsp;";
echo "("._NW_BECLEAR.")<br />";
echo "<input class=\"textbox\" type=\"text\" name=\"subject\" value=\"".$subject."\" size=\"50\" maxlength=\"80\" /><br />("._NW_BADTITLES.")";
echo "<br /><p>";
echo "<b>"._NW_TOPIC."</b>&nbsp;";
if ( isset($topicid) ) {
	$mf->makeMySelBox("topictext", "topictext", $topicid);
} else {
	$mf->makeMySelBox("topictext", "topictext");
}
echo "</p><p><b>"._NW_THESCOOP."</b>\n";
echo "("._NW_HTMLISFINE.")<br />\n";
echo "<textarea class=\"textbox\" wrap=\"virtual\" cols=\"50\" rows=\"12\" name=\"message\">$message</textarea></p>\n";
putitems();
echo "&nbsp;[<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/misc.php?action=showpopups&type=smilies','smilies',300,400);\">"._NW_MORE."</a>]";
echo "<br /><br />\n";
echo "<a href=\"javascript:checklength(document.coolsus);\">"._NW_CHKMSGLGTH."</a><br /><br />\n";
echo _NW_ALLOWEDHTML;
echo "<br />";
echo get_allowed_html();
echo "<br />";
if ( $xoopsUser && $xoopsConfig['anonpost'] ) { 
	echo "<br /><input type=\"checkbox\" name=\"noname\" /> "._NW_POSTANON.""; 
}

echo "<br /><input type=\"checkbox\" name=\"nosmiley\" value=\"1\"";
if ( $nosmiley ) {
	echo " checked=\"checked\"";
}
echo " /> "._NW_DISABLESMILEY."<br />";
echo "<input type=\"checkbox\" name=\"nohtml\" value=\"1\"";
if ( $nohtml ) {
	echo " checked=\"checked\"";
}
echo " /> "._NW_DISABLEHTML."<br />";
echo "<input type=\"checkbox\" name=\"notifypub\" value=\"1\"";
if ( $notifypub ) {
	echo " checked=\"checked\"";
}
echo " /> "._NW_NOTIFYPUBLISH."<br />";
echo "<select name=\"op\">\n";
echo "<option value=\"preview\" SELECTED>"._NW_PREVIEW."</option>\n";
echo "<option value=\"post\">"._NW_POST."</option>\n";
echo "</select>";
echo "<input type=\"submit\" value=\""._NW_GO."\" />\n";
echo "</form>";
echo "</td></tr></table>";

unset($submit_page);

function putitems() {
	global $xoopsConfig;
	$smileyPath = "images/smilies";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' :-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_smile.gif' border='0' alt=':-)' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' :-( ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_frown.gif' border='0' alt=':-(' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' :-D ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_biggrin.gif' border='0' alt=':-D' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' ;-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_wink.gif' border='0' alt=';-)' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' :-o ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_eek.gif' border='0' alt=':-o' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' 8-) ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_cool.gif' border='0' alt='8-)' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' :-? ');\"><img width='15' height='22' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_confused.gif' border='0' alt=':-?' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' :-P ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_razz.gif' border='0' alt=':-P' /></a>";
	echo "<a href='javascript: x()' onClick=\"DoSmilie(' :-x ');\"><img width='15' height='15' src='".$xoopsConfig['xoops_url']."/".$smileyPath."/icon_mad.gif' border='0' alt=':-x' /></a>";
		
}
?>