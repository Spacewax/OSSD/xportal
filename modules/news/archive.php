<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
######################################################################
# myPHP-NUKE: Web Portal System: Archive add-on
# ==--=========================================
#
# This module is to view your news items archived by month, year
# in cronological order.
#
# Original version:
# [11-may-2001] Kenneth Lee - http://www.nexgear.com/
#
######################################################################

include("header.php");
include($xoopsConfig['root_path']."header.php");
OpenTable();
include_once("class/class.story.php");

$count = 0;
$altbg = 0;
$fromyear = "0000";
$frommonth = "00";
$lastyear = "0000";
$lastmonth = "00";

switch($op) {
	case "get":
		$fromyear = $year;
		$frommonth = $month;
		break;
}
$useroffset = "";
if($xoopsUser){
	$timezone = $xoopsUser->timezone();
	if(isset($timezone)){
		$useroffset = $xoopsUser->timezone();
	}else{
		$useroffset = $xoopsConfig['default_TZ'];
	}
}

print ("<table border=\"0\" width=\"100%\"><tr><td>\n");

$result=$xoopsDB->query("SELECT published FROM ".$xoopsDB->prefix("stories")." WHERE published>0 AND published<=".time()." ORDER BY published DESC");
if(!$result) {
	echo $xoopsDB->errno(). ": ".$xoopsDB->error(). "<br />"; exit();
} else {
	echo "<b>"._NW_NEWSARCHIVES."</b><br /><br />\n";
	while (list($time) = $xoopsDB->fetch_row($result)) {
	$time = formatTimestamp($time,"mysql",$useroffset);
        ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})", $time, $datetime);
		if (($lastyear!=$datetime[1]) OR ($lastmonth!=$datetime[2])){
			$lastmonth = $datetime[2];
			$lastyear = $datetime[1];
			print ("<img src=\"images/size.gif\" border=\"0\" alt=\"\" align=\"middle\" />&nbsp;<a href=\"archive.php?op=get&amp;year=".$lastyear."&amp;month=".$lastmonth."\">");
			switch($lastmonth) { 
				case "01":print _NW_JANUARY;break;
				case "02":print _NW_FEBRUARY;break;
				case "03":print _NW_MARCH;break;
				case "04":print _NW_APRIL;break;
				case "05":print _NW_MAY;break;
				case "06":print _NW_JUNE;break;
				case "07":print _NW_JULY;break;
				case "08":print _NW_AUGUST;break;
				case "09":print _NW_SEPTEMBER;break;
				case "10":print _NW_OCTOBER;break;
				case "11":print _NW_NOVEMBER;break;
				case "12":print _NW_DECEMBER;break;
			}
			print (", $lastyear</a><br />\n");
		}
	}
}

print ("</td></tr></table>\n");

if ($fromyear != "0000") {
	print ("<br />\n");
	print ("<table border=\"0\" width=\"100%\"><tr><td>\n");
	echo "<table width=\"100%\" border=\"0\" bordercolor=\"".$xoopsTheme['bgcolor4']."\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"".$xoopsTheme['bgcolor3']."\">\n";
	echo "<tr align=\"center\" bgcolor=\"".$xoopsTheme['bgcolor2']."\"><td align=\"left\"><b>&nbsp;"._NW_ARTICLES." ( ";
	switch($frommonth) { 
		case "01":print _NW_JANUARY;break;
		case "02":print _NW_FEBRUARY;break;
		case "03":print _NW_MARCH;break;
		case "04":print _NW_APRIL;break;
		case "05":print _NW_MAY;break;
		case "06":print _NW_JUNE;break;
		case "07":print _NW_JULY;break;
		case "08":print _NW_AUGUST;break;
		case "09":print _NW_SEPTEMBER;break;
		case "10":print _NW_OCTOBER;break;
		case "11":print _NW_NOVEMBER;break;
		case "12":print _NW_DECEMBER;break;
	}
	print (" $fromyear )");

	echo "&nbsp;</b></td><td align=\"center\"><b>&nbsp;"._NW_ACTIONS."&nbsp;</b></td><td><b>&nbsp;"._NW_VIEWS."&nbsp;</b></td><td><b>&nbsp;"._NW_DATE."&nbsp;</b></td></tr>\n";
	// must adjust the selected time to server timestamp
	$timeoffset = $useroffset - $xoopsConfig['server_TZ'];
	$monthstart = mktime(0-$timeoffset,0,0,$frommonth,1,$fromyear);
	$monthend = mktime(23-$timeoffset,59,59,$frommonth+1,0,$fromyear);
	$sql = "SELECT * FROM ".$xoopsDB->prefix("stories")." WHERE published > $monthstart and published < $monthend ORDER by published DESC";
	//echo $sql;
    	$result = $xoopsDB->query($sql,1);
    	if(!$result) {
		echo $xoopsDB->errno(). ": ".$xoopsDB->error(). "<br />"; exit();
    	}
    	while ($myrow = $xoopsDB->fetch_array($result)) {
		$article = new Story($myrow);
		$printP = "<a href=\"print.php?storyid=".$article->storyid()."\"><img src=\"images/print.gif\" border=\"0\" alt=\""._NW_PRINTERFRIENDLY."\" width=\"15\" height=\"11\" /></a>&nbsp;";
		$sendF = "<a target='_top' href='mailto:?subject=".sprintf(_NW_INTARTICLE,$xoopsConfig['sitename'])."&body=".sprintf(_NW_INTARTFOUND,$xoopsConfig['sitename']).":  ".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$article->storyid()."'><img src=\"images/friend.gif\" border=\"0\" alt=\""._NW_SENDSTORY."\" width=\"15\" height=\"11\" /></a>";
		$storytitle = $article->title();
		if ($article->catid()) {
	    		$cattitle = $article->catTitle();
	    		$storytitle = "<a href=\"index.php?storycat=".$article->catid()."\">".$cattitle."</a>: <a href=\"article.php?storyid=".$article->storyid()."\">".$storytitle."</a>";
		}

		if ($altbg==0) {
			print ("<tr align=\"right\" bgcolor=\"".$xoopsTheme['bgcolor1']."\">");
			$altbg=1;
		} else {
			print ("<tr align=\"right\">");
			$altbg=0;
		}
		print ("<td><a href=\"article.php?storyid=".$article->storyid()."\">".$storytitle."&nbsp;</td><td align=\"center\">$printP&nbsp;$sendF</td><td align=\"center\"><small>".$article->counter()."</small></td><td align=\"center\"><small>".formatTimestamp($article->published(),"m",$useroffset)."</small></td></tr>\n");
		$count ++;
	}
echo "</table></td></tr></table>\n";
echo "<div align=\"center\"><small>";
printf(_NW_THEREAREINTOTAL,$count);
echo "</small></div>\n";
}

CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>