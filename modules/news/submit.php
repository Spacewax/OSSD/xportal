<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once("class/class.story.php");
include("cache/config.php");
switch($op){
	default:
		$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
		$xoopsOption['forumpage']=1;
		include ($xoopsConfig['root_path']."header.php");
		OpenTable();
		echo "<h3>"._NW_SUBMITNEWS."</h3>";
		$submit_page = "submit.php";
		include("include/storyform.inc.php");
		CloseTable();
		include ($xoopsConfig['root_path']."footer.php");
		break;

	case "preview":
		$myts = new MyTextSanitizer; // MyTextSanitizer object
		$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
		$xoopsOption['forumpage']=1;
		include ($xoopsConfig['root_path']."header.php");
		OpenTable();
		$p_subject = $myts->makeTboxData4Preview($subject);
  		if($nosmiley && $nohtml){
			$p_message = $myts->makeTareaData4Preview($message,0);
		}elseif($nohtml){
			$p_message = $myts->makeTareaData4Preview($message,0,1);
		}elseif($nosmiley){
			$p_message = $myts->makeTareaData4Preview($message);
		}else{
			$p_message = $myts->makeTareaData4Preview($message,1,1);
		}
		$subject = $myts->makeTboxData4PreviewInForm($subject);
  		$message = $myts->makeTareaData4PreviewInForm($message);
		echo "<b>"._NW_SUBMITNEWS."</b><br><br>";

		OpenTable("75%");
		if ( !empty($topicid) ) {
	    		$result = $xoopsDB->query("SELECT topicimage FROM ".$xoopsDB->prefix("topics")." WHERE topicid=".$topicid."");
	    		list($topicimage) = $xoopsDB->fetch_row($result);
		}
		echo GetImgTag($xoops_newsConfig['tipath'],$topicimage," ",0,"right");

		echo "<p><b>".$p_subject."</b><br /><br />".$p_message."<br /><br /></p>";
		CloseTable();
		$submit_page = "submit.php";
		include("include/storyform.inc.php");
		CloseTable();
		include ($xoopsConfig['root_path']."footer.php");
		break;

	case "post":

		if($xoopsUser){
			$uid = $xoopsUser->uid();
		}else{
			if($xoopsConfig['anonpost']){
				$uid = 0;
			}else{
				redirect_header("index.php",3,_NW_ANONNOTALLOWED);
				exit();
			}
		}

		$story = new Story(); 
		$story->setTitle($subject);
		$story->setHometext($message);
		$story->setUid($uid);
		$story->setTopicId($topicid);
		$story->setHostname(getenv("REMOTE_ADDR"));
		$story->setNohtml($nohtml);
		$story->setNosmiley($nosmiley);
		$story->setNotifyPub($notifypub);
		$story->setType("user");
		$story->store();
		if($xoops_newsConfig['notifysubmit']) {
			mail($xoopsConfig['adminmail'], _NW_NOTIFYSBJCT, _NW_NOTIFYMSG, "From: Webmaster\nX-Mailer: PHP/" . phpversion());
		}
		redirect_header("index.php",2,_NW_THANKS);
		break;
}

?>