<?php
// Blocks
define("_MB_NEWS_TITLE1","Today's Big Story");
define("_MB_NEWS_TITLE2","News Categories");
define("_MB_NEWS_TITLE3","News Topics");
define("_MB_NEWS_TITLE4","Recent News");
define("_MB_NEWS_TITLE5","Top Read News");
define("_MB_NEWS_NOTYET","There isn't a Biggest Story for Today, yet.");
define("_MB_NEWS_TMRSI","Today's most read Story is:");
define("_MB_NEWS_ORDER","Order");
define("_MB_NEWS_DATE","Published Date");
define("_MB_NEWS_HITS","Number of Hits");
define("_MB_NEWS_DISP","Display");
define("_MB_NEWS_ARTCLS","articles");
?>