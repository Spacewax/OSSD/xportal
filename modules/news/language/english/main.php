<?php
//%%%%%%		File Name index.php 		%%%%%
define("_NW_PRINTER","Printer Friendly Page");
define("_NW_SENDSTORY","Send this Story to a Friend");
define("_NW_READMORE","Read More...");
define("_NW_COMMENTS","Comments?");
define("_NW_ONECOMMENT","1 comment");
define("_NW_BYTESMORE","%s bytes more");
define("_NW_NUMCOMMENTS","%s comments");
define("_NW_SHOWNUMSTORIES","Show %s Stories");
define("_NW_PREVPAGE","< Previous Page");
define("_NW_NEXTPAGE","Next Page >");
define("_NW_RETURNTOP","<< Return to Top");

//%%%%%%		File Name article.php 		%%%%%
define("_NW_NOTES","<b>Notes</b>"); // Notes from admin
define("_NW_RELATEDLINKS","Related Links");

// %s represents a user name
define("_NW_NEWSBY","News by %s");

// %s represents a topic name
define("_NW_MOREABOUT","More about %s");
define("_NW_MOSTREAD","Most read story about %s");
define("_NW_LASTNEWS","Last news about %s");

define("_NW_TOP","Top");
define("_NW_PARENT","Parent");
define("_NW_POSTERC","Poster:");
define("_NW_DATEC","Date:");
define("_NW_EDITNOTALLOWED","You're not allowed to edit this comment!");
define("_NW_ANONNOTALLOWED","Anonymous user not allowed to post.");
define("_NW_THANKSFORPOST","Thanks for your submission!"); //submission of news comments
define("_NW_DELNOTALLOWED","You're not allowed to delete this comment!");
define("_NW_GOBACK","Go Back");
define("_NW_AREUSUREDEL","Are you sure you want to delete this comment and all its child comments?");
define("_NW_YES","Yes");
define("_NW_NO","No");
define("_PL_COMMENTSDEL","Comment(s) Deleted Successfully!");

//%%%%%%		File Name submit.php		%%%%%
define("_NW_SUBMITNEWS","Submit News");
define("_NW_YOURNAME","Your Name");
define("_NW_LOGOUT","Logout");
define("_NW_MORE","more"); // "more" smilies
define("_NW_REGISTER","Register");
define("_NW_TITLE","Title");
define("_NW_BECLEAR","Be Descriptive, Clear and Simple");
define("_NW_BADTITLES","bad titles='Check This Out!' or 'An Article'.");
define("_NW_TOPIC","Topic");
define("_NW_HTMLISFINE","HTML is fine, but double check those URLs and HTML tags!");
define("_NW_THESCOOP","The Scoop");
define("_NW_CHKMSGLGTH","[check message length]");
define("_NW_ALLOWEDHTML","Allowed HTML:");
define("_NW_POSTANON","Post Anonymously");
define("_NW_DISABLESMILEY","Disable Smiley");
define("_NW_DISABLEHTML","Disable html");
define("_NW_NOTIFYPUBLISH","Notify by mail when published");
define("_NW_POST","Post");
define("_NW_PREVIEW","Preview");
define("_NW_GO","Go!");
define("_NW_THANKS","Thanks for your submission."); //submission of news article

define("_NW_NOTIFYSBJCT","NEWS for my site"); // Notification mail subject
define("_NW_NOTIFYMSG","Hey! You got a new submission for your site."); // Notification mail message

//%%%%%%		File Name archive.php		%%%%%
define("_NW_NEWSARCHIVES","News Archives");
define("_NW_ARTICLES","Articles");
define("_NW_VIEWS","Views");
define("_NW_DATE","Date");
define("_NW_ACTIONS","Actions");
define("_NW_PRINTERFRIENDLY","Printer Friendly Page");
define("_NW_JANUARY","January");
define("_NW_FEBRUARY","February");
define("_NW_MARCH","March");
define("_NW_APRIL","April");
define("_NW_MAY","May");
define("_NW_JUNE","June");
define("_NW_JULY","July");
define("_NW_AUGUST","August");
define("_NW_SEPTEMBER","September");
define("_NW_OCTOBER","October");
define("_NW_NOVEMBER","November");
define("_NW_DECEMBER","December");
define("_NW_THEREAREINTOTAL","There are %s article(s) in total");

// %s is your site name
define("_NW_INTARTICLE","Interesting Article at %s");
define("_NW_INTARTFOUND","Here is an interesting article I have found at %s");

define("_NW_TOPICC","Topic:");
define("_NW_URL","URL:");
define("_NW_NOSTORY","Sorry, the selected story does not exist.");

//%%%%%%	File Name print.php 	%%%%%

define("_NW_URLFORSTORY","The URL for this story is:");

// %s represents your site name
define("_NW_THISCOMESFROM","This article comes from %s");
?>