<?php
// Module Info

// The name of this module
define("_MI_NEWS_NAME","News");

// A brief description of this module
define("_MI_NEWS_DESC","Creates a Slashdot-like news section, where users can post news/comments.");

// Names of blocks for this module (Not all module has blocks)
define("_MI_NEWS_BNAME1","News Topics Block");
define("_MI_NEWS_BNAME2","News Categories Block");
define("_MI_NEWS_BNAME3","Big Story Block");
define("_MI_NEWS_BNAME4","Top News Block");
define("_MI_NEWS_BNAME5","Recent News Block");

define("_MI_NEWS_SMNAME1","Submit News");
define("_MI_NEWS_SMNAME2","Archive");
?>