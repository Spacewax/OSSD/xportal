<?php
//%%%%%%	Admin Module Name  Articles 	%%%%%
define("_AM_DBUPDATED","Database Updated Successfully!");

define("_AM_AUTOARTICLES","Automated Articles");
define("_AM_STORYID","Stoty ID");
define("_AM_TITLE","Title");
define("_AM_TOPIC","Topic");
define("_AM_POSTER","Poster");
define("_AM_PROGRAMMED","Programmed Date/Time");
define("_AM_ACTION","Action");
define("_AM_EDIT","Edit");
define("_AM_DELETE","Delete");
define("_AM_LAST10ARTS","Last 10 Articles");
define("_AM_PUBLISHED","Published"); // Published Date
define("_AM_GO","Go!");
define("_AM_EDITARTICLE","Edit Article");
define("_AM_POSTNEWARTICLE","Post New Article");
define("_AM_ARTPUBLISHED","Your article has been published!");
define("_AM_HELLO","Hello %s,");
define("_AM_YOURARTPUB","Your article submitted on our site has been published.");
define("_AM_TITLEC","Title: ");
define("_AM_URLC","URL: ");
define("_AM_PUBLISHEDC","Published: ");
define("_AM_GMT","(GMT)");
define("_AM_RUSUREDEL","Are you sure you want to delete this comment and all its child comments?");
define("_AM_YES","Yes");
define("_AM_NO","No");
define("_AM_INTROTEXT","Intro Text");
define("_AM_EXTEXT","Extended Text");
define("_AM_NOTES","Notes");
define("_AM_ALLOWEDHTML","Allowed HTML:");
define("_AM_DISAMILEY","Disable Smiley");
define("_AM_DISHTML","Disable HTML");
define("_AM_NOTIFYBYMAIL","Notify by mail when published");
define("_AM_APPROVE","Approve");
define("_AM_MOVETOTOP","Move this story to top");
define("_AM_CHANGEDATETIME","Change the date/time of publish");
define("_AM_NOWSETTIME","It is now set at: %s"); // %s is datetime of publish
define("_AM_CURRENTTIME","Current time is: %s");  // %s is the current datetime
define("_AM_SETDATETIME","Set the date/time of publish");
define("_AM_MONTHC","Month:");
define("_AM_DAYC","Day:");
define("_AM_YEARC","Year:");
define("_AM_TIMEC","Time:");
define("_AM_PREVIEW","Preview");
define("_AM_SAVE","Save");
define("_AM_PUBINHOME","Publish in Home?");
define("_AM_ADD","Add");
define("_AM_CATEGORY","Category");

//%%%%%%	Admin Module Name  Topics 	%%%%%

define("_AM_ADDMTOPIC","Add a MAIN Topic");
define("_AM_TOPICNAME","Topic Name");
define("_AM_MAX40CHAR","(max: 40 characters)");
define("_AM_TOPICIMG","Topic Image");
define("_AM_IMGNAEXLOC","image name + extension located in %s");
define("_AM_FEXAMPLE","for example: games.gif");
define("_AM_RELLINKCAT","Related Links Category: ");
define("_AM_RELDOWNCAT","Related Downloads Category:");
define("_AM_ADDSUBTOPIC","Add a SUB-Topic");
define("_AM_IN","in");
define("_AM_MODIFYTOPIC","Modify Topic");
define("_AM_MODIFY","Modify");
define("_AM_PARENTTOPIC","Parent Topic");
define("_AM_SAVECHANGE","Save Changes");
define("_AM_DEL","Delete");
define("_AM_CANCEL","Cancel");
define("_AM_WAYSYWTDTTAL","WARNING: Are you sure you want to delete this Topic and ALL its Stories and Comments?");


// Added in Beta6
define("_AM_TOPICSMNGR","Topics Manager");
define("_AM_PEARTICLES","Post/Edit Articles");
define("_AM_NEWSUB","New Submissions");
define("_AM_POSTED","Posted");
define("_AM_NOSUBJECT","No Subject");
define("_AM_GENERALCONF","General Configuration");
define("_AM_STORYHOME","How many stories on top page?");
define("_AM_NOTIFYSUBMIT","Notify by mail upon new submission?");
define("_AM_DISPLAYNAV","Display navigation box?");
define("_AM_TIPATH","Path to topic images folder (put / only at the end)");
?>