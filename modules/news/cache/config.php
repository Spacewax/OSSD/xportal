<?php

###############################################################################
# News Module
#
# $mpn_newsConfig['storyhome']:	How many stories on top page?
# $mpn_newsConfig['notifysubmit']:	Notify by mail upon new submission? 1=Yes 0=No
# $mpn_newsConfig['displaynav']:    	Display navigation box? 1=Yes 0=No
# $mpn_newsConfig['tipath']:    	Path to topic images? Generally, you dont need to change this.
###############################################################################

$xoops_newsConfig['storyhome'] = 10;
$xoops_newsConfig['notifysubmit'] = 1;
$xoops_newsConfig['displaynav'] = 1;
$xoops_newsConfig['tipath'] = "modules/news/images/topics/";

?>