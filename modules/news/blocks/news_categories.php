<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_news_categories_show() {
	global $xoopsDB, $xoopsConfig, $xoopsOption;
	$block = array();
	$block['title'] = _MB_NEWS_TITLE2;
	$block['content'] = "";
    $result = $xoopsDB->query("SELECT catid, title FROM ".$xoopsDB->prefix("stories_cat")." ORDER BY title");
    $numrows = $xoopsDB->num_rows($result);
    if ( $numrows > 0 ) {
		while ( list($dbcatid, $title) = $xoopsDB->fetch_row($result) ) {
			$result2 = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("stories")." WHERE catid=".$dbcatid."");
            list($numrows) = $xoopsDB->fetch_row($result2);
            if ( $numrows > 0 ) {
				$res = $xoopsDB->query("SELECT published FROM ".$xoopsDB->prefix("stories")." WHERE catid=".$dbcatid." ORDER BY storyid DESC",0,1,0);
                list($time) = $xoopsDB->fetch_row($res);
  	            if ( isset($xoopsOption['storycat']) && $xoopsOption['storycat'] == $dbcatid ) {
					$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<b>$title</b><br />\n";
				} else {
					$block['content'] .= "<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/news/index.php?storycat=$dbcatid'>$title</a> <small>".formatTimestamp($time,"s")."</small><br />\n";
				}
			}
		}
		return $block;
	}
}
?>