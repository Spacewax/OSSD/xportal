<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_news_top_show($options) {
	global $xoopsDB, $xoopsConfig;
	$myts = new MyTextSanitizer();
	$block = array();
	$block['content'] = "<small>";
	$sql = "SELECT storyid, title, published, counter FROM ".$xoopsDB->prefix("stories")." WHERE published<".time()." AND published>0 ORDER BY ".$options[0]." DESC";
	$result = $xoopsDB->query($sql,0,$options[1],0);
	while ( $myrow = $xoopsDB->fetch_array($result) ) {
		$title = $myts->makeTboxData4Show($myrow["title"]);
		if ( !XOOPS_USE_MULTIBYTES ) {
			if (strlen($title) >= 19) {
				$title = substr($title,0,18)."...";
			}
		}
		$block['content'] .= "&nbsp;&nbsp;<strong><big>&middot;</big></strong>&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$myrow['storyid']."'>".$title."</a> ";
		if ( $options[0] == "published" ) {
			$block['content'] .= "(".formatTimestamp($myrow['published'],"s").")<br />";
			$block['title'] = _MB_NEWS_TITLE4;
		} elseif ( $options[0] == "counter" ) {
			$block['content'] .= "(".$myrow['counter'].")<br />";
			$block['title'] = _MB_NEWS_TITLE5;
		}
	}
    $block['content'] .= "</small>";
	return $block;
}

function b_news_top_edit($options) {
	$form = ""._MB_NEWS_ORDER."&nbsp;<select name='options[]'>";
	$form .= "<option value='published'";
	if ( $options[0] == "published" ) {
		$form .= " selected='selected'";
	}
	$form .= ">"._MB_NEWS_DATE."</option>\n";
	$form .= "<option value='counter'";
	if($options[0] == "counter"){
		$form .= " selected='selected'";
	}
	$form .= ">"._MB_NEWS_HITS."</option>\n";
	$form .= "</select>\n";
	$form .= "&nbsp;"._MB_NEWS_DISP."&nbsp;<input type='text' name='options[]' value='".$options[1]."' />&nbsp;"._MB_NEWS_ARTCLS."";
	return $form;
}
?>