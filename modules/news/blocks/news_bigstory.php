<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_news_bigstory_show() {
	global $xoopsDB, $xoopsConfig;
	$block = array();
	$block['title'] = _MB_NEWS_TITLE1;
	$tdate = mktime(0,0,0,date("n"),date("j"),date("Y"));
	$now = time();
    $result = $xoopsDB->query("SELECT storyid, title FROM ".$xoopsDB->prefix("stories")." WHERE (published > ".$tdate." AND published < $now) ORDER BY counter DESC",0,1,0);
    list($fsid, $ftitle) = $xoopsDB->fetch_row($result);
    if ( (!$fsid) AND (!$ftitle) ) {
		$block['content'] = "<div style='text-align: center;'>"._MB_NEWS_NOTYET."</div>";
	} else {
		$block['content'] = "<div style=text-align: center;'>"._MB_NEWS_TMRSI."<br /><br />";
		$block['content'] .= "<a href='".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=$fsid'>$ftitle</a></div>";
	}
	return $block;
}
?>