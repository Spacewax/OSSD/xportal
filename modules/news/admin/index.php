<?php

include("admin_header.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."modules/news/class/class.story.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
//include_once($xoopsConfig['root_path']."class/xoopshtmlform.php");
include($xoopsConfig['root_path']."modules/news/include/htmlbuttons.inc");
include($xoopsConfig['root_path']."modules/news/cache/config.php");

function newsindex(){
	global $xoopsConfig, $xoopsModule;
	include_once($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />\n";
	OpenTable();
	echo " - <b><a href='index.php?op=newsConfig'>"._AM_GENERALCONF."</a></b><br /><br />\n";
	echo " - <b><a href='index.php?op=topicsmanager'>"._AM_TOPICSMNGR."</a></b>";
	echo "<br /><br />\n";
	echo " - <b><a href='index.php?op=newarticle'>"._AM_PEARTICLES."</a></b>\n";
    	CloseTable();
}

/*
 * Show new submissions
 */
function newSubmissions(){
	global $xoopsConfig, $xoopsDB, $xoopsTheme;
	$storyarray = Story::getAllSubmitted();
	if ( count($storyarray) > 0 ) {
		OpenTable();
		echo "<div style='text-align: center;'><b>"._AM_NEWSUB."</b><br /><table width='100%' border='1'><tr bgcolor='".$xoopsTheme['bgcolor2']."'><td align='center'>"._AM_TITLE."</td><td align='center'>"._AM_POSTED."</td><td align='center'>"._AM_POSTER."</td><td align='center'>"._AM_ACTION."</td></tr>\n";
		foreach($storyarray as $newstory){
			$uname = XoopsUser::get_uname_from_id($newstory->uid);
			echo "<tr><td>\n";
			$title = $newstory->title();
			if ( !isset($title) || ($title == "") ) {
			    echo "<a href='index.php?op=edit&amp;storyid=".$newstory->storyid()."'>"._AD_NOSUBJECT."</a>\n";
			} else {
			    echo "&nbsp;<a href='index.php?op=edit&amp;storyid=".$newstory->storyid()."'>".$title."</a>\n";
			}
			echo "</td><td align='center' class='nw'>".formatTimestamp($newstory->created())."</td><td align='center'><a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$newstory->uid()."'>".$uname."</a></td><td align='right'><a href='index.php?op=delete&amp;storyid=".$newstory->storyid."'>"._AM_DELETE."</a></td></tr>\n";
		}
		echo "</table></div>\n";
		CloseTable();
		echo "<br />";
	}
}

/*
 * Shows automated stories
 */
function autoStories(){
    	global $xoopsDB, $xoopsTheme, $xoopsConfig;
	$storyarray = Story::getAllAutoStory();
	if ( count($storyarray) > 0 ) {
		OpenTable();
    		echo "<div style='text-align: center;'><b>"._AM_AUTOARTICLES."</b><br />\n";
		echo "<table border='1' width='100%'><tr bgcolor=".$xoopsTheme['bgcolor2']."><td align='center'>"._AM_STORYID."</td><td align='center'>"._AM_TITLE."</td><td align='center'>"._AM_TOPIC."</td><td align='center'>"._AM_POSTER."</td><td align='center' class='nw'>"._AM_PROGRAMMED."</td><td align='center'>"._AM_ACTION."</td></tr>";
    		foreach($storyarray as $autostory){
			$uname = XoopsUser::get_uname_from_id($autostory->uid());
        		echo "
        		<tr><td align='center'><b>".$autostory->storyid()."</b>
        		</td><td align='left'><a href='".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$autostory->storyid()."'>".$autostory->title()."</a>
        		</td><td align='center'>".$autostory->topic()."
        		</td><td align='center'><a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$autostory->uid()."'>".$uname."</a></td><td align='center' class='nw'>".formatTimestamp($autostory->published())."</td><td align='center'><a href='index.php?op=edit&amp;storyid=".$autostory->storyid()."'>"._AM_EDIT."</a>-<a href='index.php?op=delete&amp;storyid=".$autostory->storyid()."'>"._AM_DELETE."</a>";
        		echo "</td></tr>\n";
    		}
    		echo "</table>";
		echo "</div>";
		CloseTable();
		echo "<br />";
	}
}

/*
 * Shows last 10 published stories
 */
function lastStories() {
    	global $xoopsDB, $xoopsTheme, $xoopsConfig;
	OpenTable();
    	echo "<div style='text-align: center;'><b>"._AM_LAST10ARTS."</b><br />";
	$storyarray = Story::getAllPublished(10);
    	echo "<table border='1' width='100%'><tr bgcolor=".$xoopsTheme['bgcolor2']."><td align='center'>"._AM_STORYID."</td><td align='center'>"._AM_TITLE."</td><td align='center'>"._AM_TOPIC."</td><td align='center'>"._AM_POSTER."</td><td align='center' class='nw'>"._AM_PUBLISHED."</td><td align='center'>"._AM_ACTION."</td></tr>";
    	foreach($storyarray as $eachstory){
		$uname = XoopsUser::get_uname_from_id($eachstory->uid());
		$published = formatTimestamp($eachstory->published());
        	echo "
        	<tr><td align='center'><b>".$eachstory->storyid()."</b>
        	</td><td align='left'><a href='".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$eachstory->storyid()."'>".$eachstory->title()."</a>
        	</td><td align='center'>".$eachstory->topic()."
        	</td><td align='center'><a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$eachstory->uid()."'>".$uname."</a></td><td align='center' class='nw'>".$published."</td><td align='center'><a href='index.php?op=edit&amp;storyid=".$eachstory->storyid()."'>"._AM_EDIT."</a>-<a href='index.php?op=delete&amp;storyid=".$eachstory->storyid()."'>"._AM_DELETE."</a>";
        	echo "</td></tr>\n";
    	}
    	echo "</table><br />";
/*	$storyarray = Story::getAllPublished(0,false);
    	echo "<form action='index.php' method='post'>";
	echo XoopsHtmlForm::select("storyid",$storyarray);
    	echo "
    	<select name='op'>
    	<option value='edit' selected='selected'>"._AM_EDIT."</option>
    	<option value='delete'>"._AM_DELETE."</option>
    	</select>
    	<input type='submit' value='"._AM_GO."' />
    	</form>
*/
	echo "<form action='index.php' method='post'>
    	"._AM_STORYID." <input type='text' name='storyid' size='10' />
    	<select name='op'>
    	<option value='edit' selected='selected'>"._AM_EDIT."</option>
    	<option value='delete'>"._AM_DELETE."</option>
    	</select>
    	<input type='submit' value='"._AM_GO."' />
    	</form>
	</div>
    	";
    	CloseTable();
}

function topicsmanager(){
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	include($xoopsConfig['root_path']."header.php");
	$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
	$xoopsModule->printAdminMenu();
	echo "<br />";
// Add a New Main Topic
    	OpenTable();
   	echo "<form method='post' action='index.php'>\n";
    	echo "<h4>"._AM_ADDMTOPIC."</h4><br />";
	echo "<b>"._AM_TOPICNAME."</b> "._AM_MAX40CHAR."<br /><input type='text' name='topictext' size='20' maxlength='20' /><br />";
	echo "<b>"._AM_TOPICIMG."</b> (". sprintf(_AM_IMGNAEXLOC,$xoopsConfig['tipath']).")<br />"._AM_FEXAMPLE."<br /><input type='text' name='topicimage' size='20' maxlength='20' /><br />";
	echo "<br />";
	echo "<input type='hidden' name='pid' value='0' />\n";
	echo "<input type='hidden' name='op' value='addTopic' />";
	echo "<input type='submit' value="._AM_ADD." /><br /></form>";
    	CloseTable();
    	echo "<br />";
// Add a New Sub-Topic
    	$result=$xoopsDB->query("select count(*) from ".$xoopsDB->prefix("topics")."");
	list($numrows)=$xoopsDB->fetch_row($result);
    	if ($numrows>0) {
		OpenTable();
    		echo "<form method='post' action='index.php'>";
    		echo "<h4>"._AM_ADDSUBTOPIC."</h4><br />";
		echo "<b>"._AM_TOPICNAME."</b> "._AM_MAX40CHAR."<br /><input type='text' name='topictext' size='20' maxlength='20' />"._AM_IN."&nbsp;";
		$mf->makeMySelBox("topictext", "topictext", 0, 0, "pid");
		echo "<br />";
		echo "<b>"._AM_TOPICIMG."</b> (". sprintf(_AM_IMGNAEXLOC,$xoopsConfig['tipath']).")<br />"._AM_FEXAMPLE."<br /><input type='text' name='topicimage' size='20' maxlength='20' /><br />";

		echo "<br />";
    		echo "<input type='hidden' name='op' value='addTopic' />";
		echo "<input type='submit' value='"._AM_ADD."' /><br /></form>";
		CloseTable();
		echo "<br />";
   	
// Modify Topic
    		OpenTable();
    		echo "
    		<form method='post' action='index.php'>
    		<h4>"._AM_MODIFYTOPIC."</h4><br />";
    		echo "<b>"._AM_TOPIC."</b><br />";
    		$mf->makeMySelBox("topictext", "topictext");
    		echo "<br /><br />\n";
    		echo "<input type='hidden' name='op' value='modTopic' />\n";
    		echo "<input type='submit' value='"._AM_MODIFY."' />\n";
		echo "</form>";
		CloseTable();
    	}
}

function modTopic() {
    	global $xoopsDB, $HTTP_POST_VARS, $xoopsConfig;
	global $xoops_newsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
	$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
    	$topicid = $HTTP_POST_VARS['topicid'];
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	OpenTable();
    	echo "<h4>"._AM_MODIFYTOPIC."</h4><br />";
	$result=$xoopsDB->query("SELECT pid, topicimage, topictext FROM ".$xoopsDB->prefix("topics")." WHERE topicid=$topicid");
	list($pid,$topicimage,$topictext) = $xoopsDB->fetch_row($result);
	$topictext = $myts->makeTboxData4Edit($topictext);
	$topicimage = $myts->makeTboxData4Edit($topicimage);
	$topicimage = urldecode($topicimage);
	echo "<div style='text-align: right;'><img src='".$xoopsConfig['xoops_url']."/".$xoops_newsConfig['tipath']."".$topicimage."'></div>";
	echo "<form action='index.php' method='post'>";
	echo "<b>"._AM_TOPICNAME."</b>"._AM_MAX40CHAR."<br /><input type='text' name='topictext' size='20' maxlength='20' value='$topictext' /><br />";
	echo "<b>"._AM_TOPICIMG."</b>(". sprintf(_AM_IMGNAEXLOC %s ,$xoopsConfig['tipath']).")<br />"._AM_FEXAMPLE."<br /><input type='text' name='topicimage' size='20' maxlength='20' value='$topicimage' /><br />\n";
	echo "<b>"._AM_PARENTTOPIC."<b><br />\n";
	$mf->makeMySelBox("topictext", "topictext", $pid, 1, "pid");
	echo "\n<br /><br />";

	echo "<input type='hidden' name='topicid' value='$topicid' />\n";
	echo "<input type='hidden' name='op' value='modTopicS' />";
	echo "<input type='submit' value='"._AM_SAVECHANGE."'>&nbsp;<input type='button' value='"._AM_DEL."' onclick='location=\"index.php?pid=$pid&amp;topicid=$topicid&amp;op=delTopic\"' />\n";
	echo "&nbsp;<input type='button' value='"._AM_CANCEL."' onclick='javascript:history.go(-1)' />\n";
	echo "</form>";
    	CloseTable();
}
function modTopicS() {
    	global $xoopsDB, $HTTP_POST_VARS;
	$myts = new MyTextSanitizer;
	$eh = new ErrorHandler;
    	$topicid =  $HTTP_POST_VARS['topicid'];
    	$pid =  $HTTP_POST_VARS['pid'];
	if($pid == $topicid){
		echo "ERROR: Cannot select this topic for parent topic!";
		exit();
	}
    	$topictext =  $myts->makeTboxData4Save($HTTP_POST_VARS['topictext']);
	if (($HTTP_POST_VARS['topicimage']) || ($HTTP_POST_VARS['topicimage']!="")) {
		$topicimage = urlencode($HTTP_POST_VARS['topicimage']);
		$topicimage = $myts->makeTboxData4Save($topicimage);
	}
	$query = "UPDATE ".$xoopsDB->prefix("topics")." SET pid=$pid, topictext='$topictext', topicimage='$topicimage' WHERE topicid=$topicid";
	$xoopsDB->query($query) or $eh->show("0013");
	$content=getTopicTree();
	writeTopicTree($content);
	redirect_header("index.php?op=topicsmanager",1,_AM_DBUPDATED);
	exit();
}
function delTopic() {
    	global $xoopsDB, $HTTP_GET_VARS, $xoopsConfig, $xoopsModule;
	$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
	$eh = new ErrorHandler;
    	$topicid =  $HTTP_GET_VARS['topicid'];
    	if ( $HTTP_GET_VARS['ok'] ) {
    		$ok =  $HTTP_GET_VARS['ok'];
    	}	
    	if ( $ok == 1 ) {
		//get all subtopics under the specified topic
		$arr=$mf->getAllChildId($topicid);
		$size = sizeof($arr);
		for ( $i = 0; $i < $size; $i++ ) {
			//get all stories in each topic
			$result=$xoopsDB->query("SELECT storyid from ".$xoopsDB->prefix("stories")." where topicid=".$arr[$i]."",1) or $eh->show("0013");
			//now for each story, delete the comment data and vote ata associated
			while(list($storyid)=$xoopsDB->fetch_row($result)){
				$substory = new Story($storyid);
				$substory->delete();
			}
			//all stories for each subtopic is deleted, now delete the subtopic data
    	    		$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("topics")." WHERE topicid=".$arr[$i]."",1) or $eh->show("0013");
			
		}
		//all subtopic and associated data are deleted, now delete topic data and its associated data
		$result=$xoopsDB->query("SELECT storyid FROM ".$xoopsDB->prefix("stories")." WHERE topicid=".$topicid."",1) or $eh->show("0013");
		while(list($storyid)=$xoopsDB->fetch_row($result)){
			$story = new Story($storyid);
			$story->delete();
		}
		//now delete the topic itself
	    	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("topics")." WHERE topicid=".$topicid."",1) or $eh->show("0013");
		
		$content=getTopicTree();
		writeTopicTree($content);
		redirect_header("index.php?op=topicsmanager",1,_AM_DBUPDATED);
		exit();	
    	} else {
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		OpenTable();
		echo "<div style='text-align: center;'>";
		echo "<h4><font color='#ff0000'>";
		echo ""._AM_WAYSYWTDTTAL."</font></h4><br />";
    		echo "[ <a href='index.php?op=delTopic&amp;topicid=".$topicid."&amp;ok=1'>"._AM_YES."</a> | <a href='index.php?op=topicsmanager'>"._AM_NO."</a> ]</div><br /><br />";
    		CloseTable();
    	}
}

function addTopic() {
    	global $xoopsDB, $HTTP_POST_VARS;
	$myts = new MyTextSanitizer;
	$eh = new ErrorHandler;
    	$pid = $HTTP_POST_VARS['pid'];
    	$topictext = $HTTP_POST_VARS['topictext'];
    	if (($HTTP_POST_VARS['topicimage']) || ($HTTP_POST_VARS['topicimage']!="")) {
		$topicimage = urlencode($HTTP_POST_VARS['topicimage']);
		$topicimage = $myts->makeTboxData4Save($topicimage);
	}
    	$topictext = $myts->makeTboxData4Save($topictext);
	$newid = $xoopsDB->GenID("topics_topicid_seq");
	$sql = "INSERT INTO ".$xoopsDB->prefix("topics")." (topicid, pid, topicimage, topictext) VALUES ($newid, $pid, '$topicimage', '$topictext')";
    	$xoopsDB->query($sql) or $eh->show("0013");
	$content=getTopicTree();
	writeTopicTree($content);
	redirect_header("index.php?op=topicsmanager",1,_AM_DBUPDATED);
	exit();
}

function getTopicTree(){
	global $xoopsDB, $xoopsConfig, $xoopsTheme;
	$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
	$result = $xoopsDB->query("SELECT topicid, topictext FROM ".$xoopsDB->prefix("topics")." where pid=0 order by topictext");
//	$numrows = $xoopsDB->num_rows($result);
	while(list($dbtopicid, $topictext) = $xoopsDB->fetch_row($result)) {
		$arr=$mf->getChildTreeArray($dbtopicid, "topictext");
		if(sizeof($arr)>0){
			$content .= ".".$topictext."|".$xoopsConfig['xoops_url']."/modules/news/index.php?storytopic=".$dbtopicid."\n";
			for($i=0;$i<sizeof($arr);$i++){
				$content .= ".".$arr[$i]['prefix']."".$arr[$i]['topictext']."|".$xoopsConfig['xoops_url']."/modules/news/index.php?storytopic=".$arr[$i]['topicid']."\n";
				//echo "<br />";
			}
		}else{
			$content .= ".".$topictext."|".$xoopsConfig['xoops_url']."/modules/news/index.php?storytopic=".$dbtopicid."\n";
			//echo "<br />";
		}
	}
	return $content;
}

function writeTopicTree($content){
	global $xoopsConfig;
	$filename = $xoopsConfig['root_path']."modules/news/cache/topictree.txt";
	$file = fopen($filename, "w");
	fwrite($file, $content);
	fclose($file);
}

function newsConfig() {
	global $xoopsConfig, $xoopsModule, $xoops_newsConfig;
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	OpenTable();
	echo "<h4>" . _AM_GENERALCONF . "</h4><br>";
	echo "<form action='index.php' method='post'>";
    	echo "
    	<table width='100%' border='0'><tr><td class='nw'>
    	"._AM_STORYHOME."</td><td width='100%'>
        <select name='storyhome'>
        <option value='".$xoops_newsConfig['storyhome']."' selected='selected'>".$xoops_newsConfig['storyhome']."</option>
	<option value='5'>5</option>
        <option value='10'>10</option>
        <option value='15'>15</option>
        <option value='20'>20</option>
        <option value='25'>25</option>
        <option value='30'>30</option>
    	</select>";
	echo "</td></tr>";
	echo "<tr><td class='nw'>" . _AM_NOTIFYSUBMIT . "</td><td>";
	if ($xoops_newsConfig['notifysubmit']==1) {
		echo "<input type='radio' name='notifysubmit' value='1' checked='checked' />&nbsp;" ._AM_YES."&nbsp;";
		echo "<input type='radio' name='notifysubmit' value='0' />&nbsp;" ._AM_NO."&nbsp;";
	} else {
		echo "<input type='radio' name='notifysubmit' value='1' />&nbsp;" ._AM_YES."&nbsp;";
		echo "<input type='radio' name='notifysubmit' value='0' checked='checked' />&nbsp;" ._AM_NO."&nbsp;";
	}
	
	echo "</td></tr>";
	echo "<tr><td class='nw'>" . _AM_DISPLAYNAV . "</td><td>";
	if ($xoops_newsConfig['displaynav']==1) {
		echo "<input type='radio' name='displaynav' value='1' checked='checked' />&nbsp;" ._AM_YES."&nbsp;";
		echo "<input type='radio' name='displaynav' value='0' />&nbsp;" ._AM_NO."&nbsp;";
	} else {
		echo "<input type='radio' name='displaynav' value='1' />&nbsp;" ._AM_YES."&nbsp;";
		echo "<input type='radio' name='displaynav' value='0' checked='checked' />&nbsp;" ._AM_NO."&nbsp;";
	}
	
	echo "</td></tr>";
	echo "<tr><td class='nw'>" . _AM_TIPATH . "</td><td>";
	echo "<input type='text' name='tipath' value='".$xoops_newsConfig['tipath']."' />";
	echo "</td></tr>";
    	echo "</table>";
    	echo "<input type='hidden' name='op' value='newsConfigS' />";
    	echo "<input type='submit' value='"._AM_SAVECHANGE."' />";
	echo "&nbsp;<input type='button' value='"._AM_CANCEL."' onclick='javascript:history.go(-1)' />";
    	echo "</form>";
    	CloseTable();

}

function newsConfigS() {
	global $xoopsConfig, $HTTP_POST_VARS;

	$filename = "../cache/config.php";
	$file = fopen($filename, "w");
	$content = "";
	$content .= "<?php\n";
	$content .= "\n";
	$content .= "###############################################################################\n";
	$content .= "# News Module\n";
	$content .= "#\n";
	$content .= "# \$xoops_newsConfig['storyhome']:	"._AM_STORYHOME."\n";
	$content .= "# \$xoops_newsConfig['notifysubmit']:	"._AM_NOTIFYSUBMIT." 1="._AM_YES." 0="._AM_NO."\n";
	$content .= "# \$xoops_newsConfig['displaynav']:    	"._AM_DISPLAYNAV." 1="._AM_YES." 0="._AM_NO."\n";
	$content .= "# \$xoops_newsConfig['tipath']:    	"._AM_TIPATH."\n";
	$content .= "###############################################################################\n";
	$content .= "\n";
	$content .= "\$xoops_newsConfig['storyhome'] = ".$HTTP_POST_VARS['storyhome'].";\n";
	$content .= "\$xoops_newsConfig['notifysubmit'] = ".$HTTP_POST_VARS['notifysubmit'].";\n";
	$content .= "\$xoops_newsConfig['displaynav'] = ".$HTTP_POST_VARS['displaynav'].";\n";
	$content .= "\$xoops_newsConfig['tipath'] = '".$HTTP_POST_VARS['tipath']."';\n";
	$content .= "\n";
	$content .= "?>";

	fwrite($file, $content);
    	fclose($file);

	redirect_header("index.php",1,_AM_DBUPDATED);
	exit();
}


if(isset($op)){
switch($op){
	case "edit":
		include($xoopsConfig['root_path']."header.php");
		$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		OpenTable();
		echo "<h4>"._AM_EDITARTICLE."</h4>";
		$story = new Story($storyid);

		$title = $story->title("Edit");
		$hometext = $story->hometext("Edit");
		$bodytext = $story->bodytext("Edit");
	
		$notes = $story->notes("Edit");
		$nohtml = $story->nohtml();
		$nosmiley = $story->nosmiley();
		$ihome = $story->ihome();
		$topicid = $story->topicid();
		$catid = $story->catid();
		$published = $story->published();
//		$notifypub = $story->notifypub();
		$type = $story->type();
		$isedit = 1;
		include("storyform.inc.php");
		CloseTable();
		break;

	case "newarticle":
		include($xoopsConfig['root_path']."header.php");
		include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
		$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		newSubmissions();
		autoStories();
		lastStories();
		echo "<br />";
		OpenTable();
		echo "<h4>"._AM_POSTNEWARTICLE."</h4>";
		$type = "admin";
		include("storyform.inc.php");
		CloseTable();
		break;

	case "preview":
		include($xoopsConfig['root_path']."header.php");
		if(isset($storyid)){
			$story = new Story($storyid);
			$published = $story->published();
		}else{
			$story = new Story();
		}
		$story->setTitle($title);
		$story->setHomeText($hometext);
		$story->setBodyText($bodytext);
		if(isset($nohtml) && ($nohtml == 0 || $nohtml == 1)){
			$story->setNohtml($nohtml);
		}
		if(isset($nosmiley) && ($nosmiley == 0 || $nosmiley == 1)){
			$story->setNosmiley($nosmiley);
		}
		if(isset($notes) && $notes != ""){
			$story->setNotes($notes);
		}

		$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
		$p_title = $story->title("Preview");
		$p_hometext = $story->hometext("Preview");
		$p_bodytext = $story->bodytext("Preview");
		if(isset($notes) && $notes != ""){
			$p_notes = $story->notes("Preview");
			$notes = $story->notes("InForm");
		}
		$title = $story->title("InForm");
		$hometext = $story->hometext("InForm");
		$bodytext = $story->bodytext("InForm");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		OpenTable();
		OpenTable("75%");
		if ( $topicid == "" ) {
		    $topicimage="AllTopics.gif";
	    		$warning = "<div style='text-align: center;'><blink><b>"._AR_TOPIC."</b></blink></div>";
		} else {
	    		$warning = "";
	    		$result = $xoopsDB->query("SELECT topicimage FROM ".$xoopsDB->prefix("topics")." WHERE topicid=$topicid");
	    		list($topicimage) = $xoopsDB->fetch_row($result);
		}
		echo GetImgTag($xoops_newsConfig['tipath'],$topicimage," ",0,"right");
		if ( isset($p_bodytext) && $p_bodytext != "" ) {
    			echo "<p><b>".$p_title."</b><br />".$p_hometext."<br /><br />".$p_bodytext."<br /><br />";
			if(isset($p_notes) && $p_notes != ""){
				echo $p_notes;
			}
			echo "</p>";
		}else{
			echo "<p><b>".$p_title."</b><br /><br />".$p_hometext."<br /><br />";
			if(isset($p_notes) && $p_notes != ""){
				echo $p_notes;
			}
			echo "</p>";
		}
		echo $warning;
		CloseTable();
		include("storyform.inc.php");
		CloseTable();
		break;

	case "save":
		if(!$storyid){
			$story = new Story();
			$story->setUid($xoopsUser->uid());
			if($autodate){
				$pubdate = mktime($autohour, $automin, 0, $automonth, $autoday, $autoyear);
				$offset = $xoopsUser->timezone()-$xoopsConfig['server_TZ'];
				$pubdate = $pubdate - ($offset * 3600);
				$story->setPublished($pubdate);
			}else{
				$story->setPublished(time());
			}
			$story->setType($type);
			$story->setHostname(getenv("REMOTE_ADDR"));
//			$story->setNotifyPub($notifypub);
		}else{
			$story = new Story($storyid);
			if($autodate){
    				$pubdate = mktime($autohour, $automin, 0, $automonth, $autoday, $autoyear);
				$offset = $xoopsUser->timezone();
				$offset = $offset-$xoopsConfig['server_TZ'];
				$pubdate = $pubdate - ($offset * 3600);
				$story->setPublished($pubdate);
			}elseif(($story->published() == 0) && $approve){
				$story->setPublished(time());
				$isnew = 1;
			}else{
				if($movetotop){
					$story->setPublished(time());
				}
			}
		}
		$story->setApproved($approve);
		$story->setCatId($catid);
		$story->setTopicId($topicid);
		$story->setTitle($title);
		$story->setHometext($hometext);
		$story->setBodytext($bodytext);
		if(isset($notes) && $notes != ""){
			$story->setNotes($notes);
		}
		$story->setNohtml($nohtml);
		$story->setNosmiley($nosmiley);
		$story->setIhome($ihome);
		$story->store();
		if($isnew && $story->notifypub() && $story->uid() != 0){
			$poster = new XoopsUser($story->uid());
			$subject = _AM_ARTPUBLISHED;
			$message = sprintf(_AM_HELLO,$poster->uname());
			$message .= "\n\n"._AM_YOURARTPUB."\n\n";
			$message .= ""._AM_TITLEC.$story->title()."\n"._AM_URLC.$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$story->storyid()."\n"._AM_PUBLISHEDC.formatTimestamp($story->published(),"m",0)."\n\n";
			$message .= $xoopsConfig['sitename']."\n".$xoopsConfig['xoops_url']."";
			mail($poster->email(), $subject, $message, "From: ".$xoopsConfig['adminmail']."\r\nX-Mailer: ".$xoopsConfig['sitename']."");
		}
		redirect_header("index.php",1,_AM_DBUPDATED);
		exit();
		break;

	case "delete":
		if($ok){
			$story = new Story($storyid);
			$story->delete();
			redirect_header("index.php",1,_AM_DBUPDATED);
			exit();
		}else{
			include($xoopsConfig['root_path']."header.php");
			OpenTable();
			echo "<div style='text-align: center;'>";
			echo "<h4><font color='#ff0000'>"._AM_RUSUREDEL."</font></h4>";
			echo "[&nbsp;<a href='index.php?op=delete&amp;storyid=".$storyid."&amp;ok=1'>"._AM_YES."</a>&nbsp;|&nbsp;<a href='javascript:history.go(-1)'>"._AM_NO."</a>&nbsp;]";
			echo "</div>";
			CloseTable();
		}
		break;
	case "topicsmanager":
		topicsmanager();
		break;

	case "addTopic":
		addTopic();
		break;

	case "delTopic":
		delTopic();
		break;
	case "modTopic":
		modTopic();
		break;
	case "modTopicS":
		modTopicS();
		break;
	case "newsConfig":
		newsConfig();
		break;
	case "newsConfigS":
		newsConfigS();
		break;
	default:
		newsindex();
		break;
}
}else{	

	newsindex();
}
include("admin_footer.php");
?>