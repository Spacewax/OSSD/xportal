<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: Kazumi Ono (http://www.mywebaddons.com/)                  //
###############################################################################

include("header.php");
include_once($xoopsConfig['root_path']."class/xoopscomments.php");
include_once("class/class.story.php");
include_once($xoopsConfig['root_path']."include/themeuserpost.php");

// must first define the unique item name used for the class Comments
$itemname = "storyid";

switch($op) {
	default:
		if ( !isset($storyid) || $storyid == "" ) {
			redirect_header("index.php",2,_NW_NOSTORY);
			exit();
		}
		include($xoopsConfig['root_path']."modules/news/cache/config.php");
		$xoopsConfig['tipath'] = $xoops_newsConfig['tipath']; //dirty workaround
		
		include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
		$myts=new MyTextSanitizer;
		if ( isset($tid) && $tid != "" ) {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname,$tid);
		} else {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname);
			// must do these because $itemid is not set if no $tid
			$artcomment->setItemId($storyid);
		}
		if( !$artcomment->parent() ){
			$article = new Story($storyid);
			if ( $article->published() == 0 || $article->published() > time() ) {
				redirect_header("index.php",2,_NW_NOSTORY);
				exit();
			}
			include_once($xoopsConfig['root_path']."header.php");
			if ( file_exists($xoopsConfig['root_path']."themes/".$xoopsTheme['thename']."/themearticle.php") ) {
				include($xoopsConfig['root_path']."themes/".$xoopsTheme['thename']."/themearticle.php");
			}
			$article->updateCounter();
			$datetime = formatTimestamp($article->created());
			$title = $article->title();
			$hometext = $article->hometext();
			$bodytext = $article->bodytext();
			$notes = $article->notes();
			if($bodytext == "") { 
    				$bodytext = $hometext;
				if ( $notes!="" ) {
					$bodytext .= "<br><br>"._NW_NOTES."<br>".$notes."";
				} 
			} else { 
				if ( $notes!="" ) {
					$bodytext = "$hometext<br><br>$bodytext<br><br>"._NW_NOTES."<br>$notes";
				} else {
    					$bodytext = "$hometext<br><br>$bodytext";
				}
			}
			$userid = $article->uid();
			$informant = XoopsUser::get_uname_from_id($userid);

			if ( $informant != $xoopsConfig['anonymous'] ) {
				$poster = "<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$userid."\">".$informant."</a>";
			}

			$t_result=$xoopsDB->query("SELECT topicimage, topictext FROM ".$xoopsDB->prefix("topics")." WHERE topicid=".$article->topicid()."");
			list($topicimage, $topictext) = $xoopsDB->fetch_row($t_result);
			if ( $article->catid() ) {
    				$c_title = $article->catTitle();
    				$title = "<a href=\"index.php?storycat=".$article->catid()."\">".$c_title."</a>: ".$title."";
			}

			echo "<table width=\"100%\" border=0><tr><td valign=top>";
			$adminlink = "&nbsp;";
			if ( $xoopsUser ) {
				if ( $xoopsUser->is_admin($xoopsModule->mid()) ) {						$adminlink = "  [ <a href='admin/index.php?op=edit&amp;storyid=".$storyid."'>"._EDIT."</a> | <a href='admin/index.php?op=delete&amp;storyid=".$storyid."'>"._DELETE."</a> ]";
				}
			}
			themearticle($poster, $datetime, $title, $bodytext, $article->topicid(), $topicimage, $topictext, $adminlink);
			echo "</td><td>&nbsp;</td><td valign=top width=200>";
			$boxtitle = ""._NW_RELATEDLINKS."";
			$boxstuff .= "<strong><big>&middot;</big></strong>&nbsp;<a href=\"index.php?storytopic=".$article->topicid()."\">";
			$boxstuff .= sprintf(_NW_MOREABOUT,$topictext);
			$boxstuff .= "</a><br>";

			// if the poster is not anonymous, show link to other articles by this poster
			if ( $informant != $xoopsConfig['anonymous'] ) {
				$boxstuff .= "<strong><big>&middot;</big></strong>&nbsp;<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$userid."\">";
				$boxstuff .= sprintf(_NW_NEWSBY,$informant);
				$boxstuff .= "</a>";
			}
			
			$boxstuff .= "<br><br><hr noshade width=\"95%\" size=\"1\"><center><b>";
			$boxstuff .= sprintf(_NW_MOSTREAD,$topictext);
			$boxstuff .= "</b><br>";
			$result2=$xoopsDB->query("SELECT storyid, title FROM ".$xoopsDB->prefix("stories")." WHERE topicid=".$article->topicid()." AND published <= ".time()." ORDER BY counter DESC",0,1,0);
			list($topstory, $ttitle) = $xoopsDB->fetch_row($result2);
			$ttitle = $myts->makeTboxData4Show($ttitle);
			$boxstuff .= "<a href=\"".$PHP_SELF."?storyid=$topstory\">$ttitle</a></center><br><br>";
	
			$boxstuff .= "<center><b>";
			$boxstuff .= sprintf(_NW_LASTNEWS,$topictext);
			$boxstuff .= "</b><br>";
			$result2=$xoopsDB->query("SELECT storyid, title FROM ".$xoopsDB->prefix("stories")." WHERE topicid=".$article->topicid()." AND published <= ".time()." ORDER BY published DESC",0,1,0);
			list($topstory, $ttitle) = $xoopsDB->fetch_row($result2);
			$ttitle = $myts->makeTboxData4Show($ttitle);
			$boxstuff .= "<a href=\"".$PHP_SELF."?storyid=$topstory\">$ttitle</a></center><br><br>";


			$boxstuff .= "<div align=\"right\">";
			$boxstuff .= "<a href=\"print.php?storyid=$storyid\"><img src=\"images/print.gif\" border=\"0\" alt=\""._NW_PRINTERFRIENDLY."\" width=\"15\" height=\"11\" /></a>&nbsp;&nbsp;";
			$boxstuff .= "<a target='_top' href='mailto:?subject=".sprintf(_NW_INTARTICLE,$xoopsConfig['sitename'])."&body=".sprintf(_NW_INTARTFOUND,$xoopsConfig['sitename']).":  ".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$article->storyid()."'><img src=\"images/friend.gif\" border=\"0\" alt=\""._NW_SENDSTORY."\" width=\"15\" height=\"11\" /></a>";
			$boxstuff .= "</div>";
			themesidebox($boxtitle, $boxstuff);
			echo "</td></tr></table>";

		}

		if ( $xoopsUser ) {
			if ( !isset($order) ) {
				$order = $xoopsUser->uorder();
			}
			if ( !isset($mode) || $mode=="" ) {
				$mode = $xoopsUser->umode();
			}
		}
		$commentsArray = $artcomment->getTopPageComments($order,$mode);
		include_once($xoopsConfig['root_path']."header.php");
		OpenTable();
		
		// print navbar if the page is on top of comments tree
		if( !$artcomment->parent() ){
			$artcomment->printNavBar($order, $mode);
		}

		if ( is_array($commentsArray) && count($commentsArray) ) {
			foreach($commentsArray as $ele){
				$subject = $ele->subject();
				$comment = $ele->comment();
			
				$postdate = formatTimestamp($ele->postdate(),"m");
				themeuserpost($ele->tid(),$ele->uid(),$subject,$comment,$postdate,$ele->hostname(),$ele->icon(),$mode,$order);
				//if the mode is thread, show thread tree
				if ( $mode=="thread" ) {
					echo "<table><tr><td width=\"30%\" valign=\"top\">";
					//if not in the top page, show links to parent and top comment
					if ( $artcomment->parent() ) {
						echo "[&nbsp;<a href=\"".$PHP_SELF."?storyid=".$storyid."&mode=".$mode."&order=".$order."\">"._NW_TOP."</a>&nbsp;|&nbsp;";
						echo "<a href=\"".$PHP_SELF."?storyid=".$storyid."&tid=".$artcomment->parent()."&mode=".$mode."&order=".$order."\">"._NW_PARENT."</a>&nbsp;]";
					}
					echo "</td><td valign=\"top\">";
					$treeArray = $artcomment->getCommentTree($ele->tid());
					//ok, shows the tree..
					foreach($treeArray as $treeItem){
						$prefix = str_replace(".", "&nbsp;&nbsp;&nbsp;&nbsp;", $treeItem->prefix());
						$csubject = $treeItem->subject();
						$date = formatTimestamp($treeItem->postdate());
						$name = XoopsUser::get_uname_from_id($treeItem->uid());
						echo "".$prefix."<img src=\"".$xoopsConfig['xoops_url']."/images/icons/posticon.gif\">&nbsp;<a href=\"".$PHP_SELF."?storyid=".$storyid."&tid=".$treeItem->tid()."&mode=".$mode."&order=".$order."\">".$csubject."</a>&nbsp;"._POSTEDBY."&nbsp;".$name."&nbsp;(".$date.")<br>";
					}
					echo "</td></tr></table>";
				}
			}
		}
		CloseTable();
		break;
	case "new":
		$xoopsOption['forumpage']=1;
		include($xoopsConfig['root_path']."header.php");
		$article = new Story($storyid);
		$hometext=$article->hometext();
		$bodytext=$article->bodytext();
		$date = formatTimestamp($article->published());
		$name = XoopsUser::get_uname_from_id($article->uid());
		$r_subject=$article->title();
		$subject=$article->title("Edit");
		$r_text = _NW_POSTERC."&nbsp;".$name."&nbsp;"._NW_DATEC."&nbsp;".$date."";
		$r_text .= "<br><br>".$hometext."";
		if ( $bodytext!="" ) {
			$r_text .= "<br><br>".$bodytext."";
		}
		themecenterposts($r_subject,$r_text);
		$itemid = $storyid;
		$pid = 0;
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;
	case "reply":
		$xoopsOption['forumpage']=1;
		include($xoopsConfig['root_path']."header.php");
		if ( isset($tid) && $tid != "" ) {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname,$tid);
		} else {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname);
		}
		$r_comment = $artcomment->comment();
		$r_date = formatTimestamp($artcomment->postdate());
		$r_name = XoopsUser::get_uname_from_id($artcomment->uid());
		$r_content = _NW_POSTERC."&nbsp;".$r_name."&nbsp;"._NW_DATEC."&nbsp;".$r_date."<br><br>";
		$r_content .= $r_comment;
		$r_subject=$artcomment->subject();
		$subject=$artcomment->subject("Edit");
		themecenterposts($r_subject,$r_content);
		$pid=$tid;
		unset($tid);
		$itemid=$artcomment->itemid();
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;

	case "edit":
		$xoopsOption['forumpage']=1;
		include($xoopsConfig['root_path']."header.php");
		if ( isset($tid) && $tid != "" ) {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname,$tid);
		} else {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname);
		}
		$nohtml = $artcomment->nohtml();
		$nosmiley = $artcomment->nosmiley();
		$icon = $artcomment->icon();
		$itemid=$artcomment->itemid();
		$subject=$artcomment->subject("Edit");
		$message=$artcomment->comment("Edit");
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;

	case "preview":
		$xoopsOption['forumpage']=1;
		include($xoopsConfig['root_path']."header.php");
		include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
		$myts = new MyTextSanitizer;
		$p_subject = $myts->makeTboxData4Preview($subject);
		if ( $nosmiley && $nohtml ) {
			$p_comment = $myts->makeTareaData4Preview($message,0);
		} elseif ( $nohtml ) {
			$p_comment = $myts->makeTareaData4Preview($message,0,1);
		} elseif ( $nosmiley ) {
			$p_comment = $myts->makeTareaData4Preview($message);
		} else {
			$p_comment = $myts->makeTareaData4Preview($message,1,1);
		}
		themecenterposts($p_subject,$p_comment);
		$subject = $myts->makeTboxData4PreviewInForm($subject);
		$message = $myts->makeTareaData4PreviewInForm($message);
		OpenTable();
		include($xoopsConfig['root_path']."include/commentform.inc.php");
		CloseTable();
		break;
	case "post":
		if ( !empty($tid) ) {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname,$tid);
			$accesserror = 0;
			if ( $xoopsUser ) {
				if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
					if ( $artcomment->uid() != $xoopsUser->uid() ) {
						$accesserror = 1;
					}
				}
			} else {
				$accesserror = 1;
			}
			if ( $accesserror == 1 ) {
				redirect_header("article.php?storyid=$itemid&tid=$tid&order=$order&mode=$mode",1,_NW_EDITNOTALLOWED);
				exit();
			}
		} else {
			$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname);
			$artcomment->setParent($pid);
			$artcomment->setItemId($itemid);
			$artcomment->setHostname(getenv("REMOTE_ADDR"));
			if ( $xoopsUser ) {
				$uid = $xoopsUser->uid();
			} else {
				if ( $xoopsConfig['anonpost'] ) {
					$uid = 0;
				} else {
					redirect_header("article.php?storyid=$itemid&tid=$tid&order=$order&mode=$mode",1,_NW_ANONNOTALLOWED);
					exit();
				}
			}
			$artcomment->setUid($uid);
		}
		$artcomment->setSubject($subject);
		$artcomment->setComment($message);
		$artcomment->setNohtml($nohtml);
		$artcomment->setNosmiley($nosmiley);
		$artcomment->setIcon($icon);
		$artcomment->store();
		redirect_header("article.php?storyid=$itemid&tid=$tid&order=$order&mode=$mode",2,_NW_THANKSFORPOST);
		exit();
		break;
	case "delete":
		if ( $xoopsUser ) {
			if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) {
				include($xoopsConfig['root_path']."header.php");
				echo "<h4>"._NW_DELNOTALLOWED."</h4>";
				echo "<br />";
				echo "<a href='javascript:history.go(-1)'>"._NW_GOBACK."</a>";
				include($xoopsConfig['root_path']."footer.php");
				exit();
			}
		}else{
			include($xoopsConfig['root_path']."header.php");
			echo "<h4>"._NW_DELNOTALLOWED."</h4>";
			echo "<br />";
			echo "<a href='javascript:history.go(-1)'>"._NW_GOBACK."</a>";
			include($xoopsConfig['root_path']."footer.php");
			exit();
		}
		if ( !empty($ok) ) {
			if ( !empty($tid) ) {
				$artcomment = new XoopsComments($xoopsDB->prefix("comments"),$itemname,$tid);
				$deleted = $artcomment->delete();
				$itemid = $artcomment->itemid();
			}
			redirect_header("article.php?storyid=$itemid&order=$order&mode=$mode",2,_PL_COMMENTSDEL);
			exit();
		} else {
			include($xoopsConfig['root_path']."header.php");
			OpenTable();
			echo "<div align=\"center\">";
			echo "<h4><font color=\"#ff0000\">"._NW_AREUSUREDEL."</font></h4>";
			echo "[&nbsp;<a href=\"article.php?op=delete&tid=".$tid."&mode=".$mode."&order=".$order."&ok=1\">"._NW_YES."</a>&nbsp;|&nbsp;<a href='javascript:history.go(-1)'>"._NW_NO."</a>&nbsp;]";
			echo "</div>";
			CloseTable();
		}
		break;

}

include($xoopsConfig['root_path']."footer.php");
?>