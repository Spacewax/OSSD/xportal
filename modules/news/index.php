<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

include("header.php");
if($xoopsConfig['startpage'] == "news"){
	$xoopsOption['show_rblock'] =1;
	include($xoopsConfig['root_path']."header.php");
	make_cblock();
	echo "<br />";
}else{
	$xoopsOption['show_rblock'] =0;
	include($xoopsConfig['root_path']."header.php");
}
include($xoopsConfig['root_path']."modules/news/cache/config.php");
$xoopsConfig['tipath'] = $xoops_newsConfig['tipath']; //dirty workaround
if(file_exists($xoopsConfig['root_path']."themes/".$xoopsTheme['thename']."/themeindex.php")){
	include($xoopsConfig['root_path']."themes/".$xoopsTheme['thename']."/themeindex.php");
}
include_once("class/class.story.php");

if(isset($HTTP_GET_VARS['storycat'])){
	if(ereg("^[0-9]{1,}$",$HTTP_GET_VARS['storycat'])){
		$xoopsOption['storycat'] = $HTTP_GET_VARS['storycat'];
	}else{
		$xoopsOption['storycat'] = "";
	}
}else{
	$xoopsOption['storycat'] = "";
}
if(isset($HTTP_GET_VARS['storytopic'])){
	if(ereg("^[0-9]{1,}$",$HTTP_GET_VARS['storytopic'])){
		$xoopsOption['storytopic'] = $HTTP_GET_VARS['storytopic'];
	}else{
		$xoopsOption['storytopic'] = "";
	}
}else{
	$xoopsOption['storytopic'] = "";
}
if(isset($HTTP_GET_VARS['storynum'])){
	if(ereg("^[0-3][0-5]$",$HTTP_GET_VARS['storynum'])){
		$xoopsOption['storynum'] = $HTTP_GET_VARS['storynum'];
	}else{
		$xoopsOption['storynum'] =$xoops_newsConfig['storyhome'];
	}
}else{
	$xoopsOption['storynum'] =$xoops_newsConfig['storyhome'];
}


if ($xoops_newsConfig['displaynav'] == 1 && $xoopsOption['storycat'] == ""){
	include_once($xoopsConfig['root_path']."class/xoopstree.php");
	indexnav();
}
$query = "SELECT s.*, c.title AS cattitle, t.topicimage, t.topictext FROM ".$xoopsDB->prefix("stories")." s LEFT JOIN ".$xoopsDB->prefix("stories_cat")." c ON c.catid=s.catid LEFT JOIN ".$xoopsDB->prefix("topics")." t ON t.topicid=s.topicid WHERE ";
$countquery = "SELECT COUNT(*) FROM ".$xoopsDB->prefix("stories")." s WHERE ";
if($xoopsOption['storycat']) {
	$query .= "s.catid=".$xoopsOption['storycat']."";
	$countquery .= "s.catid=".$xoopsOption['storycat']."";
}elseif($xoopsOption['storytopic']) {
	$query .= "s.topicid=".$xoopsOption['storytopic']."";
	$countquery .= "s.topicid=".$xoopsOption['storytopic']."";
}else{
	$query .= "s.ihome=0";
	$countquery .= "s.ihome=0";
}
if(!isset($HTTP_GET_VARS['start']) || $HTTP_GET_VARS['start'] == ""){
	$start = 0;
}else{
	$start = $HTTP_GET_VARS['start'];
}
$query .= " AND s.published>0 AND s.published <= ".time()." ORDER BY s.published DESC";
$countquery .= " AND s.published>0 AND s.published <= ".time()."";
$result = $xoopsDB->query($query, 0, $xoopsOption['storynum'], $start);
list($storycount) = $xoopsDB->fetch_row($xoopsDB->query($countquery,1));
//    	if(!$result) {
//        	echo mysql_errno(). ": ".mysql_error(). "<br />"; exit();
//    	}
//echo $query;
while ($sarray = $xoopsDB->fetch_array($result)) {
	$uname = XoopsUser::get_uname_from_id($sarray['uid']);
	if ( $uname != $xoopsConfig['anonymous'] ) {
		$poster = "<a href='".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$sarray['uid']."'>".$uname."</a>";
	}else{
		$poster = $uname;
	}
        $printP = "<a href='".$xoopsConfig['xoops_url']."/modules/news/print.php?storyid=".$sarray['storyid']."'><img src='".$xoopsConfig['xoops_url']."/modules/news/images/print.gif' border='0' alt='"._NW_PRINTER."' width='15' height='11' /></a>&nbsp;";
        $sendF = "<a target='_top' href='mailto:?subject=".sprintf(_NW_INTARTICLE,$xoopsConfig['sitename'])."&body=".sprintf(_NW_INTARTFOUND,$xoopsConfig['sitename']).":  ".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$sarray['storyid']."'><img src=\"images/friend.gif\" border=\"0\" alt=\""._NW_SENDSTORY."\" width=\"15\" height=\"11\" /></a>";
        $created = formatTimestamp($sarray['created']);	
	$story = new Story($sarray);
        $s_title = $story->title();
        $hometext = $story->hometext();
        $notes = $story->notes();
        $introcount = strlen($hometext);
        $fullcount = strlen($sarray['bodytext']);
        $totalcount = $introcount + $fullcount;
        $morelink = "";
        if ($fullcount > 1) {
            	$morelink .= "<a href='".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$sarray['storyid']."";
 
            	$morelink .= "'><b>"._NW_READMORE."</b></a> | ";
		$morelink .= sprintf(_NW_BYTESMORE,$totalcount);
		$morelink .= " | "; 
	}
	$count_res = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("comments")." WHERE storyid=".$sarray['storyid']."");
        list($count) = $xoopsDB->fetch_row($count_res);
        $morelink .= "<a href='".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$sarray['storyid']."";

        $morelink2 = "<a href='".$xoopsConfig['xoops_url']."/modules/news/article.php?storyid=".$sarray['storyid']."";

        if(($count==0)) {
                $morelink .= "'>"._NW_COMMENTS."</a> | $printP $sendF";
        } else {
            	if (($fullcount<1)) {
                	if(($count==1)) {
                    		$morelink .= "'><b>"._NW_READMORE."</b></a> | $morelink2'>"._NW_ONECOMMENT."</a> | $printP $sendF ";
                    	} else {
                    		$morelink .= "'><b>"._NW_READMORE."</b></a> | $morelink2'>";
				$morelink .= sprintf(_NW_NUMCOMMENTS,$count);
				$morelink .= "</a> | $printP $sendF";
                	}
            	} else {
                	if(($count==1)) {
                    		$morelink .= "'>"._NW_ONECOMMENT."</a> | $printP $sendF ";
                	} else {
                    		$morelink .= "'>";
				$morelink .= sprintf(_NW_NUMCOMMENTS,$count);
				$morelink .= "</a> | $printP $sendF ";
                	}
            	}
        }

        if ($sarray['catid'] != 0) {
            	$s_title = "<a href='index.php?storycat=".$sarray['catid']."'>".$sarray['cattitle']."</a>: ".$s_title."";
        }

//add notes
	if ($notes != "") {
        	$notes = "<b>"._NW_NOTES."</b> <i>$notes</i>\n";
		$thetext = $hometext."<br /><br />".$notes."\n";
    	} else {
        	$thetext = $hometext."\n";
    	}
        	
//formatting story ends here

        themeindex($poster, $created, $s_title, $sarray['counter'], $sarray['topicid'], $thetext, $morelink, $sarray['topicimage'], $sarray['topictext']);
}

echo "<div align='center'>\n";
echo "<table border='0' width='90%'>\n";
echo "<tr><td align='left'>\n";
// check whether to display previous page link
if ($start!=0){
	if($start - $xoopsOption['storynum'] >0){
		$prevstart = $start - $xoopsOption['storynum'];
	}else{
		$prevstart = 0;
	}
	echo "<b><a href='index.php?start=".$prevstart."&amp;storytopic=".$xoopsOption['storytopic']."&amp;storycat=".$xoopsOption['storycat']."&amp;storynum=".$xoopsOption['storynum']."'>"._NW_PREVPAGE."</a></b>\n";
}
echo "</td><td align='right'>\n";

if($start + $xoopsOption['storynum'] < $storycount){
	$nextstart = $start + $xoopsOption['storynum'];
	
	// check whether to display next page or return to top link
	echo "<b><a href='index.php?start=".$nextstart."&amp;storytopic=".$xoopsOption['storytopic']."&amp;storycat=".$xoopsOption['storycat']."&amp;storynum=".$xoopsOption['storynum']."'>"._NW_NEXTPAGE."</a></b>\n";
} elseif($start != 0) {
	echo "<b><a href='index.php?start=0&amp;storytopic=".$xoopsOption['storytopic']."&amp;storycat=".$xoopsOption['storycat']."&amp;storynum=".$xoopsOption['storynum']."'>"._NW_RETURNTOP."</a></b>\n";
} else {
}
echo "</td></tr></table>\n";
echo "</div>\n";

include_once($xoopsConfig['root_path']."footer.php");


function indexnav(){
	global $xoopsConfig, $xoopsOption, $xoopsDB, $PHP_SELF;
	echo "<br />";
	echo "<form name='form1' action='".$PHP_SELF."' method='get'><div align='center'>\n";
	$mf = new XoopsTree($xoopsDB->prefix("topics"),"topicid","pid");
	$mf->makeMySelBox("topictext","topictext",$xoopsOption['storytopic'],1,"storytopic");
	echo "&nbsp;<select name='storynum'>\n";
	
	for($i=5;$i<=30;$i=$i+5){
		$sel = "";
		if($i==$xoopsOption['storynum']){
			$sel = " selected='selected'";
		}
		echo "<option value='$i'$sel>$i</option>\n";
	}
	echo "</select>\n";
	echo "<input type='submit' value='"._NW_GO."' />\n";
	echo "</div></form>\n";
}
?>