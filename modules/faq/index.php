<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

// ORIGINAL FILE INFO
/* ======================                                               */
/* Based on Automated FAQ                                               */
/* Copyright (c) 2001 by                                                */
/*    Richard Tirtadji AKA King Richard (rtirtadji@hotmail.com)         */
/*    Hutdik Hermawan AKA hotFix (hutdik76@hotmail.com)                 */
/* http://www.phpnuke.web.id                                            */
/*                                                                      */
/* This program is free software. You can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License.       */
/************************************************************************/

include("header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

function ShowFaq($id_cat, $categories) {
    	global $xoopsTheme, $xoopsConfig, $xoopsDB;
	$myts= new MyTextSanitizer;
    	echo"<table><tr><td>
    	<center><h4>".$xoopsConfig['sitename']." "._FQ_FAQ."</h4></center><br>
    	<a name=\"top\"><br>
    	" ._FQ_CATEGORY." <a href=\"index.php\">" ._FQ_MAIN."</a> -> $categories
    	<br><br>
    	<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\">
    	<tr bgcolor=\"".$xoopsTheme['bgcolor3']."\"><td colspan=\"2\"><font color=".$xoopsTheme['textcolor2']."><b>" ._FQ_QUESTION."</b></font></td></tr><tr><td colspan=\"2\">";
    	$result = $xoopsDB->query("select id, id_cat, question from ".$xoopsDB->prefix("faqanswer")." where id_cat='$id_cat' order by id");
    	while(list($id, $id_cat, $question) = $xoopsDB->fetch_row($result)) {
		$question = $myts->makeTboxData4Show($question);
		echo"&nbsp;&nbsp;<strong><big>&middot;</big></strong>&nbsp;<a href=\"#$id\">$question</a><br>";
    	}
    	echo "</td></tr></table>
    	<br>";
}

function ShowFaqAll($id_cat) {
    	global $xoopsTheme, $xoopsDB;
	$myts= new MyTextSanitizer;
    	echo"
    	<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\">
    	<tr bgcolor=\"".$xoopsTheme['bgcolor2']."\"><td colspan=\"2\"><font color=".$xoopsTheme['textcolor2']."><b>" ._FQ_ANSWER."</b></font></td></tr>";
    	$result = $xoopsDB->query("select id, id_cat, question, answer from ".$xoopsDB->prefix("faqanswer")." where id_cat='$id_cat' order by id");
    	while(list($id, $id_cat, $question, $answer) = $xoopsDB->fetch_row($result)){
		$question = $myts->makeTboxData4Show($question);
		$answer = $myts->makeTareaData4Show($answer, 0);
		echo"<tr><td><a name=\"$id\">&nbsp;&nbsp;<strong>&middot;</strong>&nbsp;<b>".$question."</b></a><br>
		<p align=\"justify\">".$answer."</p>
		<a href=\"#top\">" ._FQ_BACKTOTOP."</a>
		<br><br>
		</td></tr>";
    	}
    	echo"</table><br><br>
    	<div align=\"center\"><b>[ <a href=\"index.php\">" ._FQ_BACKTOINDEX."</a> ]</b></div></td></tr></table>";
}


if (!$myfaq) {
    	if($xoopsConfig['startpage'] == "faq"){
		$xoopsOption['show_rblock'] =1;
		include($xoopsConfig['root_path']."header.php");
		make_cblock();
	}else{
		$xoopsOption['show_rblock'] =0;
		include($xoopsConfig['root_path']."header.php");
	}
	$myts= new MyTextSanitizer;
    	OpenTable();
    	echo"<center><h4>"._FQ_FAQ."</h4></center><br>
    	<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\">
    	<tr bgcolor=\"".$xoopsTheme['bgcolor2']."\"><td colspan=\"2\"><font color=".$xoopsTheme['textcolor2']."><b>" ._FQ_CATEGORIES."</b></font></td></tr><tr><td>";
    $result = $xoopsDB->query("select id_cat, categories from ".$xoopsDB->prefix("faqcategories"));
    	while(list($id_cat, $categories) = $xoopsDB->fetch_row($result)) {
		$categories=$myts->makeTboxData4Show($categories);
		$catname = urlencode($categories);
		echo "&nbsp;&nbsp;<strong><big>&middot;</big></strong>&nbsp;<a href=\"index.php?myfaq=yes&id_cat=".$id_cat."&categories=".$catname."\">".$categories."</a><br>";
    	}
	echo"</td></tr></table>";
    	CloseTable();
    	
    	include ("../../footer.php");
} else {
    	if($xoopsConfig['startpage'] == "faq"){
		$xoopsOption['show_rblock'] =1;
		include($xoopsConfig['root_path']."header.php");
		make_cblock();
		echo "<br />";
	}else{
		$xoopsOption['show_rblock'] =0;
		include($xoopsConfig['root_path']."header.php");
	}
	OpenTable();
    	ShowFaq($id_cat, $categories);
    	ShowFaqAll($id_cat);
    	CloseTable();
    	include("../../footer.php");
}
?>