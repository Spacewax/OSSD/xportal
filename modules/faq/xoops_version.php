<?php
$modversion['name'] = _MI_FAQ_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_FAQ_DESC;
$modversion['credits'] = "The XOOPS Project";
$modversion['author'] = "Richard Tirtadji AKA King Richard<br />( rtirtadji@hotmail.com )<br />Hutdik Hermawan AKA hotFix<br />( hutdik76@hotmail.com )<br />http://www.phpnuke.web.id";
$modversion['help'] = "faq.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "faq_slogo.jpg";
$modversion['dirname'] = "faq";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Menu
$modversion['hasMain'] = 1;

?>