<?php

// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// ORIGINAL FILE INFO
/* ======================                                               */
/* Based on Automated FAQ                                               */
/* Copyright (c) 2001 by                                                */
/*    Richard Tirtadji AKA King Richard (rtirtadji@hotmail.com)         */
/*    Hutdik Hermawan AKA hotFix (hutdik76@hotmail.com)                 */
/* http://www.phpnuke.web.id                                            */
/*                                                                      */
/* This program is free software. You can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License.       */
/************************************************************************/

include_once("admin_header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

/*********************************************************/
/* Faq Admin Function                                    */
/*********************************************************/

function FaqAdmin() {
    	global $xoopsDB, $xoopsTheme, $xoopsConfig, $xoopsModule;
    	$myts = new MyTextSanitizer;
    	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	OpenTable();
    	echo "
   	<div align='center'><h4>"._FQ_FAQ."</h4></div>
    	<form action='index.php' method='post'>
    	<table border='1' width='100%' align='center'><tr bgcolor='".$xoopsTheme['bgcolor3']."'>
	<td align='center'><font color='".$xoopsTheme['textcolor2']."'>"._FQ_ID."</font></td>
	<td align='center'><font color='".$xoopsTheme['textcolor2']."'>"._FQ_CATEGORIES."</font></td><td>&nbsp</td></tr>";

    	$result = $xoopsDB->query("SELECT id_cat, categories FROM ".$xoopsDB->prefix("faqcategories")." ORDER BY id_cat");
    	while(list($id_cat, $categories) = $xoopsDB->fetch_row($result)) {
		$categories=$myts->makeTboxData4Show($categories);
		echo "
		<td align='center'>$id_cat</td>
		<td align='center'>$categories</td>
		<td align='center'><a href='index.php?op=FaqCatGo&amp;id_cat=".$id_cat."'>" ._FQ_QANDA."</a> | <a href='index.php?op=FaqCatEdit&amp;id_cat=".$id_cat."'>"._FQ_EDIT."</a> | <a href='index.php?op=FaqCatDel&amp;id_cat=".$id_cat."&amp;ok=0'>"._FQ_DELETE."</a></td><tr>";
    	}
    	echo "</form></td></tr></table>
    	<br /><br />
    	<h4>"._FQ_ADDCAT."</h4><br />
    	<form action='index.php' method='post'>
    	<table border='0' width='100%'><tr><td>
    	"._FQ_CATEGORY." </td><td><input type='text' name='categories' size='31' /></td></tr><tr><td>
    	</td></tr></table>
    	<input type='hidden' name='op' value='FaqCatAdd' />
    	<input type='submit' value='"._FQ_ADD."' />
    	</form>";
	CloseTable();
}

function FaqCatGo($id_cat) {
    	global $xoopsDB, $xoopsTheme, $xoopsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
        include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	OpenTable();
    	echo "
    	<div align='center'><h4>"._FQ_QANDA."</h4></div>
    	<form action='index.php' method='post'>
    	<table align='center' border='1' width='100%'><tr bgcolor='".$xoopsTheme['bgcolor3']."'>
	<td align='center'><font color='".$xoopsTheme['textcolor2']."'>"._FQ_QUESTION."</font></td><td>&nbsp</td></tr>";

    	$result = $xoopsDB->query("SELECT id, question, answer FROM ".$xoopsDB->prefix("faqanswer")." WHERE id_cat='$id_cat' ORDER BY id");
    	while(list($id, $question, $answer) = $xoopsDB->fetch_row($result)) {
	
    		$question = $myts->makeTboxData4Show($question);
    		$answer = $myts->makeTareaData4Show($answer,0);	
		echo "
		<tr><td align='center'>".$question."</td><td rowspan='2'><a href='index.php?op=FaqCatGoEdit&amp;id=".$id."'>"._FQ_EDIT."</a> | <a href='index.php?op=FaqCatGoDel&amp;id=".$id."&amp;ok=0'>"._FQ_DELETE."</a></td></tr>
		<tr><td>".$answer."</td></tr>";
    	}
    	echo "</form></td></tr></table>
    	<br /><br />
    	<h4>"._FQ_ADDQ."</h4><br />
    	<form action='index.php' method='post'>
    	<table border='0' width='100%'><tr><td>
    	"._FQ_QUESTION." </td><td><input type='text' name='question' size='31' /></td></tr><tr><td>
    	"._FQ_ANSWER." </td><td><textarea name='answer' cols='60' rows='5'></textarea></td></tr><tr><td>
    	</td></tr></table>
    	<input type='hidden' name='id_cat' value='".$id_cat."' />
    	<input type='hidden' name='op' value='FaqCatGoAdd' />
    	<input type='submit' value='"._FQ_ADD."' />
    	</form>";
    	CloseTable();
}

function FaqCatEdit($id_cat) {
    	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
    	$result = $xoopsDB->query("select categories from ".$xoopsDB->prefix("faqcategories")." where id_cat=".$id_cat."");
    	list($categories) = $xoopsDB->fetch_row($result);
	$categories = $myts->makeTboxData4Edit($categories);
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	OpenTable();
    	echo "
    	<div align='center'><h4>"._FQ_EDITCAT."</h4></div>
    	<form action='index.php' method='post'>
    	<input type='hidden' name='id_cat' value='".$id_cat."' />
    	<table border='0' width='100%'><tr><td>
    	"._FQ_CATEGORY." </td><td><input type='text' name='categories' size='31' value='".$categories."' /></td></tr><tr><td>
    	</td></tr></table>
    	<input type='hidden' name='op' value='FaqCatSave' />
    	<input type='submit' value='"._FQ_SAVECHANGES."' />
    	</form>";
    	CloseTable();
}

function FaqCatGoEdit($id) {
    	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
    	include($xoopsConfig['root_path']."header.php");
    	$result = $xoopsDB->query("SELECT question, answer FROM ".$xoopsDB->prefix("faqanswer")." WHERE id='$id'");
    	list($question, $answer) = $xoopsDB->fetch_row($result);
	$question = $myts->makeTboxData4Edit($question);
	$answer = $myts->makeTareaData4Edit($answer);
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	OpenTable();
    	echo "
    	<h4>"._FQ_EDITQANDA."</h4>
    	<form action='index.php' method='post'>
    	<input type='hidden' name='id' value='".$id."' />
    	<table border='0' width='100%'><tr><td>
    	"._FQ_QUESTION." </td><td><input type='text' name='question' size='31' value='".$question."' /></td></tr><tr><td>
    	"._FQ_ANSWER." </td><td><textarea name='answer' cols='60' rows='5'>".$answer."</textarea></td></tr><tr><td>    
    	</td></tr></table>
    	<input type='hidden' name='op' value='FaqCatGoSave' />
    	<input type='submit' value='"._FQ_SAVECHANGES."' />
    	</form>";
    	CloseTable();
}


function FaqCatSave($id_cat, $categories) {
	global $xoopsDB;
    	$myts = new MyTextSanitizer;
    	$categories = $myts->makeTboxData4Save($categories);
    	$xoopsDB->query("update ".$xoopsDB->prefix("faqcategories")." set categories='".$categories."' where id_cat=".$id_cat."");
    	redirect_header("index.php?op=FaqAdmin",1,_FQ_DBSUCCESS);
	exit();
}

function FaqCatGoSave($id, $question, $answer) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
    	$question = $myts->makeTboxData4Save($question);
    	$answer = $myts->makeTareaData4Save($answer);	
    	$xoopsDB->query("update ".$xoopsDB->prefix("faqanswer")." set question='".$question."', answer='".$answer."' where id=".$id."");
    	redirect_header("index.php?op=FaqAdmin",1,_FQ_DBSUCCESS);
	exit();
}

function FaqCatAdd($categories) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
    	$categories = $myts->makeTboxData4Save($categories);
	$newid = $xoopsDB->GenID("faq_id_cat_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("faqcategories")." (id_cat, categories) VALUES ($newid, '".$categories."')");
    	redirect_header("index.php?op=FaqAdmin",1,_FQ_DBSUCCESS);
	exit();
}

function FaqCatGoAdd($id_cat, $question, $answer) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
    	$question = $myts->makeTboxData4Save($question);
    	$answer = $myts->makeTareaData4Save($answer);
	$newid = $xoopsDB->GenID("faqanswer_id_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("faqanswer")." (id, id_cat, question, answer) VALUES (".$newid.", ".$id_cat.", '".$question."', '".$answer."')");
    	redirect_header("index.php?op=FaqCatGo&id_cat=".$id_cat."",1,_FQ_DBSUCCESS);
	exit();
}

function FaqCatDel($id_cat, $ok=0) {
    	global $xoopsDB, $xoopsConfig, $xoopsModule;
    	if($ok==1) {
		$xoopsDB->query("delete from ".$xoopsDB->prefix("faqcategories")." where id_cat=".$id_cat."",1);
		$xoopsDB->query("delete from ".$xoopsDB->prefix("faqanswer")." where id_cat=".$id_cat."",1);
		redirect_header("index.php?op=FaqAdmin",1,_FQ_DBSUCCESS);
		exit();
    	} else {
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		OpenTable();
		echo "<div align='center'><br />";
		echo "<h4><font color='#ff0000'>"._FQ_RUSUREFAQ."</font></h4><br />";
		echo "[ <a href='index.php?op=FaqCatDel&amp;id_cat=".$id_cat."&amp;ok=1'>"._FQ_YES."</a> | <a href='index.php?op=FaqAdmin'>"._FQ_NO."</a> ]</div>";
		CloseTable();
	}
}

function FaqCatGoDel($id, $ok=0) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
    	if($ok==1) {
		$xoopsDB->query("delete from ".$xoopsDB->prefix("faqanswer")." where id=".$id."");
		redirect_header("index.php?op=FaqAdmin",1,_FQ_DBSUCCESS);
		exit();
    	} else {
        	include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		OpenTable();
		echo "<div align='center'><br />";
		echo "<h4><font color='#ff0000'>"._FQ_RUSUREQ."</font></h4><br />";
		echo "[ <a href='index.php?op=FaqCatGoDel&amp;id=".$id."&amp;ok=1'>"._FQ_YES."</a> | <a href='index.php?op=FaqAdmin'>"._FQ_NO."</a> ]</div>";
		CloseTable();
	}
}

switch ($op) {
	default:
        	FaqAdmin();
                break;
        case "FaqCatSave":
                FaqCatSave($id_cat, $categories);
                break;
        case "FaqCatGoSave":
                FaqCatGoSave($id, $question, $answer);
                break;
        case "FaqCatAdd":
                FaqCatAdd($categories);
                break;
        case "FaqCatGoAdd":
                FaqCatGoAdd($id_cat, $question, $answer);
                break;
        case "FaqCatEdit":
                FaqCatEdit($id_cat);
                break;
        case "FaqCatGoEdit":
                FaqCatGoEdit($id);
                break;
        case "FaqCatDel":
                FaqCatDel($id_cat, $ok);
                break;
        case "FaqCatGoDel":
                FaqCatGoDel($id, $ok);
                break;
        case "FaqAdmin":
                FaqAdmin();
                break;
        case "FaqCatGo":
                FaqCatGo($id_cat);
                break;
}
include("admin_footer.php");
?>