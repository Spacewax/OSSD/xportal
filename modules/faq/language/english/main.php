<?php
//%%%%%%	Module Name 'FAQ'		  %%%%%
define("_FQ_FAQ","FAQ (Frequently Asked Questions)");
define("_FQ_CATEGORY","Category:");
define("_FQ_MAIN","Main");
define("_FQ_QUESTION","Question");
define("_FQ_ANSWER","Answer");
define("_FQ_BACKTOTOP","Back to Top");
define("_FQ_BACKTOINDEX","Back to FAQ Index");
define("_FQ_CATEGORIES","Categories");

//%%%%%%	Module Name 'FAQ' (Admin)	  %%%%%
define("_FQ_FAQCONFIG","FAQ Configuration");
define("_FQ_ID","ID");
define("_FQ_QANDA","Question & Answer");
define("_FQ_EDIT","Edit");
define("_FQ_DELETE","Delete");
define("_FQ_ADDCAT","Add Categories");
define("_FQ_ADD","Add");
define("_FQ_ADDQ","Add Question");
define("_FQ_EDITCAT","Edit Categories");
define("_FQ_SAVECHANGES","Save Changes");
define("_FQ_EDITQANDA","Edit Question and Answer");
define("_FQ_RUSUREFAQ","Are you sure you want to delete this Faq and all its question?");
define("_FQ_RUSUREQ","Are you sure you want to delete this Question?");
define("_FQ_YES","Yes");
define("_FQ_NO","No");
define("_FQ_DBSUCCESS","Database Updated Successfully!");
?>