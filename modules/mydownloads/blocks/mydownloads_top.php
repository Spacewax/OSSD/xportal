<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
/******************************************************************************
 * Function: b_mydownloads_top_show
 * Input   : $options[0] = date for the most recent downloads
 *                    hits for the most popular downloads
 *           $block['content'] = The optional above content
 *           $options[1]   = How many downloads are displayes
 * Output  : Returns the most recent or most popular downloads
 ******************************************************************************/
function b_mydownloads_top_show($options) {
	global $xoopsDB, $xoopsConfig;
	$block = array();
	$myts = new MyTextSanitizer();
	//$order = date for most recent reviews
	//$order = hits for most popular reviews
	$result = $xoopsDB->query("SELECT lid, title, date, hits FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE status>0 ORDER BY ".$options[0]." DESC",0,$options[1],0);
	$block['content'] = "<small>";
	while($myrow=$xoopsDB->fetch_array($result)){
		$title = $myts->makeTboxData4Show($myrow["title"]);
		if ( !XOOPS_USE_MULTIBYTES ) {
			if (strlen($title) >= 19) {
				$title = substr($title,0,18)."...";
			}
		}
		$block['content'] .= "&nbsp;&nbsp;<strong><big>&middot;</big></strong>&nbsp;<a href=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/singlefile.php?lid=".$myrow['lid']."\">$title</a> ";
		if($options[0] == "date"){
			$block['content'] .= "(".formatTimestamp($myrow['date'],"s").")<br />";
			$block['title'] = _MB_MYDOWNLOADS_TITLE1;
		}elseif($options[0] == "hits"){
			$block['content'] .= "(".$myrow['hits'].")<br />";
			$block['title'] = _MB_MYDOWNLOADS_TITLE2;
		}
	}
    	$block['content'] .= "</small>";
	return $block;
}

function b_mydownloads_top_edit($options) {
	$form = ""._MB_MYDOWNLOADS_DISP."&nbsp;";
	$form .= "<input type=\"hidden\" name=\"options[]\" value=\"";
	if($options[0] == "date"){
		$form .= "date\"";
	}else {
		$form .= "hits\"";
	}
	$form .= " />";
	$form .= "<input type=\"text\" name=\"options[]\" value=\"".$options[1]."\" />&nbsp;"._MB_MYDOWNLOADS_FILES."";
	return $form;
}
?>