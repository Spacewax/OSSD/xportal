<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("admin_header.php");
include("../include/functions.php");
include("../include/config.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
$myts = new MyTextSanitizer;
$eh = new ErrorHandler;
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"),"cid","pid");

function mydownloads() {
	global $xoopsDB;

        OpenTable();
        // Temporarily 'homeless' downloads (to be revised in index.php breakup)
	$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_broken")."");
        list($totalbrokendownloads) = $xoopsDB->fetch_row($result);
        if($totalbrokendownloads>0){
                $totalbrokendownloads = "<font color=\"#ff0000\"><b>$totalbrokendownloads</b></font>";
        }
        $result2 = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_mod")."");
        list($totalmodrequests) = $xoopsDB->fetch_row($result2);
        if($totalmodrequests>0){
        	$totalmodrequests = "<font color=\"#ff0000\"><b>$totalmodrequests</b></font>";
        }
        $result3 = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE status=0");
        list($totalnewdownloads) = $xoopsDB->fetch_row($result3);
        if($totalnewdownloads>0){
                $totalnewdownloads = "<font color=\"#ff0000\"><b>$totalnewdownloads</b></font>";
        }
        echo " - <a href=index.php?op=mydownloadsConfigAdmin>"._MD_GENERALSET."</a>";
        echo "<br><br>";
        echo " - <a href=index.php?op=downloadsConfigMenu>"._MD_ADDMODDELETE."</a>";
        echo "<br><br>";
        echo " - <a href=index.php?op=listNewDownloads>"._MD_DLSWAITING." ($totalnewdownloads)</a>";
        echo "<br><br>";
        echo " - <a href=index.php?op=listBrokenDownloads>"._MD_BROKENREPORTS." ($totalbrokendownloads)</a>";
        echo "<br><br>";
        echo " - <a href=index.php?op=listModReq>"._MD_MODREQUESTS." ($totalmodrequests)</a>";
	$result=$xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE status>0");
        list($numrows) = $xoopsDB->fetch_row($result);
	echo "<br><br><div align=\"center\">";
	printf(_MD_THEREARE,$numrows);	echo "</div>";
        CloseTable();
}

function listNewDownloads(){
        global $xoopsDB, $xoopsConfig, $myts, $eh, $mytree;
// List downloads waiting for validation
        $result = $xoopsDB->query("SELECT lid, cid, title, url, homepage, version, size, platform, logourl, submitter FROM ".$xoopsDB->prefix("mydownloads_downloads")." where status=0 ORDER BY date DESC");
        $numrows = $xoopsDB->num_rows($result);
        OpenTable();
        echo "<h4>"._MD_DLSWAITING."&nbsp;($numrows)</h4><br>";
        if ($numrows>0) {
                while(list($lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $uid) = $xoopsDB->fetch_row($result)) {
                	$result2 = $xoopsDB->query("SELECT description FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=$lid");
                	list($description) = $xoopsDB->fetch_row($result2);
                	$title = $myts->makeTboxData4Edit($title);
                	$url = $myts->makeTboxData4Edit($url);
                	$homepage = $myts->makeTboxData4Edit($homepage);
                	$version = $myts->makeTboxData4Edit($version);
                	$size = $myts->makeTboxData4Edit($size);
                	$platform = $myts->makeTboxData4Edit($platform);
                	$description = $myts->makeTareaData4Edit($description);
                	$submitter = XoopsUser::get_uname_from_id($uid);
                	echo "<form action=\"index.php\" method=post>\n";
                	echo "<table width=\"80%\">";
                	echo "<tr><td align=\"right\" nowrap>"._MD_SUBMITTER."</td><td>\n";
                	echo "<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$uid."\">$submitter</a>";
                	echo "</td></tr>\n";
                	echo "<tr><td align=\"right\" nowrap>"._MD_FILETITLE."</td><td>";
                	echo "<input type=\"text\" name=\"title\" size=\"50\" maxlength=\"100\" value=\"$title\">";
                	echo "</td></tr><tr><td align=\"right\" nowrap>"._MD_DLURL."</td><td>";
                	echo "<input type=\"text\" name=\"url\" size=\"50\" maxlength=\"250\" value=\"$url\">";
                	echo "&nbsp;[&nbsp;<a href=\"$url\">"._MD_DOWNLOAD."</a>&nbsp;]";
                	echo "</td></tr>";
                	echo "<tr><td align=\"right\" nowrap>"._MD_CATEGORYC."</td><td>";
                	$mytree->makeMySelBox("title", "title", $cid);
                	echo "</td></tr>\n";
                	echo "<tr><td align=\"right\" nowrap>"._MD_HOMEPAGEC."</td><td>\n";
                	echo "<input type=\"text\" name=\"homepage\" size=\"50\" maxlength=\"100\" value=\"$homepage\"></td></tr>\n";
        		echo "<tr><td align=\"right\">"._MD_VERSIONC."</td><td>\n";
        		echo "<input type=\"text\" name=\"version\" size=\"10\" maxlength=\"10\" value=\"$version\"></td></tr>\n";
        		echo "<tr><td align=\"right\">"._MD_FILESIZEC."</td><td>\n";
        		echo "<input type=\"text\" name=\"size\" size=\"10\" maxlength=\"8\" value=\"$size\">"._MD_BYTES."</td></tr>\n";
        		echo "<tr><td align=\"right\">"._MD_PLATFORMC."</td><td>\n";
        		echo "<input type=\"text\" name=\"platform\" size=\"45\" maxlength=\"50\" value=\"$platform\"></td></tr>\n";

                	echo "<tr><td align=\"right\" valign=\"top\" nowrap>"._MD_DESCRIPTIONC."</td><td>\n";
                	echo "<textarea name=description cols=\"60\" rows=\"5\">$description</textarea>\n";
                	echo "</td></tr>\n";
                	echo "<tr><td align=\"right\" nowrap>"._MD_SHOTIMAGE."</td><td>\n";
                	echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\"></td></tr>\n";
                	echo "<tr><td></td><td>";
			$directory = $xoopsConfig['xoops_url']."/modules/mydownloads/images/shots/";
			printf(_MD_MUSTBEVALID,$directory);
                	echo "</table>\n";
                	echo "<br><input type=\"hidden\" name=\"op\" value=\"approve\"></input>";
                	echo "<input type=\"hidden\" name=\"lid\" value=\"$lid\"></input>";
                	echo "<input type=\"submit\" value=\""._MD_APPROVE."\">&nbsp;<input type=\"button\" value=\""._MD_DELETE."\" onclick=\"location='index.php?op=delNewDownload&lid=$lid'\"></form><br><br>";
                }
	}else{
		echo _MD_NOSUBMITTED;
        }
        CloseTable();
}


function downloadsConfigMenu(){
        global $xoopsDB, $xoopsConfig, $myts, $eh, $mytree;
// Add a New Main Category
	OpenTable();
	echo "<form method=post action=index.php>\n";
        echo "<h4>"._MD_ADDMAIN."</h4><br>"._MD_TITLEC."<input type=text name=title size=30 maxlength=50><br>";
        echo _MD_IMGURL."<br><input type=\"text\" name=\"imgurl\" size=\"100\" maxlength=\"150\" value=\"http://\"><br><br>";
        echo "<input type=hidden name=cid value=0>\n";
        echo "<input type=hidden name=op value=addCat>";
        echo "<input type=submit value="._MD_ADD."><br></form>";
	CloseTable();
	echo "<br>";
	// Add a New Sub-Category
        $result=$xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_cat")."");
        list($numrows)=$xoopsDB->fetch_row($result);
        if($numrows>0) {
                OpenTable();
                echo "<form method=post action=index.php>";
                echo "<h4>"._MD_ADDSUB."</h4><br />"._MD_TITLEC."<input type=text name=title size=30 maxlength=50>&nbsp;"._MD_IN."&nbsp;";
                $mytree->makeMySelBox("title", "title");
#               echo "<br>"._MD_IMGURL."<br><input type=\"text\" name=\"imgurl\" size=\"100\" maxlength=\"150\">\n";
                echo "<input type=hidden name=op value=addCat><br><br>";
                echo "<input type=submit value="._MD_ADD."><br></form>";
                CloseTable();
                echo "<br>";
		// If there is a category, add a New Download

                OpenTable();
                echo "<form method=post action=index.php>\n";
                echo "<h4>"._MD_ADDNEWFILE."</h4><br>\n";
                echo "<table width=\"80%\"><tr>\n";
                echo "<td align=\"right\">"._MD_FILETITLE."</td><td>";
                echo "<input type=text name=title size=50 maxlength=100>";
                echo "</td></tr><tr><td align=\"right\" nowrap>"._MD_DLURL."</td><td>";
                echo "<input type=text name=url size=50 maxlength=100 value=\"http://\">";
                echo "</td></tr>";
                echo "<tr><td align=\"right\" nowrap>"._MD_CATEGORYC."</td><td>";
                $mytree->makeMySelBox("title", "title");
                echo "</td></tr><tr><td></td><td></td></tr>\n";
                echo "<tr><td align=\"right\" nowrap>"._MD_HOMEPAGEC."</td><td>\n";
                echo "<input type=text name=homepage size=50 maxlength=100></td></tr>\n";
        	echo "<tr><td align=\"right\">"._MD_VERSIONC."</td><td>\n";
        	echo "<input type=text name=version size=10 maxlength=10></td></tr>\n";
        	echo "<tr><td align=\"right\">"._MD_FILESIZEC."</td><td>\n";
        	echo "<input type=text name=size size=10 maxlength=100>"._MD_BYTES."</td></tr>\n";
        	echo "<tr><td align=\"right\">"._MD_PLATFORMC."</td><td>\n";
        	echo "<input type=text name=platform size=45 maxlength=60></td></tr>\n";
                echo "<tr><td align=\"right\" valign=\"top\" nowrap>"._MD_DESCRIPTIONC."</td><td>\n";
                echo "<textarea name=description cols=60 rows=5></textarea>\n";
                echo "</td></tr>\n";
                echo "<tr><td align=\"right\"nowrap>"._MD_SHOTIMAGE."</td><td>\n";
                echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\"></td></tr>\n";
                echo "<tr><td align=\"right\"></td><td>";
		$directory = $xoopsConfig['xoops_url']."/modules/mydownloads/images/shots/";
		printf(_MD_MUSTBEVALID,$directory);
		echo "</td></tr>\n";
                echo "</table>\n<br>";
                echo  "<input type=\"hidden\" name=\"op\" value=\"addDownload\"></input>";
                echo "<input type=\"submit\" class=\"button\" value=\""._MD_ADD."\"></input>\n";
                echo "</form>";
                CloseTable();
                echo "<br>";

	// Modify Category
                OpenTable();
                echo "<form method=post action=index.php><h4>"._MD_MODCAT."</h4><br>";
                echo _MD_CATEGORYC;
                $mytree->makeMySelBox("title", "title");
                echo "<br><br>\n";
                echo "<input type=hidden name=op value=modCat>\n";
                echo "<input type=submit value="._MD_MODIFY.">\n";
                echo "</form>";
                CloseTable();
                echo "<br>";
	}
	// Modify Download
        $result2 = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_downloads")."");
        list($numrows2) = $xoopsDB->fetch_row($result2);
        if ($numrows2>0) {
        	OpenTable();
                echo "<form method=get action=\"index.php\">\n";
                echo "<h4>"._MD_MODDL."</h4><br>\n";
                echo _MD_FILEID."<input type=text name=lid size=12 maxlength=11>\n";
                echo "<input type=hidden name=fct value=mydownloads>\n";
                echo "<input type=hidden name=op value=modDownload><br><br>\n";
                echo "<input type=submit value="._MD_MODIFY."></form>\n";
                echo "</td></tr></table></td></tr></table>";
	}
}

function modDownload() {
	global $xoopsDB, $HTTP_GET_VARS, $xoopsConfig, $myts, $eh, $mytree;
        $lid = $HTTP_GET_VARS['lid'];
        OpenTable();
        $result = $xoopsDB->query("SELECT cid, title, url, homepage, version, size, platform, logourl FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=$lid") or $eh->show("0013");
        echo "<h4>"._MD_MODDL."</h4><br>";
        list($cid, $title, $url, $homepage, $version, $size, $platform, $logourl) = $xoopsDB->fetch_row($result);
        $title = $myts->makeTboxData4Edit($title);
        $url = $myts->makeTboxData4Edit($url);
        $homepage = $myts->makeTboxData4Edit($homepage);
        $version = $myts->makeTboxData4Edit($version);
        $size = $myts->makeTboxData4Edit($size);
        $platform = $myts->makeTboxData4Edit($platform);
        $logourl = $myts->makeTboxData4Edit($logourl);
        $result2 = $xoopsDB->query("SELECT description FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=$lid");
        list($description)=$xoopsDB->fetch_row($result2);
        $description = $myts->makeTareaData4Edit($description);
        echo "<table>";
        echo "<form method=post action=index.php>";
        echo "<tr><td>"._MD_FILEID."</td><td><b>$lid</b></td></tr>";
        echo "<tr><td>"._MD_FILETITLE."</td><td><input type=text name=title value=\"$title\" size=50 maxlength=100></input></td></tr>\n";
        echo "<tr><td>"._MD_DLURL."</td><td><input type=text name=url value=\"$url\" size=50 maxlength=100></input></td></tr>\n";
        echo "<tr><td>"._MD_HOMEPAGEC."</td><td><input type=text name=homepage value=\"$homepage\" size=50 maxlength=100></input></td></tr>\n";
        echo "<tr><td>"._MD_VERSIONC."</td><td><input type=text name=version value=\"$version\" size=10 maxlength=10></input></td></tr>\n";
        echo "<tr><td>"._MD_FILESIZEC."</td><td><input type=text name=size value=\"$size\" size=10 maxlength=100></input>"._MD_BYTES."</td></tr>\n";
        echo "<tr><td>"._MD_PLATFORMC."</td><td><input type=text name=platform value=\"$platform\" size=45 maxlength=60></input></td></tr>\n";
        echo "<tr><td valign=\"top\">"._MD_DESCRIPTIONC."</td><td><textarea name=description cols=60 rows=5>$description</textarea></td></tr>";
        echo "<tr><td>"._MD_CATEGORYC."</td><td>";
        $mytree->makeMySelBox("title", "title", $cid);
        echo "</td></tr>\n";
        echo "<tr><td>"._MD_SHOTIMAGE."</td><td><input type=text name=logourl value=\"$logourl\" size=\"50\" maxlength=\"60\"></input></td></tr>\n";
        echo "<tr><td></td><td>";
	$directory = $xoopsConfig['xoops_url']."/modules/mydownloads/images/shots/";
	printf(_MD_MUSTBEVALID,$directory);
	echo "</td></tr>\n";
        echo "</table>";
        echo "<br><BR><input type=hidden name=lid value=$lid></input>\n";
        echo "<input type=hidden name=op value=modDownloadS><input type=submit value="._MD_SUBMIT.">";
        echo "&nbsp;<input type=button value="._MD_DELETE." onclick=\"javascript:location='index.php?op=delDownload&lid=".$lid."'\">";
        echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\">";
        echo "</form>\n";
        echo "<hr>";

        $result5=$xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_votedata")."");
        list($totalvotes) = $xoopsDB->num_rows($result5);
        echo "<table valign=top width=100%>\n";
        echo "<tr><td colspan=7><b>";
	printf(_MD_DLRATINGS,$totalvotes);
	echo "</b><br><br></td></tr>\n";
        // Show Registered Users Votes
        $result5=$xoopsDB->query("SELECT ratingid, ratinguser, rating, ratinghostname, ratingtimestamp FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE lid = $lid AND ratinguser != 0 ORDER BY ratingtimestamp DESC");
        $votes = $xoopsDB->num_rows($result5);
        echo "<tr><td colspan=7><br><br><b>";
	printf(_MD_REGUSERVOTES,$votes);
	echo "</b><br><br></td></tr>\n";
        echo "<tr><td><b>" ._MD_USER."  </b></td><td><b>" ._MD_IP."  </b></td><td><b>" ._MD_RATING."  </b></td><td><b>" ._MD_USERAVG."  </b></td><td><b>" ._MD_TOTALRATE."  </b></td><td><b>" ._MD_DATE."  </b></td><td align=\"center\"><b>" ._MD_DELETE."</b></td></tr>\n";
        if ($votes == 0){
        	echo "<tr><td align=\"center\" colspan=\"7\">" ._MD_NOREGVOTES."<br></td></tr>\n";
	}
        $x=0;
        $colorswitch="dddddd";
        while(list($ratingid, $ratinguser, $rating, $ratinghostname, $ratingtimestamp)=$xoopsDB->fetch_row($result5)) {
        	$formatted_date = formatTimestamp($ratingtimestamp);
            	//Individual user information
                $result2=$xoopsDB->query("SELECT rating FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE ratinguser = $ratinguser");
                $uservotes = $xoopsDB->num_rows($result2);
                $useravgrating = 0;
                while(list($rating2) = $xoopsDB->fetch_row($result2)){
                        $useravgrating = $useravgrating + $rating2;
                }
                $useravgrating = $useravgrating / $uservotes;
                $useravgrating = number_format($useravgrating, 1);
		$ratinguname = XoopsUser::get_uname_from_id($ratinguser);
                echo "<tr><td bgcolor=\"$colorswitch\">$ratinguname</td><td bgcolor=\"$colorswitch\">$ratinghostname</td><td bgcolor=\"$colorswitch\">$rating</td><td bgcolor=\"$colorswitch\">$useravgrating</td><td bgcolor=\"$colorswitch\">$uservotes</td><td bgcolor=\"$colorswitch\">$formatted_date</td><td bgcolor=\"$colorswitch\" align=\"center\"><b><a href=index.php?op=delVote&lid=$lid&rid=$ratingid>X</a></b></td></tr>\n";
                $x++;
                if ($colorswitch=="dddddd"){
                	$colorswitch="ffffff";
                } else {
                        $colorswitch="dddddd";
                }
	}
        // Show Unregistered Users Votes
        $result5=$xoopsDB->query("SELECT ratingid, rating, ratinghostname, ratingtimestamp FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE lid = $lid AND ratinguser = 0 ORDER BY ratingtimestamp DESC");
        $votes = $xoopsDB->num_rows($result5);
        echo "<tr><td colspan=7><b><br><br>";
	printf(_MD_ANONUSERVOTES,$votes);
	echo "</b><br><br></td></tr>\n";
        echo "<tr><td colspan=2><b>" ._MD_IP."  </b></td><td colspan=3><b>" ._MD_RATING."  </b></td><td><b>" ._MD_DATE."  </b></b></td><td align=\"center\"><b>" ._MD_DELETE."</b></td><br></tr>";
        if ($votes == 0) {
        	echo "<tr><td colspan=\"7\" align=\"center\">" ._MD_NOUNREGVOTES."<br></td></tr>";
        }
        $x=0;
        $colorswitch="dddddd";
        while(list($ratingid, $rating, $ratinghostname, $ratingtimestamp)=$xoopsDB->fetch_row($result5)) {
        	$formatted_date = formatTimestamp($ratingtimestamp);
                echo "<td colspan=\"2\" bgcolor=\"$colorswitch\">$ratinghostname</td><td colspan=\"3\" bgcolor=\"$colorswitch\">$rating</td><td bgcolor=\"$colorswitch\">$formatted_date</td><td bgcolor=\"$colorswitch\" aling=\"center\"><b><a href=index.php?op=delVote&lid=$lid&rid=$ratingid>X</a></b></td></tr>";
                $x++;
                if ($colorswitch=="dddddd") {
                	$colorswitch="ffffff";
                } else {
                        $colorswitch="dddddd";
                }
	}
        echo "<tr><td colspan=\"6\">&nbsp;<br></td></tr>\n";
        echo "</table>\n";
        echo "</td></tr></table></td></tr></table><br>";

        include ("footer.php");
}

function delVote() {
        global $xoopsDB, $HTTP_GET_VARS, $eh;
        $rid = $HTTP_GET_VARS['rid'];
        $lid = $HTTP_GET_VARS['lid'];
        $query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE ratingid=$rid";
        $xoopsDB->query($query) or $eh->show("0013");
        updaterating($lid);
        OpenTable();
        echo _MD_VOTEDELETED;
        CloseTable();
}
function listBrokenDownloads() {
        global $xoopsDB, $eh;
        $result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("mydownloads_broken")." ORDER BY reportid");
        $totalbrokendownloads = $xoopsDB->num_rows($result);
        OpenTable();
        echo "<h4>"._MD_BROKENREPORTS." ($totalbrokendownloads)</h4><br>";

        if ($totalbrokendownloads==0) {
        	echo _MD_NOBROKEN;
        } else {
                echo "<center>"._MD_IGNOREDESC."<br>"._MD_DELETEDESC."</center><br><br><br>";
                $colorswitch="#dddddd";
                echo "<table align=\"center\" width=\"90%\">";
                echo "
                <tr>
                  <td><b>"._MD_FILETITLE."</b></td>
                  <td><b>" ._MD_REPORTER."</b></td>
                  <td><b>" ._MD_FILESUBMITTER."</b></td>
                  <td><b>" ._MD_IGNORE."</b></td>
                  <td><b>" ._MD_DELETE."</b></td>
                </tr>";
                while(list($reportid, $lid, $sender, $ip)=$xoopsDB->fetch_row($result)){
                	$result2 = $xoopsDB->query("SELECT title, url, submitter FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=$lid");
                        if ($sender != 0) {
                                $result3 = $xoopsDB->query("SELECT uname, email FROM ".$xoopsDB->prefix("users")." WHERE uid=".$sender."");
                                list($sendername, $email)=$xoopsDB->fetch_row($result3);
                        }
                        list($title, $url, $owner)=$xoopsDB->fetch_row($result2);
                        $result4 = $xoopsDB->query("SELECT uname, email FROM ".$xoopsDB->prefix("users")." WHERE uid=".$owner."");
                        list($ownername, $owneremail)=$xoopsDB->fetch_row($result4);
                        echo "<tr><td bgcolor=$colorswitch><a href=$url>$title</a></td>";
                        if ($email=="") { 
				echo "<td bgcolor=$colorswitch>$sendername ($ip)";
                        } else {
                                echo "<td bgcolor=$colorswitch><a href=mailto:$email>$sendername</a> ($ip)";
                        }
                        echo "</td>";
                        if ($owneremail=='') {
                                echo "<td bgcolor=$colorswitch>$ownername";
                        } else { 
				echo "<td bgcolor=$colorswitch><a href=mailto:$owneremail>$ownername</a>";
                        }
                        echo "</td><td bgcolor=$colorswitch><center><a href=index.php?op=ignoreBrokenDownloads&lid=$lid>X</a></center></td>";
                        echo "<td bgcolor=$colorswitch><center><a href=index.php?op=delBrokenDownloads&lid=$lid>X</a></center></td></tr>";
                        if ($colorswitch=="#dddddd") {
                                $colorswitch="#ffffff";
                        } else {
                                $colorswitch="#dddddd";
                        }
		}
                echo "</table>";
	}

        CloseTable();
}
function delBrokenDownloads() {
	global $xoopsDB, $HTTP_GET_VARS, $eh;
        $lid = $HTTP_GET_VARS['lid'];
        $query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_broken")." WHERE lid=$lid";
        $xoopsDB->query($query) or $eh->show("0013");
        $query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=$lid";
        $xoopsDB->query($query) or $eh->show("0013");
        OpenTable();
        echo _MD_FILEDELETED;
        CloseTable();
}
function ignoreBrokenDownloads() {
        global $xoopsDB, $HTTP_GET_VARS, $eh;
        $query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_broken")." WHERE lid=".$HTTP_GET_VARS['lid']."";
        $xoopsDB->query($query) or $eh->show("0013");
        OpenTable();
        echo _MD_BROKENDELETED;
        CloseTable();
}
function listModReq() {
        global $xoopsDB, $myts, $eh, $mytree, $mydownloads_useshots, $mydownloads_shotwidth;
        $result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("mydownloads_mod")." ORDER BY requestid");
        $totalmodrequests = $xoopsDB->num_rows($result);
        OpenTable();
        echo "<h4>"._MD_USERMODREQ." ($totalmodrequests)</h4><br>";
        if($totalmodrequests>0){
        	echo "<table width=95%><tr><td>";
        	while(list($requestid, $lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $description, $modifysubmitter)=$xoopsDB->fetch_row($result)) {
                	$result2 = $xoopsDB->query("SELECT cid, title, url, homepage, version, size, platform, logourl, submitter FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=$lid");
                        list($origcid, $origtitle, $origurl, $orighomepage, $origversion, $origsize, $origplatform, $origlogourl, $owner)=$xoopsDB->fetch_row($result2);
                        $result2 = $xoopsDB->query("SELECT description FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=$lid");
                        list($origdescription) = $xoopsDB->fetch_row($result2);
                        $result7 = $xoopsDB->query("SELECT uname, email FROM ".$xoopsDB->prefix("users")." WHERE uid=$modifysubmitter");
                        $result8 = $xoopsDB->query("SELECT uname, email FROM ".$xoopsDB->prefix("users")." WHERE uid=$owner");
                        $cidtitle=$mytree->getPathFromId($cid, "title");
                        $origcidtitle=$mytree->getPathFromId($origcid, "title");
                        list($submittername, $submitteremail)=$xoopsDB->fetch_row($result7);
                        list($ownername, $owneremail)=$xoopsDB->fetch_row($result8);
                        $title = $myts->makeTboxData4Show($title);
                        $url = $myts->makeTboxData4Show($url);
                        $homepage = $myts->makeTboxData4Show($homepage);
                        $version = $myts->makeTboxData4Show($version);
                        $size = $myts->makeTboxData4Show($size);
                        $platform = $myts->makeTboxData4Show($platform);

// use original image file to prevent users from changing screen shots file
                        $origlogourl = $myts->makeTboxData4Edit($origlogourl);
                        $logourl = $origlogourl;
                        $description = $myts->makeTareaData4Show($description);
                        $origurl = $myts->makeTboxData4Show($origurl);
                        $orighomepage = $myts->makeTboxData4Show($orighomepage);
                        $origversion = $myts->makeTboxData4Show($origversion);
                        $origsize = $myts->makeTboxData4Show($origsize);
                        $origplatform = $myts->makeTboxData4Show($origplatform);
                        $origdescription = $myts->makeTareaData4Show($origdescription);
			if ($ownerid=="") {
                                $ownername = "administration";
                        }
                        echo "<table border=1 bordercolor=black cellpadding=5 cellspacing=0 align=center width=450><tr><td>
                               <table width=100% bgcolor=dddddd>
                                 <tr>
                                   <td valign=top width=45%><b>"._MD_ORIGINAL."</b></td>
                               <td rowspan=14 valign=top align=left><br>"._MD_DESCRIPTIONC."<br>$origdescription</td>
                                 </tr>
                                 <tr><td valign=top width=45%><small>"._MD_FILETITLE." ".$origtitle."</small></td></tr>
                                 <tr><td valign=top width=45%><small>"._MD_DLURL." ".$origurl."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_CATEGORYC." ".$origcidtitle."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_HOMEPAGEC." ".$orighomepage."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_VERSIONC." ".$origversion."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_FILESIZEC." ".$origsize."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_PLATFORMC." ".$origplatform."</small></td></tr>

                             <tr><td valign=top width=45%><small>"._MD_SHOTIMAGE."</small> ";
			if($mydownloads_useshots){
				echo "<img src=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/images/shots/".$origlogourl."\" width=\"".$mydownloads_shotwidth."\">";
			}else{
				echo "&nbsp;";
			}
			echo "</td></tr>
                               </table></td></tr><tr><td>
                               <table width=100%>
                                 <tr>
                                   <td valign=top width=45%><b>"._MD_PROPOSED."</b></td>
                                   <td rowspan=14 valign=top align=left><br>"._MD_DESCRIPTIONC."<br>$description</td>
                                 </tr>
                                 <tr><td valign=top width=45%><small>"._MD_FILETITLE." ".$title."</small></td></tr>
                                 <tr><td valign=top width=45%><small>"._MD_DLURL." ".$url."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_CATEGORYC." ".$cidtitle."</small></td></tr>
                        <tr><td valign=top width=45%><small>"._MD_HOMEPAGEC." ".$homepage."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_VERSIONC." ".$version."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_FILESIZEC." ".$size."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_PLATFORMC." ".$platform."</small></td></tr>
                             <tr><td valign=top width=45%><small>"._MD_SHOTIMAGE."</small> ";
			if($mydownloads_useshots){
				echo "<img src=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/images/shots/".$logourl."\" width=\"".$mydownloads_shotwidth."\">";
			}else{
				echo "&nbsp;";
			}
			echo "</td></tr>
                               </table></td></tr></table>
                            <table align=center width=450>
                              <tr>";
			if ($submitteremail=="") {
                        	echo "<td align=left><small>"._MD_SUBMITTER." $submittername</small></td>";
                        } else {
                                echo "<td align=left><small>"._MD_SUBMITTER." <a href=mailto:$submitteremail>$submittername</a></small></td>";
                        }
                        if ($owneremail=="") {
                                echo "<td align=center><small>"._MD_OWNER." $ownername</small></td>";
                        } else {
                                echo "<td align=center><small>"._MD_OWNER." <a href=mailto:$owneremail>$ownername</a></small></td>";
                        }
                        echo "<td align=right><small>( <a href=index.php?op=changeModReq&requestid=$requestid>"._MD_APPROVE."</a> / <a href=index.php?op=ignoreModReq&requestid=$requestid>"._MD_IGNORE."</a> )</small></td></tr>\n";
                        echo "</table><br><br>";
		}
                echo "</td></tr></table>";
        }else {
                echo _MD_NOMODREQ;
        }
        CloseTable();
}
function changeModReq() {
        global $xoopsDB, $HTTP_GET_VARS, $eh, $myts;
        $requestid = $HTTP_GET_VARS['requestid'];
        $query = "SELECT lid, cid, title, url, homepage, version, size, platform, logourl, description FROM ".$xoopsDB->prefix("mydownloads_mod")." WHERE requestid=$requestid";
        $result = $xoopsDB->query($query);
        while(list($lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $description)=$xoopsDB->fetch_row($result)) {
        	if (get_magic_quotes_runtime()) {
                	$title = stripslashes($title);
                    	$url = stripslashes($url);
                    	$homepage = stripslashes($homepage);
                    	$logourl = stripslashes($logourl);
                    	$description = stripslashes($description);
        	}
        	$title = addslashes($title);
            	$url = addslashes($url);
            	$homepage = addslashes($homepage);
            	$logourl = addslashes($logourl);
            	$description = addslashes($description);
            	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("mydownloads_downloads")." SET cid=$cid,title='$title',url='$url',homepage='$homepage',version='$version' ,size=$size ,platform='$platform' ,logourl='$logourl', status=2, date=".time()." WHERE lid=$lid") or $eh->show("0013");
        	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("mydownloads_text")." SET description='$description' WHERE lid=$lid") or $eh->show("0013");
            	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_mod")." WHERE requestid=$requestid") or $eh->show("0013");
	}
        OpenTable();
        echo _MD_DBUPDATED;
        CloseTable();
}
function ignoreModReq() {
	global $xoopsDB, $HTTP_GET_VARS, $eh;
	$query= "DELETE FROM ".$xoopsDB->prefix("mydownloads_mod")." WHERE requestid=".$HTTP_GET_VARS['requestid']."";
	$xoopsDB->query($query) or $eh->show("0013");
        OpenTable();
	echo _MD_MODREQDELETED;
        CloseTable();
}

function modDownloadS() {
	global $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
	$cid = $HTTP_POST_VARS["cid"];
	if (($HTTP_POST_VARS["url"]) || ($HTTP_POST_VARS["url"]!="")) {
                $url = $myts->makeTboxData4Save($HTTP_POST_VARS["url"]);
        }
        $logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
        $title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
        $homepage = $myts->makeTboxData4Save($HTTP_POST_VARS["homepage"]);
        $version = $myts->makeTboxData4Save($HTTP_POST_VARS["version"]);
        $size = $myts->makeTboxData4Save($HTTP_POST_VARS["size"]);
        $platform = $myts->makeTboxData4Save($HTTP_POST_VARS["platform"]);
        $description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
        $xoopsDB->query("UPDATE ".$xoopsDB->prefix("mydownloads_downloads")." SET cid=$cid, title='$title', url='$url', homepage='$homepage', version='$version', size=$size, platform='$platform', logourl='$logourl', status=2, date=".time()." WHERE lid=".$HTTP_POST_VARS['lid']."")  or $eh->show("0013");
        $xoopsDB->query("UPDATE ".$xoopsDB->prefix("mydownloads_text")." SET description='$description' WHERE lid=".$HTTP_POST_VARS['lid']."")  or $eh->show("0013");
        OpenTable();
        echo _MD_DBUPDATED;
        CloseTable();
}
function delDownload() {
        global $xoopsDB, $HTTP_GET_VARS, $eh;
        $query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=".$HTTP_GET_VARS['lid']."";
        $xoopsDB->query($query) or $eh->show("0013");
        $query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=".$HTTP_GET_VARS['lid']."";
        $xoopsDB->query($query) or $eh->show("0013");
        $query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE lid=".$HTTP_GET_VARS['lid']."";
        $xoopsDB->query($query) or $eh->show("0013");
        OpenTable();
        echo _MD_FILEDELETED;
        CloseTable();
}
function modCat() {
        global $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
        $cid = $HTTP_POST_VARS["cid"];
        OpenTable();
        echo "<h4>"._MD_MODCAT."</h4><br>";
        $result=$xoopsDB->query("SELECT pid, title, imgurl FROM ".$xoopsDB->prefix("mydownloads_cat")." WHERE cid=$cid");
        list($pid,$title,$imgurl) = $xoopsDB->fetch_row($result);
        $title = $myts->makeTboxData4Edit($title);
        $imgurl = $myts->makeTboxData4Edit($imgurl);
        echo "<form action=index.php method=post>"._MD_TITLEC."<input type=text name=title value=\"$title\" size=51 maxlength=50><br><br>"._MD_IMGURLMAIN."<br><input type=text name=imgurl value=\"$imgurl\" size=100 maxlength=150><br>
        <input type=hidden name=pid value=\"$pid\">
        <input type=hidden name=cid value=$cid>
        <input type=hidden name=op value=modCatS><br>
        <input type=submit value=\""._MD_SAVE."\">
        <input type=button value="._MD_DELETE." onClick=\"location='index.php?pid=$pid&cid=$cid&op=delCat'\">";
        echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\">";
        echo "</form>";
        echo "</td></tr></table></td></tr></table><br>";
}
function modCatS() {
        global $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
        $cid =  $HTTP_POST_VARS['cid'];
        $sid =  $HTTP_POST_VARS['pid'];
        $title =  $myts->makeTboxData4Save($HTTP_POST_VARS['title']);
        if (($HTTP_POST_VARS["imgurl"]) || ($HTTP_POST_VARS["imgurl"]!="")) {
                $imgurl = $myts->makeTboxData4Save($HTTP_POST_VARS["imgurl"]);
        }
        $xoopsDB->query("UPDATE ".$xoopsDB->prefix("mydownloads_cat")." SET title='$title', imgurl='$imgurl' where cid=$cid") or $eh->show("0013");
        OpenTable();
        echo _MD_DBUPDATED;
        CloseTable();;
}
function delCat() {
        global $xoopsDB, $HTTP_GET_VARS, $eh, $mytree;
        $cid =  $HTTP_GET_VARS['cid'];
        if($HTTP_GET_VARS['ok']){
        	$ok =  $HTTP_GET_VARS['ok'];
        }
        if($ok==1) {
                //get all subcategories under the specified category
                $arr=$mytree->getAllChildId($cid);
                for($i=0;$i<sizeof($arr);$i++){
                        //get all downloads in each subcategory
                        $result=$xoopsDB->query("SELECT lid FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE cid=".$arr[$i]."") or $eh->show("0013");
                        //now for each download, delete the text data and vote ata associated with the download
                        while(list($lid)=$xoopsDB->fetch_row($result)){
                                $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=".$lid."") or $eh->show("0013");
                                $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE lid=".$lid."") or $eh->show("0013");
                                $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=".$lid."") or $eh->show("0013");
                        }

                        //all downloads for each subcategory is deleted, now delete the subcategory data
			$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_cat")." WHERE cid=".$arr[$i]."") or $eh->show("0013");
                }
                //all subcategory and associated data are deleted, now delete category data and its associated data
                $result=$xoopsDB->query("SELECT lid FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE cid=".$cid."") or $eh->show("0013");
                while(list($lid)=$xoopsDB->fetch_row($result)){
                        $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=$lid") or $eh->show("0013");
                        $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=$lid") or $eh->show("0013");
                        $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE lid=".$lid."") or $eh->show("0013");
                }
                $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("mydownloads_cat")." WHERE cid=$cid") or $eh->show("0013");
                OpenTable();
                echo _MD_CATDELETED;
                CloseTable();
	} else {
                OpenTable();
                echo "<center>";
                echo "<h4><font color=\"#ff0000\">";
                echo _MD_WARNING."</font></h4><br>";
            	echo "[ <a href=index.php?op=delCat&cid=$cid&ok=1>"._MD_YES."</a> | <a href=index.php>"._MD_NO."</a> ]<br><br>";
            	echo "</TD></TR></TABLE></TD></TR></TABLE>";
	}
}
function delNewDownload() {
	global $xoopsDB, $HTTP_GET_VARS, $eh;
	$query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=".$HTTP_GET_VARS['lid']."";
	$xoopsDB->query($query) or $eh->show("0013");
	$query = "DELETE FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=".$HTTP_GET_VARS['lid']."";
        $xoopsDB->query($query) or $eh->show("0013");
        OpenTable();
        echo _MD_FILEDELETED;
        CloseTable();
}
function addCat() {
        global $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
        $pid = $HTTP_POST_VARS["cid"];
        $title = $HTTP_POST_VARS["title"];
        if (($HTTP_POST_VARS["imgurl"]) || ($HTTP_POST_VARS["imgurl"]!="")) {
                $imgurl = $myts->makeTboxData4Save($HTTP_POST_VARS["imgurl"]);
        }
        $title = $myts->makeTboxData4Save($title);
	$newid = $xoopsDB->GenID("mydownloads_cat_cid_seq");
        $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mydownloads_cat")." (cid, pid, title, imgurl) VALUES ($newid, $pid, '$title', '$imgurl')") or $eh->show("0013");
        OpenTable();
        echo _MD_NEWCATADDED;
        CloseTable();

}

function addDownload() {
        global $xoopsConfig, $xoopsDB, $xoopsUser, $HTTP_POST_VARS, $myts, $eh;
        if (($HTTP_POST_VARS["url"]) || ($HTTP_POST_VARS["url"]!="")) {
                $url = $myts->makeTboxData4Save($HTTP_POST_VARS["url"]);
        }
        $logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
        $title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
        $homepage = $myts->makeTboxData4Save($HTTP_POST_VARS["homepage"]);
        $version = $myts->makeTboxData4Save($HTTP_POST_VARS["version"]);
        $size = $myts->makeTboxData4Save($HTTP_POST_VARS["size"]);
        $platform = $myts->makeTboxData4Save($HTTP_POST_VARS["platform"]);
        $description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
        $submitter = $xoopsUser->uid();
        $result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE url='$url'");
        list($numrows) = $xoopsDB->fetch_row($result);
        if ($numrows>0) {
                echo "<h4><font color=\"#ff0000\">";
                echo _MD_ERROREXIST."</font></h4><br>";
                $error = 1;
        }
// Check if Title exist
        if ($title=="") {
                echo "<h4><font color=\"#ff0000\">";
                echo _MD_ERRORTITLE."</font></h4><br>";
                    $error =1;
        }
	if(!$size){
		$size = 0;
	}
// Check if Description exist
        if ($description=="") {
                echo "<h4><font color=\"#ff0000\">";
                echo _MD_ERRORDESC."</font></h4><br>";
                $error =1;
        }
        if($error == 1) {
                include("footer.php");
                exit();
        }
        $cid = $HTTP_POST_VARS['cid'];
	$newid = $xoopsDB->GenID("mydownloads_downloads_lid_seq");
        $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mydownloads_downloads")." (lid, cid, title, url, homepage, version, size, platform, logourl, submitter, status, date, hits, rating, votes, comments) VALUES ($newid, $cid, '$title', '$url', '$homepage', '$version', $size, '$platform', '$logourl', $submitter, 1, ".time().", 0, 0, 0, 0)") or $eh->show("0013");
        if($newid == 0){
		$newid = $xoopsDB->Insert_ID();
	}
        $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mydownloads_text")." (lid, description) VALUES ($newid, '$description')") or $eh->show("0013");
        OpenTable();
        echo _MD_NEWDLADDED."<br>";
        CloseTable();
}
function approve(){
        global $xoopsConfig, $xoopsDB, $HTTP_POST_VARS, $myts, $eh;
        $lid = $HTTP_POST_VARS['lid'];
        $title = $HTTP_POST_VARS['title'];
        $cid = $HTTP_POST_VARS['cid'];
        $homepage = $HTTP_POST_VARS['homepage'];
        $version = $HTTP_POST_VARS['version'];
        $size = $HTTP_POST_VARS['size'];
        $platform = $HTTP_POST_VARS['platform'];
        $description = $HTTP_POST_VARS['description'];
        if (($HTTP_POST_VARS["url"]) || ($HTTP_POST_VARS["url"]!="")) {
                $url = $myts->makeTboxData4Save($HTTP_POST_VARS["url"]);
        }
        $logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
        $title = $myts->makeTboxData4Save($title);
        $homepage = $myts->makeTboxData4Save($homepage);
        $version = $myts->makeTboxData4Save($HTTP_POST_VARS["version"]);
        $size = $myts->makeTboxData4Save($HTTP_POST_VARS["size"]);
        $platform = $myts->makeTboxData4Save($HTTP_POST_VARS["platform"]);
        $description = $myts->makeTareaData4Save($description);
        $query = "UPDATE ".$xoopsDB->prefix("mydownloads_downloads")." SET cid=$cid, title='$title', url='$url', homepage='$homepage', version='$version', size=$size, platform='$platform', logourl='$logourl', status=1, date=".time()." where lid=".$lid."";
        $xoopsDB->query($query) or $eh->show("0013");
        $query = "UPDATE ".$xoopsDB->prefix("mydownloads_text")." SET description='$description' where lid=".$lid."";
        $xoopsDB->query($query) or $eh->show("0013");
        $result = $xoopsDB->query("SELECT submitter, email FROM ".$xoopsDB->prefix("mydownloads_downloads")." where lid=$lid");
        list($submitter, $email)=$xoopsDB->fetch_row($result);
	$submitter = XoopsUser::get_uname_from_id($submitter);
        if (get_magic_quotes_runtime()) {
                $submitter = stripslashes($submitter);
        }
        $subject = sprintf(_MD_YOURFILEAT,$xoopsConfig['sitename']);
        $message = sprintf(_MD_HELLO,$submitter);
	$message .= "\n\n"._MD_WEAPPROVED."\n\n";
	$siteurl = $xoopsConfig['xoops_url']."/modules/mydownloads/";
	$message .= sprintf(_MD_VISITAT,$siteurl);
	$message .= "\n\n"._MD_THANKSSUBMIT."\n\n".$xoopsConfig['sitename']."\n".$xoopsConfig['xoops_url']."\n".$xoopsConfig['adminmail']."";
        $from = "$xoopsConfig[adminmail]";
      	mail($email, $subject, $message, "From: \"".$xoopsConfig['adminmail']."\" <$from>\nX-Mailer: PHP/" . phpversion());
        OpenTable();
        echo _MD_NEWDLADDED."<br><br>";
        CloseTable();
}


function mydownloadsConfigAdmin() {
        global $mydownloads_perpage, $mydownloads_popular, $mydownloads_newdownloads, $mydownloads_top, $mydownloads_sresults, $mydownloads_anonadddownloadlock, $mydownloads_useshots, $mydownloads_allow_bbcode, $mydownloads_allow_smilies, $mydownloads_shotwidth;

        OpenTable();

        echo "<h4>" . _MD_GENERALSET . "</h4><br>";
        echo "<form action=\"index.php\" method=\"post\">";
    	echo "
    	<table width=100% border=0><tr><td nowrap>
    	"._MD_DLSPERPAGE."</td><td width=100%>
        <select name=xmydownloads_perpage>
        <option value=$mydownloads_perpage selected>$mydownloads_perpage</option>
        <option value=10>10</option>
        <option value=15>15</option>
        <option value=20>20</option>
        <option value=25>25</option>
        <option value=30>30</option>
        <option value=50>50</option>
    	</select>
    	</td></tr><tr><td nowrap>
    	"._MD_HITSPOP."</td><td>
        <select name=xmydownloads_popular>
        <option value=$mydownloads_popular selected>$mydownloads_popular</option>
        <option value=10>10</option>
        <option value=20>20</option>
        <option value=50>50</option>
        <option value=100>100</option>
        <option value=500>500</option>
        <option value=1000>1000</option>
    	</select>
    	</td></tr><tr><td nowrap>
    	"._MD_DLSNEW."</td><td>
        <select name=xmydownloads_newdownloads>
        <option value=$mydownloads_newdownloads selected>$mydownloads_newdownloads</option>
        <option value=10>10</option>
        <option value=15>15</option>
        <option value=20>20</option>
        <option value=25>25</option>
        <option value=30>30</option>
        <option value=50>50</option>
   	</select>";
    	echo "</td></tr><tr><td nowrap>
    	"._MD_DLSSEARCH."</td><td>
        <select name=xmydownloads_sresults>
        <option value=$mydownloads_sresults selected>$mydownloads_sresults</option>
        <option value=10>10</option>
        <option value=15>15</option>
        <option value=20>20</option>
        <option value=25>25</option>
        <option value=30>30</option>
        <option value=50>50</option>
    	</select>";
        echo "</td></tr>";
        echo "<tr><td nowrap>" . _MD_USESHOTS . " </td><td>";
        if ($mydownloads_useshots==1) {
                echo "<INPUT TYPE=\"RADIO\" NAME=\"xmydownloads_useshots\" VALUE=\"1\" CHECKED>&nbsp;" ._MD_YES."&nbsp;</INPUT>";
                echo "<INPUT TYPE=\"RADIO\" NAME=\"xmydownloads_useshots\" VALUE=\"0\" >&nbsp;" ._MD_NO."&nbsp;</INPUT>";
        } else {
                echo "<INPUT TYPE=\"RADIO\" NAME=\"xmydownloads_useshots\" VALUE=\"1\">&nbsp;" ._MD_YES."&nbsp;</INPUT>";
                echo "<INPUT TYPE=\"RADIO\" NAME=\"xmydownloads_useshots\" VALUE=\"0\" CHECKED>&nbsp;" ._MD_NO."&nbsp;</INPUT>";
        }

        echo "</td></tr>";
        echo "<tr><td nowrap>" . _MD_IMGWIDTH . " </td><td>";
        if($mydownloads_shotwidth!=""){
                echo "<INPUT TYPE=\"text\" size=\"10\" NAME=\"xmydownloads_shotwidth\" VALUE=\"$mydownloads_shotwidth\"></INPUT>";
        }else{
                echo "<INPUT TYPE=\"text\" size=\"10\" NAME=\"xmydownloads_shotwidth\" VALUE=\"140\"></INPUT>";
        }
        echo "</td></tr>";

        echo "<tr><td>&nbsp;</td></tr>";
        echo "</table>";
        echo "<input type=\"hidden\" name=\"op\" value=\"mydownloadsConfigChange\">";
        echo "<input type=\"submit\" value=\""._MD_SAVE."\">";
        echo "&nbsp;<input type=\"button\" value=\""._MD_CANCEL."\" onclick=\"javascript:history.go(-1)\">";
        echo "</form>";
        echo "</td></tr></table></td></tr></table>";

}

function mydownloadsConfigChange() {
        global $xoopsConfig, $HTTP_POST_VARS;

        $xmydownloads_popular = $HTTP_POST_VARS['xmydownloads_popular'];
        $xmydownloads_newdownloads = $HTTP_POST_VARS['xmydownloads_newdownloads'];
        $xmydownloads_sresults = $HTTP_POST_VARS['xmydownloads_sresults'];
        $xmydownloads_perpage = $HTTP_POST_VARS['xmydownloads_perpage'];
        $xmydownloads_useshots = $HTTP_POST_VARS['xmydownloads_useshots'];
        $xmydownloads_shotwidth = $HTTP_POST_VARS['xmydownloads_shotwidth'];
        $filename = "../config.php";
        $file = fopen($filename, "w");
        $content = "";
        $content .= "<?PHP\n";
        $content .= "\n";
        $content .= "###############################################################################\n";
        $content .= "# my Downloads v1.0                                                                #\n";
        $content .= "#                                                                              #\n";
        $content .= "# \$mydownloads_popular:        The number of hits required for a download to be a popular site. Default = 20      #\n";
        $content .= "# \$mydownloads_newdownloads:        The number of downloads that appear on the front page as latest listings. Default = 10  #\n";
        $content .= "# \$mydownloads_sresults:        The number of downloads that appear in one page when performing search  Default = 10  #\n";
        $content .= "# \$mydownloads_perpage:            The number of downloads that appear for each page. Default = 10 #\n";
        $content .= "# \$mydownloads_useshots:            Use screenshots? Default = 1 (Yes) #\n";
        $content .= "# \$mydownloads_shotwidth:            Screenshot Image Width (Default = 140) #\n";
        $content .= "###############################################################################\n";
        $content .= "\n";
        $content .= "\$mydownloads_popular = $xmydownloads_popular;\n";
        $content .= "\$mydownloads_newdownloads = $xmydownloads_newdownloads;\n";
        $content .= "\$mydownloads_sresults = $xmydownloads_sresults;\n";
        $content .= "\$mydownloads_perpage = $xmydownloads_perpage;\n";
        $content .= "\$mydownloads_useshots = $xmydownloads_useshots;\n";
        $content .= "\$mydownloads_shotwidth = $xmydownloads_shotwidth;\n";
        $content .= "\n";
        $content .= "?>\n";

        fwrite($file, $content);
        fclose($file);

        redirect_header("index.php",1,_MD_CONFUPDATED);
}


switch ($op) {
		default:
			mydownloads();
			break;
		case "delNewDownload":
			delNewDownload();
			break;
		case "approve":
			approve();
			break;
		case "addCat":
			addCat();
			break;
		case "addSubCat":
			addSubCat();
			break;
		case "addDownload":
			addDownload();
			break;
		case "listBrokenDownloads":
			listBrokenDownloads();
			break;
		case "delBrokenDownloads":
			delBrokenDownloads();
			break;
		case "ignoreBrokenDownloads":
			ignoreBrokenDownloads();
			break;
		case "listModReq":
			listModReq();
			break;
		case "changeModReq":
			changeModReq();
			break;
	        case "ignoreModReq":
			ignoreModReq();
			break;
		case "delCat":
			delCat();
			break;
		case "modCat":
			modCat();
			break;
		case "modCatS":
			modCatS();
			break;
		case "modDownload":
			modDownload();
			break;
		case "modDownloadS":
			modDownloadS();
			break;
		case "delDownload":
			delDownload();
			break;
		case "delVote":
			delVote();
			break;
		case "delComment":
			delComment($bid, $rid);
			break;
		case "mydownloadsConfigAdmin":
			mydownloadsConfigAdmin();
			break;
		case "mydownloadsConfigChange":
			mydownloadsConfigChange();
			break;
		case "downloadsConfigMenu":
			downloadsConfigMenu();
			break;
		case "listNewDownloads":
			listNewDownloads();
			break;
}
include("admin_footer.php");
?>