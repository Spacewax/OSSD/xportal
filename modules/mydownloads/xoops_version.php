<?php
$modversion['name'] = _MI_MYDOWNLOADS_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_MYDOWNLOADS_DESC;
$modversion['credits'] = "Modified by wanderer<br>( http://www.mpn-tw.com/ ) <br>Based on MyLinks by Kazumi Ono<br>( http://www.mywebaddons.com/ )<br>The MPN SE Project";
$modversion['help'] = "mydownloads.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "images/mydl_slogo.gif";
$modversion['dirname'] = "mydownloads";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Blocks
$modversion['blocks'][1]['file'] = "mydownloads_top.php";
$modversion['blocks'][1]['name'] = _MI_MYDOWNLOADS_BNAME1;
$modversion['blocks'][1]['description'] = "Shows recently added donwload files";
$modversion['blocks'][1]['show_func'] = "b_mydownloads_top_show";
$modversion['blocks'][1]['edit_func'] = "b_mydownloads_top_edit";
$modversion['blocks'][1]['options'] = "date|10";
$modversion['blocks'][2]['file'] = "mydownloads_top.php";
$modversion['blocks'][2]['name'] = _MI_MYDOWNLOADS_BNAME2;
$modversion['blocks'][2]['description'] = "Shows most downloaded files";
$modversion['blocks'][2]['show_func'] = "b_mydownloads_top_show";
$modversion['blocks'][2]['edit_func'] = "b_mydownloads_top_edit";
$modversion['blocks'][2]['options'] = "hits|10";

// Menu
$modversion['hasMain'] = 1;
$modversion['sub'][1]['name'] = _MI_MYDOWNLOADS_SMNAME1;
$modversion['sub'][1]['url'] = "submit.php";
$modversion['sub'][2]['name'] = _MI_MYDOWNLOADS_SMNAME2;
$modversion['sub'][2]['url'] = "topten.php?hit=1";
$modversion['sub'][3]['name'] = _MI_MYDOWNLOADS_SMNAME3;
$modversion['sub'][3]['url'] = "topten.php?rate=1";

// Search
$modversion['hasSearch'] = 1;
$modversion['search']['file'] = "include/search.inc.php";
$modversion['search']['func'] = "mydownloads_search";
?>