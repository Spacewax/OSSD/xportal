<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

$myts = new MyTextSanitizer; // MyTextSanitizer object
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"),"cid","pid");

include($xoopsConfig['root_path']."header.php");
//generates top 10 charts by rating and hits for each main category
OpenTable();
mainheader();
if($rate){
	$sort = "Rating";
	$sortDB = "rating";
}else{
	$sort = "Hits";
	$sortDB = "hits";
}
$arr=array();
$result=$xoopsDB->query("SELECT cid, title FROM ".$xoopsDB->prefix("mydownloads_cat")." WHERE pid=0");
while(list($cid,$ctitle)=$xoopsDB->fetch_row($result)){
	$boxtitle = "<big>";
	$boxtitle .= sprintf(_MD_TOP10,$ctitle);
	$boxtitle .= " (".$sort.")</big>";
	$thing = "<table width=\"100%\" border=\"0\"><tr><td width=\"7%\" bgcolor=\"#DFDFDF\"><b>"._MD_RANK."</b></td><td width=\"28%\" bgcolor=\"#DFDFDF\"><b>"._MD_TITLE."</b></td><td width=\"40%\" bgcolor=\"#DFDFDF\"><b>"._MD_CATEGORY."</b></td><td width=\"8%\" bgcolor=\"#DFDFDF\" align=\"center\"><b>"._MD_HITS."</b></td><td width=\"9%\" bgcolor=\"#DFDFDF\" align=\"center\"><b>"._MD_RATING."</b></td><td width=\"8%\" bgcolor=\"#DFDFDF\" align=\"right\"><b>"._MD_VOTE."</b></td></tr>";
	$query = "SELECT lid, cid, title, hits, rating, votes FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE status>0 AND (cid=$cid";
	// get all child cat ids for a given cat id
	$arr=$mytree->getAllChildId($cid);
	$size = sizeof($arr);
	for($i=0;$i<$size;$i++){
		$query .= " OR cid=".$arr[$i]."";
	}
	$query .= ") ORDER BY ".$sortDB." DESC"; 
	$result2 = $xoopsDB->query($query,0,10,0);
	$rank = 1;
	while(list($did,$dcid,$dtitle,$hits,$rating,$votes)=$xoopsDB->fetch_row($result2)){
		$rating = number_format($rating, 2);
		if($hit){
			$hits = "<font color=\"#ff0000\">$hits</font>";
		} elseif($rate) {
			$rating = "<font color=\"#ff0000\">$rating</font>";
		}else{
		}
		$catpath = $mytree->getPathFromId($dcid, "title");
		$catpath= substr($catpath, 1);
		$catpath = str_replace("/"," <span style='font-weight:bold;color:#ff0000'>&raquo;&raquo;</span> ",$catpath);
		$thing .= "<tr><td>$rank</td>";
		$thing .= "<td><a href=\"singlefile.php?lid=$did\">$dtitle</a></td>";
		$thing .= "<td>$catpath</td>";
		$thing .= "<td align=\"center\">$hits</td>";
		$thing .= "<td align=\"center\">$rating</td><td align=\"right\">$votes</td></tr>";
		$rank++;
	}
	$thing .= "</table>";
	OpenTable();
	echo $boxtitle."<br />".$thing."";
	CloseTable();
	themecenterposts();
	echo "<br>";
}
CloseTable();

include("footer.php");

?>