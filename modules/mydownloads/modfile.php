<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
$myts = new MyTextSanitizer; // MyTextSanitizer object
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"),"cid","pid");
if($HTTP_POST_VARS['submit']) {
	$eh = new ErrorHandler; //ErrorHandler object
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	} else {
		$ratinguser = $xoopsUser->uid();
	}
    	$lid = $HTTP_POST_VARS["lid"];
	

// Check if Title exist
    	if ($HTTP_POST_VARS["title"]=="") {
        	$eh->show("1001");
    	}
// Check if URL exist
    	if ($HTTP_POST_VARS["url"]=="") {
         	$eh->show("1016");
    	}
// Check if HOMEPAGE exist
            if ($HTTP_POST_VARS["homepage"]=="") {
                 $eh->show("1016");
            }
// Check if Description exist
    	if ($HTTP_POST_VARS['description']=="") {
        	$eh->show("1008");
    	}
        $url = $myts->makeTboxData4Save($url);
        $logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
        $cid = $HTTP_POST_VARS["cid"];
        $title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
        $homepage = $myts->makeTboxData4Save($HTTP_POST_VARS["homepage"]);
        $version = $myts->makeTboxData4Save($version);
        $size = $myts->makeTboxData4Save($size);
        $platform = $myts->makeTboxData4Save($platform);
        $description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
	$newid = $xoopsDB->GenID("mydownloads_mod_requestid_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mydownloads_mod")." (requestid, lid, cid, title, url, homepage, version, size, platform, logourl, description, modifysubmitter) VALUES ($newid, $lid, $cid, '$title', '$url', '$homepage', '$version', $size, '$platform', '$logourl', '$description', $ratinguser)") or $eh->show("0013");
    	redirect_header("index.php",2,_MD_THANKSFORINFO);
	exit();
    	

} else {
    	$lid = $HTTP_GET_VARS['lid'];
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	}
	include($xoopsConfig['root_path']."header.php");
    	OpenTable();
	mainheader();
	echo "<table width=\"80%\" align=\"center\">";
    	$result = $xoopsDB->query("SELECT cid, title, url, homepage, version, size, platform, logourl FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE lid=".$lid." AND status>0");
        echo _MD_REQUESTMOD."<br><br>";
	list($cid, $title, $url, $homepage, $version, $size, $platform, $logourl) = $xoopsDB->fetch_row($result);
        $title = $myts->makeTboxData4Edit($title);
        $url = $myts->makeTboxData4Edit($url);
        $homepage = $myts->makeTboxData4Edit($homepage);
        $version = $myts->makeTboxData4Edit($version);
        $size = $myts->makeTboxData4Edit($size);
        $platform = $myts->makeTboxData4Edit($platform);
        $logourl = $myts->makeTboxData4Edit($logourl);
#       $logourl = urldecode($logourl);
        $result2 = $xoopsDB->query("SELECT description FROM ".$xoopsDB->prefix("mydownloads_text")." WHERE lid=$lid");
        list($description)=$xoopsDB->fetch_row($result2);
        $description = $myts->makeTareaData4Edit($description);
        echo "<form action=\"modfile.php\" method=\"post\">";
        echo "<table width=\"80%\"><tr><td align=\"right\">";
        echo "<b>"._MD_FILEID."</b></td><td>";
        echo $lid;
        echo "</td></tr><tr><td align=\"right\"><b>"._MD_FILETITLE."</b></td><td>";
        echo "<input type=\"text\" name=\"title\" size=\"50\" maxlength=\"100\" value=\"$title\">";
        echo "</td></tr><tr><td align=\"right\"><b>"._MD_DLURL."</b></td><td>";
        echo "<input type=\"text\" name=\"url\" size=\"50\" maxlength=\"250\" value=\"".$url."\">";
        echo "</td></tr>";
        echo "<tr><td align=\"right\"><b>"._MD_CATEGORYC."</b></td><td>";
        $mytree->makeMySelBox("title","title",$cid);
        echo "</td></tr><tr><td></td><td></td></tr>\n";
        echo "<tr><td align=\"right\"><b>"._MD_HOMEPAGEC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"homepage\" size=\"50\" maxlength=\"100\" value=\"".$homepage."\"></td></tr>\n";
        echo "<tr><td align=\"right\"><b>"._MD_VERSIONC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"version\" size=\"10\" maxlength=\"10\" value=\"".$version."\"></td></tr>\n";
        echo "<tr><td align=\"right\"><b>"._MD_FILESIZEC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"size\" size=\"10\" maxlength=\"8\" value=\"$size\">"._MD_BYTES."</td></tr>\n";
        echo "<tr><td align=\"right\"><b>"._MD_PLATFORMC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"platform\" size=\"45\" maxlength=\"50\" value=\"$platform\"></td></tr>\n";
#        echo "<tr><td align=\"right\">logo</td><td>\n";
#        echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\" value=\"$logourl\"></td></tr>\n";
        echo "<tr><td align=\"right\" valign=\"top\"><b>"._MD_DESCRIPTIONC."</b></td><td>\n";
        echo "<textarea name=\"description\" cols=\"60\" rows=\"5\">".$description."</textarea>\n";
        echo "</td></tr>\n";
        echo "<tr><td colspan=\"2\" align=\"center\"><br>\n";
        echo "<input type=\"hidden\" name=\"logourl\" value=\"$logourl\"></input>";
        echo "<input type=\"hidden\" name=\"lid\" value=\"$lid\"></input>";
        echo "</input><input name=\"submit\" type=\"submit\" value=\""._MD_SUBMIT."\"></input></form>";
        echo "</td></tr></table>";

        CloseTable();
}
include("footer.php");
?>