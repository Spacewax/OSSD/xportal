<?php
// Module Info

// The name of this module
define("_MI_MYDOWNLOADS_NAME","Downloads");

// A brief description of this module
define("_MI_MYDOWNLOADS_DESC","Creates a downloads section where users can download/submit/rate various files.");

// Names of blocks for this module (Not all module has blocks)
define("_MI_MYDOWNLOADS_BNAME1","Recent Downloads");
define("_MI_MYDOWNLOADS_BNAME2","Top Downloads");

// Sub menu titles
define("_MI_MYDOWNLOADS_SMNAME1","Submit");
define("_MI_MYDOWNLOADS_SMNAME2","Popular");
define("_MI_MYDOWNLOADS_SMNAME3","Top Rated");
?>