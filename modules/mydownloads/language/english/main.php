<?php

//%%%%%%		Module Name 'MyDownloads'		%%%%%

define("_MD_THANKSFORINFO","Thanks for the information. We'll look into your request shortly.");
define("_MD_BACKTOTOP","Back to Downloads Top");
define("_MD_THANKSFORHELP","Thank you for helping to maintain this directory's integrity.");
define("_MD_FORSECURITY","For security reasons your user name and IP address will also be temporarily recorded.");

define("_MD_SEARCHFOR","Search for");
define("_MD_MATCH","Match");
define("_MD_ALL","ALL");
define("_MD_ANY","ANY");
define("_MD_NAME","Name");
define("_MD_DESCRIPTION","Description");
define("_MD_SEARCH","Search");

define("_MD_MAIN","Main");
define("_MD_SUBMITFILE","Submit File");
define("_MD_POPULAR","Popular");
define("_MD_TOPRATED","Top Rated");

define("_MD_NEWTHISWEEK","New this week");
define("_MD_UPTHISWEEK","Updated this week");

define("_MD_POPULARITYLTOM","Popularity (Least to Most Hits)");
define("_MD_POPULARITYMTOL","Popularity (Most to Least Hits)");
define("_MD_TITLEATOZ","Title (A to Z)");
define("_MD_TITLEZTOA","Title (Z to A)");
define("_MD_DATEOLD","Date (Old Files Listed First)");
define("_MD_DATENEW","Date (New Files Listed First)");
define("_MD_RATINGLTOH","Rating (Lowest Score to Highest Score)");
define("_MD_RATINGHTOL","Rating (Highest Score to Lowest Score)");

define("_MD_NOSHOTS","No Screenshots Available");
define("_MD_EDITTHISDL","Edit This Download");

define("_MD_DESCRIPTIONC","Description: ");
define("_MD_EMAILC","Email: ");
define("_MD_CATEGORYC","Category: ");
define("_MD_LASTUPDATEC","Last Update: ");
define("_MD_DLNOW","Download Now!");
define("_MD_VERSION","Version");
define("_MD_SUBMITDATE","Submitted Date");
define("_MD_DLTIMES","Downloaded %s times");
define("_MD_FILESIZE","File Size");
define("_MD_SUPPORTEDPLAT","Supported Platforms");
define("_MD_HOMEPAGE","Home Page");
define("_MD_HITSC","Hits: ");
define("_MD_RATINGC","Rating: ");
define("_MD_ONEVOTE","1 vote");
define("_MD_NUMVOTES","%s votes");
define("_MD_ONEPOST","1 post");
define("_MD_NUMPOSTS","%s votes");
define("_MD_COMMENTSC","Comments: ");
define("_MD_RATETHISFILE","Rate this File");
define("_MD_MODIFY","Modify");
define("_MD_REPORTBROKEN","Report Broken File");
define("_MD_TELLAFRIEND","Tell a Friend");
define("_MD_VSCOMMENTS","View/Send Comments");
define("_MD_EDIT","Edit");

define("_MD_THEREARE","There are %s files in our database");
define("_MD_LATESTLIST","Latest Listings");

define("_MD_REQUESTMOD","Request Download Modification");
define("_MD_FILEID","File ID: ");
define("_MD_FILETITLE","Download Title: ");
define("_MD_DLURL","Download URL: ");
define("_MD_HOMEPAGEC","Home Page: ");
define("_MD_VERSIONC","Version: ");
define("_MD_FILESIZEC","File Size: ");
define("_MD_NUMBYTES","%s bytes");
define("_MD_PLATFORMC","Platform: ");
define("_MD_CONTACTEMAIL","Contact Email: ");
define("_MD_SHOTIMAGE","Screenshot Img: ");
define("_MD_SENDREQUEST","Send Request");


define("_MD_VOTEAPPRE","Your vote is appreciated.");
define("_MD_THANKYOU","Thank you for taking the time to vote here at %s"); // %s is your site name
define("_MD_VOTEFROMYOU","Input from users such as yourself will help other visitors better decide which file to download.");
define("_MD_VOTEONCE","Please do not vote for the same resource more than once.");
define("_MD_RATINGSCALE","The scale is 1 - 10, with 1 being poor and 10 being excellent.");
define("_MD_BEOBJECTIVE","Please be objective, if everyone receives a 1 or a 10, the ratings aren't very useful.");
define("_MD_DONOTVOTE","Do not vote for your own resource.");
define("_MD_RATEIT","Rate It!");

define("_MD_INTFILEAT","Interesting Download File at %s"); // %s is your site name
define("_MD_INTFILEFOUND","Here is an interesting download file I have found at %s"); // %s is your site name

define("_MD_RECEIVED","We received your download information. Thanks!");
define("_MD_WHENAPPROVED","You'll receive an E-mail when it's approved.");
define("_MD_SUBMITONCE","Submit your file/script only once.");
define("_MD_ALLPENDING","All file/script information are posted pending verification.");
define("_MD_DONTABUSE","Username and IP are recorded, so please don't abuse the system.");
define("_MD_TAKEDAYS","It may take several days for your file/script to be added to our database.");

define("_MD_RANK","Rank");
define("_MD_CATEGORY","Category");
define("_MD_HITS","Hits");
define("_MD_RATING","Rating");
define("_MD_VOTE","Vote");

define("_MD_SEARCHRESULT4","Search results for <b>%s</b>:");
define("_MD_MATCHESFOUND","%s matche(s) found.");
define("_MD_SORTBY","Sort by:");
define("_MD_TITLE","Title");
define("_MD_DATE","Date");
define("_MD_POPULARITY","Popularity");
define("_MD_CURSORTBY","Files currently sorted by: %s");
define("_MD_FOUNDIN","Found in:");
define("_MD_PREVIOUS","Previous");
define("_MD_NEXT","Next");
define("_MD_NOMATCH","No matches found to your query");

define("_MD_TOP10","%s Top 10"); // %s is a downloads category name
define("_MD_CATEGORIES","Categories");

define("_MD_SUBMIT","Submit");
define("_MD_CANCEL","Cancel");

define("_MD_BYTES","Bytes");
define("_MD_ALREADYREPORTED","You have already submitted a broken report for this resource.");
define("_MD_MUSTREGFIRST","Sorry, you don't have the permission to perform this action.<br>Please register or login first!");
define("_MD_NORATING","No rating selected.");
define("_MD_CANTVOTEOWN","You cannot vote on the resource you submitted.<br>All votes are logged and reviewed.");

//%%%%%%	Module Name 'MyDownloads' (Admin)	  %%%%%

define("_MD_DLCONF","Downloads Configuration");
define("_MD_GENERALSET","My Downloads General Settings");
define("_MD_ADDMODDELETE","Add, Modify, and Delete Categories/Files");
define("_MD_DLSWAITING","Downloads Waiting for Validation");
define("_MD_BROKENREPORTS","Broken File Reports");
define("_MD_MODREQUESTS","Download Info Modification Requests");
define("_MD_SUBMITTER","Submitter: ");
define("_MD_DOWNLOAD","Download");
define("_MD_APPROVE","Approve");
define("_MD_DELETE","Delete");
define("_MD_NOSUBMITTED","No New Submitted Downloads.");
define("_MD_ADDMAIN","Add a MAIN Category");
define("_MD_TITLEC","Title: ");
define("_MD_IMGURL","Image URL (OPTIONAL Image height will be resized to 50): ");
define("_MD_ADD","Add");
define("_MD_ADDSUB","Add a SUB-Category");
define("_MD_IN","in");
define("_MD_ADDNEWFILE","Add a New File");
define("_MD_MODCAT","Modify Category");
define("_MD_MODDL","Modify Download Info");
define("_MD_USER","User");
define("_MD_IP","IP Address");
define("_MD_USERAVG","User AVG Rating");
define("_MD_TOTALRATE","Total Ratings");
define("_MD_NOREGVOTES","No Registered User Votes");
define("_MD_NOUNREGVOTES","No Unregistered User Votes");
define("_MD_VOTEDELETED","Vote data deleted.");
define("_MD_NOBROKEN","No reported broken files.");
define("_MD_IGNOREDESC","Ignore (Ignores the report and only deletes the <b>broken file report</b>)");
define("_MD_DELETEDESC","Delete (Deletes <b>the reported download data</b> and <b>broken file reports</b> for the file.)");
define("_MD_REPORTER","Report Sender");
define("_MD_FILESUBMITTER","File Submitter");
define("_MD_IGNORE","Ignore");
define("_MD_FILEDELETED","File Deleted.");
define("_MD_BROKENDELETED","Broken file report deleted.");
define("_MD_USERMODREQ","User Download Info Modification Requests");
define("_MD_ORIGINAL","Original");
define("_MD_PROPOSED","Proposed");
define("_MD_OWNER","Owner: ");
define("_MD_NOMODREQ","No Download Modification Request.");
define("_MD_DBUPDATED","Database Updated Successfully!");
define("_MD_MODREQDELETED","Modification Request Deleted.");
define("_MD_IMGURLMAIN","Image URL (OPTIONAL and Only valid for main categories. Image height will be resized to 50): ");
define("_MD_PARENT","Parent Category:");
define("_MD_SAVE","Save Changes");
define("_MD_CATDELETED","Category Deleted.");
define("_MD_WARNING","WARNING: Are you sure you want to delete this Category and ALL its Files and Comments?");
define("_MD_YES","Yes");
define("_MD_NO","No");
define("_MD_NEWCATADDED","New Category Added Successfully!");
define("_MD_ERROREXIST","ERROR: The download info you provided is already in the database!");
define("_MD_ERRORTITLE","ERROR: You need to enter TITLE!");
define("_MD_ERRORDESC","ERROR: You need to enter DESCRIPTION!");
define("_MD_NEWDLADDED","New download added to the database.");
define("_MD_WEAPPROVED","We approved your download submission to our downloads section.");
define("_MD_THANKSSUBMIT","Thanks for your submission!");
define("_MD_DLSPERPAGE","Displayed Downloads per Page: ");
define("_MD_HITSPOP","Hits to be Popular: ");
define("_MD_DLSNEW","Number of Downloads as New on Top Page: ");
define("_MD_DLSSEARCH","Number of Downloads in Search Results: ");
define("_MD_USESHOTS","Use Screenshots: ");
define("_MD_IMGWIDTH","Screenshot Img Width: ");

define("_MD_MUSTBEVALID","Screenshot image must be a valid image file under %s directory (ex. shot.gif). Leave it blank if no image file.");

define("_MD_REGUSERVOTES","Registered User Votes (total votes: %s)");
define("_MD_ANONUSERVOTES","Anonymous User Votes (total votes: %s)");

define("_MD_YOURFILEAT","Your file submitted at %s"); // this is an approved mail subject. %s is your site name

define("_MD_VISITAT","Visit our downloads section at %s");

define("_MD_DLRATINGS","Download Rating (total votes: %s)");
define("_MD_CONFUPDATED","Configuration Updated Successfully!");
?>