<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");

$myts = new MyTextSanitizer; // MyTextSanitizer object
$eh = new ErrorHandler; //ErrorHandler object
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"),"cid","pid");

if($HTTP_POST_VARS['submit']){
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	} 
    	if(!isset($HTTP_POST_VARS['submitter'])) {
                $submitter = $xoopsUser->uid();
    	}else{
		$submitter = $HTTP_POST_VARS['submitter'];
	}
// Check if Title exist
    	if ($HTTP_POST_VARS["title"]=="") {
        	$eh->show("1001");
    	}
// Check if URL exist
	if (($HTTP_POST_VARS["url"]) || ($HTTP_POST_VARS["url"]!="")) {
		$url = $HTTP_POST_VARS["url"];
	}
    	if ($url=="") {
        	$eh->show("1016");
    	}
// Check if HomePage exist
        if ($HTTP_POST_VARS["homepage"]=="") {
                $eh->show("1001");
        }	
// Check if Description exist
    	if ($HTTP_POST_VARS['description']=="") {
        	$eh->show("1008");
    	}
		
    	if ( !empty($HTTP_POST_VARS['cid']) ) {
    		$cid = $HTTP_POST_VARS['cid'];
	} else {
		$cid = 0;
	}
        $url = $myts->makeTboxData4Save($url);
        $title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
        $homepage = $myts->makeTboxData4Save($HTTP_POST_VARS["homepage"]);
        $version = $myts->makeTboxData4Save($HTTP_POST_VARS["version"]);
        $size = $myts->makeTboxData4Save($HTTP_POST_VARS["size"]);
        $platform = $myts->makeTboxData4Save($HTTP_POST_VARS["platform"]);
        $description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
	if(!is_numeric($size)){
		$size = 0;
	}
	$date = time();
	$newid = $xoopsDB->GenID("mydownloads_downloads_lid_seq");
        $q = "INSERT INTO ".$xoopsDB->prefix("mydownloads_downloads")." (lid, cid, title, url, homepage, version, size, platform, logourl, submitter, status, date, hits, rating, votes, comments) VALUES ($newid, $cid, '$title', '$url', '$homepage', '$version', $size, '$platform', '', $submitter, 0, $date, 0, 0, 0, 0)";
    	$xoopsDB->query($q) or $eh->show("0013");
	if($newid == 0){
		$newid = $xoopsDB->Insert_ID();
	}
        $xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("mydownloads_text")." (lid, description) VALUES ($newid, '$description')") or $eh->show("0013");
	redirect_header("index.php",2,_MD_RECEIVED."<br>"._MD_WHENAPPROVED."");
	exit();
    	
}else{
	if(!$xoopsUser){
		redirect_header($xoopsConfig['xoops_url']."/user.php",2,_MD_MUSTREGFIRST);
		exit();
	} 
	include($xoopsConfig['root_path']."header.php");
    	OpenTable();
    	mainheader();
    	echo "<table width=\"100%\" cellspacing=0 cellpadding=1 border=0><tr><td colspan=2>\n";
    	echo "<table width=\"100%\" cellspacing=0 cellpadding=8 border=0><tr><td>\n";
    	echo "<br><br>\n";
    	echo "<LI>"._MD_SUBMITONCE."</li>\n";
        echo "<LI>"._MD_ALLPENDING."</li>\n";
        echo "<LI>"._MD_DONTABUSE."</li>\n";
	echo "<LI>"._MD_TAKEDAYS."</li>\n";

        echo "<form action=\"submit.php\" method=post>\n";
        echo "<table width=\"80%\"><tr>";
        echo "<td align=\"right\" nowrap><b>"._MD_FILETITLE."</b></td><td>";
    	echo "<input type=\"text\" name=\"title\" size=\"50\" maxlength=\"100\">";
    	echo "</td></tr><tr><td align=\"right\" nowrap><b>"._MD_DLURL."</b></td><td>";
        echo "<input type=\"text\" name=\"url\" size=\"50\" maxlength=\"250\" value=\"http://\">";
        echo "</td></tr>";
        echo "<tr><td align=\"right\" nowrap><b>"._MD_CATEGORY."</b></td><td>";
        $mytree->makeMySelBox("title", "title");
        echo "</td></tr>\n";
        echo "<tr><td align=\"right\" nowrap><b>"._MD_HOMEPAGEC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"homepage\" size=\"50\" maxlength=\"100\"></td></tr>\n";
        echo "<tr><td align=\"right\" nowrap><b>"._MD_VERSIONC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"version\" size=\"10\" maxlength=\"10\"></td></tr>\n";
        echo "<tr><td align=\"right\" nowrap><b>"._MD_FILESIZEC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"size\" size=\"10\" maxlength=\"8\">"._MD_BYTES."</td></tr>\n";
        echo "<tr><td align=\"right\" nowrap><b>"._MD_PLATFORMC."</b></td><td>\n";
        echo "<input type=\"text\" name=\"platform\" size=\"45\" maxlength=\"50\"></td></tr>\n";
/*      echo "<tr><td align=\"right\" nowrap>logourl</td><td>\n";
        echo "<input type=\"text\" name=\"logourl\" size=\"50\" maxlength=\"60\"></td></tr>\n";*/

        echo "<tr><td align=\"right\" valign=\"top\" nowrap><b>"._MD_DESCRIPTIONC."</b></td><td>\n";
        echo "<textarea name=description cols=50 rows=6></textarea>\n";
        echo "</td></tr>\n";
        echo "</table>\n";
        echo "<br>";
        echo "<input type=\"hidden\" name=\"submitter\" value=\"".$xoopsUser->uid()."\"></input>";
        echo "<center><input type=\"submit\" name=\"submit\" class=\"button\" value=\""._MD_SUBMIT."\"></input>\n";
	echo "&nbsp;<input type=button value="._MD_CANCEL." onclick=\"javascript:history.go(-1)\"></input></center>\n";
        echo "</form>\n";
        echo "</td></tr></table></td></tr></table>";
        CloseTable();

}

include("footer.php");
?>