<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

$myts = new MyTextSanitizer; // MyTextSanitizer object
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"),"cid","pid");

// Used to view just a single DL file information. Called from the rating pages

$lid = $HTTP_GET_VARS['lid'];
//$cid = $HTTP_GET_VARS['cid'];
include($xoopsConfig['root_path']."header.php");
OpenTable();
mainheader();
$q = "SELECT d.lid, d.cid, d.title, d.url, d.homepage, d.version, d.size, d.platform, d.logourl, d.status, d.date, d.hits, d.rating, d.votes, d.comments, t.description FROM ".$xoopsDB->prefix("mydownloads_downloads")." d, ".$xoopsDB->prefix("mydownloads_text")." t WHERE d.lid=$lid AND d.lid=t.lid AND status>0";
$result=$xoopsDB->query($q);
list($lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $status, $time, $hits, $rating, $votes, $comments, $description)=$xoopsDB->fetch_row($result);
echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\"><tr><td align=\"center\">\n";
echo "<table width=\"100%\" cellspacing=\"2\" cellpadding=\"2\" border=\"0\" bgcolor=\"cccccc\"><tr><td>\n";
$pathstring = "<a href=index.php>"._MD_MAIN."</a>&nbsp;:&nbsp;";
$nicepath = $mytree->getNicePathFromId($cid, "title", "viewcat.php?op=");
$pathstring .= $nicepath;
echo "<b>".$pathstring."</b>";
echo "</td></tr></table><br>";
echo "<table width=\"100%\" cellspacing=0 cellpadding=10 border=0>";

$rating = number_format($rating, 2);
$title = $myts->makeTboxData4Show($title);
$url = $myts->makeTboxData4Show($url);
$url = urldecode($url);
$homepage = $myts->makeTboxData4Show($homepage);
$version = $myts->makeTboxData4Show($version);
$size = $myts->makeTboxData4Show($size);
$platform = $myts->makeTboxData4Show($platform);
$logourl = $myts->makeTboxData4Show($logourl);
#$logourl = urldecode($logourl);
$datetime = formatTimestamp($time,"s");
$description = $myts->makeTareaData4Show($description);
include("include/dlformat.php");
    		
echo "</td></tr></table>\n";
echo "</td></tr></table>\n";
CloseTable();
    	
include("footer.php");

?>