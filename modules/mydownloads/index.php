<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

$myts = new MyTextSanitizer; // MyTextSanitizer object
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"),"cid","pid");


$q = "SELECT cid, title, imgurl FROM ".$xoopsDB->prefix("mydownloads_cat")." WHERE pid = 0 ORDER BY title";
$result=$xoopsDB->query($q) or die("");
if($xoopsConfig['startpage'] == "mydownloads"){
	$xoopsOption['show_rblock'] =1;
	include($xoopsConfig['root_path']."header.php");
	make_cblock();
	echo "<br />";
}else{
	$xoopsOption['show_rblock'] =0;
	include($xoopsConfig['root_path']."header.php");
}
OpenTable();
$mainlink = 0;
mainheader($mainlink);
echo "<center>\n";
echo "<table border=\"0\" cellspacing=\"5\" cellpadding=\"0\" width=\"90%\"><tr>\n";
$count = 0;
while($myrow = $xoopsDB->fetch_array($result)) {
	$title = $myts->makeTboxData4Show($myrow['title']);
    	echo "<td valign=\"top\" align=\"right\">";
    	if ($myrow['imgurl'] && $myrow['imgurl'] != "http://"){
		$imgurl = $myts->makeTboxData4Edit($myrow['imgurl']);
		echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/viewcat.php?cid=".$myrow['cid']."\"><img src=\"".$imgurl."\" height=\"50\" border=\"0\"></a>";
    	} else {
		echo "";
    	}
	$totaldownload = getTotalItems($myrow['cid'], 1);

    	echo "</td><td valign=\"top\" width=\"40%\"><a href=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/viewcat.php?cid=".$myrow['cid']."\"><b>$title</b></a>&nbsp;($totaldownload)<br>";
	// get child category objects
	$arr=array();
	$arr=$mytree->getFirstChild($myrow['cid'], "title");
	$space = 0;
	$chcount = 0;
	foreach($arr as $ele){
		$chtitle=$myts->makeTboxData4Show($ele['title']);
		if ($chcount>5){
			echo "...";
			break;
		}
		if ($space>0) {
        		echo ", ";
        	}
        	echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/viewcat.php?cid=".$ele['cid']."\">".$chtitle."</a>";
        	$space++;
		$chcount++;
	}
    	if ($count<1) {
        	echo "</td>";
    	}
    	$count++;
    	if ($count==2) {
        	echo "</td></tr><tr>";
        	$count = 0;
    	}
}
echo "</td></tr></table>";
list($numrows)=$xoopsDB->fetch_row($xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("mydownloads_downloads")." WHERE status>0"));
echo "<br><br>";
printf(_MD_THEREARE,$numrows);
echo "</center>";
CloseTable();

echo "<br>";

OpenTable();
echo "<div align=\"center\"><big><b>"._MD_LATESTLIST."</b></big><br><br>";
showNew($mytree);
echo "</div>";
CloseTable();	
include($xoopsConfig['root_path']."modules/mydownloads/footer.php");

// Shows the Latest Listings on the front page
function showNew($mytree){
	global $myts, $xoopsDB, $xoopsTheme, $xoopsConfig, $xoopsModule;
	global $mydownloads_shotwidth, $mydownloads_newdownloads, $mydownloads_useshots;
	$result = $xoopsDB->query("SELECT d.lid, d.cid, d.title, d.url, d.homepage, d.version, d.size, d.platform, d.logourl, d.status, d.date, d.hits, d.rating, d.votes, d.comments, t.description FROM ".$xoopsDB->prefix("mydownloads_downloads")." d, ".$xoopsDB->prefix("mydownloads_text")." t WHERE d.status>0 AND d.lid=t.lid ORDER BY date DESC",0,$mydownloads_newdownloads,0);
	echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\" border=\"0\"><tr><td width=\"110\" align=\"center\">";
    	$x=0;
    	while(list($lid, $cid, $dtitle, $url, $homepage, $version, $size, $platform, $logourl, $status, $time, $hits, $rating, $votes, $comments, $description)=$xoopsDB->fetch_row($result)) {
		$rating = number_format($rating, 2);
                $dtitle = $myts->makeTboxData4Show($dtitle);
                $url = $myts->makeTboxData4Show($url);
                $homepage = $myts->makeTboxData4Show($homepage);
                $version = $myts->makeTboxData4Show($version);
                $size = $myts->makeTboxData4Show($size);
                $platform = $myts->makeTboxData4Show($platform);
                $logourl = $myts->makeTboxData4Show($logourl);
#               $logourl = urldecode($logourl);
                $datetime = formatTimestamp($time,"s");

                $description = $myts->makeTareaData4Show($description);
		include($xoopsConfig['root_path']."modules/mydownloads/include/dlformat.php");
        	$x++;
    	}
	echo "</table>";
	
}
?>