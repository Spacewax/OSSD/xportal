<?PHP

###############################################################################
# my Downloads v1.0                                                                #
#                                                                              #
# $mydownloads_popular:        The number of hits required for a download to be popular. Default = 100      #
# $mydownloads_newdownloads:        The number of downloads that appear on the front page as latest listings. Default = 10  #
# $mydownloads_sresults:        The number of downloads that appear in one page when performing search  Default = 10  #
# $mydownloads_perpage:            The number of downloads that appear for each page. Default = 10 #
# $mydownloads_useshots:            Use screenshots? Default = 1 (Yes) #
# $mydownloads_shotwidth:            Screenshot Image Width (Default = 140) #
###############################################################################

$mydownloads_popular = 100;
$mydownloads_newdownloads = 10;
$mydownloads_sresults = 10;
$mydownloads_perpage = 10;
$mydownloads_useshots = 1;
$mydownloads_shotwidth = 140;

?>