<?php

function mainheader($mainlink=1) {
	global $xoopsConfig;
    	echo "<br /><br /><p><div align=\"center\">";
	echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/index.php\"><img src=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/images/logo-en.gif\" border=\"0\" alt\"\" /></a>";
    	echo "</p><br /><br />";
}

function newdownloadgraphic($time, $status) {
    	$count = 7;
	$startdate = (time()-(86400 * $count));
    	if ($startdate < $time) {
		if($status==1){
			echo "&nbsp;<img src=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/images/newred.gif\" alt=\""._MD_NEWTHISWEEK."\" />";
		}elseif($status==2){
			echo "&nbsp;<img src=\"".$xoopsConfig['xoops_url']."/modules/mydownloads/images/update.gif\" alt=\""._MD_UPTHISWEEK."\" />";
        	}
    	}
}

function popgraphic($hits) {
    	global $mydownloads_popular;
    	if ($hits>=$mydownloads_popular) {
    		echo "&nbsp;<img src =\"".$xoopsConfig['xoops_url']."/modules/mydownloads/images/pop.gif\" alt=\""._MD_POPULAR."\" />";
    	}
}
//Reusable Link Sorting Functions
function convertorderbyin($orderby) {
    	if ($orderby == "titleA")                        $orderby = "title ASC";
    	if ($orderby == "dateA")                        $orderby = "date ASC";
    	if ($orderby == "hitsA")                        $orderby = "hits ASC";
    	if ($orderby == "ratingA")                        $orderby = "rating ASC";
    	if ($orderby == "titleD")                        $orderby = "title DESC";
    	if ($orderby == "dateD")                        $orderby = "date DESC";
    	if ($orderby == "hitsD")                        $orderby = "hits DESC";
    	if ($orderby == "ratingD")                        $orderby = "rating DESC";
    	return $orderby;
}
function convertorderbytrans($orderby) {
    	if ($orderby == "hits ASC")   $orderbyTrans = _MD_POPULARITYLTOM;
    	if ($orderby == "hits DESC")    $orderbyTrans = _MD_POPULARITYMTOL;
    	if ($orderby == "title ASC")    $orderbyTrans = _MD_TITLEATOZ;
   	if ($orderby == "title DESC")   $orderbyTrans = _MD_TITLEZTOA;
    	if ($orderby == "date ASC") $orderbyTrans = _MD_DATEOLD;
    	if ($orderby == "date DESC")   $orderbyTrans = _MD_DATENEW;
    	if ($orderby == "rating ASC")  $orderbyTrans = _MD_RATINGLTOH;
    	if ($orderby == "rating DESC") $orderbyTrans = _MD_RATINGHTOL;
    	return $orderbyTrans;
}
function convertorderbyout($orderby) {
    	if ($orderby == "title ASC")            $orderby = "titleA";
    	if ($orderby == "date ASC")            $orderby = "dateA";
    	if ($orderby == "hits ASC")          $orderby = "hitsA";
    	if ($orderby == "rating ASC")        $orderby = "ratingA";
    	if ($orderby == "title DESC")              $orderby = "titleD";
    	if ($orderby == "date DESC")            $orderby = "dateD";
    	if ($orderby == "hits DESC")          $orderby = "hitsD";
    	if ($orderby == "rating DESC")        $orderby = "ratingD";
    	return $orderby;
}

function PrettySize($size) {
    $mb = 1024*1024;
    if ( $size > $mb ) {
        $mysize = sprintf ("%01.2f",$size/$mb) . " MB";
    }
    elseif ( $size >= 1024 ) {
        $mysize = sprintf ("%01.2f",$size/1024) . " KB";
    }
    else {
        $mysize = sprintf(_MD_NUMBYTES,$size);
    }
    return $mysize;
}

//updates rating data in itemtable for a given item
function updaterating($sel_id){
	global $xoopsDB;
	$query = "select rating FROM ".$xoopsDB->prefix("mydownloads_votedata")." WHERE lid = ".$sel_id."";
	$voteresult = $xoopsDB->query($query);
    	$votesDB = $xoopsDB->num_rows($voteresult);
	$totalrating = 0;
    	while(list($rating)=$xoopsDB->fetch_row($voteresult)){
		$totalrating += $rating;
	}
	$finalrating = $totalrating/$votesDB;
	$finalrating = number_format($finalrating, 4);
	$query =  "UPDATE ".$xoopsDB->prefix("mydownloads_downloads")." SET rating=$finalrating, votes=$votesDB WHERE lid = $sel_id";
    	$xoopsDB->query($query);
}

//returns the total number of items in items table that are accociated with a given table $table id
function getTotalItems($sel_id, $status=""){
	global $xoopsDB, $mytree;
	$count = 0;
	$arr = array();
	$query = "select count(*) from ".$xoopsDB->prefix("mydownloads_downloads")." where cid=".$sel_id."";
	if($status!=""){
		$query .= " and status>=$status";
	}
	$result = $xoopsDB->query($query);
	list($thing) = $xoopsDB->fetch_row($result);
	$count = $thing;
	$arr = $mytree->getAllChildId($sel_id);
	$size = sizeof($arr);
	for($i=0;$i<$size;$i++){
		$query2 = "select count(*) from ".$xoopsDB->prefix("mydownloads_downloads")." where cid=".$arr[$i]."";
		if($status!=""){
			$query2 .= " and status>=$status";
		}
		$result2 = $xoopsDB->query($query2);
		list($thing) = $xoopsDB->fetch_row($result2);
		$count += $thing;
	}
	return $count;
}
?>