<?php
$modversion['name'] = _MI_EPHEMERIDES_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_EPHEMERIDES_DESC;
$modversion['author'] = "";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = "yes";
$modversion['image'] = "ephemerides_slogo.jpg";
$modversion['dirname'] = "ephemerides";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Blocks
$modversion['blocks'][1]['file'] = "ephemerides.php";
$modversion['blocks'][1]['name'] = _MI_EPHEMERIDES_BNAME;
$modversion['blocks'][1]['description'] = "One day like today";
$modversion['blocks'][1]['show_func'] = "b_ephemerides_show";

// Menu
$modversion['hasMain'] = 0;
?>