<?php
//%%%%%%	Admin Module Name  Ephemerides 	%%%%%
define("_AM_DBUPDATED","Database Updated Successfully!");

define("_AM_ADDEPHEM","Add Ephemerid");
define("_AM_DAYT","Day:");
define("_AM_MONTHT","Month:");
define("_AM_YEART","Year:");
define("_AM_EMDESCT","Ephemerid Description:");
define("_AM_SEND","Send");
define("_AM_MAINTANED","Ephemerid Maintenance (Edit/Delete)");
define("_AM_EDIT","Edit");
define("_AM_MAINTAN","Ephemerids Maintenance");
define("_AM_DELETE","Delete");
define("_AM_EDITEPHEMT","Edit Ephemerid:");
?>