<?php
// Module Info

// The name of this module
define("_MI_EPHEMERIDES_NAME","Ephemerides");
// A brief description of this module
define("_MI_EPHEMERIDES_DESC","Shows ephemerides in a block");
// Names of blocks for this module (Not all module has blocks)
define("_MI_EPHEMERIDES_BNAME","Ephemerides Block");
?>