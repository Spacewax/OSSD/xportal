<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include_once("admin_header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

/*********************************************************/
/* Ephemerids Functions to have a Historic Ephemerids    */
/*********************************************************/

function Ephemerids() {
	global $xoopsConfig, $xoopsModule;
    	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	OpenTable();
    	echo "
    	<h4>"._AM_ADDEPHEM."</h4>
    	<form action='index.php' method='post'>";
    	$nday = "1";
    	echo ""._AM_DAYT." <select name='did'>";
    	while ($nday<=31) {
		echo "<option name='did'>$nday</option>";
		$nday++;
    	}
    	echo "</select>";
    	$nmonth = "1";
    	echo ""._AM_MONTHT." <select name='mid'>";
    	while ($nmonth<=12) {
		echo "<option name='mid'>$nmonth</option>";
		$nmonth++;
    	}
    	echo "</select>"._AM_YEART." <input type='text' name='yid' maxlength='4' size='5' /><br /><br />
    	"._AM_EMDESCT."<br />
    	<textarea name='content' cols='60' rows='10'></textarea><br /><br />
    	<input type='hidden' name='op' value='Ephemeridsadd' />
    	<input type='submit' value='"._AM_SEND."' />
    	</form>
    
    	<br /><br />
    	<h4>"._AM_MAINTANED."</h4>
    	<form action='index.php' method='post'>";
    	$nday = "1";
    	echo ""._AM_DAYT." <select name='did'>";
    	while ($nday<=31) {
		echo "<option name='did'>$nday</option>";
		$nday++;
    	}
    	echo "</select>";
    	$nmonth = "1";
    	echo ""._AM_MONTHT." <select name='mid'>";
    	while ( $nmonth <= 12 ) {
		echo "<option name='mid'>$nmonth</option>";
		$nmonth++;
    	}
    	echo "</select>";
    	echo "
    	<br /><br />
    	<input type='hidden' name='op' value='Ephemeridsmaintenance' />
    	<input type='submit' value='"._AM_EDIT."' />
    	</form>";
    	CloseTable();
}

function Ephemeridsadd($did, $mid, $yid, $content) {
	global $xoopsDB;
	$myts=new MyTextSanitizer;
	$content = $myts->makeTareaData4Save($content);
	$newid = $xoopsDB->GenID("ephem_eid_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("ephem")." (eid, did, mid, yid, content) VALUES ($newid, $did, $mid, $yid, '$content')");
	redirect_header("index.php?op=Ephemerids",1,_AM_DBUPDATED);
	exit();
}

function Ephemeridsmaintenance($did, $mid) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts=new MyTextSanitizer;
    	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	OpenTable();
    	echo "
    	<h4>"._AM_MAINTAN."</h4>";
    	$result=$xoopsDB->query("SELECT eid, did, mid, yid, content FROM ".$xoopsDB->prefix("ephem")." WHERE did=$did AND mid=$mid");
    	while(list($eid, $did, $mid, $yid, $content) = $xoopsDB->fetch_row($result)) {
		$content=$myts->makeTareaData4Show($content);
    		echo "<b>$yid</b> [ <a href='index.php?op=Ephemeridsedit&eid=$eid&did=$did&mid=$mid'>"._AM_EDIT."</a> | <a href='index.php?op=Ephemeridsdel&eid=$eid&did=$did&mid=$mid'>"._AM_DELETE."</a> ]<br />$content<br /><br /><br />";
    	}
    	CloseTable();
}

function Ephemeridsdel($eid, $did, $mid) {
	global $xoopsDB;
    	$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("ephem")." WHERE eid=$eid");
	redirect_header("index.php?op=Ephemeridsmaintenance&did=$did&mid=$mid",1,_AM_DBUPDATED);
	exit();
}

function Ephemeridsedit($eid, $did, $mid) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts=new MyTextSanitizer;
    	include ($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
    	$result=$xoopsDB->query("SELECT yid, content FROM ".$xoopsDB->prefix("ephem")." where eid=$eid");
    	list($yid, $content) = $xoopsDB->fetch_row($result);
    	$content=$myts->makeTareaData4Edit($content);
    	OpenTable();
    	echo "
    	<h4>"._AM_MAINTAN."</h4>
    	<b>"._AM_EDITEPHEMT."</b><br /><br />
    	<form action='index.php' method='post'>";
    	echo _AM_YEART." <input type='text' name='yid' value='$yid' maxlength='4' size='5' /><br /><br />
    	"._AM_EMDESCT."<br />
    	<textarea name='content' cols='60' rows='10'>$content</textarea><br /><br />
    	<input type='hidden' name='did' value='$did' />
    	<input type='hidden' name='mid' value='$mid' />
    	<input type='hidden' name='eid' value='$eid' />
    	<input type='hidden' name='op' value='Ephemeridschange' />
    	<input type='submit' value='"._AM_SEND."' />
    	</form>";
    	CloseTable();
}

function Ephemeridschange($eid, $did, $mid, $yid, $content) {
	global $xoopsDB;
	$myts=new MyTextSanitizer;
    	$content = $myts->makeTareaData4Save($content);
    	$xoopsDB->query("UPDATE ".$xoopsDB->prefix("ephem")." SET yid=$yid, content='$content' where eid=$eid");
	redirect_header("index.php?op=Ephemeridsmaintenance&did=$did&mid=$mid",1,_AM_DBUPDATED);
	exit();
}

switch($op) {
	case "Ephemeridsedit":
		Ephemeridsedit($eid, $did, $mid);
		break;
	case "Ephemeridschange":
		Ephemeridschange($eid, $did, $mid, $yid, $content);
		break;
	case "Ephemeridsdel":
		Ephemeridsdel($eid, $did, $mid);
		break;
	case "Ephemeridsmaintenance":
		Ephemeridsmaintenance($did, $mid);
		break;
	case "Ephemeridsadd":
		Ephemeridsadd($did, $mid, $yid, $content);
		break;
	case "Ephemerids":
		Ephemerids();
		break;
	default:
		Ephemerids();
		break;
}
include("admin_footer.php");
?>