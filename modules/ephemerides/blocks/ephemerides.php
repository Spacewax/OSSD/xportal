<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_ephemerides_show(){
	global $xoopsDB, $xoopsConfig;
	$block = array();
	$block['title'] = _MB_EPHEMERIDES_TITLE;
        $today = getdate();
        $eday = $today["mday"];
        $emonth = $today["mon"];
        $result = $xoopsDB->query("SELECT yid, content FROM ".$xoopsDB->prefix("ephem")." WHERE did=$eday AND mid=$emonth");
        $block['content'] = "<div align=\"center\"><b>"._MB_EPHEMERIDES_ODLT."</b></div><br />";
        while($myrow = $xoopsDB->fetch_array($result)) {
            	if ($cnt==1) {
                	$block['content'] .= "<br /><br />";
            	}
            	$block['content'] .= "<b>".$myrow['yid']."</b><br />";
                $block['content'] .= $myrow['content'];
            	$cnt = 1;
        }
        return $block;
}
?>