<?php

// ----------------------------------------------------------------------------
// MODULE: daily_stats
// ----------------------------------------------------------------------------

function module_daily_stats($arguments) {
    global $gDb;
    
    global $lvc_display_cache_delay;
    global $lvc_table_visitors;

    global $lvm_daily_stats, $lvm_day_visitors, $lvm_day_direct, $lvm_day_referers;
    
    $date = (!isset($arguments['date']))
                ? date('Y/m/d')
                : str_replace('-', '/', $arguments['date']);

    $buffer  = "<br /><div style='text-align: center;'><table cellspacing='1' border='0'>\n";
    $buffer .= "<tr><td colspan=3><a class='array'>$lvm_daily_stats</a></td></tr>\n";

    // count visitors
    $query  = "SELECT COUNT(*) AS C ";
    $query .= "FROM ".$lvc_table_visitors." ";
    $query .= "WHERE DATE LIKE '".$date."%'";

    if ( $gDb->DbQuery($query) && $gDb->DbNumRows() != 0 ) {
        $gDb->Row = $gDb->DbNextRow();
        $data['visitors'] = $gDb->Row['C'];

        $buffer .= "<tr><th class='vis' align='left'>$lvm_day_visitors</th>";
        $buffer .= "<td class='vis' align='right' colspan=2>&nbsp;<b>".number_format($data['visitors'], 0, ',', ' ')."</b>&nbsp;</td></tr>\n";
    } else {
        return '';
    }

    // count referers
    $query  = "SELECT COUNT(*) AS C ";
    $query .= "FROM ".$lvc_table_visitors." ";
    $query .= "WHERE DATE LIKE '".$date."%' ";
    $query .= "AND REF_HOST <> '' AND REF_HOST <> '[unknown origin]' AND REF_HOST <> 'bookmarks' ";

    if ( $gDb->DbQuery($query) && $gDb->DbNumRows() != 0 ) {
        $gDb->Row = $gDb->DbNextRow();
        $data['referers'] = $gDb->Row['C'];

        $buffer .= "<tr><th class='vis' align='left'>$lvm_day_direct</th>";
  	    $percent = ($data['visitors'] == 0) ? 0 : round((($data['visitors'] - $data['referers']) / $data['visitors']) * 1000) / 10;
        $buffer .= "<td class='vis' align='right'>&nbsp;".sprintf('%.1f', $percent)." %&nbsp;</td>\n"; // %
        $buffer .= "<td class='vis' align='right'>&nbsp;<b>".number_format($data['visitors'] - $data['referers'], 0, ',', ' ')."</b>&nbsp;</td></tr>\n";
        
        $buffer .= "<tr><th class='vis' align='left'>$lvm_day_referers</th>";
  	    $percent = ($data['visitors'] == 0) ? 0 : 100 - $percent;
        $buffer .= "<td class='vis' align='right'>&nbsp;".sprintf('%.1f', $percent)." %&nbsp;</td>\n"; // %
        $buffer .= "<td class='vis' align='right'>&nbsp;<B>".number_format($data['referers'], 0, ',', ' ')."</b>&nbsp;</td></tr>\n";
    } else {
        return '';
    }

    // cache delay
    if ( $lvc_display_cache_delay ) {
        $buffer .= "<tr><td align='center' colspan='3'>";
        $buffer .= "<a class='delay'>".cache_delay($arguments['cache'])."</a>";
    }

    $buffer .= "</table></div><br />\n";

    return $buffer;
}

?>