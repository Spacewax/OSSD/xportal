<?php

// ----------------------------------------------------------------------------
// MODULE: calendar
// ----------------------------------------------------------------------------
function module_calendar($arguments) {
	global $gDb;

    global $lvc_nb_months_calendar;
	global $lvc_site_opening_year;
	global $lvc_site_opening_month;
    global $lvc_table_visitors;
    global $lvc_display_cache_delay;
    
	global $lvm_arr_months, $lvm_visitors_per_day, $lvm_month;
    global $lvm_average, $lvm_total;

    $buffer = '';

    $current_year  = date('Y');
    $current_month = date('n');
    $today         = date('Y/m/d');

    $first_year  = $current_year;
    $first_month = $current_month;

    // looking for first month and first year in calendar
    for ( $cnt_month = 1; $cnt_month < $lvc_nb_months_calendar && !($first_year == $lvc_site_opening_year && $first_month == $lvc_site_opening_month); $cnt_month++ ) {
		$first_month = ($first_month == 1) ? 12 : $first_month - 1;
        $first_year = ($first_month == 12) ? $first_year -1 : $first_year;
    }

    $buffer .= "<br /><div style='text-align: center;'><table cellspacing='1' border='0'>
	<tr><td colspan='17' align='left'><a class='array'>". str_replace("{NB_LAST_MONTHS}", $cnt_month, $lvm_visitors_per_day)."</a></td><td colspan='17' align='right'>&nbsp;";

    if ( $lvc_display_cache_delay ) {
		$buffer .= "<a class='delay'>".cache_delay($arguments['cache'])."</a>";
	}
    $buffer .= "</td></tr>\n";
	$buffer .= "<tr><th class='vis'>$lvm_month</th>";

    for ( $count = 1; $count <= 31; $count++ ) {
		$buffer .= "<th class='vis'>" . (($count < 10) ? "0".$count : $count) . "</th>";
	}

    $buffer .= "<th class='vis'>$lvm_average</th>\n";
    $buffer .= "<th class='vis'>$lvm_total</th></tr>\n";

    $Month  = $first_month;
    $Year   = $first_year;

    $finished = false;
    $prev_year = $Year;
    
    $big_cnt_values = 0;
    $big_total  = 0;

	for ( $count = 1; $count <= $cnt_month; $count++ ) {  
		// separation between 2 years
        if ( $Year != $prev_year ) {
			$buffer .= "<tr><th bgcolor='#ffffaa' colspan='34' height='1'>";
            $buffer .= html_image("images/nothing.gif");
            $buffer .= "</th></tr>\n";
        }
        
        // load month archive if exists
        $data = archive_month($Month, $Year, 'vpj');  // vpj: visitor per day
        
		if ( $archive = ($data[0] != NO_ARCHIVE) ) {
            $values = explode('+', $data[0]);
		}
  
        $buffer .= "<tr>\n";
        $buffer .= "<td class='month'>&nbsp;<a class='month' href='?view=".VIEW_MONTH."&amp;year=$Year&amp;month=".sprintf('%02d', $Month)."'>" . $lvm_arr_months[$Month] . "</a>&nbsp;</td>";

        $the_month = sprintf('%02d', $Month);
        
        $total = 0;
        $cnt_values = 0;
        
        for ( $count2 = 1; $count2 <= 31; $count2++ ) {
  	        if ( checkdate($Month, $count2, $Year) ) {
	            $day = date('D', mktime(12,0,0,$Month, $count2, $Year));
                $color = (($day == 'Sat') || ($day == 'Sun')) ? 'vis3' : 'vis1';
	            $buffer .= "<td class='".$color."' align='center'>";
                $the_day = $Year.'/'.$the_month.'/'.(($count2 < 10) ? '0'.$count2 : $count2);
	  
	            if ( !$finished ) {
		            if ( $archive ) {
		                $val = $values[($count2 * 2) - 1];
		            } else {
	                    $query  = "SELECT COUNT(*) AS D FROM ".$lvc_table_visitors." WHERE DATE LIKE '".$the_day."%'";
                        
						$gDb->DbQuery($query);
						$gDb->DbNextRow();

						$record = $gDb->Row;

		                $val = $record['D'];
		            }
	                
                    if ( $val > 0 ) {
						$cnt_values++;
					}

		            $buffer .= ($val == 0) ? '&nbsp;' : $val;
		            $total += $val;
	            } else {
	                $buffer .= '&nbsp;';
				}
      
	            $finished = $finished || ($the_day == $today);
	            $buffer .= "</td>";
	        } else {
                $buffer .= "<td class='vis2' align='right'>&nbsp;</td>";
			}
        }
    
        if ( $cnt_values > 0 ) {
            $big_cnt_values += $cnt_values;
            $big_total += $total;
        }

        $buffer .= "<td class='avg' align='right' nowrap='nowrap'>&nbsp;".($cnt_values == 0 ? '' : number_format(round($total/$cnt_values), 0, '', ' '))."&nbsp;</td>\n";

        $buffer .= "<td class='month' align='right' nowrap='nowrap'>&nbsp;".($total == 0 ? '' : number_format($total, 0, '', ' '))."&nbsp;</td></tr>\n";
        
        $prev_year = $Year;

        $Month = ($Month == 12) ? 1         : $Month + 1;
        $Year  = ($Month ==  1) ? $Year + 1 : $Year;
    }
    
    $buffer .= "<tr><th colspan='32'>";
    $buffer .= html_image("images/nothing.gif");
    $buffer .= "</th><th bgcolor='#b8c8fe' colspan='2' height='1'>";
    $buffer .= html_image("images/nothing.gif");
    $buffer .= "</th></tr>\n";

    $buffer .= "<td colspan='32'>&nbsp;</td>";
    $buffer .= "<td class='avg' align='right' nowrap='nowrap'>&nbsp;".($big_cnt_values == 0 ? '' : number_format(round($big_total/$big_cnt_values), 0, '', ' '))."&nbsp;</td>\n";
    $buffer .= "<td class='month' align='right' nowrap='nowrap'>&nbsp;".($big_total == 0 ? '' : number_format($big_total, 0, '', ' '))."&nbsp;</td></tr>\n";

    $buffer .= "</table></div><br />\n";

    return $buffer;
}

?>