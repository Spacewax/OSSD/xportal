<?php

// ------------------------------------------------------------------------- //
// Les Visiteurs - Statistiques de fréquentation d'un site web               //
// ------------------------------------------------------------------------- //
// Visitors      - Web site statistics analysis program                      //
// ------------------------------------------------------------------------- //
// Copyright (C) 2000, 2001  J-Pierre DEZELUS <jpdezelus@phpinfo.net>        //
// ------------------------------------------------------------------------- //
//                   phpInfo.net <http://www.phpinfo.net/>                   //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

echo "<br /><div style='text-align:center;'><table cellpadding='2' cellspacing='1' border='0'><tr>";

if ( $view != VIEW_ADMIN ) {
	echo "<th class='vis'><b>".${"lvm_title_".$view}."</b></th>";
}

echo "<td class='fond' width='0'>&nbsp;</td>
<th class='vis'>&nbsp;<b>".$lvm_title_others."</b>&nbsp;</th>
<th class='vis'>&nbsp;<b>".str_replace('{NB_LAST_MONTHS}', $lvc_nb_last_months, $lvm_title_last_months)."</b>&nbsp;</th>";
if ( $lvc_view_admin_menus ) {
	echo "<th class='vis'><b>$lvm_title_admin</b></th>";
}
echo "</tr><tr>";
if ( $view != VIEW_ADMIN ) {
	echo "<td class='vis' nowrap='nowrap' valign='top'>";
	include($lvc_include_dir.'/menus-'.$view.'.inc.php');
    echo "</td>";
}

echo "<td class='fond'>&nbsp;</td>
<td class='vis' nowrap='nowrap' valign='top'>";
include($lvc_include_dir.'/menus-others.inc.php');
echo "</td>
<td class='vis' nowrap='nowrap' valign='top'>";
include($lvc_include_dir.'/menus-archives.inc.php');
echo "</td>";
      
if ($lvc_view_admin_menus) {
	echo "<td class='vis' nowrap='nowrap' valign='top'>";
	include($lvc_include_dir.'/menus-admin.inc.php');
    echo "</td>";
}

echo "</tr></table></div>";
echo html_hr();

?>