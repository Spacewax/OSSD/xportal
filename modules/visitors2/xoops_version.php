<?php
$modversion['name'] = _MI_VISITORS2_NAME;
$modversion['version'] = 1.01;
$modversion['description'] = _MI_VISITORS2_DESC;
$modversion['credits'] = "Modularized into XOOPS by Kazumi Ono<br>( http://www.mywebaddons.com/ )";
$modversion['author'] = "Original Visiteurs 2.0.1 from phpinfo.net ( http://www.phpinfo.net/ )";
$modversion['help'] = "visitors2.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "images/poweredby.gif";
$modversion['dirname'] = "visitors2";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/admin.php";

// Menu
$modversion['hasMain'] = 1;
?>