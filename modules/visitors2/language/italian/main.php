<?php

// Translation [french -> italian] by Gio Ducci <gducci@rpbw.com>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Modulo di Cache";
$lvm_adm_archiver      = "Archivio";
$lvm_admin             = "Amministrazionz";
$lvm_agent             = "Agente";
$lvm_archived          = "Archiviato";
$lvm_archive_created   = "Archivio creato senza errori !";
$lvm_archiver_check    = "Verifica la presenza o meno dell'archivio nel database.";
$lvm_archiver_archive  = "Crea o aggiorna i dati nella lista '<B>{ARCHIVES_TABLE}</B>' per il mese selezionato.";
$lvm_archiver_export   = "Esporta in un file testo i dati nella lista '<B>{VISITORS_TABLE}</B>' del mese selezionato.";
$lvm_archiver_delete   = "Sopprime <U><B>definitivamente</B></U> dalla lista '<B>{VISITORS_TABLE}</B>' le registrazioni del mese selezionato.<BR>Prima di lanciare questa operazione, verificate l'esportazione dei dati del mese (tappa 2) e la presenza dell'archivio nel database.";
$lvm_average           = "Moy.";
$lvm_btn_archive       = "Archiviare";
$lvm_btn_check         = "Verificare";
$lvm_btn_delete        = "Purgare";
$lvm_btn_export        = "Esportare";
$lvm_calendar          = "Calendario";
$lvm_check_archive     = "Verificare l'archivio";
$lvm_check_all         = "Selzionare tutto";
$lvm_create_archive    = "Creare l'archivio";
$lvm_confirm_delete    = "Confermate la soppressione ?";
$lvm_connection_error  = "La connessione al database � impossibile !";
$lvm_creation_date     = "Data di creazione";
$lvm_daily_stats       = "Le Cifre del Giorno";
$lvm_day_direct        = "In Accessso Diretto";
$lvm_day_referers      = "Per Referenti";
$lvm_day_visitors      = "Numero di Visitatori";
$lvm_delay_days        = "gg";
$lvm_delay_hours       = "ore";
$lvm_delay_last        = "agg";
$lvm_delay_minutes     = "min";
$lvm_delay_seconds     = "s";
$lvm_delete            = "Sopprimere";
$lvm_delete_ok         = "Soppressione effettuata !";
$lvm_delete_visitors   = "Purgare la tavola {VISITORS_TABLE}";
$lvm_description       = "Descrizione";
$lvm_directory         = "Annuario";
$lvm_domain            = "Dominio";
$lvm_error_nodata      = "Nessun dato nella lista {VISITORS_TABLE} per il mese selezionato !";
$lvm_file              = "File";
$lvm_go_back           = "Indietro";
$lvm_header_title      = "I Visitatori di";
$lvm_host              = "Host";
$lvm_invalid_password  = "Password non esatta";
$lvm_last_months       = "{NB_LAST_MONTHS} ultimo Mese";
$lvm_last_visitors     = "Gli {NB_LAST_VISITORS} Ultimi Visitatori";
$lvm_month             = "Mese";
$lvm_no                = "No";
$lvm_no_archive        = "Alcun archivio trovato per questo periodo";
$lvm_no_cache          = "Alcun modulo di cache trovato !";
$lvm_no_delete         = "Soppressione annullata";
$lvm_number            = "Num";
$lvm_os                = "OS";
$lvm_others_os         = "Altri";
$lvm_others_agent      = "Altri";
$lvm_page_title        = "I Visitatori";
$lvm_password          = "Password (base)";
$lvm_per_hour          = "Visitatori/Ora";
$lvm_per_day           = "Visitatori/Giorno";
$lvm_referer           = "Referente";
$lvm_size              = "Taglia";
$lvm_time              = "Ora";
$lvm_total             = "Totale";
$lvm_today             = "Oggi";

$lvm_title_admin         = "Amministrazione";
$lvm_title_last_months   = "{NB_LAST_MONTHS} Ultimo mese";
$lvm_title_current_month = "Statistiche del Mese";
$lvm_title_day           = "Statistiche Quotidiane";
$lvm_title_month         = "Statistiche Mensili";
$lvm_title_others        = "Autres Statistiche";
$lvm_title_user          = "Statistiche Personalizzate";
$lvm_title_year          = "Statistiche Annuali";

$lvm_top_agent         = "I Visitatori";
$lvm_top_agent_menu    = "Top Ten Navigatori";
$lvm_top_agent_os      = "Il Top Ten {NB_TOP_AGENT_OS} dei Navigatori/OS";
$lvm_top_agent_os_menu = "Top Ten Navigatori/OS";
$lvm_top_domain        = "Il Top Ten {NB_TOP_DOMAIN} dei Domini";
$lvm_top_domain_menu   = "Top Ten Domini";
$lvm_top_os            = "Gli OS";
$lvm_top_os_menu       = "Top Ten OS";
$lvm_top_referer       = "La Top Ten {NB_TOP_REFERER} dei Referenti";
$lvm_top_referer_menu  = "Top Ten Referenti";
$lvm_top_visitors      = "Le Top {NB_TOP_VISITORS} des Visiteurs";
$lvm_top_visitors_menu = "Top Ten Visitatori";

$lvm_uncheck_all       = "Deselezionare Tutto";
$lvm_visitors_day      = "Visitatori / Giorno";
$lvm_visitors_per_day  = "Visitatori / Giorno - Su {NB_LAST_MONTHS} mese";
$lvm_year              = "Anno";
$lvm_year_per_day      = "Visitatori / Giorno - Su 12 mesi";
$lvm_yearly_graph      = "Grafica Annuale";
$lvm_yes               = "Si";
$lvm_12_months         = "Visitatori / Mese - Su 12 mesi";

$lvm_arr_months        = Array(1 => "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", 
                                    "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");

// without accents
$lvm_arr_months_graph  = Array(1 => "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", 
                                    "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");

$lvm_arr_months_abbr   = Array(1 => "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization

    return($date);
}
?>