<?php

// Translation [french -> english] by Pascal NIZZRI <pascal.nizri@oreka.com>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Cache Modules";
$lvm_adm_archiver      = "Archive";
$lvm_admin             = "Administration";
$lvm_agent             = "Agent";
$lvm_archived          = "Stored in archives";
$lvm_archive_created   = "Archive created successfully !";
$lvm_archiver_check    = "Checks whether or not the archive is in the base.";
$lvm_archiver_archive  = "Create or update the table data '<B>{ARCHIVES_TABLE}</B>' for the selected month.";
$lvm_archiver_export   = "Export in a text file the table data '<B>{VISITORS_TABLE}</B>' of the selected month.";
$lvm_archiver_delete   = "Delete <U><B>definitively</B></U> from the table '<B>{VISITORS_TABLE}</B>' the records of the selected month.<BR>Before launching this operation, check the export of month's data (step 2) and the presence of the archive in the base.";

$lvm_average           = "Avg.";
$lvm_btn_archive       = "Archive";
$lvm_btn_check         = "Check";
$lvm_btn_delete        = "Purge";
$lvm_btn_export        = "Export";
$lvm_calendar          = "Calendar";
$lvm_check_archive     = "Check the archive";
$lvm_check_all         = "Check everything";
$lvm_create_archive    = "Create archive";
$lvm_confirm_delete    = "Delete ?";
$lvm_connection_error  = "Database connection failed !";
$lvm_creation_date     = "Date of creation";
$lvm_daily_stats       = "Numbers of the day";
$lvm_day_direct        = "Direct Access";
$lvm_day_referers      = "By referer";
$lvm_day_visitors      = "Number of Visitors";
$lvm_delay_days        = "d";
$lvm_delay_hours       = "h";
$lvm_delay_last        = "upd";
$lvm_delay_minutes     = "mn";
$lvm_delay_seconds     = "s";
$lvm_delete            = "Delete";
$lvm_delete_ok         = "Deleted !";
$lvm_delete_visitors   = "Purge the table {VISITORS_TABLE}";
$lvm_description       = "Description";
$lvm_directory         = "Address list";
$lvm_domain            = "Domain";
$lvm_error_nodata      = "No data in the table {VISITORS_TABLE} for the selected month !";
$lvm_file              = "File";
$lvm_go_back           = "Back";
$lvm_header_title      = "The Visitors from";
$lvm_host              = "Host";
$lvm_invalid_password  = "Mot de passe incorrect";
$lvm_last_months       = "{NB_LAST_MONTHS} Last Months";
$lvm_last_visitors     = "{NB_LAST_VISITORS} Last Visitors";
$lvm_month             = "Month";
$lvm_no                = "No";
$lvm_no_archive        = "No archives found for this period";
$lvm_no_cache          = "No cache module found !";
$lvm_no_delete         = "Deletion canceled";
$lvm_number            = "Nb";
$lvm_os                = "OS";
$lvm_others_os         = "Other";
$lvm_others_agent      = "Other";
$lvm_page_title        = "The Visitors";
$lvm_password          = "Password (base)";
$lvm_per_hour          = "Visitors/Hour";
$lvm_per_day           = "Visitors/Day";
$lvm_referer           = "Referer";
$lvm_size              = "Size";
$lvm_time              = "Hour";
$lvm_total             = "Total";
$lvm_today             = "Today";

$lvm_title_admin         = "Stats Administration";
$lvm_title_last_months   = "{NB_LAST_MONTHS} Last Months";
$lvm_title_current_month = "Month Stats";
$lvm_title_day           = "Daily Stats";
$lvm_title_month         = "Monthly Stats";
$lvm_title_others        = "Other Stats";
$lvm_title_user          = "Personalized Stats";
$lvm_title_year          = "Annual Stats";

$lvm_top_agent         = "The Browsers";
$lvm_top_agent_menu    = "Top Browsers";
$lvm_top_agent_os      = "The Top {NB_TOP_AGENT_OS} of Browsers/OS";
$lvm_top_agent_os_menu = "Top Browsers/OS";
$lvm_top_domain        = "The Top {NB_TOP_DOMAIN} of Domains";
$lvm_top_domain_menu   = "Top Domains";
$lvm_top_os            = "The OS";
$lvm_top_os_menu       = "Top OS";
$lvm_top_referer       = "The Top {NB_TOP_REFERER} of Referers";
$lvm_top_referer_menu  = "Top Referers";
$lvm_top_visitors      = "The Top {NB_TOP_VISITORS} of Visitors";
$lvm_top_visitors_menu = "Top Visitors";

$lvm_uncheck_all       = "Uncheck Everything";
$lvm_visitors_day      = "Visitors / Day";
$lvm_visitors_per_day  = "Visitors / Day - Per {NB_LAST_MONTHS} months";
$lvm_year              = "Year";
$lvm_year_per_day      = "Visitors/Day - Per 12 months";
$lvm_yearly_graph      = "Annual Graphic";
$lvm_yes               = "Yes";
$lvm_12_months         = "Visitors / Month - Per 12 months";

$lvm_arr_months        = Array(1 => "January", "February", "March", "April", "May", "June", "July", 
                                    "August", "September", "October", "November", "December");

// without accents
$lvm_arr_months_graph  = Array(1 => "January", "February", "March", "April", "May", "June", "July", 
                                    "August", "September", "October", "November", "December");

$lvm_arr_months_abbr   = Array(1 => "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

function show_datetime($datetime) {   
	// for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization
    return $time;
}

?>