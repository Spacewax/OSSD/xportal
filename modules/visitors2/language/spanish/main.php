<?php

// Spanish translation by Valentin SANCHEZ <valentin.sanchez@ono.com>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Modulos Cache";
$lvm_adm_archiver      = "Archivos";
$lvm_admin             = "Administraci�n";
$lvm_agent             = "Agente";
$lvm_archived          = "Archivado";
$lvm_archive_created   = "Archivo creado !";
$lvm_archiver_check    = "Chequear si el archivo esta en base";
$lvm_archiver_archive  = "Crear o actualizar tabla de archivos '<B>{ARCHIVES_TABLE}</B>' para el mes seleccionado.";
$lvm_archiver_export   = "Exporta en archivo de texto '<B>{VISITORS_TABLE}</B>' mes selecionado.";
$lvm_archiver_delete   = "Borrar <U><B>permanentemente</B></U> de la tabla '<B>{VISITORS_TABLE}</B>' los registros del mes seleccionado.<BR>Antes de lanzar esta operaci�n, comprobar si se han exportado (paso 2) y estan presentes en archivo.";

$lvm_average           = "Media";
$lvm_btn_archive       = "Archivar";
$lvm_btn_check         = "Marcar";
$lvm_btn_delete        = "Purgar";
$lvm_btn_export        = "Exportar";
$lvm_calendar          = "Calendario";
$lvm_check_archive     = "Chequear el archivo";
$lvm_check_all         = "Chequear todo";
$lvm_create_archive    = "Crear archivo";
$lvm_confirm_delete    = "Eliminar ?";
$lvm_connection_error  = "Error en conexion a Base de Datos !";
$lvm_creation_date     = "Fecha de creaci�n";
$lvm_daily_stats       = "N�mero de d�as";
$lvm_day_direct        = "Acceso directo";
$lvm_day_referers      = "Por referencia";
$lvm_day_visitors      = "N�m. Visitantes";
$lvm_delay_days        = "d";
$lvm_delay_hours       = "h";
$lvm_delay_last        = "upd";
$lvm_delay_minutes     = "mn";
$lvm_delay_seconds     = "s";
$lvm_delete            = "Borrar";
$lvm_delete_ok         = "Borrar !";
$lvm_delete_visitors   = "Purgar tabla {VISITORS_TABLE}";
$lvm_description       = "Descripvi�n";
$lvm_directory         = "Lista Direcciones";
$lvm_domain            = "Dominio";
$lvm_error_nodata      = "No hay datos en la tabla {VISITORS_TABLE} para el mes seleccionado !";
$lvm_file              = "Fichero";
$lvm_go_back           = "Volver";
$lvm_header_title      = "Visitantes de";
$lvm_host              = "Host";
$lvm_invalid_password  = "Clave incorrecta";
$lvm_last_months       = "{NB_LAST_MONTHS} ultimos meses";
$lvm_last_visitors     = "{NB_LAST_VISITORS} ultimos visitantes";
$lvm_month             = "Mes";
$lvm_no                = "No";
$lvm_no_archive        = "No se encontraron archivos para este periodo";
$lvm_no_cache          = "No hay modulos en cache !";
$lvm_no_delete         = "Borrado cancelado";
$lvm_number            = "Nb";
$lvm_os                = "OS";
$lvm_others_os         = "Otros";
$lvm_others_agent      = "Otras";
$lvm_page_title        = "Visitantes";
$lvm_password          = "Clave (base)";
$lvm_per_hour          = "Visitantes/Hora";
$lvm_per_day           = "Visitantes/D�as";
$lvm_referer           = "Referencia";
$lvm_size              = "Tama�o";
$lvm_time              = "Hora";
$lvm_total             = "Total";
$lvm_today             = "Hoy";

$lvm_title_admin         = "Administracion";
$lvm_title_last_months   = "{NB_LAST_MONTHS} ultimos meses";
$lvm_title_current_month = "Mes actual";
$lvm_title_day           = "Diarias";
$lvm_title_month         = "Otros meses";
$lvm_title_others        = "Otras Estadisticas";
$lvm_title_user          = "Personalizadas";
$lvm_title_year          = "Anuales";

$lvm_top_agent         = "Navegadores";
$lvm_top_agent_menu    = "Mejores Navegadores";
$lvm_top_agent_os      = "Los {NB_TOP_AGENT_OS} Navegador/OS mas usados";
$lvm_top_agent_os_menu = "Mejor Navegador/OS";
$lvm_top_domain        = "Los {NB_TOP_DOMAIN} Mejores Dominios";
$lvm_top_domain_menu   = "Mejores Dominios";
$lvm_top_os            = "Los OS m�s Usados";
$lvm_top_os_menu       = "Mejor OS";
$lvm_top_referer       = "Las {NB_TOP_REFERER} Mejores Referencias";
$lvm_top_referer_menu  = "Mejores Referencias";
$lvm_top_visitors      = "Los Mejores {NB_TOP_VISITORS} Visitantes";
$lvm_top_visitors_menu = "Mejores Visitantes";

$lvm_uncheck_all       = "Deseleccionar todos";
$lvm_visitors_day      = "Visitantes / Dia";
$lvm_visitors_per_day  = "Visitantes / Dia - desde {NB_LAST_MONTHS} meses";
$lvm_year              = "A�o";
$lvm_year_per_day      = "Visitantes/Dia - Ultimos 12 meses";
$lvm_yearly_graph      = "Grafico Anual";
$lvm_yes               = "Si";
$lvm_12_months         = "Visitantes / Mes - Ultimos 12 meses";

$lvm_arr_months        = Array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
                                    "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

// without accents
$lvm_arr_months_graph  = Array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
                                    "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$lvm_arr_months_abbr   = Array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization
    return($date);
}

?>