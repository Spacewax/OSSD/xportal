<?php

// Norwegian translation by Audun Engesbak <ale@atmc.no>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Cache Moduler";
$lvm_adm_archiver      = "Arkiv";
$lvm_admin             = "Administrasjon";
$lvm_agent             = "Agent";
$lvm_archived          = "Lagret i arkive";
$lvm_archive_created   = "Arkiv opprettet !";
$lvm_archiver_check    = "Sjekk om arkivet er i databasen.";
$lvm_archiver_archive  = "Opprett eller oppdater tabell data '<B>{ARCHIVES_TABLE}</B>' for den valgte m�ned.";
$lvm_archiver_export   = "Ekporter tabell data '<B>{VISITORS_TABLE}</B>' til en tekst fil for den valgte m�ned.";
$lvm_archiver_delete   = "Slett <U><B>permanent</B></U> fra tabell '<B>{VISITORS_TABLE}</B>' data for den valgte m�ned.<BR>F�r du utf�rer denne operasjonen, sjekk at eksporten av m�neds data (steg 2) at arkivet er lagret i databasen.";

$lvm_average           = "Gj.s.";
$lvm_btn_archive       = "Arkiv";
$lvm_btn_check         = "Sjekk";
$lvm_btn_delete        = "Slett";
$lvm_btn_export        = "Eksporter";
$lvm_calendar          = "Kallender";
$lvm_check_archive     = "Sjekk arkivet";
$lvm_check_all         = "Sjekk alt";
$lvm_create_archive    = "Opprett arkiv";
$lvm_confirm_delete    = "Slette ?";
$lvm_connection_error  = "Database oppkobling feilet !";
$lvm_creation_date     = "Dato opprettet";
$lvm_daily_stats       = "Daglig statistikk";
$lvm_day_direct        = "Direkte treff";
$lvm_day_referers      = "Pr, refferent";
$lvm_day_visitors      = "Antall bes�kende";
$lvm_delay_days        = "d";
$lvm_delay_hours       = "h";
$lvm_delay_last        = "sist";
$lvm_delay_minutes     = "min";
$lvm_delay_seconds     = "s";
$lvm_delete            = "Slett";
$lvm_delete_ok         = "Slett !";
$lvm_delete_visitors   = "Slett tabellen {VISITORS_TABLE}";
$lvm_description       = "Beskrivelse";
$lvm_directory         = "Adresse liste";
$lvm_domain            = "Domene";
$lvm_error_nodata      = "Ingen data i tabellen {VISITORS_TABLE} for den valgte m�ned !";
$lvm_file              = "Lagre";
$lvm_go_back           = "Tilbake";
$lvm_header_title      = "Bes�kende for";
$lvm_host              = "Host";
$lvm_invalid_password  = "Feil passord";
$lvm_last_months       = "{NB_LAST_MONTHS} Forrige m�ned";
$lvm_last_visitors     = "{NB_LAST_VISITORS} Siste bes�kende";
$lvm_month             = "M�ned";
$lvm_no                = "Nr";
$lvm_no_archive        = "Ingen arkiv funnet for denne perioden";
$lvm_no_cache          = "Ingen cache modul funnet !";
$lvm_no_delete         = "Sletting kanselert";
$lvm_number            = "Nr";
$lvm_os                = "OS";
$lvm_others_os         = "Andre";
$lvm_others_agent      = "Andre";
$lvm_page_title        = "Bes�kende";
$lvm_password          = "Passord (base)";
$lvm_per_hour          = "Bes�kende/Time";
$lvm_per_day           = "Bes�kende/Dag";
$lvm_referer           = "Refferent";
$lvm_size              = "St�rrelse";
$lvm_time              = "Time";
$lvm_total             = "Totalt";
$lvm_today             = "I dag";

$lvm_title_admin         = "Administrasjon";
$lvm_title_last_months   = "{NB_LAST_MONTHS} Forrige m�ned";
$lvm_title_current_month = "M�nedlig statistikk";
$lvm_title_day           = "Daglig statsikk";
$lvm_title_month         = "M�nedlig statistikk";
$lvm_title_others        = "Andre statistikker";
$lvm_title_user          = "Utvidet statistikk";
$lvm_title_year          = "�rlig statistikk";

$lvm_top_agent         = "Internet lesere";
$lvm_top_agent_menu    = "Topp Internet lesere";
$lvm_top_agent_os      = "Topp {NB_TOP_AGENT_OS} av internet lesere/Operativsystem";
$lvm_top_agent_os_menu = "Topp Internet lesere/Operativsystem";
$lvm_top_domain        = "Topp {NB_TOP_DOMAIN} av domener";
$lvm_top_domain_menu   = "Topp domener";
$lvm_top_os            = "Operativsystem";
$lvm_top_os_menu       = "Topp operativsystem";
$lvm_top_referer       = "Topp {NB_TOP_REFERER} av refferenter";
$lvm_top_referer_menu  = "Topp refferenter";
$lvm_top_visitors      = "Topp {NB_TOP_VISITORS} av bes�kende";
$lvm_top_visitors_menu = "Topp bes�kende";

$lvm_uncheck_all       = "Fjern alle merker";
$lvm_visitors_day      = "Bes�kende / Dag";
$lvm_visitors_per_day  = "Bes�kende / Dag - Pr. {NB_LAST_MONTHS} m�ned";
$lvm_year              = "�r";
$lvm_year_per_day      = "Bes�kende/Dag - Pr. 12 m�neder";
$lvm_yearly_graph      = "�rlig Grafikk";
$lvm_yes               = "Ja";
$lvm_12_months         = "Bes�kende / M�ned - Pr. 12 m�neder";

$lvm_arr_months        = Array(1 => "Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", 
                                    "August", "September", "Oktober", "November", "Desember");

// without accents
$lvm_arr_months_graph  = Array(1 => "Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", 
                                    "August", "September", "Oktober", "November", "Desember");

$lvm_arr_months_abbr   = Array(1 => "Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization

    return($date);
}
?>