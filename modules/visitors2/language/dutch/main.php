<?php

// Translation [english -> dutch] by Sander Scheer <scheer@globalminds.nl>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Cache Modules";
$lvm_adm_archiver      = "Archief";
$lvm_admin             = "Administratie";
$lvm_agent             = "Agent";
$lvm_archived          = "Bewaard in de archieven";
$lvm_archive_created   = "Archief succesvol aangemaakt !";
$lvm_archiver_check    = "Controleert of een archief al dan niet in de database voorkomt.";
$lvm_archiver_archive  = "Maken of wijzigen van de data in table '<B>{ARCHIVES_TABLE}</B>' voor de geselecteerde maand.";
$lvm_archiver_export   = "Exporteer de data van de geselecteerde maand uit tabel '<B>{VISITORS_TABLE}</B>' naar een tekstbestand.";
$lvm_archiver_delete   = "Verwijder <U><B>voorgoed</B></U> uit tabel '<B>{VISITORS_TABLE}</B>' de records van de geselecteerde maand.<BR>Voordat u deze operatie uitvoert, controleer de export van de data (stap 2) en de aanwezigheid hiervan in het archief.";

$lvm_average           = "Gem.";
$lvm_btn_archive       = "Archief";
$lvm_btn_check         = "Controleer";
$lvm_btn_delete        = "Verwijder";
$lvm_btn_export        = "Exporteer";
$lvm_calendar          = "Calender";
$lvm_check_archive     = "Controleer het archief";
$lvm_check_all         = "Controleer alles";
$lvm_create_archive    = "Maak een archief";
$lvm_confirm_delete    = "Verwijderen ?";
$lvm_connection_error  = "Database verbindingsfout !";
$lvm_creation_date     = "Creatie-datum";
$lvm_daily_stats       = "Dagelijkse statistieken";
$lvm_day_direct        = "Directe toegang";
$lvm_day_referers      = "Op referer";
$lvm_day_visitors      = "Aantal bezoekers";
$lvm_delay_days        = "d";
$lvm_delay_hours       = "u";
$lvm_delay_last        = "upd";
$lvm_delay_minutes     = "mn";
$lvm_delay_seconds     = "s";
$lvm_delete            = "Verwijder";
$lvm_delete_ok         = "Verwijderd !";
$lvm_delete_visitors   = "Wilt u tabel {VISITORS_TABLE} verwijderen ?";
$lvm_description       = "Omschrijving";
$lvm_directory         = "Adressen lijst";
$lvm_domain            = "Domein";
$lvm_error_nodata      = "Geen data in de tabel {VISITORS_TABLE} voor de geselecteerde maand !";
$lvm_file              = "Bestand";
$lvm_go_back           = "Terug";
$lvm_header_title      = "De bezoekers van";
$lvm_host              = "Host";
$lvm_invalid_password  = "Wachtwoord onjuist";
$lvm_last_months       = "{NB_LAST_MONTHS} Laatste maanden";
$lvm_last_visitors     = "{NB_LAST_VISITORS} Laatste bezoekers";
$lvm_month             = "Maand";
$lvm_no                = "Nee";
$lvm_no_archive        = "Geen archieven gevonden voor de geselecteerde periode !";
$lvm_no_cache          = "Geen cache module gevonden !";
$lvm_no_delete         = "Verwijderen afgebroken !";
$lvm_number            = "Nr";
$lvm_os                = "OS";
$lvm_others_os         = "Andere";
$lvm_others_agent      = "Andere";
$lvm_page_title        = "De bezoekers";
$lvm_password          = "Wachtwoord (basis)";
$lvm_per_hour          = "Bezoekers/Uur";
$lvm_per_day           = "Bezoekers/Dag";
$lvm_referer           = "Referer";
$lvm_size              = "Grootte";
$lvm_time              = "Uur";
$lvm_total             = "Totaal";
$lvm_today             = "Vandaag";

$lvm_title_admin         = "Administratie";
$lvm_title_last_months   = "{NB_LAST_MONTHS} Afgelopen maanden";
$lvm_title_current_month = "Statistieken van deze maand";
$lvm_title_day           = "Dagelijkse Statistieken";
$lvm_title_month         = "Maandelijke Statistieken";
$lvm_title_others        = "Overige Statistieken";
$lvm_title_user          = "Gepersonalizeerde Statistieken";
$lvm_title_year          = "Jaarlijkse Statistieken";

$lvm_top_agent         = "De Browsers";
$lvm_top_agent_menu    = "Top Browsers";
$lvm_top_agent_os      = "Het Top {NB_TOP_AGENT_OS} van Browsers/OS";
$lvm_top_agent_os_menu = "Top Browsers/OS";
$lvm_top_domain        = "De Top {NB_TOP_DOMAIN} van de Domeinen";
$lvm_top_domain_menu   = "Top Domeinen";
$lvm_top_os            = "Het OS";
$lvm_top_os_menu       = "Top OS";
$lvm_top_referer       = "De Top {NB_TOP_REFERER} Referers";
$lvm_top_referer_menu  = "Top Referers";
$lvm_top_visitors      = "De Top {NB_TOP_VISITORS} Bezoekers";
$lvm_top_visitors_menu = "Top Bezoekers";

$lvm_uncheck_all       = "Alles de-selecteren";
$lvm_visitors_day      = "Bezoekers / Dag";
$lvm_visitors_per_day  = "Bezoekers / Dag - Per {NB_LAST_MONTHS} maanden";
$lvm_year              = "Jaar";
$lvm_year_per_day      = "Bezoekers / Dag - Per 12 maanden";
$lvm_yearly_graph      = "Jaarlijkse grafiek";
$lvm_yes               = "Ja";
$lvm_12_months         = "Bezoekers / Maand - Per 12 maanden";

$lvm_arr_months        = Array(1 => "Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", 
                                    "Augustus", "September", "Oktober", "November", "December");

// without accents
$lvm_arr_months_graph  = Array(1 => "Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", 
                                    "Augustus", "September", "Oktober", "November", "December");

$lvm_arr_months_abbr   = Array(1 => "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization

    return($date);
}

?>