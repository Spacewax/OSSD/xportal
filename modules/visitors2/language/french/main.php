<?php

// Translation [french -> xxxxx] by xxxx xxxxx <email@email.com>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Modules de Cache";
$lvm_adm_archiver      = "Archivage";
$lvm_admin             = "Administration";
$lvm_agent             = "Agent";
$lvm_archived          = "Archiv�";
$lvm_archive_created   = "Archive cr��e avec succ�s !";
$lvm_archiver_check    = "V�rifie la pr�sence ou non de l'archive dans la base.";
$lvm_archiver_archive  = "Cr�e ou met � jour l'entr�e dans la table '<B>{ARCHIVES_TABLE}</B>' pour le mois s�lectionn�.";
$lvm_archiver_export   = "Exporte dans un fichier texte les entr�es de la table '<B>{VISITORS_TABLE}</B>' du mois s�lectionn�.";
$lvm_archiver_delete   = "Supprime <U><B>d�finitivement</B></U> de la table '<B>{VISITORS_TABLE}</B>' les enregistrements du mois s�lectionn�.<BR>Avant de lancer cette op�ration, v�rifiez l'export des donn�es du mois (�tape 2) et la pr�sence de l'archive dans la base.";
$lvm_average           = "Moy.";
$lvm_btn_archive       = "Archiver";
$lvm_btn_check         = "V�rifier";
$lvm_btn_delete        = "Purger";
$lvm_btn_export        = "Exporter";
$lvm_calendar          = "Calendrier";
$lvm_check_archive     = "V�rifier l'archive";
$lvm_check_all         = "Tout cocher";
$lvm_create_archive    = "Cr�er l'archive";
$lvm_confirm_delete    = "Confirmez-vous la suppression ?";
$lvm_connection_error  = "La connexion � la base a �chou� !";
$lvm_creation_date     = "Date de cr�ation";
$lvm_daily_stats       = "Les Chiffres du Jour";
$lvm_day_direct        = "En Acc�s Direct";
$lvm_day_referers      = "Par R�f�rants";
$lvm_day_visitors      = "Nombre de Visiteurs";
$lvm_delay_days        = "j";
$lvm_delay_hours       = "h";
$lvm_delay_last        = "maj";
$lvm_delay_minutes     = "mn";
$lvm_delay_seconds     = "s";
$lvm_delete            = "Supprimer";
$lvm_delete_ok         = "Suppression effectu�e !";
$lvm_delete_visitors   = "Purger la table {VISITORS_TABLE}";
$lvm_description       = "Description";
$lvm_directory         = "Annuaire";
$lvm_domain            = "Domaine";
$lvm_error_nodata      = "Pas de donn�es dans la table {VISITORS_TABLE} pour le mois s�lectionn� !";
$lvm_file              = "Fichier";
$lvm_go_back           = "Retour";
$lvm_header_title      = "Les Visiteurs de";
$lvm_host              = "H�te";
$lvm_invalid_password  = "Mot de passe incorrect";
$lvm_last_months       = "{NB_LAST_MONTHS} derniers Mois";
$lvm_last_visitors     = "Les {NB_LAST_VISITORS} Derniers Visiteurs";
$lvm_month             = "Mois";
$lvm_no                = "Non";
$lvm_no_archive        = "Aucune archive trouv�e pour cette p�riode";
$lvm_no_cache          = "Aucun module de cache trouv� !";
$lvm_no_delete         = "Suppression annul�e";
$lvm_number            = "Nb";
$lvm_os                = "OS";
$lvm_others_os         = "Autres";
$lvm_others_agent      = "Autres";
$lvm_page_title        = "Les Visiteurs";
$lvm_password          = "Mot de passe (base)";
$lvm_per_hour          = "Visiteurs/Heure";
$lvm_per_day           = "Visiteurs/Jour";
$lvm_referer           = "R�f�rant";
$lvm_size              = "Taille";
$lvm_time              = "Heure";
$lvm_total             = "Total";
$lvm_today             = "Aujourd'hui";

$lvm_title_admin         = "Administration";
$lvm_title_last_months   = "{NB_LAST_MONTHS} Derniers Mois";
$lvm_title_current_month = "Statistiques du Mois";
$lvm_title_day           = "Statistiques Quotidiennes";
$lvm_title_month         = "Statistiques Mensuelles";
$lvm_title_others        = "Autres Statistiques";
$lvm_title_user          = "Statistiques Personnalis�es";
$lvm_title_year          = "Statistiques Annuelles";

$lvm_top_agent         = "Les Navigateurs";
$lvm_top_agent_menu    = "Top Navigateurs";
$lvm_top_agent_os      = "Le Top {NB_TOP_AGENT_OS} des Navigateurs/OS";
$lvm_top_agent_os_menu = "Top Navigateurs/OS";
$lvm_top_domain        = "Le Top {NB_TOP_DOMAIN} des Domaines";
$lvm_top_domain_menu   = "Top Domaines";
$lvm_top_os            = "Les OS";
$lvm_top_os_menu       = "Top OS";
$lvm_top_referer       = "Le Top {NB_TOP_REFERER} des R�f�rants";
$lvm_top_referer_menu  = "Top R�f�rants";
$lvm_top_visitors      = "Le Top {NB_TOP_VISITORS} des Visiteurs";
$lvm_top_visitors_menu = "Top Visiteurs";

$lvm_uncheck_all       = "Tout d�cocher";
$lvm_visitors_day      = "Visiteurs / Jour";
$lvm_visitors_per_day  = "Visiteurs / Jour - Sur {NB_LAST_MONTHS} mois";
$lvm_year              = "Ann�e";
$lvm_year_per_day      = "Visiteurs/Jour - Sur 12 mois";
$lvm_yearly_graph      = "Graphique Annuel";
$lvm_yes               = "Oui";
$lvm_12_months         = "Visiteurs / Mois - Sur 12 mois";

$lvm_arr_months        = Array(1 => "Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", 
                                    "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre");

// without accents
$lvm_arr_months_graph  = Array(1 => "Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", 
                                    "Aout", "Septembre", "Octobre", "Novembre", "Decembre");

$lvm_arr_months_abbr   = Array(1 => "Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jui", "Aou", "Sep", "Oct", "Nov", "Dec");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization

    return($date);
}

?>