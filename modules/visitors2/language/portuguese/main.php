<?php

// Portuguese translation by JP DE FREITAS <jpdefreitas@free.fr>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Cache Modules ";
$lvm_adm_archiver      = "Arquivos";
$lvm_admin             = "Administra��o";
$lvm_agent             = "Agente";
$lvm_archived          = "Arquivado";
$lvm_archive_created   = "Arquivo gravado com sucesso !";
$lvm_archiver_check    = "Verifica se o arquivo ja est� presente na base de dados.";
$lvm_archiver_archive  = "Cria ou actualiza a entrada da tabela '<B>{ARCHIVES_TABLE}</B>' relativa ao m�s selecionado.";
$lvm_archiver_export   = "Exporte num ficheiro texto as entradas da tabela  '<B>{VISITORS_TABLE}</B>' relativas ao m�s selecionado.";
$lvm_archiver_delete   = "Apaga <U><B>definitivamente</B></U> da tabela '<B>{VISITORS_TABLE}</B>' todos os dados relativos ao m�s seleccionado.<BR>Antes de efectuar esta opera��o, Verifique a exporta��o dos dados do mes (etapa 2) e a presen�a do arquivo na base de dados.";
$lvm_average           = "Med.";
$lvm_btn_archive       = "Arquivar";
$lvm_btn_check         = "Verificar";
$lvm_btn_delete        = "Apagar";
$lvm_btn_export        = "Exportar";
$lvm_calendar          = "Calend�rio";
$lvm_check_archive     = "Verificar o arquivo";
$lvm_check_all         = "Seleccionar tudo";
$lvm_create_archive    = "Criar o arquivo";
$lvm_confirm_delete    = "Deseja realmente apagar ?";
$lvm_connection_error  = "A liga��o com a base de dados falhou !";
$lvm_creation_date     = "Data de cria��o";
$lvm_daily_stats       = "Os numeros do Dia";
$lvm_day_direct        = "Com Acesso Directo";
$lvm_day_referers      = "Por Referentes";
$lvm_day_visitors      = "Numero de visitantes";
$lvm_delay_days        = "j";
$lvm_delay_hours       = "h";
$lvm_delay_last        = "maj";
$lvm_delay_minutes     = "mn";
$lvm_delay_seconds     = "s";
$lvm_delete            = "Apagar";
$lvm_delete_ok         = "Dados apagados !";
$lvm_delete_visitors   = "Purgar a tabela {VISITORS_TABLE}";
$lvm_description       = "Descri��o";
$lvm_directory         = "Annu�rio";
$lvm_domain            = "Dom�nio";
$lvm_error_nodata      = "N�o h� dados na tabela {VISITORS_TABLE} para os m�s selecionado !";
$lvm_file              = "Ficheiro";
$lvm_go_back           = "Voltar";
$lvm_header_title      = "Os Visitantes de";
$lvm_host              = "Host";
$lvm_invalid_password  = "Palavra passe  errada";
$lvm_last_months       = "{NB_LAST_MONTHS} �ltimos meses";
$lvm_last_visitors     = "Les {NB_LAST_VISITORS} �ltimos visitantes";
$lvm_month             = "M�s";
$lvm_no                = "N�o";
$lvm_no_archive        = "Nenhum arquivo encontrado para esse periodo";
$lvm_no_cache          = "Aucun module de cache trouv� !";
$lvm_no_delete         = "Suppression annul�e";
$lvm_number            = "Nb";
$lvm_os                = "SO (sistema operativo)";
$lvm_others_os         = "Outros";
$lvm_others_agent      = "Outros";
$lvm_page_title        = "Os Visitantes";
$lvm_password          = "Palavra passe (base)";
$lvm_per_hour          = "Visitantes/Hora";
$lvm_per_day           = "Visitantes/Dia";
$lvm_referer           = "Referente";
$lvm_size              = "Tamanho";
$lvm_time              = "Hora";
$lvm_total             = "Total";
$lvm_today             = "Hoje";

$lvm_title_admin         = "Administra��o";
$lvm_title_last_months   = "{NB_LAST_MONTHS} �ltimos meses ";
$lvm_title_current_month = "Stat�sticas do M�s";
$lvm_title_day           = "Stat�sticas Di�rias";
$lvm_title_month         = "Stat�sticas Mensais";
$lvm_title_others        = "Outras Stat�sticas";
$lvm_title_user          = "Stat�sticas Personalizadas";
$lvm_title_year          = "Stat�sticas Anu�is";

$lvm_top_agent         = "Os Browsers";
$lvm_top_agent_menu    = "Top Browsers";
$lvm_top_agent_os      = "O Top {NB_TOP_AGENT_OS} dos Browsers/SO";
$lvm_top_agent_os_menu = "Top Browsers/SO";
$lvm_top_domain        = "O Top {NB_TOP_DOMAIN} dos Dominios";
$lvm_top_domain_menu   = "Top Dominios";
$lvm_top_os            = "Os SO";
$lvm_top_os_menu       = "Top SO";
$lvm_top_referer       = "O Top {NB_TOP_REFERER} dos Referentes";
$lvm_top_referer_menu  = "Top Referentes";
$lvm_top_visitors      = "O Top {NB_TOP_VISITORS} dos Visitantes";
$lvm_top_visitors_menu = "Top Visitantes";

$lvm_uncheck_all       = "Selecionar tudo";
$lvm_visitors_day      = "Visitantes / Dia";
$lvm_visitors_per_day  = "Visitantes / Dia - sobre {NB_LAST_MONTHS} m�s";
$lvm_year              = "Ano";
$lvm_year_per_day      = "Visitantes/Dia - Sobre 12 m�s";
$lvm_yearly_graph      = "Gr�fico anual";
$lvm_yes               = "Sim";
$lvm_12_months         = "Visitantes / M�s - Sobre 12 m�s";

$lvm_arr_months        = Array(1 => "Janeiro", "Fevreiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", 
                                    "Agosto", "Setembro", "Outubro", "Novembro", "D�zembro");

// without accents
$lvm_arr_months_graph  = Array(1 => "Janeiro", "Fevrier", "Mar�o", "Abril", "Maio", "Junho", "Julho", 
                                    "Aout", "Setembro", "Outubro", "Novembro", "Dezembro");

$lvm_arr_months_abbr   = Array(1 => "Jan", "Fev", "Mar", "Abr", "Maioo", "Jun", "Jui", "Ago", "Set", "Out", "Nov", "Dez");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization

    return($date);
}

?>