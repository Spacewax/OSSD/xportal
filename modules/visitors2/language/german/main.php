<?php

// Translation [french -> german] by Michael SEIDLER <michael.seidler@kit-service.de>

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "Cache Module";
$lvm_adm_archiver      = "Archiviert";
$lvm_admin             = "Administration";
$lvm_agent             = "Agent";
$lvm_archived          = "im Archiv gespeichert";
$lvm_archive_created   = "Archiv erfolgreich erstellt!";
$lvm_archiver_check    = "�berpr�fe, ob Archiv in der Datenbank vorhanden ist.";
$lvm_archiver_archive  = "Erstelle oder aktualisiere die Datenbanktabellen'<B>{ARCHIVES_TABLE}</B>' f�r den ausgw&auml;hlten Monat.";
$lvm_archiver_export   = "Exportiere die Tabellendaten '<B>{VISITORS_TABLE}</B>' des ausgew&auml;hlten Monats in eine Testdatei.";
$lvm_archiver_delete   = "L&ouml;sche <U><B>entg&uuml;ltig</B></U> aus der Tabelle '<B>{VISITORS_TABLE}</B>' die Datens&auml;tze des ausgew&auml;hlten Monats. <BR>Bevor diese Aktion ausgef&uuml;hrt wird, &uuml;berpr&uuml;fe den Export der Monatsdaten (Schritt 2) und das Vorhandensein des Archives in der Datenbank.";

$lvm_average           = "durchschnittl.";
$lvm_btn_archive       = "Archiv";
$lvm_btn_check         = "&Uuml;berpr&uuml;fe";
$lvm_btn_delete        = "S&auml;ubern";
$lvm_btn_export        = "Exportieren";
$lvm_calendar          = "Kalender";
$lvm_check_archive     = "&Uuml;berpr&uuml;fe das Archiv";
$lvm_check_all         = "&Uuml;berpr&uuml;fe alles";
$lvm_create_archive    = "Erstelle Archiv";
$lvm_confirm_delete    = "L&ouml;schen ?";
$lvm_connection_error  = "keine Verbindng zur Datenbank !";
$lvm_creation_date     = "Erstellungsdatum";
$lvm_daily_stats       = "Anzahl der Tage";
$lvm_day_direct        = "Direktzugriffe";
$lvm_day_referers      = "&uuml;ber einen Link";
$lvm_day_visitors      = "Anzahl der Besucher";
$lvm_delay_days        = "T";
$lvm_delay_hours       = "St";
$lvm_delay_last        = "upd";
$lvm_delay_minutes     = "min";
$lvm_delay_seconds     = "s";
$lvm_delete            = "L&ouml;schen";
$lvm_delete_ok         = "Gel&ouml;scht !";
$lvm_delete_visitors   = "Leeren der Tabelle {VISITORS_TABLE}";
$lvm_description       = "Beschreibung";
$lvm_directory         = "Addressenliste";
$lvm_domain            = "Domain";
$lvm_error_nodata      = "Kein Daten in der Tabelle {VISITORS_TABLE} f&uuml;r den gew&auml;hlten Monat!";
$lvm_file              = "Datei";
$lvm_go_back           = "Zur&uuml;ck";
$lvm_header_title      = "Die Besucher von";
$lvm_host              = "Host";
$lvm_invalid_password  = "Mot de passe incorrect";
$lvm_last_months       = "{NB_LAST_MONTHS} letzten Monaten";
$lvm_last_visitors     = "{NB_LAST_VISITORS} letzten Besucher";
$lvm_month             = "Monat";
$lvm_no                = "Nein";
$lvm_no_archive        = "Kein Archiv f&uuml;r diese Zeit gefunden";
$lvm_no_cache          = "Kein Cachemodul gefunden!";
$lvm_no_delete         = "L&ouml;schvorgang abgebrochen";
$lvm_number            = "Anz.";
$lvm_os                = "OS";
$lvm_others_os         = "Andere";
$lvm_others_agent      = "Andere";
$lvm_page_title        = "Die Besucher";
$lvm_password          = "Passwort (Datenbank)";
$lvm_per_hour          = "Besucher/Stunde";
$lvm_per_day           = "Besucher/Tag";
$lvm_referer           = "Referenz";
$lvm_size              = "Gr&ouml;&szlig;e";
$lvm_time              = "Stunde";
$lvm_total             = "Gesamt";
$lvm_today             = "Heute";

$lvm_title_admin         = "Administration";
$lvm_title_last_months   = "{NB_LAST_MONTHS} letzten Monaten";
$lvm_title_current_month = "Monatstatistik";
$lvm_title_day           = "Tagesstatistik";
$lvm_title_month         = "Monatliche Statistik";
$lvm_title_others        = "Andere Statistiken";
$lvm_title_user          = "Personalisierte Statistik";
$lvm_title_year          = "Jahresstatistik";

$lvm_top_agent         = "Die Browsers";
$lvm_top_agent_menu    = "Top Browsers";
$lvm_top_agent_os      = "Die Top {NB_TOP_AGENT_OS} der Browsers/OS";
$lvm_top_agent_os_menu = "Top Browsers/OS";
$lvm_top_domain        = "Die Top {NB_TOP_DOMAIN} der Domains";
$lvm_top_domain_menu   = "Top Domains";
$lvm_top_os            = "Die OS";
$lvm_top_os_menu       = "Top OS";
$lvm_top_referer       = "Die Top {NB_TOP_REFERER} der Referenzen";
$lvm_top_referer_menu  = "Top Referenzen";
$lvm_top_visitors      = "Die Top {NB_TOP_VISITORS} der Besucher";
$lvm_top_visitors_menu = "Top Besucher";

$lvm_uncheck_all       = "Nicht &uuml;berpr&uuml;fen";
$lvm_visitors_day      = "Besucher / Tag";
$lvm_visitors_per_day  = "Besucher / Tag - in {NB_LAST_MONTHS} Monaten";
$lvm_year              = "Jahr";
$lvm_year_per_day      = "Besucher/Tag - in 12 Monaten";
$lvm_yearly_graph      = "Jahresgrafik";
$lvm_yes               = "JA";
$lvm_12_months         = "Besucher / Monat - in 12 Monaten";

$lvm_arr_months        = Array(1 => "Januar", "Februar", "M&auml;rz", "April", "Mai", "Juni", "Juli", 
                                    "August", "September", "Oktober", "November", "Dezember");

// without accents
$lvm_arr_months_graph  = Array(1 => "Januar", "Februar", "M&auml;rz", "April", "Mai", "Juni", "Juli", 
                                    "August", "September", "Oktober", "November", "Dezember");

$lvm_arr_months_abbr   = Array(1 => "Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization

    return($date);
}

?>