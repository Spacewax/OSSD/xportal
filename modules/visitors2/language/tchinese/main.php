<?php

// The Chinese Big5 by Jacky Kit <email@jackykit.com>
// http://linux.3jk.com , http://jkzone.net , http://3jk.com , http://www.jackykit.com

// For translations, DO NOT delete or rename variables {VARIABLE_NAME}
// included in messages.

$lvm_adm_caches        = "存儲模塊";
$lvm_adm_archiver      = "檔案";
$lvm_admin             = "管理";
$lvm_agent             = "系統信息";
$lvm_archived          = "已儲存到檔案";
$lvm_archive_created   = "檔案創建成功!";
$lvm_archiver_check    = "檢查數據庫中是否有檔案.";
$lvm_archiver_archive  = "在選擇的月份中創建或更新數據表 '<B>{ARCHIVES_TABLE}</B>' .";
$lvm_archiver_export   = "輸出所選擇月份的數據表 '<B>{VISITORS_TABLE}</B>' 到一個文本文件.";
$lvm_archiver_delete   = "<U><B>確定</B></U> 刪除所選擇月份的數據表 '<B>{VISITORS_TABLE}</B>' 的記錄.<BR>在執行此操作前, 先檢查輸出月份的數據(第 2 步)和數據庫中存在的檔案.";

$lvm_average           = "平均";
$lvm_btn_archive       = "檔案";
$lvm_btn_check         = "檢查";
$lvm_btn_delete        = "清除";
$lvm_btn_export        = "輸出";
$lvm_calendar          = "日歷";
$lvm_check_archive     = "檢查檔案";
$lvm_check_all         = "檢查所有內容";
$lvm_create_archive    = "創建檔案";
$lvm_confirm_delete    = "刪除?";
$lvm_connection_error  = "數據庫連接失敗!";
$lvm_creation_date     = "產生日期";
$lvm_daily_stats       = "今日統計";
$lvm_day_direct        = "直接進入";
$lvm_day_referers      = "通過頁面";
$lvm_day_visitors      = "訪問數";
$lvm_delay_days        = "d";
$lvm_delay_hours       = "h";
$lvm_delay_last        = "upd";
$lvm_delay_minutes     = "mn";
$lvm_delay_seconds     = "s";
$lvm_delete            = "刪除";
$lvm_delete_ok         = "已刪除!";
$lvm_delete_visitors   = "清除表 {VISITORS_TABLE}";
$lvm_description       = "描述";
$lvm_directory         = "地址列表";
$lvm_domain            = "區域";
$lvm_error_nodata      = "在表 {VISITORS_TABLE} 中沒有所選擇月份的數據!";
$lvm_file              = "文件";
$lvm_go_back           = "返回";
$lvm_header_title      = "訪問統計 - ";
$lvm_host              = "訪問者";
$lvm_invalid_password  = "過時的不正確的語句";
$lvm_last_months       = "最後 {NB_LAST_MONTHS} 個月";
$lvm_last_visitors     = "最後 {NB_LAST_VISITORS} 位訪問者";
$lvm_month             = "月份";
$lvm_no                = "無";
$lvm_no_archive        = "未找到此時期的檔案";
$lvm_no_cache          = "未找到存儲模塊!";
$lvm_no_delete         = "已取消刪除";
$lvm_number            = "總數";
$lvm_os                = "系統";
$lvm_others_os         = "其他";
$lvm_others_agent      = "其他";
$lvm_page_title        = "訪問統計";
$lvm_password          = "密碼 (數據庫)";
$lvm_per_hour          = "訪問統計 / 小時";
$lvm_per_day           = "訪問統計 / 天";
$lvm_referer           = "頁面";
$lvm_size              = "大小";
$lvm_time              = "時間";
$lvm_total             = "總數";
$lvm_today             = "今天";

$lvm_title_admin         = "管理";
$lvm_title_last_months   = "最後 {NB_LAST_MONTHS} 個月";
$lvm_title_current_month = "月統計";
$lvm_title_day           = "日統計";
$lvm_title_month         = "每月統計";
$lvm_title_others        = "其他統計";
$lvm_title_user          = "個別統計";
$lvm_title_year          = "年統計";

$lvm_top_agent         = "瀏覽器";
$lvm_top_agent_menu    = "瀏覽器排行";
$lvm_top_agent_os      = "瀏覽器/系統 Top {NB_TOP_AGENT_OS}";
$lvm_top_agent_os_menu = "瀏覽器/系統排行";
$lvm_top_domain        = "區域 Top {NB_TOP_DOMAIN}";
$lvm_top_domain_menu   = "區域排行";
$lvm_top_os            = "系統";
$lvm_top_os_menu       = "系統排行";
$lvm_top_referer       = "頁面 Top {NB_TOP_REFERER}";
$lvm_top_referer_menu  = "頁面排行";
$lvm_top_visitors      = "訪問者 Top {NB_TOP_VISITORS}";
$lvm_top_visitors_menu = "訪問者排行";

$lvm_uncheck_all       = "任何未檢驗的";
$lvm_visitors_day      = "訪問統計 / 天";
$lvm_visitors_per_day  = "訪問統計 / 天 - 每 {NB_LAST_MONTHS} 個月";
$lvm_year              = "年";
$lvm_year_per_day      = "訪問統計 / 天 - 每12個月";
$lvm_yearly_graph      = "年統計表";
$lvm_yes               = "是";
$lvm_12_months         = "訪問統計 / 月 - 每12個月";

$lvm_arr_months        = Array(1 => "一　月", "二　月", "三　月", "四　月", "五　月", "六　月", "七　月", 
                                    "八　月", "九　月", "十　月", "十一月", "十二月");

// without accents
$lvm_arr_months_graph  = Array(1 => "一　月", "二　月", "三　月", "四　月", "五　月", "六　月", "七　月", 
                                    "八　月", "九　月", "十　月", "十一月", "十二月");

$lvm_arr_months_abbr   = Array(1 => "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

function show_datetime($datetime)
{   
    // for caches delays
    // ------------------------
    // from yyyy-mm-dd hh:mn:ss
    // to      mm-yyyy hh:mn:ss

    //$date  =  substr($datetime, 5, 2). '/'; // month
    //$date .=  substr($datetime, 8, 2). ' '; // day
    //$date .= substr($datetime, 11, 5);      // time

//XOOPS customization
	$time = mktime(substr($datetime,11,2),substr($datetime,14,2),0,substr($datetime,5,2),substr($datetime,8,2),substr($datetime,0,4));
	$time = formatTimestamp($time);
//end of XOOPS customization

    return($date);
}

?>