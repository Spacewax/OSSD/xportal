<?php
include_once("../../mainfile.php");
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");
if($xoopsUser){
	$xoopsModule = XoopsModule::getByDirname("visitors2");
	if ( !$xoopsUser->is_admin($xoopsModule->mid()) ) { 
		redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
		exit();
	}
} else {
	redirect_header($xoopsConfig['xoops_url']."/",3,_NOPERM);
	exit();
}
include($xoopsConfig['root_path']."header.php");
$xoopsModule->printAdminMenu();
echo "<br />";
?>