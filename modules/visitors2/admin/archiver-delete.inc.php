<script language="javascript">
<!--
    function user_delete_choice(yesno) {
        document.forms.confirmForm.choice.value = yesno;
    }
//-->
</script>

<?php

echo "<tr><th class='vis' colspan='3'>&nbsp;".str_replace('{VISITORS_TABLE}', $lvc_table_visitors, $lvm_delete_visitors)."&nbsp;</th></tr>
<tr><td class='visref' colspan='3' align='center'>&nbsp;";

if ( $action == 'delete' && $connection && $confirm != 'yes' ) {
	echo "<div style='text-align: center;'><form method='post' name='confirmform'>
	<a class='error'>$lvm_confirm_delete</a>&nbsp;&nbsp;&nbsp;<input type='submit' value='".htmlspecialchars($lvm_yes)."' onclick='javascript:user_delete_choice(1)' />
	&nbsp;&nbsp;&nbsp;<input type='submit' value='".htmlspecialchars($lvm_no)."'  onclick='javascript:user_delete_choice(0)' />
	<input type='hidden' name='action' value='delete' />
	<input type='hidden' name='month' value='$month' />
	<input type='hidden' name='year' value='$year' />
	<input type='hidden' name='password' value='$password' />
	<input type='hidden' name='confirm' value='yes' />
	<input type='hidden' name='choice' value='0' />
	</form></div>\n";
} elseif ( $action == 'delete' && $connection && $confirm == 'yes' ) {
	if ( $choice == 1 ) {
        $query  = 'DELETE FROM '.$lvc_table_visitors.' ';
        $query .= "WHERE DATE LIKE '".$year.'/'.sprintf('%02d', $month)."/%'";
        
        $gDb->DbQuery($query);

        echo "<div style='text-align:center;'><a class='ok'>$lvm_delete_ok</a></div>\n";
    } else {
        echo "<div style='text-align: center;'><a class='ok'>&nbsp;&nbsp;&gt;&gt; ".$lvm_no_delete." &lt;&lt;&nbsp;&nbsp;</a></div>\n";
    }
}

echo "&nbsp;</td></tr>\n";

?>