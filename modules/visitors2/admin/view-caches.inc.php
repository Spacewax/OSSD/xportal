<script tyle='text/javascript' language="javascript">
<!--
function check_all(name, state)
{
    len = document.forms[0].elements.length;
    for (i = 0; i < len; i++) {
	  if (document.forms[0].elements[i].name == name)
        document.forms[0].elements[i].checked = (state == 1);
    }
}
//-->
</script>

<?php

// ----------------------------------------------------------------------------
// ADMIN : caches
// ----------------------------------------------------------------------------
function sort_files($a, $b) {
    if ( $a[0] == $b[0] ) {
		return 0;
	}
    return ($a[0] > $b[0]) ? -1 : 1;  
}

// deleting selected files
if ( $action == 'delete' ) {
    if ( $password == $lvc_db_password ) {
        for ( $cnt = 0; $cnt < sizeof($cache_name); $cnt++ ) {
            @unlink($lvc_cache_dir.'/'.$cache_name[$cnt]);
        }
    } else {
        echo '<br />'.display_error($lvm_invalid_password);
    }
}

echo "<br /><div style='text-align:center;'><table cellspacing='1' border='0'>
<form method='post'><input type='hidden' name='action' value='delete'>
<tr><th class='vis'>&nbsp;".$lvm_delete."&nbsp;</th>
<th class='vis'>&nbsp;".$lvm_file."&nbsp;</th>
<th class='vis'>&nbsp;".$lvm_creation_date."&nbsp;</th>
<th class='vis'>&nbsp;".$lvm_size."&nbsp;</th></tr>\n";

$dir = opendir($lvc_cache_dir);
while ( $file = readdir($dir) ) {
    if ( $file != "." && $file != ".." ) {
        $file_name = $lvc_cache_dir.'/'.$file;
        $arr_files[] = Array($file, ereg('^img', $file) ? ICON_GRAPH : ICON_ARRAY, date('Y-m-d H:i:s', filemtime($file_name)), filesize($file_name));
    }
}
closedir($dir);

if ( sizeof($arr_files) ) {
	usort($arr_files, 'sort_files');
}

$size = 0;

for ( $cnt = 0; $cnt < sizeof($arr_files); $cnt++ ) {
	echo "<tr><td class='vis' align='center'>&nbsp;<input type='checkbox' name='cache_name[]' value='".$arr_files[$cnt][0]."' />&nbsp;</td>
	<td class='vis'>&nbsp;".html_image($g_relative_path.'images/'.$arr_files[$cnt][1])."&nbsp;\n";
    echo $arr_files[$cnt][0]."&nbsp;</td>
	<td class='vis' align='center'>&nbsp;".show_datetime($arr_files[$cnt][2])."&nbsp;</td>
	<td class='vis' align='right'>&nbsp;".number_format($arr_files[$cnt][3], 0, '', ' ')."&nbsp;</td>
	</tr>\n";

    $size += $arr_files[$cnt][3];
}

$size /= 1024;

if ( $cnt == 0 ) {
    echo "<tr><td class='vis' align='center' colspan='4'>&nbsp;<br /><b>$lvm_no_cache</b><br />&nbsp;</td></tr>\n";
} else {
    echo "<tr><td class='vis' align='center' colspan='2'>&nbsp;<b>&middot;</b>&nbsp;<a href='javascript:check_all(\"cache_name[]\",1)'>".$lvm_check_all."</a>
	&nbsp;<b>&middot;</b>&nbsp;<a href='javascript:check_all(\"cache_name[]\",0)'>".$lvm_uncheck_all."</a>
	&nbsp;<b>&middot;</b>&nbsp;</td>
	<td class='vis' align='center' colspan='2'>&nbsp;<b>".($size != 0 ? number_format($size, 1, '', ' ').' Ko' : '')."</b>&nbsp;</td></tr>
	<tr><th bgcolor='#ffffaa' colspan='4' height='1' class='vis'>";
    echo html_image("images/nothing.gif");
    echo "</th></tr>
	<tr height='40'>
	<td class='visref' colspan='2' align='center'>&nbsp;<b>".$lvm_password."</b>&nbsp;&nbsp;<input type='password' name='password' value='".htmlspecialchars($password)."' />&nbsp;</td>
	<td class='visref' colspan='2' align='center'>&nbsp;<input type='submit' value='".$lvm_delete."' /></td></tr>\n";
}

echo "</table></div></form><br />";

?>