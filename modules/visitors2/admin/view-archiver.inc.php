<?php

// ----------------------------------------------------------------------------
// ADMIN : archiver
// ----------------------------------------------------------------------------

$gDb = new Db;

$connection = ( ($password == $lvc_db_password) && ($gDb->DbConnect($lvc_db_host, $lvc_db_user, $lvc_db_password, $lvc_db_database)) );

// EXPORT
if ( $action == 'export' ) {
	include($lvc_admin_dir.'/archiver-export.inc.php');
    exit;
}

if ( $action != '' && !$connection ) {
    echo '<br />'.display_error($lvm_connection_error);
}

?>

<script language="javascript">
<!--
    function user_action_choice(action_value) {
        document.forms.archiverForm.action.value = action_value;
    }
//-->
</script>

<?php

$current_month = date('n');

if ( !isset($month) ) {
	$month = (($current_month == 1) ? 12 : $current_month - 1);
}
if ( !isset($year) ) {
	$year  = (($current_month == 1) ? date('Y') - 1 : date('Y'));
}

// FORM
echo "<br /><div style='text-align: center;'<table cellspacing='1' cellpadding='3' border='0'><form method='post' name='archiverForm'>";

// action
echo "<input type='hidden' name='action'>
<tr><th class='vis' colspan='2'>&nbsp;".$lvm_month." / ".$lvm_year."&nbsp;</th>
<th class='vis'>&nbsp;".$lvm_password."&nbsp;</th></tr>
<tr><td class='visref' align='center' colspan='2' nowrap='nowrap'>&nbsp;
<select name='month'>\n";   // month
for ( $cnt = 1; $cnt <= 12; $cnt++ ) {
    echo "<option value='".sprintf('%02d', $cnt)."' ".($cnt == $month ? " selected='selected'" : '').">".$lvm_arr_months[$cnt]."</option>\n";
}
echo "</select>&nbsp;\n";

echo "&nbsp;<select name='year'>\n"; // year
for ( $cnt = (date('Y')-1); $cnt <= (date('Y')+10); $cnt++ ) {
	echo "<option".($cnt == $year ? " selected='selected'" : "").">".$cnt."</option>\n";
}
echo "</select></td>
<td class='visref' align='center'>&nbsp;<input type='password' name='password' value='".htmlspecialchars($password)."' />&nbsp;</td></tr>\n";

// actions
echo "<tr><td class='visref' align='center'>&nbsp;&nbsp;</td>
<td class='visref' align='center'>&nbsp;<input type='submit' value='".htmlspecialchars($lvm_btn_check)."' onclick='javascript:user_action_choice(\"check\")' />&nbsp;</td>
<td class='vis'>$lvm_archiver_check</td></tr>
<tr><td class='visref' align='center'>&nbsp;<b>1.</b>&nbsp;</td>
<td class='visref' align='center'>&nbsp;<input type='submit' value='".htmlspecialchars($lvm_btn_archive)."' onclick='javascript:user_action_choice(\"archive\")' />&nbsp;</td>
<td class='vis'>";
echo str_replace('{ARCHIVES_TABLE}', $lvc_table_archives, $lvm_archiver_archive);
echo "</td></tr>
<tr><td class='visref' align='center'>&nbsp;<b>2.</b>&nbsp;</td>
<td class='visref' align='center'>&nbsp;<input type='submit' value='".htmlspecialchars($lvm_btn_export)."' onclick='javascript:user_action_choice(\"export\")' />&nbsp;</td>
<td class='vis'>";
echo str_replace('{VISITORS_TABLE}', $lvc_table_visitors, $lvm_archiver_export);
echo "</td></tr>
<tr><td class='visref' align='center'>&nbsp;<b>3.</b>&nbsp;</td>
<td class='visref' align='center'>&nbsp;<input type='submit' value='".htmlspecialchars($lvm_btn_delete)."' onclick='javascript:user_action_choice(\"delete\")' />&nbsp;</td>
<td class='vis'>";
echo str_replace('{VISITORS_TABLE}', $lvc_table_visitors, $lvm_archiver_delete);
echo "</td></tr>
</form></table>\n";

// ----------------------------------------------------------------------------
if ( $action != '' && $connection ) {
    echo "<br /><table cellspacing='1' cellpadding='3' border='0'>\n";
    include($lvc_admin_dir.'/archiver-'.$action.'.inc.php');
    echo "</table>\n";
}

echo "</div>";

?>