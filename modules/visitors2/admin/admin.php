<?php

// ------------------------------------------------------------------------- //
// Les Visiteurs - Statistiques de fréquentation d'un site web               //
// ------------------------------------------------------------------------- //
// Visitors      - Web site statistics analysis program                      //
// ------------------------------------------------------------------------- //
// Copyright (C) 2000, 2001  J-Pierre DEZELUS <jpdezelus@phpinfo.net>        //
// ------------------------------------------------------------------------- //
//                   phpInfo.net <http://www.phpinfo.net/>                   //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //


// ------------------------------------------------------------------------ 
// configuration file
// ------------------------------------------------------------------------
$ignore_messages = false;

$g_relative_path = '../';
chdir($g_relative_path);

$lvc_include_dir = 'include/';
$lvc_config_file = 'config.inc.php';

//mpnhack
if ( $action=='export' ) {
	include("../../mainfile.php");
	include($lvc_include_dir.$lvc_config_file);


    // ------------------------------------------------------------------------
    // library file
    // ------------------------------------------------------------------------
    include($lvc_include_dir.'library.inc.php');

    $g_page = ADMIN_PAGE;
} //mpnhack

if ( $action != 'export' ) {  // no echo before export (header)
    //include($lvc_include_dir.'header.inc.php');

	//mpnhack start 
	//redefine inclusion path from modules

	include("admin_header.php");

	include($lvc_include_dir.$lvc_config_file);
    include($lvc_include_dir.'library.inc.php');
    $g_page = ADMIN_PAGE;

	OpenTable();
	//mpnhack end
    echo "<br /><div style='text-align:center;'><table cellpadding='2' cellspacing='1' border='0'><tr><th class='vis'><b>$lvm_title_admin</b></th></tr><tr><td class='vis' nowrap='nowrap' valign='top'>";
	echo link_module($lvc_admin_file.'?p=caches',   $lvm_adm_caches,   ICON_PUCE);
	echo link_module($lvc_admin_file.'?p=archiver', $lvm_adm_archiver, ICON_PUCE);
	echo html_hr();
	echo link_module($g_relative_path, $lvm_page_title, ICON_MIXED);
	echo "</td></tr></table></div>";

	echo html_hr();
	
	if ( !isset($p) || $p == '' ) {
		$p = 'caches';
	}
	$title = $lvm_title_admin.' - '.${'msg_adm_'.$p};
    echo "<div style='text-align:center;'><a class='viewtitle'>$title</a></div>";
}

if ( file_exists($lvc_admin_dir.'/view-'.$p.'.inc.php') ) {
	include($lvc_admin_dir.'/view-'.$p.'.inc.php');
}
    
if ( $action != 'export' ) {
	CloseTable();
	include("../../footer.php");
    // include($lvc_include_dir.'/footer.inc.php');
}

?>