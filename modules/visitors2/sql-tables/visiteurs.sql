CREATE TABLE mpn_v2_visitors (
   AGENT char(100),
   REFERER char(200),
   ADDR char(50) NOT NULL,
   DATE char(20),
   HOST char(100),
   CODE int(11) DEFAULT '0' NOT NULL auto_increment,
   REF_HOST char(100),
   PRIMARY KEY (CODE),
   KEY ADDR (ADDR)
);