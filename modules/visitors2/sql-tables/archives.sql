CREATE TABLE mpn_v2_archives (
   code int(11) DEFAULT '0' NOT NULL auto_increment,
   annee int(11) DEFAULT '0' NOT NULL,
   mois int(11) DEFAULT '0' NOT NULL,
   vpm int(11) DEFAULT '0' NOT NULL,
   topVis blob NOT NULL,
   topNavOS blob NOT NULL,
   topOS blob NOT NULL,
   topNav blob NOT NULL,
   vph blob NOT NULL,
   topRef blob NOT NULL,
   topDom blob NOT NULL,
   vpj blob NOT NULL,
   PRIMARY KEY (code),
   KEY annee (annee),
   KEY mois (mois)
);