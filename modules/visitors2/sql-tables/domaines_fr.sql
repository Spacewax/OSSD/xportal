CREATE TABLE mpn_v2_domaines (
   code int(11) DEFAULT '0' NOT NULL auto_increment,
   domaine char(20) NOT NULL,
   description char(50) NOT NULL,
   PRIMARY KEY (code)
);

INSERT INTO domaines VALUES( '1', 'ac', 'Ascension (ile de l'')');
INSERT INTO domaines VALUES( '2', 'ad', 'Andorre');
INSERT INTO domaines VALUES( '3', 'ae', 'Emirats  Arabes Unis');
INSERT INTO domaines VALUES( '4', 'af', 'Afghanistan');
INSERT INTO domaines VALUES( '5', 'ag', 'Antigua et Barbuda');
INSERT INTO domaines VALUES( '6', 'ai', 'Anguilla');
INSERT INTO domaines VALUES( '7', 'al', 'Albanie');
INSERT INTO domaines VALUES( '8', 'am', 'Arm�nie');
INSERT INTO domaines VALUES( '9', 'an', 'Antilles Neerlandaises');
INSERT INTO domaines VALUES( '10', 'ao', 'Angola');
INSERT INTO domaines VALUES( '11', 'aq', 'Antarctique');
INSERT INTO domaines VALUES( '12', 'ar', 'Argentine');
INSERT INTO domaines VALUES( '13', 'as', 'American Samoa');
INSERT INTO domaines VALUES( '14', 'au', 'Australie');
INSERT INTO domaines VALUES( '15', 'aw', 'Aruba');
INSERT INTO domaines VALUES( '16', 'az', 'Azerbaijan');
INSERT INTO domaines VALUES( '17', 'ba', 'Bosnie Herzegovine');
INSERT INTO domaines VALUES( '18', 'bb', 'Barbade');
INSERT INTO domaines VALUES( '19', 'bd', 'Bangladesh');
INSERT INTO domaines VALUES( '20', 'be', 'Belgique');
INSERT INTO domaines VALUES( '21', 'bf', 'Burkina Faso');
INSERT INTO domaines VALUES( '22', 'bg', 'Bulgarie');
INSERT INTO domaines VALUES( '23', 'bh', 'Bahrain');
INSERT INTO domaines VALUES( '24', 'bi', 'Burundi');
INSERT INTO domaines VALUES( '25', 'bj', 'Benin');
INSERT INTO domaines VALUES( '26', 'bm', 'Bermudes');
INSERT INTO domaines VALUES( '27', 'bn', 'Brunei Darussalam');
INSERT INTO domaines VALUES( '28', 'bo', 'Bolivie');
INSERT INTO domaines VALUES( '29', 'br', 'Br�sil');
INSERT INTO domaines VALUES( '30', 'bs', 'Bahamas');
INSERT INTO domaines VALUES( '31', 'bt', 'Bhoutan');
INSERT INTO domaines VALUES( '32', 'bv', 'Bouvet (ile)');
INSERT INTO domaines VALUES( '33', 'bw', 'Botswana');
INSERT INTO domaines VALUES( '34', 'by', 'Bi�lorussie');
INSERT INTO domaines VALUES( '35', 'bz', 'B�lize');
INSERT INTO domaines VALUES( '36', 'ca', 'Canada');
INSERT INTO domaines VALUES( '37', 'cc', 'Cocos (Keeling) iles');
INSERT INTO domaines VALUES( '38', 'cd', 'Congo, (R�publique d�mocratique du)');
INSERT INTO domaines VALUES( '39', 'cf', 'Centrafricaine (R�publique )');
INSERT INTO domaines VALUES( '40', 'cg', 'Congo');
INSERT INTO domaines VALUES( '41', 'ch', 'Suisse');
INSERT INTO domaines VALUES( '42', 'ci', 'Cote d''Ivoire');
INSERT INTO domaines VALUES( '43', 'ck', 'Cook (iles)');
INSERT INTO domaines VALUES( '44', 'cl', 'Chili');
INSERT INTO domaines VALUES( '45', 'cm', 'Cameroun');
INSERT INTO domaines VALUES( '46', 'cn', 'Chine');
INSERT INTO domaines VALUES( '47', 'co', 'Colombie');
INSERT INTO domaines VALUES( '48', 'cr', 'Costa Rica');
INSERT INTO domaines VALUES( '49', 'cu', 'Cuba');
INSERT INTO domaines VALUES( '50', 'cv', 'Cap Vert');
INSERT INTO domaines VALUES( '51', 'cx', 'Christmas (ile)');
INSERT INTO domaines VALUES( '52', 'cy', 'Chypre');
INSERT INTO domaines VALUES( '53', 'cz', 'Tch�que (R�publique)');
INSERT INTO domaines VALUES( '54', 'de', 'Allemagne');
INSERT INTO domaines VALUES( '55', 'dj', 'Djibouti');
INSERT INTO domaines VALUES( '56', 'dk', 'Danemark');
INSERT INTO domaines VALUES( '57', 'dm', 'Dominique');
INSERT INTO domaines VALUES( '58', 'do', 'Dominicaine (r�publique)');
INSERT INTO domaines VALUES( '59', 'dz', 'Alg�rie');
INSERT INTO domaines VALUES( '60', 'ec', 'Equateur');
INSERT INTO domaines VALUES( '61', 'ee', 'Estonie');
INSERT INTO domaines VALUES( '62', 'eg', 'Egypte');
INSERT INTO domaines VALUES( '63', 'eh', 'Sahara Occidental');
INSERT INTO domaines VALUES( '64', 'er', 'Erythr�e');
INSERT INTO domaines VALUES( '65', 'es', 'Espagne');
INSERT INTO domaines VALUES( '66', 'et', 'Ethiopie');
INSERT INTO domaines VALUES( '67', 'fi', 'Finlande');
INSERT INTO domaines VALUES( '68', 'fj', 'Fiji');
INSERT INTO domaines VALUES( '69', 'fk', 'Falkland (Malouines) iles');
INSERT INTO domaines VALUES( '70', 'fm', 'Micron�sie');
INSERT INTO domaines VALUES( '71', 'fo', 'Faroe (iles)');
INSERT INTO domaines VALUES( '72', 'fr', 'France');
INSERT INTO domaines VALUES( '73', 'ga', 'Gabon');
INSERT INTO domaines VALUES( '74', 'gd', 'Grenade');
INSERT INTO domaines VALUES( '75', 'ge', 'G�orgie');
INSERT INTO domaines VALUES( '76', 'gf', 'Guyane Fran�aise');
INSERT INTO domaines VALUES( '77', 'gg', 'Guernsey');
INSERT INTO domaines VALUES( '78', 'gh', 'Ghana');
INSERT INTO domaines VALUES( '79', 'gi', 'Gibraltar');
INSERT INTO domaines VALUES( '80', 'gl', 'Groenland');
INSERT INTO domaines VALUES( '81', 'gm', 'Gambie');
INSERT INTO domaines VALUES( '82', 'gn', 'Guin�e');
INSERT INTO domaines VALUES( '83', 'gp', 'Guadeloupe');
INSERT INTO domaines VALUES( '84', 'gq', 'Guin�e Equatoriale');
INSERT INTO domaines VALUES( '85', 'gr', 'Gr�ce');
INSERT INTO domaines VALUES( '86', 'gs', 'Georgie du sud et iles Sandwich du sud');
INSERT INTO domaines VALUES( '87', 'gt', 'Guatemala');
INSERT INTO domaines VALUES( '88', 'gu', 'Guam');
INSERT INTO domaines VALUES( '89', 'gw', 'Guin�e-Bissau');
INSERT INTO domaines VALUES( '90', 'gy', 'Guyana');
INSERT INTO domaines VALUES( '91', 'hk', 'Hong Kong');
INSERT INTO domaines VALUES( '92', 'hm', 'Heard et McDonald (iles)');
INSERT INTO domaines VALUES( '93', 'hn', 'Honduras');
INSERT INTO domaines VALUES( '94', 'hr', 'Croatie');
INSERT INTO domaines VALUES( '95', 'ht', 'Haiti');
INSERT INTO domaines VALUES( '96', 'hu', 'Hongrie');
INSERT INTO domaines VALUES( '97', 'id', 'Indon�sie');
INSERT INTO domaines VALUES( '98', 'ie', 'Irlande');
INSERT INTO domaines VALUES( '99', 'il', 'Isra�l');
INSERT INTO domaines VALUES( '100', 'im', 'Ile de Man');
INSERT INTO domaines VALUES( '101', 'in', 'Inde');
INSERT INTO domaines VALUES( '102', 'io', 'Territoire Britannique de l''Oc�an Indien');
INSERT INTO domaines VALUES( '103', 'iq', 'Iraq');
INSERT INTO domaines VALUES( '104', 'ir', 'Iran (R�publique Islamique d'')');
INSERT INTO domaines VALUES( '105', 'is', 'Islande');
INSERT INTO domaines VALUES( '106', 'it', 'Italie');
INSERT INTO domaines VALUES( '107', 'je', 'Jersey');
INSERT INTO domaines VALUES( '108', 'jm', 'Jama�que');
INSERT INTO domaines VALUES( '109', 'jo', 'Jordanie');
INSERT INTO domaines VALUES( '110', 'jp', 'Japon');
INSERT INTO domaines VALUES( '111', 'ke', 'Kenya');
INSERT INTO domaines VALUES( '112', 'kg', 'Kirgizstan');
INSERT INTO domaines VALUES( '113', 'kh', 'Cambodge');
INSERT INTO domaines VALUES( '114', 'ki', 'Kiribati');
INSERT INTO domaines VALUES( '115', 'km', 'Comores');
INSERT INTO domaines VALUES( '116', 'kn', 'Saint Kitts et Nevis');
INSERT INTO domaines VALUES( '117', 'kp', 'Cor�e du nord');
INSERT INTO domaines VALUES( '118', 'kr', 'Cor�e du sud');
INSERT INTO domaines VALUES( '119', 'kw', 'Kowe�t');
INSERT INTO domaines VALUES( '120', 'ky', 'Ca�manes (iles)');
INSERT INTO domaines VALUES( '121', 'kz', 'Kazakhstan');
INSERT INTO domaines VALUES( '122', 'la', 'Laos');
INSERT INTO domaines VALUES( '123', 'lb', 'Liban');
INSERT INTO domaines VALUES( '124', 'lc', 'Sainte Lucie');
INSERT INTO domaines VALUES( '125', 'li', 'Liechtenstein');
INSERT INTO domaines VALUES( '126', 'lk', 'Sri Lanka');
INSERT INTO domaines VALUES( '127', 'lr', 'Liberia');
INSERT INTO domaines VALUES( '128', 'ls', 'Lesotho');
INSERT INTO domaines VALUES( '129', 'lt', 'Lituanie');
INSERT INTO domaines VALUES( '130', 'lu', 'Luxembourg');
INSERT INTO domaines VALUES( '131', 'lv', 'Latvia');
INSERT INTO domaines VALUES( '132', 'ly', 'Libyan Arab Jamahiriya');
INSERT INTO domaines VALUES( '133', 'ma', 'Maroc');
INSERT INTO domaines VALUES( '134', 'mc', 'Monaco');
INSERT INTO domaines VALUES( '135', 'md', 'Moldavie');
INSERT INTO domaines VALUES( '136', 'mg', 'Madagascar');
INSERT INTO domaines VALUES( '137', 'mh', 'Marshall (iles)');
INSERT INTO domaines VALUES( '138', 'mk', 'Mac�doine');
INSERT INTO domaines VALUES( '139', 'ml', 'Mali');
INSERT INTO domaines VALUES( '140', 'mm', 'Myanmar');
INSERT INTO domaines VALUES( '141', 'mn', 'Mongolie');
INSERT INTO domaines VALUES( '142', 'mo', 'Macao');
INSERT INTO domaines VALUES( '143', 'mp', 'Mariannes du nord (iles)');
INSERT INTO domaines VALUES( '144', 'mq', 'Martinique');
INSERT INTO domaines VALUES( '145', 'mr', 'Mauritanie');
INSERT INTO domaines VALUES( '146', 'ms', 'Montserrat');
INSERT INTO domaines VALUES( '147', 'mt', 'Malte');
INSERT INTO domaines VALUES( '148', 'mu', 'Maurice (ile)');
INSERT INTO domaines VALUES( '149', 'mv', 'Maldives');
INSERT INTO domaines VALUES( '150', 'mw', 'Malawi');
INSERT INTO domaines VALUES( '151', 'mx', 'Mexique');
INSERT INTO domaines VALUES( '152', 'my', 'Malaisie');
INSERT INTO domaines VALUES( '153', 'mz', 'Mozambique');
INSERT INTO domaines VALUES( '154', 'na', 'Namibie');
INSERT INTO domaines VALUES( '155', 'nc', 'Nouvelle Cal�donie');
INSERT INTO domaines VALUES( '156', 'ne', 'Niger');
INSERT INTO domaines VALUES( '157', 'nf', 'Norfolk (ile)');
INSERT INTO domaines VALUES( '158', 'ng', 'Nig�ria');
INSERT INTO domaines VALUES( '159', 'ni', 'Nicaragua');
INSERT INTO domaines VALUES( '160', 'nl', 'Pays Bas');
INSERT INTO domaines VALUES( '161', 'no', 'Norv�ge');
INSERT INTO domaines VALUES( '162', 'np', 'N�pal');
INSERT INTO domaines VALUES( '163', 'nr', 'Nauru');
INSERT INTO domaines VALUES( '164', 'nu', 'Niue');
INSERT INTO domaines VALUES( '165', 'nz', 'Nouvelle Z�lande');
INSERT INTO domaines VALUES( '166', 'om', 'Oman');
INSERT INTO domaines VALUES( '167', 'pa', 'Panama');
INSERT INTO domaines VALUES( '168', 'pe', 'P�rou');
INSERT INTO domaines VALUES( '169', 'pf', 'Polyn�sie Fran�aise');
INSERT INTO domaines VALUES( '170', 'pg', 'Papouasie Nouvelle Guin�e');
INSERT INTO domaines VALUES( '171', 'ph', 'Philippines');
INSERT INTO domaines VALUES( '172', 'pk', 'Pakistan');
INSERT INTO domaines VALUES( '173', 'pl', 'Pologne');
INSERT INTO domaines VALUES( '174', 'pm', 'St. Pierre et Miquelon');
INSERT INTO domaines VALUES( '175', 'pn', 'Pitcairn (ile)');
INSERT INTO domaines VALUES( '176', 'pr', 'Porto Rico');
INSERT INTO domaines VALUES( '177', 'pt', 'Portugal');
INSERT INTO domaines VALUES( '178', 'pw', 'Palau');
INSERT INTO domaines VALUES( '179', 'py', 'Paraguay');
INSERT INTO domaines VALUES( '180', 'qa', 'Qatar');
INSERT INTO domaines VALUES( '181', 're', 'R�union (ile de la)');
INSERT INTO domaines VALUES( '182', 'ro', 'Roumanie');
INSERT INTO domaines VALUES( '183', 'ru', 'Russie');
INSERT INTO domaines VALUES( '184', 'rw', 'Rwanda');
INSERT INTO domaines VALUES( '185', 'sa', 'Arabie Saoudite');
INSERT INTO domaines VALUES( '186', 'sb', 'Salomon (iles)');
INSERT INTO domaines VALUES( '187', 'sc', 'Seychelles');
INSERT INTO domaines VALUES( '188', 'sd', 'Soudan');
INSERT INTO domaines VALUES( '189', 'se', 'Su�de');
INSERT INTO domaines VALUES( '190', 'sg', 'Singapour');
INSERT INTO domaines VALUES( '191', 'sh', 'St. H�l�ne');
INSERT INTO domaines VALUES( '192', 'si', 'Slov�nie');
INSERT INTO domaines VALUES( '193', 'sj', 'Svalbard et Jan Mayen (iles)');
INSERT INTO domaines VALUES( '194', 'sk', 'Slovaquie');
INSERT INTO domaines VALUES( '195', 'sl', 'Sierra Leone');
INSERT INTO domaines VALUES( '196', 'sm', 'Saint Marin');
INSERT INTO domaines VALUES( '197', 'sn', 'S�n�gal');
INSERT INTO domaines VALUES( '198', 'so', 'Somalie');
INSERT INTO domaines VALUES( '199', 'sr', 'Suriname');
INSERT INTO domaines VALUES( '200', 'st', 'Sao Tome et Principe');
INSERT INTO domaines VALUES( '201', 'sv', 'Salvador');
INSERT INTO domaines VALUES( '202', 'sy', 'Syrie');
INSERT INTO domaines VALUES( '203', 'sz', 'Swaziland');
INSERT INTO domaines VALUES( '204', 'tc', 'Turks et Ca�ques (iles)');
INSERT INTO domaines VALUES( '205', 'td', 'Tchad');
INSERT INTO domaines VALUES( '206', 'tf', 'Territoires Fran�ais du sud');
INSERT INTO domaines VALUES( '207', 'tg', 'Togo');
INSERT INTO domaines VALUES( '208', 'th', 'Thailande');
INSERT INTO domaines VALUES( '209', 'tj', 'Tajikistan');
INSERT INTO domaines VALUES( '210', 'tk', 'Tokelau');
INSERT INTO domaines VALUES( '211', 'tm', 'Turkm�nistan');
INSERT INTO domaines VALUES( '212', 'tn', 'Tunisie');
INSERT INTO domaines VALUES( '213', 'to', 'Tonga');
INSERT INTO domaines VALUES( '214', 'tp', 'Timor Oriental');
INSERT INTO domaines VALUES( '215', 'tr', 'Turquie');
INSERT INTO domaines VALUES( '216', 'tt', 'Trinidad et Tobago');
INSERT INTO domaines VALUES( '217', 'tv', 'Tuvalu');
INSERT INTO domaines VALUES( '218', 'tw', 'Taiwan');
INSERT INTO domaines VALUES( '219', 'tz', 'Tanzanie');
INSERT INTO domaines VALUES( '220', 'ua', 'Ukraine');
INSERT INTO domaines VALUES( '221', 'ug', 'Ouganda');
INSERT INTO domaines VALUES( '222', 'uk', 'Royaume Uni');
INSERT INTO domaines VALUES( '223', 'gb', 'Royaume Uni');
INSERT INTO domaines VALUES( '224', 'um', 'US Minor Outlying (iles)');
INSERT INTO domaines VALUES( '225', 'us', 'Etats Unis');
INSERT INTO domaines VALUES( '226', 'uy', 'Uruguay');
INSERT INTO domaines VALUES( '227', 'uz', 'Ouzb�kistan');
INSERT INTO domaines VALUES( '228', 'va', 'Vatican');
INSERT INTO domaines VALUES( '229', 'vc', 'Saint Vincent et les Grenadines');
INSERT INTO domaines VALUES( '230', 've', 'Venezuela');
INSERT INTO domaines VALUES( '231', 'vg', 'Vierges Britaniques (iles)');
INSERT INTO domaines VALUES( '232', 'vi', 'Vierges USA (iles)');
INSERT INTO domaines VALUES( '233', 'vn', 'Vi�t Nam');
INSERT INTO domaines VALUES( '234', 'vu', 'Vanuatu');
INSERT INTO domaines VALUES( '235', 'wf', 'Wallis et Futuna (iles)');
INSERT INTO domaines VALUES( '236', 'ws', 'Western Samoa');
INSERT INTO domaines VALUES( '237', 'ye', 'Yemen');
INSERT INTO domaines VALUES( '238', 'yt', 'Mayotte');
INSERT INTO domaines VALUES( '239', 'yu', 'Yugoslavie');
INSERT INTO domaines VALUES( '240', 'za', 'Afrique du Sud');
INSERT INTO domaines VALUES( '241', 'zm', 'Zambie');
INSERT INTO domaines VALUES( '242', 'zr', 'Za�re');
INSERT INTO domaines VALUES( '243', 'zw', 'Zimbabwe');
INSERT INTO domaines VALUES( '244', 'com', '-');
INSERT INTO domaines VALUES( '245', 'net', '-');
INSERT INTO domaines VALUES( '246', 'org', '-');
INSERT INTO domaines VALUES( '247', 'edu', '-');
INSERT INTO domaines VALUES( '248', 'int', '-');
INSERT INTO domaines VALUES( '249', 'arpa', '-');
INSERT INTO domaines VALUES( '250', 'at', 'Autriche');
INSERT INTO domaines VALUES( '251', 'gov', 'Gouvernement');
INSERT INTO domaines VALUES( '252', 'mil', 'Militaire');
INSERT INTO domaines VALUES( '253', 'su', 'Ex U.R.S.S.');
INSERT INTO domaines VALUES( '254', 'reverse', '-');
