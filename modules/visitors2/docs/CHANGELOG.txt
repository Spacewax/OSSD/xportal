// ------------------------------------------------------------------------- //
// Les Visiteurs - Statistiques de fr�quentation d'un site web               //
// ------------------------------------------------------------------------- //
// Visitors      - Web site statistics analysis program                      //
// ------------------------------------------------------------------------- //
// Copyright (C) 2000, 2001  J-Pierre DEZELUS <jpdezelus@phpinfo.net>        //
// ------------------------------------------------------------------------- //
//                   phpInfo.net <http://www.phpinfo.net/>                   //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
//                                  CHANGELOG                                //
// ------------------------------------------------------------------------- //


31.01.2001 v2.00
- r�criture compl�te de l'application en anglais, les fichiers changent tous de
  noms, mais la structure des tables reste la m�me. Fichier index.php[3] en
  entr�e de l'application.
- interface de navigation entre les diff�rentes pages de stats
- Utilisation des modules de cache pour all�ger les serveurs
- la liste des moteurs de recherche est maintenant dans un fichier ini
- les diff�rents navigateurs reconnus et leurs versions sont maintenant
  d�clar�s dans le fichier de configuration principal
- Tous les messages sont centralis�s dans un m�me fichier (1 par langue)
- Fichier d'absraction de la base de donn�es, pour faciliter le portage vers
  d'autres SGBD
- Possibilit� d'exclure autant de machines que souhait�. Idem pour les serveurs.
- Ajout d'un grand nombre de variables de configuration dans le fichier de conf
  principal afin de faciliter la personnalisation des stats.
- Possibilit� de choisir ses propres motifs et couleurs pour les histogrammes.
- Ex�cution de la requ�tre : SET SQL_BIG_TABLES = 1 avant l'affichage du Top
  des Visiteurs (stats mensuelles). Quand la table 'visiteurs' contient trop
  d'enregistrements, la requ�te provoque une erreur 'table full'. Cette requ�te
  pr�alable r�gle le probl�me.
- Utilisation de la fonction strip_tags() sur l'agent avant son stockage dans
  la base pour �viter l'utilisation de balises HTML. Idem pour le r�f�rant.
- R�duction de l'agent � 50c quand il n'est pas reconnu.
- Barre de s�paration des journ�es dans le log des derniers visiteurs.
- Les @IP et hostname peuvent maintenant �tre tronqu�s


05.01.2001 v1.42
- Ajout des stats sur Windows Millenium et Windows Me dans
  visiteurs-detail.php3. Ils �taient bien reconnus et enregistr�s dans la base,
  mais n'appararaissaient pas dans les % OS [Merci � J�r�me].
- Dans visiteurs-detail.php3 toujours, $mois et $annee sont positionn�s au mois
  et � l'ann�e courante si aucun param�tre n'est pass� sur l'URL.


01.01.2001 v1.41
- Correction d'un bug dans le calendrier. Mauvaise gestion du changement
  d'ann�e. Merci � Zapoyok pour avoir trouv� la solution et m'�vit� ainsi de 
  chercher trop longtemps (difficile de se concentrer un 1er janvier !).
- Je profite de cette mise � jour pour ajouter la d�tection tant attendue de
  NS6 final et de Windows 2000 et Millenium.


30.05.2000 v1.4
- Ajout d'un script d'archivage permettant de purger la base des mois
  pr�c�dents tout en conservant les statistiques pour l'affichage.
*******************************************************************************
  Attention, cette nouvelle fonctionnalit� n�cessite la cr�ation d'une
  nouvelle table 'archives' pour le stockage des r�sultats (cf. INSTALL.txt)
*******************************************************************************
- Utilisation d'un cookie pour que ses propres machines ne soient pas prises en
  compte par les Visiteurs (en plus des 2 autres - bureau et maison - que l'on 
  peut d�finir dans 2 variables de configuration).
- Page de statistiques annuelles et pages de statistiques mensuelles s�par�es.
- Nouveau graphique : Nb Visiteurs/Jour/12 derniers mois avec d�calage 
  automatique � chaque d�but de mois (idem pour le graphique Nb Visiteurs/Mois/
  12 derniers mois et pour le tableau d�tail des 12 derniers mois).
- Nouveau syst�me d'enregistrement d'un visiteurs : un visiteur est enregistr�
  si il ne fait pas partie des n derniers visiteurs (� d�finir dans les
  param�tres de configuration).
- Table des domaines compl�te en fran�ais ou en anglais (un grand merci � 
  Christophe Meyer <chrismey@free.fr> qui me les a envoy�es).
- Possibilit� de choisir le format des graphiques g�n�r�s (GIF ou PNG).
- Am�lioration de l'affichage des �chelles et des valeurs dans les
  diff�rents histogrammes.


01.04.2000 v1.31
- Correction sur l'enregistrement des r�f�rants. Une ligne de code avait saut� !


06.03.2000 v1.3
- Utilisation de motifs (d�grad�s) pour les histogrammes.
- Affichage des max dans une autre couleur.
- Calcul dynamique de l'�chelle en fonction du max.
- Nom des serveurs des visiteurs.
- R�f�rants des visiteurs, et mots cl�s utilis�s sur les moteurs de
  recherche.
- Dans une m�me journ�e un visiteur n'est enregistr� que lors de sa 1�re
  visite
- Utilisation de feuilles de styles externes
*******************************************************************************
- Attention les noms des fichiers ont chang� :
  fonctions-free.php3  ->  lib-phpinfo.inc.php3
  config-free.php3     ->  conf/cfg-phpinfo.inc.php3
*******************************************************************************


26.02.2000 v1.2
- Ajout d'un secours texte quand la base MySQL n'est pas disponible (les
  requ�tes sont stock�es dans un fichier texte, il suffit de les ex�cuter � la
  main quand la base est revenue -> copier/coller dans phpMyAdmin).
- Utilisation de la biblioth�que graphique GD pour g�n�rer des histogrammes.
- Ajout d'un tableau de statistiques sur les noms de domaines des visiteurs
  (n�cessit� de cr�er une table 'domaines' pour ceux qui disposaient d'une
  ancienne version -> cf fichier domaines.sql pour la cr�ation).


02.02.2000 v1.1
- Modification de la structure de la table visiteurs. Ajout d'un champ REF_HOST
  contenant uniquement le nom host du REFERER (pour les statistiques).
  REFERER  = 'http://www.phpinfo.net/bidon/fichier.php3?var=15'
  REF_HOST = 'http://www.phpinfo.net/'
- Correction du fichier visiteurs.php3
  (erreurs quand la table �tait vide : divisions par z�ro).


21.01.2000 v1.0
- 1�re version 'publique'.