// ------------------------------------------------------------------------- //
// Les Visiteurs - Statistiques de fréquentation d'un site web               //
// ------------------------------------------------------------------------- //
// Visitors      - Web site statistics analysis program                      //
// ------------------------------------------------------------------------- //
// Copyright (C) 2000, 2001  J-Pierre DEZELUS <jpdezelus@phpinfo.net>        //
// ------------------------------------------------------------------------- //
//                   phpInfo.net <http://www.phpinfo.net/>                   //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
//                                INSTALLATION                               //
// ------------------------------------------------------------------------- //
// Translation [french -> english] by Pascal NIZZRI <pascal.nizri@oreka.com> // 
// ------------------------------------------------------------------------- //


INSTALLATION
------------
- Uncompress the archive in a directory of your Web server. Keep the organisation
  of the files.

- If it's a first installation, create the 3 needed MySQL tables: visiteurs,
  archives and domaines_xx (where xx matches with the code of the wanted langage
  for the domain names). Requests for tables creation are available in the 
  'sql-tables' directory with the '.sql' extension. domaines_xx files create all
  a same 'domaine' table (so just execute one). You can if you want change the
  name of these 3 tables if they are in conflict with existing tables in your
  base (else than the ones from a previous version of The Visitors, of course),
  you'll be able to change these names in the configuration file.

    mysql --user=user --password=password basename < archives.sql
    mysql --user=user --password=password basename < visiteurs.sql
    mysql --user=user --password=password basename < domaines_xx.sql

  If you don't have any MySQL base yet :

    mysqladmin --user user --password password create basename

- If you use PHP 4 or if your Web server is configured to parse the .php files
  rather than the .php3 ones, change the extension of the following files:

    admin/ admin.php3             -> admin/admin.php
    include/ new-visitor.inc.php3 -> include/ new-visitor.inc.php
    include/ config.inc.php3      -> include/ config.inc.php
    index.php3                    -> index.php
    image-vis.php3                -> image-vis.php

  Watch out, it is imperative to change the extension of the file config.inc.php
  so it is not downloadable.

- You can (must?), if you want, rename the following files and directories
  (in order to warrant the integrity of your installation):

    Dir. admin/
    Dir. include/
    Dir. include/ modules/
    Dir. include/ caches/

    File include/ new-visitor.inc.ph3
    File admin/ admin.ph3

  As a piece of advice, you should also rename the configuration file:
  -> config.inc.php3, for the same reasons as before.

- If you change the name of the 'include' directory or the name of the
  configuration file 'config.inc.php3', edit the following files and update the
  2 variables $lvc_include_dir and $lvc_config_name.

    admin/ admin.php3
    include/ new-visitor.inc.php3
    index.php3
    image-vis.php3


CONFIGURATION
-------------
Edit the include/config.inc.php3 file.

- Choose the file of used messages, it depends on your langage:

  require($lvc_include_dir.'lang/xxxxx.inc.php');

- Choose the file of database abstraction (MySQL, PostgreSQL,...).

  require($lvc_include_dir.'db/db_mysql.inc.php');

- Enter the connection parameters for the database server (MySQL,...)

    $lvc_db_host
    $lvc_db_user
    $lvc_db_password
    $lvc_db_database

- If you have changed the names of used tables, indicate it in the 3 variables:

    $lvc_table_visitors
    $lvc_table_archives
    $lvc_table_domains

- Enter the site's name (for graphics and titles) and the date when application
starts to record visitors.

    $lvc_site_name
    $lvc_site_opening_year 
    $lvc_site_opening_month

- Indicate the new name of the 'admin' directory and eventually the ones of the
  'include/caches' and 'include/modules' directories if you also renamed them.

    $lvc_admin_dir
    $lvc_cache_dir
    $lvc_modules_dir

- Enter the new name of the main administration file (admin.php3 from admin
  directory).

    $lvc_admin_file

- Precise whether or not you want a link to the admin console in the main screen
  of the Visitors (WATCH OUT, in this case the Net users will see it if you 
  didn't protect application access).

    $lvc_view_admin_menus

- You can set a cookie on your own computer so it is not recorded in the stats.
  It can be necessary if you don't have a determined @IP. You can name the
  cookie as you want (it is even adviced to change its name).
  To reset it on your computer, you just need to call in your browser the page
  where you have inserted the registering script of the Visitors (have a look
  on the next chapter 'REGISTER THE VISITORS'):

      http://www.votresite.com/index.php3?ckVisiteurs=1

  This creates the cookie "ckVisiteurs" set to 1 on your computer. The
  application will no longer record you, now it finds the cookie. Think about
  changing the cookie's name in the configuration file, so it's impossible to
  anyone to also set it to 1 on his computer and so not to be recorded anymore.

    $lvc_cookie_name

- Give the list of 'referers sites' where the visitor can come from without
  being recorded (usually, the site's address itself), the list of machines to
  ignore (their @IP), and the list of servers on which nothing should be recorded
  (test machines).

    $lvc_ignore_referer[]
    $lvc_ignore_machines[]
    $lvc_ignore_servers[]

- Indicate the number of visitors to be recorded before the same visitor is
  recorded again.

    $lvc_between_2_visits

- It is possible to cut after n letters the unknown agents before recording

    $lvc_agent_max_length

- When the database server is down, the SQL request are recorded in a log file.

    $lvc_log_file

- You can set the last of each cache modules before regeneration:

    $lvc_default_cache_delay
    $lvc_delay_archive_month
    $lvc_delay_calendar
    ...

- And if you want the last of caches and when they were regenerated to appear.

    $lvc_display_cache_delay

- Choose the format of generated images. It depends on the configuration of PHP
  on your server and on GD used version (do a phpinfo() to have more information
  about GD extension).

    $lvc_images_format

- For the histograms, give the basis scales (depends on the frequentation of
  your site)

    $lvc_base_img_last_months
    $lvc_base_img_day_per_hour
    $lvc_base_img_per_hour
    ...

- Indicate the names of the motives files used for the histograms (name without
  the extension obtained with $lvc_images_format).

    $lvc_pattern_last_months
    $lvc_pattern_day_per_hour
    $lvc_pattern_per_hour
    $lvc_pattern_per_day

- Modify eventually the diagrams colors.

    $lvc_color_bg_out
    $lvc_color_bg_in
    $lvc_color_maxvalue
    ...

- Give the number of data lines to display per table type.

    $lvc_nb_last_visitors
    $lvc_nb_last_months
    $lvc_nb_months_calendar
    ...

- Indicate if you want to hide or not a part of @IP or hostnames of your visitors.
  It is adviced to leave this value on true.

    $lvc_hide_IP

- You can complete or modify the search engines list which refer to your site.
  They are defined in the file:
  
    include/ engines-list.ini
  
  For search engines, indicates its name in between hooks, keyword used on the
  url followed with the sign =, then the different concerned domains.
  Example:

    [AltaVista]
    q=
    www.altavista.com
    www.altavista.fr
    fr.altavista.com


  For address lists, indicates the name in between hooks, 'directory' on the
  following line (without = sign), then the domains. Example:
    
    [Yahoo !]
    directory
    fr.dir.yahoo.com


REGISTRER THE VISITORS
----------------------
On each entry page of your web site where you want to record your visitors, place
the following code: 

    $lvc_include_dir = 'path_to/include-dir/';
    include($lvc_include_dir.'new-visitor.inc.php3');

$lvc_include_dir indicates the path to  the 'include' directory of The Visitors
from the file calling the script 'new-visitor.inc.php3'.

If you have installed The Visitors in your site's root, in the directory
'apps/visiteurs', and if you wish to register The Visitors from your frontpage
index.php3 which is in the root, place in the script header:

    $lvc_include_dir = 'apps/visiteurs/include/';
    include($lvc_include_dir.'new-visitor.inc.php3');

WATCH OUT, teach right the variable '$lvc_include_dir' since it is used after by
'new-visitor.inc.php3' to load the configuration file.


CONSULT THE STATS
-----------------
You can consult the stats by calling the page index.php3 from the Visitors'
installation directory, or directly this directory.

  http://www.votresite.com/apps/mes-visiteurs/

You have then a graphic navigation interface to consult the different stats or
to access the administration module if you configured the application to do it
(variable $lvc_view_admin_menus on true).


MANAGE THE APPLICATION
----------------------
*******************************************************************************
  WATCH OUT
  The scripts of the administration module use Javascript/Jscript.
*******************************************************************************

Two modules are proposed, one to delete the caches files generated by the
application, the other to create the archives of passed months (which make the
stats display faster).

o STORAGE :
  --------
  The storage is not automatic, start it manually via the administration console
  in the beginning of each month (storage of passed month).
  To store a month do the following steps:
    - Select the month and the year to store and check that the archive doesn't
      exist.

    - Select the 'Store' option to create a new entry in the 'Archives' table
      which will contain a sum up of the stats for the selected month.

    - Select then the 'Export' option to get back the SQL dump for the stored
      month (a text file is downloaded, it contains all the records of the
      'visiteurs' table for the selected month).

    - At last, make sure the archive exists, select the 'Purge' option to
      definitely purge the records of the 'visiteurs' table (this last option
      is optional, it simply enables you to reduce the base size). In the stats
      display for a selected month, if an archive is found in the base, this one
      is used and not the eventual records in the 'visiteurs' table.
      That's why they are no longer available after a storage.
  
  On each step you have to give the access password to your base. It has to be
  the same that the one given in the configuration file.

o CACHES :
  --------
  You also have the possibility to delete the cache files on the disk to force
  their regeneration (after storage for instance).


PROTECT THE SCRIPTS
-------------------
- The first protection consists to rename the directories and files indicated in
  the installation process.

- Change the configuration file extension depending on your PHP version so it is
  not downloadable., especially if you didn't rename the directories.

- Place the .htaccess files (or equivalent, depends on your Web server) in the
  'include' and 'admin' directories to forbid their access.


DIVERSE NOTICIES
----------------
- The 1.3x version users have to create the 'archive' table which didn't exist
  in this version.

- You can modify the stats pages colors by modifying the css/visitors.css CSS
  style sheets. A variable in the configuration file allows to define another
  file to use.

    $lvc_css_file