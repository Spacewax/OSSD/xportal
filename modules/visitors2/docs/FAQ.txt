// ------------------------------------------------------------------------- //
// Les Visiteurs - Statistiques de fr�quentation d'un site web               //
// ------------------------------------------------------------------------- //
// Visitors      - Web site statistics analysis program                      //
// ------------------------------------------------------------------------- //
// Copyright (C) 2000, 2001  J-Pierre DEZELUS <jpdezelus@phpinfo.net>        //
// ------------------------------------------------------------------------- //
//                   phpInfo.net <http://www.phpinfo.net/>                   //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
//                                    F.A.Q.                                 //
// ------------------------------------------------------------------------- //


Q: Comment masquer une partie des @ IP et des hostnames ?

A: En mettant � true la variable $lvc_hide_IP dans le fichier de configuration

// ------------------------------------------------------------------------- //

Q: Quand je configure le nombre de mois affich�s ($lvc_nb_last_months = 8), il
   s'affiche '8 Derniers mois', mais seuls les 6 derniers apparaissent !

A: C'est normal, c'est aussi un module de cache ! Il s'appelle
   'caches/last_months.cache.html' et a une dur�e de
   '$lvc_delay_last_months = 86400' => 1j. Il suffit de le supprimer pour le
   reg�n�rer.

// ------------------------------------------------------------------------- //

