<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_partners_show($options){	
	global $xoopsDB, $xoopsConfig;
	$block = array();
	$block['title'] = _MB_PARTNERS_TITLE;
	$result = $xoopsDB->query("SELECT num,image FROM ".$xoopsDB->prefix("click")."");
	$block['content'] = "<div align=\"center\">";
	while (list($num, $image) = $xoopsDB->fetch_row($result)) { 
		$block['content'] .= "<a href=\"".$xoopsConfig['xoops_url']."/modules/partners/index.php?op=click&id=$num\" target=\"_blank\">";
		$block['content'] .= GetImgTag(dirname($image),basename($image));
		$block['content'] .= "</a><br />\n";
		if ($options[0] == 1) {
			$block['content'] .= "<br />\n";
		}
	}
	$block['content'] .= "</div>";

	return $block;
}

function b_partners_edit($options) {
	$chk = "";
	$form = ""._MB_PARTNERS_PSPACE."&nbsp;";
	if ($options[0] == 0) {
		$chk = " checked=\"checked\"";
	}
	$form .= "<input type=\"radio\" name=\"options[]\" value=\"0\"".$chk." />&nbsp;"._NO."";
	$chk = "";
	if ($options[0] == 1) {
		$chk = " checked=\"checked\"";
	}
	$form .= "&nbsp;<input type=\"radio\" name=\"options[]\" value=\"1\"".$chk." />"._YES."";
	return $form;
}
?>