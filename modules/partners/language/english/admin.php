<?php
//%%%%%%	Admin Module Name  Partners 	%%%%%
define("_AM_DBUPDATED","Database Updated Successfully!");

define("_AM_ID","ID");
define("_AM_HITS","Hits");
define("_AM_URL","URL");
define("_AM_IMAGE","Image");
define("_AM_DESCRIPTION","Description");
define("_AM_EDIT","Edit");
define("_AM_DEL","Delete");
define("_AM_ADNPARTNER","Add a New Partner");
define("_AM_IMAGEURL","Image URL:");
define("_AM_CLICKURL","Click URL:");
define("_AM_ADDPARTNER","Add Partner");
define("_AM_EDITPARTNER","Edit Partner");
define("_AM_WAYSWTDELPA","WARNING: Are you sure you want to delete this Partner?");
define("_AM_YES","Yes");
define("_AM_NO","No");
?>