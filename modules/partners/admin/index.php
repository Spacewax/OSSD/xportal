<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// ORIGINAL FILE INFO
######################################################################
# Partner-Addon for PHP-NUKE Version 1.0 Beta
# ===========================
#
# Copyright (c) 2001 by Dietrich Bojko (webmaster@jimmy-net.de)
# http://www.jimmy-net.de
#
# This modules is the main administration part
#
######################################################################

include("admin_header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

function PartnerAdmin() {
	global $xoopsTheme, $xoopsDB;
	OpenTable();
	echo "<table width=95% border=0 align=center><tr>
        <td bgcolor='".$xoopsTheme['bgcolor1']."'><b>" ._AM_ID."</b></td>
        <td bgcolor='".$xoopsTheme['bgcolor1']."'><b>" ._AM_HITS."</b></td>
        <td bgcolor='".$xoopsTheme['bgcolor1']."'><b>" ._AM_URL."</b></td>
        <td bgcolor='".$xoopsTheme['bgcolor1']."'><b>" ._AM_IMAGE."</b></td>
        <td bgcolor='".$xoopsTheme['bgcolor1']."'><b>" ._AM_DESCRIPTION."</b></td>
        <td bgcolor='".$xoopsTheme['bgcolor1']."'></td><tr>";
        $result = $xoopsDB->query("SELECT num, hits, url, image, text FROM ".$xoopsDB->prefix("click")." ORDER BY num");

        while ( list($num, $hits, $url, $image, $text) = $xoopsDB->fetch_row($result) ) {
        	echo "
            	<td bgcolor='".$xoopsTheme['bgcolor2']."'><small>$num</small></td>
            	<td bgcolor='".$xoopsTheme['bgcolor2']."'><small>$hits</small></td>
            	<td bgcolor='".$xoopsTheme['bgcolor2']."'><small>$url</small></td>
            	<td bgcolor='".$xoopsTheme['bgcolor2']."'><small>$image</small></td>
            	<td bgcolor='".$xoopsTheme['bgcolor2']."'><small>$text</small></td>
            	<td bgcolor='".$xoopsTheme['bgcolor2']."'><small>
            	<a href='index.php?op=PartnerEdit&amp;num=".$num."'>"._AM_EDIT."</a>|<a href='index.php?op=PartnerDel&amp;num=".$num."&amp;ok=0'>"._AM_DEL."</a></small></td><tr>
            	";
        }
        echo "</td></tr></table>";
	CloseTable();
	echo "<br />";
    	OpenTable();
	echo "<h4>"._AM_ADNPARTNER."</h4>
        <form action='index.php' method='post'>";
        echo ""._AM_IMAGEURL."<input type='text' name='image' size='50' maxlength='100' /><br />
        "._AM_CLICKURL."<input type='text' name='url' size='50' maxlength='200' /><br />
        "._AM_DESCRIPTION."<br /><textarea name='text' cols='60' rows='10'></textarea><br />
        <input type='hidden' name='op' value='PartnerAdd' />
        <input type='submit' value='"._AM_ADDPARTNER."' />";
	echo "</form>";
	CloseTable();
}

function PartnerEdit($num) {
	global $xoopsDB;
	$myts = new MyTextSanitizer();
        OpenTable();
        $result = $xoopsDB->query("SELECT num, hits, url, image, text FROM ".$xoopsDB->prefix("click")." WHERE num=$num");
        list($num, $hits, $url, $image, $text) = $xoopsDB->fetch_row($result);
	$text = $myts->makeTareaData4Edit($text);
	$url = $myts->makeTareaData4Edit($url);
	$image = $myts->makeTareaData4Edit($image);
        echo "
        <h4>"._AM_EDITPARTNER."</h4>
        ".GetImgTag(dirname($image),basename($image))."<br /><br />
        <form action='index.php' method='post'>";
        echo _AM_HITS."<input type='text' name='hits' size='3' maxlength='10' value='".$hits."' /><br />
        " ._AM_URL." &nbsp;&nbsp;&nbsp;<input type='text' name='url' size='50' maxlength='100' value='".$url."' /><br />
        " ._AM_IMAGE." <input type='text' name='image' size='50' maxlength='100' value='".$image."' /><br />
        "._AM_DESCRIPTION."<br /><textarea name='text' cols='60' rows='10' />".$text."</textarea><br />
        <input type='hidden' name='num' value='".$num."' />
        <input type='hidden' name='op' value='PartnerChange' />
        <input type='submit' value='"._AM_EDITPARTNER."' />
        </form>";
	CloseTable();
}

function PartnerChange($num, $hits, $url, $image, $text) {
	global $xoopsDB;
	$myts = new MyTextSanitizer();
	$text = $myts->makeTareaData4Save($text);
	$url = $myts->makeTareaData4Save($url);
	$image = $myts->makeTareaData4Save($image);
        $xoopsDB->query("UPDATE ".$xoopsDB->prefix("click")." set num=".$num.", hits=".$hits.", url='".$url."', image='".$image."', text='".$text."' where num=".$num."");
	redirect_header("index.php",1,_AM_DBUPDATED);
	exit();
}


function PartnerAdd($num, $hits, $url, $image, $text) {
	global $xoopsDB;
	$myts = new MyTextSanitizer();
	$text = $myts->makeTareaData4Save($text);
	$url = $myts->makeTareaData4Save($url);
	$image = $myts->makeTareaData4Save($image);
	$newid = $xoopsDB->GenID("click_num_seq");
    	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("click")." (num, hits, url, image, text) VALUES ($newid, 0,'$url','$image','$text')",1);
    	redirect_header("index.php",1,_AM_DBUPDATED);
	exit();
}


function PartnerDel($num, $ok=0) {
	global $xoopsDB, $xoopsConfig;
    	if ( $ok == 1 ) {
		$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("click")." WHERE num=$num");
		redirect_header("index.php",1,_AM_DBUPDATED);
		exit();
    	} else {
		include($xoopsConfig['root_path']."header.php");
		OpenTable();
		echo "<div align='center'><br />";
		echo "<font color='#ff0000'>";
		echo "<b>"._AM_WAYSWTDELPA."</b><br /><br /></font>";
        echo "[ <a href='index.php?op=PartnerDel&amp;num=".$num."&amp;ok=1'>"._AM_YES."</a> | <a href='index.php'>"._AM_NO."</a> ]<br /><br /></div>";
		CloseTable();
    	}
}

switch ($op){

	case "PartnerChange":
		PartnerChange($num, $hits, $url, $image, $text);
		break;

	case "PartnerAdd":
		PartnerAdd($num, $hits, $url, $image, $text);
		break;
		
	case "PartnerDel":
		PartnerDel($num, $ok);
		break;

	case "PartnerEdit":
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		PartnerEdit($num);
		break;

	Default:
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		PartnerAdmin();
		echo "<br />";
		break;

}
include("admin_footer.php");
?>