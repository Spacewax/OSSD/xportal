<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// ORIGINAL FILE INFO
######################################################################
# Partner-Addon for PHP-NUKE Version 1.0 Beta
# ===========================
#
# Author: Dietrich Bojko (webmaster@jimmy-net.de)
# http://www.jimmy-net.de
######################################################################
include("header.php");

function index(){
	global $xoopsDB, $xoopsConfig, $xoopsTheme;
	if ( $xoopsConfig['startpage'] == "partners" ) {
		$xoopsOption['show_rblock'] =1;
		include($xoopsConfig['root_path']."header.php");
		make_cblock();
		echo "<br />";
	} else {
		$xoopsOption['show_rblock'] =0;
		include($xoopsConfig['root_path']."header.php");
	}

	$result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("click")."");
	$numrows = $xoopsDB->num_rows($result);
	OpenTable();
	echo "<div align='center'><h4>"._PA_PARTNERS."</h4><br />";
	if ( $numrows > 0 ) {
		echo "<table border='0' cellpadding='1' cellspacing='0' valign='top' width='100%'><tr><td bgcolor='".$xoopsTheme["bgcolor2"]."'>
    		<table width='100%' border='0' cellpadding='2' cellspacing='1'>";
 		while (list($num, $hits, $url, $image, $text) = $xoopsDB->fetch_row($result)) { 
			echo "<tr><td bgcolor='".$xoopsTheme['bgcolor3']."' align='center'>
 			<a href='".$xoopsConfig['xoops_url']."/modules/partners/index.php?op=click&amp;id=".$num."' target='_blank'>";
			echo GetImgTag(dirname($image),basename($image));
		 	echo "</td>
  			<td bgcolor='".$xoopsTheme['bgcolor3']."'>".$text."</td>
	  		<td bgcolor='".$xoopsTheme['bgcolor3']."' align='center'><small>";
			printf(_PA_NUMHITS,$hits);
			echo "</small></td></tr>";
  		}   
		echo "</table></td></tr></table>";
	}
	echo "<br /><br /><br />
	<a href='mailto:".$xoopsConfig['adminmail']."'><b>"._PA_BECOMEAPARTNER."</b></a><br /><br />
	</div>";
	CloseTable();

	include($xoopsConfig['root_path']."footer.php");
}

function clickpartner($id){
	global $xoopsDB;
	$res = $xoopsDB->query("UPDATE ".$xoopsDB->prefix("click")." SET hits=hits+1 WHERE num=$id");
	$res = $xoopsDB->fetch_array($xoopsDB->query("SELECT url from ".$xoopsDB->prefix("click")." WHERE num=$id"));
	echo "<html><head><meta http-equiv='Refresh' content='0; URL=".$res['url']."'></head><body></body></html>";
	exit();
} 

switch($op){
	case "click":
		clickpartner($id);
		break;
	default:
		index();
		break;
}
?>