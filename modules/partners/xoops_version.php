<?php
$modversion['name'] = _MI_PARTNERS_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_PARTNERS_DESC;
$modversion['author'] = "";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "";
$modversion['license'] = "GPL";
$modversion['official'] = 1;
$modversion['image'] = "partners_slogo.jpg";
$modversion['dirname'] = "partners";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Blocks
$modversion['blocks'][1]['file'] = "partners.php";
$modversion['blocks'][1]['name'] = _MI_PARTNERS_BNAME;
$modversion['blocks'][1]['description'] = _MI_PARTNERS_DESC;
$modversion['blocks'][1]['show_func'] = "b_partners_show";
$modversion['blocks'][1]['edit_func'] = "b_partners_edit";
$modversion['blocks'][1]['options'] = "1";

// Menu
$modversion['hasMain'] = 1;
?>