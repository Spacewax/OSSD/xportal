<?php
$modversion['name'] = _MI_QUOTES_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_QUOTES_DESC;
$modversion['author'] = "Erik Slooff<br />( erik@slooff.com )<br /> http://www.slooff.com/";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "quotes.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "quotes_slogo.jpg";
$modversion['dirname'] = "quotes";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Blocks
$modversion['blocks'][1]['file'] = "quotes.php";
$modversion['blocks'][1]['name'] = _MI_QUOTES_BNAME1;
$modversion['blocks'][1]['description'] = "Shows quotes in a random manner";
$modversion['blocks'][1]['show_func'] = "b_quotes_show";

// Menu
$modversion['hasMain'] = 0;
?>