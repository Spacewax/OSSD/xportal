<?php
// Module Info

// The name of this module
define("_MI_QUOTES_NAME","Quote of the Day");

// A brief description of this module
define("_MI_QUOTES_DESC","Shows a Quote of the Day block");

// Names of blocks for this module (Not all module has blocks)
define("_MI_QUOTES_BNAME1","Quotes Block");
?>