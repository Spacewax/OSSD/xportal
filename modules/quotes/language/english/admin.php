<?php
//%%%%%%	Admin Module Name  Quote of the Day 	%%%%%
define("_AM_DBUPDATED","Database Updated Successfully!");

define("_AM_ADDQUOTE","Add Quote");
define("_AM_MODIFYQUOTES","Modify Quotes");
define("_AM_QUOTETEXT","Quote text:");
define("_AM_AUTHOR","Author:");
define("_AM_ADD","Add");
define("_AM_SEARCHQUOTE","Search for quote:");
define("_AM_SEARCH","Search");
define("_AM_RESETSEARCH","Reset Search");
define("_AM_QUOTE","Quote");
define("_AM_ACTION","Action");
define("_AM_EDIT","Edit");
define("_AM_DELETE","Delete");
define("_AM_PREVIOUSPAGE","Previous Page");
define("_AM_BACK","back");
define("_AM_NEXTPAGE","Next Page");
define("_AM_NOQUOTEFD","No quotes found");
define("_AM_DELQUOTE","Delete Quote");
define("_AM_WAYWDTQ","WARNING: Are you sure you want to delete this quote?");
define("_AM_YES","Yes");
define("_AM_NO","No");
define("_AM_EDITQUOTE","Edit Quote");
define("_AM_SAVE","Save");

?>