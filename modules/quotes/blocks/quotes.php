<?php
function b_quotes_show(){
	global $xoopsDB;
	$block = array();
	$block['title'] = _MB_QUOTES_TITLE1;
	// random quote block
	// Create the random id for the quote
	$result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("quotes")."");
	list($quote_count) = $xoopsDB->fetch_row($result);
	
	// only proceed if we have quotes in the database
	if ($quote_count > 1) {
		mt_srand((double)microtime()*1000000);
		$random = mt_rand(0,$quote_count - 1);
		// Now that we have the random number we can pull out the record
		$result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("quotes")."",0,1,$random);
		$myrow = $xoopsDB->fetch_row($result);
		$myts = new MyTextSanitizer();
		// this is the start of the content for the qotd block
		$block['content'] = "<small><div align=\"center\"><i>";
		// add the quote text and remove whitespaces at begin or end
		$block['content'] .= $myts->makeTareaData4Show(trim($myrow[1]));
		$block['content'] .= "</i><br />";
		// add the author and then close everything
		$block['content'] .= "-- ".$myts->makeTboxData4Show($myrow[2])."<br /></div></small>";
	} elseif ($quote_count == 1) {
		// Now that we have the random number we can pull out the record
		$result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("quotes")."",0,1,1);
		$myrow = $xoopsDB->fetch_row($result);
		$myts = new MyTextSanitizer();
		// this is the start of the content for the qotd block
		$block['content'] = "<small><div align=\"center\"><i>";
		// add the quote text and remove whitespaces at begin or end
		$block['content'] .= $myts->makeTareaData4Show(trim($myrow[1]));
		$block['content'] .= "</i><br />";
		// add the author and then close everything
		$block['content'] .= "-- ".$myts->makeTboxData4Show($myrow[2])."<br /></div></small>";
	} else {
		// this is what we say when the quotes database is empty
		$block['content'] = _MB_QUOTES_NOQS;
	}
	return $block;
}
?>