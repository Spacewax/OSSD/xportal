<?php

######################################################################
# PHP-NUKE: Web Portal System
# ===========================
#
# Copyright (c) 2000 by Francisco Burzi (fburzi@ncc.org.ve)
# http://phpnuke.org
#
# This module is the main administration part
#
# This program is free software. You can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License.
######################################################################
# PHP-NUKE 4.4: Quote of the day Add-On
# =====================================
#
#        Brought to you by Erik Slooff
#
# Copyright (c) 2000 by Erik Slooff (erik@slooff.com)
#
# http://www.slooff.com
#
######################################################################

include("admin_header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

	// Basic function to display the admin stuff for the Quote Add-on
	function QotdAdmin() {
	    	OpenTable();
	    	echo "<h4>" ._AM_ADDQUOTE."</h4>
	    	<a href =\"index.php?op=QotdDisplay&page=1\">" ._AM_MODIFYQUOTES."</a><br /><br />
	    	<form action=index.php method=post>
	    	<table border=\"0\" width=\"100%\"><tr><td>
	    	" ._AM_QUOTETEXT." </td><td><textarea class=textbox name=qquote cols=60 rows=5></textarea></td></tr><tr><td>
	    	" ._AM_AUTHOR." </td><td><input class=textbox type=text name=qauthor size=31 maxlength=128></td></tr><tr><td>
	    	</td></tr></table>
	    	<input type=hidden name=op value=QotdAdd>
	    	<input type=submit value=" ._AM_ADD.">
	    	</form>";
		CloseTable();
	}

	// need to do some work here
	// change this function...
	function QotdAdd($qquote, $qauthor) {
		global $xoopsDB;
		$myts = new MyTextSanitizer();
		$qquote = $myts->makeTareaData4Save($qquote);
		$qauthor = $myts->makeTboxData4Save($qauthor);
		$newid = $xoopsDB->GenID("quotes_qid_seq");
	   	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("quotes")." (qid, quote, author) VALUES ($newid, '$qquote', '$qauthor')");
		redirect_header("index.php",2,_AM_DBUPDATED);
		exit();
	}
	
	function QotdDisplay($page) {
		global $keyword, $xoopsDB;
	    	OpenTable();
	    	$keyword2 = ereg_replace(" ", "%", $keyword);
            	$whereclause = "WHERE quote LIKE \"%".$keyword2."%\"";
	    	if ($keyword == "") $whereclause = "";
	    	$result = $xoopsDB->query("SELECT COUNT(*) AS help FROM ".$xoopsDB->prefix("quotes")." $whereclause");
	    	list($numrecords) = $xoopsDB->fetch_row($result);
	    	$entriesperpage = 20;
	    	$totalpages = ceil($numrecords / $entriesperpage);
	    	if ($numrecords > 0) {
			$myts = new MyTextSanitizer();
	    		echo "<h4>" ._AM_MODIFYQUOTES."</h4><br /><br />
			<table><tr><td>
			<form action=index.php method=post>
			" ._AM_SEARCHQUOTE." <input class=textbox type=text name=keyword size=31 maxlength=128 value=\"$keyword\">
			<input type=hidden name=page value=1>
			<input type=hidden name=op value=QotdDisplay>
			<input type=submit value=\"" ._AM_SEARCH."\">
			</form></td><td><form action=index.php method=post>
			<input type=hidden name=page value=1>
			<input type=hidden name=op value=QotdDisplay>
			<input type=submit value=\"" ._AM_RESETSEARCH."\">
			</form></td></tr></table>
	    		<table border width=\"100%\">";
	    		echo "<tr><td>" ._AM_QUOTE."</td><td>"._AM_AUTHOR."</td><td>" ._AM_ACTION."</td></tr>";
	    		$start = $entriesperpage * ($page - 1);
	    		$result = $xoopsDB->query("SELECT qid, quote, author FROM ".$xoopsDB->prefix("quotes")." $whereclause ORDER BY qid",1,$entriesperpage,$start);
	    		while(list($qqid, $qquote, $qauthor) = $xoopsDB->fetch_row($result)) {
				$qquote = $myts->makeTareaData4Show($qquote);
				$qauthor = $myts->makeTboxData4Show($qauthor);
	    			echo "<tr><td>".substr($qquote,0,50)."</td><td>".substr($qauthor,0,50)."</td>";
	    			echo "<td><a href=\"index.php?op=QotdEdit&qid=$qqid&page=$page\">" ._AM_EDIT."</a> | <a href=\"index.php?op=QotdDelete&qid=$qqid&page=$page\">" ._AM_DELETE."</a></td>";
	    			echo "</tr>";
	    		}
	    		echo "</table><br />";
	    		if ($page > 1) {
	    			$prevpage = $page - 1;
	    		} else {
	    			$prevpage = 0;
	    		}
	    		if ($page < $totalpages) {
	    			$nextpage = $page + 1;
	    		} else {
	    			$nextpage = 0;
	    		}
	    		echo "<table border=0 width=\"100%\"><tr><td align=\"center\" width=\"33%\">";
	    		if ($prevpage != 0) {
	    			echo "<a href=\"index.php?op=QotdDisplay&page=$prevpage&keyword=$keyword\">" ._AM_PREVIOUSPAGE."</a>";
	    		} else {
	    			echo " ";
	    		}
	    		echo "</td><td align=\"center\" width=\"33%\">";
	    		echo "<a href=\"index.php\">" ._AM_BACK."</a>";
	    		echo "</td><td align=\"center\" width=\"33%\">";
	    		if ($nextpage !=0) {
	    			echo "<a href=\"index.php?op=QotdDisplay&page=$nextpage&keyword=$keyword\">" ._AM_NEXTPAGE."</a>";
	    		} else {
	    			echo " ";
	    		}
	    		echo "</td></tr></table>";
	    	} else {
	    		echo _AM_NOQUOTEFD;
	    	}
	    	CloseTable();
	}
	
	function QotdDelete($qid,$page) {
		OpenTable();
		echo "<h4>" ._AM_DELQUOTE."</h4><br />";
		echo "<center>";
		echo "<font color=\"#ff0000\">";
		echo "<b>"._AM_WAYWDTQ."</b><br /><br /></font>";
		echo "[ <a href=index.php?op=QotdDeleteOk&qid=$qid&page=$page>"._AM_YES."</a> | <a href=index.php?op=QotdDisplay&page=$page>"._AM_NO."</a> ]<br /><br />";
		CloseTable();
	}
	
	function QotdDeleteOk($qid,$page) {
		global $xoopsDB;
		$xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("quotes")." WHERE qid=$qid");
	    	redirect_header("index.php?op=QotdDisplay&page=$page",2,_AM_DBUPDATED);
		exit();
	}

	function QotdEdit($qid,$page) {
		global $xoopsDB;
	    	$result = $xoopsDB->query("SELECT quote, author FROM ".$xoopsDB->prefix("quotes")." WHERE qid = $qid");
		$myts = new MyTextSanitizer();
	    	list($quote,$author) = $xoopsDB->fetch_row($result);
		$qquote = $myts->makeTareaData4Edit($qquote);
		$qauthor = $myts->makeTboxData4Edit($qauthor);
	    	OpenTable();
	    	echo "<h4>" ._AM_EDITQUOTE."</h4><br />
	    	<form action=index.php method=post>
	    	<table border=\"0\" width=\"100%\"><tr><td>
	    	" ._AM_QUOTETEXT." </td><td><textarea class=\"textbox\" name=\"qquote\" cols=\"60\" rows=\"5\">$quote</textarea></td></tr><tr><td>
	    	" ._AM_AUTHOR." </td><td><input class=\"textbox\" type=\"text\" name=\"qauthor\" size=\"31\" maxlength=\"128\" value=\"$author\"></td></tr><tr><td>
	    	</td></tr></table>
	    	<input type=hidden name=op value=QotdSave>
	    	<input type=hidden name=page value=$page>
	    	<input type=hidden name=qqid value=$qid>
	    	<input type=submit value=" ._AM_SAVE.">
	    	</form>";
	    	CloseTable();
    	}
   
	function QotdSave($qqid,$qquote,$qauthor,$page) {
		global $xoopsDB;
		$myts = new MyTextSanitizer();
		$qquote = $myts->makeTareaData4Save($qquote);
		$qauthor = $myts->makeTboxData4Save($qauthor);
		$xoopsDB->query("UPDATE ".$xoopsDB->prefix("quotes")." SET quote='$qquote', author='$qauthor' WHERE qid=$qqid");
		redirect_header("index.php?op=QotdDisplay&page=$page",2,_AM_DBUPDATED);
		exit();
	}

switch($op){
   	case "QotdAdd":
      // change the fields here for the quotes table
	  	QotdAdd($qquote, $qauthor);
      		break;

   	case "QotdSave":
      		QotdSave($qqid,$qquote,$qauthor,$page);
      		break;

  	case "QotdDeleteOk":
      		QotdDeleteOk($qid,$page);
      		break;
   
   	case "QotdDisplay":
      		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		QotdDisplay($page);
		echo "<br />";
      		break;
	     
   	case "QotdDelete":
      		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		QotdDelete($qid,$page);
		echo "<br />";
      		break;

   	case "QotdEdit":
      		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
		QotdEdit($qid,$page);
		echo "<br />";
      		break;
   
   	default:
      		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
      		QotdAdmin();
		echo "<br />";
      		break;
}
include("admin_footer.php");
?>