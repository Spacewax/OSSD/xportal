<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
# ORIGINAL FILE INFO
#
# Filename: contact.php
# Author: John Hoke
# Purpose: Cheap and dirty User Comment emailer. This script passes the form data
#          to contactmailer.php for processing and mailing.	   
# email: admin@slapd.net
#
################################################################################

include("header.php");
if(!isset($send) || $send ==""){
if($xoopsConfig['startpage'] == "contact"){
	$xoopsOption['show_rblock'] =1;
	include($xoopsConfig['root_path']."header.php");
	make_cblock();
	echo "<br />";
}else{
	$xoopsOption['show_rblock'] =0;
	include($xoopsConfig['root_path']."header.php");
}
?>
<SCRIPT LANGUAGE="JavaScript">
<!--
function CheckForm(form)
{
	if ( form.usersName.value == "" ){
			alert( "<?php echo _CT_ENTERNAME;?>" )
			form.usersName.focus()
			return false
	}
	else if ( form.usersEmail.value ==""){
			alert( "<?php echo _CT_ENTEREMAIL;?>" )
			form.usersEmail.focus()
			return false
	}
	else if ( form.usersComments.value == "" ){
			alert( "<?php echo _CT_ENTERCOMMENT;?>" )
			form.usersComments.focus()
			return false
	}
	else {
		document.Comment.submit()
		return true
	}
}
//--->

</script>

<?php 
OpenTable();   
?>
<center>
<b><?php echo _CT_CONTACTFORM; ?></b>
<form name="Comment" action="<?php echo $PHP_SELF;?>" method="post">
<table align="center" width="100%">
<tr>
 <td align="right"><b>*&nbsp;<?php echo _CT_NAME; ?></b></td>
 <td><input name="usersName" size="28" value="<?php if($xoopsUser) echo $xoopsUser->uname(); ?>"></input></td>
</tr>
<tr>
 <td align="right"><b>*&nbsp;<?php echo _CT_EMAIL; ?></b></td>
 <td><input name="usersEmail" size="28" value="<?php if($xoopsUser)echo $xoopsUser->email(); ?>"></input></td>
</tr>
<tr>
 <td align="right"><b><?php echo _CT_URL; ?></b></td>
 <td><input name="usersSite" size="28" value="<?php if($xoopsUser) echo $xoopsUser->url(); ?>"></input></td>
</tr>
<tr>
 <td align="right"><b><?php echo _CT_ICQ; ?></b></td>
 <td><input name="usersICQ" size="28" value="<?php if($xoopsUser) echo $xoopsUser->user_icq(); ?>"></input></td>
</tr>
<tr>
 <td align="right"><b><?php echo _CT_COMPANY; ?></b></td>
 <td><input name="usersCompanyName" size="28" value=""></input></td>
</tr>
<tr>
 <td align="right"><b><?php echo _CT_LOCATION; ?></b></td>
 <td><input name="usersCompanyLocation" size="28" value="<?php if($xoopsUser)echo $xoopsUser->user_from(); ?>"></input></td>
</tr>
<tr>
 <td align="right" valign="top"><b>*&nbsp;<?php echo _CT_COMMENTS; ?></b></td>
 <td><textarea name="usersComments" cols="28" rows="7" wrap="soft"></textarea></td>
</tr>
<tr>
 <td></td><td align="left"><input type="hidden" name="send" value="1"><input type="button" value="<?php echo _CT_SUBMIT; ?>" onClick="CheckForm(this.form)"></input></td>
</tr>
</table>

</form>
<?php 
echo _CT_COMPULSARY;

?>
</center>
<?php CloseTable();
include($xoopsConfig['root_path']."footer.php");
}else{

if(get_magic_quotes_gpc()){
	$usersEmail = stripslashes($usersEmail);
	$usersCompanyName = stripslashes($usersCompanyName);
	$usersCompanyLocation = stripslashes($usersCompanyLocation);
	$usersComments = stripslashes($usersComments);
	$usersName = stripslashes($usersName);
}

$AdminMessage = sprintf(_CT_SUBMITTED,$usersName);
$AdminMessage .= "\n";
$AdminMessage .= ""._CT_EMAIL." $usersEmail\n";
if ($usersSite != "")
	$AdminMessage .= ""._CT_URL." $usersSite\n";
if ($usersICQ != "")
	$AdminMessage .= ""._CT_ICQ." $usersICQ\n";
if ($usersCompanyName != "")
	$AdminMessage .= _CT_COMPANY. " $usersCompanyName\n";
if ($usersCompanyLocation != "")
	$AdminMessage .= _CT_LOCATION." $usersCompanyLocation\n";
$AdminMessage .= _CT_COMMENTS."\n";
$AdminMessage .= "\n$usersComments\n";
$AdminMessage .= "\n$HTTP_USER_AGENT\n";
$subject = $xoopsConfig['sitename']." - "._CT_CONTACTFORM;
mail($xoopsConfig['adminmail'], $subject, $AdminMessage, "From: $usersEmail");
$messagesent = sprintf(_CT_MESSAGESENT,$xoopsConfig['sitename'])."<br>"._CT_THANKYOU."";
// uncomment the following lines if you want to send confirmation mail to the user
/*
$conf_subject = _CT_THANKYOU;
$UserMessage = sprintf(_CT_HELLO,$usersName);
$UserMessage .= "\n\n";
$UserMessage .= sprintf(_CT_THANKYOUCOMMENTS,$xoopsConfig['sitename']);
$UserMessage .= "\n";
$UserMessage .= sprintf(_CT_SENTTOWEBMASTER,$xoopsConfig['sitename']);
$UserMessage .= "\n";
$UserMessage .= _CT_YOURMESSAGE."\n";
$UserMessage .= "\n$usersComments\n\n";
$UserMessage .= "--------------\n";
$UserMessage .= "".$xoopsConfig['sitename']." "._CT_WEBMASTER."\n";
$UserMessage .= "".$xoopsConfig['adminmail']."";
mail($usersEmail, $conf_subject, $UserMessage, "From: ".$xoopsConfig['adminmail']."");
$messagesent .= sprintf(_CT_SENTASCONFIRM,$usersEmail);
*/
redirect_header($xoopsConfig['xoops_url']."/index.php",2,$messagesent);

}


?>