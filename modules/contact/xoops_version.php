<?php
$modversion['name'] = _MI_CONTACT_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_CONTACT_DESC;
$modversion['credits'] = "";
$modversion['author'] = "";
$modversion['help'] = "top.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "contact_slogo.jpg";
$modversion['dirname'] = "contact";

//Admin things
$modversion['hasAdmin'] = 0;
$modversion['adminpath'] = "";

// Menu
$modversion['hasMain'] = 1;
?>