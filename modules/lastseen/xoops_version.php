<?php
$modversion['name'] = _MI_LASTSEEN_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_LASTSEEN_DESC;
$modversion['author'] = "";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "lastseen_slogo.jpg";
$modversion['dirname'] = "lastseen";

// Admin things
$modversion['hasAdmin'] = 0;
$modversion['adminpath'] = "";

// Blocks
$modversion['blocks'][1]['file'] = "lastseen.php";
$modversion['blocks'][1]['name'] = _MI_LASTSEEN_BNAME;
$modversion['blocks'][1]['description'] = "Shows last seen users";
$modversion['blocks'][1]['show_func'] = "b_lastseen_show";
$modversion['blocks'][1]['edit_func'] = "b_lastseen_edit";
$modversion['blocks'][1]['options'] = "10|20";

// Menu
$modversion['hasMain'] = 0;
?>