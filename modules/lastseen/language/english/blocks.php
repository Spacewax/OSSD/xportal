<?php
// Blocks
define("_MB_LASTSEEN_TITLE","Last Seen");
define("_MB_LASTSEEN_DAYS","%s days");
define("_MB_LASTSEEN_1DAY","1 day");
define("_MB_LASTSEEN_HRS"," %s hours");
define("_MB_LASTSEEN_1HR"," 1 hour");
define("_MB_LASTSEEN_MINS"," %s minutes");
define("_MB_LASTSEEN_1MIN"," 1 minute");
define("_MB_LASTSEEN_SCNDS"," %s seconds");
define("_MB_LASTSEEN_AGO"," ago");
define("_MB_LASTSEEN_MDAYS","Max number of days");
define("_MB_LASTSEEN_MMEM","Max number of members");
define("_MB_LASTSEEN_DAYS2","days");
define("_MB_LASTSEEN_MEMS","members");
?>