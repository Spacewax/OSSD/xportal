<?php
// Module Info

// The name of this module
define("_MI_LASTSEEN_NAME","Last Seen");

// A brief description of this module
define("_MI_LASTSEEN_DESC","Displayes last visited users in a block");

// Names of blocks for this module (Not all module has blocks)
define("_MI_LASTSEEN_BNAME","Last Seen Block");
?>