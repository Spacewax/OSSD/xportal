<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
function b_lastseen_show($options) {
	global $xoopsConfig, $xoopsDB, $xoopsUser;
	$block = array();
	$block['title'] = _MB_LASTSEEN_TITLE;
	$myID = "";
	$block['content'] = "";
	$myts = new MyTextSanitizer();
	if($xoopsUser){
		$myID = $xoopsUser->uid();
	}
	$mintime = time() - ($options[0] * 86400);
	$result = $xoopsDB->query("SELECT uid, username, time FROM ".$xoopsDB->prefix("lastseen")." WHERE uid>0 AND time>".$mintime." ORDER BY time DESC",0,$options[1],0);
	while (list($uid, $uname, $time) = $xoopsDB->fetch_row($result)) {
		if ($uid != $myID) {
			$lastvisit = b_lastSeen_create($time);
			$block['content'] .= "<small><a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$uid."\">".$myts->makeTboxData4Show($uname)."</a>:&nbsp;";
			$block['content'] .= $lastvisit."</small><br />\n";
		}
	}
	return $block;
}

function b_lastseen_create($date){
	$realtime = time() - $date;
	$lastvisit ="";
	$days = "";
	$hours = "";
	$mins = "";
	//echo $realtime;
	// how many days ago?
	if ($realtime >= 86400) { // if it's been more than a day
		$days = floor($realtime / (86400));
		$realtime -= (86400 * $days);
	}
	
	// how many hours ago?
	if ($realtime >= (3600)) {
		$hours = floor($realtime / (3600));
		$realtime -= (3600*$hours);
	}
	
	// how many minutes ago?
	if ($realtime >= 60) {
		$mins = floor($realtime / 60);
		$realtime -= (60*$mins);				
	}

	// just a little precation, although I don't *think* mins will ever be 60...
	if ($mins == 60) {
		$mins = 0;
		$hours += 1;
	}
	if($days > 1){
		$lastvisit .= sprintf(_MB_LASTSEEN_DAYS,$days);
	} elseif($days == 1) {
		$lastvisit .= _MB_LASTSEEN_1DAY;
	}
	if ($hours > 0) {
		if($hours == 1){
			$lastvisit .= _MB_LASTSEEN_1HR;
		}else{
			$lastvisit .= sprintf(_MB_LASTSEEN_HRS,$hours);
		}
	}
	if ($mins > 0) {
		if($mins == 1){
			$lastvisit .= _MB_LASTSEEN_1MIN;
		}else{
			$lastvisit .= sprintf(_MB_LASTSEEN_MINS,$mins);
		}
	}
	if(!$days && !$hours && !$mins){
		$lastvisit .= sprintf(_MB_LASTSEEN_SCNDS,$realtime);
	}
			
	$lastvisit .= _MB_LASTSEEN_AGO;
	return $lastvisit;
}

function b_lastseen_edit($options) {
	$form = _MB_LASTSEEN_MDAYS."&nbsp;<input type=\"text\" name=\"options[]\" value=\"".$options[0]."\"></input>&nbsp;"._MB_LASTSEEN_DAYS2."\n";
	$form .= "<br />"._MB_LASTSEEN_MMEM."&nbsp;<input type=\"text\" name=\"options[]\" value=\"".$options[1]."\"></input>&nbsp;"._MB_LASTSEEN_MEMS."\n";
	return $form;
}
?>