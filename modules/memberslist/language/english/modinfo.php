<?php
// Module Info

// The name of this module
define("_MI_MEMBERSLIST_NAME","Members List");

// A brief description of this module
define("_MI_MEMBERSLIST_DESC","Shows a list of registered users");

// Names of blocks for this module (Not all module has blocks)
//define("_MI_","");
?>