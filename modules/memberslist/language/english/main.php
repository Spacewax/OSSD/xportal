<?php
//%%%%%%	File Name memberslist.php 	%%%%%
define("_ML_SORTBY","Sort by:");
define("_ML_ALL","All");
define("_ML_OTHER","Other");
define("_ML_WELCOMETO","Welcome to %s Members List"); // %s is your site name
define("_ML_GREETINGS","Greetings to our latest registered user:");
define("_ML_WEHAVESOFAR","We have <b>%s</b> registered users so far.");
define("_ML_CURRENTONLINE","Current Online Registered Users:");
define("_ML_SEARCH","Search");
define("_ML_RESETSEARCH","Reset search");
define("_ML_AVATAR","Avatar");
define("_ML_NICKNAME","Nickname");
define("_ML_REALNAME","Real Name");
define("_ML_REGDATE","Registered Date");
define("_ML_EMAIL","Email");
define("_ML_PM","PM");
define("_ML_URL","URL");
define("_ML_FUNCTIONS","Functions");
define("_ML_EDIT","Edit");
define("_ML_DELETE","Delete");
define("_ML_PREVIOUS","Previous Page");
define("_ML_NEXT","Next Page");
define("_ML_USERSFOUND","%s user(s) found");
define("_ML_PEGESUSERS","%s page(s), %s user(s) shown");
define("_ML_NOUSERFOUND","No Members Found for %s"); // %s is an alphabet letter
?>