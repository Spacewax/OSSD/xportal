<?php 
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

include("header.php");
function alpha() { //Creates the list of letters and makes them a link.
	global $sortby, $xoopsConfig;
	$alphabet = array (_ML_ALL, "A","B","C","D","E","F","G","H","I","J","K","L","M",
                            "N","O","P","Q","R","S","T","U","V","W","X","Y","Z",_ML_OTHER);
        $num = count($alphabet) - 1;
        echo "<div align=\"center\">[ "; // start of HTML
        $counter = 0;
        while (list(, $ltr) = each($alphabet)) {
            	echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$ltr."&amp;sortby=".$sortby."\">".$ltr."</a>";
            	if ( $counter == round($num/2) ) {
                	echo " ]\n<br />\n[ "; 
            	} elseif ( $counter != $num ) {
                	echo "&nbsp;|&nbsp;\n";
            	}
            	$counter++;
        }
        echo " ]\n</div>\n<br />\n";  // end of HTML
}


/*
function isAlpha($character) {  // Obviously to see if a character is a letter ?
    $c = Ord($character);
    return ((($c >= 64) && ($c <= 90)) || (($c >= 97) && ($c <= 122)));
}
*/


if($xoopsConfig['startpage'] == "memberslist"){
	$xoopsOption['show_rblock'] =1;
	include($xoopsConfig['root_path']."header.php");
	make_cblock();
	echo "<br />";
}else{
	$xoopsOption['show_rblock'] =0;
	include($xoopsConfig['root_path']."header.php");
}
$pagesize = 20;

if (!isset($letter)) { $letter = _ML_ALL; }
if (!isset($sortby)) { $sortby = "uid"; }
if (!isset($orderby)) { $orderby = "ASC"; }
if (!isset($page)) { $page = 1; }

/* This is the header section that displays the last registered and who's logged in and whatnot */
if (isset($query)) {
        $where = "WHERE level>0 AND (uname LIKE '%$query%' OR user_icq LIKE '%$query%' ";
        $where .= "OR user_from LIKE '%$query%' OR user_sig LIKE '%$query%' ";
        $where .= "OR user_aim LIKE '%$query%' OR user_yim LIKE '%$query%' OR user_msnm like '%$query%'";
	if ( $xoopsUser ) {
        	if ( $xoopsUser->is_admin() ) {
        		$where .= " OR email LIKE '%$query%'";
		}
        }
	$where .= ") ";
} else {
    	$where = "WHERE level>0";
}
$result = $xoopsDB->query("SELECT uid, uname FROM ".$xoopsDB->prefix("users")." $where ORDER BY uid DESC",0,1,0);
list($lastuid, $lastuser) = $xoopsDB->fetch_row($result);

echo "\n<!-- memberlist.php listMembers -->\n";
OpenTable();
echo "<div align=\"center\"><b>";
printf(_ML_WELCOMETO,$xoopsConfig['sitename']);
echo "</b><br /><br />\n";
echo _ML_GREETINGS." <a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$lastuid."\">".$lastuser."</a></div><br />\n";
if (isset($query) && trim($query) != "") {
        $where = "WHERE level>0 AND (uname LIKE '%$query%' OR user_icq LIKE '%$query%' ";
        $where .= "OR user_from LIKE '%$query%' OR user_sig LIKE '%$query%' ";
        $where .= "OR user_aim LIKE '%$query%' OR user_yim LIKE '%$query%' OR user_msnm like '%$query%'";
        if ( $xoopsUser ) {
		if ( $xoopsUser->is_admin() ) {
        		$where .= " OR email LIKE '%$query%'";
        	}
	}
	$where .= ") ";
} else {
    	$where = "WHERE level>0";
}

list($numrows) = $xoopsDB->fetch_row($xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("users")." $where"));

echo "<div align=\"center\">";
printf(_ML_WEHAVESOFAR,$numrows);
echo "</div><br /><br />";
        
echo "<table align=\"center\"><tr><td><form action=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php\" method=\"post\">";
if (isset($query)) {
    	echo "<input type=\"text\" size=\"30\" name=\"query\" value=\"$query\"></input>";
} else {
    	echo "<input type=\"text\" size=\"30\" name=\"query\"></input>";
}
echo "<input type=\"submit\" value=\""._ML_SEARCH."\"></input></form></td><td>";

echo "<form action=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php\" method=\"post\">";
echo "<input type=\"submit\" value=\"" ._ML_RESETSEARCH."\"></input>";
echo "</form></td></tr></table>";
alpha();

/* end of top memberlist section thingie */


$min = $pagesize * ($page - 1); // This is where we start our record set from
$max = $pagesize; // This is how many rows to select
        
/* All my SQL stuff. DO NOT ALTER ANYTHING UNLESS YOU KNOW WHAT YOU ARE DOING */
$count = "SELECT COUNT(uid) AS total FROM ".$xoopsDB->prefix("users")." "; // Count all the users in the db..
$select = "SELECT uid, name, uname, email, url, user_avatar, user_regdate, user_icq, user_from, user_aim, user_yim, user_msnm, user_viewemail FROM ".$xoopsDB->prefix("users")." "; //select our data
if ( ( $letter != _ML_OTHER ) AND ( $letter != _ML_ALL ) ) {  // are we listing all or "other" ?
	$where = "WHERE level>0 AND uname LIKE '".$letter."%' "; // I guess we are not.. 
} else if ( ( $letter == _ML_OTHER ) AND ( $letter != _ML_ALL ) ) { // But other is numbers ?
        $where = "WHERE level>0 AND uname REGEXP \"^\[1-9]\" "; 
} else { // or we are unknown or all..
        $where = "WHERE level>0 "; // this is to get rid of anoying "undefinied variable" message
}
$sort = "order by $sortby $orderby"; //sorty by .....

/* due to how this works, i need the total number of users per letter group, then we can hack of the ones we want to view */
if (isset($query)) {
        $where = "WHERE level>0 AND (uname LIKE '%$query%' OR user_icq LIKE '%$query%' ";
        $where .= "OR user_from LIKE '%$query%' OR user_sig LIKE '%$query%' ";
        $where .= "OR user_aim LIKE '%$query%' OR user_yim LIKE '%$query%' OR user_msnm LIKE '%$query%'";
    	if ( $xoopsUser ) {
		if ( $xoopsUser->is_admin() ) {
        		$where .= "OR email LIKE '%$query%'";
		}
        }
	$where .= ") ";
}
$count_result = $xoopsDB->query($count.$where);
list($num_rows_per_order) = $xoopsDB->fetch_row($count_result);
/* This is where we get our limit'd result set. */
$result = $xoopsDB->query($select.$where.$sort,0,$max,$min) or die($xoopsDB->error() ); // Now lets do it !!
echo "<br />";
if ( $letter != "front" ) {
	if($orderby == "ASC"){
		$orderby = "DESC";
	} else {
		$orderby = "ASC";
	}
	if(!isset($query)){
		$query = "";
	}
	echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" width=\"100%\"><tr><td bgcolor=\"".$xoopsTheme["bgcolor2"]."\">\n";
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\"><tr>\n";
	echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=user_avatar&amp;orderby=".$orderby."&amp;query=".$query."\">"._ML_AVATAR."</a></b></font></td>\n";
        echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=uname&amp;orderby=".$orderby."&amp;query=".$query."\">"._ML_NICKNAME."</a></b></font></td>\n";
        echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=name&amp;orderby=".$orderby."&amp;query=".$query."\">"._ML_REALNAME."</a></b></font></td>\n";
	echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=user_regdate&amp;orderby=".$orderby."&amp;query=".$query."\">"._ML_REGDATE."</a></b></font></td>\n";
        echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=email&amp;orderby=".$orderby."&amp;query=".$query."\">"._ML_EMAIL."</a></b></font></td>\n";
	echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b>"._ML_PM."</b></font></td>\n";
        echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=url&amp;orderby=".$orderby."&amp;query=".$query."\">"._ML_URL."</a></b></font></td>\n";
        $cols = 7;
        if($xoopsUser){
		if ( $xoopsUser->is_admin() ) {
                	$cols = 8;
                	echo "<td bgcolor=\"".$xoopsTheme['bgcolor4']."\" align=\"center\"><font color=\"".$xoopsTheme['textcolor2']."\"><b>"._ML_FUNCTIONS."</b></font></td>\n";
		}
        }
        echo "</tr>";
        $a = 0;
        $dcolor_A = $xoopsTheme['bgcolor3'];
        $dcolor_B = $xoopsTheme['bgcolor1'];

        $num_users = $xoopsDB->num_rows($result); //number of users per sorted and limit query
        if ( $num_rows_per_order > 0  ) {
                while($userinfo = $xoopsDB->fetch_array($result) ) {
			$userinfo = new XoopsUser($userinfo['uid']);
                    	$dcolor = ($a == 0 ? $dcolor_A : $dcolor_B);
                    	echo "<tr>\n";
			echo "<td bgcolor=\"".$dcolor."\" align=\"center\"><img src=\"".$xoopsConfig['xoops_url']."/images/avatar/".$userinfo->user_avatar()."\" alt=\"\" />&nbsp;</td>\n";
			echo "<td bgcolor=\"".$dcolor."\"><a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$userinfo->uid()."\">".$userinfo->uname()."</a>&nbsp;</td>\n";
                    	echo "<td bgcolor=\"".$dcolor."\">".$userinfo->name()."&nbsp;</td>\n";
			echo "<td bgcolor=\"".$dcolor."\">".formatTimeStamp($userinfo->user_regdate(),"m")."&nbsp;</td>\n";
			$showmail = 0;
			if ( $userinfo->user_viewemail() ) {
				$showmail = 1;
			} else {
				if ( $xoopsUser ) {
					if ( $xoopsUser->is_admin() ) {
						$showmail = 1;
					}
				}
				
			}
			if($showmail){
				echo "<td align=\"center\" bgcolor=\"".$dcolor."\"><a href=\"mailto:".$userinfo->email()."\"><img src=\"".$xoopsConfig['xoops_url']."/images/icons/email.gif\" border=\"0\" alt=\"";
				printf(_SENDEMAILTO,$userinfo->uname());
				echo "\" /></a></td>\n";
			}else{
				echo "<td bgcolor=\"".$dcolor."\">&nbsp;</td>\n";
			}
			echo "<td align=\"center\" bgcolor=\"".$dcolor."\">";
			if($xoopsUser){
				echo "<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?send2=1&theme=".$xoopsTheme['thename']."&to_userid=".$userinfo->uid()."','pmlite',360,300);\">";
				echo "<img src=\"".$xoopsConfig['xoops_url']."/images/icons/pm.gif\" border=\"0\" alt=\"";
				printf(_SENDPMTO,$userinfo->uname());
				echo "\" /></a>";
			}else{
				echo "&nbsp;";
			}
			echo "</td>\n";
			if ( $userinfo->url() ) {
                    		echo "<td align=\"center\" bgcolor=\"".$dcolor."\"><a href=\"".$userinfo->url()."\" target=new><img src=\"".$xoopsConfig['xoops_url']."/images/icons/www.gif\" border=\"0\" alt=\""._VISITWEBSITE."\" /></a></td>\n";
			} else {
				echo "<td bgcolor=\"".$dcolor."\">&nbsp;</td>\n";
			}
                    	if ( $xoopsUser ) {
				if ( $xoopsUser->is_admin() ) {
                        		echo "<td bgcolor=\"".$dcolor."\" align=\"center\">[ <a href=\"".$xoopsConfig['xoops_url']."/modules/system/admin.php?fct=users&amp;chng_uid=".$userinfo->uid()."&amp;op=modifyUser\">"._ML_EDIT."</a> | \n";
                        		echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/system/admin.php?fct=users&amp;op=delUser&amp;chng_uid=".$userinfo->uid()."\">"._ML_DELETE."</a> ]</td>\n";
				}
                    	}
                    	echo "</tr>";
                    	$a = ($dcolor == $dcolor_A ? 1 : 0);
                } // end while ()
		echo "</table></td></tr></table>";
                // start of next/prev/row links.
		echo "<br /><br />";
		OpenTable();
                echo "\n<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tr>";
                
                if ( $num_rows_per_order > $pagesize ) { 
                    	$total_pages = ceil($num_rows_per_order / $pagesize); // How many pages are we dealing with here ??
                    	$prev_page = $page - 1;
                    
                    	if ( $prev_page > 0 ) {
                        	echo "<td align=\"left\" width=\"15%\"><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=".$sortby."&amp;page=".$prev_page."\">";
                        	echo "<<($prev_page)</a></td>";
                    	} else { 
                        	echo "<td width=\"15%\">&nbsp;</td>\n"; 
                    	}
                
                    	echo "<td align=\"center\" width=\"70%\">";
			printf(_ML_USERSFOUND,$num_rows_per_order,$letter);
			echo " (";
                    	printf(_ML_PEGESUSERS,$total_pages,$num_users);
                    	echo ")</td>";

                    	$next_page = $page + 1;
                    	if ( $next_page <= $total_pages ) {
                        	echo "<td align=\"right\" width=\"15%\"><a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=".$sortby."&amp;page=".$next_page."\">";
                        	echo "(".$next_page.")>></a></td>";
                    	} else {
                        	echo "<td width=\"15%\">&nbsp;</td>\n"; 
                    	}
    /* Added a numbered page list, only shows up to 50 pages. */
                    
                        echo "</tr><tr><td colspan=\"3\" align=\"center\">";
                        echo " <small>[ </small>";
                        
                        for($n=1; $n < $total_pages; $n++) {
                            	if ($n == $page) {
					echo "<small><b>$n</b></small></a>";
                            	} else {
					echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=".$sortby."&amp;page=".$n."\">";
					echo "<small>$n</small></a>";
			    	}
                            	if($n >= 50) {  // if more than 50 pages are required, break it at 50.
                                	$break = true; 
                                	break;
                            	} else {  // guess not.
                                	echo "<small> | </small>"; 
                            	}
                        }
                        
                        if(!isset($break)) { // are we sopposed to break ?
			    	if ($n == $page) {
                        		echo "<small><b>$n</b></small></a>";
			    	} else {
                        		echo "<a href=\"".$xoopsConfig['xoops_url']."/modules/memberslist/index.php?letter=".$letter."&amp;sortby=".$sortby."&amp;page=".$total_pages."\">";
                        		echo "<small>$n</small></a>";
			    	}
                        }
                        echo " <small>]</small> ";
                        echo "</td></tr>";

    /* This is where it ends */
                }else{  // or we dont have any users..
                    	echo "<td align=\"center\">";
			printf(_ML_USERSFOUND,$num_rows_per_order);
                    	echo "</td></tr>";
                    
		}
                
                echo "</table>\n";
		CloseTable();
                //echo "</td></tr>\n";

                // end of next/prev/row links
                
	} else { // you have no members on this letter, hahaha
                echo "<tr><td bgcolor=\"".$dcolor_A."\" colspan=\"".$cols."\" align=\"center\"><br />\n";
		echo "<b>";
		printf(_ML_NOUSERFOUND,$letter);
                echo "</b>\n";
                echo "<br /></td></tr>\n";
            	echo "</table></td></tr></table><br />\n";
        }
}
CloseTable();
include($xoopsConfig['root_path']."footer.php");
?>