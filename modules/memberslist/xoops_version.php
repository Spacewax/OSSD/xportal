<?php
$modversion['name'] = _MI_MEMBERSLIST_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_MEMBERSLIST_DESC;
$modversion['credits'] = "Modified by The XOOPS Project";
$modversion['author'] = "Francisco Burzi<br />( http://www.phpnuke.org/ )";
$modversion['help'] = "memberslist.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = "yes";
$modversion['image'] = "memberslist_slogo.jpg";
$modversion['dirname'] = "memberslist";

// Admin things
$modversion['hasAdmin'] = 0;
$modversion['adminpath'] = "";

// Menu
$modversion['hasMain'] = 1;
?>