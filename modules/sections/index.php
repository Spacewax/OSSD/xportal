<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("header.php");

function listsections() {
    	global $xoopsConfig, $xoopsDB, $xoopsUser;
 	if($xoopsConfig['startpage'] == "sections"){
		$xoopsOption['show_rblock'] =1;
		include($xoopsConfig['root_path']."header.php");
		make_cblock();
		echo "<br />";
	}else{
		$xoopsOption['show_rblock'] =0;
		include($xoopsConfig['root_path']."header.php");
	}
    	OpenTable();
	$myts = new MyTextSanitizer;
        $result = $xoopsDB->query("select secid, secname, image from ".$xoopsDB->prefix("sections")." order by secname");
        echo "<center>";
        printf(_MD_WELCOMETOSEC,$xoopsConfig['sitename']);
	echo "<br><br>";
        echo _MD_HEREUCANFIND."<br><br><table border=0>";
        $count = 0;
        while (list($secid, $secname, $image) = $xoopsDB->fetch_row($result)){
		$secname = $myts->makeTboxData4Show($secname);
		$image = $myts->makeTboxData4Show($image);
            	if ($count==2) {
                	echo "<tr>";
                	$count = 0;
            	}
            	echo "<td><a href=index.php?op=listarticles&secid=$secid><img src=\"images/".$image."\" border=\"0\" Alt=\"".$secname."\"></a>";
            	$count++;
            	if ($count==2) {
                	echo "</tr>";
            	}
            	echo "</td>";
        }
        echo "</table></center>";
    	CloseTable();
    	include ("../../footer.php");
}

function listarticles($secid) {
    	global $xoopsConfig, $xoopsUser, $xoopsDB;
	include ("../../header.php");
    	OpenTable();
	$myts = new MyTextSanitizer;
        $result = $xoopsDB->query("select secname, image from ".$xoopsDB->prefix("sections")." where secid=$secid");
        list($secname, $image) = $xoopsDB->fetch_row($result);
	$secname = $myts->makeTboxData4Show($secname);
	$image = $myts->makeTboxData4Show($image);
        $result = $xoopsDB->query("select artid, secid, title, content, counter from ".$xoopsDB->prefix("seccont")." where secid=$secid");
        echo "<center><img src=\"images/".$image."\" border=\"0\"><br><br>";
        printf(_MD_THISISSECTION,$secname);
	echo "<br>"._MD_THEFOLLOWING."<br><br><table border=0>";
        while (list($artid, $secid, $title, $content, $counter) = $xoopsDB->fetch_row($result)) {
		$title = $myts->makeTboxData4Show($title);
		$content = $myts->makeTareaData4Show($content);
            	echo "<tr><td align=left nowrap><font size=2>
            	&nbsp;&nbsp;<strong><big>&middot;</big></strong>&nbsp;<a href=\"index.php?op=viewarticle&artid=$artid\">$title</a> ";
		printf("(read: %s times)",$counter);
            	echo "<a href=\"index.php?op=printpage&artid=$artid\"><img src=\"".$xoopsConfig['xoops_url']."/images/print.gif\" border=0 Alt=\"" . _MD_PRINTERPAGE."\" width=15 height=11\"></a>
            	</td></tr>";
        }
        echo "</table><br><br><br>
        [ <a href=index.php>"._MD_RETURN2INDEX."</a> ]
        </center>";
    	CloseTable();
    	include ("../../footer.php");
}

function viewarticle($artid,$page) {
    	global $xoopsConfig, $xoopsUser, $xoopsDB;
	include("../../header.php");
    	OpenTable();
	$myts = new MyTextSanitizer;
        $xoopsDB->query("update ".$xoopsDB->prefix("seccont")." set counter=counter+1 where artid='$artid'");
	$result = $xoopsDB->query("select artid, secid, title, content, counter from ".$xoopsDB->prefix("seccont")." where artid=$artid");
        list($artid, $secid, $title, $content, $counter) = $xoopsDB->fetch_row($result);
	$title = $myts->makeTboxData4Show($title);
	$content = $myts->makeTareaData4Show($content);
        $result2 = $xoopsDB->query("select secid, secname from ".$xoopsDB->prefix("sections")." where secid=$secid");
        list($secid, $secname) = $xoopsDB->fetch_row($result2);
	$secname = $myts->makeTboxData4Show($secname);
        $words = sizeof(explode(" ", $content));
        //echo "<center>";
       	/* Rip the article into pages. Delimiter string is "[pagebreak]"  */
	$contentpages = explode( "[pagebreak]", $content );
	$pageno = count($contentpages);
	/* Define the current page	*/
	if ( $page=="" || $page < 1 ) {
	 	$page = 1;
	}
	if ( $page > $pageno ) {
	  	$page = $pageno;
	}
	$arrayelement = (int)$page;
	$arrayelement --;
	echo "<table width=\"100%\"><tr><td><font size=3>
        <b>$title</b></font><br>
        <font size=2><br>";
	if($page >= $pageno) {
	  	$next_page = "<a href=\"index.php\">" ._MD_RETURN2INDEX."</a>";
    	} else {
	  	$next_pagenumber = $page + 1;
	  	$next_page = "<a href=\"index.php?op=viewarticle&artid=$artid&page=$next_pagenumber\">"._MD_NEXTPAGE." ".sprintf("(%s/%s)",$next_pagenumber,$pageno)." >></a>";
    	}

    	if($page <= 1) {
	  	$previous_page = "<a href=\"/\">" ._MD_RETURN2INDEX."</a>";
    	} else {
	  	$previous_pagenumber = $page -1;
   	  	$previous_page = "<a href=\"index.php?op=viewarticle&artid=$artid&page=$previous_pagenumber\"><< "._MD_PREVPAGE." ".sprintf("(%s/%s)",$previous_pagenumber,$pageno)."</a>";
    	}
        echo ($contentpages[$arrayelement]);
	echo "<br><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>
        <td><font size=\"2\">$previous_page</font></td>
        <td><div align=\"right\"><font size=\"2\">$next_page</font></div></td>
        </tr></table>" ;
        echo "</td></tr>
        <tr><td align=center>
        [ <a href=index.php?op=listarticles&secid=$secid>".sprintf(_MD_BACK2SEC,$secname)."</a> |
        <a href=index.php>"._MD_RETURN2INDEX."</a> | <a href=\"index.php?op=printpage&artid=$artid\"><img src=\"".$xoopsConfig['xoops_url']."/images/print.gif\" border=0 Alt=\"" . _MD_PRINTERPAGE."\" width=15 height=11\"></a>
]
        </font></td></tr></table>";
    	CloseTable();
    	//echo "</center>";
    	include ("../../footer.php");
}

function PrintSecPage($artid) {
    	global $xoopsConfig, $xoopsUser, $xoopsDB;
	$myts = new MyTextSanitizer;
        $result=$xoopsDB->query("select title, content from ".$xoopsDB->prefix("seccont")." where artid=$artid");
        list($title, $content) = $xoopsDB->fetch_row($result);
	$title = $myts->makeTboxData4Show($title);
	$content = $myts->makeTareaData4Show($content);
        echo "
        <html>
        <head><title>".$xoopsConfig['sitename']."</title></head>
        <body bgcolor=FFFFFF text=000000>
        <table border=0><tr><td>
        <table border=0 width=640 cellpadding=0 cellspacing=1 bgcolor=000000><tr><td>
        <table border=0 width=640 cellpadding=20 cellspacing=1 bgcolor=FFFFFF><tr><td>
        <center>
        <img src=\"".$xoopsConfig['xoops_url']."/images/logo.gif\" border=\"0\" alt=\"\" /><br /><br />
        <font face=".$xoopsConfig['site_font']." size=+2>
        <b>$title</b><br>
        </center><font size=2>
        ".str_replace("[pagebreak]","",$content)."<br><br>";
        echo "</td></tr></table></td></tr></table>";
        echo "
        <br><br><center>
        <font face=".$xoopsConfig['site_font']." size=2>";
        printf(_MD_COMESFROM,$xoopsConfig['sitename']);
	echo "<br><a href=".$xoopsConfig['xoops_url'].">".$xoopsConfig['xoops_url']."</a><br><br>";
        echo _MD_URLFORTHIS."<br>
        <a href=\"".$xoopsConfig['xoops_url']."/modules/sections/index.php?op=viewarticle&artid=$artid\">".$xoopsConfig['xoops_url']."/modules/sections/index.php?op=viewarticle&artid=$artid</a>
        </td></tr></table>
        </body>
        </html>";
}

switch($op) {
    	case "viewarticle":
    		viewarticle($artid,$page);
    		break;
    	case "listarticles":
    		listarticles($secid);
    		break;
    	case "printpage":
    		PrintSecPage($artid);
    		break;
    	default:
    		listsections();
    		break;
}
?>