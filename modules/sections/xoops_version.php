<?php
$modversion['name'] = _MI_SECTIONS_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_SECTIONS_DESC;
$modversion['credits'] = "The XOOPS Project";
$modversion['author'] = "Francisco Burzi<br />( http://www.phpnuke.org/ )";
$modversion['help'] = "sections.html";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "sections_slogo.jpg";
$modversion['dirname'] = "sections";

// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminpath'] = "admin/index.php";

// Menu
$modversion['hasMain'] = 1;
?>