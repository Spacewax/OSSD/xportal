<?php
include("../../mainfile.php");
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");
include_once($xoopsConfig['root_path']."class/xoopsgroup.php");
$xoopsModule = XoopsModule::getByDirname("sections");
if ( !$xoopsModule ) {
	redirect_header($xoopsConfig['xoops_url']."/",2,_MODULENOEXIST);
	exit();
}
if ( $xoopsUser ) {
	if ( !XoopsGroup::hasAccessRight($xoopsModule->mid(), $xoopsUser->groups()) ) {
		redirect_header($xoopsConfig['xoops_url']."/",2,_NOPERM);
		exit();
	}
} else {
	if ( !XoopsGroup::hasAccessRight($xoopsModule->mid(), 0) ) {
		redirect_header($xoopsConfig['xoops_url']."/",2,_NOPERM);
		exit();
	}
}
include_once ($xoopsConfig['root_path']."class/module.textsanitizer.php"); 
if(file_exists("language/".$xoopsConfig['language']."/main.php")){
	include("language/".$xoopsConfig['language']."/main.php");
}else{
	include("language/english/main.php");
}
?>