<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

include("admin_header.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

/*********************************************************/
/* Sections Manager Functions                            */
/*********************************************************/

function sections() {
    global $xoopsConfig, $xoopsDB, $xoopsModule;
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	$result = $xoopsDB->query("select secid, secname from ".$xoopsDB->prefix(sections)." order by secid");
	if ($xoopsDB->num_rows($result) > 0) {
		$myts= new MyTextSanitizer;
		OpenTable();
		echo "
		<b><center>"._MD_CURACTIVESEC."</b><br />"._MD_CLICK2EDIT."<br />
		<table border=0 width=100% align=center cellpadding=1><tr><td align=center>";
		echo "<ul>";
		while(list($secid, $secname) = $xoopsDB->fetch_row($result)) {
			$secname=$myts->makeTboxData4Show($secname);
			echo "<li><a href=\"index.php?op=sectionedit&secid=".$secid."\">".$secname."</a></li>";
		}
		echo "</ul>";
		echo "</td></tr></table>";
		CloseTable();
	?>
		<br />
<?php 
		OpenTable(); 
?>
		<h4><?php echo _MD_ADDARTICLE; ?></h4>
		<br /><br />
	<?php echo "<form action=\"index.php\" method=\"post\">"; ?><br />
	<b><?php echo _MD_TITLEC; ?></b><br />
	<input class=textbox type="text" name="title" size=60 value=""><br /><br />
	<?php
	$result = $xoopsDB->query("select secid, secname from ".$xoopsDB->prefix(sections)." order by secid");
	while(list($secid, $secname) = $xoopsDB->fetch_row($result)) {
		$secname=$myts->makeTboxData4Show($secname);
	echo "<input type=radio name=secid value=$secid>$secname<br />";
	} ?>
	<br />
	<b><?php echo _MD_CONTENTC; ?></b><br />
	<textarea class=textbox name="content" cols=60 rows=10></textarea><br /><br />
	<input type=hidden name=op value=secarticleadd>
	<input type="submit" value="<?php echo _MD_DOADDARTICLE; ?>">
	</form>
<?php
	CloseTable();
	echo "<br />";
	OpenTable(); ?>
	<h4><?php echo _MD_LAST20ART; ?></h4>
	<br /><br />
	<ul>
	<?php
	$result = $xoopsDB->query("select artid, secid, title from ".$xoopsDB->prefix(seccont)." order by artid desc",1,20,0);
	while ( list($artid, $secid, $title) = $xoopsDB->fetch_row($result) ) {
		$title = $myts->makeTboxData4Show($title);
	    $result2 = $xoopsDB->query("select secid, secname from ".$xoopsDB->prefix(sections)." where secid='$secid'");
	    list($secid, $secname) = $xoopsDB->fetch_row($result2);
		$secname = $myts->makeTboxData4Show($secname);
	    echo "<li>$title ($secname) [ <a href=index.php?op=secartedit&artid=$artid>"._MD_EDIT."</a> ]";
	} ?>
	</ul>
	<?php echo "<form action=\"index.php\" method=\"post\">"; ?>
	<?php echo _MD_EDITARTID; ?> <input class=textbox type="text" NAME="artid" SIZE=10>
	<input type=hidden name=op value="secartedit">
	<input type="submit" value="<?php echo _MD_GO;?>">
	</form>
<?php 
	CloseTable();
	}
	echo "<br />";
	OpenTable(); ?>
	<h4><?php echo _MD_ADDNEWSEC; ?></h4>
	<br /><br />
	<?php echo "<form action=\"index.php\" method=\"post\">"; ?><br />
	<b><?php echo _MD_SECNAMEC; ?></b>  <?php echo _MD_MAXCHAR; ?><br />
	<input class=textbox type="text" name="secname" size=40 maxlength=40><br /><br />
	<b><?php echo _MD_SECIMAGEC; ?></b>&nbsp;<?php echo _MD_EXIMAGE; ?><br />
	<input class=textbox type="text" name="image" size=40 maxlength=50><br /><br />
	<input type=hidden name=op value=sectionmake>
	<input type="submit" value="<?php echo _MD_GOADDSECTION; ?>">
	</form>
<?php
	CloseTable();
}

function secarticleadd($secid, $title, $content) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$title = $myts->makeTboxData4Save($title);
	$content = $myts->makeTareaData4Save($content);
	$newid = $xoopsDB->GenID("seccont_artid_seq");
	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("seccont")." (artid, secid, title, content, counter) VALUES ($newid,$secid,'$title','$content',0)");
	redirect_header("index.php?op=sections",2,_MD_DBUPDATED);
	exit();
}

function secartedit($artid) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	$myts = new MyTextSanitizer;
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	$result = $xoopsDB->query("select artid, secid, title, content from ".$xoopsDB->prefix(seccont)." where artid='$artid'");
	list($artid, $secid, $title, $content) = $xoopsDB->fetch_row($result);
	$title = $myts->makeTboxData4Edit($title);
	$content = $myts->makeTareaData4Edit($content);
	OpenTable();
	?>
	<h4><?php echo _MD_EDITARTICLE; ?></h4>
	<br /><br />
	<?php echo "<form action=\"index.php\" method=\"post\">"; ?><br />
	<b><?php echo _MD_TITLEC; ?></b><br />
	<input class=textbox type="text" name="title" size=60 value="<?php echo "$title"; ?>"><br /><br />
	<?php
	$result2 = $xoopsDB->query("select secid, secname from ".$xoopsDB->prefix(sections)." order by secname");
	while(list($secid2, $secname) = $xoopsDB->fetch_row($result2)) {	
	$secname = $myts->makeTboxData4Show($secname);
	    if ($secid2==$secid) { $che = "checked"; }
	    echo "<input type=radio name=secid value=$secid2 $che>$secname<br />";
	    $che = "";
	} ?>
	<br />
	<b><?php echo _MD_CONTENTC; ?></b><br />
	<textarea class=textbox name=content cols=60 rows=10><?php echo "$content"; ?></textarea>
	<input type=hidden name=artid value="<?php echo "$artid"; ?>">
	<input type=hidden name=op value=secartchange>
	<table border=0><tr><td>
	<input type="submit" value="<?php echo _MD_SAVECHANGES; ?>">
	</form></td><td>
	<?php echo "<form action=\"index.php\" method=\"post\">"; ?>
	<input type=hidden name=artid value="<?php echo "$artid"; ?>">
	<input type=hidden name=op value=secartdelete>
	<input type="submit" value="<?php echo _MD_DELETE; ?>">
	</form></td></tr></table>
<?php
	CloseTable();
}

function sectionmake($secname, $image) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$secname = $myts->makeTboxData4Save($secname);
	$image = $myts->makeTboxData4Save($image);
	$newid = $xoopsDB->GenID("sections_secid_seq");
	$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("sections")." (secid, secname, image) VALUES ($newid,'$secname', '$image')");
	redirect_header("index.php?op=sections",2,_MD_DBUPDATED);
	exit();
}

function sectionedit($secid) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	include($xoopsConfig['root_path']."header.php");
	$xoopsModule->printAdminMenu();
	echo "<br />";
	$myts = new MyTextSanitizer;
	$result = $xoopsDB->query("select secid, secname, image from ".$xoopsDB->prefix(sections)." where secid=$secid");
	list($secid, $secname, $image) = $xoopsDB->fetch_row($result);
	$secname = $myts->makeTboxData4Edit($secname);
	$image = $myts->makeTboxData4Edit($image);
	$result2 = $xoopsDB->query("select artid from ".$xoopsDB->prefix(seccont)." where secid=$secid");
	$number = $xoopsDB->num_rows($result2);
	OpenTable();
	?>
	<?php echo "<img src=\"".$xoopsConfig[xoops_url]."/modules/sections/images/".$image."\" border=0><br /><br />"; ?>
	<h4><?php printf(_MD_EDITTHISSEC,$secname); ?></h4>
	<br />
     <?php
      $help = sprintf(_MD_THISSECHAS,$number);
      echo "$help";
     ?>
	<br /><br />
	<?php echo "<form action=\"index.php\" method=\"post\">"; ?><br />
	<b><?php echo _MD_SECNAMEC; ?></b> <?php echo _MD_MAXCHAR; ?><br />
	<input class=textbox type="text" name="secname" size=40 maxlength=40 value="<?php echo "$secname"; ?>"><br /><br />
	<b><?php echo _MD_SECIMAGEC; ?></b> <?php echo _MD_EXIMAGE; ?><br />
	<input class=textbox type="text" name="image" size=40 maxlength=50 value="<?php echo "$image"; ?>"><br /><br />
	<input type=hidden name=secid value="<?php echo "$secid"; ?>">
	<input type=hidden name=op value=sectionchange>
	<table border=0><tr><td>
	<INPUT type="submit" value="<?php echo _MD_SAVECHANGES; ?>">
	</form></td><td>
	<?php echo "<form action=\"index.php\" method=\"post\">"; ?>
	<input type=hidden name=secid value="<?php echo "$secid"; ?>">
	<input type=hidden name=op value=sectiondelete>
	<INPUT type="submit" value="<?php echo _MD_DELETE; ?>">
	</form></td></tr></table>
<?php
	CloseTable();
}

function sectionchange($secid, $secname, $image) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$secname = $myts->makeTboxData4Save($secname);
	$image = $myts->makeTboxData4Save($image);
	$xoopsDB->query("update ".$xoopsDB->prefix(sections)." set secname='$secname', image='$image' where secid=$secid");
	redirect_header("index.php?op=sections",2,_MD_DBUPDATED);
	exit();
}

function secartchange($artid, $secid, $title, $content) {
	global $xoopsDB;
	$myts = new MyTextSanitizer;
	$title = $myts->makeTboxData4Save($title);
	$content = $myts->makeTareaData4Save($content);
	$xoopsDB->query("update ".$xoopsDB->prefix(seccont)." set secid='$secid', title='$title', content='$content' where artid=$artid");
	redirect_header("index.php?op=sections",2,_MD_DBUPDATED);
	exit();
}

function sectiondelete($secid, $ok=0) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;

	if ( $ok == 1 ) {
	    	$xoopsDB->query("delete from ".$xoopsDB->prefix(seccont)." where secid='$secid'");
	    	$xoopsDB->query("delete from ".$xoopsDB->prefix(sections)." where secid='$secid'");
	    	redirect_header("index.php?op=sections",2,_MD_DBUPDATED);
		exit();
	} else {
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
	    	$myts = new MyTextSanitizer;
	    	$result=$xoopsDB->query("select secname from ".$xoopsDB->prefix(sections)." where secid=$secid");
	    	list($secname) = $xoopsDB->fetch_row($result);
		$secname = $myts->makeTboxData4Show($secname);
	 	OpenTable();
	    	echo "
	    	<center><font color=\"#ff0000\"><h4>".sprintf(_MD_DELETETHISSEC,$secname)."</h4></font><br />
	    	"._MD_RUSUREDELSEC."<br />
	    	"._MD_THISDELETESALL."<br /><br />
	    	[ <a href=\"index.php?op=sections\">"._MD_NO."</a> | <a href=\"index.php?op=sectiondelete&secid=$secid&ok=1\">"._MD_YES."</a> ]</center><br /><br /></center>";
		CloseTable();
	}
}

function secartdelete($artid, $ok=0) {
	global $xoopsDB, $xoopsConfig, $xoopsModule;
	if ( $ok == 1 ) {
	    	$xoopsDB->query("delete from ".$xoopsDB->prefix(seccont)." where artid='$artid'");
	    	redirect_header("index.php?op=sections",2,_MD_DBUPDATED);
		exit();
	} else {
		include($xoopsConfig['root_path']."header.php");
		$xoopsModule->printAdminMenu();
		echo "<br />";
	    	$myts = new MyTextSanitizer;
	    	$result = $xoopsDB->query("select title from ".$xoopsDB->prefix(seccont)." where artid=$artid");
	    	list($title) = $xoopsDB->fetch_row($result);
	    	$title = $myts->makeTboxData4Show($title);
	    	OpenTable();
	    	echo "
	    	<center><b>".sprintf(_MD_DELETETHISART,$title)."</b><br /><br />
	    	"._MD_RUSUREDELART."<br /><br />
	    	[ <a href=\"index.php?op=sections\">"._MD_NO."</a> | <a href=\"index.php?op=secartdelete&artid=$artid&ok=1\">"._MD_YES."</a> ]</center><br /><br /></center>";
		CloseTable();
	}
}

switch ($op) {
	        default:
			sections();
			break;
		case "sections":
			sections();
			break;
		case "sectionedit":
			sectionedit($secid);
			break;
		case "sectionmake":
			sectionmake($secname, $image);
			break;
		case "sectiondelete":
			sectiondelete($secid, $ok);
			break;
		case "sectionchange":
			sectionchange($secid, $secname, $image);
			break;
		case "secarticleadd":
			secarticleadd($secid, $title, $content);
			break;
		case "secartedit":
			secartedit($artid);
			break;
		case "secartchange":
			secartchange($artid, $secid, $title, $content);
			break;
		case "secartdelete":
			secartdelete($artid, $ok);
			break;
}
include("admin_footer.php");
?>