<?php
// Module Info

// The name of this module
define("_MI_SECTIONS_NAME","Sections");

// A brief description of this module
define("_MI_SECTIONS_DESC","Creates a section where admins can post various articles.");

// Names of blocks for this module (Not all module has blocks)
//define("_MI_","");
?>