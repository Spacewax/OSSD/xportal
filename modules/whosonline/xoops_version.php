<?php
$modversion['name'] = _MI_WHOSONLINE_NAME;
$modversion['version'] = 1.00;
$modversion['description'] = _MI_WHOSONLINE_DESC;
$modversion['author'] = "";
$modversion['credits'] = "The XOOPS Project";
$modversion['help'] = "";
$modversion['license'] = "GPL see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "whosonline_slogo.jpg";
$modversion['dirname'] = "whosonline";

// Admin things
$modversion['hasAdmin'] = 0;
$modversion['adminpath'] = "";

// Blocks
$modversion['blocks'][1]['file'] = "whosonline.php";
$modversion['blocks'][1]['name'] = _MI_WHOSONLINE_BNAME1;
$modversion['blocks'][1]['description'] = "Shows users that are currently online";
$modversion['blocks'][1]['show_func'] = "b_whosonline_show";

// Menu
$modversion['hasMain'] = 0;
?>