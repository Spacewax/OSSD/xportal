<?php
function b_whosonline_show() {
        global $xoopsDB, $xoopsConfig, $xoopsTheme, $xoopsUser;
	$block = array();
	$block['title'] = _MB_WHOSONLINE_TITLE1;
	$myts = new MyTextSanitizer();
        $result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("lastseen")." WHERE uid=0 AND online=1");
        list($guest_online_num) = $xoopsDB->fetch_row($result);
        $result = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("lastseen")." WHERE uid>0 AND online=1");
        list($member_online_num) = $xoopsDB->fetch_row($result);

        $who_online_num = $guest_online_num + $member_online_num;
        $block['content'] = "<div align=\"center\">";
	$block['content'] .= sprintf(_MB_WHOSONLINE_THERE,$guest_online_num,$member_online_num);
	$block['content'] .= "<br /><br />";
	if ($member_online_num > 3) {
		$result = $xoopsDB->query("SELECT uid, username FROM ".$xoopsDB->prefix("lastseen")." WHERE uid > 0 AND online=1",0,3,0);
		while(list($memuid, $memusername) = $xoopsDB->fetch_row($result)) {
                	$block['content'] .="<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=$memuid\">".$myts->makeTboxData4Show($memusername)."</a>, ";
		}
                $block['content'] .="<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/misc.php?action=showpopups&amp;type=whosonline','Online',220,350);\">"._MB_WHOSONLINE_MORE."</a><br />";
	} elseif ($member_online_num != 0) {
        	$first = 0;
                $result = $xoopsDB->query("SELECT uid, username FROM ".$xoopsDB->prefix("lastseen")." WHERE uid>0 AND online=1");
                while(list($memuid, $memusername) = $xoopsDB->fetch_row($result)) {
                	if ($first!=0) { $block['content'] .=", "; }
			$block['content'] .="<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=$memuid\">".$myts->makeTboxData4Show($memusername)."</a>";
                        $first= 1;
		}
                $block['content'] .="<br />";
	}
        $block['content'] .= "<br />";
	if($xoopsUser){
		$block['content'] .= sprintf(_MB_WHOSONLINE_URLAS,$xoopsUser->uname);
		$block['content'] .="<br />";
	}else{
                $block['content'] .= _MB_WHOSONLINE_URAU;
                $block['content'] .= "<br /><a href=".$xoopsConfig['xoops_url']."/register.php>"._MB_WHOSONLINE_RNOW."</a><br />";
        }

        $result = $xoopsDB->query("SELECT u.uid, u.uname, u.email, u.user_viewemail, g.name AS groupname FROM ".$xoopsDB->prefix("groups_users_link")." l LEFT JOIN ".$xoopsDB->prefix("users")." u ON l.uid=u.uid LEFT JOIN ".$xoopsDB->prefix("groups")." g ON l.groupid=g.groupid WHERE g.type='Admin' ORDER BY l.groupid");

        if ($xoopsDB->num_rows($result) > 0) {

                $prev_cation = "";

                $block['content'] .= "<br /><br />";

                $block['content'] .= "<table width=\"92%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\" bgcolor=\"".$xoopsTheme['bgcolor2']."\"><tr><td>\n";
                $block['content'] .= "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"8\" bgcolor=\"".$xoopsTheme['bgcolor1']."\"><tr><td>\n";
		$prev_caption = "";

                while  ($userinfo = $xoopsDB->fetch_array($result)) {
                        if ($prev_caption != $userinfo['groupname']) {
                                $prev_caption = $userinfo['groupname'];
                                $block['content'] .= "<tr><td colspan=\"2\">";
                                $block['content'] .= "<small>";
                                $block['content'] .= "<b>".$myts->makeTboxData4Show($prev_caption)."</b>";
                                $block['content'] .= "</small>";
                                $block['content'] .= "</td></tr>";
                        }
			$userinfo['uname'] = $myts->makeTboxData4Show($userinfo['uname']);
                        $block['content'] .= "<tr><td width=80%>";
                        $block['content'] .= "<small>";
                        $block['content'] .= "<a href=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$userinfo['uid']."\">".$userinfo['uname']."</a>";
                        $block['content'] .= "</small>";
                        $block['content'] .= "</td><td width=20% align=right>";
                        if ($xoopsUser) {
				$block['content'] .= "<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/pmlite.php?send2=1&theme=".$xoopsTheme['thename']."&to_userid=".$userinfo['uid']."','pmlite',360,300);\">";
				$block['content'] .= "<img src=\"".$xoopsConfig['xoops_url']."/images/icons/pm_small.gif\" border=\"0\" width=\"27\" height=\"17\" alt=\"";
				$block['content'] .= sprintf(_MB_WHOSONLINE_SPMTO,$userinfo['uname']);
				$block['content'] .= "\" /></a>\n";
                        } else {
				if($userinfo['user_viewemail']){
     					$block['content'] .= "<a href=\"mailto:".$userinfo['email']."\"><img src=\"".$xoopsConfig['xoops_url']."/images/icons/em_small.gif\" border=\"0\" width=\"16\" height=\"14\" alt=\"";
					$block['content'] .= sprintf(_MB_WHOSONLINE_SEMTO,$userinfo['uname']);
					$block['content'] .= "\" /></a>\n";
				}else{
					$block['content'] .= "&nbsp;";
				}
                        }
                        $block['content'] .= "</td></tr>";

                }

                $block['content'] .= "</td></tr></table></td></tr></table>";
	}
	$block['content'] .= "</div><br /><br />";
	return $block;
}
?>