<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
$xoopsOption['pagetype'] = "pmsg";

include("mainfile.php");

if ( !$refresh && $op!="submit" ) {
	$jump= "pmlite.php?refresh=".time()."";
	if ( $send ) {
		$jump .= "&amp;send=".$send."&amp;theme=".$theme."";
	} elseif ( $send2 ) {
		$jump .= "&amp;send2=".$send2."&amp;to_userid=".$to_userid."&amp;theme=".$theme."";
	} elseif ( $reply ) {
		$jump .= "&amp;reply=".$reply."&amp;msg_id=".$msg_id."&amp;theme=".$theme."";
	} else {
	}
	echo "<html><head><meta http-equiv='Refresh' content='0; url=".$jump."' /></head><body></body></html>";
	exit();
}

include_once("class/module.textsanitizer.php");
$myts = new MyTextSanitizer;
//include_once("class/module.errorhandler.php");
//$eh = new ErrorHandler; //ErrorHandler object

if ( !isset($theme) ) {
	$theme = $xoopsConfig['default_theme'];
}
include($xoopsConfig['root_path']."themes/".$theme."/theme.php");
$css = getCss($theme);
echo "<html><head><title>";
printf(_PM_PMTITLE,$xoopsConfig['sitename']);
echo "</title>\n";
echo "<meta http-equiv='Content-Type' content='text/html; charset="._CHARSET."' />\n";
echo "<style type='text/css' media='all'><!-- @import url(".$xoopsConfig['xoops_url']."/xoops.css); --></style>\n";
if ( $css ) {
	echo "<style type='text/css' media='all'><!-- @import url($css); --></style>\n\n";
}
echo "</head>\n";
//echo "<body bgcolor=\"".$xoopsTheme['bgcolor1']."\" color=\"".$xoopsTheme['textcolor1']."\">\n";
echo "<body>\n";
if ( $xoopsUser ) {
	if ( $op == "submit" ) {
		if ( $subject == '' ) {
			$haserror = 1;
		}
		if ( $message == '' ) {
			$haserror = 1;
		}
		if ( $haserror ) {
			echo "<br /><br /><div style='text-align: center;'><h4>"._PM_PLZENTER."</h4><br />";
			echo "[ <a href='javascript:history.go(-1)'>"._PM_GOBACK."</a> ]</div>";
			exit();
		}
		$message = $myts->makeTareaData4Save($message);

		$res = $xoopsDB->query("SELECT COUNT(*) FROM ".$xoopsDB->prefix("users")." WHERE uid=".$to_userid."");
        list($count) = $xoopsDB->fetch_row($res);

        if ($count != 1) {
			//OpenTable();
            echo "<br /><br /><div style='text-align: center;'><h4>"._PM_USERNOEXIST."<br />";
			echo _PM_PLZTRYAGAIN."</h4><br />";
            echo "[ <a href='javascript:history.go(-1)'>"._PM_GOBACK."</a> ]</div>";
			//CloseTable();
		} else {
			$newid = $xoopsDB->GenID("priv_msgs_msg_id_seq");
			$sql = "INSERT INTO ".$xoopsDB->prefix("priv_msgs")." (msg_id, msg_image, subject, from_userid, to_userid, msg_time, msg_text) ";
            $sql .= "VALUES ($newid, '$image', '$subject', ".$xoopsUser->uid().", $to_userid, ".time().", '$message')";
            if ( !$result = $xoopsDB->query($sql,1) ) {
				die("ERROR");
            }
			//OpenTable();
            echo "<br /><br /><div style='text-align: center;'><h4>"._PM_MESSAGEPOSTED."</h4><br /><a href=\"javascript:window.opener.location='".$xoopsConfig['xoops_url']."/viewpmsg.php';window.close();\">"._PM_CLICKHERE."</a><br /><br /><a href=\"javascript:window.close();\">"._PM_ORCLOSEWINDOW."</a></div>";
			//CloseTable();
		}
	} elseif ($reply || $send || $send2) {
		if ($reply) {
			$sql = "SELECT u.uname, p.msg_image, p.subject, p.from_userid, p.to_userid, p.msg_text FROM ".$xoopsDB->prefix("priv_msgs")." p, ".$xoopsDB->prefix("users")." u WHERE p.msg_id = $msg_id AND p.from_userid = u.uid";
			//echo $sql;
        	$result = $xoopsDB->query($sql,1);
        	if (!$result) {
				die("ERROR");
			}
			$row = $xoopsDB->fetch_array($result);
        	if (!$row) {
				die("ERROR");
        	}
        	if ($xoopsUser->uid() != $row['to_userid']) {
				die("ERROR");
        	}
			$row['subject'] = $myts->makeTboxData4Edit($row['subject']);
			$row['msg_text'] = $myts->makeTareaData4Edit($row['msg_text']);
            $replytext = "[quote]\n";
			$replytext .= sprintf(_PM_USERWROTE,$row['uname']);
			$replytext .= "\n".$row['msg_text']."\n[/quote]";
		}
		echo "<form action='pmlite.php' method='post' name='coolsus'>\n"; 
		//OpenTable();
        echo "<table width='300' align='center'><tr align='left'><td bgcolor='".$xoopsTheme['bgcolor3']."' width='25%'><b>"._PM_TO."<b></td>";
		if ($reply) {
			echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><input class='textbox' type='hidden' name='to_userid' value='".$row['from_userid']."' />".$row['uname']."</td>";
		} elseif ($send2) {
			$to_username = XoopsUser::get_uname_from_id($to_userid);
            echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><input class='textbox' type='hidden' name='to_userid' value='".$to_userid."' />".$to_username."</td>";
		} else {
			echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><select name='to_userid'>";
			$result = $xoopsDB->query("SELECT uid, uname FROM ".$xoopsDB->prefix("users")." WHERE level > 0 ORDER BY uname");
            while ( list($ftouid, $ftouname) = $xoopsDB->fetch_row($result) ) {
				echo "<option value='".$ftouid."'>".$ftouname."</option>";
			}
            echo "</td>";
        }

        echo "</tr>";
        echo "<tr align='left'><td bgcolor='".$xoopsTheme['bgcolor3']."' width='25%'><b>"._PM_SUBJECTC."<b></td>";
		if ( $reply ) {
			echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><input class='textbox' type='text' name='subject' value='Re: ".$row['subject']."' size='30' maxlength='100' /></td>";
		} else {
			echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><input class='textbox' type='text' name='subject' size='30' maxlength='100' /></td>";
		}

		echo "</tr>";
        echo "<tr align='left' valign='top'><td bgcolor='".$xoopsTheme['bgcolor3']."' width='25%'><b>"._PM_MESSAGEC."</b><br /><br /></td>";
		echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'><textarea class='textbox' name='message' rows='8' cols='30' wrap='virtual'>";
		if ($reply) {
			echo $replytext;
		}
		echo "</textarea><br /></td>";
		echo "</tr>";
        echo "<tr align='left'><td bgcolor='".$xoopsTheme['bgcolor3']."' width='25%'><b>"._PM_OPTIONSC."</b></td>";
		echo "<td bgcolor='".$xoopsTheme['bgcolor1']."'>";
		echo "<input type='checkbox' name='nohtml' />"._PM_DISABLEHTML."<br />";
		echo "<input type='checkbox' name='nosmiley' />"._PM_DISABLESMILIES."<br />";
		echo "</td></tr>\n";
        echo "<tr><td bgcolor='".$xoopsTheme['bgcolor1']."' colspan='2' align='center'>
		<input type='hidden' name='msg_id' value='".$msg_id."' />
		<input type='hidden' name='image' value='icon1.gif' />
		<input type='hidden' name='op' value='submit' />
		<input type='hidden' name='theme' value='$theme' />
		<input type='submit' name='submit' value='"._PM_SUBMIT."' />&nbsp;
		<input type='reset' value='"._PM_CLEAR."' />
		&nbsp;<input type='button' name='cancel' value='"._PM_CANCELSEND."' onclick='javascript:window.close();' />
		</td></tr></table>\n";
		//CloseTable();
	   	echo "</form>\n";
	}
} else {
	echo _PM_SORRY."<br /><br /><a href=\"".$xoopsConfig['xoops_url']."/register.php\">"._PM_REGISTERNOW."</a>.";
}

echo "</body></html>";

?>