<?php
/************************************************************/
/* Function themeindex()                                    */
/*                                                          */
/* This function format the stories on the Homepage         */
/************************************************************/

function themeindex ($poster, $time, $title, $counter, $topic, $thetext, $morelink, $topicimage, $topictext) {
 global $xoopsConfig;
 echo"<table border='0' cellpadding='3' cellspacing='5' width='100%'><tr>"
    ."<td bgcolor='#F5F5F5'><div class='indextitle'>$title</div><br /><font size='1'>"._TH_POSTEDBY." $poster "._TH_ON." $time ($counter "._TH_READS.")"
    ."<br /><b>"._TH_TOPIC."</b> <a href='index.php?storytopic=$topic'>$topictext</a><br /></td>"
    ."</tr><tr><td><a href='index.php?storytopic=$topic'>".GetImgTag($xoopsConfig['tipath'],$topicimage,$topictext,0,"right",-1,-1,10,10)."</a>$thetext<br /><br /></td></tr><tr>"
    ."<td align='right'><font size='2'>$morelink</td></tr></table>";
 }
?>