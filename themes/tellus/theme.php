<?php
/* version: SE 1.0 build 20011202 */

/* Udated 10/12/2001 Xend         */

/************************************************************/
/* Theme Name: tellus (v1.0)                             */
/* Copyright (c) 2001 MPN-se (http://www.myphpnuke-se.com)  */
/* Created by Xend  (Http;//xendtech.com)                   */
/* Last Updated: 15/11/2001                                 */
/************************************************************/

/************************************************************/
/* Theme Colors Definition                                  */
/*                                                          */
/* Define colors for your web site. $bgcolor2 is generaly   */
/* used for the tables border as you can see on OpenTable() */
/* function, $bgcolor1 is for the table background and the  */
/* other two bgcolor variables follows the same criteria.   */
/* $texcolor1 and 2 are for tables internal texts           */
/************************************************************/

 $xoopsTheme['thename'] = "tellus";
 $xoopsTheme['bgcolor1'] = "#F5F5F5";
 $xoopsTheme['bgcolor2'] = "#AAB8C4";
 $xoopsTheme['bgcolor3'] = "#E0E3E6";
 $xoopsTheme['bgcolor4'] = "#B6C6C9";
 $xoopsTheme['textcolor1'] = "#000000";
 $xoopsTheme['textcolor2'] = "#354548";

/************************************************************/
/* OpenTable Functions                                      */
/*                                                          */
/* Define the tables look&feel for you whole site. For this */
/* we have two options: OpenTable and OpenTable2 functions. */
/* Then we have CloseTable and CloseTable2 function to      */
/* properly close our tables. The difference is that        */
/* OpenTable has a 100% width and OpenTable2 has a width    */
/* according with the table content                         */
/************************************************************/

function OpenTable($width="100%") {
 global $xoopsTheme;
 echo "&nbsp;<table width='".$width."' border='0' cellspacing='1' cellpadding='0' bgcolor='".$xoopsTheme['bgcolor3']."'><tr><td valign='top'>\n";
 echo "<table width='100%' border='0' cellspacing='1' cellpadding='8' bgcolor='".$xoopsTheme['bgcolor1']."'><tr><td valign='top'>\n";
 }
function CloseTable() {
 echo "</td></tr></table></td></tr></table>\n";
 }

/************************************************************/
/* Function themeheader()                                   */
/*                                                          */
/* Control the header for your site. You need to define the */
/* BODY tag and in some part of the code call the blocks    */
/* function for left side with: blocks(left);               */
/************************************************************/

function themeheader($show_rblock){
 global $xoopsConfig;
 echo "</head><body>\n";
 echo"<center><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr height='75'>"
    ."<td class='top' width='160' height='75'><a href='".$xoopsConfig['xoops_url']."/index.php'><img src='".$xoopsConfig['xoops_url']."/themes/tellus/images/logo.gif' align='left' alt='".sprintf(_TH_WELCOMETO,$xoopsConfig['sitename'])."' /></a>";
 echo"</td>"
    ."<td class='top' width='10' height='75'>&nbsp;</td>"
    ."<td class='top' height='75'>"
    ."<center>&nbsp;";
 if ($xoopsConfig['banners']) {
 showbanner();
 }
 echo"&nbsp;</center>"
    ."</td>"
    ."<td class='top' width='10' height='75'>&nbsp;</td>"
    ."<td class='top' width='1' height='75'>&nbsp;</td>"
    ."</tr>"
    ."<tr height='10'>"
    ."<td class='topborder' width='160' height='10'>&nbsp;</td>"
    ."<td class='topborder' height='10'>&nbsp;</td>"
    ."<td class='topborder' width='10' height='10'>&nbsp;</td>"
    ."<td class='topborder' height='10'>&nbsp;</td>"
    ."<td class='topborder' height='10'>&nbsp;</td>"
    ."</tr>"
    ."</td></table>"
    ."<table border='0' cellpadding='0' cellspacing='0' width='100%' height='100%'>"
    ."<tr>"
    ."<td class='sidebar1' width='160' valign='top'>";
 make_sidebar("left");
 echo"</td>"
    ."<td class='leftmb' width='10'>&nbsp;</td>"
    ."<td class='newstd' valign='top'>";
}

/************************************************************/
/* Function themefooter()                                   */
/*                                                          */
/* Control the footer for your site. You don't need to      */
/* close BODY and HTML tags at the end. In some part call   */
/* the function for right blocks with: blocks(right);       */
/* Also, $index variable need to be global and is used to   */
/* determine if the page your're viewing is the Homepage or */
/* and internal one.                                        */
/************************************************************/

function themefooter($show_rblock, $footer){
 echo"</td>";
 if ( $show_rblock ) {
 echo"<td class='rigthmb' width='10'>&nbsp;</td>"
    ."<td class='sidebar2' width='160' valign='top'>";
 make_sidebar("right");
 echo"</td>"
    ."</tr>"
    ."<tr height='10'>"
    ."<td class='bunnmbleft' width='160' height='10'>&nbsp;</td>"
    ."<td class='leftmb' width='10' height='10'>&nbsp;</td>"
    ."<td class='bunnmb' height='10'>&nbsp;</td>"
    ."<td class='rigthmb' width='10' height='10'>&nbsp;</td>"
    ."<td class='bunnmbrigth' width='160' height='10'>&nbsp;</td>"
    ."</tr>"
    ."<tr height='60'>"
    ."<td class='bunnmfooter' width='160' height='60'>&nbsp;</td>"
    ."<td class='bunnmfooter' width='10' height='60'>&nbsp;</td>"
    ."<td class='bunnmfooter' height='60'>"
    ."<center>";
 echo $footer;
 echo"</center>"
    ."</td>"
    ."<td class='bunnmfooter' width='10' height='60'>&nbsp;</td>"
    ."<td class='bunnmfooter' width='160' height='60'>&nbsp;</td>"
    ."</tr>"
    ."</table></center>";
 }
 if ( !$show_rblock ) {
 echo"<td class='rigthmb' width='10'>&nbsp;</td>"
    ."<td class='sidebar2' width='12' valign='top'>&nbsp;";
 echo"</td>"
    ."</tr>"
    ."<tr height='10'>"
    ."<td class='bunnmbleft' width='160' height='10'>&nbsp;</td>"
    ."<td class='leftmb' width='10' height='10'>&nbsp;</td>"
    ."<td class='bunnmb' height='10'>&nbsp;</td>"
    ."<td class='rigthmb' width='10' height='10'>&nbsp;</td>"
    ."<td class='bunnmbrigth' width='12' height='10'>&nbsp;</td>"
    ."</tr>"
    ."<tr height='60'>"
    ."<td class='bunnmfooter' width='160' height='60'>&nbsp;</td>"
    ."<td class='bunnmfooter' width='10' height='60'>&nbsp;</td>"
    ."<td class='bunnmfooter' height='60'>"
    ."<center>";
 echo $footer;
 echo"</center>"
    ."</td>"
    ."<td class='bunnmfooter' width='10' height='60'>&nbsp;</td>"
    ."<td class='bunnmfooter' width='12' height='60'>&nbsp;</td>"
    ."</tr>"
    ."</table></center>";
 }
 }

/************************************************************/
/* Function themesidebox()                                  */
/*                                                          */
/* Control look of your blocks. Just simple.                */
/************************************************************/

function themesidebox($title, $content) {
 echo"<table width='100%' border='0' cellspacing='1' cellpadding='5'>"
    ."<tr>"
    ."<td colspan='1'><div class='sidboxtitle'>$title</div></td>"
    ."</tr>"
    ."<tr>"
    ."<td><font size='2'>$content</font></td>"
    ."</tr>"
    ."</table>";
 }

/************************************************************/
/* Function themecenterposts()                              */
/*                                                          */
/* Control look of your blocks. Just simple.                */
/************************************************************/

function themecenterposts($title, $content) {
 echo"<table border='0' cellpadding='3' cellspacing='5' width='100%'>"
    ."<tr>"
    ."<td><div class='indextitle'>$title</div><br /></td>"
    ."</tr>"
    ."<tr><td>$content</td>"
    ."</tr>"
    ."<tr><td align='right'>&nbsp;</td>"
    ."</tr>"
    ."</table>";
 }
?>