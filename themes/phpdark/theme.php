<?php
/* version: SE 1.0 build 20011202 */
/************************************************************/
/* Theme Name: phpdark (v1.0)                               */
/* Theme Developer: kaox 2001 (http://www.kaox.d2g.com)     */
/* Modified by : w4z004 (http://www.xoops.sytes.net)        */
/* Last Updated: 12/18/2001                                 */
/*                                                          */
/* Note: This theme looks best in IE 5-6 and Netscape 6.    */
/* It looks okay in Netscape 4.7x but the right-hand side   */
/* of the Story Box displays a little off by a few pixels.  */
/************************************************************/

/************************************************************/
/* Theme Colors Definition                                  */
/*                                                          */
/* Define colors for your web site. $bgcolor2 is generaly   */
/* used for the tables border as you can see on OpenTable() */
/* function, $bgcolor1 is for the table background and the  */
/* other two bgcolor variables follows the same criteria.   */
/* $texcolor1 and 2 are for tables internal texts           */
/************************************************************/
$xoopsTheme['thename'] = "phpdark";
$xoopsTheme['bgcolor1'] = "#a4a4a4";  // E3E4E0";
$xoopsTheme['bgcolor2'] = "#c1c1c1";  // CCCCCC";
$xoopsTheme['bgcolor3'] = "#000000";  // DDE1DE";
$xoopsTheme['bgcolor4'] = "#000000"; // F5F5F5";
$xoopsTheme['bgcolor5'] = "#6a6a6a";  // F5F5F5";
$xoopsTheme['textcolor1'] = "#ffffff"; // #000000";
$xoopsTheme['textcolor2'] = "#e5e5e5"; // #000000";

/************************************************************/
/* OpenTable Functions                                      */
/*                                                          */
/* Define the tables look&feel for you whole site. For this */
/* we have two options: OpenTable and OpenTable2 functions. */
/* Then we have CloseTable and CloseTable2 function to      */
/* properly close our tables. The difference is that        */
/* OpenTable has a 100% width and OpenTable2 has a width    */
/* according with the table content                         */
/************************************************************/
function OpenTable($width="100%") {
    	global $xoopsTheme;
	echo "<table width='".$width."' border='0' cellspacing='0' cellpadding='0' bgcolor='".$xoopsTheme['bgcolor2']."'><tr><td>\n"; 
    	echo "<table width='100%' border='0' cellspacing='1' cellpadding='8' bgcolor='".$xoopsTheme['bgcolor5']."'><tr><td>\n";
}

function CloseTable() {
     	echo "</td></tr></table></td></tr></table>\n";
}

/************************************************************/
/* Function themeheader()                                   */
/*                                                          */
/* Control the header for your site. You need to define the */
/* BODY tag and in some part of the code call the blocks    */
/* function for left side with: blocks(left);               */
/************************************************************/

function themeheader($show_rblock) {
  	global $xoopsConfig, $xoopsUser;
   	if ( $xoopsUser ) { 
    		$username = $xoopsUser->uname();
    	} else {
        	$username = $xoopsConfig['anonymous']; 
    	}
    	echo "<body bgcolor='#6a6a6a'>\n\n";
    	OpenWaitBox();
    	echo "<table width='100%' border='0' cellspacing='1' cellpadding='0' bgcolor='#666666'>
    	<tr>
    	<td valign='top' bgcolor='#000000'>
    	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
    	<tr>
    	<td width='285' valign='middle' align='center'> <a href='".$xoopsConfig['xoops_url']."/'><img src='".$xoopsConfig['xoops_url']."/themes/phpdark/images/logo.gif'></a></td>
    	<td>";
    	if ( $xoopsConfig['banners'] ) {
		showbanner();
    	}
    	echo "</td>
    	</tr>
    	</table>
    	</td>
    	</tr>
    	<tr>
    	<td valign='top' bgcolor='#6a6a6a'>
    	<table width='100%' border='0' cellspacing='0' cellpadding='0' height='100%'>
    	<tr>
    	<td width='20%' bgcolor='#818181' valign='top' align='center'0>";
    	make_sidebar("left");
    	echo "</td>
    	<td align='center' valign='top'>";
}

/************************************************************/
/* Function themefooter()                                   */
/*                                                          */
/* Control the footer for your site. You don't need to      */
/* close BODY and HTML tags at the end. In some part call   */
/* the function for right blocks with: blocks(right);       */
/* Also, $index variable need to be global and is used to   */
/* determine if the page your're viewing is the Homepage or */
/* and internal one.                                        */
/************************************************************/

function themefooter($show_rblock, $footer) {
	echo "</td>";
	if ( $show_rblock ) {
		echo "<td width='20%' bgcolor='#818181' valign='top' align='center'>";
		make_sidebar("right");
	}
    	echo "</td></tr></table></td></tr><tr><td height='30' valign='middle' bgcolor='#000000' align='center'>";    
	echo $footer;
    	echo "</td></tr></table>";
	CloseWaitBox();
}

/************************************************************/
/* Function themesidebox()                                  */
/*                                                          */
/* Control look of your blocks. Just simple.                */
/************************************************************/

function themesidebox($title, $content){
	global $xoopsConfig;
	echo "<table width='100%' border='0' cellspacing='0' cellpadding='2'><tr><td bgcolor='#cdcdcd'>&nbsp;<b><font color='#000000'>$title</font></b></td></tr><tr><td valign='top'>$content</td></tr></table><br />";
}

function themecenterposts($title, $content){
	global $xoopsConfig;
	echo "<br /><table width='95%' border='0' cellspacing='1' cellpadding='5' cellpadding='0' bgcolor='#ffffff'> <tr><td bgcolor='#000000'>&nbsp;<b><font color='#ffffff'>$title</font></b></td></tr><tr><td bgcolor='#818181'>$content</td></tr></table>";
}
?>