<?php
/************************************************************/
/* Function themeindex()                                    */
/*                                                          */
/* This function format the stories on the story page, when */
/* you click on that "Read More..." link in the home        */
/************************************************************/

function themearticle ($poster, $datetime, $title, $thetext, $topic, $topicimage, $topictext, $adminlink) {
    	global $xoopsConfig;
    	echo "<table width='95%' border='0' cellspacing='1' cellpadding='3' bgcolor='#000000'><tr><td> &nbsp;<b>$title</b></td></tr><tr><td bgcolor='#9a9a9a' valign='top'><font size='1'>"._TH_POSTEDBY." $poster</font>";
        echo $adminlink;
    	echo "<br /><br />";
    	echo "<a href='index.php?storytopic=$topic'>".GetImgTag($xoopsConfig['tipath'],$topicimage,$topictext,0,"right",-1,-1,10,10)."</a>";
    	echo "$thetext</td></tr><tr><td bgcolor='#efefef'>$morelink</td></tr></table><br />";
}
?>