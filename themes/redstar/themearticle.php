<?php
/************************************************************/
/* Function themearticle()                                  */
/*                                                          */
/* This function format the stories on the story page, when */
/* you click on that "Read More..." link in the home        */
/************************************************************/

function themearticle ($poster, $time, $title, $thetext, $topic, $topicimage, $topictext, $adminlink) {
	global $xoopsConfig;
	echo"<table summary='article' border='0' cellpadding='3' cellspacing='5' width='100%'><tr>"
    	."<td><div class='indextitle'>$title</div><br /><font size='1'>"._TH_POSTEDBY." $poster "._TH_ON." $time";
	echo $adminlink;
	echo"<br />"._TH_TOPIC.": <a href='index.php?storytopic=$topic'>$topictext</a></td>"
    	."</tr><tr><td><a href='index.php?storytopic=$topic'>".GetImgTag($xoopsConfig['tipath'],$topicimage,$topictext,0,"right",-1,-1,10,10)."</a><div class='articletext'>$thetext</div></td></tr></table><br />";
}
?>