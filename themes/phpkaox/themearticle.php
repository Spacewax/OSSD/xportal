<?php
/************************************************************/
/* Function themeindex()                                    */
/*                                                          */
/* This function format the stories on the story page, when */
/* you click on that "Read More..." link in the home        */
/************************************************************/

function themearticle($poster, $datetime, $title, $thetext, $topic, $topicimage, $topictext, $adminlink) {
    global $xoopsUser, $xoopsConfig;
    echo "<table width=\"95%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#E2DBD3\"><tr><td> &nbsp;<b>$title</b></td></tr><tr><td bgcolor=\"#EFEFEF\" valign=\"top\"><font size=\"1\">"._TH_POSTEDBY." $poster</font>";
    echo $adminlink;
    echo "<br><br>";
    echo "<a href=\"index.php?storytopic=$topic\">".GetImgTag($xoopsConfig['tipath'],$topicimage,$topictext,0,"right",-1,-1,10,10)."</a>";
    echo "$thetext</td></tr><tr><td bgcolor=\"#EFEFEF\">$morelink</td></tr></table><br>";
}

?>