<?php
/* version: SE 1.0 build 20011202 */
/************************************************************/
/* Theme Name: phpkaox (v1.0)                               */
/* Theme Developer: kaox 2001 (http://www.kaox.d2g.com)     */
/* Last Updated: 12/14/2001                                 */
/*                                                          */
/* Note: This theme looks best in IE 5-6 and Netscape 6.    */
/* It looks okay in Netscape 4.7x but the right-hand side   */
/* of the Story Box displays a little off by a few pixels.  */
/************************************************************/

/************************************************************/
/* Theme Colors Definition                                  */
/*                                                          */
/* Define colors for your web site. $bgcolor2 is generaly   */
/* used for the tables border as you can see on OpenTable() */
/* function, $bgcolor1 is for the table background and the  */
/* other two bgcolor variables follows the same criteria.   */
/* $texcolor1 and 2 are for tables internal texts           */
/************************************************************/
$xoopsTheme['thename'] = "phpkaox";
$xoopsTheme['bgcolor1'] = "#E3E4E0";
$xoopsTheme['bgcolor2'] = "#CCCCCC";
$xoopsTheme['bgcolor3'] = "#DDE1DE";
$xoopsTheme['bgcolor4'] = "#F5F5F5";
$xoopsTheme['bgcolor5'] = "#F5F5F5";
$xoopsTheme['textcolor1'] = "#000000";
$xoopsTheme['textcolor2'] = "#000000";

/************************************************************/
/* OpenTable Functions                                      */
/*                                                          */
/* Define the tables look&feel for you whole site. For this */
/* we have two options: OpenTable and OpenTable2 functions. */
/* Then we have CloseTable and CloseTable2 function to      */
/* properly close our tables. The difference is that        */
/* OpenTable has a 100% width and OpenTable2 has a width    */
/* according with the table content                         */
/************************************************************/

function OpenTable($width="100%") {
    global $xoopsTheme;
	echo "<table width=\"".$width."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"".$xoopsTheme['bgcolor2']."\"><tr><td>\n";
    	echo "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"8\" bgcolor=\"".$xoopsTheme['bgcolor5']."\"><tr><td>\n";
}

function CloseTable() {
     echo "</td></tr></table></td></tr></table>\n";
}

/************************************************************/
/* Function themeheader()                                   */
/*                                                          */
/* Control the header for your site. You need to define the */
/* BODY tag and in some part of the code call the blocks    */
/* function for left side with: blocks(left);               */
/************************************************************/

function themeheader($show_rblock) {
	global $xoopsConfig, $xoopsUser;
    if($xoopsUser){ 
    	$username = $xoopsUser->uname();
    } else {
        $username = $xoopsConfig['anonymous']; 
    }
    echo "<body bgcolor=\"#ffffff\">\n\n";
    OpenWaitBox();
    echo "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\" bgcolor=\"#666666\">
    <tr>
    <td valign=\"top\" bgcolor=\"#DDE1DE\">
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
    <tr>
    <td width=\"285\" valign=\"middle\" align=\"center\"> <a href='".$xoopsConfig['xoops_url']."/'><img src=\"".$xoopsConfig['xoops_url']."/themes/phpkaox/images/logo.gif\"></a></td>
    <td>";
    if ($xoopsConfig['banners']) {
	showbanner();
    }
    echo "</td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td valign=\"top\" bgcolor=\"#FFFFFF\">
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"100%\">
    <tr>
    <td width=\"20%\" bgcolor=\"#EFEFEF\" valign=\"top\" align=\"center\"0>";
    make_sidebar("left");
    echo "</td>
    <td align=\"center\" valign=\"top\">";
}

/************************************************************/
/* Function themefooter()                                   */
/*                                                          */
/* Control the footer for your site. You don't need to      */
/* close BODY and HTML tags at the end. In some part call   */
/* the function for right blocks with: blocks(right);       */
/* Also, $index variable need to be global and is used to   */
/* determine if the page your're viewing is the Homepage or */
/* and internal one.                                        */
/************************************************************/

function themefooter($show_rblock, $footer) {
    echo "</td>";
    if ( $show_rblock ) {
	echo "<td width=\"20%\" bgcolor=\"#efefef\" valign=\"top\" align=\"center\">";
	make_sidebar("right");
    }
    echo "</td></tr></table></td></tr><tr>
    <td height=\"30\" valign=\"middle\" bgcolor=\"#DDE1DE\" align=\"center\">";
    echo $footer;
    echo "</td>
    </tr>
    </table>";
    CloseWaitBox();
}

/************************************************************/
/* Function themesidebox()                                  */
/*                                                          */
/* Control look of your blocks. Just simple.                */
/************************************************************/

function themesidebox($title, $content) {
    echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
    <tr>
    <td bgcolor=\"#E2DBD3\">&nbsp;<b><font color=\"#666600\">$title</font></b></td>
    </tr>
    <tr>
    <td valign=\"top\">$content</td>
    </tr>
    </table><br>";
}

function themecenterposts($title, $content) {
    echo "<br><table width=\"95%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\" cellpadding=\"0\" bgcolor=\"#E2DBD3\"> <tr><td bgcolor=\"#E2DBD3\">&nbsp;<b><font color=\"#666600\">$title</font></b></td></tr><tr><td bgcolor=\"#EFEFEF\">$content</td></tr></table>";

}
?>