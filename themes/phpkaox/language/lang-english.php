<?php
define("_TH_POSTEDBY","Posted by");
define("_TH_ON","on");
define("_TH_READS","reads");
define("_TH_CREATE","Create an account");
define("_TH_EDIT","Edit");
define("_TH_DELETE","Delete");
define("_TH_WELCOME","Welcome %s");
define("_TH_WELCOMETO","Welcome To %s");
define("_TH_SEARCH","Search");
define("_TH_HOME","Home");
define("_TH_FORUM","Forum");
define("_TH_FORUMS","Forums");
define("_TH_STATS","Stats");
define("_TH_WEBLINKS","Web Links");
define("_TH_SUBMITNEWS","Submit");
define("_TH_ARCHIVE","Archives");
define("_TH_ALLTOPICS","All Topics");
define("_TH_DOWNLOADS","Downloads");
define("_TH_YOURACCOUNT","Your Account");
define("_TH_TOP10","Top 10");
define("_TH_LINKS","Links");
define("_TH_TELLFRIEND","Tell Friend");
define("_TH_USERS","Users");
define("_TH_ACCOUNT","Account");
define("_TH_SECTIONS","Sections");
?>