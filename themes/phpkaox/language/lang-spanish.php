<?php
define("_TH_POSTEDBY","Enviado por");
define("_TH_ON","el");
define("_TH_TOPIC","Tema");
define("_TH_READS","le�das");
define("_TH_CREATE","Crear una Cuenta");
define("_TH_EDIT","Editar");
define("_TH_DELETE","Borrar");
define("_TH_WELCOME","Bienvenido %s");
define("_TH_WELCOMETO","Bienvenido a %s");
define("_TH_SEARCH","Buscar");
define("_TH_HOME","Inicio");
define("_TH_FORUM","Foro");
define("_TH_FORUMS","Foros");
define("_TH_STATS","Estad�sticas");
define("_TH_WEBLINKS","Enlaces");
define("_TH_SUBMITNEWS","Enviar");
define("_TH_ARCHIVE","Archivos");
define("_TH_ALLTOPICS","Todos los Temas");
define("_TH_DOWNLOADS","Descargas");
define("_TH_YOURACCOUNT","Su Cuenta");
define("_TH_TOP10","Top 10");
define("_TH_LINKS","Enlaces");
define("_TH_TELLFRIEND","Recom.");
define("_TH_USERS","Usuarios");
define("_TH_ACCOUNT","Cuenta");
define("_TH_SECTIONS","Secciones");
?>
