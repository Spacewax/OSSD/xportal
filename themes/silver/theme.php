<?php
/* version: Xoops 6.5 build 20011222 */

/************************************************************/
/* Theme Name: silver (v1.0)                                */
/* Copyright (c) 2001 MPN-se (http://www.xoops.org)         */
/* Created by Xend  (Http;//xendtech.com)                   */
/* Last Updated: 12/22/2001                                 */
/************************************************************/

/************************************************************/
/* Theme Colors Definition                                  */
/*                                                          */
/* Define colors for your web site. $bgcolor2 is generaly   */
/* used for the tables border as you can see on OpenTable() */
/* function, $bgcolor1 is for the table background and the  */
/* other two bgcolor variables follows the same criteria.   */
/* $texcolor1 and 2 are for tables internal texts           */
/************************************************************/

 $xoopsTheme['thename'] = "silver";
 $xoopsTheme['bgcolor1'] = "#DCDCDC";
 $xoopsTheme['bgcolor2'] = "#D3D3D3";
 $xoopsTheme['bgcolor3'] = "#DCDCDC";
 $xoopsTheme['bgcolor4'] = "#C0C0C0";
 $xoopsTheme['textcolor1'] = "#000000";
 $xoopsTheme['textcolor2'] = "#000000";


/************************************************************/
/* OpenTable Functions                                      */
/*                                                          */
/* Define the tables look&feel for you whole site. For this */
/* we have two options: OpenTable and OpenTable2 functions. */
/* Then we have CloseTable and CloseTable2 function to      */
/* properly close our tables. The difference is that        */
/* OpenTable has a 100% width and OpenTable2 has a width    */
/* according with the table content                         */
/************************************************************/



function OpenTable($width="100%") {
 global $xoopsTheme;
 echo "<table width='".$width."' border='0' cellspacing='1' cellpadding='0' bgcolor='".$xoopsTheme['bgcolor4']."'><tr><td valign='top'>\n";
 echo "<table width='100%' border='0' cellspacing='1' cellpadding='8' bgcolor='".$xoopsTheme['bgcolor1']."'><tr><td valign='top'>\n";
 }
function CloseTable() {
 echo "</td></tr></table></td></tr></table>\n";
 }

/************************************************************/
/* Function themeheader()                                   */
/*                                                          */
/* Control the header for your site. You need to define the */
/* BODY tag and in some part of the code call the blocks    */
/* function for left side with: blocks(left);               */
/************************************************************/

function themeheader($show_rblock){
 global $xoopsConfig;
 echo"</head><body>\n";
 OpenWaitBox();

/************************************************************/
/*  IF you wan't to have 750 width on site                  */
/*  remove double // this line                              */
/**/      //echo"<div id=canvas>";                          /**/
/************************************************************/

 echo"<table border='0' cellpadding='0' cellspacing='0' class='toptable'><tr>"
    ."<td class='top'><a href='".$xoopsConfig['xoops_url']."/index.php'><img src='".$xoopsConfig['xoops_url']."/themes/silver/images/logo.gif' align='left' alt='".sprintf(_TH_WELCOMETO,$xoopsConfig['sitename'])."' /></a></td>";
 echo"<td align='center' class='top'>&nbsp;";
 if ( $xoopsConfig['banners'] ) {
 showbanner();
 }
 echo"</td><td align='right' class='top'>"


//''''''''''''''''''''''''''''''''search''''''''''''''''''''''''''''''''''''

    ."<table border='0' cellpadding='8' cellspacing='8'><tr>"
    ."<td align='right' valign='middle'><form action='".$xoopsConfig['xoops_url']."/search.php' method='post'>"
    ."<input type='text' name='query' size='14' />"
    ."<input type='hidden' name='action' value='results' /></form></td>"
    ."</tr></table>"

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ."</td></tr><tr>"
    ."<td class='topborder'>&nbsp;</td>"
    ."<td class='topborder'>&nbsp;</td>"
    ."<td class='topborder'>&nbsp;</td>"
    ."</tr></table>"
    ."<table border='0' cellpadding='0' cellspacing='0' class='table'><tr>"
    ."<td class='sidebar1' valign='top'>";
 make_sidebar("left");
 echo"</td><td class='leftmb'>&nbsp;</td><td class='newstd'>";
}

/************************************************************/
/* Function themefooter()                                   */
/*                                                          */
/* Control the footer for your site. You don't need to      */
/* close BODY and HTML tags at the end. In some part call   */
/* the function for right blocks with: blocks(right);       */
/* Also, $index variable need to be global and is used to   */
/* determine if the page your're viewing is the Homepage or */
/* and internal one.                                        */
/************************************************************/

function themefooter($show_rblock, $footer){
 global $xoopsConfig;

 echo"</td>";
 if ( $show_rblock ) {
 echo"<td class='rigthmb'>&nbsp;</td>"
    ."<td class='sidebar2' valign='top'>";
 make_sidebar("right");
 echo"</td></tr><tr>"
    ."<td class='bunnmbleft'>&nbsp;</td>"
    ."<td class='leftmb2'>&nbsp;</td>"
    ."<td class='bunnmb'>&nbsp;</td>"
    ."<td class='rigthmb2'>&nbsp;</td>"
    ."<td class='bunnmbrigth'>&nbsp;</td>"
    ."</tr><tr>"
    ."<td class='bunnmfooter2'>&nbsp;</td>"
    ."<td class='bunnmfooter'>&nbsp;</td>"
    ."<td class='bunnmfooter'><center><br />";
 echo $footer;
 echo"<br></center></td>"
    ."<td class='bunnmfooter'>&nbsp;</td>"
    ."<td class='bunnmfooter3'>&nbsp;</td>"
    ."</tr></table></center>";
 }
 if ( !$show_rblock ) {
 echo"<td class='rigthmb'>&nbsp;</td>"
    ."<td class='sidebar2'>&nbsp;";
 echo"</td></tr><tr>"
        ."<td class='bunnmbleft'>&nbsp;</td>"
        ."<td class='leftmb2'>&nbsp;</td>"
        ."<td class='bunnmb'>&nbsp;</td>"
        ."<td class='rigthmb2'>&nbsp;</td>"
        ."<td class='bunnmbrigth'>&nbsp;</td>"
        ."</tr><tr>"
        ."<td class='bunnmfooter2'>&nbsp;</td>"
        ."<td class='bunnmfooter'>&nbsp;</td>"
        ."<td class='bunnmfooter'>"
        ."<center><br />";
 echo $footer;
 echo"<br /></center></td>"
    ."<td class='bunnmfooter'>&nbsp;</td>"
    ."<td class='bunnmfooter3'>&nbsp;</td>"
    ."</tr></table>";
 }
 CloseWaitBox();
 }

/************************************************************/
/* Function themesidebox()                                  */
/*                                                          */
/* Control look of your blocks. Just simple.                */
/************************************************************/

function themesidebox($title, $content) {
 echo"<table border='0' cellpadding='0' cellspacing='0' class='tablesidbox'><tr>"
    ."<td><div class='sidboxtitle'>$title</div></td></tr><tr>"
    ."<td class='sideboxcontent'>$content<div class='sidboxspace'>&nbsp;</div></td></tr></table>";
 }

/************************************************************/
/* Function themecenterposts()                              */
/*                                                          */
/* Control look of your blocks. Just simple.                */
/************************************************************/

function themecenterposts($title, $content) {
 echo"<table border='0' cellpadding='0' cellspacing='0' class='table'><tr>"
    ."<td><div class='indextitle'>$title</div><br /></td></tr>"
    ."<tr><td>$content</td></tr>"
    ."<tr><td align='right'>&nbsp;</td></tr></table>";
 }
?>
