<?php
/************************************************************/
/* Function themeindex()                                    */
/*                                                          */
/* This function format the stories on the Homepage         */
/************************************************************/

function themeindex ($poster, $time, $title, $counter, $topic, $thetext, $morelink, $topicimage, $topictext) {
 global $xoopsConfig;
 echo"<table border='0' cellpadding='3' cellspacing='5' width='100%' class='table'><tr>"
    ."<td><div class='indextitle'>$title</div><br /><font size='1'>"._TH_POSTEDBY." $poster "._TH_ON." $time ($counter "._TH_READS.")<br /><b>"._TH_TOPIC."</b> <a href='index.php?storytopic=$topic'>$topictext</a><br /></td>"
    ."</tr><tr><td><a href='index.php?storytopic=$topic'>".GetImgTag($xoopsConfig['tipath'],$topicimage,$topictext,0,"right",-1,-1,10,10)."</a><div class='indextext'>$thetext</div><br /><br /></td></tr><tr><td align='right'><font size='2'>$morelink</td></tr></table>";
 }
?>