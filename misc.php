<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
include("mainfile.php");

if ( $action == "showpopups" ) {
	$theme = getTheme();
	include($xoopsConfig['root_path']."themes/".$theme."/theme.php");
	$css = getCss($theme);
	echo "<html>\n<head>\n";
	echo "<title>".$xoopsConfig['sitename']." ".ucfirst($type)."</title>\n";
	echo "<meta http-equiv='Content-Type' content='text/html; charset="._CHARSET."'>\n";
	echo "<style type='text/css' media='all'><!-- @import url(".$xoopsConfig['xoops_url']."/xoops.css); --></style>\n";
	if ( !empty($css) ) {
		echo "<style type='text/css' media='all'><!-- @import url($css); --></style>\n\n";
	}
	// show javascript close button?
	$closebutton = 1;
	switch ( $type ) {
		case "smilies":
			echo "<script type=\"text/javascript\">\n"
			."<!--\n"
			."function x () {\n"
			."return;\n"
			."}\n"
			."\n"
			."function DoSmilie(addSmilie) {\n"
			."var addSmilie;\n"
			."var revisedMessage;\n"
			."var currentMessage = window.opener.coolsus.message.value;\n"
			."revisedMessage = currentMessage+addSmilie;\n"
			."window.opener.coolsus.message.value=revisedMessage;\n"
			."return;\n"
			."}\n"
			."</script>\n\n";
			echo "</head><body>\n";
			echo "<div style='text-align: center;'>";
			echo "<table width='100%'>
			<tr bgcolor='#ffffff'>
			<td colspan='3'><big><b>"._SMILIES."</b></big><br />"._CLICKASMILIE."</td>
			</tr>
			<tr bgcolor='".$xoopsTheme['bgcolor2']."'>
			<td><b>"._CODE."</b></td>
			<td><b>"._EMOTION."</b></td>
			<td><b>"._IMAGE."</b></td>
			</tr>";
			if ( $getsmiles = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("smiles")) ) {
				$url_smiles = $xoopsConfig['xoops_url']."/images/smilies";
				$rcolor = $xoopsTheme['bgcolor1'];
				$tcolor = $xoopsTheme['textcolor2'];
				while ( $smile = $xoopsDB->fetch_array($getsmiles) ) {
					echo "<tr bgcolor='".$rcolor."'><td>".$smile['code']."</td><td>".$smile['emotion']."</td><td><a href='javascript: x()' onclick='DoSmilie(\" ".$smile['code']." \");'><img src='$url_smiles/".$smile['smile_url']."' alt='' /></a></td></tr>";
					if ( $rcolor == $xoopsTheme['bgcolor1'] ) {
						$rcolor = $xoopsTheme['bgcolor2'];
						$tcolor = $xoopsTheme['textcolor1'];
					} else {
						$rcolor = $xoopsTheme['bgcolor1'];
						$tcolor = $xoopsTheme['textcolor2'];
					}
     			}
			} else {
				echo "Could not retrieve data from the database.";
			}
			echo "</table></div>";
			break;
        case "whosonline":
			echo "</head><body><div style='text-align: center;'><b>"._WHOSONLINE."</b><br /><br /><table width='100%'>\n";
			$isadmin = 0;
			if ( $xoopsUser ) {
				if ( $xoopsUser->is_admin() ) {
					$sql = "SELECT uid, username, ip FROM ".$xoopsDB->prefix("lastseen")." WHERE online=1 ORDER BY time DESC";
					$isadmin = 1;
				} else {
					$sql = "SELECT uid, username FROM ".$xoopsDB->prefix("lastseen")." WHERE uid>0 AND online=1 ORDER BY time DESC";
				}
			} else {
				$sql = "SELECT uid, username FROM ".$xoopsDB->prefix("lastseen")." WHERE uid>0 AND online=1 ORDER BY time DESC";
			}
			$result = $xoopsDB->query($sql);
			if (!$xoopsDB->num_rows($result)) {
				echo"<tr bgcolor='".$xoopsTheme['bgcolor1']."'><td>"._NOUSRONLINE."</td></tr>\n"; 
			} else {
				if ( $isadmin== 1 ) {
					while ( list($uid, $username, $ip) = $xoopsDB->fetch_row($result) ) {
						echo "<tr bgcolor='".$xoopsTheme['bgcolor1']."'><td>";
						if ( $uid>0 ) {
							echo "<a href='javascript:window.opener.location=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$uid."\";window.close();'>$username</a>";
						} else {
							echo $xoopsConfig['anonymous'];
						}
						echo "</td><td>$ip</td></tr>\n";
					}
				} else {
					while ( list($uid, $username) = $xoopsDB->fetch_row($result) ) {
						echo "<tr bgcolor='".$xoopsTheme['bgcolor1']."'><td><a href='javascript:window.opener.location=\"".$xoopsConfig['xoops_url']."/userinfo.php?uid=".$uid."\";window.close();'>$username</a></td></tr>\n";
					}
				}
			}
			echo "</table></div>\n";
			break;
		case "avatars":
			?>
			<script language='javascript'>
			function myimage_onclick(counter){
			window.opener.document.userinfo.user_avatar.options[counter].selected = true;
			showimage();
			window.close();
			}
			function showimage() {
			if (!window.opener.document.images)
			return
			window.opener.document.images.avatar.src='<?php echo $xoopsConfig['xoops_url'];?>/images/avatar/' + window.opener.document.userinfo.user_avatar.options[window.opener.document.userinfo.user_avatar.selectedIndex].value
			}
			</script>
			</head><body>
			<div style='text-align: center;'><h4>Available Avatars</h4>
			<form name='avatars'>
			<table width='100%'><tr>
			<?php
			include_once($xoopsConfig['root_path']."class/xoopslists.php");
			if ( !isset($subdir) ) {
				$subdir = "";
			} else {
				$subdir = $subdir."/";
			}
			$lists = new XoopsLists;
			$avatarslist = $lists->getAvatarsList($subdir);
			$cntavs = 0;
			$counter = 0;
    		foreach ( $avatarslist as $avatar ) { 
				echo "<td><img src='images/avatar/".$subdir."".$avatar."' alt='".$file."' style='padding: 10px;vertical-align:top;'  /><br /><input name='myimage' type='button' value='".$avatar."' onclick='myimage_onclick(".$counter.")' /></td>\n";
				$counter++;
				$cntavs++; 
				if ( $cntavs > 8 ) { 
					echo "</tr><tr>\n";
					$cntavs=0;
				}
			}
    		echo "</tr></table></form></div>\n"; 
	        break;
        case "friend":
			if ( !isset($op) || $op == "sendform" ) {
				if ( $xoopsUser ) {
					$yname = $xoopsUser->uname();
					$ymail = $xoopsUser->email();
				} else {
					break;
				}
			    	if ( isset($tryagain) && $tryagain==1 ) {
					include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
					$myts = new MyTextSanitizer();
					$yname = $myts->makeTboxData4PreviewInForm($yname);
					$fname = $myts->makeTboxData4PreviewInForm($fname);
					$fmail = $myts->makeTboxData4PreviewInForm($fmail);
				}
				printCheckForm();
				echo "</head><body>";
				OpenTable();
				echo "<div style='text-align: center;'>\n";
				echo "<h4>"._RECOMMENDSITE."</h4>\n";
    				echo "<form action='".$xoopsConfig['xoops_url']."/misc.php' method='post' name='friend'>\n";
				echo "<table width='100%'><tr><td>";
    				echo "<input type='hidden' name='op' value='sendsite' />\n";
				echo "<input type='hidden' name='action' value='showpopups' />\n";
				echo "<input type='hidden' name='type' value='friend' />\n";
    			echo _YOURNAMEC."</td><td><input type='text' name='yname' value='$yname' /></td></tr>\n";
    			echo "<tr><td>"._YOUREMAILC."</td><td>".$ymail."</td></tr>\n";
    			echo "<tr><td>"._FRIENDNAMEC."</td><td><input type='text' name='fname' value='$fname' /></td></tr>\n";
    			echo "<tr><td>"._FRIENDEMAILC."</td><td><input type='text' name='fmail' value='$fmail' /></td></tr>\n";
    			echo "<tr><td align='center' colspan='2'><br /><br /><input type='button' value='"._SEND."' onclick='CheckForm(this.form)' />&nbsp;<input value='"._CLOSE."' type='button' onclick='javascript:window.close();' /></td></tr>\n";
    			echo "</table></form></div>\n";
				$closebutton = 0;
    			CloseTable();
			} elseif ( $op == "sendsite" ) {
				if ( $xoopsUser ) {
					$ymail = $xoopsUser->email();
				} else {
					break;
				}
				if ( !isset($yname) || $yname == "" || !isset($fname) || $fname == ""  || !$fmail ) {
				    redirect_header($xoopsConfig['xoops_url']."/misc.php?action=showpopups&amp;type=friend&amp;op=sendform&amp;tryagain=1",2,_NEEDINFO);
					exit();
				}
				if ( !checkEmail($fmail) ) {
					$errormessage = _INVALIDEMAIL1."<br />"._INVALIDEMAIL2."";
					redirect_header($xoopsConfig['xoops_url']."/misc.php?action=showpopups&amp;type=friend&amp;op=sendform&amp;yname=$yname&amp;fname=$fname&amp;fmail=$fmail&amp;tryagain=1",2,$errormessage);
					exit();
				}
    				$subject = sprintf(_INTSITE,$xoopsConfig['sitename']);
    				$message = sprintf(_HELLO,$fname);
				$message .= "\n\n";
				$message .= sprintf(_YOURFRIEND,$yname);
				$message .= "\n\n"._SITENAME." ".$xoopsConfig['sitename']."\n".$xoopsConfig['slogan']."\n"._SITEURL." ".$xoopsConfig['xoops_url']."/\n";
    				mail($fmail, $subject, $message, "From: $ymail\nX-Mailer: PHP/" . phpversion());
				OpenTable();
				echo "<div style='text-align: center;'><h4>"._REFERENCESENT."</h4></div>";
				CloseTable();
			}
			break;
	}
	if ($closebutton) {
		echo "<div style='text-align: center;'><p><input value='"._CLOSE."' type='button' onclick='javascript:window.close();' /></p></div>\n";
	}
	echo "</body>\n";
	echo "</html>\n";
}


function printCheckForm(){
?>
<script language='javascript'>
<!--
function CheckForm(form)
{
	if ( form.yname.value == "" ){
			alert( "<?php echo _ENTERYNAME;?>" )
			form.yame.focus()
			return false
	}
	else if ( form.fname.value == "" ){
			alert( "<?php echo _ENTERFNAME;?>" )
			form.fname.focus()
			return false
	}
	else if ( form.fmail.value ==""){
			alert( "<?php echo _ENTERFMAIL;?>" )
			form.fmail.focus()
			return false
	}
	else {
		document.friend.submit()
		return true
	}
}
//--->

</script>
<?php
}
?>