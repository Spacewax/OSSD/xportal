<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

$xoopsOption['pagetype'] = "user";
include("mainfile.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopslists.php");
include_once($xoopsConfig['root_path']."class/xoopshtmlform.php");

if ( !isset($op) || trim($op) == "" ) {
	if ( !$xoopsUser ) {
		redirect_header("index.php",0);
		exit();
	}
	$hf = new XoopsHtmlForm();
	$lists = new XoopsLists();
	include("header.php");
	OpenTable();

	echo "<h4 style='text-align:center;'>". _US_EDITPROFILE ."</h4><br /><a href='userinfo.php?uid=".$xoopsUser->uid()."'>". _US_PROFILE ."</a>&nbsp;<span style='font-weight:bold;'>&raquo;&raquo;</span>&nbsp;". _US_EDITPROFILE ."<br /><br />
	<table cellpadding='8' border='0'><tr><td><form name='userinfo' action='edituser.php' method='post'><b>". _US_REALNAME ."</b> ". _US_OPTIONAL ."<br /><input class='textbox' type='text' name='name' value='". $xoopsUser->name("E")."' size='30' maxlength='60' /><br /><b>".  _US_EMAIL ."</b> ". _US_REQUIRED ."<br />". _US_THISWILLBEPUBLIC ."<br /><input class='textbox' type='text' name='email' value='". $xoopsUser->email("E") ."' size='30' maxlength='60' /><br />". _US_OPTION ." <input type='checkbox' name='user_viewemail' value='1'";
	if ( $xoopsUser->user_viewemail() ) { 
		echo " checked='checked'";
	}
	echo " /> ". _US_ALLOWVIEWEMAIL ."<br />
	<b>". _US_WEBSITE ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='url' value='". $xoopsUser->url("E") ."' size='30' maxlength='100' /><br />
	<b>". _US_AVATAR ."</b> ". _US_OPTIONAL ." [ <a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/misc.php?action=showpopups&amp;type=avatars','avatars',600,400);\">"._US_LIST."</a> ]<br />";
	$avatarlist = $lists->getAvatarsList();
	echo $hf->select("user_avatar",$avatarlist,$xoopsUser->user_avatar(),1,FALSE,"onchange='showimage()'");
	echo "&nbsp;&nbsp;<img src='images/avatar/".$xoopsUser->user_avatar("E")."' name='avatar' alt='' /><br /><b>". _US_THEME ."</b><br />";
	$themeslist = $lists->getThemesList();
	echo $hf->select("theme",$themeslist,$xoopsUser->theme());
	echo "<br /><b>"._US_TIMEZONE."</b><br />\n";
	$tzlist = $lists->getTimeZoneList();
	echo $hf->select("timezone_offset",$tzlist,$xoopsUser->timezone());
	echo "<br />\n";
	echo "<b>". _US_ICQ ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='user_icq' value='". $xoopsUser->user_icq() ."' size='30' maxlength='100' /><br />
	<b>". _US_AIM ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='user_aim' value='". $xoopsUser->user_aim() ."' size='30' maxlength='100' /><br />
	<b>". _US_YIM ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='user_yim' value='". $xoopsUser->user_yim() ."' size='30' maxlength='100' /><br />
	<b>". _US_MSNM ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='user_msnm' value='". $xoopsUser->user_msnm() ."' size='30' maxlength='100' /><br />
	<b>". _US_LOCATION ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='user_from' value='". $xoopsUser->user_from("E") ."' size='30' maxlength='100'><br />
	<b>". _US_OCCUPATION ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='user_occ' value='". $xoopsUser->user_occ("E") ."' size='30' maxlength='100' /><br />
	<b>". _US_INTEREST ."</b> ". _US_OPTIONAL ."<br />
	<input class='textbox' type='text' name='user_intrest' value='". $xoopsUser->user_intrest("E") ."' size='30' maxlength='100' /><br />
	<b>". _US_SIGNATURE ."</b> ". _US_OPTIONAL ."<br />". _US_TYPEYOURSIG ."<br />
	<textarea class='textbox' wrap='virtual' cols='50' rows='5' name='user_sig'>". $xoopsUser->user_sig("E") ."</textarea><br />";

	if ( $xoopsUser->attachsig() ) {
		$sel = " checked='checked'";
	} else {
		$sel = "";
	}
	ECHO "<input type='checkbox' value='1' name='attach'".$sel." /> ". _US_SHOWSIG ."<br /><br />
	<b>". _US_CDISPLAYMODE ."</b><br />";
	
	$array = array("0"=>_US_NOCOMMENTS, "flat"=>_US_FLAT, "thread"=>_US_THREAD);
	echo $hf->select("umode",$array,$xoopsUser->umode());
	echo "<br /><b>"._US_CSORTORDER."</b><br />\n";
	$array = array("0"=>_US_OLDESTFIRST, "1"=>_US_NEWESTFIRST);
	echo $hf->select("uorder",$array,$xoopsUser->uorder());
	echo "<br /><br /><b>"._US_EXTRAINFO."</b> "._US_OPTIONAL."<br />\n";
	echo _US_255CHARMAX;
	echo "<br /><textarea class='textbox' wrap='virtual' cols='50' rows='5' name='bio'>". $xoopsUser->bio("E") ."</textarea><br /><br /><b>". _US_PASSWORD ."</b> ". _US_TYPEPASSTWICE ."<br /><input class='textbox' type='password' name='pass' size='10' maxlength='20' /><input class='textbox' type='password' name='vpass' size='10' maxlength='20' /><br /><br /><b>". _US_USECOOKIE ."</b><br />\n";
	if ( $HTTP_COOKIE_VARS[$xoopsConfig['usercookie']] ) {
		$checked = 1;
	} else {
		$checked = 0;
	}
	echo $hf->input_radio_YN("usecookie",_US_YES,_US_NO,$checked);
	echo "<br /><br />
	<input type='hidden' name='uname' value='". $xoopsUser->uname("E") ."' />
	<input type='hidden' name='uid' value='". $xoopsUser->uid() ."' />
	<input type='hidden' name='op' value='saveuser' />
	<input type='submit' value='". _US_SAVECHANGES ."' />
	</form></td></tr></table>";
	CloseTable();
	echo "<br /><br />";
	include("footer.php");
} elseif ( $op == "saveuser" ) {
	$myts= new MyTextSanitizer;
	if ( !$xoopsUser ) {
		redirect_header("index.php",3,_US_NOEDITRIGHT);
		exit();
	} else {
		if ( $xoopsUser->uid() != $uid ) {
			redirect_header("index.php",3,_US_NOEDITRIGHT);
			exit();
		}
	}
	$email = $myts->oopsStripSlashesGPC(trim($email));
	if ( (!$email) || ($email=="") || (!checkEmail($email)) ) {
		include("header.php");
		echo "<div style='text-align: center;'>"._US_INVALIDMAIL."</div><br />";
		include("footer.php");
		exit();
	}
	$email = addslashes($email);
	if ( !isset($user_viewemail) || $user_viewemail != 1 ) {
		$user_viewemail = 0;
	}
	if ( !isset($uorder) || $uorder != 1 ) {
		$uorder = 0;
	}
	if ( !isset($attach) || $attach != 1 ) {
		$attach = 0;
	}
	if ( !isset($umode) ) {
		$umode = "flat";
	}
	if ( $name != "" ) {
		$name = $myts->makeTboxData4Save($name);
	}
	if ( $user_icq != "" ) {
		$user_icq = $myts->makeTboxData4Save($user_icq);
	}
	if ( $user_aim != "" ) {
		$user_aim = $myts->makeTboxData4Save($user_aim);
	}
	if ( $user_yim != "" ) {
		$user_yim = $myts->makeTboxData4Save($user_yim);
	}
	if ( $user_msnm != "" ) {
		$user_msnm = $myts->makeTboxData4Save($user_msnm);
	}
	if ( $theme != "" ) {
		$theme = $myts->makeTboxData4Save($theme);
	}
	if ( $user_avatar != "" ) {
		$user_avatar = $myts->makeTboxData4Save($user_avatar);
	}
	if ( $user_from != "" ) {
		$user_from = $myts->makeTboxData4Save($user_from);
	}
	if ( $user_sig != "" ) {
		$user_sig = $myts->makeTareaData4Save($user_sig);
	}
	if ( $user_occ != "" ) {
		$user_occ = $myts->makeTboxData4Save($user_occ);
	}
	if ( $user_intrest != "" ) {
		$user_intrest = $myts->makeTboxData4Save($user_intrest);
	}
	if (isset($pass)) {
		$pass = trim($pass);
	}
	if (isset($vpass)) {
		$vpass = trim($vpass);
	}
	if ( (isset($pass)) && ("$pass" != "$vpass") ) {
		echo "<div style='text-align: center;'>"._US_PASSNOTSAME."</div>";
	} elseif ( ($pass != "") && (strlen($pass) < $xoopsConfig['minpass']) ) {
		echo "<div style='text-align: center;'>";
		printf(_US_PWDTOOSHORT,$xoopsConfig['minpass']);
		echo "</div>";
	} else {
		if ($bio) {
			$bio = $myts->makeTareaData4Save($bio);
		}
		if (isset($url) && $url != "") {
			$url = formatURL($url);
			$url = $myts->makeTboxData4Save($url);
		}
		if ( isset($pass) && $pass != "") {
			$pass = md5($pass);
			$xoopsDB->query("UPDATE ".$xoopsDB->prefix("users")." SET name='$name', email='$email', url='$url', user_avatar='$user_avatar', user_icq='$user_icq', user_from='$user_from', user_sig='$user_sig', user_viewemail='$user_viewemail', user_aim='$user_aim', user_yim='$user_yim', user_msnm='$user_msnm', pass='$pass', attachsig='$attach', timezone_offset='$timezone_offset', theme='$theme' WHERE uid=$uid");
		} else {
			$xoopsDB->query("UPDATE ".$xoopsDB->prefix("users")." SET name='$name', email='$email', url='$url', user_avatar='$user_avatar', user_icq='$user_icq', user_from='$user_from', user_sig='$user_sig', user_viewemail='$user_viewemail',  user_aim='$user_aim', user_yim='$user_yim', user_msnm='$user_msnm', attachsig='$attach', timezone_offset='$timezone_offset', theme='$theme' WHERE uid=$uid");
		}
		$xoopsDB->query("UPDATE ".$xoopsDB->prefix("users_status")." SET uorder='$uorder', umode='$umode', bio='$bio' , user_occ='$user_occ', user_intrest='$user_intrest' WHERE uid=$uid");
		if ( $usecookie ) {
			setcookie($xoopsConfig['usercookie'],$uname,time()+ 31536000);
		} else {
			setcookie($xoopsConfig['usercookie']);
		}
		redirect_header("userinfo.php?uid=".$uid."",1,_US_PROFUPDATED); 
		exit();
	}
}
?>