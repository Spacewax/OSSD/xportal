<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

if ( !defined("XOOPS_LISTS_INCLUDED") ) {
	define("XOOPS_LISTS_INCLUDED",1);
	
	class XoopsLists {
		function getTimeZoneList() {
			$time_zone_list = array (
			"-12" => "(GMT -12:00 hours) Eniwetok, Kwajalein",
			"-11" => "(GMT -11:00 hours) Midway Island, Samoa",
			"-10" => "(GMT -10:00 hours) Hawaii",
			"-9" => "(GMT -9:00 hours) Alaska",
			"-8" => "(GMT -8:00 hours) Pacific Time (US & Canada)",
			"-7" => "(GMT -7:00 hours) Mountain Time (US & Canada)",
			"-6" => "(GMT -6:00 hours) Central Time (US & Canada), Mexico City",
			"-5" => "(GMT -5:00 hours) Eastern Time (US & Canada), Bogota, Lima, Quito",
			"-4" => "(GMT -4:00 hours) Atlantic Time (Canada), Caracas, La Paz",
			"-3.5" => "(GMT -3:30 hours) Newfoundland",
			"-3" => "(GMT -3:00 hours) Brazil, Buenos Aires, Georgetown",
			"-2" => "(GMT -2:00 hours) Mid-Atlantic",
			"-1" => "(GMT -1:00 hours) Azores, Cape Verde Islands",
			"0" => "(GMT) Western Europe Time, London, Lisbon, Casablanca, Monrovia",
			"1" => "(GMT +1:00 hours) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris",
			"2" => "(GMT +2:00 hours) EET(Eastern Europe Time), Kaliningrad, South Africa",
			"3" => "(GMT +3:00 hours) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg",
			"3.5" => "(GMT +3:30 hours) Tehran",
			"4" => "(GMT +4:00 hours) Abu Dhabi, Muscat, Baku, Tbilisi",
			"4.5" => "(GMT +4:30 hours) Kabul",
			"5" => "(GMT +5:00 hours) Ekaterinburg, Islamabad, Karachi, Tashkent",
			"5.5" => "(GMT +5:30 hours) Bombay, Calcutta, Madras, New Delhi",
			"6" => "(GMT +6:00 hours) Almaty, Dhaka, Colombo",
			"7" => "(GMT +7:00 hours) Bangkok, Hanoi, Jakarta",
			"8" => "(GMT +8:00 hours) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei",
			"9" => "(GMT +9:00 hours) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",
			"9.5" => "(GMT +9:30 hours) Adelaide, Darwin",
			"10" => "(GMT +10:00 hours) EAST(East Australian Standard)",
			"11" => "(GMT +11:00 hours) Magadan, Solomon Islands, New Caledonia",
			"12" => "(GMT +12:00 hours) Auckland, Wellington, Fiji, Kamchatka, Marshall Island");
			return $time_zone_list;
		}

		/*
		 * gets list of themes folder from themes directory
		 */
		function getThemesList() {
			global $xoopsConfig;
			$themes_list = array();
			$themes_list = $this->getDirListAsArray($xoopsConfig['root_path']."themes/");
			return $themes_list;
		}

		/*
		 * gets list of name of directories inside a directory
		 */
		function getDirListAsArray($dirname) {
			$dirlist = array();
			$handle=@opendir($dirname);
			while ( ($file = readdir($handle)) ) {
				if ( !ereg("^[.]{1,2}$",$file) ) {
					if ( is_dir($dirname.$file) ) {
						$dirlist[$file]=$file;
					}
				}
			}
			closedir($handle);
			asort($dirlist);
			reset($dirlist);
			return $dirlist;
		}

		/*
		 *  gets list of image file names in a directory
		 */
		function getImgListAsArray($dirname) {
			$filelist = array();
			$handle=@opendir($dirname);
			while ( ($file = readdir($handle)) ) {
				if ( !ereg("^[.]{1,2}$",$file) && ereg(".gif|.jpg|.png",$file) ) {
					$filelist[$file]=$file;
				}
			}
			closedir($handle);
			asort($filelist);
			reset($filelist);
			return $filelist;
		}

		/*
		 *  gets list of avatar file names in a certain directory
		 *  if directory is not specified, default directory will be searched
		 */
		function getAvatarsList($avatar_dir="") {
			global $xoopsConfig;
			$avatars = array();
			if ( $avatar_dir != "" ) {
				$avatars = $this->getImgListAsArray($xoopsConfig['root_path']."images/avatar/".$avatar_dir);
			} else {
				$avatars = $this->getImgListAsArray($xoopsConfig['root_path']."images/avatar/");
			}
			return $avatars;
		}

		/*
		 *  gets list of all avatar image files inside default avatars directory
		 */
		function getAllAvatarsList() {
			global $xoopsConfig;
			$avatars = array();
			$dirlist = array();
			$dirlist = $this->getDirListAsArray($xoopsConfig['root_path']."images/avatars/");
			if ( count($dirlist) > 0 ) {
				foreach ( $dirlist as $dir ) {
					$avatars[$dir] = $this->getImgListAsArray($dir);
				}
			} else {
				return false;
			}
			return $avatars;
		}

		/*
		*  gets list of subject icon image file names in a certain directory
		*  if directory is not specified, default directory will be searched
		*/
		function getSubjectsList($subject_dir=""){
			global $xoopsConfig;
			$subjects = array();
			if($subject_dir != ""){
				$subjects = $this->getImgListAsArray($xoopsConfig['root_path']."images/subject/".$subject_dir);
			} else {
				$subjects = $this->getImgListAsArray($xoopsConfig['root_path']."images/subject/");
			}
			return $subjects;
		}



		/*
		 * gets list of language folders inside default language directory
		 */
		function getLangList() {
			global $xoopsConfig;
			$lang_list = array();
			$lang_list = $this->getDirListAsArray($xoopsConfig['root_path']."language/");
			return $lang_list;
		}

		function getCountryList() {
			$country_list = array (
				""   => "-",
				"AD" => "Andorra",
				"AE" => "United Arab Emirates",
				"AF" => "Afghanistan",
				"AG" => "Antigua and Barbuda",
				"AI" => "Anguilla",
				"AL" => "Albania",
				"AM" => "Armenia",
				"AN" => "Netherlands Antilles",
				"AO" => "Angola",
				"AQ" => "Antarctica",
				"AR" => "Argentina",
				"AS" => "American Samoa",
				"AT" => "Austria",
				"AU" => "Australia",
				"AW" => "Aruba",
				"AZ" => "Azerbaijan",
				"BA" => "Bosnia and Herzegovina",
				"BB" => "Barbados",
				"BD" => "Bangladesh",
				"BE" => "Belgium",
				"BF" => "Burkina Faso",
				"BG" => "Bulgaria",
				"BH" => "Bahrain",
				"BI" => "Burundi",
				"BJ" => "Benin",
				"BM" => "Bermuda",
				"BN" => "Brunei Darussalam",
				"BO" => "Bolivia",
				"BR" => "Brazil",
				"BS" => "Bahamas",
				"BT" => "Bhutan",
				"BV" => "Bouvet Island",
				"BW" => "Botswana",
				"BY" => "Belarus",
				"BZ" => "Belize",
				"CA" => "Canada",
				"CC" => "Cocos (Keeling) Islands",
				"CF" => "Central African Republic",
				"CG" => "Congo",
				"CH" => "Switzerland",
				"CI" => "Cote D'Ivoire (Ivory Coast)",
				"CK" => "Cook Islands",
				"CL" => "Chile",
				"CM" => "Cameroon",
				"CN" => "China",
				"CO" => "Colombia",
				"CR" => "Costa Rica",
				"CS" => "Czechoslovakia (former)",
				"CU" => "Cuba",
				"CV" => "Cape Verde",
				"CX" => "Christmas Island",
				"CY" => "Cyprus",
				"CZ" => "Czech Republic",
				"DE" => "Germany",
				"DJ" => "Djibouti",
				"DK" => "Denmark",
				"DM" => "Dominica",
				"DO" => "Dominican Republic",
				"DZ" => "Algeria",
				"EC" => "Ecuador",
				"EE" => "Estonia",
				"EG" => "Egypt",
				"EH" => "Western Sahara",
				"ER" => "Eritrea",
				"ES" => "Spain",
				"ET" => "Ethiopia",
				"FI" => "Finland",
				"FJ" => "Fiji",
				"FK" => "Falkland Islands (Malvinas)",
				"FM" => "Micronesia",
				"FO" => "Faroe Islands",
				"FR" => "France",
				"FX" => "France, Metropolitan",
				"GA" => "Gabon",
				"GB" => "Great Britain (UK)",
				"GD" => "Grenada",
				"GE" => "Georgia",
				"GF" => "French Guiana",
				"GH" => "Ghana",
				"GI" => "Gibraltar",
				"GL" => "Greenland",
				"GM" => "Gambia",
				"GN" => "Guinea",
				"GP" => "Guadeloupe",
				"GQ" => "Equatorial Guinea",
				"GR" => "Greece",
				"GS" => "S. Georgia and S. Sandwich Isls.",
				"GT" => "Guatemala",
				"GU" => "Guam",
				"GW" => "Guinea-Bissau",
				"GY" => "Guyana",
				"HK" => "Hong Kong",
				"HM" => "Heard and McDonald Islands",
				"HN" => "Honduras",
				"HR" => "Croatia (Hrvatska)",
				"HT" => "Haiti",
				"HU" => "Hungary",
				"ID" => "Indonesia",
				"IE" => "Ireland",
				"IL" => "Israel",
				"IN" => "India",
				"IO" => "British Indian Ocean Territory",
				"IQ" => "Iraq",
				"IR" => "Iran",
				"IS" => "Iceland",
				"IT" => "Italy",
				"JM" => "Jamaica",
				"JO" => "Jordan",
				"JP" => "Japan",
				"KE" => "Kenya",
				"KG" => "Kyrgyzstan",
				"KH" => "Cambodia",
				"KI" => "Kiribati",
				"KM" => "Comoros",
				"KN" => "Saint Kitts and Nevis",
				"KP" => "Korea (North)",
				"KR" => "Korea (South)",
				"KW" => "Kuwait",
				"KY" => "Cayman Islands",
				"KZ" => "Kazakhstan",
				"LA" => "Laos",
				"LB" => "Lebanon",
				"LC" => "Saint Lucia",
				"LI" => "Liechtenstein",
				"LK" => "Sri Lanka",
				"LR" => "Liberia",
				"LS" => "Lesotho",
				"LT" => "Lithuania",
				"LU" => "Luxembourg",
				"LV" => "Latvia",
				"LY" => "Libya",
				"MA" => "Morocco",
				"MC" => "Monaco",
				"MD" => "Moldova",
				"MG" => "Madagascar",
				"MH" => "Marshall Islands",
				"MK" => "Macedonia",
				"ML" => "Mali",
				"MM" => "Myanmar",
				"MN" => "Mongolia",
				"MO" => "Macau",
				"MP" => "Northern Mariana Islands",
				"MQ" => "Martinique",
				"MR" => "Mauritania",
				"MS" => "Montserrat",
				"MT" => "Malta",
				"MU" => "Mauritius",
				"MV" => "Maldives",
				"MW" => "Malawi",
				"MX" => "Mexico",
				"MY" => "Malaysia",
				"MZ" => "Mozambique",
				"NA" => "Namibia",
				"NC" => "New Caledonia",
				"NE" => "Niger",
				"NF" => "Norfolk Island",
				"NG" => "Nigeria",
				"NI" => "Nicaragua",
				"NL" => "Netherlands",
				"NO" => "Norway",
				"NP" => "Nepal",
				"NR" => "Nauru",
				"NT" => "Neutral Zone",
				"NU" => "Niue",
				"NZ" => "New Zealand (Aotearoa)",
				"OM" => "Oman",
				"PA" => "Panama",
				"PE" => "Peru",
				"PF" => "French Polynesia",
				"PG" => "Papua New Guinea",
				"PH" => "Philippines",
				"PK" => "Pakistan",
				"PL" => "Poland",
				"PM" => "St. Pierre and Miquelon",
				"PN" => "Pitcairn",
				"PR" => "Puerto Rico",
				"PT" => "Portugal",
				"PW" => "Palau",
				"PY" => "Paraguay",
				"QA" => "Qatar",
				"RE" => "Reunion",
				"RO" => "Romania",
				"RU" => "Russian Federation",
				"RW" => "Rwanda",
				"SA" => "Saudi Arabia",
				"Sb" => "Solomon Islands",
				"SC" => "Seychelles",
				"SD" => "Sudan",
				"SE" => "Sweden",
				"SG" => "Singapore",
				"SH" => "St. Helena",
				"SI" => "Slovenia",
				"SJ" => "Svalbard and Jan Mayen Islands",
				"SK" => "Slovak Republic",
				"SL" => "Sierra Leone",
				"SM" => "San Marino",
				"SN" => "Senegal",
				"SO" => "Somalia",
				"SR" => "Suriname",
				"ST" => "Sao Tome and Principe",
				"SU" => "USSR (former)",
				"SV" => "El Salvador",
				"SY" => "Syria",
				"SZ" => "Swaziland",
				"TC" => "Turks and Caicos Islands",
				"TD" => "Chad",
				"TF" => "French Southern Territories",
				"TG" => "Togo",
				"TH" => "Thailand",
				"TJ" => "Tajikistan",
				"TK" => "Tokelau",
				"TM" => "Turkmenistan",
				"TN" => "Tunisia",
				"TO" => "Tonga",
				"TP" => "East Timor",
				"TR" => "Turkey",
				"TT" => "Trinidad and Tobago",
				"TV" => "Tuvalu",
				"TW" => "Taiwan",
				"TZ" => "Tanzania",
				"UA" => "Ukraine",
				"UG" => "Uganda",
				"UK" => "United Kingdom",
				"UM" => "US Minor Outlying Islands",
				"US" => "United States",
				"UY" => "Uruguay",
				"UZ" => "Uzbekistan",
				"VA" => "Vatican City State (Holy See)",
				"VC" => "Saint Vincent and the Grenadines",
				"VE" => "Venezuela",
				"VG" => "Virgin Islands (British)",
				"VI" => "Virgin Islands (U.S.)",
				"VN" => "Viet Nam",
				"VU" => "Vanuatu",
				"WF" => "Wallis and Futuna Islands",
				"WS" => "Samoa",
				"YE" => "Yemen",
				"YT" => "Mayotte",
				"YU" => "Yugoslavia",
				"ZA" => "South Africa",
				"ZM" => "Zambia",
				"ZR" => "Zaire",
				"ZW" => "Zimbabwe"
			);
    			asort($country_list);
    			reset($country_list);
    			return $country_list;
		}

		function getHtmlList() {
			$html_list = array (
				"a" => "&lt;a&gt;",
				"abbr" => "&lt;abbr&gt;",
				"acronym" => "&lt;acronym&gt;",
				"address" => "&lt;address&gt;",
				"b" => "&lt;b&gt;",
				"bdo" => "&lt;bdo&gt;",
				"big" => "&lt;big&gt;",
				"blockquote" => "&lt;blockquote&gt;",
				"caption" => "&lt;caption&gt;",
				"cite" => "&lt;cite&gt;",
				"code" => "&lt;code&gt;",
				"col" => "&lt;col&gt;",
				"colgroup" => "&lt;colgroup&gt;",
				"dd" => "&lt;dd&gt;",
				"del" => "&lt;del&gt;",
				"dfn" => "&lt;dfn&gt;",
				"div" => "&lt;div&gt;",
				"dl" => "&lt;dl&gt;",
				"dt" => "&lt;dt&gt;",
				"em" => "&lt;em&gt;",
				"font" => "&lt;font&gt;",
				"h1" => "&lt;h1&gt;",
				"h2" => "&lt;h2&gt;",
				"h3" => "&lt;h3&gt;",
				"h4" => "&lt;h4&gt;",
				"h5" => "&lt;h5&gt;",
				"h6" => "&lt;h6&gt;",
				"hr" => "&lt;hr&gt;",
				"i" => "&lt;i&gt;",
				"img" => "&lt;img&gt;",
				"ins" => "&lt;ins&gt;",
				"kbd" => "&lt;kbd&gt;",
				"li" => "&lt;li&gt;",
				"map" => "&lt;map&gt;",
				"object" => "&lt;object&gt;",
				"ol" => "&lt;ol&gt;",
				"samp" => "&lt;samp&gt;",
				"small" => "&lt;small&gt;",
				"strong" => "&lt;strong&gt;",
				"sub" => "&lt;sub&gt;",
				"sup" => "&lt;sup&gt;",
				"table" => "&lt;table&gt;",
				"tbody" => "&lt;tbody&gt;",
				"td" => "&lt;td&gt;",
				"tfoot" => "&lt;tfoot&gt;",
				"th" => "&lt;th&gt;",
				"thead" => "&lt;thead&gt;",
				"tr" => "&lt;tr&gt;",
				"tt" => "&lt;tt&gt;",
				"ul" => "&lt;ul&gt;",
				"var" => "&lt;var&gt;"
			);
			asort($html_list);
    			reset($html_list);
    			return $html_list;
		}
	}
}
?>
