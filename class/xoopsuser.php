<?php
###############################################################################
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
###############################################################################

include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopsgroup.php");

class XoopsUserSession{

	var $sessionID;
	var $cookie;
	var $expiretime;
	var $db;
	var $hash;
	var $uid;
	var $uname;

	function XoopsUserSession($sessionid=""){
		global $xoopsDB, $xoopsConfig;
		$this->db =& $xoopsDB;
		$this->cookie = $xoopsConfig['sessioncookie'];
		$this->expiretime = $xoopsConfig['sessionexpire'];
		if($sessionid != ""){
			$this->sessionID = $sessionid;
		}
	}

	/*
	* creates new session for user 
	* and sets a cookie containing the session id
	*/
	function store(){
		global $xoopsConfig;
		$ip = $this->getClientIP();
		$this->hash = md5(uniqid(rand()));
		$this->db->query("DELETE FROM ".$this->db->prefix("session")." WHERE uid=".$this->uid." AND ip='".$ip."'",1);
        	$this->db->query("INSERT INTO ".$this->db->prefix("session")." (uid, time, ip, hash) VALUES ($this->uid, ".time().", '$ip', '$this->hash')",1);
		setcookie($this->cookie, $this->hash, time()+360000, "/",  "", 0);
	}

	function setUid($value){
		$this->uid = $value;
	}

	function uid(){
		return $this->uid;
	}

	function isValid() {
		unset($uid);
		$mintime = time() - $this->expiretime;
        	$this->db->query("DELETE FROM ".$this->db->prefix("session")." WHERE time < $mintime");
		$ip = $this->getClientIP();
		$sql = "SELECT uid FROM ".$this->db->prefix("session")." WHERE hash='".$this->sessionID."' AND ip='$ip'";
		//echo $sql;
    		if(!$result=$this->db->query($sql)){
			return false;
		}
		
    		list($uid) = $this->db->fetch_row($result);
		if($uid){
			$this->uid = $uid;
    			return true;
    		}
    		return false;
	}

	/*
	* updates the session table
	*/
	function update(){
		$ip = $this->getClientIP();
		$this->db->query("UPDATE ".$this->db->prefix("session")." SET time=".time()." WHERE uid=".$this->uid." AND ip='$ip'");
	}

	/*
	*  Gets client's IP based on the server settings 
	*  the client is coming from
	*/
	function getClientIP(){
		if ( getenv("HTTP_VIA") ) {
			if ( $value = getenv("HTTP_CLIENT_IP") ) {
				if ( ereg ("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $value, $array) ) {
					return $array[0];
				}
			} elseif ( $value = getenv("HTTP_X_FORWARDED_FOR") ) {
				if ( ereg ("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $value, $array) ) {
					return $array[0];
				}
			} elseif ( $value = getenv("HTTP_X_FORWARDED") ) {
				if ( ereg ("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $value, $array) ) {
					return $array[0];
				}
			} elseif ( $value = getenv("HTTP_FORWARDED_FOR") ) {
				if ( ereg ("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $value, $array) ) {
					return $array[0];
				}
			} elseif ( $value = getenv("HTTP_FORWARDED") ) {
				if ( ereg ("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $value, $array) ) {
					return $array[0];
				}
			}
		}
		return getenv("REMOTE_ADDR");
	}
}


Class XoopsUser{

	var $db;
	var $cookie;
	var $inactive=0;
	var $uid;
	var $name;
	var $uname;
	var $email;
	var $url;
	var $user_avatar;
	var $user_regdate;
	var $user_icq;
	var $user_from;
	var $user_sig;
	var $user_viewemail;
	var $actkey;
	var $user_aim;
	var $user_yim;
	var $user_msnm;
	var $pass;
	var $posts;
	var $attachsig;
	var $rank;
	var $level;
	var $theme;
	var $timezone_offset;
	var $umode;
	var $uorder;
	var $ublockon;
	var $ublock;
	var $user_occ;
	var $bio;
	var $user_intrest;
	var $groups = array();

	function XoopsUser($uid=0){
		global $xoopsDB, $xoopsConfig;
		$this->db = $xoopsDB;
		$this->cookie = $xoopsConfig['sessioncookie'];
		if($uid != 0){
			if(is_array($uid)){
				$this->makeUser($uid);
			}else{
				$this->uid = $uid;
				$this->getUser();
			}
		}
	}

	function login($uname, $pass){
		$myts = new MyTextSanitizer();
		$uname = $myts->makeTboxData4Save($uname);
		$result = $this->db->query("SELECT uid, pass, level FROM ".$this->db->prefix("users")." WHERE uname='".$uname."'");
		if($this->db->num_rows($result)==1) {
			$convert = 0;
			$passed = 0;
			$setinfo = $this->db->fetch_array($result);
			if($setinfo['level'] == 0 && $setinfo['uid']){
				$this->inactive = 1;
				return false;
			}
			if ($setinfo['uid'] != 0) {
				if ($pass == $setinfo['pass']) {
					$convert =1;
					$passed=1;
				} 
				if ($setinfo['pass']==md5($pass)) {
					$passed=1;
				}
				if ($convert) {
					$newpass = md5($pass);
					$this->db->query("UPDATE ".$this->db->prefix("users")." SET pass = '$newpass' WHERE uname = '$uname'");
				}
				if ($passed) {
					$session = new XoopsUserSession;
					$session->setUid($setinfo['uid']);
					$session->store();
					return $setinfo['uid'];
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/*
	* Logs out user
	* removes user session from session table 
	* and unsets user session cookie
	*/
	function logout(){
		$this->db->query("DELETE FROM ".$this->db->prefix("session")." WHERE uid=".$this->uid."");
	}

	function getUser(){
		$sql = "SELECT u.*, s.* FROM ".$this->db->prefix("users")." u, ".$this->db->prefix("users_status")." s WHERE u.uid=".$this->uid." AND u.uid=s.uid";
		//echo $sql;
		if(!$result = $this->db->query($sql,1)){
			die("ERROR");
		}
		$numrows = $this->db->num_rows($result);
		if($numrows == 1){
			$myrow = $this->db->fetch_array($result);
			$this->makeUser($myrow);
		}elseif($numrows==0){
			$this->inactive = 1;
		}else{
			die("Duplicate User Entries!");
		}
	}

	function makeUser($array){
		foreach ($array as $key=>$value) {
			$this->$key = $value;
		}
		if($this->level == 0){
			$this->inactive = 1;
		}
		$groups = XoopsGroup::getByUser($this);
		$this->groups = $groups;
	}

	function is_admin($moduleid=0){
		if ( XoopsGroup::hasAdminRight($this->groups, $moduleid) ) {
			return TRUE;
		}
		return FALSE;
	}

	function getRank(){
		$myts = new MyTextSanitizer();
		if($this->rank != 0){
        		$sql = "SELECT rank_title AS title, rank_image AS image FROM ".$this->db->prefix("ranks")." WHERE rank_id = $this->rank";
		}else{
			$sql = "SELECT rank_title AS title, rank_image AS image FROM ".$this->db->prefix("ranks")." WHERE rank_min <= " . $this->posts . " AND rank_max >= " . $this->posts . " AND rank_special = 0";
		}
		$result = $this->db->query($sql);
        	$myrow = $this->db->fetch_array($result);
		$myrow['title'] = $myts->makeTboxData4Show($myrow['title']);
		return $myrow;
	}

	function lastVisit(){
		$sql = "SELECT time FROM ".$this->db->prefix("lastseen")." WHERE uid=".$this->uid."";
		$result = $this->db->query($sql);
		list($timestamp) = $this->db->fetch_row($result);
		return $timestamp;
	}

	function delete(){
		if(isset($this->uid)){
			$result = $this->db->query("DELETE FROM ".$this->db->prefix("users")." WHERE uid=".$this->uid."");
			if(!$result){
				die("Could not delete the user account");
			}
			$this->db->query("DELETE FROM ".$this->db->prefix("users_status")." WHERE uid=".$this->uid."");
			$this->db->query("DELETE FROM ".$this->db->prefix("groups_users_link")." WHERE uid=".$this->uid."");
			$this->db->query("DELETE FROM ".$this->db->prefix("bb_forum_mods")." WHERE user_id=".$this->uid."");
			$this->db->query("DELETE FROM ".$this->db->prefix("lastseen")." WHERE uid=".$this->uid."");
			$this->db->query("DELETE FROM ".$this->db->prefix("priv_msgs")." WHERE to_userid=".$this->uid."");
			$this->logout();
		}
	}

	function activate(){
		$result = $this->db->query("UPDATE ".$this->db->prefix("users")." SET level=1 WHERE uid=".$this->uid()." AND actkey='".$this->actkey()."'",1);
		if (!$result) {
			return false;
		}
		return true;
	}
		
	function is_active(){
		if($this->inactive == 1){
			return false;
		}else{
			return true;
		}
	}

	function uid(){
		return $this->uid;
	}

	function name($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$name = $myts->makeTboxData4Show($this->name);
				break;
			case "E":
				$name = $myts->makeTboxData4Edit($this->name);
				break;
		}
		return $name;
	}

	function uname($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$uname = $myts->makeTboxData4Show($this->uname);
				break;
			case "E":
				$uname = $myts->makeTboxData4Edit($this->uname);
				break;
		}
		return $uname;
	}

	function email($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$email = $myts->makeTboxData4Show($this->email);
				break;
			case "E":
				$email = $myts->makeTboxData4Edit($this->email);
				break;
		}
		return $email;
	}

	function url($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$url = $myts->makeTboxData4Show($this->url);
				break;
			case "E":
				$url = $myts->makeTboxData4Edit($this->url);
				break;
		}
		return $url;
	}

	function user_avatar($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$user_avatar = $myts->makeTboxData4Show($this->user_avatar);
				break;
			case "E":
				$user_avatar = $myts->makeTboxData4Edit($this->user_avatar);
				break;
		}
		return $user_avatar;
	}

	function user_regdate(){
		return $this->user_regdate;
	}

	function user_icq(){
		return $this->user_icq;
	}

	function user_from($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$user_from = $myts->makeTboxData4Show($this->user_from);
				break;
			case "E":
				$user_from = $myts->makeTboxData4Edit($this->user_from);
				break;
		}
		return $user_from;
	}

	function user_sig($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$user_sig = $myts->makeTareaData4Show($this->user_sig);
				break;
			case "E":
				$user_sig = $myts->makeTareaData4Edit($this->user_sig);
				break;
		}
		return $user_sig;
	}

	function user_viewemail(){
		return $this->user_viewemail;
	}

	function actkey(){
		return $this->actkey;
	}

	function user_aim(){
		return $this->user_aim;
	}

	function user_yim(){
		return $this->user_yim;
	}

	function user_msnm(){
		return $this->user_msnm;
	}

	function pass(){
		return $this->pass;
	}

	function posts(){
		return $this->posts;
	}

	function attachsig(){
		return $this->attachsig;
	}

	function rank($astitle=true){
		if($astitle){
			$rank = array();
			$rank = $this->getRank();
			return $rank;
		}else{
			return $this->rank;
		}
	}

	function level(){
		return $this->level;
	}

	function theme(){
		return $this->theme;
	}

	function timezone(){
		return $this->timezone_offset;
	}

	function umode(){
		return $this->umode;
	}

	function uorder(){
		return $this->uorder;
	}

	function ublockon(){
		return $this->ublockon;
	}

	function ublock($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$ublock = $myts->makeTareaData4Show($this->ublock);
				break;
			case "E":
				$ublock = $myts->makeTareaData4Edit($this->ublock);
				break;
		}
		return $ublock;
	}

	function user_occ($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$user_occ = $myts->makeTboxData4Show($this->user_occ);
				break;
			case "E":
				$user_occ = $myts->makeTboxData4Edit($this->user_occ);
				break;
		}
		return $user_occ;
	}

	function bio($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$bio = $myts->makeTareaData4Show($this->bio);
				break;
			case "E":
				$bio = $myts->makeTareaData4Edit($this->bio);
				break;
		}
		return $bio;
	}

	function user_intrest($format="S"){
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$user_intrest = $myts->makeTboxData4Show($this->user_intrest);
				break;
			case "E":
				$user_intrest = $myts->makeTboxData4Edit($this->user_intrest);
				break;
		}
		return $this->user_intrest;
	}

	/*
	*  returns an array of groups this user belongs
	*/
	function groups(){
		return $this->groups;
	}

	/*
	 * Function to get user name from a certain user id
	 */
	function get_uname_from_id($userid) {
		global $xoopsDB, $xoopsConfig;
		$db =& $xoopsDB;
		$ret = $xoopsConfig['anonymous'];
		$myts = new MyTextSanitizer();
		$sql = "SELECT uname FROM ".$db->prefix("users")." WHERE uid = $userid";
		if(!$result = $db->query($sql)) {
			return $ret;
		}
		if(!list($uname) = $db->fetch_row($result)) {
			return $ret;
		}
		$uname = $myts->makeTboxData4Show($uname);
		return $uname;
	}


	function incrementPost($uid){
		global $xoopsDB;
		$db =& $xoopsDB;
		$sql = "UPDATE ".$db->prefix("users")." SET posts=posts+1 WHERE uid=".$uid."";
		$result = $db->query($sql);
	}

}
?>