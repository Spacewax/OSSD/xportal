<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
################################################################################
include_once($xoopsConfig['root_path']."class/xoopsmodule.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/xoopsgroup.php");

class XoopsBlock {
	var $db;
	var $bid;
	var $mid;
	var $function;
	var $options;
	var $optionsArr=array();
	var $name;
	var $position;
	var $title;
	var $content;
	var $side;
	var $weight;
	var $visible;
	var $type;
	var $c_type;

	function XoopsBlock($bid = -1) {
		global $xoopsDB;
		$this->db = $xoopsDB;
		if ( $bid != -1 ) {
			if ( is_array($bid) ) {
				$this->makeBlock($bid);
			} else {
				$this->getBlock($bid);
			}
		}
	}

	function getBlock($bid) {
		$sql = "SELECT * FROM ".$this->db->prefix("newblocks")." WHERE bid = ".$bid."";
		$result = $this->db->query($sql, 1);
		$array = $this->db->fetch_array($result);
		$this->makeBlock($array);
	}

	function store() {
		$myts = new MyTextSanitizer();
		$title = "";
		$content = "";
		$name = "";
		$options = "";
		if ( isset($this->name) && $this->name != "" ) {
			$name = $myts->makeTboxData4Save($this->name);
		}
		if ( isset($this->title) && $this->title != "" ) {
			$title = $myts->makeTboxData4Save($this->title);
		}
		if ( isset($this->content) && $this->content != "" ) {
			$content = $myts->makeTareaData4Save($this->content);
		}
		if ( isset($this->optionsArr) && (count($this->optionsArr) > 0) ) {
			$options = implode("|", $this->optionsArr);
		}
		if ( !isset($this->position) || $this->position != 1 ) {
			$this->position = 0;
		}
		if ( !isset($this->side) ) {
			$this->side = 0;
		}
		if ( !isset($this->weight) || $this->weight == "" || !is_numeric($this->weight) ) {
			$this->weight = 0;
		}
		if ( !isset($this->visible) ) {
			$this->visible = 0;
		}
		if ( !isset($this->bid) ) {
			$newbid = $this->db->GenID("blocks_bid_seq");
			$sql = "INSERT INTO ".$this->db->prefix("newblocks")." (bid, mid, func_num, options, name, position, title, content, side, weight, visible, type, c_type) VALUES ($newbid, 0, 0,'','".$name."',$this->position,'".$title."','".$content."',$this->side,$this->weight,$this->visible,'C','".$this->c_type."')";
		} else {
			$sql = "UPDATE ".$this->db->prefix("newblocks")." SET options='".$options."'";
			if ( $this->type() == "C" ) {
				$sql .= ", name='".$this->name."'";
			}
			$sql .= ", position=".$this->position.", title='".$title."', content='".$content."', side=".$this->side.", weight=".$this->weight.", visible=".$this->visible.", c_type='".$this->c_type."' WHERE bid=".$this->bid."";
		}
		$this->db->query($sql,1);
	}

	function delete() {
		$sql = "DELETE FROM ".$this->db->prefix("newblocks")." WHERE bid=".$this->bid."";
		$this->db->query($sql,1);
	}

	function setName($value) {
		$this->name = $value;
	}

	function setTitle($value) {
		$this->title = $value;
	}

	function setContent($value) {
		$this->content = $value;
	}

	function setOptions($array) {
		$this->optionsArr = $array;
	}

	function setPosition($value) {
		$this->position = $value;
	}

	function setSide($value) {
		$this->side = $value;
	}

	function setWeight($value) {
		$this->weight = $value;
	}

	function setVisible($value) {
		$this->visible = $value;
	}

	function setType($value) {
		$this->type = $value;
	}

	function setCType($value) {
		$this->c_type = $value;
	}

	function makeBlock($array) {
		foreach($array as $key=>$value){
			$this->$key = $value;
		}
	}

	function bid() {
		return $this->bid;
	}

	function mid() {
		return $this->mid;
	}
	
	function funcNum() {
		return $this->func_num;
	}

	function options($asarray = 1) {
		if ( $asarray ) {
			$options = array();
			$options = explode("|", $this->options);
			return $options;
		}
		return $this->options;
	}

	function name() {
		$myts = new MyTextSanitizer();
		$name = $myts->makeTboxData4Show($this->name);
		return $name;
	}

	function position() {
		return $this->position;
	}

	/**
   	* do stripslashes/htmlspecialchars according to the needed output
	*
	* @param $format      output use: S for Show and E for Edit
	* @returns string
	*/
	function title($format = "S") {
		$myts = new MyTextSanitizer();
		switch($format){
			case "S":
				$title = $myts->makeTboxData4Show($this->title);
			case "E":
				$title = $myts->makeTboxData4Edit($this->title);
		}
		return $title;
	}

	/**
   	* do stripslashes/htmlspecialchars according to the needed output
	*
	* @param $format      output use: S for Show and E for Edit
	* @param $c_type    type of block content
	* @returns string
	*/
	function content($format = "S", $c_type = "T") {
		$myts = new MyTextSanitizer();
		switch ( $format ) {
			case "S":
				// check the type of content
				// H : custom HTML block
				// P : custom PHP block
				// S : use text sanitizater (smilies enabled)
				// T : use text sanitizater (smilies disabled)
				if ( $c_type == "H" ) {
					$content = $myts->oopsStripSlashesRT($this->content);
					return $content;
				} elseif ( $c_type == "P" ) {
					ob_start();
					print eval($this->content);
    					$content = ob_get_contents();
    					ob_end_clean();
					return $content;
				} elseif ( $c_type == "S" ) {
					$content = $myts->makeTareaData4Show($this->content, 1, 1);
					return $content;
				} else {
					$content = $myts->makeTareaData4Show($this->content);
					return $content;
				}
			case "E":
				$content = $myts->makeTareaData4Edit($this->content);
		}
		return $content;
	}

	function side() {
		return $this->side;
	}

	function visible() {
		return $this->visible;
	}

	function weight() {
		return $this->weight;
	}

	function type() {
		return $this->type;
	}

	function c_type() {
		return $this->c_type;
	}

	function buildBlock() {
		global $xoopsConfig;
		$block = array();
		// M for module block, S for system block C for Custom
		if ( $this->type != "C" ) {
			$module = new XoopsModule($this->mid());
			// get block display function
			$show_func = $module->b_show_func($this->funcNum());
			if ( !$show_func ) {
				return false;
			}
			// must get lang files b4 execution of the function
			if ( file_exists($xoopsConfig['root_path']."modules/".$module->dirname()."/blocks/".$module->blockFuncFile($this->funcNum())."") ) {
				if ( file_exists($xoopsConfig['root_path']."modules/".$module->dirname()."/language/".$xoopsConfig['language']."/blocks.php") ) {
					include_once($xoopsConfig['root_path']."modules/".$module->dirname()."/language/".$xoopsConfig['language']."/blocks.php");
				} elseif ( file_exists($xoopsConfig['root_path']."modules/".$module->dirname()."/language/english/blocks.php") ) {
					include_once($xoopsConfig['root_path']."modules/".$module->dirname()."/language/english/blocks.php");
				}
				// get the file where the function is defined
				include_once($xoopsConfig['root_path']."modules/".$module->dirname()."/blocks/".$module->blockFuncFile($this->funcNum())."");
				$options = array();
				$options = $this->options();
				if ( function_exists($show_func) ) {
					// execute the function
					$block = $show_func($options);
					if ( !$block ) {
						return false;
					}
				} else {
					return false;
				}
				// align content if there is additional content
				// in db
				$block['content'] = $this->buildContent($this->position(),$block['content'],$this->content("S",$this->c_type));
				// replace title if there is additional title
				// in db
				$block['title'] = $this->buildTitle($block['title'], $this->title());
			} else {
				return false;
			}
		}else{
			// it is a custom block, so just return the contents
			// and title in db
			$block['title'] = $this->title();
			$block['content'] = $this->content("S",$this->c_type);
		}
		return $block;
	}

	/*
	* Aligns the content of a block
	* If position is 0, content in DB is positioned
	* before the original content
	* If position is 1, content in DB is positioned
	* after the original content
	*/
	function buildContent($position,$content="",$contentdb="") {
		if ( $position == 0 ) {
			$ret = $contentdb.$content;
		} elseif ( $position == 1 ) {
			$ret = $content.$contentdb;
		}
		return $ret;
	}

	function buildTitle($originaltitle, $newtitle="") {
		if ($newtitle != "") {
			$ret = $newtitle;
		} else {
			$ret = $originaltitle;
		}
		return $ret;
	}


	/**
   	* get all the blocks that match the supplied parameters
	* @param $side   0: sideblock - left
	*		 1: sideblock - right
	*		 2: sideblock - left and right
	*		 3: centerblock - left
	*		 4: centerblock - right
	*		 5: centerblock - center
	* @param $user    XoopsUser class object
	* @param $visible   0: not visible 1: visible
	* @param $orderby   order of the blocks
	* @returns array of block objects
	*/
	function getAllBlocks($side, $user=0, $visible=-1, $orderby="weight") {
		$return_arr = array();
		if ( $orderby != "bid" ) {
			$orderby .= ", bid";
		}
		// get both sides in sidebox? (some themes need this)
		if ( $side == 2 ) {
			$side = "(side=0 OR side=1)"; 
		} else {
			$side = "side=".$side."";
		}
		$sql = "SELECT * FROM ".$this->db->prefix("newblocks")." WHERE ".$side."";
		if ( $visible != -1 ) {
			if ( $visible != 0 ) {
				$sql .= " AND visible>0 AND visible<=".$visible."";
			} else {
				$sql .= " AND visible=0";
			}
		}
		if ( $user != 0 && get_class($user) == "xoopsuser" ) {
			$usergroup = $user->groups();
		} else {
			$usergroup = XoopsGroup::getAnonGroup();
		}
		$restr_mids = XoopsGroup::getRestrictedModules($usergroup);
		//$admin_mids = XoopsGroup::getAdminRightModules($usergroup);
		foreach ( $restr_mids as $restr_mid ) {
			//if ( !in_array($restr_mid, $admin_mids) ) {
				$sql .= " AND mid<>".$restr_mid."";
			//}
		}
		$sql .= " ORDER BY $orderby";
		$result = $this->db->query($sql,1);
		while ( $myrow = $this->db->fetch_array($result) ) {
			$return_arr[] = new XoopsBlock($myrow);
		}
		return $return_arr;
	}

	function isCustom() {
		if ( $this->type() == "C" ) {
			return true;
		}
		return false;
	}

	/**
   	* gets html form for editting block options
	* 
	*/
	function getOptions() {
		global $xoopsConfig;
		if ( $this->type != "C" ) {
			$module = new XoopsModule($this->mid());
			$edit_func = $module->b_edit_func($this->funcNum());
			if ( !$edit_func ) {
				return false;
			}
			if ( file_exists($xoopsConfig['root_path']."modules/".$module->dirname()."/blocks/".$module->blockFuncFile($this->funcNum())."") ) {
				if ( file_exists($xoopsConfig['root_path']."modules/".$module->dirname()."/language/".$xoopsConfig['language']."/blocks.php") ) {
					include_once($xoopsConfig['root_path']."modules/".$module->dirname()."/language/".$xoopsConfig['language']."/blocks.php");
				} elseif ( file_exists($xoopsConfig['root_path']."modules/".$module->dirname()."/language/english/blocks.php") ) {
					include_once($xoopsConfig['root_path']."modules/".$module->dirname()."/language/english/blocks.php");
				}
				include_once($xoopsConfig['root_path']."modules/".$module->dirname()."/blocks/".$module->blockFuncFile($this->funcNum())."");
				$options = array();
				$options = $this->options();
				$edit_form = $edit_func($options);
				if ( !$edit_form ) {
					return false;
				}
				return $edit_form;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function move ($new_weight) {
		if ( $this->weight > $new_weight ) {
			$this->moveUp($new_weight);
		} elseif ( $this->weight < $new_weight ) {
			$this->moveDown($new_weight);
		} else {
			return true;
		}
	}

	/*
	*  Moves up the position of a block to specified position (weight)
	*/
	function moveUp($new_weight) {
		$this->db->query("UPDATE ".$this->db->prefix("newblocks")." SET weight=weight+1 WHERE weight>=".$new_weight." AND weight<".$this->weight."");
		$this->db->query("UPDATE ".$this->db->prefix("newblocks")." SET weight=".$new_weight." WHERE bid=".$this->bid."");
	}

	/*
	*  Moves down the position of block to specified position (weight)
	*/
	function moveDown($new_weight) {
		$this->db->query("UPDATE ".$this->db->prefix("newblocks")." SET weight=weight+1 WHERE weight>=".$new_weight."");
		$this->db->query("UPDATE ".$this->db->prefix("newblocks")." SET weight=".$new_weight." WHERE bid=".$this->bid."");
	}
}

?>