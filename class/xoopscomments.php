<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
###############################################################################

include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");
include_once($xoopsConfig['root_path']."class/module.errorhandler.php");
include_once($xoopsConfig['root_path']."class/xoopstree.php");

class XoopsComments {
	var $ctable;
	var $itemname;
	var $itemid;
	var $order;
	var $subject;
	var $comment;
	var $host_name;
	var $tid;
	var $pid="";
	var $date;
	var $nohtml=0;
	var $nosmiley=0;
	var $prefix;
	var $uid;
	var $icon;
	var $db;
	
	function XoopsComments($ctable,$itemname,$tid=-1) {
		global $xoopsDB;
		$this->db = $xoopsDB;
		$this->ctable = $ctable;
		$this->itemname = $itemname;
		if ( is_array($tid) ) {
			$this->makeComment($tid);
		} elseif ( $tid != -1 ) {
			$this->tid = $tid;
			$this->getComment($this->tid);
		} else {
			$this->pid = 0;
		}
	}

	function setCommentTable($value) {
		$this->ctable = $value;
	}

	function setItemId($value) {
		$this->itemid = $value;
	}

	function setOrder($value) {
		$this->order = $value;
	}

	function getTopComments() {
		$sql = "SELECT * FROM ".$this->ctable." WHERE ".$this->itemname."=".$this->itemid." AND pid=".$this->pid." ORDER BY ".$this->order."";
		//echo $sql;
		$arr = array();
		$result = $this->db->query($sql);
		$numrows = $this->db->num_rows($result);
		if ( $numrows == 0 ) {
			return 0;
		}
		while ( $myrow = $this->db->fetch_array($result) ) {
			$arr[] =  new XoopsComments($this->ctable,$this->itemname,$myrow);
		}
		return $arr;
	}

	function getCommentTree($pid) {
		$mytree = new XoopsTree($this->ctable, "tid", "pid");
		$tarray = array();
		$arr = array();
		$tarray = $mytree->getChildTreeArray($pid, "tid");
		foreach ( $tarray as $ele ) {
			$arr[] = new XoopsComments($this->ctable,$this->itemname,$ele);
		}
		return $arr;
	}

	function getAllComments() {
		$sql = "SELECT * FROM ".$this->ctable." WHERE ".$this->itemname."=".$this->itemid." ORDER BY ".$this->order."";
		//echo $sql;
		$arr=array();
		$result=$this->db->query($sql);
		$numrows = $this->db->num_rows($result);
		if ( $numrows == 0 ) {
			return 0;
		}
		while ( $myrow = $this->db->fetch_array($result) ) {
			$arr[] =  new XoopsComments($this->ctable,$this->itemname,$myrow);
		}
		return $arr;
	}

	function setParent($value) {
		$this->pid=$value;
	}

	function setSubject($value) {
		$this->subject=$value;
	}

	function setComment($value) {
		$this->comment=$value;
	}

	function setUid($value) {
		$this->uid=$value;
	}

	function setHostname($value) {
		$this->host_name=$value;
	}

	function setNohtml($value=0) {
		$this->nohtml=$value;
	}

	function setNosmiley($value=0) {
		$this->nosmiley=$value;
	}

	function setIcon($value) {
		$this->icon=$value;
	}

	function store() {
		$newpost = 0;
		$myts = new MyTextSanitizer;
		$subject =$myts->censorString($this->subject);
		$comment =$myts->censorString($this->comment);
		$subject = $myts->makeTboxData4Save($subject);
		$comment = $myts->makeTareaData4Save($comment);
		if ( !$this->nohtml ) {
			$this->nohtml = 0;
		}
		if ( !$this->nosmiley ) {
			$this->nosmiley = 0;
		}
		if ( !isset($this->tid) ) {
			$newpost = 1;
			$newtid = $this->db->GenID($this->table."_tid_seq");
			$sql = "INSERT INTO ".$this->ctable." (tid, pid, ".$this->itemname.", date, uid, host_name, subject, comment, nohtml, nosmiley, icon) VALUES ($newtid,".$this->pid.",".$this->itemid.",".time().",".$this->uid.",'".$this->host_name."','".$subject."','".$comment."',".$this->nohtml.",".$this->nosmiley.",'".$this->icon."')";
			//			echo $sql;
		} else {
			$sql = "UPDATE ".$this->ctable." SET subject='".$subject."',comment='".$comment."',nohtml=".$this->nohtml.",nosmiley=".$this->nosmiley.",icon='".$this->icon."' WHERE tid=".$this->tid."";
			//			echo $sql;
		}
		if ( !$result = $this->db->query($sql,1) ) {
			ErrorHandler::show('0022');
		}
		if ( $newpost ) {
			$sql = "UPDATE ".$this->db->prefix("users")." SET posts=posts+1 WHERE uid=".$this->uid."";
			$result = $this->db->query($sql,1);
			if (!$result) {
				echo "Could not update user posts.";
			}
		}
	}

	function getComment($tid) {
		$sql = "SELECT * FROM ".$this->ctable." WHERE tid=".$tid."";
		//echo $sql;
		$result = $this->db->query($sql);
		$array = $this->db->fetch_array($result);
		$this->makeComment($array);	
	}

	function makeComment($array) {
		foreach ( $array as $key=>$value ) {
			if ( $key == $this->itemname ) {
				$this->itemid = $value;
			} else {
				$this->$key = $value;
			}
		}
	}
	
	function getTopPageComments($order=1,$mode="thread") {
		if ( $order == 1 ) {
			$qorder = "date DESC";
		} else {
			$qorder = "date ASC";
		}
		$this->setOrder($qorder);
		if ( $mode == "flat" ) {
			$commentsArray = $this->getAllComments();
		} elseif ( $mode=="nocomments" ) {
			$commentsArray = false;
		} else {
			$commentsArray = $this->getTopComments();
		}
		return $commentsArray;
	}

	function delete() {
		$sql = "DELETE FROM ".$this->ctable." WHERE tid=".$this->tid."";
		if ( !$result = $this->db->query($sql,1) ) {
			ErrorHandler::show('0022');
		}
		//echo $sql;
		$sql = "UPDATE ".$this->db->prefix("users")." SET posts=posts-1 WHERE uid=".$this->uid."";
		if ( !$result = $this->db->query($sql,1) ) {
			echo "Could not update user posts.";
		}
		//echo $sql;
		$mytree = new XoopsTree($this->ctable, "tid", "pid");
		$arr = $mytree->getAllChild($this->tid, "tid");
		$size = sizeof($arr);
		if ( $size>0 ) {
			for ( $i=0;$i<$size;$i++ ) {
				$sql = "DELETE FROM ".$this->ctable." WHERE tid=".$arr[$i][tid]."";
				if ( !$result = $this->db->query($sql,1) ) {
					echo "Could not delete comment.";
				}
				
				$sql = "UPDATE ".$this->db->prefix("users")." SET posts=posts-1 WHERE uid=".$arr[$i]['uid']."";
				if ( !$result = $this->db->query($sql,1) ) {
					echo "Could not update user posts.";
				}
				//echo $sql;
			}
		}
		return ($size + 1);
	}

	function search($term, $mode, $searchin="text", $orderby="c.date", $userid=0, $start=0) {
		global $xoopsConfig;
		$query = "SELECT c.tid, c.".$this->itemname.", c.date, c.uid, c.subject, c.icon, u.uname FROM ".$this->ctable." c, ".$this->db->prefix("users")." u";
		$count_query = "SELECT COUNT(*) FROM ".$this->ctable." c, ".$this->db->prefix("users")." u";

		if ( isset($term) && $term != "" ) {
			$terms = split(" ",addslashes($term));						// Get all the words into an array
			$addquery .= "(c.comment LIKE '%$terms[0]%'";		
			$subquery .= "(c.subject LIKE '%$terms[0]%'"; 
	
			if ( $mode=="any" ) {
				// AND/OR relates to the ANY or ALL on Search Page
				$andor = "OR";
			} else {
				$andor = "AND";
			}
			for ( $i=1;$i<sizeof($terms);$i++ ) {
				$addquery.=" $andor c.comment LIKE '%$terms[$i]%'";
				$subquery.=" $andor c.subject LIKE '%$terms[$i]%'"; 
			}	     
			$addquery.=")";
			$subquery.=")";
		}

		if ( $userid ) {
			if ( isset($addquery) ) {
				$addquery.=" AND c.uid=$userid";
      			$subquery.=" AND c.uid=$userid";
   			} else {
      			$addquery.=" c.uid=$userid";
      			$subquery.=" c.uid=$userid";
   			}
		}	
		if ( isset($addquery) ) {
			switch ($searchin) { 
				case "both" : 
					$query .= " WHERE ( $subquery OR $addquery ) AND "; 
					$count_query .= " WHERE ( $subquery OR $addquery ) AND "; 
      				break; 
    			case "title" : 
      				$query .= " WHERE ( $subquery ) AND "; 
					$count_query .= " WHERE ( $subquery ) AND "; 
      				break; 
    			default: 
      				$query .= " WHERE ( $addquery ) AND "; 
					$count_query .= " WHERE ( $addquery ) AND "; 
      				break; 
   			}
		} else {
			$query.=" WHERE ";
			$count_query.=" WHERE ";
		}

   		$query .= " c.uid = u.uid";
		$count_query .= " c.uid = u.uid";

   		$query .= " ORDER BY $orderby";
		//echo $query;
		//echo "<br>".$count_query;
		if ( !$result = $this->db->query($query,1,30,$start) ) {
			die();
		}
		$rows = array();
		if ( $this->db->num_rows($result)>0 ) {
			while ( $row = $this->db->fetch_array($result) ) {
				if ( !$row['uname'] ) {
					$row['uname'] = $xoopsConfig['anonymous'];
				}
				array_push($rows, $row);
			}
		} else {
			return 0;
		}
		list($total_rows) = $this->db->fetch_row($this->db->query($count_query));
		$searchresult=array();
		$searchresult['rows'] = $rows;
		$searchresult['numrows'] = $total_rows;
		return $searchresult;
	}

	function tid() {
		return $this->tid;
	}

	function subject($format="Show") {
		$myts = new MyTextSanitizer;
		$smiley = 1;
		if ( $this->nosmiley() ) {
			$smiley = 0;
		}
		switch ( $format ) {
			case "Show":
				$subject= $myts->makeTboxData4Show($this->subject,$smiley);
				break;
			case "Edit":
				$subject = $myts->makeTboxData4Edit($this->subject);
				break;
			case "Preview":
				$subject = $myts->makeTboxData4Preview($this->subject,$smiley);
				break;
			case "InForm":
				$subject = $myts->makeTboxData4PreviewInForm($this->subject);
				break;
		}
		return $subject;
	}

	function comment($format="Show") {
		$myts = new MyTextSanitizer;
		$smiley = 1;
		$html = 1;
		if ( $this->nohtml() ) {
			$html = 0;
		}
		if ( $this->nosmiley() ) {
			$smiley = 0;
		}
		switch ( $format ) {
			case "Show":
				$comment = $myts->makeTareaData4Show($this->comment,$html,$smiley);
				break;
			case "Edit":
				$comment = $myts->makeTareaData4Edit($this->comment);
				break;
			case "Preview":
				$comment = $myts->makeTareaData4Preview($this->comment,$html,$smiley);
				break;
			case "InForm":
				$comment = $myts->makeTareaData4PreviewInForm($this->comment);
				break;
		}
		return $comment;
	}

	function postdate() {
		return $this->date;
	}

	function uid() {
		return $this->uid;
	}

	function parent() {
		return $this->pid;
	}

	function itemid() {
		return $this->itemid;
	}

	function nohtml() {
		return $this->nohtml;
	}

	function nosmiley() {
		return $this->nosmiley;
	}

	function icon() {
		return $this->icon;
	}

	function hostname() {
		return $this->host_name;
	}

	function prefix() {
		return $this->prefix;
	}

	function printNavBar($order=1, $mode="thread"){
		global $PHP_SELF, $xoopsConfig, $xoopsTheme, $xoopsUser;
		echo "<table width='100%' border='0' cellspacing='1' cellpadding='2'><tr><td bgcolor='". $xoopsTheme['bgcolor1'] ."' align='center'><form method='get' action='".$PHP_SELF."'><select name='mode'><option value='nocomments'";
		if ( $mode == "nocomments" ) { 
			echo " selected='selected'";
		}
		echo ">". _NOCOMMENTS ."</option><option value='flat'";
		if ($mode == 'flat') {
			echo " selected='selected'";
		}
		echo ">". _FLAT ."</option><option value='thread'";
		if ( $mode == "thread" || $mode == "" ) { 
			echo " selected='selected'";
		}
		echo ">". _THREAD ."</option></select><select name='order'><option value='0'";
		if ( $order != 1 ) {
			echo " selected='selected'";
			$order = 0;
		}
		echo ">". _OLDESTFIRST ."</option><option value='1'";
		if ( $order == 1 ) {
			echo " selected='selected'";
		}
		echo ">". _NEWESTFIRST ."</option></select><input type='hidden' name='".$this->itemname."' value='".$this->itemid."' /><input type='submit' value='". _REFRESH ."' />";
		if ( $xoopsConfig['anonpost'] || $xoopsUser ) {
			echo "&nbsp;<input type='button' onclick=\"location='".$PHP_SELF."?op=new&amp;".$this->itemname."=".$this->itemid."&amp;order=".$order."&amp;mode=".$mode."'\" value='"._POSTCOMMENT."' />";
		}
		echo "</form></td></tr></table>";
	}
}

?>