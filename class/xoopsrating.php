<?php
###############################################################################
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
###############################################################################

class XoopsRating{
	
	var $db;
	var $table;
	var $itemname;
	var $interval;
	var $itemid;
	var $rating;
	var $userid;
	var $ip;

	function XoopsRating($table, $itemname, $interval=864000){
		global $xoopsDB;
		$this->db = $xoopsDB;
		$this->table = $table;
		$this->itemname = $itemname;
		$this->intereval = $interval;
	}

	function setItemId($value){
		$this->itemid = $value;
	}

	function setRating($value){
		$this->rating = $value;
	}

	function setUserId($value){
		$this->userid = $value;
	}

	function setIp($value){
		$this->ip = $value;
	}

	function store(){
		if ( !$this->isValid() ) {
			return false;
		}
		$new_ratingid = $this->db->GenID($this->table."_ratingid_seq");
		$this->db->query("INSERT INTO ".$this->table." (ratingid, ".$this->itemname.", rating, userid, ip, timestamp) VALUES ($new_ratingid, $this->itemid, $this->rating, $this->userid, $this->ip, ".time().")");
		$mintime = time() - $this->interval;
		$this->db->query("DELETE FROM ".$this->table." WHERE timestamp<".$mintime."");
		return true;
	}

	function isValid(){
		if ( $this->userid != 0 ) {
			$result=$xoopsDB->query("select count(*) from ".$this->table." where ".$this->itemname."=".$this->itemid." AND userid=".$this->userid."");
        		list($count) = $xoopsDB->fetch_row($result)) {
			if ( $count>0 ) {
				return false;
			}
        	} else {
			$result=$xoopsDB->query("select count(*) from ".$this->table." where ".$this->itemname."=".$itemid." AND ip=".$this->ip."");
        		list($count) = $xoopsDB->fetch_row($result)) {
			if ( $count>0 ) {
				return false;
			}
		}
		return true;
	}
}
?>