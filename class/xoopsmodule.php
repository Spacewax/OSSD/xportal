<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
###############################################################################

class XoopsModule{

	var $db;
	var $mid;
	var $name;
	var $dirname;
	var $hasadmin;
	var $hasmain;
	var $hassearch;
	var $modinfo = array();

	function XoopsModule($mid = -1, $load=true){
		global $xoopsDB;
		$this->db = $xoopsDB;
		if ( $mid != -1 ) {
			if ( is_array($mid) ) {
				$this->makeModule($mid);
			} else {
				$this->getModule($mid);
			}
			if ( $load ) {
				$this->loadModInfo($this->dirname);
			}
		}
	}

	function loadModInfo($dirname) {
		global $xoopsConfig;
		if(file_exists($xoopsConfig['root_path']."modules/".$dirname."/language/".$xoopsConfig['language']."/modinfo.php")) {
			include_once($xoopsConfig['root_path']."modules/".$dirname."/language/".$xoopsConfig['language']."/modinfo.php");
		} elseif(file_exists($xoopsConfig['root_path']."modules/".$dirname."/language/english/modinfo.php")) {
			include_once($xoopsConfig['root_path']."modules/".$dirname."/language/english/modinfo.php");
		}
		if(file_exists($xoopsConfig['root_path']."modules/".$dirname."/xoops_version.php")) {
			include($xoopsConfig['root_path']."modules/".$dirname."/xoops_version.php");
		} else {
			echo "Module File for $dirname Not Found!";
			exit();
		}
		//foreach($modversion as $key => $value){
			//$this->modinfo[$key] = $value;
			$this->modinfo = $modversion;
		//}
	}

	function getModule($mid){
		$sql = "SELECT * FROM ".$this->db->prefix("modules")." WHERE mid = ".$mid."";
		$result = $this->db->query($sql, 1);
		$array = $this->db->fetch_array($result);
		$this->makeModule($array);
	}

	function makeModule($array){
		foreach($array as $key=>$value){
			$this->$key = $value;
		}
	}

	function getByDirname($dirname){
		$db =& $GLOBALS["xoopsDB"];
		$ret = false;
		$sql = "SELECT * FROM ".$db->prefix("modules")." WHERE dirname = '".$dirname."'";
		$result = $db->query($sql, 1);
		if ( $db->num_rows($result)==1 ) {
			$array = $db->fetch_array($result);
			$ret = new XoopsModule($array);
		}
		return $ret;
	}

	/**
   	* Activate modules
	* If the module has blocks, insert blocks info in blocks table
	*/
	function activate(){
		$newmid = $this->db->GenID("modules_mid_seq");
		if(!isset($this->modinfo['hasAdmin']) || $this->modinfo['hasAdmin'] != 1){
			$this->modinfo['hasAdmin'] = 0;
		}
		if(!isset($this->modinfo['hasMain']) || $this->modinfo['hasMain'] != 1){
			$this->modinfo['hasMain'] = 0;
		}
		if(!isset($this->modinfo['hasSearch']) || $this->modinfo['hasSearch'] != 1){
			$this->modinfo['hasSearch'] = 0;
		}
		$version = $this->modinfo['version'] * 100;
		$sql = "INSERT INTO ".$this->db->prefix("modules")." (mid, name, dirname, version, last_update, hasmain, hasadmin, hassearch) VALUES ($newmid, '".addslashes($this->modinfo['name'])."', '".addslashes($this->modinfo['dirname'])."', ".$version.", ".time().", ".$this->modinfo['hasMain'].", ".$this->modinfo['hasAdmin'].", ".$this->modinfo['hasSearch']." )";
		$this->db->query($sql,1);
		if(!$newmid){
			$newmid = $this->db->Insert_ID();
		}
		if(isset($this->modinfo['blocks'])){
			$i = 1;
			foreach($this->modinfo['blocks'] as $block){
				$options = "";
				if(isset($block['options']) && $block['options']!= ""){
					$options = $block['options'];
				}
				$newbid = $this->db->GenID("blocks_bid_seq");
				$sql = "INSERT INTO ".$this->db->prefix("newblocks")." (bid, mid, func_num, options, name, position, title, content, side, weight, visible, type) VALUES ($newbid, $newmid, $i,'$options','".addslashes($block['name'])."',0,'','',0,0,0,'M')";
				$this->db->query($sql,1);
				$i++;
			}
		}
	}

	function deactivate(){
		$sql = "DELETE FROM ".$this->db->prefix("modules")." WHERE mid=".$this->mid()."";
		$this->db->query($sql,1);
		$sql = "DELETE FROM ".$this->db->prefix("newblocks")." WHERE mid=".$this->mid()."";
		$this->db->query($sql,1);
		$sql = "DELETE FROM ".$this->db->prefix("groups_modules_link")." WHERE mid=".$this->mid()."";
		$this->db->query($sql,1);
	}

	function isActivated(){
		$result = $this->db->query("SELECT COUNT(*) FROM ".$this->db->prefix("modules")." WHERE mid=".$this->mid()."",1);
		list($count) = $this->db->fetch_row($result);
		if($count>0){
			return TRUE;
		}
		return FALSE;
	}

	/**
   	* Updates module info in module table and blocks info in blocks table.
	* It is generally used when blocks are added or modified.
	* Or to change the block title in blocks admin menu.
	* This does not change the actual block contents in DB
	*/
	function update(){
		if(!isset($this->modinfo['hasAdmin']) || $this->modinfo['hasAdmin'] != 1){
			$this->modinfo['hasAdmin'] = 0;
		}
		if(!isset($this->modinfo['hasMain']) || $this->modinfo['hasMain'] != 1){
			$this->modinfo['hasMain'] = 0;
		}
		if(!isset($this->modinfo['hasSearch']) || $this->modinfo['hasSearch'] != 1){
			$this->modinfo['hasSearch'] = 0;
		}
		$version = $this->modinfo['version'] * 100;
		$sql = "UPDATE ".$this->db->prefix("modules")." SET name='".addslashes($this->modinfo['name'])."', dirname='".addslashes($this->modinfo['dirname'])."', version=".$version.", last_update=".time().", hasmain=".$this->modinfo['hasMain'].", hasadmin=".$this->modinfo['hasAdmin'].", hassearch=".$this->modinfo['hasSearch']." WHERE mid=".$this->mid."";
		$this->db->query($sql,1);
		if(isset($this->modinfo['blocks'])){
			$i = 0;
			foreach($this->modinfo['blocks'] as $block){
				$i++;
				$options = "";
				if(isset($block['options']) && $block['options']!= ""){
					$options = $block['options'];
				}
				$sql = "SELECT COUNT(*) FROM ".$this->db->prefix("newblocks")." WHERE mid=".$this->mid." AND func_num=".$i."";
				list($count) = $this->db->fetch_row($this->db->query($sql));
				if($count>0){
					$sql = "UPDATE ".$this->db->prefix("newblocks")." SET options='".$options."', name='".addslashes($block['name'])."' WHERE mid=".$this->mid." AND func_num=".$i."";
				}else{
					$newbid = $this->db->GenID("blocks_bid_seq");
					$sql = "INSERT INTO ".$this->db->prefix("newblocks")." (bid, mid, func_num, options, name, position, title, content, side, weight, visible, type) VALUES (".$newbid.", ".$this->mid.", ".$i.",'".$options."','".addslashes($block['name'])."',0,'','',0,0,0,'M')";
				}
				$this->db->query($sql,1);
			}
			$sql = "SELECT COUNT(*) FROM ".$this->db->prefix("newblocks")." WHERE mid=".$this->mid."";
			list($count) = $this->db->fetch_row($this->db->query($sql));
			if ( $count > $i ) {
				for ( $j = $i + 1; $j <= $count; $j++ ) {
					$sql = "DELETE FROM ".$this->db->prefix("newblocks")." WHERE mid=".$this->mid." AND func_num=".$j."";
					$this->db->query($sql);
				}
			}
		}
	}

	function current_version(){
		if(isset($this->version)){
			$version = round($this->version / 100,2);
		}else{
			$version = $this->version();
		}
		return $version;
	}

	function last_update(){
		return $this->last_update;
	}

	function mid(){
		if(isset($this->mid)){
			return $this->mid;
		}
	}

	function hasAdmin(){
		if($this->modinfo['hasAdmin']){
			return true;
		}
		return false;
	}

	function hasMain(){
		if($this->modinfo['hasMain']){
			return true;
		}
		return false;
	}

	function name(){
		if(isset($this->modinfo['name'])){
			return $this->modinfo['name'];
		}else{
			return false;
		}
	}

	function dirname(){
		if(isset($this->modinfo['dirname'])){
			return $this->modinfo['dirname'];
		}else{
			return false;
		}
	}

	function date(){
		if(isset($this->modinfo['date'])){
			return $this->modinfo['date'];
		}else{
			return false;
		}
	}

	function version(){
		if(isset($this->modinfo['version'])){
			return $this->modinfo['version'];
		}else{
			return false;
		}
	}

	function description(){
		if(isset($this->modinfo['description'])){
			return $this->modinfo['description'];
		}else{
			return false;
		}
	}

	function author(){
		if(isset($this->modinfo['author'])){
			return $this->modinfo['author'];
		}else{
			return false;
		}
	}

	function credits(){
		if(isset($this->modinfo['credits'])){
			return $this->modinfo['credits'];
		}else{
			return false;
		}
	}

	function help(){
		if(isset($this->modinfo['help'])){
			return $this->modinfo['help'];
		}else{
			return false;
		}
	}

	function license(){
		if(isset($this->modinfo['license'])){
			return $this->modinfo['license'];
		}else{
			return false;
		}
	}

	function official(){
		if(isset($this->modinfo['official'])){
			return $this->modinfo['official'];
		}else{
			return 1;
		}
	}

	function image(){
		if(isset($this->modinfo['image'])){
			return $this->modinfo['image'];
		}else{
			return false;
		}
	}

	function adminpath(){
		if(isset($this->modinfo['adminpath'])){
			return $this->modinfo['adminpath'];
		}else{
			return false;
		}
	}

	function blockFuncFile($func_number){
		return $this->modinfo['blocks'][$func_number]['file'];
	}

	function b_show_func($func_number){
		if ( isset($this->modinfo['blocks'][$func_number]['show_func']) ){
			return $this->modinfo['blocks'][$func_number]['show_func'];
		}
		return false;
	}

	function b_edit_func($func_number){
		if ( isset($this->modinfo['blocks'][$func_number]['edit_func']) ){
			return $this->modinfo['blocks'][$func_number]['edit_func'];
		}
		return false;
	}

	function mainLink(){
		global $xoopsConfig;
		if($this->hasMain()){
			$ret = "<a href=\\\"".$xoopsConfig['xoops_url']."/modules/".$this->dirname()."/\\\">".$this->name()."</a>";
			return $ret;
		}
		return false;
	}

	function subLink(){
		global $xoopsConfig;
		$ret = array();
		if(isset($this->modinfo['sub']) && is_array($this->modinfo['sub'])){
			foreach($this->modinfo['sub'] as $submenu){
				$ret[] .= "<a href=\\\"".$xoopsConfig['xoops_url']."/modules/".$this->dirname()."/".$submenu['url']."\\\">".$submenu['name']."</a>";
			}
			return $ret;
		}
		return false;
	}

	function getHasMainModulesList(){
		$db =& $GLOBALS["xoopsDB"];
		$ret = array();
		$result = $db->query("SELECT name, dirname from ".$db->prefix("modules")." WHERE hasmain=1 ORDER BY name ASC");
		while($myrow=$db->fetch_array($result)){
			$ret[$myrow['dirname']] = $myrow['name'];
		}
		return $ret;
	}


	function Search($term="", $andor="AND", $limit=0, $offset=0, $userid=0){
		global $xoopsConfig;
		if (!isset($this->modinfo['hasSearch']) || !$this->modinfo['hasSearch'] || !isset($this->modinfo['search']['func']) || !isset($this->modinfo['search']['file']) || $this->modinfo['search']['func'] == "" || $this->modinfo['search']['file'] == ""){
			return false;
		}
		if (file_exists($xoopsConfig['root_path']."modules/".$this->dirname()."/".$this->modinfo['search']['file'])){
			include_once($xoopsConfig['root_path']."modules/".$this->dirname()."/".$this->modinfo['search']['file']);
		} else {
			return false;
		}
		//$ret = array();
		if (function_exists($this->modinfo['search']['func'])) {
			$func = $this->modinfo['search']['func'];
			$ret = $func($term, $andor, $limit, $offset, $userid);
			return $ret;
		} else {
			return false;
		}
	}

	function getHasSearchModulesList($aslist = true){
		$db =& $GLOBALS["xoopsDB"];
		$ret = array();
		$result = $db->query("SELECT mid,name from ".$db->prefix("modules")." WHERE hassearch=1 ORDER BY name ASC");
		while($myrow=$db->fetch_array($result)){
			if ($aslist) {
				$ret[$myrow['mid']] = $myrow['name'];
			} else {
				$ret[] = $myrow['mid'];
			}
		}
		return $ret;
	}

	function getHasAdminModulesList($aslist = true){
		$db =& $GLOBALS["xoopsDB"];
		$ret = array();
		$result = $db->query("SELECT mid,name from ".$db->prefix("modules")." WHERE hasadmin=1 ORDER BY mid ASC");
		while($myrow=$db->fetch_array($result)){
			if ($aslist) {
				$ret[$myrow['mid']] = $myrow['name'];
			} else {
				$ret[] = $myrow['mid'];
			}
		}
		return $ret;
	}

	/*
	*  Get all modules as a list
	*  set $aslist to true for ease of use with class XoopsLists
	*  $exclude is an array of module ids that you dont want to get
	*/
	function getAllModulesList($exclude=array(), $aslist = true){
		$db =& $GLOBALS["xoopsDB"];
		$ret = array();
		$sql = "SELECT mid,name from ".$db->prefix("modules")."";
		if ( $size = count($exclude) ) {
			$sql .= " WHERE mid<>".$exclude[0]."";
			for ( $i = 1; $i < $size; $i++ ) {
				$sql .= " AND mid<>".$exclude[$i]."";
			}
		}
		$sql .= " ORDER BY name ASC";
		$result = $db->query($sql);
		while($myrow=$db->fetch_array($result)){
			if ($aslist) {
				$ret[$myrow['mid']] = $myrow['name'];
			} else {
				$ret[] = $myrow['mid'];
			}
		}
		return $ret;
	}

	/*
	*  Moves up the position of selected module to specified position
	*/
	function moveUp($new_weight){
		$this->db->query("UPDATE ".$this->db->prefix("modules")." SET weight=weight+1 WHERE weight>=".$new_weight." AND weight<".$this->weight."");
		$this->db->query("UPDATE ".$this->db->prefix("modules")." SET weight=".$new_weight." WHERE mid=".$this->mid."");
	}


	/*
	*  Moves down the position of selected module to specified position
	*/
	function moveDown($new_weight){
		$this->db->query("UPDATE ".$this->db->prefix("modules")." SET weight=weight+1 WHERE weight>=".$new_weight."");
		$this->db->query("UPDATE ".$this->db->prefix("modules")." SET weight=".$new_weight." WHERE mid=".$this->mid."");
	}

	/*
	* Displays Nav Menu at the top of module admin page
	*/
	function printAdminMenu() {
		global $xoopsConfig;
		OpenTable();
		echo "<div style='text-align:center'><h4><a href='".$xoopsConfig['xoops_url']."/admin.php'>"._ADMINMENU."</a></h4></div>";
		CloseTable();
		echo "<br />";
		OpenTable();
		echo "<div style='text-align:center'><img src='".$xoopsConfig['xoops_url']."/modules/".$this->dirname()."/".$this->image()."' alt='' /><br /><h4>".$this->name()."</h4>[&nbsp;<a href='".$xoopsConfig['xoops_url']."/modules/".$this->dirname()."/".$this->adminpath()."'>"._MAIN."</a>&nbsp;|&nbsp;<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/".$this->dirname()."/".$this->help()."','Manual',600,400);\">"._MANUAL."</a>&nbsp;|&nbsp;<a href=\"javascript:openWithSelfMain('".$xoopsConfig['xoops_url']."/modules/system/admin.php?fct=version&amp;mid=".$this->mid()."','Info',300,230);\">"._INFO."</a>&nbsp;]</div>";
		CloseTable();
	}
}

?>