<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
###############################################################################

include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

class XoopsGroup{

	var $db;
	var $groupid;
	var $name;
	var $description;
	var $type;
	var $admin_right_modules = array();
	var $restricted_modules = array();

	function XoopsGroup($id = 0){
		global $xoopsDB;
		$this->db = $xoopsDB;
		if ( $id != 0 ) {
			if ( is_array($id) ) {
				$this->makeGroup($id);
			} else {
				$this->getGroup($id);
			}
		}
	}

	function getGroup($groupid) {
		$sql = "SELECT * FROM ".$this->db->prefix("groups")." WHERE groupid=".$groupid."";
		$result = $this->db->query($sql);
		$array = $this->db->fetch_array($result);
		$this->makegroup($array);	
	}

	function makeGroup($array) {
		foreach ( $array as $key=>$value ) {
			$this->$key = $value;
		}
	}

	function setName($value){
		$this->name = $value;
	}
	
	function setDescription($value){
		$this->description = $value;
	}

	function setAdminRightModules($array){
		$this->admin_right_modules = $array;
	}

	function setRestrictedModules($array){
		$this->restricted_modules = $array;
	}

	function setType($value){
		$this->type = $value;
	}

	function store(){
		$myts = new MyTextSanitizer;
		if ( isset($this->name) && $this->name != "" ) {
			$name = $myts->makeTboxData4Save($this->name);
		}
		if ( isset($this->description) && $this->description != "" ) {
			$description = $myts->makeTareaData4Save($this->description);
		}
		if ( isset($this->type) && $this->type == "Anonymous" ) {
			$type = "Anonymous";
		} elseif ( isset($this->type) && $this->type == "User" ) {
			$type = "User";
		} elseif ( is_array($this->admin_right_modules) && count($this->admin_right_modules)>0 ) {
			$type = "Admin";
		} else {
			$type = "Custom";
		}
		if ( !isset($this->groupid) ) {
			$newgroupid = $this->db->GenID("groups_groupid_seq");
			$sql = "INSERT INTO ".$this->db->prefix("groups")." (groupid, name, description, type) VALUES ($newgroupid, '".$name."', '".$description."','".$type."')";
		} else {
			$sql = "DELETE FROM ".$this->db->prefix("groups_modules_link")." WHERE groupid=".$this->groupid."";
        		if ( !$result = $this->db->query($sql,1) ) {
				exit();
			}
			$sql = "UPDATE ".$this->db->prefix("groups")." SET name='".$name."', description='".$description."', type='".$type."' WHERE groupid=".$this->groupid."";
			$newgroupid = $this->groupid;
		}
		if ( !$result = $this->db->query($sql,1) ) {
			exit();
		}
		if ( $newgroupid == 0 ) {
			$newgroupid = $this->db->Insert_ID();
		}
		foreach($this->admin_right_modules as $admin_right_module){
			$sql = "INSERT INTO ".$this->db->prefix("groups_modules_link")." (groupid, mid, type) VALUES ($newgroupid, $admin_right_module, 'A')";
			$result = $this->db->query($sql,1);
		}
		foreach($this->restricted_modules as $restricted_module){
			if ( !in_array($restricted_module, $this->admin_right_modules ) ) {
				$sql = "INSERT INTO ".$this->db->prefix("groups_modules_link")." (groupid, mid, type) VALUES ($newgroupid, $restricted_module, 'R')";
				$result = $this->db->query($sql,1);
			}
		}
		return true;
	}

	function delete(){
		if ( !$result = $this->db->query("DELETE FROM ".$this->db->prefix("groups")." WHERE groupid=".$this->groupid."",1) ) {
			exit();
		}
		if ( !$result = $this->db->query("DELETE FROM ".$this->db->prefix("groups_users_link")." WHERE groupid=".$this->groupid."",1) ) {
			exit();
		}
		if ( !$result = $this->db->query("DELETE FROM ".$this->db->prefix("groups_modules_link")." WHERE groupid=".$this->groupid."",1) ) {
			exit();
		}
		return TRUE;
	}

	function groupid(){
		return $this->groupid;
	}

	function name($format="S"){
		$myts = new MyTextSanitizer;
		switch($format){
			case "S":
				$name = $myts->makeTboxData4Show($this->name);
				break;
			case "E":
				$name = $myts->makeTboxData4Edit($this->name);
				break;
			case "P":
				$name = $myts->makeTboxData4Preview($this->name);
				break;
			case "F":
				$name = $myts->makeTboxData4PreviewInForm($this->name);
				break;
		}
		return $name;
	}

	function description($format="S"){
		$myts = new MyTextSanitizer;
		switch($format){
			case "S":
				$description = $myts->makeTareaData4Show($this->description);
				break;
			case "E":
				$description = $myts->makeTareaData4Edit($this->description);
				break;
			case "P":
				$description = $myts->makeTareaData4Preview($this->description);
				break;
			case "F":
				$description = $myts->makeTareaData4PreviewInForm($this->description);
				break;
		}
		return $description;
	}

	function type(){
		return $this->type;
	}
		
	function getAdminRightModules($groupid){
		global $xoopsDB;
		$db =& $xoopsDB;
		$sql = "SELECT DISTINCT mid FROM ".$db->prefix("groups_modules_link")." WHERE type='A'";
		if ( is_array($groupid) ) {
			$sql .= " AND (groupid=".$groupid[0]."";
			$size = sizeof($groupid);
			if ( $size  > 1 ) {
				for ( $i = 1; $i < $size; $i++ ) {
					$sql .= " OR groupid=".$groupid[$i]."";
				}
			}
			$sql .= ")";
		} else {
			$sql .= " AND groupid=".$groupid."";
		}
		$result = $db->query($sql);
		$mids = array();
		while( $myrow = $db->fetch_array($result) ) {
			$mids[] = $myrow['mid'];
		}
		return $mids;
	}

	function getRestrictedModules($groupid){
		global $xoopsDB;
		$db =& $xoopsDB;
		$sql = "";
		$mids = array();
		if ( is_array($groupid) ) {
			$size = sizeof($groupid);
			for( $i = 0; $i < $size; $i++ ) {
				$sql = "SELECT DISTINCT mid FROM ".$db->prefix("groups_modules_link")." WHERE type='R' AND groupid=".$groupid[$i]."";
				$result = $db->query($sql);
				$temparray = array();
				while( $myrow = $db->fetch_array($result) ) {
					$temparray[] = $myrow['mid'];
				}
				if ( $i ==0 ) {
					$mids = $temparray;
				} else {
					$mids = array_intersect($temparray, $mids);
				}
				unset($result);
			}
		} else {
			$sql = "SELECT DISTINCT mid FROM ".$db->prefix("groups_modules_link")." WHERE type='R' AND groupid=".$groupid."";
			$result = $db->query($sql);
			while( $myrow = $db->fetch_array($result) ) {
				$mids[] = $myrow['mid'];
			}
		}
		return $mids;
	}

	function hasAccessRight($moduleid, $groupid=0) {
		global $xoopsDB;
		$db =& $xoopsDB;
		$size = 1;
		$sql = "SELECT COUNT(*) FROM ".$db->prefix("groups_modules_link")." WHERE type='R' AND mid=".$moduleid."";
		if ( is_array($groupid) ) {
			$sql .= " AND (groupid=".$groupid[0]."";
			$size = sizeof($groupid);
			if ( $size > 1 ) {
				for ( $i = 1; $i < $size; $i++ ) {
					$sql .= " OR groupid=".$groupid[$i]."";
				}
			}
			$sql .= ")";
		} elseif ( $groupid != 0 ) {
			$sql .= " AND groupid=".$groupid."";
		} else {
			$groupid = XoopsGroup::getAnonGroup();
			$sql .= " AND groupid=".$groupid."";
		}
		list($count) = $db->fetch_row($db->query($sql));
		// return false only when all groups for this user are restricted, 
		if ( $count == $size ) {
			return FALSE;
		}
		return TRUE;
	}

	function hasAdminRight($groupid, $moduleid=0) {
		global $xoopsDB;
		$db =& $xoopsDB;
		$sql = "SELECT COUNT(*) FROM ".$db->prefix("groups_modules_link")." WHERE type='A'";
		if ( $moduleid != 0 ) {
			$sql .= " AND mid=".$moduleid."";
		}
		if ( is_array($groupid) ) {
			$sql .= " AND (groupid=".$groupid[0]."";
			if ( $size = sizeof($groupid) > 1 ) {
				for ( $i = 1; $i < $size; $i++ ) {
					$sql .= " OR groupid=".$groupid[$i]."";
				}
			}
			$sql .= ")";
		} elseif ( $groupid != 0 ) {
			$sql .= " AND groupid=".$groupid."";
		} else {
			return FALSE;
		}
		list($count) = $db->fetch_row($db->query($sql));
		if ( $count>0 ) {
			return TRUE;
		}
		return FALSE;
	}

	function getByUser($user, $asobject=false){
		global $xoopsDB;
		$db =& $xoopsDB;
		$ret = array();
		if ( get_class($user) == "xoopsuser" ) {
			$sql = "SELECT g.* FROM ".$db->prefix("groups")." g LEFT JOIN ".$db->prefix("groups_users_link")." l ON l.groupid=g.groupid WHERE l.uid=".$user->uid()."";
			$result = $db->query($sql);
			if ( !$db->num_rows($result) ) {
				$default_group = XoopsGroup::getDefaultUserGroup(true);
				$default_group->addMember($user);
				if ( $asobject ) {
					$ret[] = $default_group;
				} else {
					$ret[] = $default_group->groupid();
				}
			}
			
			while ( $myrow = $db->fetch_array($result) ) {
				if ( $asobject ) {
					$ret[] = new XoopsGroup($myrow);
				} else {
					$ret[] = $myrow['groupid'];
				}
			}
		}
		return $ret;
	}

	function getAnonGroup($asobject=false){
		global $xoopsDB;
		$db =& $xoopsDB;
		if ( !$asobject ) {
			$sql = "SELECT groupid FROM ".$db->prefix("groups")." WHERE type='Anonymous'";
			list($ret) = $db->fetch_row($db->query($sql));
		} else {
			$sql = "SELECT * FROM ".$db->prefix("groups")." WHERE type='Anonymous'";
			$myrow = $db->fetch_array($db->query($sql));
			$ret = new XoopsGroup($myrow);
		}
		return $ret;
	}

	function getDefaultUserGroup($asobject=false){
		global $xoopsDB;
		$db =& $xoopsDB;
		if ( !$asobject ) {
			$sql = "SELECT groupid FROM ".$db->prefix("groups")." WHERE type='User'";
			list($ret) = $db->fetch_row($db->query($sql));
		} else {
			$sql = "SELECT * FROM ".$db->prefix("groups")." WHERE type='User'";
			$myrow = $db->fetch_array($db->query($sql));
			$ret = new XoopsGroup($myrow);
		}
		return $ret;
	}

	function getMembersList(){
		$myts = new MyTextSanitizer();
		$sql = "SELECT uid, uname FROM ".$this->db->prefix("groups_users_link")." WHERE groupid=".$this->groupid."";
		while ( $myrow = $this->db->fetch_array($this->db->query($sql)) ) {
			$myrow['uid'] = $myts->makeTboxData4Show($myrow['uname']);
		}
		return $myrow;
	}

	function addMember($user){
		if ( get_class($user) == "xoopsuser" ) {
			$this->db->query("INSERT INTO ".$this->db->prefix("groups_users_link")." (groupid, uid) VALUES (".$this->groupid.", ".$user->uid().")");
			return true;
		}
		return false;
	}
}
?>