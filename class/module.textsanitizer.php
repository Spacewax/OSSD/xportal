<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Goghs Cheng (http://www.eqiao.com/)                             	     //
// Kazumi Ono (http://www.mywebaddons.com/)                                  //
################################################################################
if(!defined("XOOPS_C_TEXTSANITIZER_INCLUDED")){
	define("XOOPS_C_TEXTSANITIZER_INCLUDED",1);

class MyTextSanitizer {

	/*
	* path to smilie images folder from main directory
	*/
	var $smileyPath = "images/smilies";

	/*
	* allowed HTML tags
	*/
	var $allowed;

	/*
	* Constructor of this class
	* Gets allowed html tags from admin config settings
	* <br> should not be allowed since nl2br will be used 
	* when storing data
	*/
	function MyTextSanitizer(){
		global $xoopsConfig;
		$this->allowed = $xoopsConfig['allowed_html'];
	}

    	function smiley($message) {
		global $xoopsDB, $xoopsConfig;
		if ($getsmiles = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("smiles"))){
			while ($smiles = $xoopsDB->fetch_array($getsmiles)) {
				$message = str_replace($smiles['code'], "<img src=\"".$xoopsConfig['xoops_url']."/".$this->smileyPath."/".$smiles['smile_url']."\" />", $message);
			}
		}
		return $message;
	}

	function desmile($message) {
		global $xoopsDB, $xoopsConfig;
		if ($getsmiles = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("smiles"))){
			while ($smiles = $xoopsDB->fetch_array($getsmiles)) {
				$message = str_replace("<img src=\"".$xoopsConfig['xoops_url']."/".$this->smileyPath."/".$smiles['smile_url']."\" />", $smiles['code'], $message);
			}
		}
		return $message;
	}
	
	
	/*
 	* Nathan Codding - Jan. 12, 2001.
 	* Performs [quote][/quote] bbencoding on the given string, and returns the results.
 	* Any unmatched "[quote]" or "[/quote]" token will just be left alone. 
 	* This works fine with both having more than one quote in a message, and with nested quotes.
 	* Since that is not a regular language, this is actually a PDA and uses a stack. Great fun.
 	*
 	* Note: This function assumes the first character of $message is a space, which is added by 
 	* bbencode().
 	*/
	function bbencode_quote($message){
		$message = " " . $message;
		// First things first: If there aren't any "[quote]" strings in the message, we don't
		// need to process it at all.
	
		if (!strpos(strtolower($message), "[quote]")){
			return $message;	
		}
		$stack = Array();
		$curr_pos = 1;
		while ($curr_pos && ($curr_pos < strlen($message))){	
			$curr_pos = strpos($message, "[", $curr_pos);
	
			// If not found, $curr_pos will be 0, and the loop will end.
			if ($curr_pos){
				// We found a [. It starts at $curr_pos.
				// check if it's a starting or ending quote tag.
				$possible_start = substr($message, $curr_pos, 7);
				$possible_end = substr($message, $curr_pos, 8);
				if (strcasecmp("[quote]", $possible_start) == 0){
					// We have a starting quote tag.
					// Push its position on to the stack, and then keep going to the right.
					array_push($stack, $curr_pos);
					++$curr_pos;
				}else if (strcasecmp("[/quote]", $possible_end) == 0){
					// We have an ending quote tag.
					// Check if we've already found a matching starting tag.
					if (sizeof($stack) > 0){
						// There exists a starting tag. 
						// We need to do 2 replacements now.
						$start_index = array_pop($stack);

						// everything before the [quote] tag.
						$before_start_tag = substr($message, 0, $start_index);

						// everything after the [quote] tag, but before the [/quote] tag.
						$between_tags = substr($message, $start_index + 7, $curr_pos - $start_index - 7);

						// everything after the [/quote] tag.
						$after_end_tag = substr($message, $curr_pos + 8);

						$message = $before_start_tag . "<!-- BBCode Quote Start --><table border=\"0\" align=\"center\" width=\"85%\"><tr><td><small>"._QUOTEC."</small><hr></td></tr><tr><td><small><blockquote>";
						$message .= $between_tags . "</blockquote></small></td></tr><tr><td><hr></td></tr></table><!-- BBCode Quote End -->";
						$message .= $after_end_tag;
					
						// Now.. we've screwed up the indices by changing the length of the string. 
						// So, if there's anything in the stack, we want to resume searching just after it.
						// otherwise, we go back to the start.
						if (sizeof($stack) > 0){
							$curr_pos = array_pop($stack);
							array_push($stack, $curr_pos);
							++$curr_pos;
						}else{
							$curr_pos = 1;
						}
					}else{
						// No matching start tag found. Increment pos, keep going.
						++$curr_pos;	
					}
				}else{
					// No starting tag or ending tag.. Increment pos, keep looping.,
					++$curr_pos;	
				}
			}
		} // while
	
		return $message;
	
	} // bbencode_quote()

	 /*
	 * Rewritten by Nathan Codding - Feb 6, 2001.
	 * - Goes through the given string, and replaces xxxx://yyyy with an HTML <a> tag linking
	 * 	to that URL
	 * - Goes through the given string, and replaces www.xxxx.yyyy[zzzz] with an HTML <a> tag linking
	 * 	to http://www.xxxx.yyyy[/zzzz] 
	 * - Goes through the given string, and replaces xxxx@yyyy with an HTML mailto: tag linking
	 *		to that email address
	 * - Only matches these 2 patterns either after a space, or at the beginning of a line
	 *
	 * Notes: the email one might get annoying - it's easy to make it more restrictive, though.. maybe
	 * have it require something like xxxx@yyyy.zzzz or such. We'll see.
	 */
	function makeClickable($text) {
	    	// pad it with a space so we can match things at the start of the 1st line.
		$ret = " " . $text;
	
		// matches an "xxxx://yyyy" URL at the start of a line, or after a space.
		// xxxx can only be alpha characters.
		// yyyy is anything up to the first space, newline, or comma.
		$ret = preg_replace("#([\n ])([a-z]+?)://([^, \n\r]+)#i", "\\1<!-- BBCode auto-link start --><a href=\"\\2://\\3\" target=\"_blank\">\\2://\\3</a><!-- BBCode auto-link end -->", $ret);
	
		// matches a "www.xxxx.yyyy[/zzzz]" kinda lazy URL thing
		// Must contain at least 2 dots. xxxx contains either alphanum, or "-"
		// yyyy contains either alphanum, "-", or "."
		// zzzz is optional.. will contain everything up to the first space, newline, or comma.
		// This is slightly restrictive - it's not going to match stuff like "forums.foo.com"
		// This is to keep it from getting annoying and matching stuff that's not meant to be a link.
		$ret = preg_replace("#([\n ])www\.([a-z0-9\-]+)\.([a-z0-9\-.\~]+)((?:/[^, \n\r]*)?)#i", "\\1<!-- BBCode auto-link start --><a href=\"http://www.\\2.\\3\\4\" target=\"_blank\">www.\\2.\\3\\4</a><!-- BBCode auto-link end -->", $ret);
	
		// matches an email@domain type address at the start of a line, or after a space.
		// Note: before the @ sign, the only valid characters are the alphanums and "-", "_", or ".".
		// After the @ sign, we accept anything up to the first space, linebreak, or comma.
		$ret = preg_replace("#([\n ])([a-z0-9\-_.]+?)@([^, \n\r]+)#i", "\\1<!-- BBcode auto-mailto start --><a href=\"mailto:\\2@\\3\">\\2@\\3</a><!-- BBCode auto-mailto end -->", $ret);
	
		// Remove our padding..
		$ret = substr($ret, 1);
	
		return($ret);
	}

	/*
	* Nathan Codding - August 24, 2000.
	* Takes a string, and does the reverse of the PHP standard function
	* HtmlSpecialChars().
	* Original Name : undo_htmlspecialchars
	*/
    	function undoHtmlSpecialChars($input) {
		$input = preg_replace("/&gt;/i", ">", $input);
        	$input = preg_replace("/&lt;/i", "<", $input);
        	$input = preg_replace("/&quot;/i", "\"", $input);
        	$input = preg_replace("/&amp;/i", "&", $input);
		return $input;
	}

	
	function oopsNl2Br($string) { 
		$string = preg_replace("/(\015\012)|(\015)|(\012)/","<br />",$string); 
    		$string = str_replace("<br /><br><br />","<br />",$string); 
		return $string; 
	} 

	/*
	* if magic_quotes_gpc is off, add back slashes
	*/
	function oopsAddSlashes($text) {
		if (!get_magic_quotes_gpc()) {
			$text = addslashes($text);
		} 
		return $text;
	}

	/*
	* if magic_quotes_gpc is on, stirip back slashes
	*/
	function oopsStripSlashesGPC($text) {
		if (get_magic_quotes_gpc()) {
			$text = stripslashes($text);
		}
		return $text;
	}

	/*
	* if magic_quotes_runtime is on, stirip back slashes
	*/
	function oopsStripSlashesRT($text) {
		if (get_magic_quotes_runtime()) {
			$text = stripslashes($text);
		}
		return $text;
	}

	/*
	*  htmlspecialchars will not convert single quotes by default,
	*  so i made this function.
	*/
	function oopsHtmlSpecialChars($text) {
		$text = htmlspecialchars($text);
		$text = str_replace("'","&#039;",$text);
		return $text;
	}

	/*
	*  Filters both textbox and textarea form data before display
	*  For internal use
	*/
	function sanitizeForDisplay($text, $allowhtml = 0, $smiley = 1, $bbcode = 1) {
		$numargs = func_num_args();
		$text = $this->oopsStripSlashesRT($text);

		if ($numargs == 4) {
		
		if ($allowhtml == 0) {
			$text = $this->oopsHtmlSpecialChars($text);
			$text = preg_replace("/&amp;/i", "&", $text);
		}else{
			$text = strip_tags($text, $this->allowed);  // strip unallowed html tags
			$text = $this->makeClickable($text);
		}
		if ($smiley == 1) {
			$text = $this->smiley($text);
		}
		if ($bbcode == 1) {
			$text = $this->bbencode_quote($text);
		}
		$text = $this->oopsNl2Br($text);
		}
		return $text;
	}

	/*
	*  Filters both textbox and textarea form data before preview
	*  For internal use
	*/
	function sanitizeForPreview($text, $allowhtml = 0, $smiley = 1, $bbcode = 1) {
		$numargs = func_num_args();
		$text = $this->oopsStripSlashesGPC($text);

		if ($numargs == 4) {
		
		if ($allowhtml == 0) {
			$text = $this->oopsHtmlSpecialChars($text);
			$text = preg_replace("/&amp;/i", "&", $text);
		}else{
			$text = strip_tags($text, $this->allowed);  // strip unallowed html tags
			$text = $this->makeClickable($text);
		}
		if ($smiley == 1) {
			$text = $this->smiley($text);
		}
		if ($bbcode == 1) {
			$text = $this->bbencode_quote($text);
		}
		$text = $this->oopsNl2Br($text);
		}
		return $text;
	}


	/*
	*  Used for saving textbox form data.
	*  Adds slashes if magic_quotes_gpc is off.
	*/
	function makeTboxData4Save($text){
		//$text = $this->undoHtmlSpecialChars($text);
		$text = $this->oopsAddSlashes($text);
		return $text;
	}

	/*
	*  Used for displaying textbox form data from DB.
	*  Smilies can also be used.
	*/
	function makeTboxData4Show($text,$smiley=0){
		$text = $this->sanitizeForDisplay($text,0,$smiley,0); //do HtmlSpecialChars
		return $text;
	}

	/*
	*  Used when textbox data in DB is to be editted in html form.
	*  "&amp;" must be converted back to "&" to maintain the correct 
	*  ISO encoding values, which is needed for some multi-bytes chars.
	*/
	function makeTboxData4Edit($text){
		$text = $this->oopsStripSlashesRT($text);
		$text = $this->oopsHtmlSpecialChars($text);
		$text = preg_replace("/&amp;/i", "&", $text);
		return $text;
	}

	/* 
	*  Called when previewing textbox form data submitted from a form.
	*  Smilies can be used if needed
	*  Use makeTboxData4PreviewInForm when textbox data is to be
	*  previewed in textbox again
	*/
	function makeTboxData4Preview($text,$smiley=0){
		$text = $this->sanitizeForPreview($text,0,$smiley,0); //do HtmlSpecialChars
		return $text;
	}
	function makeTboxData4PreviewInForm($text){
		$text = $this->oopsStripSlashesGPC($text);
		$text = $this->oopsHtmlSpecialChars($text);
		$text = preg_replace("/&amp;/i", "&", $text);
		return $text;
	}


	/*
	*  Called before saving first-time or editted textarea
	*  data into DB
	*/
	function makeTareaData4Save($text){
		$text = $this->oopsAddSlashes($text);
		return $text;
	}

	/*
	*   Called before displaying textarea form data from DB
	*/
	function makeTareaData4Show($text, $allowhtml=1, $smiley=0, $bbcode=0){
		$text = $this->sanitizeForDisplay($text,$allowhtml,$smiley,$bbcode); 
		return $text;
	}

	/*
	*   Called when textarea data in DB is to be editted in html form
	*/
	function makeTareaData4Edit($text){
		//if magic_quotes_runtime is on, do stipslashes
		$text = $this->oopsStripSlashesRT($text);
		return $text;
	}

	/*
	*   Called when previewing textarea data which was submitted 
	*   via an html form
	*/
	function makeTareaData4Preview($text, $allowhtml=1, $smiley=0, $bbcode=0){
		$text = $this->sanitizeForPreview($text,$allowhtml,$smiley,$bbcode); 
		return $text;
	}

	/*
	*  Called when previewing textarea data whih was submitted via an
	*  html form.
	*  This time, text area data is inserted into textarea again
	*/
	function makeTareaData4PreviewInForm($text){
		//if magic_quotes_gpc is on, do stipslashes
		$text = $this->oopsStripSlashesGPC($text);
		return $text;
	}

	/*
	*  Use this function when you need to output textarea value inside
	*  quotes. For example, meta keywords are saved as textarea value
	*  but it is displayed inside <meta> tag keywords attribute with 
	*  quotes around it. This can be also used for textbox values.
	*/
	function makeTareaData4InsideQuotes($text){
		$text = $this->oopsStripSlashesRT($text);
		$text = $this->oopsHtmlSpecialChars($text);
		$text = preg_replace("/&amp;/i", "&", $text);
		return $text;
	}

	/*
 	*  Replaces banned words in a string with their replacements
 	*/
	function censorString($string) {
   		global $xoopsDB;
   		$sql = "SELECT word, replacement FROM ".$xoopsDB->prefix("bad_words")."";
   		if(!$r = $xoopsDB->query($sql))
      		die("Error, could not contact the database! Please check your database settings in config.php");
   		while($w = $xoopsDB->fetch_array($r)) {
      		if (get_magic_quotes_runtime()) {
      			$w['word'] = stripslashes($w['word']);
      			$w['replacement'] = stripslashes($w['replacement']);
      		}
      		$word = quotemeta($w['word']);
      		$string = eregi_replace(" ".$word."", " ".$w['replacement']."", $string);
      		$string = eregi_replace("^".$word."", "".$w['replacement']."", $string);
      		$string = eregi_replace("\n".$word."", "\n".$w['replacement']."", $string);
   		}
   		return($string);
	}
}

}

?>