<?php
###############################################################################
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
###############################################################################

include_once($xoopsConfig['root_path']."class/database.php");

class Database extends AbsDatabase{

var $conn;


	function connect($host, $user, $password, $db, $persistent=1){
		if($persistent){
			$this->conn = @mysql_pconnect($host, $user, $password);
		}else{
			$this->conn = @mysql_connect($host, $user, $password);
		}
		if(!$this->conn){
			print("<b>Error Connecting DB</b>: ".$this->error($this->conn).":".$this->errno($this->conn));
		}
		$selDB = mysql_select_db($db);
		if(!$selDB){
			print("<b>Error Selecting DB</b>: ".$this->errno($this->conn).":".$this->error($this->conn));
		}
	}

    	function &query($sql, $debug=false, $limit=0, $start=0){
		if (!empty($limit)) {
			if (empty($start)) {
				$start = 0;
			}
			$sql = $sql. " LIMIT ".$start.",".$limit."";
		}
        	$result =& mysql_query($sql, $this->conn);

        	$errorMsg = @mysql_error($this->conn);
        	$errorNum = @mysql_errno($this->conn);

        	if ($result){
            		return $result;
        	} else {
            		//$this->unlock();
            		if ($debug){
                		print( "<b>MySQL Query Error</b>: " . htmlentities( $sql ) . "<br><b> Error number:</b>" . $errorNum . "<br><b> Error message:</b> ". $errorMsg ."<br>" );
            		}
            		return false;
        	}
        	mysql_free_result($result);
    	}
    

    	function GenID($sequence){

		return 0; // will use auto_increment
    	}


	function fetch_row($result){
		return @mysql_fetch_row($result);
	}

	function fetch_array($result){
		return @mysql_fetch_array($result,MYSQL_ASSOC);
	}

    	function Insert_ID(){
            	return mysql_insert_id($this->conn);
    	}

	function num_rows($result){
		return @mysql_num_rows($result);
	}

    	function close(){
        	mysql_close($this->conn);
    	}

	function free_result($result){
		return mysql_free_result($result);
	}

	function error(){
		return @mysql_error();
	}

	function errno(){
		return @mysql_errno();
	}

}


?>