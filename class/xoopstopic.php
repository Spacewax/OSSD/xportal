<?php
###############################################################################
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of File: 							     //
// Kazumi Ono (http://www.mywebaddons.com/ , http://www.myweb.ne.jp/)        //
###############################################################################

include_once($xoopsConfig['root_path']."class/xoopstree.php");
include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

class XoopsTopic extends XoopsTree{

	var $topic_id;
	var $topic_pid;
	var $topic_title;
	var $topic_imgurl;
	var $prefix; // only used in topic tree


	function XoopsTopic($table, $topicid=0){
		$this->db =& $GLOBALS["xoopsDB"];
		$this->table = $table;
		$this->id = "topic_id";
		$this->pid = "topic_pid";
		if ( is_array($topicid) ) {
			$this->makeTopic($topicid);
		} elseif ( $topicid != 0 ) {
			$this->getTopic($topicid);
		} else {
			$this->topic_id = $topicid;
		}
	}

	function setTopicTitle($value){
		$this->topic_title = $value;
	}

	function setTopicImgurl($value){
		$this->topic_imgurl = $value;
	}

	function setTopicPid($value){
		$this->topic_pid = $value;
	}

	function getTopic($topicid){
		$sql = "SELECT * FROM ".$this->table." WHERE topic_id=".$topicid."";
		$array = $this->db->fetch_array($this->db->query($sql));
		$this->makeTopic($array);
	}

	function makeTopic($array){
		foreach($array as $key=>$value){
			$this->$key = $value;
		}
	}

	function store(){
		$myts = new MyTextSanitizer;
		if ( isset($this->topic_title) && $this->topic_title != "" ) {
			$title = $myts->makeTboxData4Save($this->topic_title);
		}
		if ( isset($this->topic_imgurl) && $this->topic_imgurl != "" ) {
			$imgurl = $myts->makeTboxData4Save($this->topic_imgurl);
		}
		if ( !isset($this->topic_pid) || !is_numeric($this->topic_pid) ) {
			$this->pid = 0;
		}
		if ( !isset($this->topic_id) ) {
			$newtopicid = $this->db->GenID($this->table."_topic_id_seq");
			$sql = "INSERT INTO ".$this->table." (topic_id, topic_pid, topic_title, topic_imgurl) VALUES ($newtopicid, ".$this->topic_pid.", '".$title."','".$imgurl."')";
		} else {
			$sql = "UPDATE ".$this->table." SET topic_pid=".$this->pid.", topic_title='".$title."', topic_imgurl='".$imgurl."'";
		}
		if ( !$result = $this->db->query($sql,1) ) {
			ErrorHandler::show('0022');
		}
		return true;
	}

	funtion delete(){
		$sql = "DELETE FROM ".$this->table." (topic_id, topic_pid, topic_title, topic_imgurl) VALUES ($newtopicid, ".$this->topic_pid.", '".$title."','".$imgurl."')";

	function topic_id(){
		return $this->topic_id;
	}

	function topic_pid(){
		return $this->topic_pid;
	}

	function topic_title($format="S"){
		$myts = new MyTextSanitizer;
		switch($format){
			case "S":
				$title = $myts->makeTboxData4Show($this->topic_title);
				break;
			case "E":
				$title = $myts->makeTboxData4Edit($this->topic_title);
				break;
			case "P":
				$title = $myts->makeTboxData4Preview($this->topic_title);
				break;
			case "F":
				$title = $myts->makeTboxData4PreviewInForm($this->topic_title);
				break;
		}
		return $title;
	}

	function topic_imgurl($format="S"){
		$myts = new MyTextSanitizer;
		switch($format){
			case "S":
				$imgurl= $myts->makeTboxData4Show($this->topic_imgurl);
				break;
			case "E":
				$imgurl = $myts->makeTboxData4Edit($this->topic_imgurl);
				break;
			case "P":
				$imgurl = $myts->makeTboxData4Preview($this->topic_imgurl);
				break;
			case "F":
				$imgurl = $myts->makeTboxData4PreviewInForm($this->topic_imgurl);
				break;
		}
		return $imgurl;
	}

	function prefix(){
		if ( isset($this->prefix) ) {
			return $this->prefix;
		}
	}

	function getFirstChildTopics(){
		$ret = array();
		$topic_arr = $this->getFirstChild($this->topic_id, "topic_title");
		if ( is_array($topic_arr) && count($topic_arr) ) {
			foreach($topic_arr as $topic){
				$ret[] = new XoopsTopic($this->table, $topic);
			}
		}
		return $ret;
	}

	function getAllChildTopics(){
		$ret = array();
		$topic_arr = $this->getAllChild($this->topic_id, "topic_title");
		if ( is_array($topic_arr) && count($topic_arr) ) {
			foreach($topic_arr as $topic){
				$ret[] = new XoopsTopic($this->table, $topic);
			}
		}
		return $ret;
	}

	function getChildTopicsTreeArray(){
		$ret = array();
		$topic_arr = $this->getChildTreeArray($this->topic_id, "topic_title");
		if ( is_array($topic_arr) && count($topic_arr) ) {
			foreach($topic_arr as $topic){
				$ret[] = new XoopsTopic($this->table, $topic);
			}
		}
		return $ret;
	}
}
?>